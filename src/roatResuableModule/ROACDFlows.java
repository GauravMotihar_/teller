package roatResuableModule;

import utility.HtmlReporter;
import utility.Launcher;

import java.util.List;

public class ROACDFlows {	
	
	 public static boolean flag;
		static String LCY_Validation = "labelLyc";
		static String Overall_FCY_Validation = "labelOFyc";
		static String Individual_FCY_Validation = "labelIFyc";
		static String BranchLCY_Validation = "branchCashPosLCY";
		static String BranchOverall_FCY_Validation = "branchCashPosOverallFCY";
		static String BranchIndividual_FCY_Validation = "branchCashPosIndividualFCY";
		
		
		public static void cash_Drawer_User() throws Exception
		{	
			ROATFunctions.refesh(false, Launcher.UserRole);
			if (HtmlReporter.tc_result==true)ROATModules.CD_navigateCashDrawer("tab_Sub_CashDrawer");
			if (HtmlReporter.tc_result==true)ROATModules.CD_navigateViewStatus();
			//if (HtmlReporter.tc_result==true)ROATModules.CD_validations(LCY_Validation,"LCY_Validation");
			//if (HtmlReporter.tc_result==true)ROATModules.CD_validations(Overall_FCY_Validation,"Overall_FCY_Validation");
			//if (HtmlReporter.tc_result==true)ROATModules.CD_validations(Individual_FCY_Validation,"Individual_FCY_Validation");
			if (HtmlReporter.tc_result==true)ROATModules.CD_btnBack();		
			if (HtmlReporter.tc_result==true)ROATModules.CD_UpdateRecord("user");
			if (HtmlReporter.tc_result==true)ROATModules.CD_CancelRecord();			
			if (HtmlReporter.tc_result==true)ROATModules.CD_Add_NewRecord();
			if (HtmlReporter.tc_result==true)ROATModules.CD_ActivateRecord("user");
			if (HtmlReporter.tc_result==true)ROATModules.CD_tooltip_verification();
			
		}
		//*************************************sanity********************************
		public static boolean cash_Drawer_User1() throws Exception
		{	
			boolean user = false;
			
			if (HtmlReporter.tc_result==true)ROATModules.CD_navigateCashDrawer("tab_Sub_CashDrawer");
			if (HtmlReporter.tc_result==true)ROATModules.CD_navigateViewStatus();
//			if (HtmlReporter.tc_result==true)ROATModules.CD_validations(LCY_Validation,"LCY_Validation");
//			if (HtmlReporter.tc_result==true)ROATModules.CD_validations(Overall_FCY_Validation,"Overall_FCY_Validation");
//			if (HtmlReporter.tc_result==true)ROATModules.CD_validations(Individual_FCY_Validation,"Individual_FCY_Validation");
			if (HtmlReporter.tc_result)ROATModules.CD_btnBack();		
			if (HtmlReporter.tc_result)ROATModules.CD_UpdateRecord("user");
			if (HtmlReporter.tc_result)ROATModules.CD_CancelRecord();			
//			if (HtmlReporter.tc_result)ROATModules.CD_Add_NewRecord();
			if (HtmlReporter.tc_result)ROATModules.CD_ActivateRecord("user");
			
			return user;
			
		}
		

	public static void cash_Drawer_Branch() throws Exception
		{	
		ROATFunctions.refesh(false, Launcher.UserRole);
			if (HtmlReporter.tc_result==true)ROATModules.CD_navigateCashDrawerBranch();		
			//if (HtmlReporter.tc_result==true)ROATModules.CD_Add_NewBranchRecord();
			if (HtmlReporter.tc_result==true)ROATModules.CD_ActivateRecord("branch");
			if (HtmlReporter.tc_result==true)ROATModules.CD_UpdateRecord("branch");
			if (HtmlReporter.tc_result==true)ROATModules.CD_CancelRecord();
			if (HtmlReporter.tc_result==true)ROATModules.CD_navigateBranchViewStatus();
//			if (HtmlReporter.tc_result==true)ROATModules.CD_validations(BranchLCY_Validation,"BranchLCY_Validation");
//			if (HtmlReporter.tc_result==true)ROATModules.CD_validations(BranchOverall_FCY_Validation,"BranchOverall_FCY_Validation");
//			if (HtmlReporter.tc_result==true)ROATModules.CD_validations(BranchIndividual_FCY_Validation,"BranchIndividual_FCY_Validation");
			if (HtmlReporter.tc_result==true)ROATModules.CD_btnBack(); 
			if (HtmlReporter.tc_result==true)ROATModules.CD_tooltip1_verification();
					
		}
}
