package roatResuableModule;

import java.awt.AWTException;
import java.awt.Robot;
import java.awt.event.KeyEvent;
import java.math.BigDecimal;
import java.math.RoundingMode;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;
import java.util.concurrent.TimeUnit;

import org.openqa.selenium.By;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.TimeoutException;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.ui.ExpectedCondition;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

import testCases.Driver;
import utility.HtmlReporter;
import utility.Reporter;
import utility.WebDr;

/**
 * @author G01197717
 */
public class ROATProcesses {

	static Robot robotObj;
	public static JavascriptExecutor js;
	public static BigDecimal commission, midRate;
	public static BigDecimal beforeTransaction, beforeTransactionPL, afterTransaction, afterTransactionPL;

	public static void navigateChargeApplication() throws Exception {
		ROATFunctions.refesh(false, "Teller");

		WebDr.SetPageObjects("E2E_ChargesModule");
		waitForPageLoaded();

		WebDr.waitTillElementIsDisplayed("Teller_Tab");
		WebDr.click("X", "Teller_Tab", "Clicking on Setup Tab", true);
		waitForPageLoaded();

		WebDr.click("X", "ChargesTab", "Clicking on LCY Inquiry", true);
		Reporter.ReportEvent("Navigate to Teller Charge page ", "Successfully navigated to Teller Charge page ",
				" Successfully navigated to Teller Charge page ", true);
		waitForPageLoaded();

		WebDr.click("X", "IDV", "Click on performed Manual ID&V", true);
		Reporter.ReportEvent("Select IDV", "Select Manual IDV", "Manual IDV Selected", true);
		waitForPageLoaded();
	}

	public static void tellerCA(HashMap<String, ArrayList<String>> limits, HashMap<String, String> CreditRisk,
			HashMap<String, Map<String, String>> teller_matrix, int range, String action, int forex_midrate) {

		try {
			for (int i = 1; i < 100; i++) {
				String Account = "";
				if (forex_midrate == 1) {
					Account = WebDr.getValue("AccountNumber" + i);
				} else {
					Account = WebDr.getValue("FCYAccountNumber" + i);
				}
				String condition;
				String branch_numb;
				String account_numb;
				// String fetch_account=WebDr.getValue("AccountNumber" + i);
				if (Account.equals("")) {
					break;
				} else {
					if (HtmlReporter.tc_result)
						navigateChargeApplication();
					WebDr.SetPageObjects("E2E_ChargesModule");
					condition = Account.split("\\|")[0];
					branch_numb = Account.split("\\|")[1].split("-")[0];
					account_numb = Account.split("\\|")[1].split("-")[1];
					WebDr.waitTillElementIsDisplayed("select_BranchNumber");
					WebDr.click("X", "BranchCodeCM", "Click to select Branch Code", true);
					WebDr.setText("X", "EnterBranchCodeCM", branch_numb, "Enter Branch Code", true);
					WebDr.click("X", "SelectBranchCode", "Click on entered Branch Code", true);

					Reporter.ReportEvent("Select Branch Code: ", "Select Branch Code",
							"Branch Code Selected: " + branch_numb, true);

					WebDr.setText("X", "AccountNumber", account_numb, "Set Account Number", true);
					rTab();
					waitForPageLoaded();

					ROATFunctions.clickTab();
					WebDr.Wait_Time(3000);

					switch (condition) {
					case "Teller Transactions": {
						Charge_limit_breach_check(limits, CreditRisk, teller_matrix, action, Account, range, condition,
								forex_midrate);
					}
						break;

					default: {
						break;
					}

					}

				}

			}
		} catch (Exception e) {
			e.printStackTrace();
			Reporter.ReportEvent("Navigation to Teller Cash Deposit Page",
					"Navigated to Teller Cash Deposit Page successfully",
					"Navigation to Teller Cash Deposit Page failed", true);

		}

	}

	private static void Charge_limit_breach_check(HashMap<String, ArrayList<String>> limits,
			HashMap<String, String> CreditRisk, HashMap<String, Map<String, String>> teller_matrix, String action,
			String Account, int range, String condition, int midrate) throws Exception {
		// TODO Auto-generated method stub

		int amount = (((int) Double.parseDouble(limits.get(condition).get(0).replace(",", ""))) + 1) / midrate;
		int denomination_count = (amount / range);
		// addearmark_amount(amount);
		tellerCharge(amount);
		if (!action.equals("Decline")) {
			ROATModules.verify_mandate_auth_panel(Account, CreditRisk, teller_matrix, limits, "Charges", action);
		} else {
			WebDr.SetPageObjects("end_end");
			try {
				WebDr.waitTillElementIsDisplayed("displayDecline");
				WebDr.Wait_Time(3000);
				WebDr.verifyText("X", "displayDecline", true, "Transaction Declined", "transaction declined", true);
				WebDr.click("X", "button_okay", "Transaction declined confirmation  okay button", true);
			} catch (Exception e) {
				e.printStackTrace();
			}
		}

	}

	public static void tellerCharge(int amount) {
		try {
			selectDropDown("Charge Type", WebDr.getValue("ChargeType"));
			waitForPageLoaded();

			WebDr.click("X", "ChargeType", "Click on Charge Type", true);
			waitForPageLoaded();
			
			if(WebDr.isExists_Display("YesButton")>0){
				WebDr.click("X", "YesButton", "Click on Yes Button", true);
			}
			waitForPageLoaded();
			js = (JavascriptExecutor) Driver.driver;
			WebElement existingChargeAmount = WebDr.getElement("ChargeAmount");
			String existingChargeStr = js.executeScript("return arguments[0].value;", existingChargeAmount).toString();
			System.out.println("Existing Charge Amount: "+existingChargeStr);
			
			Reporter.ReportEvent("Account Information Verifcation", "Verify Account information",
					"Account information verified", true);
			waitForPageLoaded();
			
			WebDr.setText("X", "ChargeAmount", Integer.toString(amount), "Set Charge Amount", true);
			waitForPageLoaded();

			WebDr.click("X", "NextButtonCM", "Click on Next Button", true);
			waitForPageLoaded();
			if(WebDr.isExists_Display("YesInsufficient")>0){
				WebDr.click("X", "YesInsufficient", "Click on Yes Button", true);
			}			
			
			Reporter.ReportEvent("Enter Charges Details", "Enter required Charges details", "Chagers value entered",
					true);
			waitForPageLoaded();

			String accountInfo = WebDr.getElement("AccountInformation").getText();
			System.out.println("Account Information: "+accountInfo);
			Reporter.ReportEvent("Account Information Verifcation", "Verify Account information",
					"Account information verified", true);
			waitForPageLoaded();

			String balanceInfo = WebDr.getElement("BalanceInformation").getText();
			System.out.println("Balance Information: "+balanceInfo);
			Reporter.ReportEvent("Balance Information Verifcation", "Verify Balance information",
					"Balance information verified", true);
			waitForPageLoaded();

			String notes = WebDr.getElement("Notes").getText();
			System.out.println("Notes Verifcation: "+notes);
			Reporter.ReportEvent("Notes Verifcation", "Verify Notes Existence", "Notes Existence verified", true);
			waitForPageLoaded();

			String mandates = WebDr.getElement("Mandates").getText();
			System.out.println("Mandates Verifcation: "+mandates);
			Reporter.ReportEvent("Mandates Verifcation", "Verify Mandates Existence", "Mandates Existence verified",
					true);
			waitForPageLoaded();

			Reporter.ReportEvent("Verify Menu Bar", "Verify Menu Bar present on screen ", "Menu Bar verified", true);

			String levelValue = WebDr.getElement("Level").getText();
			System.out.println("Level: "+levelValue);
			Reporter.ReportEvent("Level Verifcation", "Verify Level Value", "Level value verified", true);
			waitForPageLoaded();

			String generalAuthConditions = WebDr.getElement("GeneralAuthConditions").getText();
			System.out.println("General Authorization Conditions: "+generalAuthConditions);
			Reporter.ReportEvent("General Authorization Conditions Verifcation",
					"Verify General Authorization Conditions", "General Authorization Conditions verified", true);
			waitForPageLoaded();

			String accountAuthConditions = WebDr.getElement("AccountAuthConditions").getText();
			System.out.println("Account Authorization Conditions Verifcation: "+accountAuthConditions);
			Reporter.ReportEvent("Account Authorization Conditions Verifcation",
					"Verify Account Authorization Conditions", "Account Authorization Conditions verified", true);
			waitForPageLoaded();

			/*
			 * authorization1();
			 * 
			 * WebDr.click("X", "AcceptButton", "Click on Accept Button", true);
			 * waitForPageLoaded();
			 * 
			 * WebDr.click("X", "PostButton", "Click on Post Button", true);
			 * waitForPageLoaded();
			 * 
			 * WebDr.click("X", "DoneButton", "Click on Done Button", true);
			 * waitForPageLoaded();
			 */
		} catch (Exception e) {
			Reporter.ReportEvent("Navigate to Teller Withdrawal page ", "unable to navigate to Vault FCY Brakdown page",
					"unable to navigate to Vault FCY Brakdown page", false);
		}
	}

	public static void rTab() throws AWTException {
		robotObj = new Robot();
		robotObj.keyPress(KeyEvent.VK_TAB);
		robotObj.keyRelease(KeyEvent.VK_TAB);
		Reporter.ReportEvent("Robot Tab Action.", "Tab key press", "Key Dress Done", true);
	}

	public static void rDown() throws AWTException {
		robotObj = new Robot();
		robotObj.keyPress(KeyEvent.VK_DOWN);
		robotObj.keyRelease(KeyEvent.VK_DOWN);
	}

	public static void rEnter() throws AWTException {
		robotObj = new Robot();
		robotObj.keyPress(KeyEvent.VK_ENTER);
		robotObj.keyRelease(KeyEvent.VK_ENTER);
	}

	public static void rEscape() throws Exception {
		robotObj = new Robot();
		robotObj.keyPress(KeyEvent.VK_ESCAPE);
		robotObj.keyRelease(KeyEvent.VK_ESCAPE);
	}

	public static void selectDropDown(String elem, String option) throws Exception {
		String DD = "XPATH|//*[contains(text(),'" + elem + "')]/following::label[1]";
		WebDr.waitTillElementIsDisplayed(DD);
		waitForElementToBeAvailable("CUSTOM", DD, 8);
		WebDr.click("CUSTOM", DD, "Clicking on " + elem + " DD", true);
		String DDoption = "XPATH|//*[contains(text(),'" + elem + "')]/following::input[2]";
		waitForElementToBeAvailable("CUSTOM", DDoption, 8);
		WebDr.setText("CUSTOM", DDoption, option, "Entering option to select", true);
		rDown();
		rTab();
		waitForPageLoaded();
	}

	public static boolean waitForElementToBeAvailable(String custom, String elementName, int timeout) throws Exception {
		boolean exist = false;
		WebElement elmn = WebDr.getElementObject(elementName);
		WebDriverWait wait = new WebDriverWait(Driver.driver, timeout);
		WebElement element = wait.until(ExpectedConditions.visibilityOf(elmn));
		element.getSize();
		System.out.println(element.getSize());
		exist = true;
		return exist;
	}

	public static void waitForPageLoaded() throws Exception {
		ExpectedCondition<Boolean> expectation = new ExpectedCondition<Boolean>() {
			public Boolean apply(WebDriver driver) {
				return ((JavascriptExecutor) driver).executeScript("return document.readyState").toString()
						.equals("complete");
			}
		};
		try {
			Thread.sleep(1000);
			WebDriverWait wait = new WebDriverWait(Driver.driver, 30);
			wait.until(expectation);
		} catch (Throwable error) {

		}
	}

	public static void authorization() throws TimeoutException, Exception {
		try {
			if (WebDr.displayed("X", "AuthUsername", true, "Check for Authorization Panel", true)) {
				WebDr.setText("X", "AuthUsername", WebDr.getValue("Authorizer"), "Set Auth username", true);
				Thread.sleep(8000);
				rTab();
				WebDr.click("X", "AuthPassword", "Click on Password text field", true);
				Thread.sleep(3000);
				WebDr.click("X", "AuthPassword", "Click on Password text field", true);
				WebDr.setText("X", "AuthPassword", WebDr.getValue("AuthPassword"), "Set Auth password", true);
				WebDr.click("X", "AuthorizeButton", "Click on Authorization Button", true);
			}
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}

	public static void authorization1() throws TimeoutException, Exception {
		try {
			if (WebDr.displayed("X", "AuthUsername1", true, "Check for Authorization Panel", true)) {
				WebDr.setText("X", "AuthUsername1", WebDr.getValue("Authorizer"), "Set Auth username", true);
				Thread.sleep(8000);
				rTab();
				WebDr.click("X", "AuthPassword1", "Click on Password text field", true);
				Thread.sleep(3000);
				WebDr.click("X", "AuthPassword1", "Click on Password text field", true);
				WebDr.setText("X", "AuthPassword1", WebDr.getValue("AuthPassword"), "Set Auth password", true);
				WebDr.click("X", "AuthorizeButton", "Click on Authorization Button", true);
			}
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}

	public static void logintoROATeller(String username, String userRole) {
		// TODO Auto-generated method stub
		try {
			waitForPageLoaded();
			WebDr.SetPageObjects("MasterPage");
			// if (HtmlReporter.tc_result == true)
			WebDr.click("X", "username", "Clicking on username text", true);

			// if (HtmlReporter.tc_result == true)
			WebDr.setText("X", "username", username, "Entering data in Username field", true);

			// if (HtmlReporter.tc_result == true)
			WebDr.click("X", "Login", "Clicking on login button", true);

			waitForPageLoaded();

			waitForElementToBeAvailable("X", "Password", 30);
			WebDr.click("X", "Password", "Clicking on Password field", true);

			WebDr.setText("X", "Password", "123", "Entering data in password field", true);

			WebDr.click("X", "Login", "Clicking on login button", true);

			if (WebDr.isExists("dropdown_role") == 0) {
				WebDr.clickX("X", "Login", "Clicking on login button", true);
				waitForPageLoaded();
				rEnter();
			}

			if (WebDr.isExists("dropdown_role") > 0) {
				if (HtmlReporter.tc_result == true)
					WebDr.click("X", "dropdown_role", "Clicking on drop down", true);

				if (HtmlReporter.tc_result == true)
					WebDr.select("X", "dropdown_role", "VisibleText", userRole, "Select role" + userRole, true);

				if (HtmlReporter.tc_result == true)
					WebDr.clickX("X", "Login", "Clicking on login button", true);
			}

			waitForPageLoaded();

			if (WebDr.isExists("Login") > 0) {
				rEnter();
				WebDr.clickX("X", "Login", "Clicking on login button", true);
				waitForPageLoaded();
			}
			rEnter();
			if (WebDr.isExists("WarningPopup") > 0) {
				rEnter();
				WebDr.clickX("X", "OkButton", "Click on warning Ok button.", true);
			}

			waitForPageLoaded();
			if (HtmlReporter.tc_result == true)
				try {
					waitForElementToBeAvailable("X", "DashBoard", 30);

				} catch (Exception e) {
					e.printStackTrace();
					if (WebDr.isExists("OkButton") > 0) {

						rEnter();
						WebDr.clickX("X", "OkButton", "Click on warning Ok button.", true);
						waitForPageLoaded();
					}
				}

		} catch (Exception e) {
			Reporter.ReportEvent("login error", "login button should click", "login button object not found", false);
			Driver.driver.quit();
		}
	}

	public static void SetUp_TariffMaintenance(String operation) throws Exception {
		// login with SuperUser
		ROATFunctions.refesh(false, "Super User");

		// navigate
		ROATModules.setup_E2ENavigateTariffMaintnance();

		// update or add Tariff Maintenance operation
		if (operation.equalsIgnoreCase("Update")) {
			if (HtmlReporter.tc_result)
				ROATModules.setup_E2EUpdateTariffMaintenance();
		} else {
			if (HtmlReporter.tc_result)
				ROATModules.setup_E2EAddTariffMaintenance();
		}

	}

	public static void onScreenAuthorization1() throws Exception {

		boolean acceptbutton = waitTillElementIsDisplayed("AcceptButtonPopup", 10);
		if (acceptbutton) {
			Reporter.ReportEvent("Accept Button visibility ", "Accept button should appear after set duration",
					"Accept button present", true);
			WebDr.clickX("X", "AcceptButtonPopup", "Click on Accept button on popup", true);
		} else {
			// Reporter.ReportEvent("Accept Button visibility ", "Accept button
			// should appear after set duration", "Accept button present",
			// false);
			System.out.println("------Accept button not visible------");
		}

		// WebDr.waitForElementToBeAvailable("AcceptButtonPopup", 60);

		waitForPageLoaded();
		authorization();
	}

	public static WebElement getElementObject(String elementProperty) {
		// System.out.println("WebDr.java- getElement Invoked");
		try {
			String[] a = elementProperty.split("\\|");
			switch (a[0]) {
			case "ID":
				return Driver.driver.findElement(By.id(a[1]));
			case "CLASSNAME":
				return Driver.driver.findElement(By.className(a[1]));
			case "LINKTEXT":
				return Driver.driver.findElement(By.linkText(a[1]));
			case "NAME":
				return Driver.driver.findElement(By.name(a[1]));
			case "XPATH":
				return Driver.driver.findElement(By.xpath(a[1]));
			default:
				System.out.println("Function getElement cannot return object for " + elementProperty);
				break;
			}
		} catch (Exception e) {
			System.out.println("Exeption in WebDr.getElement - " + e);
			return null;
		}
		return null;
	}

	public static boolean waitTillElementIsDisplayed(String Object_name) {
		boolean bflag = false;
		// WebElement elm = WebDr.getElement(Object_name);
		for (int k = 1; k <= 30; k++) {
			try {
				Thread.sleep(1000);
				// System.out.println(k);
				if (WebDr.isExists(Object_name) != 0) {
					bflag = true;
					break;
				}
			} catch (Exception e) {
				bflag = false;
			}
		}
		if (bflag) {
			Reporter.ReportEvent("Object is Displayed", "Object should be Display", "Object Display on screen", true);
		} else {
			// Reporter.ReportEvent("Object is not Displayed", "Object is not
			// getting Displyed", "Error!", false);
		}
		return bflag;
	}

	public static boolean waitTillElementIsDisplayed(String Object_name, int time) {
		boolean bflag = false;
		// WebElement elm = WebDr.getElement(Object_name);
		for (int k = 1; k <= 30; k++) {
			try {
				Thread.sleep(time);
				// System.out.println(k);
				if (WebDr.isExists(Object_name) != 0) {
					bflag = true;
					break;
				}
			} catch (Exception e) {
				bflag = false;
			}
		}
		if (bflag) {
			Reporter.ReportEvent("Object is Displayed", "Object should be Display", "Object Display on screen", true);
		} else {
			// Reporter.ReportEvent("Object is not Displayed", "Object is not
			// getting Displyed", "Error!", false);
		}
		return bflag;
	}

	public static int isExistsCustom(String elementName) {
		// TODO Auto-generated method stub
		int val = 0;

		try {
			Driver.driver.manage().timeouts().implicitlyWait(2, TimeUnit.SECONDS);
			val = WebDr.getElements_custome(elementName).size();

			System.out.println(val);
			Driver.driver.manage().timeouts().implicitlyWait(5, TimeUnit.SECONDS);
			return val;
		} catch (Exception e) {
			// Reporter.ReportEvent("Object not Found", "Object should be
			// present", "object is not present" , false );
			System.out.println(e);

			return val;
		}
	}

	public static Integer isExists(String elementName) {
		int val = 0;
		try {
			val = WebDr.getElements(elementName).size();
			System.out.println("Existence of Element: " + val);
			return val;
		} catch (Exception e) {
			System.out.println(e);
			return val;
		}
	}

	/**
	 * @param Charge:
	 *            Charge Value
	 * @throws Exception
	 */
	public static void setUpChargePackage(String Charge) throws Exception {
		// TODO Auto-generated method stub
		ROATModules.navigateToChargePackage();
		/*
		 * ROATModules.addActivity(Charge); ROATModules.setUpTariffMaint() ;
		 */
	}

	/**
	 * @param branchNo:
	 *            Branch of Inquired Account
	 * @param accountNo:
	 *            Inquired Account Number
	 * @return: Available Balance of Inquired Account
	 * @throws Exception
	 */
	public static BigDecimal accountInquiry(String branchNo, String accountNo) throws Exception {
		WebDr.SetPageObjects("E2E_ChargesModule");
		waitForPageLoaded();

		WebDr.waitTillElementIsDisplayed("InquiryTab");
		WebDr.click("X", "InquiryTab", "Clicking on Inquiry Tab", true);
		waitForPageLoaded();

		WebDr.click("X", "AccountInquiryTab", "Clicking on Account Inquiry Tab", true);
		Reporter.ReportEvent("Navigate to Account Inquiry page ", "Successfully navigated to Account Inquiry page ",
				" Successfully navigated to Account Inquiry page ", true);
		waitForPageLoaded();

		WebDr.click("X", "BranchCodeAI", "Click to select Branch Code", true);

		WebDr.setText("X", "EnterBranchCodeAI", branchNo, "Enter Branch Code", true);
		WebDr.click("X", "SelectBranchCode", "Click on entered Branch Code", true);

		Reporter.ReportEvent("Select Branch Code: ", "Select Branch Code",
				"Branch Code Selected: " + WebDr.getValue("Branch_Code"), true);
		waitForPageLoaded();

		WebDr.setText("X", "AccountNumberInquiry", accountNo, "Set Account Number", true);
		rTab();
		waitForPageLoaded();

		WebDr.click("X", "SearchButton", "Click on entered Branch Code", true);
		waitForPageLoaded();
		WebDr.Wait_Time(2000);
		if (WebDr.isExists_Display("ActInqOK") > 0) {
			WebDr.click("X", "ActInqOK", "Click on Account Inq OK", true);
		}

		// String availableBalance =
		// WebDr.getElement("AvailableBalance").getText().replace(",", "");
		WebElement avBal = WebDr.getElement("AvailableBalance");
		waitForPageLoaded();
		String avBalStr = avBal.getText().toString().replace(",", "");
		System.out.println("balance : " + avBalStr);
		BigDecimal availableBalance = new BigDecimal(avBalStr);
		System.out.println("Available balance: " + availableBalance);
		Reporter.ReportEvent("Available Balance: ", "Verify Available Balance",
				"Available Balance: " + availableBalance, true);
		waitForPageLoaded();

		return availableBalance;
	}

	/**
	 * @param currency:
	 *            LOCAL, FOREIGN
	 * @param TrxType:
	 *            STANDARD, MASSMARK, ACCTTYPE
	 * @throws Exception
	 * 
	 */
	public static void cashWithdrawal(String currency, String TrxType) throws Exception {

		WebDr.SetPageObjects("E2E_ChargesModule");
		waitForPageLoaded();

		WebDr.waitTillElementIsDisplayed("Teller_Tab");
		WebDr.click("X", "Teller_Tab", "Clicking on Teller Tab", true);
		waitForPageLoaded();

		WebDr.click("X", "WithdrawalTab", "Clicking on Withdrawal Tab", true);
		Reporter.ReportEvent("Navigate to Teller Withdrawal page ", "Successfully navigated to Teller Withdrawal page ",
				" Successfully navigated to Teller Withdrawal page ", true);
		waitForPageLoaded();

		WebDr.click("X", "CashWithDrawal", "Clicking on Cash WithDrawal Radio Button", true);
		waitForPageLoaded();
		Reporter.ReportEvent("Select Cash WithDrawal", "Select Cash WithDrawal", "Cash WithDrawal Selected", true);

		WebDr.click("X", "IDV", "Click on performed Manual ID&V", true);
		Reporter.ReportEvent("Select IDV", "Select Manual IDV", "Manual IDV Selected", true);
		waitForPageLoaded();

		WebDr.click("X", "BranchCodeCW", "Click to select Branch Code", true);
		WebDr.setText("X", "EnterBranchCodeCW",
				WebDr.getValue("ETE_" + currency + "_" + TrxType + "_Account_no").split("\\-")[0].split("\\|")[1],
				"Enter Branch Code", true);
		WebDr.click("X", "SelectBranchCode", "Click on entered Branch Code", true);

		Reporter.ReportEvent("Select Branch Code: ", "Select Branch Code", "Branch Code Selected: "
				+ WebDr.getValue("ETE_" + currency + "_" + TrxType + "_Account_no").split("\\-")[0].split("\\|")[1],
				true);
		waitForPageLoaded();
		// "ETE_"+currency+"_"+TrxType+"_Account_no"
		// "ETE_LOCAL_STANDARD_Account_no"
		WebDr.setText("X", "AccountNumberCW",
				WebDr.getValue("ETE_" + currency + "_" + TrxType + "_Account_no").split("\\-")[1], "Set Account Number",
				true);
		rTab();
		waitForPageLoaded();

		if (WebDr.isExists_Display("AccountNotes") > 0) {
			WebDr.click("X", "OKButtonCW", "Click on Account Notes OK", true);
		}

		WebDr.setText("X", "Amount", WebDr.getValue("Amount_CW_" + currency), "Enter Amount to Withdrawal", true);
		waitForPageLoaded();

		WebDr.setText("X", "Narrative", "CW_Narrative", "Enter Narrative", true);
		waitForPageLoaded();

		WebDr.setText("X", "WithdrawalName", "CW_Name", "Enter Withdrawal Name", true);
		waitForPageLoaded();

		/*
		 * commission = new
		 * BigDecimal(WebDr.getElement("Commission").getText());
		 */

		js = (JavascriptExecutor) Driver.driver;
		WebElement commCW = WebDr.getElement("Commission");
		String commCWStr = js.executeScript("return arguments[0].value;", commCW).toString();
		System.out.println(commCWStr);

		commission = new BigDecimal(commCWStr);

		System.out.println("Commission: " + commission);
		Reporter.ReportEvent("Commision: ", "Verify Commision", "Commision: " + commission, true);
		waitForPageLoaded();

		WebDr.click("X", "NextButtonCW", "Click on Next Button", true);
		waitForPageLoaded();
		Thread.sleep(8000);
		if ((WebDr.isExists_Display("IgnoreButton")) > 0) {
			waitForPageLoaded();
			WebDr.click("X", "IgnoreButton", "Click on Next Button", true);
		}

		WebDr.click("X", "AcceptButton", "Click on Accept Button", true);
		waitForPageLoaded();
		Thread.sleep(5000);
		authorization1();

		WebDr.click("X", "PostButtonCW", "Click on Post Button", true);
		waitForPageLoaded();

		if (WebDr.displayed("X", "SuccessMsg", true, "Verify success message", true)) {
			Reporter.ReportEvent("Validate E2E Process: Cash Withdrawal",
					"Success message on complete E2E process of Cash Withdrawal ", "As expected", true);
		} else {
			Reporter.ReportEvent("Validate E2E Process: Cash Withdrawal",
					"Success message on complete E2E process of Cash Withdrawal", "Not as expected", false);
		}

		WebDr.click("X", "DoneButtonCW", "Click on Done Button", true);
		waitForPageLoaded();
	}

	/**
	 * @param currency:
	 *            Country Currency- usd, eur etc
	 * @return: Mid Rate of given Currency
	 * @throws Exception
	 */
	public static BigDecimal midRateFinder(String currency) throws Exception {
		WebDr.SetPageObjects("E2E_ChargesModule");
		waitForPageLoaded();
		WebDr.click("X", "Forex_Tab", "Click on Forex Tab", true);
		waitForPageLoaded();
		WebDr.click("X", "Rate", "Click on Rate Tab", true);
		waitForPageLoaded();
		WebDr.click("X", "ForexInquiry", "Click on Forex Inquiry sub Tab", true);
		waitForPageLoaded();
		selectDropDown("Currency :", currency);
		waitForPageLoaded();

		js = (JavascriptExecutor) Driver.driver;
		WebElement midRate = WebDr.getElement("MidRate");
		String midRateText = js.executeScript("return arguments[0].value;", midRate).toString();
		System.out.println(midRateText);

		Reporter.ReportEvent("Mid Rate: ", "Mid rate of Currency: " + currency, "Correct Mid rate: " + midRateText,
				true);

		return new BigDecimal(midRateText);
	}

	/**
	 * @param midRate:
	 *            Mid Rate of Currency
	 * @param commission:
	 *            Commission fetch during Transaction
	 * @return Commission in Foreign Currency
	 * @throws Exception
	 */
	public static BigDecimal lcyToFCYCalculater(BigDecimal midRate, BigDecimal commission) throws Exception {
		WebDr.SetPageObjects("E2E_ChargesModule");
		waitForPageLoaded();

		// String midRate = midRateFinder(currency);
		BigDecimal midRateObj = midRate;
		BigDecimal commissionObj = commission;
		System.out.println("MidRate: " + midRateObj + " Commission: " + commissionObj);

		BigDecimal fcyAmount = commissionObj.divide(midRateObj, RoundingMode.CEILING);

		System.out.println("FCY amount calculated" + fcyAmount.toString());
		Reporter.ReportEvent("LCY To FCY Calculater: ", "Foreign Currency Amount: " + fcyAmount,
				"Foreign Currency Amount: " + fcyAmount, true);
		return fcyAmount;
	}

	/**
	 * @param currency:
	 *            LOCAL, FOREIGN
	 * @param TransactionAmt:
	 *            Amount of Transaction
	 * @param beforePL:
	 *            Balance of PL Account before transaction
	 * @param afterPL:
	 *            Balance of PL Account after transaction
	 * @param beforeCust:
	 *            Balance of Customer Account before transaction
	 * @param afterCust:
	 *            Balance of Customer Account after transaction
	 * @return: Result
	 * @throws Exception
	 */
	public static boolean verifyChargeApplication(String currency, BigDecimal TransactionAmt, BigDecimal beforePL,
			BigDecimal afterPL, BigDecimal beforeCust, BigDecimal afterCust) throws Exception {
		boolean result = false;

		System.out.println("PL Balance before Trx: " + beforePL);
		System.out.println("Cust Balance before Trx: " + beforeCust);
		System.out.println("PL Balance after Trx: " + afterPL);
		System.out.println("Cust Balance after Trx: " + afterCust);
		// check the commission calculated is perfect a per the SetUp done

		BigDecimal expectedComission = new BigDecimal(WebDr.getValue("FlatFee")).add(
				TransactionAmt.multiply((new BigDecimal(WebDr.getValue("Percentage")).divide(new BigDecimal(100)))));
		System.out.println("Expected commission calculated: " + expectedComission);
		System.out.println("Actual commission charged: " + commission);

		if ((expectedComission.compareTo(new BigDecimal(WebDr.getValue("MaxFee"))) == 1)
				|| (expectedComission.compareTo(new BigDecimal(WebDr.getValue("MinFee"))) == -1)) {
			if ((commission.compareTo(new BigDecimal(WebDr.getValue("MaxFee"))) == 0)
					|| (commission.compareTo(new BigDecimal(WebDr.getValue("MinFee"))) == 0)) {
				Reporter.ReportEvent("Charge commission calculation for the transaction verfication",
						"Charge calculated must be equal to comission mentioned by the system", "As expected", true);
			} else {
				Reporter.ReportEvent("Charge commission calculation for the transaction verfication",
						"Charge calculated must be equal to comission mentioned by the system", "Not as expected",
						false);
			}
		} else {

			if (expectedComission.compareTo(commission) == 0) {
				Reporter.ReportEvent("Charge commission calculation for the transaction verfication",
						"Charge calculated must be equal to comission mentioned by the system " + expectedComission,
						"As expected " + commission, true);
			} else {
				Reporter.ReportEvent("Charge commission calculation for the transaction verfication",
						"Charge calculated must be equal to comission mentioned by the system " + expectedComission,
						"Not as expected " + commission, false);
			}
		}

		if (currency.equalsIgnoreCase("LOCAL")) {
			if (((afterPL.subtract(beforePL)).compareTo(commission) == 0)
					&& (beforeCust.subtract(afterCust)).compareTo(commission.add(TransactionAmt)) == 0) {
				result = true;
			}
		} else {
			BigDecimal FCYcommission = lcyToFCYCalculater(midRate, commission);
			System.out.println("Actual commission charged(in FCY): " + FCYcommission);
			if (((afterPL.subtract(beforePL)).compareTo(commission) == 0)
					&& (beforeCust.subtract(afterCust)).compareTo(FCYcommission.add(TransactionAmt)) == 0) {
				result = true;
			}
		}
		return result;
	}

	/**
	 * @param currency:
	 *            LOCAL, FOREIGN
	 * @param TrxType:
	 *            STANDARD, MASSMARKET, ACCTTYPE
	 * @throws Exception
	 */
	public static void internalTransfer(String currency, String TrxType) throws Exception {
		WebDr.SetPageObjects("E2E_ChargesModule");

		waitForPageLoaded();
		WebDr.click("X", "TAB_Teller", "Click on Teller Tab", true);
		WebDr.click("X", "TransferTab", "Click on Transfer Tab", true);

		waitForPageLoaded();
		if ((WebDr.isExists("IDVPopUp")) > 0) {
			waitForPageLoaded();
			Thread.sleep(3000);
			WebDr.click("X", "IDVRadioButton", "Select Radio Button for Manual ID&V", true);
			WebDr.click("X", "IDVOKButton", "Click on OK Button", true);
		}

		enterBranchCode("From",
				WebDr.getValue("ETE_" + currency + "_" + TrxType + "_From_Account_no").split("\\-")[0].split("\\|")[1]);

		WebDr.setText("X", "FromAccountNumber",
				WebDr.getValue("ETE_" + currency + "_" + TrxType + "_From_Account_no").split("\\-")[1],
				"From Account Number Entered", true);
		WebDr.setText("X", "TransferAmount", WebDr.getValue("Amount_IT_" + currency), "Transfer Amount Entered", true);
		rTab();
		if (WebDr.isExists("CommissionPopup") > 0) {
			WebDr.click("X", "OKButton", "", true);
		}
		rEscape();
		WebDr.setText("X", "FromNarrative", "FromNarrative", "From Narrative Entered", true);
		waitForPageLoaded();
		enterBranchCode("To",
				WebDr.getValue("ETE_" + currency + "_" + TrxType + "_To_Account_no").split("\\-")[0].split("\\|")[1]);
		WebDr.setText("X", "ToAccountNumber",
				WebDr.getValue("ETE_" + currency + "_" + TrxType + "_To_Account_no").split("\\-")[1],
				"To Account Number Entered", true);
		rTab();
		if ((isExists("AccountNote")) > 0) {
			waitForPageLoaded();
			WebDr.click("X", "AccountOkButton", "Click on OK Button", true);
		}

		WebDr.setText("X", "ToNarrative", "ToNarrative", "To Narrative Entered", true);
		waitForPageLoaded();
		// String commIT = WebDr.getElement("CommissionIT").getText();
		js = (JavascriptExecutor) Driver.driver;
		WebElement commIT = WebDr.getElement("CommissionIT");
		String commITStr = js.executeScript("return arguments[0].value;", commIT).toString();
		System.out.println(commITStr);

		commission = new BigDecimal(commITStr);
		System.out.println(commission);
		Reporter.ReportEvent("Commision: ", "Verify Commision", "Commision: " + commission, true);
		waitForPageLoaded();

		WebDr.click("X", "NextButton", "Clicked Next Button", true);

		waitForPageLoaded();

		if ((isExists("IgnoreButton")) > 0) {
			waitForPageLoaded();
			WebDr.click("X", "IgnoreButton", "Click on Next Button", true);
		}

		WebDr.click("X", "AcceptButton", "Click on Accept Button", true);
		waitForPageLoaded();

		authorization1();

		WebDr.click("X", "PostButton", "Click on Post Button", true);
		waitForPageLoaded();
		Thread.sleep(5000);
		ROATProcesses.waitTillElementIsDisplayed("SuccessMessage", 10);
		// successText = WebDr.getText("X", "SuccessMessage", "Verifying Text
		// Message");
		// String successText = WebDr.getElement("SuccessMessage").getText();

		/*
		 * if (WebDr.displayed("X", "sucs_msg", true,
		 * "Check for Success message", true)) {
		 * Reporter.ReportEvent("Verify Success Message ",
		 * "Transaction Posted and Receipt sent for Printing", successText,
		 * true); } else if
		 * (successText.contains("Transaction completed successfully")) {
		 * Reporter.ReportEvent("Verify Success Message ",
		 * "Transaction completed successfully", successText, true); } else {
		 * Reporter.ReportEvent("Verify Success Message ",
		 * "This Transaction Message Unexpected", successText, false); }
		 */

		if (WebDr.displayed("X", "sucs_msg", true, "Check for Success message", true)) {
			Reporter.ReportEvent("Verify Success Message ", "Transaction Posted and Receipt sent for Printing",
					"successText", true);
		} else {
			Reporter.ReportEvent("Verify Success Message ", "Transaction Posted and Receipt sent for Printing",
					"Not As Expected", true);
		}

		WebDr.click("X", "DoneButton", "Click on Done Button", true);
		waitForPageLoaded();

	}

	/**
	 * @param fromTo:
	 *            From Account Number and To Account Number
	 * @param branchCode:
	 *            Branch Code for From Account Number and To Account Number
	 * @throws Exception
	 */
	public static void enterBranchCode(String fromTo, String branchCode) throws Exception {
		waitForPageLoaded();
		if (fromTo.equalsIgnoreCase("From")) {
			WebDr.click("X", "FromBranchCode", "Clicked dropdown Button", true);
			WebDr.setText("X", "EnterFromBranchCode", branchCode, "Branch Code Selected", true);
		} else {
			WebDr.click("X", "ToBranchCode", "Clicked dropdown Button", true);
			WebDr.setText("X", "EnterToBranchCode", branchCode, "Branch Code Selected", true);
		}
		waitForPageLoaded();
		WebDr.click("X", "SelectBranchCode", "Clicked dropdown Button", true);
	}

	/**
	 * @param currency:
	 *            LOCAL, FOREIGN
	 * @param TrxType:
	 *            STANDARD, MASSMARKET, ACCTTYPE
	 * @param Trx:
	 *            CW, IT
	 * @throws Exception
	 */
	public static void chargeModule(String currency, String TrxType, String Trx) throws Exception {
		// ROATModules.performChargeTariffSetUp();
		ROATFunctions.refesh(false, "Teller");
		ROATProcesses.midRate = ROATProcesses.midRateFinder(
				WebDr.getValue("ETE_" + currency + "_" + TrxType + "_Account_no").split("\\-")[0].split("\\|")[0]);
		ROATProcesses.beforeTransactionPL = ROATProcesses.accountInquiry(
				WebDr.getValue("ETE_LOCAL_PL_Account_no").split("\\-")[0].split("\\|")[1],
				WebDr.getValue("ETE_LOCAL_PL_Account_no").split("\\-")[1]);
		ROATProcesses.beforeTransaction = ROATProcesses.accountInquiry(
				WebDr.getValue("ETE_" + currency + "_" + TrxType + "_Account_no").split("\\-")[0].split("\\|")[1],
				WebDr.getValue("ETE_" + currency + "_" + TrxType + "_Account_no").split("\\-")[1]);
		if (Trx.equalsIgnoreCase("CW")) {
			ROATProcesses.cashWithdrawal(currency, TrxType);
		} else {
			ROATProcesses.internalTransfer(currency, TrxType);
		}
		ROATProcesses.afterTransactionPL = ROATProcesses.accountInquiry(
				WebDr.getValue("ETE_LOCAL_PL_Account_no").split("\\-")[0].split("\\|")[1],
				WebDr.getValue("ETE_LOCAL_PL_Account_no").split("\\-")[1]);
		ROATProcesses.afterTransaction = ROATProcesses.accountInquiry(
				WebDr.getValue("ETE_" + currency + "_" + TrxType + "_Account_no").split("\\-")[0].split("\\|")[1],
				WebDr.getValue("ETE_" + currency + "_" + TrxType + "_Account_no").split("\\-")[1]);
		// ROATProcesses.lcyToFCYCalculater(ROATProcesses.midRate,
		// ROATProcesses.commission);
		if (verifyChargeApplication(currency, new BigDecimal(WebDr.getValue("Amount_" + Trx + "_" + currency)),
				ROATProcesses.beforeTransactionPL, ROATProcesses.afterTransactionPL, ROATProcesses.beforeTransaction,
				ROATProcesses.afterTransaction)) {
			System.out.println("Pass");
			Reporter.ReportEvent(
					"E2E Charge verification successful for " + currency + " " + Trx + " " + TrxType + " transaction",
					"Charge must be applied as per the Set up", "As expected", true);
		} else {
			System.out.println("Fail");
			Reporter.ReportEvent(
					"E2E Charge verification successful for " + currency + " " + Trx + " " + TrxType + " transaction",
					"Charge must be applied as per the Set up", "As expected", false);
		}
		// ROATProcesses.SetUp_TariffMaintenance("Add");
		// ROATProcesses.tellerCharge();
	}
}
