
package roatResuableModule;

import java.awt.Robot;
import java.awt.event.KeyEvent;
import java.text.DateFormat;
//import java.text.NumberFormat;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.HashMap;
import java.util.Map;
import java.util.List;
//import java.util.Locale;
//import org.apache.commons.collections.set.SynchronizedSortedSet;
//import org.apache.poi.util.SystemOutLogger;
//import org.junit.runner.Runner;
import org.openqa.selenium.By;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.Keys;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.interactions.Actions;
import testCases.Driver;
import utility.CommonUtils;
import utility.DatabaseFactory;
//import utility.Constant;
//import utility.ExcelUtils;
import utility.HtmlReporter;
import utility.Launcher;
import utility.Reporter;
import utility.WebDr;


 
public class ROATFunctions 
{        
	//*************************************************************************************
	//*************************************************************************************
	public static Map<String, String> roatEnv = new HashMap<String, String>();

	
	public static void logintoROATeller(String userrole) 
	{
		boolean lflag = false;
		try
		{
			try
			{
				DatabaseFactory.update_User_Session(CommonUtils.readConfig("Country_code"),"0",Driver.Env_IP);
				//DatabaseFactory.update_User_Session(Driver.Country_code,"0",Driver.Env_IP);//rhea added comment
				Driver.driver.manage().window().maximize();
			}
			catch(Exception e)
			{
				e.printStackTrace();
				Reporter.ReportEvent("session query failed", "session query failed","session query failed" , true);
				HtmlReporter.tc_result=true;
			}
			Driver.DB_Flag = "N";

			WebDr.SetPageObjects("MasterPage");
			//  if (HtmlReporter.tc_result==true) WebDr.click("X", "username", "Clicking on username text", true);

			String Login[] = Launcher.User_Login(userrole);
			int flag=0;
			for(int i=0;i<5;i++)
			{

				if(WebDr.getElement("headingLoginError").getText().contains("Application Error while service communication"))
				{
					Launcher.login_error=WebDr.getElement("headingLoginError").getText();
					Reporter.ReportEvent("login error", "Login screen should be available ", Launcher.login_error+"'", false);
					Launcher.login_result=false;
					Driver.DB_Flag = "Y";
					flag=1;
					break;
				}
				if(WebDr.isExists_Display("username")>0)
				{
					flag=1;
					break;
				}
				Thread.sleep(1000);
			}
			if(flag==0)
			{
				Reporter.ReportEvent("login error", "Login screen should be available ", "Login screen is not available ", false);
				Launcher.login_result=false;
			}
			if(Launcher.login_result)
			{
				if(HtmlReporter.tc_result) WebDr.setText("X", "username", Login[0], "Entering data in Username field", true);
				
				if(HtmlReporter.tc_result) WebDr.Wait_Time(3000);
				try
				{
					ROATFunctions.clickTab();

					if(WebDr.isExists_Display("headingLoginError")>0)
					{
						for(int i=0;i<3;i++)
						{
							if(WebDr.getElement("headingLoginError").getText().contains("User id:"))
							{
								Driver.driver.navigate().to((Driver.UrLp [Driver.urlcounter]+Driver.Country_code));
								if(HtmlReporter.tc_result) WebDr.setText("X", "username", Login[0], "Entering data in Username field", true);
								ROATFunctions.clickTab();
								//if(HtmlReporter.tc_result)  WebDr.click("X", "Login", "Clicking on login button", true);
								if(HtmlReporter.tc_result) WebDr.Wait_Time(3000);
							}
							else
							{
								break;
							}
						}

					}

				}catch (Exception e) {
					e.printStackTrace();
				}
				
				flag=0;
				for(int i=0;i<5;i++)
				{
					if(WebDr.isExists_Display("Password")>0)
					{ flag=1;
					break;
					}
					Thread.sleep(1000);

				}
				if(flag==0&&!WebDr.getElement("headingLoginError").getText().isEmpty()&&!WebDr.getElement("headingLoginError").getText().contains("ActiveX is not enable on browser.'"))
				{ Launcher.login_error=WebDr.getElement("headingLoginError").getText();
				Reporter.ReportEvent("login error", "Login screen should be available ", Launcher.login_error+"'", false);
				Launcher.login_result=false;
				Driver.DB_Flag = "Y";
				flag=1;
				}
				else if(flag==0)
				{
					Reporter.ReportEvent("login error", "Login screen should be available ", " DB connection failed application not loaded", false);
					Launcher.login_result=false;
					Driver.DB_Flag = "Y";
					flag=1;
				}
				if(Launcher.login_result)
				{
					
					
					if(HtmlReporter.tc_result)   WebDr.setText("X", "Password", Login[1], "Entering data in password field", true);
					ROATFunctions.clickTab();
					if(HtmlReporter.tc_result) WebDr.Wait_Time(3000);
					if(HtmlReporter.tc_result)  WebDr.click("X", "Login", "Clicking on login button", true);
					if(HtmlReporter.tc_result) WebDr.Wait_Time(3000);
					try
					{
						

						if(WebDr.isExists_Display("headingLoginError")>0)
						{
							for(int i=0;i<3;i++)
							{
								if(WebDr.getElement("headingLoginError").getText().contains("User id:"))
								{
									Driver.driver.navigate().to((Driver.UrLp [Driver.urlcounter]+Driver.Country_code));
									if(HtmlReporter.tc_result) WebDr.setText("X", "username", Login[0], "Entering data in Username field", true);
									ROATFunctions.clickTab();
									if(HtmlReporter.tc_result) WebDr.Wait_Time(3000);
									if(HtmlReporter.tc_result)WebDr.setText("X", "Password", Login[1], "Entering data in password field", true);
									if(HtmlReporter.tc_result) WebDr.Wait_Time(3000);
									if(HtmlReporter.tc_result)  WebDr.click("X", "Login", "Clicking on login button", true);
									//if(HtmlReporter.tc_result)  WebDr.click("X", "Login", "Clicking on login button", true);
									if(HtmlReporter.tc_result) WebDr.Wait_Time(3000);
								}
								else
								{
									break;
								}
							}

						}

					}catch (Exception e) {
						e.printStackTrace();
					}
					
					for(int i=0;i<5;i++)
					{

						if(WebDr.getElement("headingLoginError").getText().contains("LDAP Connection failed!")&&!WebDr.getElement("headingLoginError").getText().contains("ActiveX is not enable on browser.'"))
						{ 
							Launcher.login_error=WebDr.getElement("headingLoginError").getText();
							Reporter.ReportEvent("login error", "Login screen should be available ", Launcher.login_error+"'", false);
							Launcher.login_result=false;
							flag=1;
							Driver.DB_Flag = "Y";


							break;
						}
						else if(WebDr.isExists_Display("dropdown_role")>0)
						{
							break;
						}
						Thread.sleep(1000);
					}
					if(Launcher.login_result)
					{
						if(HtmlReporter.tc_result) WebDr.Wait_Time(3000);
						
						if(HtmlReporter.tc_result) WebDr.waitTillElementIsDisplayed("dropdown_role");

						if(HtmlReporter.tc_result)  WebDr.select("X", "dropdown_role", "VisibleText",userrole, "Select role", true);

						if(HtmlReporter.tc_result)  WebDr.click("X", "Login", "Clicking on login button", true);
						if(HtmlReporter.tc_result) WebDr.Wait_Time(3000);
						
						try
						{
							//ROATFunctions.clickTab();

							if(WebDr.isExists_Display("headingLoginError")>0)
							{
								for(int i=0;i<3;i++)
								{
									if(WebDr.getElement("headingLoginError").getText().contains("User id:"))
									{
										Driver.driver.navigate().to((Driver.UrLp [Driver.urlcounter]+Driver.Country_code));
										if(HtmlReporter.tc_result) WebDr.setText("X", "username", Login[0], "Entering data in Username field", true);
										ROATFunctions.clickTab();
										if(HtmlReporter.tc_result) WebDr.Wait_Time(3000);
										if(HtmlReporter.tc_result)WebDr.setText("X", "Password", Login[1], "Entering data in password field", true);
										if(HtmlReporter.tc_result) WebDr.Wait_Time(3000);
										if(HtmlReporter.tc_result)  WebDr.click("X", "Login", "Clicking on login button", true);
										//if(HtmlReporter.tc_result)  WebDr.click("X", "Login", "Clicking on login button", true);
										if(HtmlReporter.tc_result) WebDr.Wait_Time(3000);
										if(HtmlReporter.tc_result) WebDr.waitTillElementIsDisplayed("dropdown_role");

										if(HtmlReporter.tc_result)  WebDr.select("X", "dropdown_role", "VisibleText",userrole, "Select role", true);

										if(HtmlReporter.tc_result)  WebDr.click("X", "Login", "Clicking on login button", true);
										if(HtmlReporter.tc_result) WebDr.Wait_Time(3000);
									}
									else
									{
										break;
									}
								}

							}

						}catch (Exception e) {
							e.printStackTrace();
						}
						
						flag=0;
						for(int i=0;i<10;i++)
						{
							if(WebDr.isExists("labelMenu")>0)
							{ 
								flag=1;
								break;
							}
							Thread.sleep(1000);

						}
						if(flag==0&&!WebDr.getElement("headingLoginError").getText().isEmpty()&&!WebDr.getElement("headingLoginError").getText().contains("ActiveX is not enable on browser.'"))
						{
							Launcher.login_error=WebDr.getElement("headingLoginError").getText();
							Reporter.ReportEvent("login error", "Login screen should be available ", Launcher.login_error+"'", false);
							Launcher.login_result=false;
							Driver.DB_Flag = "Y";
							flag=1;
						}
						else if(flag==0)
						{
							Reporter.ReportEvent("login error", "Login screen should be available ", " DB connection failed application not loaded", false);
							Launcher.login_result=false;
							Driver.DB_Flag = "Y";
							flag=1;

						}
						else if(flag==1)
						{
							check_current_system_date(userrole);
						}
					}
				}
			}
		}

		catch(Exception e)

		{
			e.printStackTrace();

			Reporter.ReportEvent("login error", "login button should click", "Error!:- Page not Found 404", false);
			Launcher.login_error="Error!:- Page not Found 404";
			Launcher.login_result=false;

		}

	}
	public static void logintoROATeller_authoriser(String userrole) 
	{
		boolean lflag = false;
		try
		{
			try
			{
//				DatabaseFactory.update_User_Session(Driver.Country_code,"0",Driver.Env_IP);
				Driver.driver.manage().window().maximize();
			}
			catch(Exception e)
			{
				e.printStackTrace();
				Reporter.ReportEvent("session query failed", "session query failed","session query failed" , true);
				HtmlReporter.tc_result=true;
			}
			Driver.DB_Flag = "N";

			WebDr.SetPageObjects("MasterPage");
			//  if (HtmlReporter.tc_result==true) WebDr.click("X", "username", "Clicking on username text", true);

			String Login[] = Launcher.Auth_User_Login(userrole);
			int flag=0;
			for(int i=0;i<5;i++)
			{

				if(WebDr.getElement("headingLoginError").getText().contains("Application Error while service communication"))
				{
					Launcher.login_error=WebDr.getElement("headingLoginError").getText();
					Reporter.ReportEvent("login error", "Login screen should be available ", Launcher.login_error+"'", false);
					Launcher.login_result=false;
					Driver.DB_Flag = "Y";
					flag=1;
					break;
				}
				if(WebDr.isExists_Display("username")>0)
				{
					flag=1;
					break;
				}
				Thread.sleep(1000);
			}
			if(flag==0)
			{
				Reporter.ReportEvent("login error", "Login screen should be available ", "Login screen is not available ", false);
				Launcher.login_result=false;
			}
			if(Launcher.login_result)
			{
				if(HtmlReporter.tc_result) WebDr.setText("X", "username", Login[0], "Entering data in Username field", true);
				
				if(HtmlReporter.tc_result) WebDr.Wait_Time(3000);
				try
				{
					ROATFunctions.clickTab();

					if(WebDr.isExists_Display("headingLoginError")>0)
					{
						for(int i=0;i<3;i++)
						{
							if(WebDr.getElement("headingLoginError").getText().contains("User id:"))
							{
								Driver.driver.navigate().to((Driver.UrLp [Driver.urlcounter]+Driver.Country_code));
								if(HtmlReporter.tc_result) WebDr.setText("X", "username", Login[0], "Entering data in Username field", true);
								ROATFunctions.clickTab();
								//if(HtmlReporter.tc_result)  WebDr.click("X", "Login", "Clicking on login button", true);
								if(HtmlReporter.tc_result) WebDr.Wait_Time(3000);
							}
							else
							{
								break;
							}
						}

					}

				}catch (Exception e) {
					e.printStackTrace();
				}
				
				flag=0;
				for(int i=0;i<5;i++)
				{
					if(WebDr.isExists_Display("Password")>0)
					{ flag=1;
					break;
					}
					Thread.sleep(1000);

				}
				if(flag==0&&!WebDr.getElement("headingLoginError").getText().isEmpty()&&!WebDr.getElement("headingLoginError").getText().contains("ActiveX is not enable on browser.'"))
				{ Launcher.login_error=WebDr.getElement("headingLoginError").getText();
				Reporter.ReportEvent("login error", "Login screen should be available ", Launcher.login_error+"'", false);
				Launcher.login_result=false;
				Driver.DB_Flag = "Y";
				flag=1;
				}
				else if(flag==0)
				{
					Reporter.ReportEvent("login error", "Login screen should be available ", " DB connection failed application not loaded", false);
					Launcher.login_result=false;
					Driver.DB_Flag = "Y";
					flag=1;
				}
				if(Launcher.login_result)
				{
					
					
					if(HtmlReporter.tc_result)   WebDr.setText("X", "Password", Login[1], "Entering data in password field", true);
					ROATFunctions.clickTab();
					if(HtmlReporter.tc_result) WebDr.Wait_Time(3000);
					if(HtmlReporter.tc_result)  WebDr.click("X", "Login", "Clicking on login button", true);
					if(HtmlReporter.tc_result) WebDr.Wait_Time(3000);
					try
					{
						

						if(WebDr.isExists_Display("headingLoginError")>0)
						{
							for(int i=0;i<3;i++)
							{
								if(WebDr.getElement("headingLoginError").getText().contains("User id:"))
								{
									Driver.driver.navigate().to((Driver.UrLp [Driver.urlcounter]+Driver.Country_code));
									if(HtmlReporter.tc_result) WebDr.setText("X", "username", Login[0], "Entering data in Username field", true);
									ROATFunctions.clickTab();
									if(HtmlReporter.tc_result) WebDr.Wait_Time(3000);
									if(HtmlReporter.tc_result)WebDr.setText("X", "Password", Login[1], "Entering data in password field", true);
									if(HtmlReporter.tc_result) WebDr.Wait_Time(3000);
									if(HtmlReporter.tc_result)  WebDr.click("X", "Login", "Clicking on login button", true);
									//if(HtmlReporter.tc_result)  WebDr.click("X", "Login", "Clicking on login button", true);
									if(HtmlReporter.tc_result) WebDr.Wait_Time(3000);
								}
								else
								{
									break;
								}
							}

						}

					}catch (Exception e) {
						e.printStackTrace();
					}
					
					for(int i=0;i<5;i++)
					{

						if(WebDr.getElement("headingLoginError").getText().contains("LDAP Connection failed!")&&!WebDr.getElement("headingLoginError").getText().contains("ActiveX is not enable on browser.'"))
						{ 
							Launcher.login_error=WebDr.getElement("headingLoginError").getText();
							Reporter.ReportEvent("login error", "Login screen should be available ", Launcher.login_error+"'", false);
							Launcher.login_result=false;
							flag=1;
							Driver.DB_Flag = "Y";


							break;
						}
						else if(WebDr.isExists_Display("dropdown_role")>0)
						{
							break;
						}
						Thread.sleep(1000);
					}
					if(Launcher.login_result)
					{
						if(HtmlReporter.tc_result) WebDr.Wait_Time(3000);
						
						if(HtmlReporter.tc_result) WebDr.waitTillElementIsDisplayed("dropdown_role");

						if(HtmlReporter.tc_result)  WebDr.select("X", "dropdown_role", "VisibleText",userrole, "Select role", true);

						if(HtmlReporter.tc_result)  WebDr.click("X", "Login", "Clicking on login button", true);
						if(HtmlReporter.tc_result) WebDr.Wait_Time(3000);
						
						try
						{
							//ROATFunctions.clickTab();

							if(WebDr.isExists_Display("headingLoginError")>0)
							{
								for(int i=0;i<3;i++)
								{
									if(WebDr.getElement("headingLoginError").getText().contains("User id:"))
									{
										Driver.driver.navigate().to((Driver.UrLp [Driver.urlcounter]+Driver.Country_code));
										if(HtmlReporter.tc_result) WebDr.setText("X", "username", Login[0], "Entering data in Username field", true);
										ROATFunctions.clickTab();
										if(HtmlReporter.tc_result) WebDr.Wait_Time(3000);
										if(HtmlReporter.tc_result)WebDr.setText("X", "Password", Login[1], "Entering data in password field", true);
										if(HtmlReporter.tc_result) WebDr.Wait_Time(3000);
										if(HtmlReporter.tc_result)  WebDr.click("X", "Login", "Clicking on login button", true);
										//if(HtmlReporter.tc_result)  WebDr.click("X", "Login", "Clicking on login button", true);
										if(HtmlReporter.tc_result) WebDr.Wait_Time(3000);
										if(HtmlReporter.tc_result) WebDr.waitTillElementIsDisplayed("dropdown_role");

										if(HtmlReporter.tc_result)  WebDr.select("X", "dropdown_role", "VisibleText",userrole, "Select role", true);

										if(HtmlReporter.tc_result)  WebDr.click("X", "Login", "Clicking on login button", true);
										if(HtmlReporter.tc_result) WebDr.Wait_Time(3000);
									}
									else
									{
										break;
									}
								}

							}

						}catch (Exception e) {
							e.printStackTrace();
						}
						
						flag=0;
						for(int i=0;i<10;i++)
						{
							if(WebDr.isExists("labelMenu")>0)
							{ 
								flag=1;
								break;
							}
							Thread.sleep(1000);

						}
						if(flag==0&&!WebDr.getElement("headingLoginError").getText().isEmpty()&&!WebDr.getElement("headingLoginError").getText().contains("ActiveX is not enable on browser.'"))
						{
							Launcher.login_error=WebDr.getElement("headingLoginError").getText();
							Reporter.ReportEvent("login error", "Login screen should be available ", Launcher.login_error+"'", false);
							Launcher.login_result=false;
							Driver.DB_Flag = "Y";
							flag=1;
						}
						else if(flag==0)
						{
							Reporter.ReportEvent("login error", "Login screen should be available ", " DB connection failed application not loaded", false);
							Launcher.login_result=false;
							Driver.DB_Flag = "Y";
							flag=1;

						}
						else if(flag==1)
						{
							check_current_system_date(userrole);
						}
					}
				}
			}
		}

		catch(Exception e)

		{
			e.printStackTrace();

			Reporter.ReportEvent("login error", "login button should click", "Error!:- Page not Found 404", false);
			Launcher.login_error="Error!:- Page not Found 404";
			Launcher.login_result=false;

		}

	}
	public static void logintoROATeller_force(String userrole) 
	{
		try
		{
			try
			{
				//DatabaseFactory.update_User_Session(Driver.Country_code,"0",Driver.Env_IP);//rhea added comment
				DatabaseFactory.update_User_Session(CommonUtils.readConfig("Country_code"),"0",Driver.Env_IP);
			}
			catch(Exception e)
			{
				e.printStackTrace();
				Reporter.ReportEvent("session query failed", "session query failed","session query failed" , false);
				HtmlReporter.tc_result=true;
			}
			Driver.DB_Flag = "N";
			 
			WebDr.SetPageObjects("MasterPage");
			//  if (HtmlReporter.tc_result==true) WebDr.click("X", "username", "Clicking on username text", true);

			Driver.driver.manage().window().maximize();
			
			String Login[] = Launcher.User_Login(userrole);
			int flag=0;
			for(int i=0;i<5;i++)
			{

				if(WebDr.getElement("headingLoginError").getText().contains("Application Error while service communication"))
				{
					Launcher.login_error=WebDr.getElement("headingLoginError").getText();
					Reporter.ReportEvent("login error", "Login screen should be available ", Launcher.login_error+"'", false);
					Launcher.login_result=false;
					Driver.DB_Flag = "Y";
					flag=1;
					break;
				}
				if(WebDr.isExists_Display("username")>0)
				{
					flag=1;
					break;
				}
				Thread.sleep(1000);
			}
			if(flag==0)
			{
				Reporter.ReportEvent("login error", "Login screen should be available ", "Login screen is not available ", false);
				Launcher.login_result=false;
			}
			if(Launcher.login_result)
			{
				if(HtmlReporter.tc_result) WebDr.setText("X", "username", Login[0], "Entering data in Username field", true);
				
				if(HtmlReporter.tc_result) WebDr.Wait_Time(3000);
				try
				{
					ROATFunctions.clickTab();

					if(WebDr.isExists_Display("headingLoginError")>0)
					{
						for(int i=0;i<3;i++)
						{
							if(WebDr.getElement("headingLoginError").getText().contains("User id:"))
							{
								Driver.driver.navigate().to((Driver.UrLp [Driver.urlcounter]+Driver.Country_code));
								if(HtmlReporter.tc_result) WebDr.setText("X", "username", Login[0], "Entering data in Username field", true);
								ROATFunctions.clickTab();
								//if(HtmlReporter.tc_result)  WebDr.click("X", "Login", "Clicking on login button", true);
								if(HtmlReporter.tc_result) WebDr.Wait_Time(3000);
							}
							else
							{
								break;
							}
						}

					}

				}catch (Exception e) {
					e.printStackTrace();
				}

				flag=0;
				for(int i=0;i<5;i++)
				{
					if(WebDr.isExists_Display("Password")>0)
					{ flag=1;
					break;
					}
					Thread.sleep(1000);

				}
				if(flag==0&&!WebDr.getElement("headingLoginError").getText().isEmpty()&&!WebDr.getElement("headingLoginError").getText().contains("ActiveX is not enable on browser.'"))
				{ Launcher.login_error=WebDr.getElement("headingLoginError").getText();
				Reporter.ReportEvent("login error", "Login screen should be available ", Launcher.login_error+"'", false);
				Launcher.login_result=false;
				Driver.DB_Flag = "Y";
				flag=1;
				}
				else if(flag==0)
				{
					Reporter.ReportEvent("login error", "Login screen should be available ", " DB connection failed application not loaded", false);
					Launcher.login_result=false;
					Driver.DB_Flag = "Y";
					flag=1;
				}
				if(Launcher.login_result)
				{
					if(HtmlReporter.tc_result) WebDr.Wait_Time(3000);
					
					if(HtmlReporter.tc_result)   WebDr.setText("X", "Password", Login[1], "Entering data in password field", true);
					ROATFunctions.clickTab();
					if(HtmlReporter.tc_result) WebDr.Wait_Time(3000);
					if(HtmlReporter.tc_result)  WebDr.click("X", "Login", "Clicking on login button", true);
					if(HtmlReporter.tc_result) WebDr.Wait_Time(3000);
					try
					{
						

						if(WebDr.isExists_Display("headingLoginError")>0)
						{
							for(int i=0;i<3;i++)
							{
								if(WebDr.getElement("headingLoginError").getText().contains("User id:"))
								{
									Driver.driver.navigate().to((Driver.UrLp [Driver.urlcounter]+Driver.Country_code));
									if(HtmlReporter.tc_result) WebDr.setText("X", "username", Login[0], "Entering data in Username field", true);
									ROATFunctions.clickTab();
									if(HtmlReporter.tc_result) WebDr.Wait_Time(3000);
									if(HtmlReporter.tc_result)WebDr.setText("X", "Password", Login[1], "Entering data in password field", true);
									if(HtmlReporter.tc_result) WebDr.Wait_Time(3000);
									if(HtmlReporter.tc_result)  WebDr.click("X", "Login", "Clicking on login button", true);
									//if(HtmlReporter.tc_result)  WebDr.click("X", "Login", "Clicking on login button", true);
									if(HtmlReporter.tc_result) WebDr.Wait_Time(3000);
								}
								else
								{
									break;
								}
							}

						}

					}catch (Exception e) {
						e.printStackTrace();
					}

					for(int i=0;i<5;i++)
					{

						if(WebDr.getElement("headingLoginError").getText().contains("LDAP Connection failed!")&&!WebDr.getElement("headingLoginError").getText().contains("ActiveX is not enable on browser.'"))
						{ 
							Launcher.login_error=WebDr.getElement("headingLoginError").getText();
							Reporter.ReportEvent("login error", "Login screen should be available ", Launcher.login_error+"'", false);
							Launcher.login_result=false;
							flag=1;
							Driver.DB_Flag = "Y";


							break;
						}
						else if(WebDr.isExists_Display("dropdown_role")>0)
						{
							break;
						}
						Thread.sleep(1000);
					}
					if(Launcher.login_result)
					{
						if(HtmlReporter.tc_result) WebDr.Wait_Time(3000);
						
						if(HtmlReporter.tc_result) WebDr.waitTillElementIsDisplayed("dropdown_role");

						if(HtmlReporter.tc_result)  WebDr.select("X", "dropdown_role", "VisibleText",userrole, "Select role", true);

						if(HtmlReporter.tc_result)  WebDr.click("X", "Login", "Clicking on login button", true);
						if(HtmlReporter.tc_result) WebDr.Wait_Time(3000);
						
						try
						{
							//ROATFunctions.clickTab();

							if(WebDr.isExists_Display("headingLoginError")>0)
							{
								for(int i=0;i<3;i++)
								{
									if(WebDr.getElement("headingLoginError").getText().contains("User id:"))
									{
										Driver.driver.navigate().to((Driver.UrLp [Driver.urlcounter]+Driver.Country_code));
										if(HtmlReporter.tc_result) WebDr.setText("X", "username", Login[0], "Entering data in Username field", true);
										ROATFunctions.clickTab();
										if(HtmlReporter.tc_result) WebDr.Wait_Time(3000);
										if(HtmlReporter.tc_result)WebDr.setText("X", "Password", Login[1], "Entering data in password field", true);
										if(HtmlReporter.tc_result) WebDr.Wait_Time(3000);
										if(HtmlReporter.tc_result)  WebDr.click("X", "Login", "Clicking on login button", true);
										//if(HtmlReporter.tc_result)  WebDr.click("X", "Login", "Clicking on login button", true);
										if(HtmlReporter.tc_result) WebDr.Wait_Time(3000);
										if(HtmlReporter.tc_result) WebDr.waitTillElementIsDisplayed("dropdown_role");

										if(HtmlReporter.tc_result)  WebDr.select("X", "dropdown_role", "VisibleText",userrole, "Select role", true);

										if(HtmlReporter.tc_result)  WebDr.click("X", "Login", "Clicking on login button", true);
										if(HtmlReporter.tc_result) WebDr.Wait_Time(3000);
									}
									else
									{
										break;
									}
								}

							}

						}catch (Exception e) {
							e.printStackTrace();
						}
						
						
						flag=0;
						for(int i=0;i<10;i++)
						{
							if(WebDr.isExists("labelMenu")>0)
							{ 
								flag=1;
								break;
							}
							Thread.sleep(1000);

						}
						if(flag==0&&!WebDr.getElement("headingLoginError").getText().isEmpty()&&!WebDr.getElement("headingLoginError").getText().contains("ActiveX is not enable on browser.'"))
						{
							Launcher.login_error=WebDr.getElement("headingLoginError").getText();
							Reporter.ReportEvent("login error", "Login screen should be available ", Launcher.login_error+"'", false);
							Launcher.login_result=false;
							Driver.DB_Flag = "Y";
							flag=1;
						}
						else if(flag==0)
						{
							Reporter.ReportEvent("login error", "Login screen should be available ", " DB connection failed application not loaded", false);
							Launcher.login_result=false;
							Driver.DB_Flag = "Y";
							flag=1;

						}
					}
				}
			}
		}

		catch(Exception e)

		{
			e.printStackTrace();

			Reporter.ReportEvent("login error", "login button should click", "Error!:- Page not Found 404", false);
			Launcher.login_error="Error!:- Page not Found 404";
			Launcher.login_result=false;

		}

	}
	public static void check_current_system_date(String userrole)
{
	try
	{

		for(int i=0;i<3;i++)
		{
			if((WebDr.isExists_Display("sod_eod_pop_up")>0))
			{ 


				if(!fetchuser_role().equals("Supervisor"))
				{

					
					
					ROATFunctions.refesh_force(false,"Supervisor");
				}
				ROATNTBFLows.force_sod_eod();
				ROATFunctions.refesh(true,userrole);
				break;
			}
			Thread.sleep(1000);
		}
	}
	catch(Exception e)
	{
		e.printStackTrace();
		Reporter.ReportEvent("Performing eod", "Must be able to perform eod and sod", "Sod or eod not performed", false);
	}
}
	public static int Convert(String elementName)

	{
		try
		{
			String text=null;
			String desc=utility.WebDr.page_Objects.get(elementName);
			String [] a= desc.split("\\|");
			switch(a[0])
			{
			case "ID":  
				text = Driver.driver.findElement(By.id(a[1])).getText();
				return Integer.parseInt(text);
			case "CLASSNAME":  
				text =  Driver.driver.findElement(By.className(a[1])).getText();
				return Integer.parseInt(text);
			case "LINKTEXT": 
				text =  Driver.driver.findElement(By.linkText(a[1])).getText();
				return Integer.parseInt(text);
			case "NAME": 
				text =  Driver.driver.findElement(By.name(a[1])).getText();
				return Integer.parseInt(text);
			case "XPATH": 
				text =  Driver.driver.findElement(By.xpath(a[1])).getText();
				return Integer.parseInt(text);
			default: System.out.println("Function getElement cannot return object for " + elementName);break;
			}
		}
		catch(Exception e){System.out.println("Exeption in WebDr.getElement - "+e);return 0;}


		return 0;    
	}

	public static void cash_drawer_DD(String Dropdown, String Searchbox, String DD_values, String Inputvalue) throws Exception{

		//WebDr.waitTillElementIsDisplayed(WebDr.getElement(WebDr.page_Objects.get(Dropdown)), 2000);
		WebDr.clickX("X",Dropdown, "Click on dropdown", true);
		WebDr.Wait_Time(300);
		WebDr.setText("X", Searchbox,Inputvalue, "select the teller", true);
		WebDr.Wait_Time(300);
		WebDr.click("CUSTOM", WebDr.page_Objects.get(DD_values)+"//following::div[contains(text(),'"+Inputvalue+"')]", "click", true);


	}

	public static void cash_drawer_DDX(String Dropdown, String Searchbox, String DD_values, String Inputvalue) // DO NOT MO
			throws Exception{

		//WebDr.waitTillElementIsDisplayed(WebDr.getElement(WebDr.page_Objects.get(Dropdown)), 2000);
		WebDr.click("X",Dropdown, "Click on dropdown", true);
		WebDr.Wait_Time(300);
		WebDr.setText("X", Searchbox,Inputvalue, "select the teller", true);
		WebDr.Wait_Time(300);
		WebDr.click("CUSTOM", WebDr.page_Objects.get(DD_values)+"//following::div[contains(text(),'"+Inputvalue+"')]", "click", true);


	}
	//***************************************Navigation to Menu *********************************
	//********************************************************************************************

	public static void NavigateToMenu(String Page, String MenuName, String SubMenu) throws Exception{

		WebDr.SetPageObjects(Page);

		WebDr.click("X", MenuName, "Click on Main Menu", true);

		WebDr.click("X", SubMenu, "Click on Sub Menu", true);
	}
	public static void cash_drawer_DD1(String Dropdown, String Searchbox, String DD_values, String Inputvalue) throws Exception{

		//WebDr.waitTillElementIsDisplayed(WebDr.getElement(WebDr.page_Objects.get(Dropdown)), 2000);
		WebDr.clickX("X",Dropdown, "Click on dropdown", true);
		WebDr.setText("X", Searchbox,Inputvalue, "select the teller", true);
	}

	public static void CDA_SysDateValidation(String pageObject, String xpath) throws Exception {
		String SysDate=null;
		String CDA_Date=null;
		try {

			WebDr.SetPageObjects(pageObject);
			if (HtmlReporter.tc_result==true){
				CDA_Date=WebDr.page_Objects.get(xpath);              
				DateFormat dateFormat = new SimpleDateFormat("dd-MM-yyyy HH:mm");
				Date date = new Date();
				String CurrentDate = dateFormat.format(date);
				SysDate = WebDr.getElementObject(CDA_Date)
						.getAttribute("innerText");
				Date sys = new SimpleDateFormat("dd-MM-yyyy HH:mm").parse(SysDate);

				DateFormat dateFormat2 = new SimpleDateFormat("dd-MM-yyyy HH:mm");
				String sysD = dateFormat2.format(sys);



				if (sysD.equalsIgnoreCase(CurrentDate)) {
					Reporter.ReportEvent("Successfully verified system data and time with application date and time",
							"Verify:" + SysDate + "", "Verified:" + CurrentDate + "", true);

				} else {
					Reporter.ReportEvent("Unable to verify system data and time with application date and time",
							"Verify:" + SysDate + "", "Verified:" + dateFormat + "", false);
					//Reporter.ReportBug("Application date should be equal to system date format ","Application date should be  present");
				}
			}                    
		}
		catch (Exception e) {

			Reporter.ReportEvent("Unable to verify system data and time with application date and time",
					"Verify:" + SysDate + "", "Verified:" + e + "", false);
			//Reporter.ReportBug("Application date should be equal to system date format ","Application date should be  present");
		}
	}

	public static void validate_CashDrawer_user_page(String Htmlid,String value, int index){
		try{
			WebElement oElm = null;
			String fieldName,  expectedValue;
			String [] a= value.split("\\|");
			fieldName=a[0];
			expectedValue=a[1];

			if(a[0].contains("Branch")){
				oElm = WebDr.getElementObject("XPATH|(//*[contains(@id,'"+Htmlid+"')][contains(@id,'Val')])["+index+"]");
			}else{
				oElm = WebDr.getElementObject("XPATH|//div[@id='"+Htmlid + index+ "'][text()='" + fieldName + "']/following-sibling::div[1]");

			}
			String actual=oElm.getText().replace(",", "");
			actual=actual.substring(0, actual.length()-3);

			if(actual.equals(expectedValue))
				HtmlReporter.WriteStep("Verifying User_view_Page", fieldName + "  Expected:" + expectedValue, "Actual:" + actual, true);

			else
				HtmlReporter.WriteStep("Verifying User_view_Page", fieldName + "  Expected:" + expectedValue, "Actual:" + actual, false);
		}catch(Exception e ){

			e.printStackTrace();
			Reporter.ReportEvent("Test Case execution is failed", "View Text is displayed", "View Text is not displayed",false);

		}

	}

	public static void clickEnter() {
		Actions action = new Actions(Driver.driver);
		action.sendKeys(Keys.ENTER).build().perform();    
	}

	public static void clickTab() throws InterruptedException {
		Actions action = new Actions(Driver.driver);
		action.sendKeys(Keys.TAB).build().perform();  
		Thread.sleep(1000);
	}
	public static void clickEsc() throws InterruptedException {
		Actions action = new Actions(Driver.driver);
		action.sendKeys(Keys.ESCAPE).build().perform();  
		Thread.sleep(1000);
	}
	public static void clickPageDown() throws InterruptedException
	{
		Actions action = new Actions(Driver.driver);
		action.sendKeys(Keys.PAGE_DOWN).build().perform();
		Thread.sleep(1000);
	}

	public static void validateAccountInquiry(String value) throws Exception
	{try
	{
		String fieldName,  expectedValue;
		String [] a= value.split("\\|");
		fieldName=a[0];
		expectedValue=a[1];
		WebElement oElm=WebDr.getElementObject("XPATH|//p-accordiontab//div[text()='" + fieldName + "']/following-sibling::div[1]");
		String actual=oElm.getText();
		if(actual.equals(": " + expectedValue))
			HtmlReporter.WriteStep("Verifying Account Inquiry", fieldName + "  Expected:" + expectedValue, "Actual:" + actual, true);
		else
			HtmlReporter.WriteStep("Verifying Account Inquiry", fieldName + "  Expected:" + expectedValue, "Actual:" + actual, false);

	}catch(Exception e )
	{
		Reporter.ReportBug("Testing", "This is testing");
	}
	}

	public static void validateCustomerInquiry(String value)
	{
		String fieldName,  expectedValue;
		String [] a= value.split("\\|");
		fieldName=a[0];
		expectedValue=a[1];
		WebElement oElm=WebDr.getElementObject("XPATH|//p-accordiontab//div[text()='" + fieldName + "']/following-sibling::div[1]");
		String actual=oElm.getText();
		if(actual.equals(": " + expectedValue))
			HtmlReporter.WriteStep("Verifying Customer Inquiry", fieldName + "  Expected:" + expectedValue, "Actual:" + actual, true);
		else
			HtmlReporter.WriteStep("Verifying Customer Inquiry", fieldName + "  Expected:" + expectedValue, "Actual:" + actual, false);

	}

	public static void validateReferStream(String value) 
	{
		try{
			String fieldName,  expectedValue;
			String [] a= value.split("\\|");
			fieldName=a[0];
			expectedValue=a[1];
			WebElement oElm=WebDr.getElementObject("XPATH|//div[@class='ui-grid-row']//div[contains(text(),'" + fieldName + "')]/following-sibling::div[1]");
			String actual=oElm.getText();                 
			if(actual.equals(": " + expectedValue))
				Reporter.ReportEvent("Verifying Refer Stream Code", fieldName + "  Expected:" + expectedValue, "Actual:" + actual, true);
			else
				Reporter.ReportEvent("Verifying Refer Stream Code", fieldName + "  Expected:" + expectedValue, "Actual:" + actual, false);


		}catch(Exception e)
		{
			Reporter.ReportEvent("View page is not visible hence cannot be validated", "View page should be visible to validate", "View page is not visible ", false);
			//Driver.driver.quit();
		}
	}

	//*************************************************************************************************************
	//********************Authorizing the Action*******************************************************************

	public static int Authorization(String Auth_Username,String Password, boolean isMandatory)
	{

		WebDr.isDefect=false;
		try
		{
			if(WebDr.getValue(Auth_Username)!=null)
			{
				WebElement elmn;

				elmn=WebDr.getElementObject("ID|inputauthousername");

				if(elmn.isDisplayed())
				{
					elmn.clear();
					Thread.sleep(500);
					elmn.sendKeys(Auth_Username);
					ROATFunctions.clickTab();
					WebDr.isDefect=false;
					Reporter.ReportEvent("User Authorization", "Enter Text", "Entered: Username", true );
				}
				else
				{
					WebDr.isDefect=true;
					Reporter.ReportEvent("Object not visible - " + "User Authorization", "Enter Text", "Not Entered - Username" , false );
				}
			}

			if(WebDr.getValue(Password)!=null)
			{
				WebElement elmn;

				elmn=WebDr.getElementObject("ID|inputauthopassword");

				if(elmn.isDisplayed())
				{
					elmn.clear();
					Thread.sleep(500);
					elmn.sendKeys(Password);
					WebDr.isDefect=false;
					Reporter.ReportEvent("User Authorization", "Enter Text", "Entered: Password" , true );
				}
				else
				{
					WebDr.isDefect=true;
					Reporter.ReportEvent("Object not visible - " + "User Authorization", "Enter Text", "Not Entered - Password" , false );
				}
			}

			Driver.driver.findElement(By.id("inputauthopassword")).sendKeys(Keys.TAB);
			Thread.sleep(5000);
			Driver.driver.findElement(By.id("buttonAuthorize")).sendKeys(Keys.ENTER);
		}
		catch(Exception e){System.out.println("Exeption in WebDr.setText - "+e);}
		finally
		{
			if((isMandatory) &&(WebDr.isDefect))
			{
				System.out.println("WebDr.Call for a Bug Creation - Create Bug");
			}
		}

		return 0;

	}

	//***************************************************************************************************************
	//***************************************************************Rejecting the Action****************************


	public static int Rejection(String Auth_Username,String Auth_Password, boolean isMandatory)
	{

		WebDr.isDefect=false;
		try
		{
			if(WebDr.getValue(Auth_Username)!=null)
			{
				WebElement elmn;

				elmn=WebDr.getElementObject("ID|inputauthousername");

				if(elmn.isDisplayed())
				{
					elmn.clear();
					Thread.sleep(500);
					elmn.sendKeys(Auth_Username);
					WebDr.isDefect=false;
					Reporter.ReportEvent("User Rejection", "Enter Text", "Entered: Username", true );
				}
				
				if(WebDr.getValue(Auth_Password)!=null)
				{
					WebDr.isDefect=true;
					Reporter.ReportEvent("Object not visible - " + "User Rejection", "Enter Text", "Not Entered - Username" , false );
				}
			}

			if(WebDr.getValue(Auth_Password)!=null)
			{
				WebElement elmn;

				elmn=WebDr.getElementObject("ID|inputauthopassword");

				if(elmn.isDisplayed())
				{
					elmn.clear();
					Thread.sleep(500);
					elmn.sendKeys(Auth_Password);
					WebDr.isDefect=false;
					Reporter.ReportEvent("User Rejection", "Enter Text", "Entered: Password", true );
				}
				else
				{
					WebDr.isDefect=true;
					Reporter.ReportEvent("Object not visible - " + "User Rejection", "Enter Text", "Not Entered - Password", false );
				}
			}

			Driver.driver.findElement(By.id("inputauthopassword")).sendKeys(Keys.TAB);
			Driver.driver.findElement(By.id("buttonAuthorize")).sendKeys(Keys.TAB);
			Thread.sleep(5000);
			Driver.driver.findElement(By.id("buttonReject")).sendKeys(Keys.ENTER);
			WebDr.setText("X", "txt_rejectreason", WebDr.getValue("Reject_Reason"), "Entering reason for deletion", true);
			Driver.driver.findElement(By.id("inputautho")).sendKeys(Keys.TAB);
			Driver.driver.findElement(By.id("authosubmit")).sendKeys(Keys.ENTER);
		}
		catch(Exception e){System.out.println("Exeption in WebDr.setText - "+e);}
		finally
		{
			if((isMandatory) &&(WebDr.isDefect))
			{
				System.out.println("WebDr.Call for a Bug Creation - Create Bug");
			}
		}

		return 0;

	}
	//		public static void TellerPosition_Count(String PageObjectname,String tableID,String textboxID, String Soiled, String Decoy, String TotalAmount, String CashAmount) throws Exception{
	//			WebDr.SetPageObjects(PageObjectname);
	//			try{			
	//				String table = WebDr.page_Objects.get(tableID);
	//				String txt = WebDr.page_Objects.get(textboxID);
	//							
	//				List<WebElement> rows = WebDr.getElements(tableID);
	//				int j = 1;
	//				boolean flag  = false;
	//				int RowSize=rows.size();
	//				System.out.println(RowSize);
	//				for (int i=1;i<=rows.size();i++)
	//				{
	//					
	//					WebElement row_value = WebDr.getElementObject(table+"["+i+"]//span");							
	//					
	//					flag = false;
	//					System.out.println(row_value.getText());
	//					if (Integer.parseInt(row_value.getAttribute("innerText"))>0)
	//					{
	//						WebDr.setText("CUSTOM", txt+"["+j+"]", "1", "Enter value in text field", true);
	//						
	//						j = j +1;
	//						flag = true;
	//					}
	//					else
	//					{						
	//						WebDr.setText("CUSTOM", txt+"["+j+"]", "0", "Enter value in text field", true);
	//						
	//						j = j +1;
	//						flag = true;
	//					}
	//				}
	//				if (flag)
	//				{
	//					Reporter.ReportEvent("All Notes values are entered", "All Notes values should be enter", "All Notes values are entered",true);
	//				}
	//				
	//				Robot robot =new Robot();
	//		    	String Amount = WebDr.getValue("Varify_Text");
	//		    	String labelAmt = WebDr.page_Objects.get("label_TotalAmt");
	//		    	if (HtmlReporter.tc_result==true)WebDr.setText("X", Soiled, "1", "Enter soiled Notes", true);
	//		    	if (HtmlReporter.tc_result==true)WebDr.setText("X", Decoy, "1", "Enter Decoy Coins", true);//WebDr.getValue("Positive_Flow_Currency")
	//		    	
	//		    	robot.keyPress(KeyEvent.VK_TAB);
	//		    	robot.keyRelease(KeyEvent.VK_TAB);
	//		  
	//			}
	//			catch(Exception e)
	//			{
	//				e.printStackTrace();
	//				System.out.println("Exeption in WebDr.setText - "+e);
	//				Reporter.ReportEvent("Object is not Found", "Object should be display on screen", "Object not present",false);
	//			}
	//			
	//		}

	public static void  scrollTillEnd() {
		JavascriptExecutor js = (JavascriptExecutor) Driver.driver;
		js.executeScript("window.scrollTo(0, document.body.scrollHeight)");
	}
	public static void  scrolltotop() 
	{
		JavascriptExecutor js = (JavascriptExecutor) Driver.driver;
		js.executeScript("window.scrollTo(0, 0)");
	}
	public static void  scrollTillEnd_element(WebElement element) {
		JavascriptExecutor js = (JavascriptExecutor) Driver.driver;
		js.executeScript("arguments[0].scrollIntoView(true);",element);
	}

	public static void Vault_PaymentToATM_DenominationCount(String PageObjectname,String tableID,String textboxID, String TotalAmount, String CashAmount) throws Exception{
		WebDr.SetPageObjects(PageObjectname);
		try{			
			String table = WebDr.page_Objects.get(tableID);
			String txt = WebDr.page_Objects.get(textboxID);


			List<WebElement> rows = WebDr.getElements(tableID);
			int j = 1;
			boolean flag  = false;
			int RowSize=rows.size();
			System.out.println(RowSize);
			for (int i=1;i<=rows.size();i++)
			{

				WebElement row_value = WebDr.getElementObject(table+"["+i+"]");							

				WebDr.setText("CUSTOM", txt+"["+i+"]", "1", "Enter value in text field", true);

			}
			Robot robot=new Robot();
			//if (HtmlReporter.tc_result == true)ROATFunctions.clickTab();
			String CDA_TotAmount = WebDr.page_Objects.get(TotalAmount);
			String CDA_TotAmountFinal =WebDr.getElementObject(CDA_TotAmount).getAttribute("innerText");
			String CDA_TotAmountFinalNum=CDA_TotAmountFinal.split(" ")[1].toString();
			String CDA_TotAmountFinalNumUpdated= CDA_TotAmountFinalNum.replaceAll(",", "");

			System.out.println(CDA_TotAmountFinalNumUpdated);
			String val=CDA_TotAmountFinalNumUpdated;

			WebElement element = Driver.driver.findElement(By.id("txtAmount"));

			//		        element.clear();
			//		        for (int i = 0; i < val.length(); i++)
			//		        {
			//
			//		            char c = val.charAt(i);
			//
			//		            String s = new StringBuilder().append(c).toString();
			//
			//		            element.sendKeys(s);
			//
			//		        }  				

			//		        robot.keyPress(KeyEvent.VK_TAB);
			//		    	robot.keyRelease(KeyEvent.VK_TAB);


		}
		catch(Exception e)
		{
			e.printStackTrace();
			System.out.println("Exeption in WebDr.setText - "+e);
			Reporter.ReportEvent("Object is not Found", "Object should be display on screen", "Object not present",false);
		}

	}

	//************************************************************************************************************************
	// ********************************************Vault FCY Breakdown**********************************************************************

	public static void Vault_FCYBreakdown() throws Exception {

		if (HtmlReporter.tc_result==true)ROATModules.navigateVault_FCYBreakdown();
		if (HtmlReporter.tc_result==true)ROATModules.FCYB_VerificationAndAuthorization();
		if (HtmlReporter.tc_result==true)ROATModules.PaginationValidation("VaultFCYBreakdown","PaginationTop");
		if (HtmlReporter.tc_result==true)ROATModules.PaginationValidation("VaultFCYBreakdown","PaginationBottom");
		if (HtmlReporter.tc_result==true)ROATModules.setup_BalanceFCYVerification_VaultBreakdown();					
		if (HtmlReporter.tc_result==true)ROATModules.Vault_FCYBreakdown_ExpandCollapse();
		if (HtmlReporter.tc_result==true)ROATModules.setup_FCYDownload();




	}
	// *******************************************Pagination function

	// ******************************************************************************************
	// Author:Nilima Vaidya
	//creation Date:25/05/2018
	//Functionality: To validation pagination functionality on each page

	public static void PaginationValidation(String pageObject,String xpath) throws Exception {
		try {
			WebDr.SetPageObjects(pageObject);
			WebDr.Implicite_wait(3000);
			String PaginationPath=WebDr.page_Objects.get(xpath);	    	
			if(WebDr.getElementObject(PaginationPath).isDisplayed()){
				Reporter.ReportEvent("Able to get Pagination format on current page", "Pagination format is present on this page",
						"Pagination format is present on this page", true);
			}else{
				Reporter.ReportEvent("Unable to get Pagination format on current page", "Pagination format is not present on this page",
						"Pagination format is present on this page", false);
			}
		} catch (Exception e) {
			e.printStackTrace();
			Reporter.ReportEvent("Unable to get Pagination format on current page", "Verify:" + "Pagination" + "",
					"Verified:" + e + "", false);			
		}
	}
	//public static void refesh(boolean ismandatory, String Userrole)
	//{
	//	boolean tc_result=HtmlReporter.tc_result;
	//    HtmlReporter.tc_result=true;
	//	
	//	try{
	//		WebDr.SetPageObjects("MasterPage");
	//		if(Launcher.login_result){
	//		if(ismandatory)
	//		{
	//			if(!Launcher.UserRole_Last.equals("0"))
	//			{
	//				WebDr.clickX("X", "profile_name", "Clicking logout menu", true);
	//				WebDr.waitTillElementIsDisplayed("logout_button");
	//				WebDr.clickX("X", "logout_button", "click on logout button", true);
	//				WebDr.waitTillElementIsDisplayed("logout_confirm");
	//				JavascriptExecutor js  = (JavascriptExecutor) Driver.driver;	
	//				WebElement koken = WebDr.getElementObject("XPATH|//*[@id='logoutDialog']/div");
	//				
	//				js.executeScript("arguments[0].style.opacity ='1'", koken);
	//				WebDr.clickX("X", "logout_confirm", "click on logout confirm button", true);
	//			}
	//					
	//			if (Driver.EnvType.equals("ALL"))
	//			{
	//			Driver.driver.navigate().to((Driver.UrLp [Driver.urlcounter]+Driver.Country_code));
	//				ROATFunctions.logintoROATeller(Userrole);
	//			}
	//			else
	//			{
	//				Driver.driver.navigate().to(Driver.appURL);
	//				ROATFunctions.logintoROATeller(Userrole);
	//				Driver.Country_code = "";
	//			}
	//		}
	//		else
	//		{
	//			if (!(Launcher.UserRole_Last.equals(Userrole)))
	//			{
	//				if(!Launcher.UserRole_Last.equals("0"))
	//				{
	//					WebDr.clickX("X", "profile_name", "Clicking logout menu", true);
	//					WebDr.waitTillElementIsDisplayed("profile_name");
	//					WebDr.clickX("X", "logout_button", "click on logout button", true);
	//					WebDr.waitTillElementIsDisplayed("logout_button");
	//					JavascriptExecutor js  = (JavascriptExecutor) Driver.driver;
	//					WebElement koken = WebDr.getElementObject("XPATH|//*[@id='logoutDialog']/div");
	//					
	//					js.executeScript("arguments[0].style.opacity ='1'", koken);
	//					WebDr.clickX("X", "logout_confirm", "click on logout confirm button", true);
	//					
	//				}
	//				
	//			if (Driver.EnvType.equals("ALL"))
	//			{
	//				Driver.driver.navigate().to(Driver.UrLp [Driver.urlcounter]+Driver.Country_code);
	//				ROATFunctions.logintoROATeller(Userrole);
	//			}
	//			else
	//			{
	//				Driver.driver.navigate().to(Driver.appURL);
	//				ROATFunctions.logintoROATeller(Userrole);
	//				Driver.Country_code = "";
	//			}
	//			}
	//		
	//
	//		
	//		}
	//		
	//	Launcher.UserRole_Last = Userrole;
	//		}
	//		else
	//		{
	//			
	//			Reporter.ReportEvent("login error", "Login screen should be available",  Launcher.login_error, false);
	//			
	//		}
	//		}
	//	
	//	catch(Exception e)
	//	{
	//		e.printStackTrace();
	//	
	//	}
	//	HtmlReporter.tc_result=tc_result;
	//}
	/*public static void refesh(boolean ismandatory, String Userrole)
{
	boolean tc_result=HtmlReporter.tc_result;
    HtmlReporter.tc_result=true;
   for(int i=0;i<3;i++)
	   {
	   WebDr.SetPageObjects("MasterPage");

	   if(!(WebDr.isExists_Display("profile_name")>0))
	   {
		   ismandatory=true;
		   Launcher.UserRole_Last="0";
		   break;
	   }
	   }
	try{
		WebDr.SetPageObjects("MasterPage");
		if(Launcher.login_result){
		if(ismandatory)
		{
			if(!Launcher.UserRole_Last.equals("0"))
			{
				WebDr.clickX("X", "profile_name", "Clicking logout menu", true);
				WebDr.waitTillElementIsDisplayed("logout_button");
				WebDr.clickX("X", "logout_button", "click on logout button", true);
				WebDr.waitTillElementIsDisplayed("logout_confirm");
				JavascriptExecutor js  = (JavascriptExecutor) Driver.driver;	
				WebElement koken = WebDr.getElementObject("XPATH|//*[@id='logoutDialog']/div");

				js.executeScript("arguments[0].style.opacity ='1'", koken);
				WebDr.clickX("X", "logout_confirm", "click on logout confirm button", true);
			}

			if (Driver.EnvType.equals("ALL"))
			{
			Driver.driver.navigate().to((Driver.UrLp [Driver.urlcounter]+Driver.Country_code));
				ROATFunctions.logintoROATeller(Userrole);
			}
			else
			{
				Driver.driver.navigate().to(Driver.appURL);
				ROATFunctions.logintoROATeller(Userrole);
				Driver.Country_code = "";
			}
		}
		else
		{
			if (!(Launcher.UserRole_Last.equals(Userrole)))
			{
				if(!Launcher.UserRole_Last.equals("0"))
				{
					WebDr.clickX("X", "profile_name", "Clicking logout menu", true);
					WebDr.waitTillElementIsDisplayed("profile_name");
					WebDr.clickX("X", "logout_button", "click on logout button", true);
					WebDr.waitTillElementIsDisplayed("logout_button");
					JavascriptExecutor js  = (JavascriptExecutor) Driver.driver;
					WebElement koken = WebDr.getElementObject("XPATH|//*[@id='logoutDialog']/div");

					js.executeScript("arguments[0].style.opacity ='1'", koken);
					WebDr.clickX("X", "logout_confirm", "click on logout confirm button", true);

				}

			if (Driver.EnvType.equals("ALL"))
			{
				Driver.driver.navigate().to(Driver.UrLp [Driver.urlcounter]+Driver.Country_code);
				ROATFunctions.logintoROATeller(Userrole);
			}
			else
			{
				Driver.driver.navigate().to(Driver.appURL);
				ROATFunctions.logintoROATeller(Userrole);
				Driver.Country_code = "";
			}
			}



		}

	Launcher.UserRole_Last = Userrole;
		}
		else
		{

			Reporter.ReportEvent("login error", "Login screen should be available",  Launcher.login_error, false);

		}
		}

	catch(Exception e)
	{
		e.printStackTrace();

	}
	HtmlReporter.tc_result=tc_result;
}*/

	public static void refesh(boolean ismandatory, String Userrole) throws Exception
	{

		
		boolean tc_result=HtmlReporter.tc_result;
		HtmlReporter.tc_result=true;
		for(int i=0;i<3;i++)
		{
			WebDr.SetPageObjects("MasterPage");

			if(!(WebDr.isExists_Display("profile_name")>0))
			{
				ismandatory=true;
				Launcher.UserRole_Last="0";
				break;
			}
		}
		try{
			WebDr.SetPageObjects("MasterPage");
			if(Launcher.login_result){
				if(ismandatory)
				{
					if(!Launcher.UserRole_Last.equals("0"))
					{
						//				WebDr.clickX("X", "logout_button", "Clicking logout menu", true);
						WebDr.waitTillElementIsDisplayed("logout_button");
						WebDr.clickX("X", "logout_button", "click on logout button", true);
						WebDr.waitTillElementIsDisplayed("logout_confirm");
						JavascriptExecutor js  = (JavascriptExecutor) Driver.driver;	
						WebElement koken = WebDr.getElementObject("XPATH|//*[@id='logoutDialog']/div");

						js.executeScript("arguments[0].style.opacity ='1'", koken);
						WebDr.clickX("X", "logout_confirm", "click on logout confirm button", true);
						//rhea added
						Driver.driver.quit();
						Driver ob=new Driver();  
						ob.relaunch();
					}

					if (Driver.EnvType.equals("ALL"))
					{
						
						Driver.driver.navigate().to((Driver.appURL));
						ROATFunctions.logintoROATeller(Userrole);
					}
					else
					{
						Launcher.UserRole_Last = Userrole;
						Driver.driver.navigate().to(Driver.appURL);
						ROATFunctions.logintoROATeller(Userrole);
						Driver.Country_code = "";
						
					}
				}
				else
				{
					if (!(Launcher.UserRole_Last.equals(Userrole)))
					{
						if(!Launcher.UserRole_Last.equals("0"))
						{
							//					WebDr.clickX("X", "profile_name", "Clicking logout menu", true);
							//					WebDr.waitTillElementIsDisplayed("profile_name");
							//					WebDr.clickX("X", "logout_button", "click on logout button", true);
							//					WebDr.waitTillElementIsDisplayed("logout_button");
							//					JavascriptExecutor js  = (JavascriptExecutor) Driver.driver;
							//					WebElement koken = WebDr.getElementObject("XPATH|//*[@id='logoutDialog']/div");
							//					
							//					js.executeScript("arguments[0].style.opacity ='1'", koken);
							//					WebDr.clickX("X", "logout_confirm", "click on logout confirm button", true);
							WebDr.waitTillElementIsDisplayed("logout_button");
							WebDr.clickX("X", "logout_button", "click on logout button", true);
							WebDr.waitTillElementIsDisplayed("logout_confirm");
							JavascriptExecutor js  = (JavascriptExecutor) Driver.driver;	
							WebElement koken = WebDr.getElementObject("XPATH|//*[@id='logoutDialog']/div");

							js.executeScript("arguments[0].style.opacity ='1'", koken);
							WebDr.clickX("X", "logout_confirm", "click on logout confirm button", true);
							WebDr.Wait_Time(2000);
							Driver.driver.quit();
							//rhea added
							Driver ob=new Driver();  
							ob.relaunch();
							//WebDr.openApplication(Driver.UrLp [Driver.urlcounter]+CommonUtils.readConfig("Country_code"));
							//rhea added above single comment
						}

						if (Driver.EnvType.equals("ALL"))
						{
							//rhea added
							Driver.driver.close();
							Driver ob=new Driver();  
							ob.relaunch();
							//s
							//
							Driver.driver.navigate().to(Driver.UrLp [Driver.urlcounter]+Driver.Country_code);
							ROATFunctions.logintoROATeller(Userrole);
						}
						else
						{
							//rhea added
							Driver.driver.close();
							Driver ob=new Driver();  
							ob.relaunch();
							//s
							//Driver.driver.navigate().to(Driver.appURL);
							ROATFunctions.logintoROATeller(Userrole);
							Driver.Country_code = "";
						}
					}

				}

				Launcher.UserRole_Last = Userrole;
			}
			else
			{

				Reporter.ReportEvent("login error", "Login screen should be available",  Launcher.login_error, false);

			}
		}

		catch(Exception e)
		{
			e.printStackTrace();

		}
		HtmlReporter.tc_result=tc_result;

	}
	public static void refesh_authorizer(boolean ismandatory, String Userrole) throws Exception
	{

		//rhea added
				Driver.driver.close();
				Driver ob=new Driver();  
				ob.relaunch();
		//s
		boolean tc_result=HtmlReporter.tc_result;
		HtmlReporter.tc_result=true;
		for(int i=0;i<3;i++)
		{
			WebDr.SetPageObjects("MasterPage");

			if(!(WebDr.isExists_Display("profile_name")>0))
			{
				ismandatory=true;
				Launcher.UserRole_Last="0";
				break;
			}
		}
		try{
			WebDr.SetPageObjects("MasterPage");
			if(Launcher.login_result){
				if(ismandatory)
				{
					if(!Launcher.UserRole_Last.equals("0"))
					{
						//				WebDr.clickX("X", "logout_button", "Clicking logout menu", true);
						WebDr.waitTillElementIsDisplayed("logout_button");
						WebDr.clickX("X", "logout_button", "click on logout button", true);
						WebDr.waitTillElementIsDisplayed("logout_confirm");
						JavascriptExecutor js  = (JavascriptExecutor) Driver.driver;	
						WebElement koken = WebDr.getElementObject("XPATH|//*[@id='logoutDialog']/div");

						js.executeScript("arguments[0].style.opacity ='1'", koken);
						WebDr.clickX("X", "logout_confirm", "click on logout confirm button", true);
						Driver.driver.quit();
						Driver ob1=new Driver();  
						ob1.relaunch();
					}

					if (Driver.EnvType.equals("ALL"))
					{
						Driver.driver.navigate().to((Driver.UrLp [Driver.urlcounter]+Driver.Country_code));
						ROATFunctions.logintoROATeller_authoriser(Userrole);
					}
					else
					{
						Driver.driver.quit();
						Driver ob1=new Driver();  
						ob1.relaunch();
						Driver.driver.navigate().to(Driver.appURL);
						ROATFunctions.logintoROATeller_authoriser(Userrole);
						Driver.Country_code = "";
					}
				}
				else
				{
					if (!(Launcher.UserRole_Last.equals(Userrole)))
					{
						if(!Launcher.UserRole_Last.equals("0"))
						{
							//					WebDr.clickX("X", "profile_name", "Clicking logout menu", true);
							//					WebDr.waitTillElementIsDisplayed("profile_name");
							//					WebDr.clickX("X", "logout_button", "click on logout button", true);
							//					WebDr.waitTillElementIsDisplayed("logout_button");
							//					JavascriptExecutor js  = (JavascriptExecutor) Driver.driver;
							//					WebElement koken = WebDr.getElementObject("XPATH|//*[@id='logoutDialog']/div");
							//					
							//					js.executeScript("arguments[0].style.opacity ='1'", koken);
							//					WebDr.clickX("X", "logout_confirm", "click on logout confirm button", true);
							WebDr.waitTillElementIsDisplayed("logout_button");
							WebDr.clickX("X", "logout_button", "click on logout button", true);
							WebDr.waitTillElementIsDisplayed("logout_confirm");
							JavascriptExecutor js  = (JavascriptExecutor) Driver.driver;	
							WebElement koken = WebDr.getElementObject("XPATH|//*[@id='logoutDialog']/div");

							js.executeScript("arguments[0].style.opacity ='1'", koken);
							WebDr.clickX("X", "logout_confirm", "click on logout confirm button", true);
							WebDr.Wait_Time(2000);
							Driver.driver.quit();
							Driver.driver.quit();
							Driver ob1=new Driver();  
							ob1.relaunch();

						}

						if (Driver.EnvType.equals("ALL"))
						{
							Driver.driver.navigate().to(Driver.UrLp [Driver.urlcounter]+Driver.Country_code);
							ROATFunctions.logintoROATeller_authoriser(Userrole);
						}
						else
						{
							Driver.driver.navigate().to(Driver.appURL);
							ROATFunctions.logintoROATeller_authoriser(Userrole);
							Driver.Country_code = "";
						}
					}



				}

				Launcher.UserRole_Last = Userrole;
			}
			else
			{

				Reporter.ReportEvent("login error", "Login screen should be available",  Launcher.login_error, false);

			}
		}

		catch(Exception e)
		{
			e.printStackTrace();

		}
		HtmlReporter.tc_result=tc_result;

	}
	public static void refesh_force(boolean ismandatory, String Userrole) throws Exception
	{
		//rhea added
		Driver.driver.close();
		Driver ob=new Driver();  
		ob.relaunch();
		//s
		boolean tc_result=HtmlReporter.tc_result;
		HtmlReporter.tc_result=true;
		for(int i=0;i<3;i++)
		{
			WebDr.SetPageObjects("MasterPage");

			if(!(WebDr.isExists_Display("profile_name")>0))
			{
				ismandatory=true;
				Launcher.UserRole_Last="0";
				break;
			}
		}
		try{
			WebDr.SetPageObjects("MasterPage");
			if(Launcher.login_result){
				if(ismandatory)
				{
					if(!Launcher.UserRole_Last.equals("0"))
					{
						//				WebDr.clickX("X", "logout_button", "Clicking logout menu", true);
						WebDr.waitTillElementIsDisplayed("logout_button");
						WebDr.clickX("X", "logout_button", "click on logout button", true);
						WebDr.waitTillElementIsDisplayed("logout_confirm");
						JavascriptExecutor js  = (JavascriptExecutor) Driver.driver;	
						WebElement koken = WebDr.getElementObject("XPATH|//*[@id='logoutDialog']/div");

						js.executeScript("arguments[0].style.opacity ='1'", koken);
						WebDr.clickX("X", "logout_confirm", "click on logout confirm button", true);
						Driver.driver.quit();
						WebDr.openApplication(Driver.UrLp [Driver.urlcounter]+Driver.Country_code);
					}

					if (Driver.EnvType.equals("ALL"))
					{
						Driver.driver.navigate().to((Driver.UrLp [Driver.urlcounter]+Driver.Country_code));
						ROATFunctions.logintoROATeller_force(Userrole);
					}
					else
					{
						Driver.driver.navigate().to(Driver.appURL);
						ROATFunctions.logintoROATeller_force(Userrole);
						Driver.Country_code = "";
					}
				}
				else
				{
					if (!(Launcher.UserRole_Last.equals(Userrole)))
					{
						if(!Launcher.UserRole_Last.equals("0"))
						{
							//					WebDr.clickX("X", "profile_name", "Clicking logout menu", true);
							//					WebDr.waitTillElementIsDisplayed("profile_name");
							//					WebDr.clickX("X", "logout_button", "click on logout button", true);
							//					WebDr.waitTillElementIsDisplayed("logout_button");
							//					JavascriptExecutor js  = (JavascriptExecutor) Driver.driver;
							//					WebElement koken = WebDr.getElementObject("XPATH|//*[@id='logoutDialog']/div");
							//					
							//					js.executeScript("arguments[0].style.opacity ='1'", koken);
							//					WebDr.clickX("X", "logout_confirm", "click on logout confirm button", true);
							WebDr.waitTillElementIsDisplayed("logout_button");
							WebDr.clickX("X", "logout_button", "click on logout button", true);
							WebDr.waitTillElementIsDisplayed("logout_confirm");
							JavascriptExecutor js  = (JavascriptExecutor) Driver.driver;	
							WebElement koken = WebDr.getElementObject("XPATH|//*[@id='logoutDialog']/div");

							js.executeScript("arguments[0].style.opacity ='1'", koken);
							WebDr.clickX("X", "logout_confirm", "click on logout confirm button", true);
							Driver.driver.quit();
							WebDr.openApplication(Driver.UrLp [Driver.urlcounter]+Driver.Country_code);

						}

						if (Driver.EnvType.equals("ALL"))
						{
							Driver.driver.close();
							Driver ob1=new Driver();  
							ob1.relaunch();
							Driver.driver.navigate().to(Driver.UrLp [Driver.urlcounter]+Driver.Country_code);
							ROATFunctions.logintoROATeller_force(Userrole);
						}
						else
						{
							Driver.driver.close();
							Driver ob1=new Driver();  
							ob1.relaunch();
							Driver.driver.navigate().to(Driver.appURL);
							ROATFunctions.logintoROATeller_force(Userrole);
							Driver.Country_code = "";
						}
					}



				}

				Launcher.UserRole_Last = Userrole;
			}
			else
			{

				Reporter.ReportEvent("login error", "Login screen should be available",  Launcher.login_error, false);

			}
		}

		catch(Exception e)
		{
			e.printStackTrace();

		}
		HtmlReporter.tc_result=tc_result;

	}

	public static void refesh(boolean ismandatory)
	{
		int flag=0;
		try{
			if(HtmlReporter.tc_result==false)
			{
				flag=1;
				HtmlReporter.tc_result=true;
			}

			if(ismandatory)
			{
				HtmlReporter.tc_result=true;
				if (Driver.EnvType.equals("ALL"))
				{
					Driver.driver.navigate().to((Driver.UrLp [Driver.urlcounter]+Driver.Country_code));
					ROATFunctions.logintoROATeller("Super User");
				}
				else
				{
					Driver.driver.navigate().to(Driver.appURL);
					ROATFunctions.logintoROATeller("Super User");
					Driver.Country_code = "";
				}
			}
			else
			{
				if (Driver.EnvType.equals("ALL"))
				{
					WebDr.openApplication(Driver.UrLp [Driver.urlcounter]+Driver.Country_code);
					ROATFunctions.logintoROATeller("Super User");
				}
				else
				{
					WebDr.openApplication(Driver.appURL);
					ROATFunctions.logintoROATeller("Super User");
					Driver.Country_code = "";
				}
			}


			if(flag==1)
			{
				HtmlReporter.tc_result=false;
			}
		}
		catch(Exception e)
		{
			e.printStackTrace();

		}
	}

	//FCY sanity inquiry karishma
	public static boolean TellerPosition_TellInq(String PageObjectname,String tableID) throws Exception{
		WebDr.SetPageObjects(PageObjectname);
		String TellerPos=null;
		boolean TellerPosValue = false;
		try{			
			String table = WebDr.page_Objects.get(tableID);

			List<WebElement> rows = WebDr.getElements(tableID);

			boolean flag  = false;
			int RowSize=rows.size();
			System.out.println(RowSize);
			for (int i=1;i<=rows.size();i++)
			{

				WebElement row_value = WebDr.getElementObject(table+"["+i+"]");							

				flag = false;
				if (Integer.parseInt(row_value.getText())>0)
				{

					System.out.println("Teller position is greater than 0");
					flag = true;
					i++;
					TellerPos=row_value.getAttribute("innerText");	
					Reporter.ReportEvent("Sufficient teller position", "Sufficient teller position-successfully entered the teller postion count ", "Sufficient teller position-successfully entered the teller postion count",true);
					TellerPosValue=  true;

				}
				else
				{						

					System.out.println("Teller position is  0");
					//flag = true;
					TellerPos=row_value.getAttribute("innerText");
					Reporter.ReportEvent("InSufficient teller position", "InSufficient teller position", "InSufficient teller position",true);
					TellerPosValue=  false;

				}
			}
			if (flag)
			{
				Reporter.ReportEvent("All Notes values are entered", "All Notes values should be enter", "All Notes values are entered",true);
			}

			Robot robot =new Robot();

			robot.keyPress(KeyEvent.VK_TAB);
			robot.keyRelease(KeyEvent.VK_TAB);

		}
		catch(Exception e)
		{
			e.printStackTrace();
			System.out.println("Exeption in WebDr.setText - "+e);
			Reporter.ReportEvent("Object is not Found", "Object should be display on screen", "Object not present",false);
		}

		return TellerPosValue;

	}

	public static String fetch_user_login_id()
	{
		try
		{
		WebDr.SetPageObjects("MasterPage");
		String usersession=WebDr.getElement("user_session").getAttribute("value");
		int start=usersession.indexOf("loginId")+10;
		int end=start+7;
		String login=(String) usersession.subSequence(start,end);
		System.out.println(login);
		return login;}
	catch(Exception e)
	{
		Reporter.ReportEvent("To fetch user branch", "must be able to fetch user branch", "unable to fetch user branch",false);
		return null;
	}
	}

	public static String fetch_user_Id()
	
	{String login;
		try{
		if(WebDr.getElement("user_session").isDisplayed()){
		WebDr.SetPageObjects("MasterPage");
		String usersession=WebDr.getElement("user_session").getAttribute("value");
		int start=usersession.indexOf("userId")+8;
		int end=start+3;
		 login=(String) usersession.subSequence(start,end);
		System.out.println(login);
		return login;}
	else{
		login="0";
		return login;
	}
	}
	catch(Exception e)
	{
		Reporter.ReportEvent("To fetch user id", "must be able to fetch user id", "unable to fetch user branch",false);
		return null;
	}
	}

//



	//Cash drawer System Date Time validation code here...
	public static void CD_SysDateValidation(String pageObject, String xpath) throws Exception {
		String SysDate=null;
		String CD_Date=null;
		try {

			WebDr.SetPageObjects(pageObject);
			if (HtmlReporter.tc_result==true){
				CD_Date=WebDr.page_Objects.get(xpath);              
				DateFormat dateFormat = new SimpleDateFormat("dd-MM-yyyy HH:mm");
				Date date = new Date();
				String CurrentDate = dateFormat.format(date);
				SysDate = WebDr.getElementObject(CD_Date)
						.getAttribute("innerText");
				Date sys = new SimpleDateFormat("dd-MM-yyyy HH:mm").parse(SysDate);

				DateFormat dateFormat2 = new SimpleDateFormat("dd-MM-yyyy HH:mm");
				String sysD = dateFormat2.format(sys);



				if (sysD.equalsIgnoreCase(CurrentDate)) {
					Reporter.ReportEvent("Successfully verified system data and time with application date and time",
							"Verify:" + SysDate + "", "Verified:" + CurrentDate + "", true);

				} else {
					Reporter.ReportEvent("Unable to verify system data and time with application date and time",
							"Verify:" + SysDate + "", "Verified:" + dateFormat + "", false);
					//Reporter.ReportBug("Application date should be equal to system date format ","Application date should be  present");
				}
			}                    
		}
		catch (Exception e) {
			Reporter.ReportEvent("Unable to verify system data and time with application date and time",
					"Verify:" + SysDate + "", "Verified:" + e + "", false);
			//Reporter.ReportBug("Application date should be equal to system date format ","Application date should be  present");
		}

	}
	//ROATFunctions.logintoROATeller(role,username,password);
	public static void refesh(boolean ismandatory,String role,String username,String password)
	{

		boolean tc_result=HtmlReporter.tc_result;
		HtmlReporter.tc_result=true;
		for(int i=0;i<3;i++)//
		{
			WebDr.SetPageObjects("MasterPage");

			if(!(WebDr.isExists_Display("profile_name")>0))
			{
				ismandatory=true;
				Launcher.UserRole_Last="0";
				break;
			}
		}
		try{
			WebDr.SetPageObjects("MasterPage");
			if(Launcher.login_result){
				if(ismandatory)
				{
					if(!Launcher.UserRole_Last.equals("0"))
					{
						//				WebDr.clickX("X", "logout_button", "Clicking logout menu", true);
						WebDr.waitTillElementIsDisplayed("logout_button");
						WebDr.clickX("X", "logout_button", "click on logout button", true);
						WebDr.waitTillElementIsDisplayed("logout_confirm");
						JavascriptExecutor js  = (JavascriptExecutor) Driver.driver;	
						WebElement koken = WebDr.getElementObject("XPATH|//*[@id='logoutDialog']/div");

						js.executeScript("arguments[0].style.opacity ='1'", koken);
						WebDr.clickX("X", "logout_confirm", "click on logout confirm button", true);
						Driver.driver.quit();
						WebDr.openApplication(Driver.UrLp [Driver.urlcounter]+Driver.Country_code);
					}

					if (Driver.EnvType.equals("ALL"))
					{
						//rhea added
						Driver.driver.quit();
						WebDr.openApplication(Driver.appURL);
						//
						Driver.driver.navigate().to((Driver.appURL));
						ROATFunctions.logintoROATeller(role,username,password);
					}
					else
					{
						//rhea added
						Driver.driver.quit();
						WebDr.openApplication(Driver.appURL);
						//
						Driver.driver.navigate().to(Driver.appURL);
						ROATFunctions.logintoROATeller(role,username,password);
						Driver.Country_code = "";
					}
				}
				else
				{
					if (!(Launcher.UserRole_Last.equals(role)))
					{
						if(!Launcher.UserRole_Last.equals("0"))
						{
							//					WebDr.clickX("X", "profile_name", "Clicking logout menu", true);
							//					WebDr.waitTillElementIsDisplayed("profile_name");
							//					WebDr.clickX("X", "logout_button", "click on logout button", true);
							//					WebDr.waitTillElementIsDisplayed("logout_button");
							//					JavascriptExecutor js  = (JavascriptExecutor) Driver.driver;
							//					WebElement koken = WebDr.getElementObject("XPATH|//*[@id='logoutDialog']/div");
							//					
							//					js.executeScript("arguments[0].style.opacity ='1'", koken);
							//					WebDr.clickX("X", "logout_confirm", "click on logout confirm button", true);
							WebDr.waitTillElementIsDisplayed("logout_button");
							WebDr.clickX("X", "logout_button", "click on logout button", true);
							WebDr.waitTillElementIsDisplayed("logout_confirm");
							JavascriptExecutor js  = (JavascriptExecutor) Driver.driver;	
							WebElement koken = WebDr.getElementObject("XPATH|//*[@id='logoutDialog']/div");

							js.executeScript("arguments[0].style.opacity ='1'", koken);
							WebDr.clickX("X", "logout_confirm", "click on logout confirm button", true);
							Driver.driver.quit();
							WebDr.openApplication(Driver.UrLp [Driver.urlcounter]+Driver.Country_code);

						}

						if (Driver.EnvType.equals("ALL"))
						{
							Driver.driver.navigate().to(Driver.UrLp [Driver.urlcounter]+Driver.Country_code);
							ROATFunctions.logintoROATeller(role,username,password);
						}
						else
						{
							Driver.driver.navigate().to(Driver.appURL);
							ROATFunctions.logintoROATeller(role,username,password);
							Driver.Country_code = "";
						}
					}



				}

				Launcher.UserRole_Last = role;
			}
			else
			{

				Reporter.ReportEvent("login error", "Login screen should be available",  Launcher.login_error, false);

			}
		}

		catch(Exception e)
		{
			e.printStackTrace();

		}
		HtmlReporter.tc_result=tc_result;

	}

	public static void logintoROATeller(String role,String userName,String Password) throws InterruptedException
	{
		try
		{
			try
			{
				//DatabaseFactory.update_User_Session(Driver.Country_code,"0",Driver.Env_IP);//rhea added comment
				DatabaseFactory.update_User_Session(CommonUtils.readConfig("Country_code"),"0",Driver.Env_IP);
				Driver.driver.manage().window().maximize();
			}
			catch(Exception e)
			{
				e.printStackTrace();
				Reporter.ReportEvent("session query failed", "session query failed","session query failed" , false);
				HtmlReporter.tc_result=true;
			}
			Driver.DB_Flag = "N";

			WebDr.SetPageObjects("MasterPage");
			//  if (HtmlReporter.tc_result==true) WebDr.click("X", "username", "Clicking on username text", true);

			String Login[] = new String[2];
			Login[0]=userName;
			Login[1]=Password;
			int flag=0;
			for(int i=0;i<5;i++)
			{

				if(WebDr.getElement("headingLoginError").getText().contains("Application Error while service communication"))
				{
					Launcher.login_error=WebDr.getElement("headingLoginError").getText();
					Reporter.ReportEvent("login error", "Login screen should be available ", Launcher.login_error+"'", false);
					Launcher.login_result=false;
					Driver.DB_Flag = "Y";
					flag=1;
					break;
				}
				if(WebDr.isExists_Display("username")>0)
				{
					flag=1;
					break;
				}
				Thread.sleep(1000);
			}
			if(flag==0)
			{
				Reporter.ReportEvent("login error", "Login screen should be available ", "Login screen is not available ", false);
				Launcher.login_result=false;
			}
			if(Launcher.login_result)
			{
				if(HtmlReporter.tc_result) WebDr.setText("X", "username", Login[0], "Entering data in Username field", true);

				if(HtmlReporter.tc_result)  WebDr.click("X", "Login", "Clicking on login button", true);

				flag=0;
				for(int i=0;i<5;i++)
				{
					if(WebDr.isExists_Display("Password")>0)
					{ flag=1;
					break;
					}
					Thread.sleep(1000);

				}
				if(flag==0&&!WebDr.getElement("headingLoginError").getText().isEmpty()&&!WebDr.getElement("headingLoginError").getText().contains("ActiveX is not enable on browser.'"))
				{ Launcher.login_error=WebDr.getElement("headingLoginError").getText();
				Reporter.ReportEvent("login error", "Login screen should be available ", Launcher.login_error+"'", false);
				Launcher.login_result=false;
				Driver.DB_Flag = "Y";
				flag=1;
				}
				else if(flag==0)
				{
					Reporter.ReportEvent("login error", "Login screen should be available ", " DB connection failed application not loaded", false);
					Launcher.login_result=false;
					Driver.DB_Flag = "Y";
					flag=1;
				}
				if(Launcher.login_result)
				{

					if(HtmlReporter.tc_result)   WebDr.setText("X", "Password", Login[1], "Entering data in password field", true);

					if(HtmlReporter.tc_result)  WebDr.click("X", "Login", "Clicking on login button", true);
					for(int i=0;i<5;i++)
					{

						if(WebDr.getElement("headingLoginError").getText().contains("LDAP Connection failed!")&&!WebDr.getElement("headingLoginError").getText().contains("ActiveX is not enable on browser.'"))
						{ 
							Launcher.login_error=WebDr.getElement("headingLoginError").getText();
							Reporter.ReportEvent("login error", "Login screen should be available ", Launcher.login_error+"'", false);
							Launcher.login_result=false;
							flag=1;
							Driver.DB_Flag = "Y";


							break;
						}
						else if(WebDr.isExists_Display("dropdown_role")>0)
						{
							break;
						}
						Thread.sleep(1000);
					}
					if(Launcher.login_result)
					{

						if(HtmlReporter.tc_result) WebDr.waitTillElementIsDisplayed("dropdown_role");

						if(HtmlReporter.tc_result)  WebDr.select("X", "dropdown_role", "VisibleText",role, "Select role", true);

						if(HtmlReporter.tc_result)  WebDr.click("X", "Login", "Clicking on login button", true);

						flag=0;
						for(int i=0;i<10;i++)
						{
							if(WebDr.isExists("labelMenu")>0)
							{ 
								flag=1;
								break;
							}
							Thread.sleep(1000);

						}
						if(flag==0&&!WebDr.getElement("headingLoginError").getText().isEmpty()&&!WebDr.getElement("headingLoginError").getText().contains("ActiveX is not enable on browser.'"))
						{
							Launcher.login_error=WebDr.getElement("headingLoginError").getText();
						Reporter.ReportEvent("login error", "Login screen should be available ", Launcher.login_error+"'", false);
						Launcher.login_result=false;
						Driver.DB_Flag = "Y";
						flag=1;
						}
						else if(flag==0)
						{
							Reporter.ReportEvent("login error", "Login screen should be available ", " DB connection failed application not loaded", false);
							Launcher.login_result=false;
							Driver.DB_Flag = "Y";
							flag=1;

						}
						else if(flag==1)
						{
						//	check_current_system_date(role);
						}
					}
				}
			}
		}

		catch(Exception e)

		{
			e.printStackTrace();

			Reporter.ReportEvent("login error", "login button should click", "Error!:- Page not Found 404", false);
			Launcher.login_error="Error!:- Page not Found 404";
			Launcher.login_result=false;

		}

	}

	///*****************sanity********************************************
	public static void TellerPosition_Count(String PageObjectname,String tableID,String textboxID, String Soiled, String Decoy, String TotalAmount, String CashAmount) throws Exception{
		WebDr.SetPageObjects(PageObjectname);
		try{			
			String table = WebDr.page_Objects.get(tableID);
			String txt = WebDr.page_Objects.get(textboxID);

			List<WebElement> rows = WebDr.getElements(tableID);
			int j = 1;
			boolean flag  = false;
			int RowSize=rows.size();
			System.out.println(RowSize);
			for (int i=1;i<=rows.size();i++)
			{

				WebElement row_value = WebDr.getElementObject(table+"["+i+"]//span");							

				flag = false;
				System.out.println(row_value.getText());
				if (Integer.parseInt(row_value.getAttribute("innerText"))>0)
				{
					WebDr.setText("CUSTOM", txt+"["+j+"]", "1", "Enter value in text field", true);

					j = j +1;
					flag = true;
				}
				else
				{						
					WebDr.setText("CUSTOM", txt+"["+j+"]", "0", "Enter value in text field", true);

					j = j +1;
					flag = true;
				}
			}
			if (flag)
			{
				Reporter.ReportEvent("All Notes values are entered", "All Notes values should be enter", "All Notes values are entered",true);
			}

			String Amount = WebDr.getValue("Varify_Text");	
			String labelAmt = WebDr.page_Objects.get("label_TotalAmt");
			if (HtmlReporter.tc_result==true)WebDr.setText("X", Soiled, "1", "Enter soiled Notes", true);
			if (HtmlReporter.tc_result==true)WebDr.setText("X", Decoy, "1", "Enter Decoy Coins", true);//WebDr.getValue("Positive_Flow_Currency")

			ROATFunctions.clickTab();
			ROATFunctions.clickTab();
		}
		catch(Exception e)
		{
			e.printStackTrace();
			System.out.println("Exeption in WebDr.setText - "+e);
			Reporter.ReportEvent("Object is not Found", "Object should be display on screen", "Object not present",false);
		}

	}
	///**************ldap and authorization ***************************************************

	public static void ldap_auth_field_check(String PageObjectsPage, String UserID)
	{
		try {
			if (HtmlReporter.tc_result==true)WebDr.SetPageObjects(PageObjectsPage);
			if (HtmlReporter.tc_result==true)WebDr.waitTillElementIsDisplayed(UserID);
			if (HtmlReporter.tc_result==true)WebDr.setText("X", UserID,"123", "Entering Authorizer username", true);
			if (HtmlReporter.tc_result==true)ROATFunctions.clickTab();
			if (HtmlReporter.tc_result==true)WebDr.SetPageObjects("LdapAuth");
			if (HtmlReporter.tc_result==true)WebDr.waitTillElementIsDisplayed("tooltip");
			if (HtmlReporter.tc_result==true)WebDr.verifyText("X", "tooltip", true,"Minimum length required is 7 bytes", "Checcking for error message", true);
			if (HtmlReporter.tc_result==true)WebDr.SetPageObjects(PageObjectsPage);
			if (HtmlReporter.tc_result==true)WebDr.setText("X", UserID,"1234567", "Entering Authorizer username", true);
			if (HtmlReporter.tc_result==true)ROATFunctions.clickTab();
			if (HtmlReporter.tc_result==true)WebDr.SetPageObjects("LdapAuth");
			if (HtmlReporter.tc_result==true)WebDr.waitTillElementIsDisplayed("tooltip");
			if (HtmlReporter.tc_result==true)WebDr.verifyText("X", "tooltip", true,"User not Found", "Checcking for error message", true);


		} catch (Exception e) {
			e.printStackTrace();

		}
	}

	public static String fetch_user_branch_name()
	{try{
		WebDr.SetPageObjects("MasterPage");
		String usersession=WebDr.getElement("branch_number").getText();
		String branch=(String) usersession.subSequence(4,usersession.length());
		System.out.println(branch);
		return branch;
		}
	catch(Exception e)
	{
		Reporter.ReportEvent("To fetch user branch", "must be able to fetch user branch", "unable to fetch user branch",false);
		return null;
	}
	}
	public static String fetch_user_primay_currency()
	{try{
		WebDr.SetPageObjects("MasterPage");
		String usersession=WebDr.getElement("user_session").getAttribute("value");
		String[] values=usersession.split(",");
		String currency="";
		for(int i=0;i<values.length;i++)
		{
			if(values[i].contains("countryPrimaryCurrency"))
			{
				currency=values[i].split(":")[1];
				currency=currency.substring(1,currency.length()-1);
				break;
			}
		}

		System.out.println(currency);
		return currency;}
	catch(Exception e)
	{
		Reporter.ReportEvent("To fetch user branch", "must be able to fetch user branch", "unable to fetch user branch",false);
		return null;
	}
	}
	
//fetch current login currency
	public static String fetch_userCurrent_primay_currency()
	{try{
		
		WebDr.SetPageObjects("LCYPayment");

		if (HtmlReporter.tc_result)ROATModules.menu_navigation("MasterPage", "tab_CashDrawer", "tab_CashDrawer_NotToClose");
		if (HtmlReporter.tc_result)ROATModules.menu_navigation1("MasterPage", "tab_LCY", "tab_LCY_closed");
		if (HtmlReporter.tc_result)ROATModules.menu_navigation1("MasterPage", "tab_LCY_payment", "null");
		WebDr.Wait_Time(3000);
		String PCurrency= WebDr.getElement("Pcurrency").getText();		
		return PCurrency;
		}
	catch(Exception e)
	{
		Reporter.ReportEvent("To fetch user branch", "must be able to fetch user branch", "unable to fetch user branch",false);
		return null;
	}
	}

	public static void clickDownArrow(String xpath) throws InterruptedException
	{
		WebElement element = Driver.driver.findElement(By.xpath(WebDr.page_Objects.get(xpath)));
		((JavascriptExecutor) Driver.driver).executeScript("arguments[0].scrollIntoView(true);", element);
		Thread.sleep(500); 
	}

	public static void cmscheck() 
	{
		try
		{
			if (HtmlReporter.tc_result==true)
			{
				for(int i=1; i<20; i++){
					String verification;
					if(WebDr.getValue("Clearrep"+i).equals(""))
					{////System.out.println("here");
						break;
					}
					else

						verification = WebDr.getValue("Clearrep" +i);
					WebElement oElm = WebDr.getElement("main_page");
					if((oElm.getText().contains(verification)))
					{
						Reporter.ReportEvent("Clearing_Reports ", "Verify CMS:"+verification+"", "CMS value "+verification+"  present", true);
					}
					else
					{
						Reporter.ReportEvent("Clearing_Reports ", "Verify CMS:"+verification+"", "CMS value "+verification+" not present", false);
					}			
				}
			}

			//WebDr.click("X", "generate_button", "Clicking on generate button", true);
		}
		catch(Exception e)
		{
			e.printStackTrace();
			Reporter.ReportEvent("Clearing_Reports  ", "Checking cms of Account Inquiry ","Unable to check cms of Account Inquiry ", false);
		}
	}

	public static void p_dropdown(String Dropdown, String Searchbox, String DD_values, String Inputvalue) throws Exception
	{

		//WebDr.waitTillElementIsDisplayed(WebDr.getElement(WebDr.page_Objects.get(Dropdown)), 2000);
		WebDr.click("X",Dropdown, "Click on dropdown", true);
		WebDr.setText("X", Searchbox,Inputvalue, "select the teller", true);
		WebDr.click("X", DD_values, "click", true);


	}
	public static void p_dropdownX(String Dropdown, String Searchbox, String DD_values, String Inputvalue) throws Exception
	{

		//WebDr.waitTillElementIsDisplayed(WebDr.getElement(WebDr.page_Objects.get(Dropdown)), 2000);
		WebDr.click("X",Dropdown, "Click on dropdown", true);
		WebDr.setText("X", Searchbox,Inputvalue, "select the teller", true);
		WebDr.clickX("X", DD_values, "click", true);


	}
	
	 public static String fetchuser_role()
     {{try{
 		WebDr.SetPageObjects("MasterPage");
 		String usersession=WebDr.getElement("user_role").getText();
 		String branch=usersession.trim();
 		System.out.println(branch);
 		return branch;
 		}
 	catch(Exception e)
 	{
 		Reporter.ReportEvent("To fetch user branch", "must be able to fetch user branch", "unable to fetch user branch",false);
 		return null;
 	}
 	}}
	 
	 public static void DropdownNew(String Dropdown, String Searchbox, String DD_values, String Inputvalue)  throws Exception // DO NOT MO
	   	 	{
	 
			 //WebDr.waitTillElementIsDisplayed(WebDr.getElement(WebDr.page_Objects.get(Dropdown)), 2000);
			 WebDr.click("X",Dropdown, "Click on dropdown", true);
			 WebDr.Wait_Time(300);
			 WebDr.setText("X", Searchbox,Inputvalue, "select the value", true);
			 WebDr.Wait_Time(300);
			 WebDr.click("CUSTOM", WebDr.page_Objects.get(DD_values)+Inputvalue+"')]", "click", true);

			} 
	 public static void wait_till_load()
	 {
		 try
			{

				List<WebElement> lod=WebDr.getElements_custome("XPATH|//div[@class='loading']");
				for(int i=1;i<=lod.size();i++)
				{
					for(int k=0;k<20;k++)
					{
						if(WebDr.getElementObject("XPATH|(//div[@class='loading'])["+i+"]").isDisplayed())
						{
							WebDr.Wait_Time(1000);
						}
						else
						{
							break;	
						}
					}
				}
			}
			catch(Exception e)
			{
			e.printStackTrace();
			}
	 }
}//class close