package roatResuableModule;

import java.math.BigDecimal;
import java.sql.SQLException;
import java.time.LocalDate;
import java.time.format.DateTimeFormatter;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Random;
import java.util.Set;
import java.util.stream.Collectors;

import utility.CommonUtils;
import utility.DatabaseFactory;
import utility.HtmlReporter;
import utility.Launcher;
import utility.Reporter;
import utility.WebDr;
import org.apache.commons.lang.RandomStringUtils;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.WebElement;

import testCases.Driver;

public class ROATNTBFLows {
	public static boolean flag;
	public static String forex_gl__number, local_gl__number;


	//*********************************************health***********************************
	public static void Health_CustomerSearch() 
	{
		try 
		{
			ROATFunctions.refesh(false, Launcher.UserRole);
			if (HtmlReporter.tc_result)ROATModules.CS_navigateCustomerSearch();
			if (HtmlReporter.tc_result)WebDr.SetPageObjects("CustomerSearchPage");
			if (HtmlReporter.tc_result)WebDr.waitTillElementIsDisplayed("bancodeclick");
			if (HtmlReporter.tc_result)ROATModules.HCS_cmscheck();
			if (HtmlReporter.tc_result)ROATModules.Health_customerDeatilSearch();
			if(HtmlReporter.tc_result)ROATModules.CS_customerSearchSorting();
			if (HtmlReporter.tc_result)ROATModules.PaginationValidation("CustomerSearchPage","PaginationTop");
			if (HtmlReporter.tc_result)ROATModules.PaginationValidation("CustomerSearchPage","PaginationBottom");
		}
		catch (Exception e) 
		{
			Reporter.ReportEvent("Test Case execution is failed", "Test Case execution is failed","Test Case execution is failed", false);
		}

	}
	public static void Health_AccountInquiry()
	{
		try
		{
			ROATFunctions.refesh(false, Launcher.UserRole);
			if (HtmlReporter.tc_result)ROATModules.AI_navigateAccountInquiry();
			if (HtmlReporter.tc_result)ROATModules.HAI_cmscheck();
			if (HtmlReporter.tc_result)ROATModules.Health_dropdowndetails();
			if (HtmlReporter.tc_result)ROATModules.Health_searchAccountNumber();//////
			if (HtmlReporter.tc_result)ROATModules.AI_validateExpandCollapse();
			if (HtmlReporter.tc_result)ROATModules.AI_clearSearch();
		} 
		catch (Exception e)
		{
			Reporter.ReportEvent("Test Case execution is failed", "Test Case execution is failed","Test Case execution is failed", false);
		}

	}

	//************************************sanity*********************
	@SuppressWarnings("unused")
	public static void lycReceiptImpact()
	{
		try 
		{
			long balance=0 ,current_cash_position=0; int notification_counter_before_breach=0;
			try
			{
				ROATFunctions.refesh(true,Launcher.UserRole);
				if (HtmlReporter.tc_result) {String user_id = ROATFunctions.fetch_user_login_id();
				String user_rol=ROATFunctions.fetch_user_Id();
				if (HtmlReporter.tc_result){String user_id_name=user_id+"-"+ROATModules.fetchuser();
				if (HtmlReporter.tc_result) {String user_branch = ROATModules.fetch_user_branch();
				if (HtmlReporter.tc_result){String range=ROATModules.lcy_payments();	
				ROATFunctions.refesh(false, "Head Teller");
				if (HtmlReporter.tc_result)ROATModules.CD_navigateTeller_Inquiry();
				if (HtmlReporter.tc_result)ROATModules.selectTeller(user_id_name);
				if (HtmlReporter.tc_result){balance = ROATModules.balanceInquiry();
				if (HtmlReporter.tc_result){current_cash_position= ROATModules.balanceVerification(balance);
				Map<String, String> amount = ROATModules.storevalues("impact_lcy_payment", "tellerinquiry");
				Map<String, String> tellerPosition = ROATModules.storeTellerPosition("impact_lcy_payment", "tellerposition");
				if (HtmlReporter.tc_result)ROATFunctions.refesh(false, "Super User");
				if (HtmlReporter.tc_result)ROATModules.CD_navigateCashDrawer("tab_Sub_CashDrawer");
				if (HtmlReporter.tc_result)ROATModules.CD_navigateViewStatus();
				if (HtmlReporter.tc_result)ROATModules.CD_btnBack();
				String range_limit=String.valueOf((Integer.parseInt(range.replace(",", "").replace(".", "")))/100);
				if (HtmlReporter.tc_result)ROATModules.set_user_limits(user_branch, user_rol,String.valueOf(current_cash_position/100),range_limit,"impact_lcy_payment");
				//				if (HtmlReporter.tc_result)ROATModules.set_user_limits(user_branch, user_id,String.valueOf(current_cash_position%100),range);


				if (HtmlReporter.tc_result)ROATFunctions.refesh( true, "Teller");

				notification_counter_before_breach = ROATModules.get_counter_value();

				if (HtmlReporter.tc_result)ROATFunctions.refesh( true, "Teller");
				if (HtmlReporter.tc_result)ROATModules.lcy_receipt_authorise("1",range);
				if (HtmlReporter.tc_result)ROATFunctions.refesh(true,"Head Teller");
				if (HtmlReporter.tc_result)ROATModules.CD_navigateTeller_Inquiry();
				if (HtmlReporter.tc_result)ROATModules.selectTeller(user_id_name);

				System.out.println(balance+Long.parseLong(range.replaceAll(",", "").replace(".", "")));
				if(balance+Long.parseLong(range.replaceAll(",", "").replace(".", ""))== ROATModules.balanceInquiry())
					Reporter.ReportEvent("Test Case execution is pass", "balance updates on teller inquiry page", "balance updates on teller inquiry page",true);
				else
					Reporter.ReportEvent("Test Case execution is failed", "balance updates on teller inquiry page", "balance is not updated on teller inquiry page",false);

				if(current_cash_position+Long.parseLong(range.replaceAll(",", "").replace(".", ""))==ROATModules.balanceVerification(balance))
					Reporter.ReportEvent("Test Case execution is pass", "sum of subtotal updates on teller inquiry page", "sum of subtotal updates on teller inquiry page",true);
				else
					Reporter.ReportEvent("Test Case execution is failed", "sum of subtotal updates on teller inquiry page", "sum of subtotal is not updated on teller inquiry page",false);

				long abc = Long.parseLong(amount.get("2"))+Long.parseLong(range.replaceAll(",", "").replace(".", ""));
				System.out.println(Long.parseLong(amount.get("2"))+Long.parseLong(range.replaceAll(",", "").replace(".", "")));
				if(Long.parseLong(amount.get("2"))+Long.parseLong(range.replaceAll(",", "").replace(".", ""))== Long.parseLong(ROATModules.storevalues("impact_lcy_payment", "tellerinquiry").get("2")))
					Reporter.ReportEvent("Test Case execution is pass", "sum of subtotal updates on teller inquiry page", "sum of subtotal updates on teller inquiry page",true);
				else
					Reporter.ReportEvent("Test Case execution is failed", "sum of subtotal updates on teller inquiry page", "sum of subtotal is not updated on teller inquiry page",false);
				}	
				if (HtmlReporter.tc_result)ROATFunctions.refesh( true, "Teller");
				if (HtmlReporter.tc_result)ROATModules.lcy_receipt_authorise("2",String.valueOf(2*Double.parseDouble(range.replace(",", ""))));
				if (HtmlReporter.tc_result)ROATFunctions.refesh( true, "Teller");

				if (HtmlReporter.tc_result){int notification_counter_after_breach=ROATModules.get_counter_value();
				if(notification_counter_after_breach == notification_counter_before_breach+4)
				{
					Reporter.ReportEvent("Notification counter is not increamented", "Notification counter is to be increamented","Notification counter is increamented",true);
					//					if(HtmlReporter.tc_result)ROATModules.opn_not_area();
					//					if(HtmlReporter.tc_result)ROATModules.check_notification_msg("Max_lcy");
					//					ROATModules.opn_not_area();
				}
				else 
				{
					Reporter.ReportEvent("Notification  is generated", "Notification  is generated","Notification  is not generated",true);
				}
				}
				}
				}
				}
				}
				}
			}
			catch (Exception e)
			{
				e.printStackTrace();
				Reporter.ReportEvent("Test Case execution is failed", "Test Case execution is failed","Test Case execution is failed", false);
			}


		}
		catch (Exception e) 
		{
			e.printStackTrace();
			Reporter.ReportEvent("Test Case execution is failed", "Test Case execution is failed","Test Case execution is failed", false);
		}
	}

	@SuppressWarnings("unused")
	public static void SanitySuite_SetupLimits()
	{
		try
		{
			if (HtmlReporter.tc_result)ROATFunctions.refesh(false, Launcher.UserRole);
			boolean Role = false;
			List<HashMap<String, String>> Limit = null; 

			if (HtmlReporter.tc_result)
			{
				Role = ROATModules.verifyUserRole(WebDr.getValue("Role"));
			}
			if (HtmlReporter.tc_result)ROATModules.setup_navigateLTLimits();
			WebDr.Wait_Time(1000);
			if (HtmlReporter.tc_result)
			{
				Limit = ROATModules.verifyLimitValues();
			}
		}
		catch(Exception e)
		{
			Reporter.ReportEvent("Setup Limits Sanity", "Setup Limits Sanity failed", "Setup Limits Sanity failed", false);
		}
	}
	public static boolean SanitySuite_CashDrawerUser()
	{
		boolean Role = false, CurrencySetup = false, BranchSetup = false, User=false;
		try
		{
			if (HtmlReporter.tc_result)ROATFunctions.refesh(false, Launcher.UserRole);
			if (HtmlReporter.tc_result)
			{
				Role = ROATModules.verifyUserRole(WebDr.getValue("Role"));
			}
			if (HtmlReporter.tc_result)ROATModules.setup_navigateCurrency();//////////////////////////////////////////
			if (HtmlReporter.tc_result)
			{
				CurrencySetup = ROATModules.verifyCurrencySetup(WebDr.getValue("Currency"));
			}
			if (HtmlReporter.tc_result)ROATModules.setup_navigateLTLimitsBranch();/////////////////////////////////////
			if (HtmlReporter.tc_result)
			{
				BranchSetup = ROATModules.verifyBranchSetup(WebDr.getValue("Branch"));
			}
			if (HtmlReporter.tc_result)
			{
				if(Role && CurrencySetup && BranchSetup)
				{
					if (HtmlReporter.tc_result)
					{
						User = ROACDFlows.cash_Drawer_User1();
					}
				}
			}
			if (HtmlReporter.tc_result==true)ROATModules.CD_tooltip_verification();
		}
		catch(Exception e)
		{
			Reporter.ReportEvent("Cash Drawer User Sanity", "Cash Drawer User Sanity failed", "Cash Drawer User Sanity failed", false);
		}
		return User;
	}

	@SuppressWarnings("unused")
	public static void SanitySuite_VaultLCYReceiptFrmCDC()
	{
		try
		{
			boolean Role = false;
			boolean accStatus = false, sodStatus = false, ocpStatus = false, aml = false, status = false;
			if (HtmlReporter.tc_result)ROATFunctions.refesh(false, Launcher.UserRole);
			if (HtmlReporter.tc_result)
			{
				Role = ROATModules.verifyUserRole(WebDr.getValue("Role"));
			}
			if (HtmlReporter.tc_result)ROATFunctions.refesh(false, "Supervisor");
			if (HtmlReporter.tc_result)ROATModules.navigateSOD();

			if (HtmlReporter.tc_result) { sodStatus = ROATModules.verifySOD();
			System.out.println("SOD Status : " +sodStatus);
			}
			if (HtmlReporter.tc_result)ROATFunctions.refesh(false, "Teller");
			if (HtmlReporter.tc_result)ROATModules.navigateOCP();
			if (HtmlReporter.tc_result)
			{ 
				ocpStatus = ROATModules.verifyOCP();
				System.out.println("OCP Status : " +ocpStatus);
			}

			if (HtmlReporter.tc_result)
			{
				if(Role && sodStatus && ocpStatus)
				{
					status = true;
					try 
					{
						ROATFunctions.refesh(false,"Vault Custodian");
						if (HtmlReporter.tc_result)ROATModules.Navigate_VaultLCYReceiptCDC();
						if (HtmlReporter.tc_result)ROATModules.ViewLCYReceiptCDC();
						if (HtmlReporter.tc_result)ROATModules.Vault_LCYReceiptFromCDCReject();
						if (HtmlReporter.tc_result)ROATModules.Vault_LCYReceiptFromCDCCancel();
						if (HtmlReporter.tc_result)ROATModules.Vault_LCYReceiptFromCDCAuthorize();
					}
					catch (Exception e) 
					{
						Reporter.ReportEvent("Test Case execution is failed", "Test Case execution is failed","Test Case execution is failed", false);
					}
				}
			}
		}
		catch(Exception e)
		{
			Reporter.ReportEvent("Vault LCY Receipt from CDC Sanity", "Vault LCY Receipt from CDC Sanity failed", "Vault LCY Receipt from CDC Sanity failed", false);
		}
	}

	@SuppressWarnings("unused")
	public static void SanitySuite_VaultFCYReceiptFrmCDC()
	{
		try
		{
			boolean Role = false;
			boolean accStatus = false, sodStatus = false, ocpStatus = false, aml = false, status = false;

			//			if (HtmlReporter.tc_result)ROATFunctions.refesh(false, Launcher.UserRole);
			//			if (HtmlReporter.tc_result)ROATModules.AI_navigateAccountInquiry();
			//			if (HtmlReporter.tc_result)ROATModules.AI_searchAccountNumber();
			//			if (HtmlReporter.tc_result)
			//			{
			//				Role = ROATModules.verifyUserRole(WebDr.getValue("Role"));
			//			}
			//			if (HtmlReporter.tc_result)ROATFunctions.refesh(false, "Supervisor");
			//			if (HtmlReporter.tc_result)ROATModules.navigateSOD();
			//			if (HtmlReporter.tc_result) 
			//			{ 
			//				sodStatus = ROATModules.verifySOD();
			//				System.out.println("SOD Status : " +sodStatus);
			//			} 
			//			if (HtmlReporter.tc_result)ROATFunctions.refesh(false, "Teller");
			//			if (HtmlReporter.tc_result)ROATModules.navigateOCP();
			//			if (HtmlReporter.tc_result)
			//			{ 
			//				ocpStatus = ROATModules.verifyOCP();
			//				System.out.println("OCP Status : " +ocpStatus);
			//			}

			if (HtmlReporter.tc_result)
			{
				//				if(Role && sodStatus && ocpStatus)
				{
					status = true;
					try
					{
						ROATFunctions.refesh(false, "Vault Custodian");
						if (HtmlReporter.tc_result)ROATModules.Navigate_VaultFCYReceiptfromCDC();
						if (HtmlReporter.tc_result)ROATModules.ViewFCYReceiptCDC();
						if (HtmlReporter.tc_result)ROATModules.Vault_FCYReceiptFromCDCReject();
						if (HtmlReporter.tc_result)ROATModules.Vault_FCYReceiptFromCDCCancel();
						if(HtmlReporter.tc_result)ROATModules.Vault_FCYReceiptfromCDCAuthorize();
					}
					catch (Exception e)
					{
						Reporter.ReportEvent("Test Case execution is failed", "Test Case execution is failed","Test Case execution is failed", false);
					}
				}
			}
		}
		catch(Exception e)
		{
			Reporter.ReportEvent("Vault LCY Receipt from CDC Sanity", "Vault LCY Receipt from CDC Sanity failed", "Vault LCY Receipt from CDC Sanity failed", false);
		}
	}

	@SuppressWarnings("unused")
	public static void SanitySuite_LCYPaymentToATM()  
	{		
		boolean Role = false;
		boolean accStatus = false, sodStatus = false, ocpStatus = false, aml = false, status = false;
		try
		{
			if (HtmlReporter.tc_result)ROATFunctions.refesh(false, Launcher.UserRole);
			if (HtmlReporter.tc_result)ROATModules.Prereq_AccInquiry();
			if (HtmlReporter.tc_result)ROATFunctions.refesh(false, "Supervisor");
			if (HtmlReporter.tc_result)ROATModules.Prereq_SOD();
			if (HtmlReporter.tc_result)ROATFunctions.refesh(false, "Teller");
			if (HtmlReporter.tc_result)ROATModules.navigateOCP();
			if (HtmlReporter.tc_result)
			{ 
				ocpStatus = ROATModules.verifyOCP();
				System.out.println("OCP Status : " +ocpStatus);	
				if (HtmlReporter.tc_result)ROATFunctions.refesh(false, "Head Teller");
				if (HtmlReporter.tc_result)ROATModules.CD_navigateTellerInquiry();
				if (HtmlReporter.tc_result)ROATModules.Prereq_SufficientTellerPos();
				if (HtmlReporter.tc_result)ROATFunctions.refesh(false, "Vault Custodian");
				if (HtmlReporter.tc_result)ROATModules.Impact_PaymentToATM();


				if (HtmlReporter.tc_result)
				{
					try
					{
						if (HtmlReporter.tc_result)ROATModules.navigate_VaultLCY_PaymentToTransfer();
						if (HtmlReporter.tc_result)ROATModules.VaultLCY_PaymentToTransfer_EnterInformation();
						if (HtmlReporter.tc_result)ROATModules.VaultLCY_PaymentToTransfer_CancelFunctionality();
						if (HtmlReporter.tc_result)ROATModules.VaultLCY_PaymentToTransfer_EnterInformation();
						if (HtmlReporter.tc_result)ROATFunctions.Vault_PaymentToATM_DenominationCount("Vault_LCY_PaymentToATM","Vault_LCY_PTA_DenomPosSize", "Vault_LCY_PTA_DenomCount", "CDA_TotalCashAmount","Vault_LCY_PTA_PaymentAmount");
						if (HtmlReporter.tc_result)ROATModules.setup_LCYAuthorize();
						if (HtmlReporter.tc_result)WebDr.SetPageObjects("Vault_LCY_PaymentToATM");
						if (HtmlReporter.tc_result)WebDr.waitTillElementIsDisplayed("success_msg");
						if (HtmlReporter.tc_result)ROATModules.VaultLCY_PaymentToTransfer_TotalPaymentVerification();
						if (HtmlReporter.tc_result)ROATModules.VaultLCY_PaymentToTransfer_EnterInformation();
						if (HtmlReporter.tc_result)ROATFunctions.Vault_PaymentToATM_DenominationCount("Vault_LCY_PaymentToATM",
								"Vault_LCY_PTA_DenomPosSize", "Vault_LCY_PTA_DenomCount", "CDA_TotalCashAmount","Vault_LCY_PTA_PaymentAmount");
						if (HtmlReporter.tc_result)ROATModules.setup_LCYReject();
					} 
					catch (Exception e)
					{
						Reporter.ReportEvent("Test Case execution is failed", "Test Case execution is failed","Test Case execution is failed", false);
					}
					if (HtmlReporter.tc_result)ROATModules.Impact_PaymentToATM();
				}	
			}
		}
		catch (Exception e)
		{
			Reporter.ReportEvent("Test Case execution is failed", "Test Case execution is failed","Test Case execution is failed", false);
			e.printStackTrace();
		}
	}	

	public static void SanitySuite_LCYInquiry()
	{								
		try 
		{
			if (HtmlReporter.tc_result)ROATFunctions.refesh(false, Launcher.UserRole);
			if (HtmlReporter.tc_result)ROATModules.Prereq_AccInquiry();
			if (HtmlReporter.tc_result)ROATFunctions.refesh(false, "Supervisor");
			if (HtmlReporter.tc_result)ROATModules.Prereq_SOD();
			if (HtmlReporter.tc_result)ROATFunctions.refesh(false, "Head Teller");
			if (HtmlReporter.tc_result)ROATModules.Prereq_OCP();
			if (HtmlReporter.tc_result)ROATModules.CD_navigateTellerInquiry();
			if (HtmlReporter.tc_result)ROATModules.Prereq_SufficientTellerPos();

			if (HtmlReporter.tc_result)
			{
				try 
				{
					if (HtmlReporter.tc_result)ROATModules.setup_navigateLCYInquiry();
					//	if (HtmlReporter.tc_result)
					//		ROATModules.setup_BalanceLCYVerification();
				}
				catch (Exception e) 
				{
					e.printStackTrace();
					Reporter.ReportEvent("Test Case execution is failed", "Test Case execution is failed","Test Case execution is failed", false);
				}

			}
		} 
		catch (Exception e)
		{
			e.printStackTrace();
			Reporter.ReportEvent("Test Case execution is failed", "Test Case execution is failed","Test Case execution is failed", false);
		}					
	}

	public static void SanitySuite_AccountInquiry() 
	{				
		try 
		{
			if (HtmlReporter.tc_result)ROATFunctions.refesh(false, Launcher.UserRole);
			if (HtmlReporter.tc_result)ROATModules.Prereq_AccInquiry();							
			if (HtmlReporter.tc_result)
			{							
				if (HtmlReporter.tc_result)ROATModules.AI_navigateAccountInquiry();
				if (HtmlReporter.tc_result)WebDr.SetPageObjects("AccountInquiryPage");
				if (HtmlReporter.tc_result)WebDr.clickX("X","bancodeclick", "Clicking on drpdown",true);
				if (HtmlReporter.tc_result)ROATModules.HAI_cmscheck();
				if (HtmlReporter.tc_result)ROATModules.AHI_dropdowndetails();
				//if (HtmlReporter.tc_result)
				//	ROATModules.AI_navigateAccountInquiry();
				//if (HtmlReporter.tc_result)
				//	ROATModules.AI_searchAccountNumber();
				//if (HtmlReporter.tc_result)
				ROATModules.AI_navigatecustomerpage();
				if (HtmlReporter.tc_result)ROATModules.AI_navigateAccountInquiry();
				if (HtmlReporter.tc_result)ROATModules.AI_validations();								

			}
		} 
		catch (Exception e)
		{
			Reporter.ReportEvent("Test Case execution is failed", "Test Case execution is failed","Test Case execution is failed", false);
			e.printStackTrace();
		}
	}
	public static void SanitySuite_TellerWithdral() 
	{								
		try
		{
			if (HtmlReporter.tc_result)ROATFunctions.refesh(true, Launcher.UserRole);
			//			if (HtmlReporter.tc_result)ROATModules.Prereq_AccInquiry();
			//			if (HtmlReporter.tc_result)ROATFunctions.refesh(false, "Supervisor");
			//			if (HtmlReporter.tc_result)ROATModules.Prereq_SOD();
			//			if (HtmlReporter.tc_result)ROATFunctions.refesh(false, "Head Teller");
			//			if (HtmlReporter.tc_result)ROATModules.Prereq_OCP();	
			//			if (HtmlReporter.tc_result)ROATModules.CD_navigateTeller_Inquiry();
			//			if (HtmlReporter.tc_result)ROATModules.Prereq_SufficientTellerPos();
			//			if (HtmlReporter.tc_result)ROATFunctions.refesh(false, "Super User");
			//			if (HtmlReporter.tc_result)ROATModules.Prereq_AMLLimit();
			//			if (HtmlReporter.tc_result)ROATFunctions.refesh(false, "Teller");
			//			if (HtmlReporter.tc_result)ROATModules.Impact_TellerWithdrawal();
			if (HtmlReporter.tc_result)ROATFunctions.refesh(false, "Teller");
			if (HtmlReporter.tc_result)
			{
				if (HtmlReporter.tc_result)
				{
					try 
					{
						if (HtmlReporter.tc_result)ROATModules.navigate_TellerWithdrawal();
						if (HtmlReporter.tc_result)ROATModules.TellerWithdrawal_EnterDetails();//////////////////////////////////////////////////////////
						if (HtmlReporter.tc_result)ROATModules.TellerWithdrawal_CancelFun();
						if (HtmlReporter.tc_result)ROATModules.TellerWithdrawal_EnterDetails();
						if (HtmlReporter.tc_result)//////////////////////////////////////////////////////////////////////////////////////////
							ROATFunctions.TellerPosition_Count("Teller_Withdrawal", "Teller_With_CurrentTelPosSize",
									"Teller_With_CurrentCount", "txt_soiled", "txt_Decoy", "CDA_TotalCashAmount",
									"txt_PaymentAmount");
						if (HtmlReporter.tc_result)ROATModules.TellerWithdrawal_AgentDetails();/////////////////////////////////////////////////////////////////////////////////////////
						if (HtmlReporter.tc_result)ROATModules.TellerWithdrawal_EnterDetails();
						if (HtmlReporter.tc_result)ROATFunctions.TellerPosition_Count("Teller_Withdrawal", "Teller_With_CurrentTelPosSize",
								"Teller_With_CurrentCount", "txt_soiled", "txt_Decoy", "CDA_TotalCashAmount","txt_PaymentAmount");
						if (HtmlReporter.tc_result)ROATModules.TellerWithdrawal_CancelFun();
						if (HtmlReporter.tc_result == false)
						{
							ROATFunctions.refesh(true,Launcher.UserRole);
						} 
					} 
					catch (Exception e) 
					{
						Reporter.ReportEvent("Test Case execution is failed", "Test Case execution is failed","Test Case execution is failed", false);
					}
				}
				//				if (HtmlReporter.tc_result)ROATModules.Impact_TellerWithdrawal();
			}
		} 
		catch (Exception e) 
		{
			Reporter.ReportEvent("Test Case execution is failed", "Test Case execution is failed","Test Case execution is failed", false);
			e.printStackTrace();
		}					
	}

	@SuppressWarnings("unused")
	public static void CD_tellerInquiryImpact() throws Exception 
	{
		long balance,current_cash_position =0;
		if (HtmlReporter.tc_result)ROATFunctions.refesh(false, Launcher.UserRole);
		try 
		{
			if (HtmlReporter.tc_result) 
			{
				String user_id = ROATFunctions.fetch_user_login_id();
				if (HtmlReporter.tc_result)
				{
					String user_id_name=user_id+"-"+ROATModules.fetchuser();
					if (HtmlReporter.tc_result)
					{
						String user_branch = ROATModules.fetch_user_branch();
						if (HtmlReporter.tc_result)ROATModules.CD_navigateTeller_Inquiry();
						String abc = WebDr.getValue("Currency1");
						System.out.println(abc);
						if (HtmlReporter.tc_result)ROATModules.selectTellerCurrency(user_id_name, WebDr.getValue("Currency1"));
						//if (HtmlReporter.tc_result)ROATModules.countTellerRecords();
						if (HtmlReporter.tc_result)ROATModules.selectTellerCurrency(user_id_name, WebDr.getValue("Currency2"));
						if (HtmlReporter.tc_result)WebDr.click("X", "Expand", "Clicking on Expand", true);
						if (HtmlReporter.tc_result)
						{
							balance = ROATModules.balanceInquiry();
							if (HtmlReporter.tc_result)
							{
								current_cash_position= ROATModules.balanceVerification(balance);
								Map<String, String> amount = ROATModules.storevalues("impact_lcy_payment", "tellerinquiry");
								Map<String, String> tellerPosition = ROATModules.storeTellerPosition("impact_lcy_payment", "tellerposition");
							}
						}
					}
				}
			}
		} 
		catch (Exception e)
		{
			e.printStackTrace();
			Reporter.ReportEvent("Test Case execution is failed", "Test Case execution is failed","Test Case execution is failed", false);
		}
	}
	@SuppressWarnings({ "unused", "unchecked" })
	public static void sanity_lcy_payments()
	{
		int lcy_range=Integer.parseInt(WebDr.getValue("lcy_range"));
		String local_currency=WebDr.getValue("lcy_currency");
		String branch_name="";
		String den_value="1";
		int number_rows=99;
		List<String> conditions=new ArrayList<String>();
		HashMap<String, HashMap<String,Object>> auth_dual = new HashMap<String, HashMap<String,Object>>();
		HashMap<String, BigDecimal> tt_inquiry_lcy = new HashMap<String, BigDecimal>();
		HashMap<String, BigDecimal> tt_inquiry_lcy_after = new HashMap<String, BigDecimal>();
		HashMap<String, BigDecimal> vault_inquiry_lcy = new HashMap<String, BigDecimal>();
		HashMap<String, BigDecimal> vault_inquiry_lcy_after = new HashMap<String, BigDecimal>();
		try 
		{
			ROATFunctions.refesh(false,"Teller");
			branch_name=ROATModules.fetch_user_branch();
			auth_dual=ROATModules.authoriation_dual_control_inquiry(branch_name);

		}
		catch (Exception e) 
		{
			Reporter.ReportEvent("End to end ", "prerequistes inquiry fetching must be successfull","prerequistes inquiry fetching failed", true);
			e.printStackTrace();
		}

		try 
		{
			//*************************************************************Teller---LCY Payment**********************************************
			Reporter.ReportEvent("End to end ", "Teller LCY payment","Teller LCY payment", true);
			HtmlReporter.tc_result=true;
			try
			{
				//Teller inquiry before transaction
				tt_inquiry_lcy=ROATModules.LCY_inquiry();

				if (HtmlReporter.tc_result)
				{
					System.out.println("tt_inquiry_lcy:"+tt_inquiry_lcy);

					//vault inquiry before transaction
					if(HtmlReporter.tc_result)
					{
						vault_inquiry_lcy=ROATModules.Vault_LCY_Inquiry();
						System.out.println("vault_inquiry_lcy:"+vault_inquiry_lcy);

						//Teller receipt 
						if (HtmlReporter.tc_result)ROATModules.tt_payment_lcy(lcy_range,den_value,number_rows,auth_dual);

						//teller cash drawer verification
						tt_inquiry_lcy_after=ROATModules.LCY_inquiry();
						if (HtmlReporter.tc_result)
						{
							System.out.println("tt_inquiry_lcy_after:"+tt_inquiry_lcy_after);
							conditions.add("Payment");
							ROATModules.teller_inquiry_check(tt_inquiry_lcy,tt_inquiry_lcy_after,conditions, lcy_range,"Addition");
							conditions.clear();
							conditions.add("Balance");
							conditions.add("Denomination_balance");
							ROATModules.teller_inquiry_check(tt_inquiry_lcy,tt_inquiry_lcy_after,conditions, lcy_range,"Subtraction");
							conditions.clear();
						}

						//vault Inquiry verification
						if (HtmlReporter.tc_result)
						{
							vault_inquiry_lcy_after=ROATModules.Vault_LCY_Inquiry();
							{
								System.out.println("vault_inquiry_lcy_after:"+vault_inquiry_lcy_after);
								conditions.add("Teller_Payment");
								conditions.add("Denomination_balance");
								conditions.add("balance");
								ROATModules.teller_inquiry_check(vault_inquiry_lcy,vault_inquiry_lcy_after,conditions, lcy_range,"Addition");
								conditions.clear();
							}
						}
					}
				}
			}
			catch(Exception e)
			{
				e.printStackTrace();
				Reporter.ReportEvent("Test Case execution is Failed", "LCY payment transaction","LCY payment transaction failed", false);
			}
		}
		catch (Exception e)
		{
			e.printStackTrace();
			Reporter.ReportEvent("Test Case execution is failed", "Test Case execution is failed","Test Case execution is failed", false);
		}
	}

	////////********************************************Reggression*******************************************************
	//******************************************Account Inquiry***************************************************************
	// ************************************************************************************************************************

	public static void AI_AccountInquiry() 
	{
		try 
		{
			ROATFunctions.refesh(true, Launcher.UserRole);
			if (HtmlReporter.tc_result)ROATModules.AI_navigateAccountInquiry();
			WebDr.SetPageObjects("AccountInquiryPage");
			WebDr.clickX("X","bancodeclick", "Clicking on drpdown",true);
			if (HtmlReporter.tc_result)ROATModules.HAI_cmscheck();
			if (HtmlReporter.tc_result)ROATModules.AHI_dropdowndetails();
			if (HtmlReporter.tc_result)ROATModules.AI_navigateAccountInquiry();
			if (HtmlReporter.tc_result)ROATModules.AI_searchAccountNumber();
			if (HtmlReporter.tc_result)ROATModules.AI_navigatecustomerpage();
			if (HtmlReporter.tc_result)ROATModules.AI_navigateAccountInquiry();
			if (HtmlReporter.tc_result)ROATModules.AI_validations();
			//			if (HtmlReporter.tc_result)
			//				ROATModules.AI_validateExpandCollapse();
			//			if (HtmlReporter.tc_result)
			//				ROATModules.AI_clearSearch();
			//			if (HtmlReporter.tc_result)
			//				ROATValidations.Verify_FieldValidation(1);
			//			if(HtmlReporter.tc_result  == true)
			//				ROATModules.AI_invalidAccountNumber();
			//			if (HtmlReporter.tc_result)
			//				ROATModules.AI_clearSearch();
			//			if(HtmlReporter.tc_result)
			//				ROATModules.AI_invalidBranchNumber();
		}
		catch (Exception e) 
		{
			e.printStackTrace();
			Reporter.ReportEvent("Test Case execution is failed", "Test Case execution is failed","Test Case execution is failed", false);
		}

	}

	// *****************************Customer Inquiry***************************************************************************
	// ************************************************************************************************************************
	public static void CI_CustomerInquiry()
	{
		try
		{
			ROATFunctions.refesh(true, Launcher.UserRole);
			if (HtmlReporter.tc_result)ROATModules.CI_navigateCustomerInquiry();
			if (HtmlReporter.tc_result)ROATModules.CI_customerSearch();
			if (HtmlReporter.tc_result)ROATModules.CI_customerExpand();
			if (HtmlReporter.tc_result)ROATModules.CI_CustomerCollapse();
			if (HtmlReporter.tc_result)ROATModules.CI_accountAccordianVerification();
			if (HtmlReporter.tc_result)ROATModules.CI_productAccordianVerification();
			if (HtmlReporter.tc_result)ROATModules.CI_contactAccordianVerification();
			if (HtmlReporter.tc_result)ROATModules.CI_notesAccordianVerification();
			if (HtmlReporter.tc_result)ROATModules.CI_CustomerClear();
			//			if (HtmlReporter.tc_result)
			//				ROATValidations.Verify_FieldValidation(1);
		} 
		catch (Exception e) 
		{
			e.printStackTrace();
			Reporter.ReportEvent("Test Case execution is failed", "Test Case execution is failed","Test Case execution is failed", false);
		}
	}

	// *****************************Account History Inquiry******************************************************************
	// ************************************************************************************************************************
	public static void AHI_AccountHistoryInquiry() 
	{
		try
		{
			ROATFunctions.refesh(true, Launcher.UserRole);
			if (HtmlReporter.tc_result)ROATModules.AHI_navigateAccountHistoryInquiry();
			WebDr.clickX("X","bancodeclick", "Clicking on drpdown",true);
			if (HtmlReporter.tc_result)ROATModules.AHI_dropdowndetails();
			if (HtmlReporter.tc_result)ROATModules.AHI_PerformAccountSearch();
			if (HtmlReporter.tc_result)WebDr.click("x", "click_on_worst_balace", "ckicking for head", true);
			if (HtmlReporter.tc_result)ROATModules.sort_search("AccountHistoryInquiry","worst_balance_table","click_input_worst_balance","integer",2,"balance_btn");
			if (HtmlReporter.tc_result)ROATModules.AHI_Verification();
			if (HtmlReporter.tc_result)ROATModules.AHI_PaginationValidation("AccountHistoryInquiry", "PaginationTop");
			if (HtmlReporter.tc_result)ROATModules.AHI_PaginationValidation("AccountHistoryInquiry", "PaginationBottom");
			if (HtmlReporter.tc_result)ROATValidations.Verify_FieldValidation(1);

			//			if (HtmlReporter.tc_result)
			//				ROATModules.AHI_invalidAccountNumber();
		}
		catch (Exception e) 
		{
			e.printStackTrace();
			Reporter.ReportEvent("Test Case execution is failed", "Test Case execution is failed","Test Case execution is failed", false);
		}
	}

	// *********************************CUSTOMER
	// SEARCH*******************************
	public static void CS_CustomerSearch() 
	{
		try 
		{
			ROATFunctions.refesh(true, Launcher.UserRole);
			if (HtmlReporter.tc_result)ROATModules.CS_navigateCustomerSearch();
			WebDr.SetPageObjects("CustomerSearchPage");
			if (HtmlReporter.tc_result) WebDr.click("X", "tab_ReferStreamCode", "Clicking on ReferStreamCode field", true);
			//			if (HtmlReporter.tc_result)
			//				ROATModules.AHI_dropdowndetails();
			if (HtmlReporter.tc_result)ROATModules.CS_customerDeatilSearch();
			if(HtmlReporter.tc_result)ROATModules.CS_customerSearchSorting();
			if (HtmlReporter.tc_result)ROATModules.PaginationValidation("CustomerSearchPage","PaginationTop");
			if (HtmlReporter.tc_result)ROATModules.PaginationValidation("CustomerSearchPage","PaginationBottom");

			//			if (HtmlReporter.tc_result)
			//				ROATValidations.Verify_FieldValidation(2);
		}
		catch (Exception e)
		{
			e.printStackTrace();
			Reporter.ReportEvent("Test Case execution is failed", "Test Case execution is failed","Test Case execution is failed", false);
		}
	}

	// ******************************Account Search********************************
	public static void AS_AccountSearch() 
	{
		try 
		{
			ROATFunctions.refesh(false, Launcher.UserRole);
			if (HtmlReporter.tc_result)ROATModules.AS_NavigateAccountSearch();
			if (HtmlReporter.tc_result)ROATModules.HAS_cmscheck();
			if (HtmlReporter.tc_result)ROATModules.AS_PerformAccountSearch();
			//				if (HtmlReporter.tc_result)
			//					ROATValidations.Verify_FieldValidation(2);
		} 
		catch (Exception e) 
		{
			e.printStackTrace();
			Reporter.ReportEvent("Test Case execution is failed", "Test Case execution is failed","Test Case execution is failed", false);
		}

	}

	// *****************************Limits*************************************************************************************
	public static void LT_LimitsVerificationViewUpdate()
	{
		try
		{
			ROATFunctions.refesh(false, Launcher.UserRole);
			if (HtmlReporter.tc_result)ROATModules.setup_navigateLTLimits();
			if (HtmlReporter.tc_result)ROATModules.setup_limitsVerification();
			if (HtmlReporter.tc_result)ROATModules.setup_limitsAdd();
			if (HtmlReporter.tc_result)ROATModules.setup_limitsViewUpdate();
			//if (HtmlReporter.tc_result)
			//ROATModules.setup_limitsToogleButton();
			if (HtmlReporter.tc_result == false)
			{
				ROATFunctions.refesh(true,Launcher.UserRole);
			}
			//			if (HtmlReporter.tc_result)
			//				ROATValidations.Verify_FieldValidation(3);
		}
		catch (Exception e) 
		{
			e.printStackTrace();
			Reporter.ReportEvent("Test Case execution is failed", "Test Case execution is failed","Test Case execution is failed", false);
		}

	}

	// ****************************************Bank****************************************************************************
	// ************************************************************************************************************************
	public static void SetUp_Bank() 
	{
		try 
		{
			ROATFunctions.refesh(false, Launcher.UserRole);
			if (HtmlReporter.tc_result)ROATModules.setup_navigateLTLimitsBank();
			if (HtmlReporter.tc_result)ROATFunctions.CDA_SysDateValidation("BankSetup","Bank_Setup_Date");
			if (HtmlReporter.tc_result)ROATModules.setup_navigateLTLimitsBankVerification1();
		} 
		catch (Exception e)
		{
			e.printStackTrace();
			Reporter.ReportEvent("Test Case execution is failed", "Test Case execution is failed","Test Case execution is failed", false);
		}

	}

	// *****************************************Branch*************************************************************************
	// ************************************************************************************************************************
	public static void SetUp_Branch()
	{
		try 
		{
			ROATFunctions.refesh(false, Launcher.UserRole);
			if (HtmlReporter.tc_result)ROATModules.setup_navigateLTLimitsBranch();
			if (HtmlReporter.tc_result)ROATModules.setup_navigateLTLimitsBranchVerification();
			if (HtmlReporter.tc_result)ROATFunctions.CDA_SysDateValidation("BranchSetup","Branch_Setup_Date");
			if(HtmlReporter.tc_result)ROATModules.BranchSetup_Sorting();
			if (HtmlReporter.tc_result)ROATModules.setup_BranchCountTotalRecords();
			if (HtmlReporter.tc_result)ROATModules.menu_navigation("MasterPage", "tab_bank", "null");
			/*if (HtmlReporter.tc_result)
				ROATModules.menu_navigation1("MasterPage", "tab_sig_authorise", "null");*/
			if (HtmlReporter.tc_result)ROATModules.setup_navigateLTLimitsBranch();			
			if (HtmlReporter.tc_result)ROATModules.setup_branchFetchDataWithBrains();
		} 
		catch (Exception e)
		{
			e.printStackTrace();
			Reporter.ReportEvent("Test Case execution is failed", "Test Case execution is failed","Test Case execution is failed", false);
		}

	}
	//***********************************Mixed deposit***********************************************************************
	public static void mixedDeposit()
	{
		try 
		{
			ROATFunctions.refesh(true, Launcher.UserRole);
			if (HtmlReporter.tc_result)ROATModules.menu_navigation("MasterPage", "tab_Teller", "null");
			if (HtmlReporter.tc_result)ROATModules.menu_navigation1("MasterPage", "tab_Deposit", "null");
			if (HtmlReporter.tc_result)ROATModules.Mixed_Deposit_AddCheque();
			if (HtmlReporter.tc_result)ROATModules.menu_navigation1("MasterPage", "tab_CashWithdraw", "null");
			if (HtmlReporter.tc_result)ROATModules.menu_navigation1("MasterPage", "tab_Deposit", "null");
			if (HtmlReporter.tc_result)ROATModules.Mixed_Deposit_Add();
		} 
		catch (Exception e)
		{
			e.printStackTrace();
			Reporter.ReportEvent("Test Case execution is failed", "Test Case execution is failed","Test Case execution is failed", false);
		}
	}


	// *************************************************************************************************************************


	public static void LT_Currency() 
	{
		try 
		{
			ROATFunctions.refesh(false, Launcher.UserRole);
			if (HtmlReporter.tc_result)ROATModules.setup_navigateCurrency();
			if (HtmlReporter.tc_result)ROATModules.setup_currencyVerification();
			if (HtmlReporter.tc_result)ROATModules.setup_currencyView();
			if (HtmlReporter.tc_result)ROATModules.setup_currencyUpdate();
			if (HtmlReporter.tc_result)ROATFunctions.CDA_SysDateValidation("CurrencyPage","Currency_Setup_Date");
			if (HtmlReporter.tc_result)ROATModules.PaginationValidation("CurrencyPage","PaginationTop");
			if (HtmlReporter.tc_result)ROATModules.PaginationValidation("CurrencyPage","PaginationBottom");
			if(HtmlReporter.tc_result)ROATModules.CurrencySetup_Sorting();
			//			if (HtmlReporter.tc_result)
			//				ROATValidations.Verify_FieldValidation(2);
			//			if (HtmlReporter.tc_result == false)
			//				ROATModules.setup_currencyStatusToggle();
		} 
		catch (Exception e) 
		{
			e.printStackTrace();
			Reporter.ReportEvent("Test Case execution is failed", "Test Case execution is failed","Test Case execution is failed", false);
		}
	}

	// *********************************************BRANCH
	// CURRENCY*******************************************************
	// *******************************************************************************************************************

	public static void Setup_navigateBranchCurrency() 
	{
		try
		{
			ROATFunctions.refesh(false, Launcher.UserRole);
			if (HtmlReporter.tc_result)ROATModules.setup_navigateBranchCurrency();
			if (HtmlReporter.tc_result)ROATModules.setup_navigateBranchCurrencyVerification();
			//			if (HtmlReporter.tc_result)
			//				ROATModules.setupp_addBranchCurrency();
			//			if (HtmlReporter.tc_result)
			//				ROATModules.setupp_addBranchCurrencyVerification();
			if (HtmlReporter.tc_result)ROATModules.setup_navigateBranchCurrencyView();
			if (HtmlReporter.tc_result)ROATModules.setup_navigateBranchCurrencyViewVerification();
			//			if (HtmlReporter.tc_result)
			//				ROATModules.setup_navigateBranchCurrencyUpdate();
		} 
		catch (Exception e) 
		{
			e.printStackTrace();
			Reporter.ReportEvent("Test Case execution is failed", "Test Case execution is failed","Test Case execution is failed", false);
		}
	}

	// *************************************************************************************************************************
	// ******************************************Refer Code
	// Maintenance*********************************************************
	public static void RC_ReferCodeMaintenace()
	{
		try
		{
			ROATFunctions.refesh(false, Launcher.UserRole);
			if (HtmlReporter.tc_result)ROATModules.setup_navigateReferCodeMaintenance();
			if (HtmlReporter.tc_result)ROATModules.setup_ReferCodeMaintenanceView();
			if (HtmlReporter.tc_result)ROATModules.RF_validations();
			if (HtmlReporter.tc_result)ROATModules.setup_ReferCodeMaintenanceUpdate();
			if (HtmlReporter.tc_result)ROATModules.setup_ReferCodeMaintenanceDelete();
		}
		catch (Exception e)
		{
			e.printStackTrace();
			Reporter.ReportEvent("Test Case execution is failed", "Test Case execution is failed","Test Case execution is failed", false);
		}
	}

	public static void RC_ReferCodeMaintenaceAdd()
	{
		try 
		{
			ROATFunctions.refesh(true, Launcher.UserRole);
			if (HtmlReporter.tc_result)ROATModules.setup_navigateReferCodeMaintenance();
			if (HtmlReporter.tc_result)ROATModules.setup_ReferCodeMaintenanceAddNew();
			//			if (HtmlReporter.tc_result)
			//				ROATValidations.Verify_FieldValidation(10);
		} 
		catch (Exception e) 
		{
			e.printStackTrace();
			Reporter.ReportEvent("Test Case execution is failed", "Test Case execution is failed","Test Case execution is failed", false);
		}
	}
	//*********************************************Fees*******************************************************
	//*******************************************************************************************************************	

	public static void LT_Fees_AddNewFunctionality()
	{
		try
		{
			ROATFunctions.refesh(false, Launcher.UserRole);
			if (HtmlReporter.tc_result) ROATModules.setup_navigateFeesExchange();
			if (HtmlReporter.tc_result) ROATModules.setup_FeesDraftIssue_AddNewAuthorize();
			if (HtmlReporter.tc_result) ROATModules.setup_FeesDraftCancel_AddNewAuthorize();
			if (HtmlReporter.tc_result) ROATModules.setup_FeesExchangeSell_AddNewAuthorize();
			if (HtmlReporter.tc_result) ROATModules.setup_FeesExchangePurcahse_AddNewAuthorize();
			if (HtmlReporter.tc_result) ROATModules.setup_FeesExchangeTransfer_AddNewAuthorize();
			if (HtmlReporter.tc_result) ROATModules.setup_FeesBills_AddNewAuthorize();
			if (HtmlReporter.tc_result) ROATModules.setup_FeesDeposit_AddNewAuthorize();
			if (HtmlReporter.tc_result) ROATModules.setup_FeesDraftIssue_View();
			if (HtmlReporter.tc_result) ROATModules.setup_FeesDraftCancel_View();
			if (HtmlReporter.tc_result) ROATModules.setup_FeesExchangeSell_View();
			if (HtmlReporter.tc_result) ROATModules.setup_FeesExchangePurchase_View();
			if (HtmlReporter.tc_result) ROATModules.setup_FeesExchangeTransfer_View();
			if (HtmlReporter.tc_result) ROATModules.setup_FeesBills_View();
			if (HtmlReporter.tc_result) ROATModules.setup_FeesDeposit_View();
			if (HtmlReporter.tc_result) ROATModules.setup_FeesStatusToggle();	
			if (HtmlReporter.tc_result) ROATModules.setup_FeesStatusToggle_Reject();
		}
		catch (Exception e) 
		{
			e.printStackTrace();
			Reporter.ReportEvent("Test Case execution is failed", "Test Case execution is failed","Test Case execution is failed", false);
		}
	}
	public static void LT_Fees_UpdateAndRejectFunctionality()
	{
		try
		{
			ROATFunctions.refesh(true, Launcher.UserRole);
			if (HtmlReporter.tc_result) ROATModules.setup_navigateFeesExchange();
			if (HtmlReporter.tc_result) ROATModules.setup_FeesDraftCancel_AddNewRejection();
			if (HtmlReporter.tc_result) ROATModules.setup_FeesDraftCancel_SlidingAcceptAddNewReject();
			if (HtmlReporter.tc_result) ROATModules.setup_FeesDraftIssue_Update();
			if (HtmlReporter.tc_result) ROATModules.setup_FeesDraftCancel_Update();
			if (HtmlReporter.tc_result) ROATModules.setup_FeesExchangeSell_Update();
			if (HtmlReporter.tc_result) ROATModules.setup_FeesExchangePurchase_Update();
			if (HtmlReporter.tc_result) ROATModules.setup_FeesExchangeTransfer_Update();
			if (HtmlReporter.tc_result) ROATModules.setup_FeesBills_Update();
			if (HtmlReporter.tc_result) ROATModules.setup_FeesDeposit_Update();
		}
		catch (Exception e)
		{
			e.printStackTrace();
			Reporter.ReportEvent("Test Case execution is failed", "Test Case execution is failed","Test Case execution is failed", false);
		}
	}


	public static void LT_Fees_AddNewFieldValidation()
	{
		try
		{
			ROATFunctions.refesh(true, Launcher.UserRole);
			if (HtmlReporter.tc_result) ROATModules.setup_navigateFeesExchange();

			if (HtmlReporter.tc_result)ROATValidations.Verify_FieldValidation(8);
			ROATFunctions.refesh(true,Launcher.UserRole);
		}
		catch (Exception e)
		{
			e.printStackTrace();
			Reporter.ReportEvent("Test Case execution is failed", "Test Case execution is failed","Test Case execution is failed", false);
		}
	}

	// **************************LCY Inquiry******************************************************************************************************************************************************
	// **********************************************************************************************************************************************************************************************

	public static void CD_LCYinquiry()
	{
		try 
		{
			ROATFunctions.refesh(false, Launcher.UserRole);
			if (HtmlReporter.tc_result)ROATModules.setup_navigateLCYInquiry();
			if (HtmlReporter.tc_result)ROATModules.LCYDownload();
		}
		catch (Exception e) 
		{
			e.printStackTrace();
			Reporter.ReportEvent("Test Case execution is failed", "Test Case execution is failed","Test Case execution is failed", false);
		}
	}

	public static void CD_LCYTotalVerification()
	{
		boolean temp=true;
		try
		{
			ROATFunctions.refesh(false, Launcher.UserRole);
			if (HtmlReporter.tc_result)ROATModules.setup_navigateLCYInquiry();
			if (HtmlReporter.tc_result)ROATModules.setup_BalanceLCYVerification();

			temp=HtmlReporter.tc_result;
			HtmlReporter.tc_result=true;
			ROATFunctions.refesh(false, "Super User");

			if (HtmlReporter.tc_result)ROATModules.setup_navigateLCYInquiry2();
			if (HtmlReporter.tc_result)ROATModules.notes_check(0);
			ROATFunctions.refesh(false,"Teller");
			if (HtmlReporter.tc_result)ROATModules.setup_navigateLCYInquiry();
			if (HtmlReporter.tc_result)ROATModules.notes_check(1);
			HtmlReporter.tc_result=temp;
		} 
		catch (Exception e) 
		{
			e.printStackTrace();
			Reporter.ReportEvent("Test Case execution is failed", "Test Case execution is failed","Test Case execution is failed", false);
		}
	}
	// **************************FCY Inquiry******************************************************************************************************************************************************
	// **************************************************************************************************************************************************************

	public static void FC_FCYinquiry() 
	{
		try
		{
			ROATFunctions.refesh(false, Launcher.UserRole);
			if (HtmlReporter.tc_result)ROATModules.setup_navigateFCYInquiry();
			if (HtmlReporter.tc_result)ROATModules.setup_FCYDownload();
		} 
		catch (Exception e)
		{
			e.printStackTrace();
			Reporter.ReportEvent("Test Case execution is failed", "Test Case execution is failed","Test Case execution is failed", false);
		}
	}

	public static void FC_FCYBalanceVerification() 
	{
		try 
		{
			ROATFunctions.refesh(false, Launcher.UserRole);
			if (HtmlReporter.tc_result)ROATModules.setup_navigateFCYInquiry();
			if (HtmlReporter.tc_result)ROATModules.setup_BalanceFCYVerification();
			if (HtmlReporter.tc_result)ROATModules.setup_ExpandCollapse();
		} 
		catch (Exception e)
		{
			e.printStackTrace();
			Reporter.ReportEvent("Test Case execution is failed", "Test Case execution is failed","Test Case execution is failed", false);
		}
	}

	public static void FCY_Payment()
	{
		try 
		{
			ROATFunctions.refesh(false, Launcher.UserRole);
			WebDr.SetPageObjects("FCYPayment");
			if (HtmlReporter.tc_result)ROATModules.menu_navigation("MasterPage", "tab_CashDrawer", "tab_CashDrawer_NotToClose");
			if (HtmlReporter.tc_result)ROATModules.menu_navigation1("MasterPage", "tab_FCY", "tab_FCY_closed");
			if (HtmlReporter.tc_result)ROATModules.menu_navigation1("MasterPage", "tab_FCY_payment", "null");
			if (HtmlReporter.tc_result)ROATModules.FCY_payment_select_currency();
			//if (HtmlReporter.tc_result)ROATModules.checkSoiledDecoy("FCYPayment", "Payment");
			if (HtmlReporter.tc_result)ROATModules.Enter_payment_Amount("FCYPayment", "txtAmounts");
			if (HtmlReporter.tc_result)ROATModules.error_Message_verification("Mismatch", WebDr.getValue("ErrorMsg_Mismatch"));
			if (HtmlReporter.tc_result)ROATModules.enter_note_coin_Count("FCYPayment", "tablenotes", "txtnots");
			if (HtmlReporter.tc_result)ROATModules.enter_note_coin_Count("FCYPayment", "tableCoins", "txtcoins");

			String value= WebDr.getElement("re_enter").getText().substring(4).trim().replace(",", "");
			if (HtmlReporter.tc_result)ROATModules.Reenter_payment_Amount("FCYPayment","txtAmounts",value);
			// iffasdf
			// (HtmlReporter.tc_result)ROATModules.enter_Soiled_decoy_value();
			if (HtmlReporter.tc_result)ROATModules.fn_verify_total_cash_payment_amount("FCYPayment", WebDr.getValue("Enter_Amount"));
			String Auth_user[] =Launcher.Auth_User_Login("Super User");
			String Login[] = Launcher.User_Login("Super User");
			if (HtmlReporter.tc_result)ROATModules.Enter_Authorize_Credential("FCYPayment", "txt_AuthorizerUser", "txt_AuthorizerPass",Auth_user[0], Auth_user[1], Login[0], Login[1],"password2","username2", true);
			if (HtmlReporter.tc_result)ROATModules.error_Message_verification("Authorize", WebDr.getValue("VerifyPostMessage"));
			// Reject payment scenario
			if (HtmlReporter.tc_result)ROATModules.FCY_payment_select_currency();
			if (HtmlReporter.tc_result)ROATModules.Enter_payment_Amount("FCYPayment", "txtAmounts");
			if (HtmlReporter.tc_result)ROATModules.error_Message_verification("Mismatch", WebDr.getValue("ErrorMsg_Mismatch"));
			if (HtmlReporter.tc_result)ROATModules.enter_note_coin_Count("FCYPayment", "tablenotes", "txtnots");
			if (HtmlReporter.tc_result)ROATModules.enter_note_coin_Count("FCYPayment", "tableCoins", "txtcoins");
			if (HtmlReporter.tc_result)ROATModules.Reenter_payment_Amount("FCYPayment","txtAmounts",value);
			// if
			// (HtmlReporter.tc_result)ROATModules.enter_Soiled_decoy_value();
			if (HtmlReporter.tc_result)ROATModules.Enter_Authorize_Credential("FCYPayment", "txt_AuthorizerUser", "txt_AuthorizerPass",Auth_user[0], Auth_user[1], Login[0], Login[1],"password2","username2", false);
			if (HtmlReporter.tc_result)ROATModules.error_Message_verification("Reject", WebDr.getValue("Verify_Reject_Message"));
			if (HtmlReporter.tc_result)ROATModules.Enter_payment_Amount("FCYPayment", "txtAmounts");
			if (HtmlReporter.tc_result)ROATModules.Cancel_LCY_Payment_function();
		}
		catch (Exception e) 
		{
			e.printStackTrace();
			Reporter.ReportEvent("Test Case execution is failed", "Test Case execution is failed","Test Case execution is failed", false);
		}
	}

	public static void LCY_Payment()
	{
		try
		{
			ROATFunctions.refesh(false, Launcher.UserRole);
			WebDr.SetPageObjects("LCYPayment");
			if (HtmlReporter.tc_result)ROATModules.menu_navigation("MasterPage", "tab_CashDrawer", "tab_CashDrawer_NotToClose");
			if (HtmlReporter.tc_result)ROATModules.menu_navigation1("MasterPage", "tab_LCY", "tab_LCY_closed");
			if (HtmlReporter.tc_result)ROATModules.menu_navigation1("MasterPage", "tab_LCY_payment", "null");
			if (HtmlReporter.tc_result)ROATModules.Enter_payment_Amount("LCYPayment", "txtAmounts");
			//if (HtmlReporter.tc_result)ROATModules.checkSoiledDecoy("LCYPayment", "Payment");
			if (HtmlReporter.tc_result)ROATModules.error_Message_verification("Mismatch", WebDr.getValue("ErrorMsg_Mismatch"));

			if (HtmlReporter.tc_result)ROATModules.enter_note_coin_Count("LCYPayment", "tablenotes", "txtnots");
			if (HtmlReporter.tc_result)ROATModules.enter_note_coin_Count("LCYPayment", "tableCoins", "txtcoins");
			String value= WebDr.getElement("re_enter").getText().substring(4).trim().replace(",", "");
			if (HtmlReporter.tc_result)ROATModules.Reenter_payment_Amount("LCYPayment","txtAmounts",value);
			// if
			// (HtmlReporter.tc_result)ROATModules.enter_Soiled_decoy_value();

			if (HtmlReporter.tc_result)ROATModules.fn_verify_total_cash_payment_amount("LCYPayment", WebDr.getValue("Enter_Amount"));
			String Auth_user[] =Launcher.Auth_User_Login("Super User");
			String Login[] = Launcher.User_Login("Super User");
			if (HtmlReporter.tc_result)ROATModules.Enter_Authorize_Credential("LCYPayment", "txt_AuthorizerUser", "txt_AuthorizerPass",Auth_user[0], Auth_user[1],Login[0],Login[1], "password2","username2", true);

			if (HtmlReporter.tc_result)ROATModules.error_Message_verification("Authorize", WebDr.getValue("VerifyPostMessage"));

			// Reject payment scenario

			if (HtmlReporter.tc_result)ROATModules.Enter_payment_Amount("LCYPayment", "txtAmounts");
			if (HtmlReporter.tc_result)ROATModules.error_Message_verification("Mismatch", WebDr.getValue("ErrorMsg_Mismatch"));
			if (HtmlReporter.tc_result)ROATModules.enter_note_coin_Count("LCYPayment", "tablenotes", "txtnots");
			if (HtmlReporter.tc_result)ROATModules.enter_note_coin_Count("LCYPayment", "tableCoins", "txtcoins");
			if (HtmlReporter.tc_result)ROATModules.Reenter_payment_Amount("LCYPayment","txtAmounts",value);
			// if
			// (HtmlReporter.tc_result)ROATModules.enter_Soiled_decoy_value();

			if (HtmlReporter.tc_result)ROATModules.Enter_Authorize_Credential("LCYPayment", "txt_AuthorizerUser", "txt_AuthorizerPass",Auth_user[0], Auth_user[1],Login[0],Login[1], "password2","username2", false);
			if (HtmlReporter.tc_result)ROATModules.error_Message_verification("Reject", WebDr.getValue("Verify_Reject_Message"));
			if (HtmlReporter.tc_result)ROATModules.Cancel_LCY_Payment_function();
		} 
		catch (Exception e)
		{
			e.printStackTrace();
			Reporter.ReportEvent("Test Case execution is failed", "Test Case execution is failed","Test Case execution is failed", false);
		}
	}

	public static void cash_DrawerLCYReceipt()
	{
		try 
		{
			ROATFunctions.refesh(true, Launcher.UserRole);
			String teller=ROATModules.fetchuser();
			System.out.println(teller);
			if (HtmlReporter.tc_result)ROATModules.CD_navigateLCYReceipt();
			if (HtmlReporter.tc_result)ROATModules.CD_transferLCYReceipt();
			String Auth_user[] =Launcher.Auth_User_Login("Vault Custodian");
			String Login[] = Launcher.User_Login("Teller");
			if (HtmlReporter.tc_result)ROATModules.Enter_Authorize_Credential("LCYReceipt","username", "password",Auth_user[0], Auth_user[1],Login[0], Login[1],"password2", "username2",true);
			Map<String, String> a = ROATModules.storeDenomination("LCYReceipt", "DenominationRows");
			ROATFunctions.refesh(false, "Head Teller");
			if (HtmlReporter.tc_result)ROATModules.CD_navigateTeller_Inquiry();
			if (HtmlReporter.tc_result)ROATModules.CDA_navigateTellerInquiry(teller, WebDr.getValue("CurrencyDrop2"));
			Map<String, String> b = ROATModules.storeDenomination("TellerInquiry", "DenominationRows");
			if (HtmlReporter.tc_result)ROATModules.compare(a, b);
		}
		catch (Exception e) 
		{
			e.printStackTrace();
			Reporter.ReportEvent("Test Case execution is failed", "Test Case execution is failed","Test Case execution is failed", false);
		}
	}

	public static void cash_DrawerFCYReceipt()
	{
		try
		{
			ROATFunctions.refesh(false, Launcher.UserRole);
			String teller=ROATModules.fetchuser();
			System.out.println(teller);
			if (HtmlReporter.tc_result)ROATModules.CD_navigateFCYReceipt();
			if (HtmlReporter.tc_result)ROATModules.CD_navigateFCYReceiptVeri();
			if (HtmlReporter.tc_result)ROATModules.CD_transferFCYReceipt();
			if (HtmlReporter.tc_result)WebDr.clickX("X", "drp_Currency", "Clicking on the dropdown Currency", true);

			String XPath = WebDr.getValue("CurrencyDrop2");
			String path="XPATH|//div[text()='"+XPath+"']";
			if (HtmlReporter.tc_result)WebDr.clickX("CUSTOM",path, "Selecting currency", true);
			Map<String, String> a = ROATModules.storeDenomination("FCYReceipt", "DenRows");
			ROATFunctions.refesh(false, "Head Teller");
			if (HtmlReporter.tc_result)ROATModules.CD_navigateTeller_Inquiry();
			if (HtmlReporter.tc_result)ROATModules.CDA_navigateTellerInquiry(teller, WebDr.getValue("CurrencyDrop2"));
			Map<String, String> b = ROATModules.storeDenomination("TellerInquiry", "DenominationRows");
			if (HtmlReporter.tc_result)ROATModules.compare(a, b);
		} 
		catch (Exception e)
		{
			e.printStackTrace();
			Reporter.ReportEvent("Test Case execution is failed", "Test Case execution is failed","Test Case execution is failed", false);
		}
	}

	public static void cash_DrawerTeller_Inquiry()
	{
		try
		{
			ROATFunctions.refesh(false, Launcher.UserRole);
			if (HtmlReporter.tc_result)ROATModules.CD_navigateTeller_Inquiry();
			if (HtmlReporter.tc_result)ROATModules.CD_Teller_InquiryView();
			if (HtmlReporter.tc_result)ROATModules.CD_Teller_InquiryBalanceCheck();
			if (HtmlReporter.tc_result)ROATModules.CD_Teller_InquiryDataSort();
		} 
		catch (Exception e) 
		{
			e.printStackTrace();
			Reporter.ReportEvent("Test Case execution is failed", "Test Case execution is failed","Test Case execution is failed", false);
		}
	}

	public static void cash_DrawerTeller_InquiryPrintDownload()
	{
		try 
		{
			ROATFunctions.refesh(false, Launcher.UserRole);
			if (HtmlReporter.tc_result)ROATModules.CD_navigateTeller_Inquiry();
			if (HtmlReporter.tc_result)ROATModules.CD_TellerInquiryDownload();
		}
		catch (Exception e) 
		{
			e.printStackTrace();
			Reporter.ReportEvent("Test Case execution is failed", "Test Case execution is failed","Test Case execution is failed", false);
		}
	}

	public static void cash_DrawerCash_Summary()
	{
		try
		{
			ROATFunctions.refesh(false, Launcher.UserRole);
			if (HtmlReporter.tc_result)ROATModules.CD_navigateCash_Summary();
			if (HtmlReporter.tc_result)ROATModules.CD_Cash_SummaryView();
			if (HtmlReporter.tc_result)ROATFunctions.CD_SysDateValidation("CashSummaryPage","SystemTimeDate");
			if (HtmlReporter.tc_result)ROATModules.CD_Cash_SummaryDataSort();
		}
		catch (Exception e)
		{
			e.printStackTrace();
			Reporter.ReportEvent("Test Case execution is failed", "Test Case execution is failed","Test Case execution is failed", false);
		}
	}

	public static void cash_DrawerCash_SummaryPrintDownload()
	{
		try
		{
			ROATFunctions.refesh(false, Launcher.UserRole);
			if (HtmlReporter.tc_result)ROATModules.CD_navigateCash_Summary();
			if (HtmlReporter.tc_result)ROATModules.CD_Cash_SummaryDownload();
		} 
		catch (Exception e)
		{
			e.printStackTrace();
			Reporter.ReportEvent("Test Case execution is failed", "Test Case execution is failed","Test Case execution is failed", false);
		}
	}

	// ***********************************************FCY Transfer Out**********************************************************
	// *************************************************************************************************************************
	public static void cash_DrawerFCYTransferOut() 
	{
		try 
		{
			ROATFunctions.refesh(false, Launcher.UserRole);
			if (HtmlReporter.tc_result)ROATModules.setup_FCYTransferOutFieldVerification();
			if (HtmlReporter.tc_result)ROATModules.setup_selectCurrency();
			//if (HtmlReporter.tc_result)ROATModules.checkSoiledDecoy("FCYTransferOutPage", "Transfer_out");
			if (HtmlReporter.tc_result)ROATModules.setup_FCYTransferOutE2E();
			if (HtmlReporter.tc_result)ROATModules.setup_FCY_Notes();
			if (HtmlReporter.tc_result)ROATModules.setup_FCY_Coins();
			String value= WebDr.getElement("re_enter").getText().substring(4).trim().replace(",", "");
			if (HtmlReporter.tc_result)ROATModules.Reenter_payment_Amount("FCYTransferOutPage","txt_PaymentAmount",value);
			if (HtmlReporter.tc_result)ROATModules.setup_FCYE2E_Authorize();

			if (HtmlReporter.tc_result)ROATModules.setup_CurrencyDecrementCount();
			if (HtmlReporter.tc_result)ROATModules.setup_selectCurrency();
			if (HtmlReporter.tc_result)ROATModules.setup_FCYNotes1(WebDr.getValue("Payment_Amount"), "Correct");
			if (HtmlReporter.tc_result)ROATModules.setup_FCYAuthorizeMismatchReject("Authorize");

			if (HtmlReporter.tc_result)ROATModules.setup_ShowCurrency();
			if (HtmlReporter.tc_result)ROATModules.setup_selectCurrency();
			if (HtmlReporter.tc_result)ROATModules.setup_FCYNotes1(WebDr.getValue("Payment_Amount"), "Payment Amount already full");
			if (HtmlReporter.tc_result)ROATModules.setup_FCYAuthorizeMismatchReject("Reject");

			if (HtmlReporter.tc_result)ROATModules.setup_ShowCurrency();
			if (HtmlReporter.tc_result)ROATModules.setup_selectCurrency();
			if (HtmlReporter.tc_result)ROATModules.setup_FCYNotes1(WebDr.getValue("PaymentAmtLessThenTotalCash"), "Currency is not match");
			if (HtmlReporter.tc_result)ROATModules.setup_FCYAuthorizeMismatchReject("Mismatch");
			if (HtmlReporter.tc_result)ROATModules.setup_FCYNotes1(WebDr.getValue("PaymentAmtMoreThenTotalCash"), "Currency is not match");
			if (HtmlReporter.tc_result)ROATModules.setup_FCYAuthorizeMismatchReject("Mismatch");

			if (HtmlReporter.tc_result)ROATModules.setup_FCYCancel();
		} 
		catch (Exception e) 
		{
			e.printStackTrace();
			Reporter.ReportEvent("Test Case execution is failed", "Test Case execution is failed","Test Case execution is failed", false);
		}
	}

	// ***********************************************LCY Transfer Out**********************************************************
	// *************************************************************************************************************************
	public static void cash_DrawerLCYTransferOut()
	{
		try 
		{
			ROATFunctions.refesh(false, Launcher.UserRole);
			if (HtmlReporter.tc_result)ROATModules.setup_LCYTransferOutFieldVerification();
			if (HtmlReporter.tc_result)ROATModules.setup_LCYTransferOutE2E();
			Map<String, String> a = ROATModules.storeDenomination("LCYTransferOutPage", "tableCurrTellerPosition");
			if (HtmlReporter.tc_result)ROATFunctions.TellerPosition_Count("LCYTransferOutPage", "tableCurrTellerPosition", "tableTellerCount",
					"txt_soiled", "txt_Decoy", "CDA_TotalCashAmount", "txt_PaymentAmount");// String
			String value= WebDr.getElement("re_enter").getText().substring(4).trim().replace(",", "");
			if (HtmlReporter.tc_result)ROATModules.Reenter_payment_Amount("LCYTransferOutPage","txt_PaymentAmount",value);
			if (HtmlReporter.tc_result)ROATModules.setup_LCYAuthorize();
			if (HtmlReporter.tc_result)ROATModules.setup_LCYTransactionMessage();
			Map<String, String> b = ROATModules.storeDenomination("LCYTransferOutPage", "tableCurrTellerPosition");
			if (HtmlReporter.tc_result)ROATModules.compareLCYTransferOut(a, b);
			if (HtmlReporter.tc_result)ROATModules.setup_selectLCYTeller();
			if (HtmlReporter.tc_result)ROATModules.setup_LCYNotes1(WebDr.getValue("Payment_Amount"), "Correct");
			if (HtmlReporter.tc_result)ROATModules.setup_LCYAuthorizeMismatchReject("Authorize");
			if (HtmlReporter.tc_result)ROATModules.setup_selectLCYTeller();
			if (HtmlReporter.tc_result)ROATModules.setup_LCYNotes1(WebDr.getValue("Payment_Amount"), "Payment Amount already full");
			if (HtmlReporter.tc_result)ROATModules.setup_LCYAuthorizeMismatchReject("Reject");
			if (HtmlReporter.tc_result)ROATModules.setup_selectLCYTeller();
			if (HtmlReporter.tc_result)ROATModules.setup_LCYNotes1(WebDr.getValue("PaymentAmtLessThenTotalCash"),"Payment Amount is less then Denomination");
			if (HtmlReporter.tc_result)ROATModules.setup_LCYAuthorizeMismatchReject("Mismatch");
			if (HtmlReporter.tc_result)ROATModules.setup_LCYNotes1(WebDr.getValue("PaymentAmtMoreThenTotalCash"),"Payment Amount is more then Denomination");
			if (HtmlReporter.tc_result)ROATModules.setup_LCYAuthorizeMismatchReject("Mismatch");
			if (HtmlReporter.tc_result)ROATModules.setup_LCYCancel();
		}
		catch (Exception e) 
		{
			e.printStackTrace();
			Reporter.ReportEvent("Test Case execution is failed", "Test Case execution is failed","Test Case execution is failed", false);
		}
	}

	// **************************Remote Authorization************************
	public static void RemoteAuthorization() 
	{
		try
		{
			ROATFunctions.refesh(false, Launcher.UserRole);
			if (HtmlReporter.tc_result)
				ROATModules.AUTHRem_navigateRemAuthorization();

			if (HtmlReporter.tc_result)ROATModules.Rem_AUTH();
		} catch (Exception e) {
			Reporter.ReportEvent("Test Case execution is failed", "Test Case execution is failed",
					"Test Case execution is failed", false);
		}
	}

	// ********************Setup User
	// Maintenance********************************
	// *****************************************************************************

	public static void Setup_UserMaintenanceView() 
	{
		try
		{
			ROATFunctions.refesh(false, Launcher.UserRole);

			if (HtmlReporter.tc_result)ROATModules.Setup_navigateUser_Maintenance();
			if (HtmlReporter.tc_result)ROATModules.Setup_UserMaintenance_View();			
		}
		catch (Exception e)
		{
			Reporter.ReportEvent("Test Case execution is failed", "Test Case execution is failed",
					"Test Case execution is failed", false);
		}
	}


	public static void Setup_UserMaintenanceAdd() 
	{
		try
		{
			ROATFunctions.refesh(true, Launcher.UserRole);

			if (HtmlReporter.tc_result)ROATModules.Setup_navigateUser_Maintenance();
			if (HtmlReporter.tc_result)ROATModules.AddUser_UserMaintenance();
		}
		catch (Exception e)
		{
			Reporter.ReportEvent("Test Case execution is failed", "Test Case execution is failed","Test Case execution is failed", false);
		}
	}

	public static void Setup_UserMaintenanceUpdate() 
	{
		try
		{
			ROATFunctions.refesh(false, Launcher.UserRole);

			if (HtmlReporter.tc_result)
				ROATModules.Setup_navigateUser_Maintenance();
			if (HtmlReporter.tc_result)
				ROATModules.UpdateStatus_UserMaintenance();
		}
		catch (Exception e)
		{
			Reporter.ReportEvent("Test Case execution is failed", "Test Case execution is failed",
					"Test Case execution is failed", false);
		}
	}

	// *********************Setup-Bank
	// ********************************
	// *****************************************************************************
	public static void Setup_Bank() {

		try {
			String Auth_user1[] =Launcher.Auth_User_Login("Super User");
			ROATFunctions.refesh(false, Launcher.UserRole);
			if (HtmlReporter.tc_result)
				ROATModules.SE_bankNavigation();

			if (HtmlReporter.tc_result)
				ROATModules.ViewBankBreadCrumb();
			if (HtmlReporter.tc_result)
				ROATModules.SE_bankAdd();
			ROATModules.Enter_Authorize_Credential("BankPage", "username", "password", Auth_user1[0], Auth_user1[1], "", "", "", "", true);
			if (HtmlReporter.tc_result)
				ROATModules.verify_SucMessg("Verify_Msg", "msg_success");
			if (HtmlReporter.tc_result)
				ROATModules.SE_UpdateBank();
			if (HtmlReporter.tc_result)
				ROATModules.Enter_Authorize_Credential("BankPage", "username", "password", Auth_user1[0], Auth_user1[1], "", "", "", "", true);
			if (HtmlReporter.tc_result)
				ROATModules.verify_SucMessg("Verify_Msg", "msg_success");
			if (HtmlReporter.tc_result)
				ROATModules.SE_bankDelete();
			ROATModules.Enter_Authorize_CredentialDelete("BankPage", "username", "password", Auth_user1[0], Auth_user1[1], "", "", "", "", true);
			if (HtmlReporter.tc_result)
				ROATModules.verify_SucMessg("Verify_Msg", "msg_success");
			//			if (HtmlReporter.tc_result)
			//				ROATValidations.Verify_FieldValidation(2);
			//			if (HtmlReporter.tc_result)
			//			ROATModules.PaginationValidation("BankPage","PaginationTop");
			//			if (HtmlReporter.tc_result)
			//				ROATModules.PaginationValidation("BankPage","PaginationBottom");
			//			if(HtmlReporter.tc_result)
			//				ROATModules.BankSetup_Sorting();

		}catch (Exception e) {
			Reporter.ReportEvent("Test Case execution is failed", "Test Case execution is failed",
					"Test Case execution is failed", false);
		}
	}


	// *********************Authorization Auto
	// Rules********************************
	// *****************************************************************************

	public static void AuthorizationAutoRules()  {
		try {
			ROATFunctions.refesh(false, Launcher.UserRole);
			if (HtmlReporter.tc_result)
				ROATModules.Authorization_navigateAutoRules();
			if (HtmlReporter.tc_result)
				ROATModules.Authorization_AutoRulesView();
			if (HtmlReporter.tc_result)
				ROATModules.Authorization_updateAutoRules();
		} catch (Exception e) {
			Reporter.ReportEvent("Test Case execution is failed", "Test Case execution is failed",
					"Test Case execution is failed", false);
		}
	}

	public static void AuthorizationAutoRulesAddNew()  {
		try {
			ROATFunctions.refesh(false, Launcher.UserRole);
			if (HtmlReporter.tc_result)
				ROATModules.Authorization_navigateAutoRules();
			if (HtmlReporter.tc_result)
				ROATModules.Authorization_addNewAutoRules();
			//			if (HtmlReporter.tc_result)
			//				ROATValidations.Verify_FieldValidation(3);
		} catch (Exception e) {
			Reporter.ReportEvent("Test Case execution is failed", "Test Case execution is failed",
					"Test Case execution is failed", false);
		}
	}

	// ********************Teller matrix**********************************
	public static void TM_TellerMatrix() 
	{
		try 
		{
			ROATFunctions.refesh(false, Launcher.UserRole);
			if (HtmlReporter.tc_result)ROATModules.setup_TellerMatrixNavigation();
			if (HtmlReporter.tc_result)ROATModules.setup_TellerMatrixVerification();
		} catch (Exception e) 
		{
			Reporter.ReportEvent("Test Case execution is failed", "Test Case execution is failed",
					"Test Case execution is failed", false);
		}
	}

	public static void TM_TellerMatrixAddNew()  {
		try {
			ROATFunctions.refesh(false, Launcher.UserRole);
			if (HtmlReporter.tc_result)
				ROATModules.setup_TellerMatrixNavigation();
			if (HtmlReporter.tc_result)
				ROATModules.setup_TellerMatrixAdd();
		} catch (Exception e) {
			Reporter.ReportEvent("Test Case execution is failed", "Test Case execution is failed",
					"Test Case execution is failed", false);
		}
	}

	public static void TM_TellerMatrixUpdate()  {
		try {
			ROATFunctions.refesh(false, Launcher.UserRole);
			if (HtmlReporter.tc_result)
				ROATModules.setup_TellerMatrixNavigation();
			if (HtmlReporter.tc_result)
				ROATModules.setup_TellerMatrixUpdate();
			if (HtmlReporter.tc_result == false)
				ROATFunctions.refesh(true,Launcher.UserRole);
		} catch (Exception e) {
			Reporter.ReportEvent("Test Case execution is failed", "Test Case execution is failed",
					"Test Case execution is failed", false);
		}
	}

	// ******************** Authorisation routing
	// *******************************
	public static void AuthorizationRouting() 
	{
		try
		{
			ROATFunctions.refesh(false, Launcher.UserRole);
			if (HtmlReporter.tc_result)ROATModules.setup_GenericAuthorizationRouting();
			if (HtmlReporter.tc_result == false)
			{
				ROATFunctions.refesh(true,Launcher.UserRole);
			}
		} 
		catch (Exception e) 
		{
			Reporter.ReportEvent("Test Case execution is failed", "Test Case execution is failed","Test Case execution is failed", false);
		}
	}

	public static void Search() 
	{
		try 
		{
			ROATFunctions.refesh(false, Launcher.UserRole);
			if (HtmlReporter.tc_result)ROATModules.setup_SearchBar();
			if (HtmlReporter.tc_result)ROATValidations.Verify_FieldValidation(1);

			ROATFunctions.refesh(true,Launcher.UserRole);

		} 
		catch (Exception e)
		{
			Reporter.ReportEvent("Test Case execution is failed", "Test Case execution is failed","Test Case execution is failed", false);
		}

	}

	public static void QuickLink()
	{
		try 
		{
			ROATFunctions.refesh(false, Launcher.UserRole);
			if (HtmlReporter.tc_result)ROATModules.Setup_QuickLink();
			if (HtmlReporter.tc_result)ROATValidations.Verify_FieldValidation(1);
			ROATFunctions.refesh(true,Launcher.UserRole);

		}
		catch (Exception e)
		{
			Reporter.ReportEvent("Test Case execution is failed", "Test Case execution is failed","Test Case execution is failed", false);
		}
	}

	public static void Favorite() 
	{
		try
		{
			ROATFunctions.refesh(false, Launcher.UserRole);
			if (HtmlReporter.tc_result)ROATModules.Setup_FavoriteBar();
			if (HtmlReporter.tc_result == false)
			{
				ROATFunctions.refesh(true,"Teller");
			}
		} 
		catch (Exception e) 
		{
			Reporter.ReportEvent("Test Case execution is failed", "Test Case execution is failed","Test Case execution is failed", false);
		}
	}

	// ************************** Account Notes Popup
	// ***************************************
	// ****************************************************************************************************

	public static void AccountNotesPopup_TellerandInquiry() 
	{
		try
		{
			ROATFunctions.refesh(false, Launcher.UserRole);
			if (HtmlReporter.tc_result)ROATModules.AccountNotesPopup();
			if (HtmlReporter.tc_result == false)
			{
				ROATFunctions.refesh(true,Launcher.UserRole);
			}

		}
		catch (Exception e)
		{
			Reporter.ReportEvent("Test Case execution is failed", "Test Case execution is failed","Test Case execution is failed", false);
		}
	}
	// **************************Teller Cash
	// Deposit***************************************
	// ****************************************************************************************************

	public static void Teller_Cash_Deposit()
	{
		try
		{
			ROATFunctions.refesh(false, Launcher.UserRole);
			if (HtmlReporter.tc_result)ROATModules.Teller_navigateCashDeposit();
			if (HtmlReporter.tc_result)ROATModules.Teller_BCviewCashDeposit();

			ROATFunctions.refesh(true,Launcher.UserRole);

		} 
		catch (Exception e)
		{
			Reporter.ReportEvent("Test Case execution is failed", "Test Case execution is failed","Test Case execution is failed", false);
		}
	}

	public static void Teller_Cash_DepositEdit() 
	{
		try 
		{
			ROATFunctions.refesh(true, Launcher.UserRole);
			//			if (HtmlReporter.tc_result) {
			//				String user_branch = ROATModules.fetch_user_branch();
			//				
			//				ROATFunctions.refesh(false, "Super User");
			//				if (HtmlReporter.tc_result)
			//					ROATModules.AUTHRem_navigateRemAuthorization();
			//				if (HtmlReporter.tc_result)
			//					ROATModules.updateRemoteAuthorization(user_branch);
			//				ROATFunctions.refesh(false,"Teller");
			//			if (HtmlReporter.tc_result)
			ROATModules.Teller_navigateCashDeposit();
			if (HtmlReporter.tc_result)ROATModules.Teller_AuthorizeCashDeposit();
			if (HtmlReporter.tc_result)ROATModules.Teller_AuthorizeCashDeposit_Edit();
			if (HtmlReporter.tc_result)ROATValidations.Verify_FieldValidation(2);

			ROATFunctions.refesh(true,Launcher.UserRole);

			//		}
		} catch (Exception e) {
			Reporter.ReportEvent("Test Case execution is failed", "Test Case execution is failed",
					"Test Case execution is failed", false);
		}

	}
	// *********************************************Athorization Dual
	// Control*******************************************************
	// *******************************************************************************************************************

	public static void AuthorizationDualControl() 
	{
		try
		{
			ROATFunctions.refesh(false, Launcher.UserRole);
			if (HtmlReporter.tc_result)ROATModules.Authorization_NavigateDualContriol();
			if (HtmlReporter.tc_result)ROATModules.CDA_SysDateValidation("AuthorizationDualControl", "CashDrawer_Date");
			if (HtmlReporter.tc_result)ROATModules.Authorization_ValidateUpdateDualContriol_Authorization();
			if (HtmlReporter.tc_result)ROATModules.Authorization_ValidateUpdateDualContriol_Rejection();
			if (HtmlReporter.tc_result)ROATModules.Authorization_ValidateUpdateDualContriol_Cancel();
			//			if (HtmlReporter.tc_result)
			//				ROATModules.Authorization_ValidateUpdateDualContriol_AddFunctionality();
		}
		catch (Exception e) 
		{
			Reporter.ReportEvent("Test Case execution is failed", "Test Case execution is failed","Test Case execution is failed", false);
		}
	}

	// ***********************************************NotesExchange_FCY*****************************************************
	// *************************************************************************************************************************
	public static void CD_FcyNotesExchange()
	{// succsseful
		// scenario
		try 
		{
			ROATFunctions.refesh(false, Launcher.UserRole);

			Reporter.ReportEvent("Executing flow for postive flow", "Covers successfull senario","Covers successfull senario", true);

			if (HtmlReporter.tc_result)ROATModules.setup_FcyChangeinAmountNavigation();

			if (HtmlReporter.tc_result)ROATModules.FCY_SysDateValidation("FCYChangeinAmount","SystemTimeDate");

			if (HtmlReporter.tc_result)ROATModules.currencySelection();
			if (HtmlReporter.tc_result)ROATModules.enterChangeinAmount("FCYChangeinAmount", "Enter_Amount");
			if (HtmlReporter.tc_result)ROATFunctions.clickEnter();
			if (HtmlReporter.tc_result)ROATModules.verifyChangeInOutAmount();
			if (HtmlReporter.tc_result)ROATModules.enterNoteandCoinIn();
			if (HtmlReporter.tc_result)ROATModules.enter_note_coin_Out("FCYChangeinAmount", "tablenotes1", "tablenotes2", "txtnotes_Out");

			// cancel scenario
			HtmlReporter.tc_result = true; // reseting flag in case previous
			// scenario fails
			Reporter.ReportEvent("Executing cancel functionality flow", "Covers cancel senario",
					"Covers cancel senario", true);
			if (HtmlReporter.tc_result)ROATModules.setup_FcyChangeinAmountNavigation();
			if (HtmlReporter.tc_result)ROATModules.currencySelection();
			if (HtmlReporter.tc_result)ROATModules.enterChangeinAmount("FCYChangeinAmount", "Enter_Amount");
			if (HtmlReporter.tc_result)ROATFunctions.clickEnter();
			if (HtmlReporter.tc_result)ROATModules.enterNoteandCoinIn();
			if (HtmlReporter.tc_result)ROATModules.fn_verify_total_cash_payment_amountIn("FCYChangeinAmount", WebDr.getValue("Enter_Amount"));
			if (HtmlReporter.tc_result)ROATModules.btnCancel();
			if (HtmlReporter.tc_result)ROATModules.clickYesCancle();

			// change in ammt is less than total cash
			HtmlReporter.tc_result = true;
			Reporter.ReportEvent("Executing negative functionality flow: 1","Covers senario when amount is less than total cash","Covers senario when amount is less than total cash", true);
			if (HtmlReporter.tc_result)ROATModules.setup_FcyChangeinAmountNavigation();
			if (HtmlReporter.tc_result)ROATModules.currencySelection();
			if (HtmlReporter.tc_result)ROATModules.enterChangeinAmount("FCYChangeinAmount", "Enter_Amount2");
			if (HtmlReporter.tc_result)ROATFunctions.clickEnter();
			if (HtmlReporter.tc_result)ROATModules.enterNoteandCoinInLess("FCYChangeinAmount"); // Enter

			// amount=1
			if (HtmlReporter.tc_result)ROATModules.fn_verify_total_cash_payment_amountIn("FCYPayment", WebDr.getValue("Enter_Amount2"));
			if (HtmlReporter.tc_result)ROATModules.btnSubmit();
			//			Thread.sleep(5000);// to update teller position
			if (HtmlReporter.tc_result)ROATModules.enter_note_coin_OutAll("FCYChangeinAmount", "tablenotes1", "tablenotes2", "txtnotes_Out"); // total
			// cash
			// is
			// more
			if (HtmlReporter.tc_result)ROATModules.fn_verifyMore_total_cash_payment_amountOut("FCYChangeinAmount",WebDr.getValue("Enter_Amount2"));
			if (HtmlReporter.tc_result)ROATModules.fn_verifyError_Msg("FCYChangeinAmount", WebDr.getValue("Error1"));
			if (HtmlReporter.tc_result)ROATModules.btnCancel();
			if (HtmlReporter.tc_result)ROATModules.clickYesCancle();
			//
			//
			HtmlReporter.tc_result = true;
			Reporter.ReportEvent("Executing negative functionality flow: 2","Covers senario when amount is less than total cash","Covers senario when amount is less than total cash", true);
			if (HtmlReporter.tc_result)ROATModules.setup_FcyChangeinAmountNavigation();
			if (HtmlReporter.tc_result)ROATModules.currencySelection();
			if (HtmlReporter.tc_result)ROATModules.enterChangeinAmount("FCYChangeinAmount", "Enter_Amount1");
			if (HtmlReporter.tc_result)ROATFunctions.clickEnter();
			if (HtmlReporter.tc_result)ROATModules.enterNoteandCoinInMore("FCYChangeinAmount"); // 200*(100

			// notes)=amnt=20000
			if (HtmlReporter.tc_result)ROATModules.fn_verify_total_cash_payment_amountIn("FCYPayment", WebDr.getValue("Enter_Amount1"));
			if (HtmlReporter.tc_result)ROATModules.btnSubmit();
			if (HtmlReporter.tc_result)ROATModules.enter_note_coin_OutAll("FCYChangeinAmount", "tablenotes1", "tablenotes2", "txtnotes_Out"); // enter
			// all
			// notes
			if (HtmlReporter.tc_result)ROATModules.fn_verifyLess_total_cash_payment_amountOut("FCYChangeinAmount",WebDr.getValue("Enter_Amount1"));
			if (HtmlReporter.tc_result)ROATModules.fn_verifyError_Msg("FCYChangeinAmount", WebDr.getValue("Error1"));
			if (HtmlReporter.tc_result)ROATModules.btnCancel();
			if (HtmlReporter.tc_result)ROATModules.clickYesCancle();

			// to verfiy change in amount is less than total cash
			Reporter.ReportEvent("Executing negative functionality flow: 3","Covers senario when change in amount is less than total cash","Covers senario when change in amount is less than total cash", true);
			HtmlReporter.tc_result = true;
			if (HtmlReporter.tc_result)ROATModules.setup_FcyChangeinAmountNavigation();
			if (HtmlReporter.tc_result)ROATModules.currencySelection();
			if (HtmlReporter.tc_result)ROATModules.enterChangeinAmount("FCYChangeinAmount", "Enter_Amount3"); // 2.0
			if (HtmlReporter.tc_result)ROATFunctions.clickEnter();
			if (HtmlReporter.tc_result)ROATModules.enterNoteandCoinInMore("FCYChangeinAmount"); // 200*(100// notes)=amnt=20000
			if (HtmlReporter.tc_result)ROATModules.fn_verifyMore1_total_cash_payment_amountOut("FCYChangeinAmount",WebDr.getValue("Enter_Amount3"));
			if (HtmlReporter.tc_result)ROATModules.fn_verifyError_Msg2("FCYChangeinAmount", WebDr.getValue("Error2"));
			if (HtmlReporter.tc_result)ROATModules.btnCancel();
			if (HtmlReporter.tc_result)ROATModules.clickYesCancle();

			// to verfiy change in amount is more than total cash
			HtmlReporter.tc_result = true;
			Reporter.ReportEvent("Executing negative functionality flow: 4","Covers senario when  change in amount is more than total cash ","Covers senario when  change in amount is more than total cash ", true);
			if (HtmlReporter.tc_result)ROATModules.setup_FcyChangeinAmountNavigation();
			if (HtmlReporter.tc_result)ROATModules.currencySelection();
			if (HtmlReporter.tc_result)ROATModules.enterChangeinAmount("FCYChangeinAmount", "Enter_Amount4");
			if (HtmlReporter.tc_result)ROATFunctions.clickEnter();
			if (HtmlReporter.tc_result)ROATModules.enterNoteandCoinInLess("FCYChangeinAmount"); // 200*(100 notes)=amnt=20000
			if (HtmlReporter.tc_result)ROATModules.fn_verifyLess1_total_cash_payment_amountOut("FCYChangeinAmount",WebDr.getValue("Enter_Amount4"));
			if (HtmlReporter.tc_result)ROATModules.fn_verifyError_Msg2("FCYChangeinAmount", WebDr.getValue("Error2"));
			if (HtmlReporter.tc_result)ROATModules.btnCancel();
			if (HtmlReporter.tc_result)ROATModules.clickYesCancle();
		}
		catch (Exception e)
		{
			Reporter.ReportEvent("Test Case execution is failed", "Test Case execution is failed","Test Case execution is failed", false);
		}

	}

	public static void forexRateInquiry() 
	{
		try 
		{
			ROATFunctions.refesh(false, Launcher.UserRole);
			if (HtmlReporter.tc_result)ROATModules.navigateForexRateInquiry();
			if (HtmlReporter.tc_result)ROATModules.forexRateInquiryVerification();
		}
		catch (Exception e) 
		{
			Reporter.ReportEvent("Test Case execution is failed", "Test Case execution is failed","Test Case execution is failed", false);
		}
	}

	public static void forexRateUpdate() 
	{
		try
		{
			ROATFunctions.refesh(false, Launcher.UserRole);
			// cancel scenario
			if (HtmlReporter.tc_result)
				ROATModules.navigateForexRateUpdate();
			if (HtmlReporter.tc_result)
				ROATModules.performBranchSelection();
			if (HtmlReporter.tc_result)
				ROATModules.forexRateUpdate();
			if (HtmlReporter.tc_result)
				ROATModules.cancelForexRateUpdate();

			// success scenario
			if (HtmlReporter.tc_result)
				ROATModules.navigateForexRateUpdate();
			if (HtmlReporter.tc_result)
				ROATModules.performBranchSelection();
			if (HtmlReporter.tc_result)
				ROATModules.forexRateUpdate();
			if (HtmlReporter.tc_result)
				ROATModules.verifySuccessForexRateUpdate();

			if (HtmlReporter.tc_result)
				ROATModules.clickBackBtn();

			// reject scenario
			if (HtmlReporter.tc_result)
				ROATModules.navigateForexRateUpdate();
			if (HtmlReporter.tc_result)
				ROATModules.performBranchSelection();
			if (HtmlReporter.tc_result)
				ROATModules.forexRateUpdate();
			if (HtmlReporter.tc_result)
				ROATModules.verifyRejectForexRateUpdate();

			// validation
			if (HtmlReporter.tc_result)
				ROATModules.navigateForexRateUpdate();
			if (HtmlReporter.tc_result)
				ROATModules.performBranchSelection();
			boolean temp=HtmlReporter.tc_result;
			if (HtmlReporter.tc_result)
				ROATValidations.Verify_FieldValidation(6);
			if(!HtmlReporter.tc_result)
			{
				HtmlReporter.tc_result=temp;
			}

		} catch (Exception e) 
		{
			Reporter.ReportEvent("Test Case execution is failed", "Test Case execution is failed","Test Case execution is failed", false);
		}
	}

	public static void CD_LcyNotesExchange() throws Exception 
	{
		ROATFunctions.refesh(false, Launcher.UserRole);
		if (HtmlReporter.tc_result)
			try 
		{
				ROATValidations.Verify_FieldValidation(1);
				HtmlReporter.tc_result = true;

		} 
		catch (Exception e1) 
		{
			e1.printStackTrace();
			Reporter.ReportEvent("Test Case execution is failed", "Test Case execution is failed","Test Case execution is failed", false);
		}


		// succsseful scenario
		Reporter.ReportEvent("Executing flow for postive flow", "Covering successfull senario","Covers successfull senario", true);
		try 
		{
			if (HtmlReporter.tc_result)ROATModules.setup_LcyChangeinAmountNavigation();
			if (HtmlReporter.tc_result)ROATModules.LCY_SysDateValidation("LCYChangeinAmount","SystemTimeDate");
			if (HtmlReporter.tc_result)ROATModules.enterChangeinAmount("LCYChangeinAmount", "Enter_Amount");
			if (HtmlReporter.tc_result)ROATFunctions.clickEnter();
			if (HtmlReporter.tc_result)ROATModules.verifyChangeInOutAmount();
			if (HtmlReporter.tc_result)ROATModules.enterNoteandCoinIn_lcy();
			if (HtmlReporter.tc_result)ROATModules.enter_note_coin_Out("LCYChangeinAmount", "tablenotes1", "tablenotes2", "txtnotes_Out");
		}
		catch (Exception e) 
		{
			e.printStackTrace();
			Reporter.ReportEvent("Test Case execution is failed", "Test Case execution is failed","Test Case execution is failed", false);
		}

		// cancel scenario
		Reporter.ReportEvent("Executing cancel functionality flow", "Covering cancel senario","Covering cancel senario", true);
		if (HtmlReporter.tc_result)
			try 
		{
				if (HtmlReporter.tc_result)ROATModules.setup_LcyChangeinAmountNavigation();
				if (HtmlReporter.tc_result)ROATModules.enterChangeinAmount("LCYChangeinAmount", "Enter_Amount");
				if (HtmlReporter.tc_result)ROATFunctions.clickEnter();
				if (HtmlReporter.tc_result)ROATModules.enterNoteandCoinIn_lcy();
				if (HtmlReporter.tc_result)ROATModules.fn_verify_total_cash_payment_amountIn("LCYChangeinAmount",WebDr.getValue("Enter_Amount"));
				if (HtmlReporter.tc_result)ROATModules.btnCancel();
				if (HtmlReporter.tc_result)ROATModules.clickYesCancle();
		}
		catch (Exception e)
		{
			e.printStackTrace();
			Reporter.ReportEvent("Test Case execution is failed", "Test Case execution is failed","Test Case execution is failed", false);
		}


		// change in ammt is less than total cash
		Reporter.ReportEvent("Executing negative functionality flow: 1","Covers senario when amount is less than total cash","Covers senario when amount is less than total cash", true);
		HtmlReporter.tc_result=true;
		try
		{
			if (HtmlReporter.tc_result)ROATModules.setup_LcyChangeinAmountNavigation();
			if (HtmlReporter.tc_result)ROATModules.enterChangeinAmount("LCYChangeinAmount", "Enter_Amount2");
			if (HtmlReporter.tc_result)ROATFunctions.clickEnter();
			if (HtmlReporter.tc_result)ROATModules.enterNoteandCoinInLess("LCYChangeinAmount"); // Enter amount=1
			if (HtmlReporter.tc_result)ROATModules.fn_verify_total_cash_payment_amountIn("LCYPayment", WebDr.getValue("Enter_Amount2"));
			if (HtmlReporter.tc_result)ROATModules.btnSubmit();
			if (HtmlReporter.tc_result)ROATModules.enter_note_coin_OutAll("LCYChangeinAmount", "tablenotes1", "tablenotes2", "txtnotes_Out"); // total

			// cash is more
			if (HtmlReporter.tc_result)ROATModules.fn_verifyMore_total_cash_payment_amountOut("LCYChangeinAmount",WebDr.getValue("Enter_Amount2"));
			if (HtmlReporter.tc_result)ROATModules.fn_verifyError_Msg("LCYChangeinAmount", WebDr.getValue("Error1"));
			if (HtmlReporter.tc_result)ROATModules.btnCancel();
			if (HtmlReporter.tc_result)ROATModules.clickYesCancle();
		}
		catch (Exception e)
		{
			e.printStackTrace();
			Reporter.ReportEvent("Test Case execution is failed", "Test Case execution is failed","Test Case execution is failed", false);
		}


		Reporter.ReportEvent("Executing negative functionality flow: 2","Covers senario when amount is less than total cash","Covers senario when amount is less than total cash", true);
		HtmlReporter.tc_result=true;
		try 
		{
			if (HtmlReporter.tc_result)ROATModules.setup_LcyChangeinAmountNavigation();
			if (HtmlReporter.tc_result)ROATModules.enterChangeinAmount("LCYChangeinAmount", "Enter_Amount1");
			if (HtmlReporter.tc_result)ROATFunctions.clickEnter();
			if (HtmlReporter.tc_result)ROATModules.enterNoteandCoinInMore("LCYChangeinAmount"); // 200*(100 notes)=amnt=20000
			if (HtmlReporter.tc_result)ROATModules.fn_verify_total_cash_payment_amountIn("FCYPayment", WebDr.getValue("Enter_Amount1"));
			if (HtmlReporter.tc_result)ROATModules.btnSubmit();
			if (HtmlReporter.tc_result)ROATModules.enter_note_coin_OutAll("LCYChangeinAmount", "tablenotes1", "tablenotes2", "txtnotes_Out"); // enter
			// all notes
			if (HtmlReporter.tc_result)ROATModules.fn_verifyLess_total_cash_payment_amountOut("LCYChangeinAmount",WebDr.getValue("Enter_Amount1"));
			if (HtmlReporter.tc_result)ROATModules.fn_verifyError_Msg("LCYChangeinAmount", WebDr.getValue("Error1"));
			if (HtmlReporter.tc_result)ROATModules.btnCancel();
			if (HtmlReporter.tc_result)ROATModules.clickYesCancle();
		} 
		catch (Exception e) 
		{
			e.printStackTrace();
			Reporter.ReportEvent("Test Case execution is failed", "Test Case execution is failed","Test Case execution is failed", false);
		}

		// //to verfiy change in amount is less than total cash
		HtmlReporter.tc_result=true;
		Reporter.ReportEvent("Executing negative functionality flow: 3","Covers senario when change in amount is less than total cash","Covers senario when change in amount is less than total cash", true);
		try 
		{
			if (HtmlReporter.tc_result)ROATModules.setup_LcyChangeinAmountNavigation();
			if (HtmlReporter.tc_result)ROATModules.enterChangeinAmount("LCYChangeinAmount", "Enter_Amount3"); // 2.0
			if (HtmlReporter.tc_result)ROATFunctions.clickEnter();
			if (HtmlReporter.tc_result)ROATModules.enterNoteandCoinInMore("LCYChangeinAmount"); // 200*(100notes)=amnt=20000
			if (HtmlReporter.tc_result)ROATModules.fn_verifyMore1_total_cash_payment_amountOut("LCYChangeinAmount",WebDr.getValue("Enter_Amount3"));
			if (HtmlReporter.tc_result)ROATModules.fn_verifyError_Msg2("LCYChangeinAmount", WebDr.getValue("Error2"));
			if (HtmlReporter.tc_result)ROATModules.btnCancel();
			if (HtmlReporter.tc_result)ROATModules.clickYesCancle();
		}
		catch (Exception e) 
		{
			e.printStackTrace();
			Reporter.ReportEvent("Test Case execution is failed", "Test Case execution is failed","Test Case execution is failed", false);
		}

		// to verfiy change in amount is more than total cash
		HtmlReporter.tc_result=true;
		Reporter.ReportEvent("Executing negative functionality flow: 4","Covers senario when  change in amount is more than total cash ","Covers senario when  change in amount is more than total cash ", true);
		try 
		{
			if (HtmlReporter.tc_result)ROATModules.setup_LcyChangeinAmountNavigation();
			if (HtmlReporter.tc_result)ROATModules.enterChangeinAmount("LCYChangeinAmount", "Enter_Amount4");
			if (HtmlReporter.tc_result)ROATFunctions.clickEnter();
			if (HtmlReporter.tc_result)ROATModules.enterNoteandCoinInLess("LCYChangeinAmount"); // 200*(100notes)=amnt=20000
			if (HtmlReporter.tc_result)ROATModules.fn_verifyLess1_total_cash_payment_amountOut("LCYChangeinAmount",WebDr.getValue("Enter_Amount4"));
			if (HtmlReporter.tc_result)ROATModules.fn_verifyError_Msg2("FCYChangeinAmount", WebDr.getValue("Error2"));
			if (HtmlReporter.tc_result)ROATModules.btnCancel();
			if (HtmlReporter.tc_result)ROATModules.clickYesCancle();
		}
		catch (Exception e) 
		{
			e.printStackTrace();
			Reporter.ReportEvent("Test Case execution is failed", "Test Case execution is failed","Test Case execution is failed", false);
		}

	}

	/// Sprint 8

	public static void Vault_LCY_PaymentToATM() 
	{
		try 
		{
			ROATFunctions.refesh(false, Launcher.UserRole);
			if (HtmlReporter.tc_result)ROATModules.navigate_VaultLCY_PaymentToTransfer();
			if (HtmlReporter.tc_result)ROATModules.VaultLCY_PaymentToTransfer_EnterInformation();
			if (HtmlReporter.tc_result)ROATModules.VaultLCY_PaymentToTransfer_CancelFunctionality();
			if (HtmlReporter.tc_result)ROATModules.VaultLCY_PaymentToTransfer_EnterInformation();
			if (HtmlReporter.tc_result)ROATFunctions.Vault_PaymentToATM_DenominationCount("Vault_LCY_PaymentToATM",
					"Vault_LCY_PTA_DenomPosSize", "Vault_LCY_PTA_DenomCount", "CDA_TotalCashAmount",
					"Vault_LCY_PTA_PaymentAmount");
			if (HtmlReporter.tc_result)ROATModules.setup_LCYAuthorize();
			if (HtmlReporter.tc_result)ROATModules.VaultLCY_PaymentToTransfer_TotalPaymentVerification();
			if (HtmlReporter.tc_result)ROATModules.VaultLCY_PaymentToTransfer_EnterInformation();
			if (HtmlReporter.tc_result)ROATFunctions.Vault_PaymentToATM_DenominationCount("Vault_LCY_PaymentToATM",
					"Vault_LCY_PTA_DenomPosSize", "Vault_LCY_PTA_DenomCount", "CDA_TotalCashAmount",
					"Vault_LCY_PTA_PaymentAmount");
			if (HtmlReporter.tc_result)ROATModules.setup_LCYReject();
		} 
		catch (Exception e)
		{
			Reporter.ReportEvent("Test Case execution is failed", "Test Case execution is failed","Test Case execution is failed", false);
		}
	}

	public static void Teller_Withdrawal() 
	{
		try
		{
			ROATFunctions.refesh(false, Launcher.UserRole);
			if (HtmlReporter.tc_result)ROATModules.navigate_TellerWithdrawal();
			if (HtmlReporter.tc_result)ROATModules.TellerWithdrawal_EnterDetails();
			if (HtmlReporter.tc_result)ROATModules.TellerWithdrawal_CancelFun();

			if (HtmlReporter.tc_result)ROATModules.TellerWithdrawal_EnterDetails();
			if (HtmlReporter.tc_result)ROATFunctions.TellerPosition_Count("Teller_Withdrawal", "Teller_With_CurrentTelPosSize",
					"Teller_With_CurrentCount", "txt_soiled", "txt_Decoy", "CDA_TotalCashAmount",
					"txt_PaymentAmount");
			if (HtmlReporter.tc_result)ROATModules.TellerWithdrawal_AgentDetails();

			if (HtmlReporter.tc_result)ROATModules.TellerWithdrawal_EnterDetails();
			if (HtmlReporter.tc_result)ROATFunctions.TellerPosition_Count("Teller_Withdrawal", "Teller_With_CurrentTelPosSize",
					"Teller_With_CurrentCount", "txt_soiled", "txt_Decoy", "CDA_TotalCashAmount",
					"txt_PaymentAmount");
			if (HtmlReporter.tc_result)ROATModules.TellerWithdrawal_CancelFun();
			if (HtmlReporter.tc_result == false)
			{
				ROATFunctions.refesh(true,Launcher.UserRole);
			} 
		}
		catch (Exception e) 
		{
			Reporter.ReportEvent("Test Case execution is failed", "Test Case execution is failed",
					"Test Case execution is failed", false);
		}
	}
	// *************************** Vault LCY Inquiry
	// *********************************************
	// *****************************************************************************************

	public static void VaultLCYInquiry()
	{
		try {
			ROATFunctions.refesh(false, Launcher.UserRole);

			if (HtmlReporter.tc_result)
				ROATModules.Navigate_VaultLCYBreakdown();
			if (HtmlReporter.tc_result)
				ROATModules.Vault_LCYInquiry();
		} catch (Exception e) {
			Reporter.ReportEvent("Test Case execution is failed", "Test Case execution is failed",
					"Test Case execution is failed", false);
		}
	}

	// ********************* Vault LCY Payment to CDC
	// *****************************************
	// ****************************************************************************************

	public static void VaultLCYPaymentCDC()  {
		try {
			ROATFunctions.refesh(true, Launcher.UserRole);
			if (HtmlReporter.tc_result)
				ROATModules.Navigate_VaultLCYPaymentCDC();
			if (HtmlReporter.tc_result)
				ROATModules.ViewLCYPaymentCDC();
			if (HtmlReporter.tc_result)
				ROATModules.Vault_LCYPaymentToCDCReject();
			if (HtmlReporter.tc_result)
				ROATModules.Vault_LCYPaymentToCDCCancel();
			//			if (HtmlReporter.tc_result)
			//				ROATValidations.Verify_FieldValidation(3);
			if (HtmlReporter.tc_result)
				ROATModules.Vault_LCYPaymentToCDCAuthorize();
		} catch (Exception e) {
			Reporter.ReportEvent("Test Case execution is failed", "Test Case execution is failed",
					"Test Case execution is failed", false);
		}
	}

	// ********************* Vault LCY Receipt to CDC
	// *****************************************
	// ****************************************************************************************

	public static void VaultLCYReceiptCDC()  {

		try {
			ROATFunctions.refesh(true, Launcher.UserRole);
			if (HtmlReporter.tc_result)
				ROATModules.Navigate_VaultLCYReceiptCDC();
			if (HtmlReporter.tc_result)
				ROATModules.ViewLCYReceiptCDC();
			if (HtmlReporter.tc_result)
				ROATModules.Vault_LCYReceiptFromCDCReject();
			if (HtmlReporter.tc_result)
				ROATModules.Vault_LCYReceiptFromCDCCancel();
			if (HtmlReporter.tc_result)
				ROATModules.Vault_LCYReceiptFromCDCAuthorize();
		} catch (Exception e) {
			Reporter.ReportEvent("Test Case execution is failed", "Test Case execution is failed",
					"Test Case execution is failed", false);
		}
	}

	// ********************* Vault FCY Payment to CDC
	// *****************************************
	// ****************************************************************************************

	public static void VaultFCYPaymentCDC()  {

		try {
			ROATFunctions.refesh(true, Launcher.UserRole);
			if (HtmlReporter.tc_result)
				ROATModules.Navigate_VaultFCYPaymentCDC();
			if (HtmlReporter.tc_result)
				ROATModules.ViewFCYPaymentCDC();
			if (HtmlReporter.tc_result)
				ROATModules.Vault_FCYPaymentToCDCReject();
			if (HtmlReporter.tc_result)
				ROATModules.Vault_FCYPaymentToCDCAccept();
			if (HtmlReporter.tc_result)
				ROATModules.Vault_FCYPaymentToCDCCancel();		

		} catch (Exception e) {
			Reporter.ReportEvent("Test Case execution is failed", "Test Case execution is failed",
					"Test Case execution is failed", false);
		}
	}
	// ********************* Vault FCY Receipt from CDC
	// *****************************************
	// ****************************************************************************************

	public static void VaultFCYReceiptCDC()  {

		try {
			ROATFunctions.refesh(true, Launcher.UserRole);
			if (HtmlReporter.tc_result)
				ROATModules.Navigate_VaultFCYReceiptfromCDC();
			if (HtmlReporter.tc_result)
				ROATModules.ViewFCYReceiptCDC();
			if (HtmlReporter.tc_result)
				ROATModules.Vault_FCYReceiptFromCDCReject();
			if (HtmlReporter.tc_result)
				ROATModules.Vault_FCYReceiptFromCDCCancel();
			if
			(HtmlReporter.tc_result)ROATModules.Vault_FCYReceiptfromCDCAuthorize();
		} catch (Exception e) {
			Reporter.ReportEvent("Test Case execution is failed", "Test Case execution is failed",
					"Test Case execution is failed", false);
		}
	}

	public static void add_delete_StopCheque()  {
		try {
			ROATFunctions.refesh(false, Launcher.UserRole);
			// add scenario
			if (HtmlReporter.tc_result)
				ROATModules.navigateAddStoppedCheque();
			if (HtmlReporter.tc_result)
				ROATModules.searchAccountNumber();
			if (HtmlReporter.tc_result)
				ROATFunctions.clickEnter();
			if (HtmlReporter.tc_result)
				ROATModules.addStopCheque();
			ROATFunctions.refesh(false, "Supervisor");
			// delete scenario
			if (HtmlReporter.tc_result)
				ROATModules.navigateDeleteStoppedCheque();
			if (HtmlReporter.tc_result)
				ROATModules.searchAccountNumber();
			if (HtmlReporter.tc_result)
				ROATFunctions.clickEnter();
			if (HtmlReporter.tc_result)
				ROATModules.deleteStopCheque();
			ROATFunctions.refesh(false, "Teller");
			// validation
			if (HtmlReporter.tc_result)
				ROATModules.navigateAddStoppedCheque();
			if (HtmlReporter.tc_result)
				ROATModules.searchAccountNumber();
			if (HtmlReporter.tc_result)
				ROATFunctions.clickEnter();
			//			if (HtmlReporter.tc_result)
			//				ROATValidations.Verify_FieldValidation(4);
		} catch (Exception e) {
			Reporter.ReportEvent("Test Case execution is failed", "Test Case execution is failed",
					"Test Case execution is failed", false);
		}
	}

	public static void view_update_StopCheque()  {
		try {
			ROATFunctions.refesh(false, Launcher.UserRole);
			// view
			if (HtmlReporter.tc_result)
				ROATModules.navigateViewStoppedCheque();
			if (HtmlReporter.tc_result)
				ROATModules.searchAccountNumber();
			if (HtmlReporter.tc_result)
				ROATFunctions.clickEnter();
			WebDr.SetPageObjects("StopChequeInquiryUpdate");
			if (HtmlReporter.tc_result)
				WebDr.waitTillElementIsDisplayed("back_btn");
			if (WebDr.isExists("message") != 0) {
				String message = WebDr.getElement("message").getText();
				String noRecord = WebDr.getValue("No_record");
				if (message.contains(noRecord)) {
					Reporter.ReportEvent("No records present", "No records present", "No records present", true);
				} else {
					Reporter.ReportEvent("Could not verify stop cheque", "Could not verify stop cheque",
							"Could not verify stop cheque", false);
				}
			} else {
				if (HtmlReporter.tc_result)
					ROATModules.viewStopCheque();
				ROATFunctions.refesh(false, "Supervisor");
				// update
				if (HtmlReporter.tc_result)
					ROATModules.navigateUpdateStoppedCheque();
				if (HtmlReporter.tc_result)
					ROATModules.searchAccountNumber();
				if (HtmlReporter.tc_result)
					ROATFunctions.clickEnter();
				if (HtmlReporter.tc_result)
					ROATModules.updateStopCheque();
				if (HtmlReporter.tc_result)
					ROATModules.updateStopChequeSuccess();
				if (HtmlReporter.tc_result)
					ROATModules.updateStopCheque();
				if (HtmlReporter.tc_result)
					ROATModules.updateStopChequeCancel();

				// validation
				if (HtmlReporter.tc_result)
					ROATModules.searchAccountNumber();
				if (HtmlReporter.tc_result)
					ROATFunctions.clickEnter();
				if (HtmlReporter.tc_result)
					ROATModules.updateStopCheque();
				if (HtmlReporter.tc_result)
					ROATValidations.Verify_FieldValidation(4);
			}
		} catch (Exception e) {
			Reporter.ReportEvent("Test Case execution is failed", "Test Case execution is failed",
					"Test Case execution is failed", false);
		}
	}

	// ***********************************************Transaction code*******************************************************
	// *************************************************************************************************************************

	public static void SetUp_TransactionCode()  {
		try {
			ROATFunctions.refesh(false, Launcher.UserRole);
			if (HtmlReporter.tc_result)
				ROATModules.setup_TransactionCodeNavigation();
			if (HtmlReporter.tc_result)
				ROATModules.ViewTransctionCode();
			if(HtmlReporter.tc_result)
				ROATModules.ViewTransctionCodeBreadCrumb();
			if (HtmlReporter.tc_result)
				ROATFunctions.CDA_SysDateValidation("TransactionCodeNew","TransactionCode_Setup_Date");
			if (HtmlReporter.tc_result)
				ROATModules.updateTransactionCode();
			//			if (HtmlReporter.tc_result)
			//				ROATModules.buttonAccept("accept");
			if (HtmlReporter.tc_result)
				ROATModules.verify_message("succ_mgs", "msg_success");
			//			if (HtmlReporter.tc_result)
			//				ROATValidations.Verify_FieldValidation(1);

		} catch (Exception e) {
			Reporter.ReportEvent("Test Case execution is failed", "Test Case execution is failed",
					"Test Case execution is failed", false);
		}
	}

	// ***********************************************Forex_Teller_Matrix*******************************************************
	// *************************************************************************************************************************
	public static void Forex_Matrix() {

		try {
			ROATFunctions.refesh(false, Launcher.UserRole);
			if (HtmlReporter.tc_result)
				ROATModules.Forex_MatrixNavigation();
			if (HtmlReporter.tc_result)
				ROATModules.Select_Branch_Search("ForexMatrix", "Branch_Number");
			if (HtmlReporter.tc_result)
				ROATModules.Forex_MatrixVerification();
			HtmlReporter.tc_result = true;
			if (HtmlReporter.tc_result)
				ROATModules.Forex_MatrixNavigation();
			if (HtmlReporter.tc_result)
				ROATModules.Select_Branch_Search("ForexMatrix", "Branch_Number_Add");
			if (HtmlReporter.tc_result)
				ROATModules.Forex_MatrixAdd();

			HtmlReporter.tc_result = true;
			if (HtmlReporter.tc_result)
				ROATModules.Forex_MatrixNavigation();
			if (HtmlReporter.tc_result)
				ROATModules.Select_Branch_Search("ForexMatrix", "Branch_Number");
			if (HtmlReporter.tc_result)
				ROATModules.Forex_MatrixUpdate();
		} catch (Exception e) {
			e.printStackTrace();
		}

	}

	public static void Vault_FCYBreakdown()  {
		try{



			ROATFunctions.refesh(false, Launcher.UserRole);
			/*WebDr.SetPageObjects("VaultSummary");

			if (HtmlReporter.tc_result)
				ROATModules.navigateVault_FCYBreakdown();

			String Auth_user[] = Launcher.Auth_User_Login("Vault Custodian");
			String Login[] = Launcher.User_Login("Vault Custodian");
			if (HtmlReporter.tc_result)
				ROATModules.authorize_Credential_Vault("VaultSummary","txt_AuthorizerUser", "txt_AuthorizerPass","username2","password2",Auth_user[0], Auth_user[1],Login[0], Login[1], true);
			 */
			if (HtmlReporter.tc_result)ROATModules.FCYB_VerificationAndAuthorization();
			if (HtmlReporter.tc_result)ROATModules.setup_BalanceFCYVerification_VaultBreakdown();					
			if (HtmlReporter.tc_result)ROATModules.Vault_FCYBreakdown_ExpandCollapse();
			//if (HtmlReporter.tc_result)ROATModules.setup_FCYDownload();
		}
		catch(Exception e)
		{
			Reporter.ReportEvent("Test Case execution is failed", "Test Case execution is failed",
					"Test Case execution is failed", false);
		}
	}
	// ***************** Packages Charge packages *************************
	// *********************************************************************

	public static void PackagesChargePackages() 
	{
		try 
		{
			ROATFunctions.refesh(false, Launcher.UserRole);
			String ChargePack = null;
			if (HtmlReporter.tc_result)ROATModules.Packages_navigateChargePackages();
			ChargePack = ROATModules.Packages_AddChargePackage();
			if (HtmlReporter.tc_result)ROATModules.Packages_CancelAddChargePackage();
			/*if (HtmlReporter.tc_result)
				ROATModules.Packages_ViewChargePackages();*/
			if (HtmlReporter.tc_result)ROATModules.Packages_UpdateChargePackage(ChargePack);
			if (HtmlReporter.tc_result)ROATModules.Packages_DeleteChargePackage(ChargePack);
			// if
			// (HtmlReporter.tc_result)ROATModules.Packages_ActivityChargePackage(ChargePack);
			if (HtmlReporter.tc_result)ROATValidations.Verify_FieldValidation(1);
			if (HtmlReporter.tc_result == false)
			{
				ROATFunctions.refesh(true,Launcher.UserRole);
			} 
		} 
		catch (Exception e) 
		{
			Reporter.ReportEvent("Test Case execution is failed", "Test Case execution is failed",
					"Test Case execution is failed", false);
		}
	}
	// ************************** Tariff Maintenence
	// *****************************
	// ***************************************************************************

	public static void SetupTariffMaintenence() 
	{
		try
		{
			ROATFunctions.refesh(false, Launcher.UserRole);
			if (HtmlReporter.tc_result)ROATModules.Setup_navigateTariffMaintenence();
			if (HtmlReporter.tc_result)ROATModules.Setup_ViewTariffMaintenence();
			if (HtmlReporter.tc_result)ROATModules.Setup_UpdateTariffMaintenece();
			if (HtmlReporter.tc_result)ROATModules.Setup_DeleteTariffMaintenece();
			if (HtmlReporter.tc_result)ROATValidations.Verify_FieldValidation(3);
		}
		catch (Exception e)
		{
			Reporter.ReportEvent("Test Case execution is failed", "Test Case execution is failed",
					"Test Case execution is failed", false);
		}
	}

	// ********************* Teller
	// Charges**********************************************************************
	// ****************************************************************************************

	public static void Teller_Charges()
	{
		try 
		{
			ROATFunctions.refesh(false, Launcher.UserRole);
			if (HtmlReporter.tc_result)
				ROATModules.navigate_TellerCharge();
			ROATFunctions.refesh(false, "Super User");
			if (HtmlReporter.tc_result)
				ROATModules.setup_navigateTariffMaintenance();
			if (HtmlReporter.tc_result)
				ROATModules.TellerCharge_TariffMainView();
			ROATFunctions.refesh(false, "Head Teller");
			if (HtmlReporter.tc_result)
				ROATModules.navigate_TellerCharge();
			if (HtmlReporter.tc_result)ROATModules.TellerCharge_CancelFun();
			if (HtmlReporter.tc_result)
				ROATModules.TellerCharge_SignPadCancelFun();
			//			if (HtmlReporter.tc_result)
			//				ROATModules.TellerCharge_EnterAccountInfo();
			//			if (HtmlReporter.tc_result == false) {
			//				ROATFunctions.refesh(true,Launcher.UserRole);
			//			}

		}
		catch (Exception e)
		{
			Reporter.ReportEvent("Test Case execution is failed", "Test Case execution is failed",
					"Test Case execution is failed", false);
		}
	}

	// *********** statements *********************
	public static void SI_Inquirystatements() throws Exception
	{
		try 
		{
			ROATFunctions.refesh(false, Launcher.UserRole);

			if (HtmlReporter.tc_result)ROATModules.AI_navigateAccountInquirystatements();
			if (HtmlReporter.tc_result)ROATModules.statementpage();
			if (HtmlReporter.tc_result)
			{
				Map<String, String> a = ROATModules.storevalues("statements", "stat");
				if (HtmlReporter.tc_result)ROATModules.accounthistoryinquirycheck();
				// compare both statment and account history inquiry page
				if (HtmlReporter.tc_result)
				{
					Map<String, String> b = ROATModules.storevalues("statements", "inquiryhistory");
					if (HtmlReporter.tc_result)ROATModules.comparestat(a, b);
					if (HtmlReporter.tc_result)ROATModules.AI_navigateAccountInquirystatements();
					if (HtmlReporter.tc_result)ROATModules.statementpage();
					if (HtmlReporter.tc_result)ROATModules.statementsignature();
					if (HtmlReporter.tc_result)
					{
						Map<String, String> d = ROATModules.storevalues("statements", "sign");
						if (HtmlReporter.tc_result)WebDr.waitTillElementIsDisplayed("Click_signatureclose");
						if (HtmlReporter.tc_result)WebDr.click("X", "Click_signatureclose", "Clicking on signature button", true);
						// compare both signature and account inquiry page
						if (HtmlReporter.tc_result)ROATModules.accountinquirycheck();
						if (HtmlReporter.tc_result)
						{
							Map<String, String> c = ROATModules.storevalues("statements", "inquiry");
							if (HtmlReporter.tc_result)ROATModules.comparestat(d, c);
							//back  function
							if (HtmlReporter.tc_result)ROATModules.AI_navigateAccountInquirystatements();
							if (HtmlReporter.tc_result)ROATModules.statementpage();
							if (HtmlReporter.tc_result)ROATModules.statementback();
						}
					}
				}
			}
			//and print function
			if (HtmlReporter.tc_result)ROATModules.AI_navigateAccountInquirystatements();
			if (HtmlReporter.tc_result)ROATModules.statementpage();
			if (HtmlReporter.tc_result)ROATModules.statementprint();

			//			if (HtmlReporter.tc_result)ROATModules.AI_navigateAccountInquirystatements();
			//			if (HtmlReporter.tc_result)ROATValidations.Verify_FieldValidation(1);
		} 
		catch (Exception e) 
		{
			Reporter.ReportEvent("Test Case execution is failed", "Test Case execution is failed","Test Case execution is failed", false);
		}

		ROATFunctions.refesh(true, Launcher.UserRole);
	}

	// ********************* Packages Rate
	// Package*****************************************
	// ****************************************************************************************

	public static void Packages_RatePackages()
	{
		try 
		{
			ROATFunctions.refesh(false, Launcher.UserRole);
			if (HtmlReporter.tc_result)ROATModules.navigate_RatePakages();
			if (HtmlReporter.tc_result)ROATModules.AddNew_MarketSegment_CancelFunc();
			if (HtmlReporter.tc_result)ROATModules.PaginationValidation("Packages_RatePackages", "PaginationTop");
			if (HtmlReporter.tc_result)ROATModules.PaginationValidation("Packages_RatePackages", "PaginationBottom");

			if (HtmlReporter.tc_result)ROATModules.AddNew_MarketSegment_AuthFunc();
			/*if (HtmlReporter.tc_result)
						ROATModules.AddNew_MarkSeg_DupMsgFunc();*/
			if (HtmlReporter.tc_result)ROATModules.RatePackage_ViewAddNewValFunc();
			if (HtmlReporter.tc_result)ROATModules.AddNew_AcountType_AuthorizationFun();
			if (HtmlReporter.tc_result)ROATModules.RatePackage_ViewFunc();
			if (HtmlReporter.tc_result)ROATModules.RatePackage_Update_CancelFunc();
			if (HtmlReporter.tc_result)ROATModules.RatePackage_UpdateRejectFunc();
			if (HtmlReporter.tc_result)ROATModules.RatePackage_UpdateFunc();
			if (HtmlReporter.tc_result)ROATModules.RatePackage_ViewValidationFunc();

			if (HtmlReporter.tc_result)ROATModules.RatePackage_MarkUp_CancelFunc();
			if (HtmlReporter.tc_result)ROATModules.RatePackage_MarkUp_RejectionFunc();
			if (HtmlReporter.tc_result)ROATModules.RatePackage_MarkUpFunc();
			if (HtmlReporter.tc_result)ROATModules.RatePackage_ViewMarkUpFunc();
			if (HtmlReporter.tc_result)ROATModules.RatePackage_UpdateMarkUpFunc();
			if (HtmlReporter.tc_result)ROATModules.RatePackage_ViewMarkUpFunc();
			if (HtmlReporter.tc_result)ROATModules.RatePackage_DeleteMarkUpFunc();

			if (HtmlReporter.tc_result)ROATModules.RatePackage_DeleteFunc_No();
			if (HtmlReporter.tc_result)ROATModules.RatePackage_Delete_RejectionFunc();
			if (HtmlReporter.tc_result)ROATModules.RatePackage_DeleteFunc_Yes();
			if (!HtmlReporter.tc_result) 
			{
				ROATFunctions.refesh(true,Launcher.UserRole);
			}
		}
		catch (Exception e)
		{
			Reporter.ReportEvent("Test Case execution is failed", "Test Case execution is failed","Test Case execution is failed", false);
		}
	}



	public static void earmarkInquiry() 
	{
		try {
			ROATFunctions.refesh(false, Launcher.UserRole);
			if (HtmlReporter.tc_result)
				ROATModules.earmarkInquiryNavigation();
			if (HtmlReporter.tc_result)
				ROATModules.selectBranch("EarmarksInquiry");
			if (HtmlReporter.tc_result)
				ROATFunctions.clickEnter();
			if (WebDr.isExists("warning_message") == 0) {
				if (HtmlReporter.tc_result)
					ROATModules.earmarkInquiryVerification();
				if (HtmlReporter.tc_result)
					ROATModules.countRecords("EarmarksInquiry");
				if (HtmlReporter.tc_result)
					ROATModules.performEarmarkInquiry();
				if (HtmlReporter.tc_result)
					ROATModules.clickBackBtn();
			}
			if (HtmlReporter.tc_result)
				ROATModules.earmarkInquiryNavigation();
			//	if (HtmlReporter.tc_result)
			//	ROATValidations.Verify_FieldValidation(1);
		} catch (Exception e) {
			Reporter.ReportEvent("Test Case execution is failed", "Test Case execution is failed",
					"Test Case execution is failed", false);
		}
	}

	public static void earmarkDelete()
	{
		try
		{
			ROATFunctions.refesh(false, Launcher.UserRole);
			if (HtmlReporter.tc_result)ROATModules.earmarkDeleteNavigation();
			for (int i = 1; i < 6; i++)
			{
				if (WebDr.isExists("SOD_message") == 0 && WebDr.isExists("select_BranchNumber") == 0)
				{
					Thread.sleep(1000);
				} else
					break;
			}
			System.out.println(WebDr.getElementObject(WebDr.page_Objects.get("SOD_message")).getText());

			if (WebDr.getElement("SOD_message").getText().equals(WebDr.getValue("SOD")))
			{
				Reporter.ReportEvent("SOD not performed", "SOD not performed", "SOD not performed", false);
			}
			else
			{
				if (HtmlReporter.tc_result)ROATModules.selectBranch("EarmarksDelete");
				if (HtmlReporter.tc_result)ROATFunctions.clickEnter();
				if (WebDr.isExists("warning_message") == 0)
				{
					if (HtmlReporter.tc_result)ROATModules.deleteEarmark();
				}
				if (HtmlReporter.tc_result)ROATModules.earmarkDeleteNavigation();
				//if (HtmlReporter.tc_result)
				//ROATValidations.Verify_FieldValidation(1);
			}
		} 
		catch (Exception e) 
		{
			Reporter.ReportEvent("Test Case execution is failed", "Test Case execution is failed",
					"Test Case execution is failed", false);
		}
	}

	public static void Forex_RatesDownload() 
	{
		try
		{
			ROATFunctions.refesh(false, Launcher.UserRole);
			if (HtmlReporter.tc_result)ROATModules.Navigation_ForexRatesDownload();
			if (HtmlReporter.tc_result)ROATModules.View_ForexRatesDownload();
		} 
		catch (Exception e)
		{
			Reporter.ReportEvent("Test Case execution is failed", "Test Case execution is failed",
					"Test Case execution is failed", false);
		}
	}
	//earmarks update and add 
	public static void Earmarks_Add(){
		try{
			ROATFunctions.refesh(false, Launcher.UserRole);
			if (HtmlReporter.tc_result)ROATModules.AI_navigateearmark();
			if (HtmlReporter.tc_result)ROATModules.AI_addearmarkpage();
			if (HtmlReporter.tc_result)ROATModules.AI_addearmarkpagecancel();

			//if (HtmlReporter.tc_result)ROATModules.AI_navigateearmark();
			if (HtmlReporter.tc_result)ROATModules.AI_addearmarkpage();
			if (HtmlReporter.tc_result)ROATModules.AI_addearmarkpageaccept();	
			if (HtmlReporter.tc_result)ROATModules.AI_addearmarkpageaddanother();	

		}
		catch(Exception e)
		{
			Reporter.ReportEvent("Test Case execution is failed", "Test Case execution is failed",
					"Test Case execution is failed", false);
		}
	}


	public static void Earmarks_Update(){
		try{
			ROATFunctions.refesh(false, Launcher.UserRole);

			if (HtmlReporter.tc_result)ROATModules.navigate_EarmarksUpdate();
			if (HtmlReporter.tc_result)ROATFunctions.CDA_SysDateValidation("Earmarks_Update","Earmark_Date");
			if (HtmlReporter.tc_result)ROATModules.EarmarksUpdate_AccInfor();	

			//if (HtmlReporter.tc_result)ROATModules.PaginationValidation("Earmarks_Update","PaginationTop");
			//if (HtmlReporter.tc_result)ROATModules.PaginationValidation("Earmarks_Update","PaginationBottom");
			if (HtmlReporter.tc_result)ROATModules.Earmarks_UpdateFun_NoYes();
			if (HtmlReporter.tc_result)ROATModules.Earmarks_BackBtnFun();
			if (HtmlReporter.tc_result)ROATModules.EarmarksUpdate_AccInfor();	
			if (HtmlReporter.tc_result)ROATModules.Earmarks_UpdateFun_cancel();
			if (HtmlReporter.tc_result)ROATModules.Earmarks_FilterFun();
		}
		catch(Exception e)
		{
			Reporter.ReportEvent("Test Case execution is Failed", "Earmarks_Update transaction successfull",
					"Earmarks_Update transaction  Failed", false);
		}

	}

	// ****************************************Internal Transfer***************************************************************
	// ************************************************************************************************************************
	public static void Internal_transfer() 
	{
		try {
			ROATFunctions.refesh(false, Launcher.UserRole);
			if (HtmlReporter.tc_result) {
				String user_id = ROATFunctions.fetch_user_Id();
				System.out.println(user_id);
				if (HtmlReporter.tc_result)
					ROATModules.accountinquirycheck_tranfer("Frm_Branch_acc_lcy", "Frm_acc_no_lcy");
				//		if (HtmlReporter.tc_result) {
				//			Map<String, String> b = ROATModules.storevalues("Internal_Transfer", "inquiry");
				ROATFunctions.refesh(false,"Super User" );
				/*if (HtmlReporter.tc_result)
					ROATModules.setup_navigateLTLimits();*/

				if (HtmlReporter.tc_result) {
					//String user_name = ROATModules.fetchuser();
					if (HtmlReporter.tc_result) {
						String user_branch = ROATModules.fetch_user_branch();
						//if (HtmlReporter.tc_result)ROATModules.fetch_limits(user_branch, user_id+"-"+user_name);/////////////////////////////////

						if (HtmlReporter.tc_result)
							ROATModules.AUTHRem_navigateRemAuthorization();
						if (HtmlReporter.tc_result)
							ROATModules.updateRemoteAuthorization(user_branch);
						//					if (HtmlReporter.tc_result)ROATFunctions.refesh(false,"Central Supervisor");
						if (HtmlReporter.tc_result) {
							//						int midRate = ROATModules.getMidRate(WebDr.getValue("Local currency"));
							//						if (HtmlReporter.tc_result)
							//							ROATModules.verifySuccessForexRateUpdate();
							//						//
							int midRate=1;
							if (HtmlReporter.tc_result)ROATFunctions.refesh(false,"Teller");
							if (HtmlReporter.tc_result) {
								int trxnamt = ROATModules.generateBreachAmount("Tranfer_amt_no_breach", midRate, "low");
								////							//

								//
								if (HtmlReporter.tc_result)
									ROATModules.navigateInternalTransfer();
								if (HtmlReporter.tc_result)
									ROATModules.transferPageVerification();
								if (HtmlReporter.tc_result) {
									Double fromnewbal = ROATModules.ent_detail_lcy(Integer.toString(trxnamt));
									if (HtmlReporter.tc_result)
										ROATModules.button("nextbt");
									//								if (HtmlReporter.tc_result) {
									//									Map<String, String> c = ROATModules.storevalues("Internal_Transfer","internal_transfer");
									//									if (HtmlReporter.tc_result)
									//										ROATModules.comparestat(c,b);
									Thread.sleep(4000);
									if (HtmlReporter.tc_result)
										ROATModules.button("accept");
									if (HtmlReporter.tc_result)ROATModules.breachauthorize();
									/*if (HtmlReporter.tc_result)
										ROATModules.verifySequenceNo("Internal_Transfer", "Ref_no");*/
									if (HtmlReporter.tc_result)
										ROATModules.button("post");
									if (HtmlReporter.tc_result)
										ROATModules.verify_messg("succ_mgs", "msg_success");
									if (HtmlReporter.tc_result)
										ROATModules.button("done");
									if (HtmlReporter.tc_result)
										ROATModules.verifyTransferUpdate(WebDr.getValue("Frm_Branch_No"), WebDr.getValue("Frm_acc_no_lcy"), fromnewbal);

									// foriegn currency
									//
									//																		if (HtmlReporter.tc_result) {
									//																			int midRate2 = ROATModules.getMidRate(WebDr.getValue("fcy_currency"));
									//																			if (HtmlReporter.tc_result)
									//																				ROATModules.verifySuccessForexRateUpdate();
									//
									//										if (HtmlReporter.tc_result) {
									//											int trxnamt2 = ROATModules.generateBreachAmount("Tranfer_amt_no_breach",
									//													midRate2, "low");
									//
									//											if (HtmlReporter.tc_result)
									//												ROATModules.navigateInternalTransfer();
									//											if (HtmlReporter.tc_result) {
									//												Double tonewbal = ROATModules.ent_detail_fcy(Integer.toString(trxnamt2));
									//												if (HtmlReporter.tc_result)
									//													ROATModules.button("nextbt");
									//												if (HtmlReporter.tc_result)
									//													ROATModules.button("accept");
									//												if (HtmlReporter.tc_result)
									//													ROATModules.button("post");
									//												if (HtmlReporter.tc_result)
									//													ROATModules.verify_messg("succ_mgs", "msg_success");
									//												if (HtmlReporter.tc_result)
									//													ROATModules.button("done");
									//												if (HtmlReporter.tc_result)
									//													ROATModules.verifyTransferUpdate(WebDr.getValue("To_Branch_No_fcy"),WebDr.getValue("To_acc_no_fcy"), tonewbal);
									//
									//												// lcy and fcy account
									//												if (HtmlReporter.tc_result)
									//													ROATModules.navigateInternalTransfer();
									//												if (HtmlReporter.tc_result)
									//													ROATModules.ent_detail_lcy_fcy(Integer.toString(100));
									//												if (HtmlReporter.tc_result)
									//													ROATModules.verify_messg("lcy_fcy_err", "lcy_fcy_err_msg");

									// //breach flow
									// if(HtmlReporter.tc_result)ROATModules.navigateInternalTransfer();
									// if(HtmlReporter.tc_result)ROATModules.ent_detail_lcy("Tranfer_amt_breach");
									// if(HtmlReporter.tc_result)ROATModules.button("nextbt");
									//
									// if(HtmlReporter.tc_result)ROATModules.button("accept");
									// if(HtmlReporter.tc_result)ROATModules.button("post");
									// if(HtmlReporter.tc_result)ROATModules.verify_messg("succ_mgs");
									// if(HtmlReporter.tc_result)ROATModules.button("done");

									//											}
									//										}
									//									}
									//								}
									//							}
								}
							}
						}
					}
				}
			}			
			if (HtmlReporter.tc_result == false) {
				ROATFunctions.refesh(true,Launcher.UserRole);
			}

		} catch (Exception e) {
			e.printStackTrace();
			Reporter.ReportEvent("Test Case execution is Failed", "Internal transfer transaction successfull",
					"Internal transfer transaction  Failed", false);
		}
	}


	//****************************** Signature *******************************************
	//************************************************************************************

	public static void Signature()
	{
		String SignName = "";
		boolean Update = false;
		try{
			ROATFunctions.refesh(false, Launcher.UserRole);
			if (HtmlReporter.tc_result){
				SignName = ROATNTBFLows.signatureadd();
			}
			//				ROATFunctions.refesh(false, "Signature Authorizer");
			//				if (HtmlReporter.tc_result){
			//					ROATNTBFLows.signature_authorise(SignName);
			//					
			//				}
			//				ROATFunctions.refesh(false, "Head Teller");
			//				if (HtmlReporter.tc_result){
			//					ROATNTBFLows.signatureInquiry(SignName,Update);
			//				}
			//				ROATFunctions.refesh(false, "Supervisor");
			//				if (HtmlReporter.tc_result){
			//					Update = ROATNTBFLows.SignatureUpdate(SignName,Update);
			//				}
			//				ROATFunctions.refesh(false, "Signature Authorizer");
			//				if (HtmlReporter.tc_result){
			//					ROATNTBFLows.signature_authorise(SignName);
			//				}
			//				ROATFunctions.refesh(false, "Head Teller");
			//				if (HtmlReporter.tc_result){
			//					ROATNTBFLows.signatureInquiry(SignName,Update);
			//				}
			//				ROATFunctions.refesh(false, "Supervisor");
			//				if (HtmlReporter.tc_result){
			//					ROATNTBFLows.SignatureDelete(SignName);
			//				}
			//				
		}catch(Exception e){
			Reporter.ReportEvent("Test case execution failed", "Signature end to end execution successful", "Signature end to end execution failed", false);
		}
	}
	//sig auth -AkshAY
	public static void signature_authorise(String SignName)
	{
		try {//ROATFunctions.refesh(false, Launcher.UserRole);

			String sign_name="";
			String power_attory="no";
			/*try {
				WebDr.SetPageObjects("MasterPage");
				if (HtmlReporter.tc_result)ROATModules.menu_navigation("MasterPage","tab_signature","tab_signature_closed");
				if (HtmlReporter.tc_result)ROATModules.menu_navigation1("MasterPage","tab_sig_add","null");
				if (HtmlReporter.tc_result)ROATModules.selectBranch("sig_authorise");
				if (HtmlReporter.tc_result)ROATFunctions.clickEnter();
				if (HtmlReporter.tc_result)sign_name=ROATModules.addnew_sign(power_attory);
			} catch (Exception e1) {
				e1.printStackTrace();
				Reporter.ReportEvent("Test case failed", "To add Signature ","Signature not added",true);
			}*/

			String account_no="";
			String branch_no="";
			try {


				if (HtmlReporter.tc_result)
					ROATModules.menu_navigation("MasterPage", "tab_signature", "tab_signature_closed");
				if (HtmlReporter.tc_result)
					ROATModules.menu_navigation1("MasterPage", "tab_sig_authorise", "null");
				if (HtmlReporter.tc_result)
					ROATModules.sign_auth_verifications();
				if (HtmlReporter.tc_result)ROATModules.selectBranch("sig_authorise");
				if (HtmlReporter.tc_result)ROATFunctions.clickEnter();
				if (HtmlReporter.tc_result)ROATModules.perform_signAuthorise(SignName,power_attory);

			}

			catch(Exception e)
			{
				e.printStackTrace();
			}

			/*try {
				if (HtmlReporter.tc_result)ROATModules.menu_navigation("MasterPage","tab_signature","tab_signature_closed");
				if (HtmlReporter.tc_result)ROATModules.menu_navigation1("MasterPage","tab_sign_inquiry","null");
				if (HtmlReporter.tc_result)ROATModules.selectBranch("sig_authorise");
				if (HtmlReporter.tc_result)ROATFunctions.clickEnter();
				if (HtmlReporter.tc_result)ROATModules.perform_signInquire(sign_name);


			} catch (Exception e) {
				e.printStackTrace();
			}*/
		}
		catch (Exception e) {
			Reporter.ReportEvent("Testcase execution failed", "End to end functionality failed signature authorise", "End to end functionality failed signature authorise", false);
			e.printStackTrace();
		}

	}

	public static void signatureInquiry(String SignName,boolean update) {
		try {//ROATFunctions.refesh(false, Launcher.UserRole);
			if (HtmlReporter.tc_result)ROATModules.menu_navigation("MasterPage","tab_signature","tab_signature_close");
			if (HtmlReporter.tc_result)ROATModules.menu_navigation1("MasterPage","tab_sign_inquiry","null");
			if (HtmlReporter.tc_result)ROATModules.selectBranch("SignatureInquiry");
			if (HtmlReporter.tc_result)ROATFunctions.clickEnter();
			String accCurr = WebDr.getElementObject(WebDr.page_Objects.get("accountCurr")).getText();
			for(int i=1; i<5; i++){
				if(WebDr.isExists("no_sign")==0 && accCurr.equals("")){
					accCurr = WebDr.getElementObject(WebDr.page_Objects.get("accountCurr")).getText();
					Thread.sleep(1000);
				} else {
					break;
				}
			}
			//				if(WebDr.isExists("no_sign") == 0)
			{

				if (HtmlReporter.tc_result)ROATModules.signatureInquiryVerification("VerificationIn");
				//					if (HtmlReporter.tc_result)ROATModules.countRecords("SignatureInquiry");
				WebDr.setText("X", "SignatureNameFilter", SignName, "Enter signatory name", true);
				if (HtmlReporter.tc_result)WebDr.click("X", "table_row", "Clicking on row", true);
				if (HtmlReporter.tc_result)WebDr.waitTillElementIsDisplayed("expanded_row");
				if(update==true){
					String UpdateDate = WebDr.getElement("SignatureUpdateDate").getText();

					DateTimeFormatter dtf = DateTimeFormatter.ofPattern("dd/MM/yy");
					LocalDate localDate = LocalDate.now();
					String Date = dtf.format(localDate);

					if(UpdateDate.contains(Date)){
						Reporter.ReportEvent("Signature Update", "Signature updated", "Signature Updated", true);
					}else{
						Reporter.ReportEvent("Signature Update", "Signature updated", "Signature not Updated", false);
					}
				}
				if (HtmlReporter.tc_result)ROATModules.signatureInquiryVerification("Validation");
				if (HtmlReporter.tc_result)WebDr.click("X", "cancel_btn", "Clicking on cancel button", true);
			} 
			//					else {
			//					System.out.println(WebDr.getElement("no_sign").getText());
			//					if(WebDr.getElement("no_sign").getText().contains(WebDr.getValue("No_signature")))
			//					Reporter.ReportEvent("No signatories present for this account", "No signatories present for this account", "No signatories present for this account", true);
			//					else
			//					Reporter.ReportEvent("Testcase execution failed", "Should be able to perform signature inquiry", "Not able to perform signature inquiry", false);
			//				}



		} catch (Exception e) {
			Reporter.ReportEvent("Testcase execution failed", "Should be able to perform signature inquiry", "Not able to perform signature inquiry", false);
			e.printStackTrace();
		}
	}

	//signature add -karishma
	public static String signatureadd()  {
		RandomStringUtils ranValue= new RandomStringUtils();
		String UpdateNum=ranValue.randomNumeric(2);
		String signame=WebDr.getValue("signname")+UpdateNum;

		try {ROATFunctions.refesh(false, Launcher.UserRole);

		WebDr.SetPageObjects("MasterPage");

		if (HtmlReporter.tc_result)ROATModules.menu_navigation("MasterPage","tab_signature","tab_signature_close");
		if (HtmlReporter.tc_result)ROATModules.menu_navigation1("MasterPage","tab_sign_add","null");

		if (HtmlReporter.tc_result)ROATModules.selectBranch("signature");
		if (HtmlReporter.tc_result)ROATFunctions.clickEnter();
		if (HtmlReporter.tc_result)ROATModules.addnew("yes",signame);
		//	        
		//	        if (HtmlReporter.tc_result)ROATModules.menu_navigation("MasterPage","tab_signature","tab_signature_close");
		//	        if (HtmlReporter.tc_result)ROATModules.menu_navigation1("MasterPage","tab_sign_auth","null");
		//	        if (HtmlReporter.tc_result)ROATModules.menu_navigation("MasterPage","tab_signature","tab_signature_close");
		//	        if (HtmlReporter.tc_result)ROATModules.menu_navigation1("MasterPage","tab_sign_add","null");
		//	        if (HtmlReporter.tc_result)ROATModules.selectBranch("signature");
		//	        if (HtmlReporter.tc_result)ROATFunctions.clickEnter();
		//	        if (HtmlReporter.tc_result){
		//	        	
		//	        	SignName = ROATModules.addnew("no",signame);
		//	        }
		//	        

		if (HtmlReporter.tc_result)ROATModules.cancel();
		if (HtmlReporter.tc_result)ROATModules.filesizeexceeeds();	

		//	        if (HtmlReporter.tc_result)ROATModules.menu_navigation("MasterPage","tab_signature","tab_signature_close");
		//	        if (HtmlReporter.tc_result)ROATModules.menu_navigation1("MasterPage","tab_sign_add","null");
		// ROATValidations.Verify_FieldValidation(1);
		}catch (Exception e) {
			Reporter.ReportEvent("Testcase execution failed", "End to end functionality failed signature add", "End to end functionality failed signature add", false);
			e.printStackTrace();
		}
		return signame;


	}

	//******************************Signature Delete ****************************************

	public static void SignatureDelete(String SignName) {
		try{//ROATFunctions.refesh(false, Launcher.UserRole);
			WebDr.SetPageObjects("MasterPage");
			if (HtmlReporter.tc_result)ROATModules.menu_navigation("MasterPage","tab_signature","tab_signature_close");
			if (HtmlReporter.tc_result)ROATModules.menu_navigation1("MasterPage","tab_sign_delete","null");
			if (HtmlReporter.tc_result)ROATModules.selectBranch("SignaturePage");
			if (HtmlReporter.tc_result)ROATFunctions.clickEnter();
			if (HtmlReporter.tc_result)ROATModules.SignatureDelete(SignName);
		}catch (Exception e) {
			Reporter.ReportEvent("Testcase execution failed", "End to end functionality failed signature add", "End to end functionality failed signature add", false);
			e.printStackTrace();
		}
	}

	//******************************Signature update ****************************************

	public static boolean SignatureUpdate(String SignName,boolean Update) {
		try{//ROATFunctions.refesh(false, Launcher.UserRole);
			WebDr.SetPageObjects("MasterPage");
			if (HtmlReporter.tc_result)ROATModules.menu_navigation("MasterPage","tab_signature","tab_signature_close");
			if (HtmlReporter.tc_result)ROATModules.menu_navigation1("MasterPage","tab_sign_Update","null");
			if (HtmlReporter.tc_result)ROATModules.selectBranch("SignaturePage");
			if (HtmlReporter.tc_result)ROATFunctions.clickEnter();
			if (HtmlReporter.tc_result)Update = ROATModules.SignatureUpdate(SignName,Update);

			return Update;
		}catch (Exception e) {
			Reporter.ReportEvent("Testcase execution failed", "End to end functionality failed signature add", "End to end functionality failed signature add", false);
			e.printStackTrace();
			return false;
		}
	}


	//sanity FCY inquiry -karishma

	public static void SanitySuite_FCYInquiry()  {								

		try{
			boolean Role = false;
			boolean accStatus = false, sodStatus = false, ocpStatus = false, aml = false, status = false;

			if (HtmlReporter.tc_result)ROATFunctions.refesh(false, Launcher.UserRole);
			if (HtmlReporter.tc_result)ROATModules.AI_navigateAccountInquiry();
			if (HtmlReporter.tc_result)ROATModules.AI_searchAccountNumber();
			if (HtmlReporter.tc_result){
				Role = ROATModules.verifyUserRole(WebDr.getValue("Role"));
			}
			if (HtmlReporter.tc_result)ROATFunctions.refesh(false, "Supervisor");
			if (HtmlReporter.tc_result)ROATModules.navigateSOD();
			if (HtmlReporter.tc_result) { sodStatus = ROATModules.verifySOD();
			System.out.println("SOD Status : " +sodStatus);
			} 

			if (HtmlReporter.tc_result)ROATFunctions.refesh(false, "Head Teller");
			if (HtmlReporter.tc_result)ROATModules.CD_navigateTellerInquiry();
			if (HtmlReporter.tc_result)ROATModules.Prereq_SufficientTellerPos();	


			if (HtmlReporter.tc_result)ROATModules.navigateOCP();
			if (HtmlReporter.tc_result) { ocpStatus = ROATModules.verifyOCP();
			System.out.println("OCP Status : " +ocpStatus);
			}

			if (HtmlReporter.tc_result){
				if(Role && sodStatus && ocpStatus){
					status = true;
					try {
						if (HtmlReporter.tc_result)ROATModules.setup_navigateFCYInquiry();
						//						if (HtmlReporter.tc_result)ROATModules.setup_FCYDownload();	
					} catch (Exception e) {
						Reporter.ReportEvent("Test Case execution is failed", "Test Case execution is failed",
								"Test Case execution is failed", false);
					}
				}
			}

		}catch(Exception e){
			Reporter.ReportEvent("Vault LCY Receipt from CDC Sanity", "Vault LCY Receipt from CDC Sanity failed", "Vault LCY Receipt from CDC Sanity failed", false);
		}
	}
	public static void SOD() {
		try{ROATFunctions.refesh(false, Launcher.UserRole);
		ROATModules.setup_StartOfTheDay();}
		catch (Exception e) {
			Reporter.ReportEvent("Testcase execution failed", "End to end functionality failed signature add", "End to end functionality failed signature add", false);
			e.printStackTrace();

		}
	}

	//******************************Vault-FCY-Cash Summary****************************************

	public static void Vault_Summary() 
	{
		try
		{
			ROATFunctions.refesh(false, "Super User");
			if (HtmlReporter.tc_result)
				ROATModules.getDualAuthorizationCount();

			ROATFunctions.refesh(false, Launcher.UserRole);
			WebDr.SetPageObjects("VaultSummary");

			if (HtmlReporter.tc_result)
				ROATModules.vault_NavigateCashSummary();

			String Auth_user[] = Launcher.Auth_User_Login("Vault Custodian");
			String Login[] = Launcher.User_Login("Vault Custodian");
			if (HtmlReporter.tc_result)
				ROATModules.authorize_Credential_Vault_FCY_Cash_Summary("VaultSummary","txt_AuthorizerUser", "txt_AuthorizerPass","username2","password2",Auth_user[0], Auth_user[1],Login[0], Login[1], true);
			if (HtmlReporter.tc_result)
				ROATModules.vaultCashSummaryVerification();
			if (HtmlReporter.tc_result)
			{
				if(WebDr.isExists("btn_Print")==1)
				{
					Reporter.ReportEvent("Test Case execution is Passed", "Print button is present", "Print button is present",true);
				}
				else
				{
					Reporter.ReportEvent("Test Case execution is failed", "Print button is present", "Print button is not present",false);
				}
			}
			if (HtmlReporter.tc_result)
			{
				if(WebDr.isExists("btn_OpenAsPDF")==1)
				{
					Reporter.ReportEvent("Test Case execution is Passed", "Open As PDF button is present", "Open As PDF button is present",true);
				}
				else
				{
					Reporter.ReportEvent("Test Case execution is failed", "Open As PDF button is present", "Open As PDF button is not present",false);
				}
			}
			if (HtmlReporter.tc_result)
				ROATModules.vaultCashSummaryPagination();
			if (HtmlReporter.tc_result)
				ROATModules.PaginationValidation("VaultSummary","PaginationTop");
			if (HtmlReporter.tc_result)
				ROATModules.PaginationValidation("VaultSummary","PaginationBottom");
			if (HtmlReporter.tc_result)
				ROATModules.VaultCashSummary_Sorting();
			if (HtmlReporter.tc_result)
				ROATModules.VaultCashSummaryBalanceVerification();
		}
		catch (Exception e)
		{
			Reporter.ReportEvent("Test Case execution is failed", "Test Case execution is failed",
					"Test Case execution is failed", false);
		}
	}

	public static void LT_LimitsVerificationViewUpdate_Sanity()
	{
		try 
		{
			ROATFunctions.refesh(false, Launcher.UserRole);

			if (HtmlReporter.tc_result)ROATModules.setup_navigateLTLimits();
			if (HtmlReporter.tc_result)ROATModules.setup_limitsVerification();
			if (HtmlReporter.tc_result)ROATModules.setup_limitsAdd();
			if (HtmlReporter.tc_result)ROATModules.setup_limitsViewUpdate();
			if (HtmlReporter.tc_result)ROATModules.setup_navigateLTLimits();
			if (HtmlReporter.tc_result)ROATModules.setup_limitsToogleButton();
			if (HtmlReporter.tc_result == false)
			{
				ROATFunctions.refesh(true,Launcher.UserRole);
			}
		} 
		catch (Exception e) 
		{
			Reporter.ReportEvent("Test Case execution is failed", "Test Case execution is failed","Test Case execution is failed", false);
		}
	}

	public static void forexRateUpdate_Sanity() 
	{
		try
		{
			ROATFunctions.refesh(false, Launcher.UserRole);
			//			if (HtmlReporter.tc_result)
			//				ROATFunctions.refesh(true, "Supervisor");
			// success scenario
			if (HtmlReporter.tc_result)ROATModules.navigateForexRateUpdate();
			if (HtmlReporter.tc_result)ROATModules.performBranchSelection();
			if (HtmlReporter.tc_result)ROATModules.forexRateUpdate();
			if (HtmlReporter.tc_result)ROATModules.verifySuccessForexRateUpdate();
			if (HtmlReporter.tc_result)ROATModules.clickBackBtn();

			// reject scenario
			if (HtmlReporter.tc_result)ROATModules.navigateForexRateUpdate();
			if (HtmlReporter.tc_result)ROATModules.performBranchSelection();
			if (HtmlReporter.tc_result)ROATModules.forexRateUpdate();
			if (HtmlReporter.tc_result)ROATModules.verifyRejectForexRateUpdate();

		} 
		catch (Exception e)
		{
			Reporter.ReportEvent("Test Case execution is failed", "Test Case execution is failed",
					"Test Case execution is failed", false);
		}
	}

	public static void cash_DrawerLCYReceipt_Sanity() 
	{
		try
		{
			ROATFunctions.refesh(false, Launcher.UserRole);

			String teller=ROATModules.fetchuser();
			System.out.println(teller);

			if (HtmlReporter.tc_result)ROATModules.CD_navigateLCYReceipt();
			Map<String, String> x = ROATModules.storeDenomination("LCYReceipt", "DenominationRows");
			if (HtmlReporter.tc_result)ROATModules.CD_transferLCYReceipt();
			if (HtmlReporter.tc_result)
			{
				String Auth_user1[] =Launcher.Auth_User_Login("Vault Custodian");
				String Auth_user2[] =Launcher.User_Login("Super User");

				ROATModules.Enter_Authorize_Credential("LCYReceipt", "username", "password",Auth_user1[0], Auth_user1[1],Auth_user2[0], Auth_user2[1],"password2","username2", true);
			}
			WebDr.SetPageObjects("MasterPage");
			if (HtmlReporter.tc_result)ROATModules.AS_NavigateAccountSearch();
			if (HtmlReporter.tc_result)ROATModules.CD_navigateLCYReceipt();
			Map<String, String> a = ROATModules.storeDenomination("LCYReceipt", "DenominationRows");
			if (HtmlReporter.tc_result)ROATModules.compare_not(a, x);
			if (HtmlReporter.tc_result)	ROATFunctions.refesh(true, "Head Teller");
			if (HtmlReporter.tc_result)ROATModules.CD_navigateTellerInquiry();
			if (HtmlReporter.tc_result)ROATModules.CDA_navigateTellerInquiry(teller, WebDr.getValue("CurrencyDrop2"));
			Map<String, String> b = ROATModules.storeDenomination("TellerInquiry", "DenominationRows");
			if (HtmlReporter.tc_result)ROATModules.compare(a, b);
		}
		catch (Exception e)
		{
			Reporter.ReportEvent("Test Case execution is failed", "Test Case execution is failed",
					"Test Case execution is failed", false);
		}
	}

	public static void FCY_Payment_Sanity() 
	{
		try
		{
			ROATFunctions.refesh(false, Launcher.UserRole);
			//			if (HtmlReporter.tc_result) ROATFunctions.refesh(true, "Teller", WebDr.getValue("Login_Username"), WebDr.getValue("Login_Password"));
			String teller=ROATModules.fetchuser();
			System.out.println(teller);
			WebDr.SetPageObjects("FCYPayment");

			if (HtmlReporter.tc_result)ROATModules.menu_navigation("MasterPage", "tab_CashDrawer", "tab_CashDrawer_NotToClose");
			if (HtmlReporter.tc_result)ROATModules.menu_navigation1("MasterPage", "tab_FCY", "tab_FCY_closed");
			if (HtmlReporter.tc_result)ROATModules.menu_navigation1("MasterPage", "tab_FCY_payment", "null");
			if (HtmlReporter.tc_result)ROATModules.FCY_payment_select_currency();
			if (HtmlReporter.tc_result)ROATModules.Enter_payment_Amount("FCYPayment", "txtAmounts");
			if (HtmlReporter.tc_result)ROATModules.error_Message_verification("Mismatch", WebDr.getValue("ErrorMsg_Mismatch"));
			if (HtmlReporter.tc_result)ROATModules.enter_note_coin_Count("FCYPayment", "tablenotes", "txtnots");
			if (HtmlReporter.tc_result)ROATModules.enter_note_coin_Count("FCYPayment", "tableCoins", "txtcoins");

			String value= WebDr.getElement("re_enter").getText().substring(4).trim().replace(",", "");
			if (HtmlReporter.tc_result)ROATModules.Reenter_payment_Amount("FCYPayment","txtAmounts",value);

			// if
			// (HtmlReporter.tc_result)ROATModules.enter_Soiled_decoy_value();
			if (HtmlReporter.tc_result)ROATModules.fn_verify_total_cash_payment_amount("FCYPayment", WebDr.getValue("Enter_Amount"));
			if (HtmlReporter.tc_result)
			{
				String Auth_user1[] =Launcher.Auth_User_Login("Vault Custodian");
				String Auth_user2[] =Launcher.User_Login("Super User");				
				ROATModules.Enter_Authorize_Credential("FCYPayment", "txt_AuthorizerUser", "txt_AuthorizerPass",Auth_user1[0], Auth_user1[1],Auth_user2[0], Auth_user2[1],"password2","username2", true);
			}
			if (HtmlReporter.tc_result)ROATModules.error_Message_verification("Authorize", WebDr.getValue("VerifyPostMessage"));
			// Reject payment scenario
			if (HtmlReporter.tc_result)ROATModules.FCY_payment_select_currency();
			if (HtmlReporter.tc_result)ROATModules.Enter_payment_Amount("FCYPayment", "txtAmounts");
			if (HtmlReporter.tc_result)ROATModules.error_Message_verification("Mismatch", WebDr.getValue("ErrorMsg_Mismatch"));


			Map<String, String> a = ROATModules.storeDenomination("FCYPayment", "DenominationRows");
			Map<String, String> c = ROATModules.storeDenomination1("FCYPayment", "DenominationRows1");
			a.putAll(c);
			if (HtmlReporter.tc_result)
				ROATModules.enter_note_coin_Count("FCYPayment", "tablenotes", "txtnots");
			if (HtmlReporter.tc_result)
				ROATModules.enter_note_coin_Count("FCYPayment", "tableCoins", "txtcoins");

			if (HtmlReporter.tc_result)ROATModules.Reenter_payment_Amount("FCYPayment","txtAmounts",value);
			// if
			// (HtmlReporter.tc_result)ROATModules.enter_Soiled_decoy_value();
			if (HtmlReporter.tc_result)
			{
				String Auth_user1[] =Launcher.Auth_User_Login("Vault Custodian");
				String Auth_user2[] =Launcher.Auth_User_Login("Super User");	
				ROATModules.Enter_Authorize_Credential("FCYPayment", "txt_AuthorizerUser", "txt_AuthorizerPass",Auth_user1[0], Auth_user1[1],Auth_user2[0], Auth_user2[1],"password2","username2", false);
			}
			if (HtmlReporter.tc_result)ROATModules.error_Message_verification("Reject", WebDr.getValue("Verify_Reject_Message"));
			if (HtmlReporter.tc_result)ROATModules.Enter_payment_Amount("FCYPayment", "txtAmounts");
			if (HtmlReporter.tc_result)ROATModules.Cancel_LCY_Payment_function();
			if (HtmlReporter.tc_result)ROATFunctions.refesh(true, "Head Teller");
			if (HtmlReporter.tc_result)ROATModules.CD_navigateTellerInquiry();
			if (HtmlReporter.tc_result)ROATModules.CDA_navigateTellerInquiry(teller, WebDr.getValue("Currency_Name"));
			Map<String, String> b = ROATModules.storeDenomination("TellerInquiry", "DenominationRows");
			if (HtmlReporter.tc_result)ROATModules.compare(a, b);
		}
		catch (Exception e)
		{
			Reporter.ReportEvent("Test Case execution is failed", "Test Case execution is failed",
					"Test Case execution is failed", false);
		}
	}

	public static void AS_AccountSearch_sanity()  {
		try {
			ROATFunctions.refesh(false, Launcher.UserRole);
			//			if (HtmlReporter.tc_result)ROATFunctions.refesh(true, "Teller");
			if (HtmlReporter.tc_result)
				ROATModules.AS_NavigateAccountSearch();
			if (HtmlReporter.tc_result)
				ROATModules.AS_PerformAccountSearch();
		} catch (Exception e) {
			Reporter.ReportEvent("Test Case execution is failed", "Test Case execution is failed",
					"Test Case execution is failed", false);
		}

	}
	public static void CS_CustomerSearch_sanity()  {
		try {
			ROATFunctions.refesh(false, Launcher.UserRole);
			if (HtmlReporter.tc_result)
				ROATModules.CS_navigateCustomerSearch();
			WebDr.SetPageObjects("CustomerSearchPage");
			if (HtmlReporter.tc_result) WebDr.click("X", "tab_ReferStreamCode", "Clicking on ReferStreamCode field", true);
			if (HtmlReporter.tc_result)
				ROATModules.CS_customerDeatilSearch();
			if(HtmlReporter.tc_result)
				ROATModules.CS_customerSearchSorting();
			if (HtmlReporter.tc_result)
				ROATModules.PaginationValidation("CustomerSearchPage","PaginationTop");
			if (HtmlReporter.tc_result)
				ROATModules.PaginationValidation("CustomerSearchPage","PaginationBottom");

		} catch (Exception e) {
			Reporter.ReportEvent("Test Case execution is failed", "Test Case execution is failed",
					"Test Case execution is failed", false);
		}

	}
	public static void Teller_Cash_Deposit_sanity() 
	{
		try 
		{
			ROATFunctions.refesh(false, Launcher.UserRole);
			if (HtmlReporter.tc_result)ROATModules.Teller_navigateCashDeposit();
			if (HtmlReporter.tc_result)ROATModules.Teller_BCviewCashDeposit();
			ROATFunctions.refesh(true,Launcher.UserRole);

		}
		catch (Exception e)
		{
			Reporter.ReportEvent("Test Case execution is failed", "Test Case execution is failed","Test Case execution is failed", false);
		}
	}
	public static void Teller_Cash_DepositEdit_sanity()  {
		try {
			ROATFunctions.refesh(false, Launcher.UserRole);
			//			if (HtmlReporter.tc_result)ROATFunctions.refesh(true, "Teller");
			if (HtmlReporter.tc_result) {
				String user_branch = ROATModules.fetch_user_branch();

				//				if (HtmlReporter.tc_result)ROATFunctions.refesh(true, "Super User");
				//				if (HtmlReporter.tc_result)
				//					ROATModules.AUTHRem_navigateRemAuthorization();
				//				if (HtmlReporter.tc_result)
				//					ROATModules.updateRemoteAuthorization(user_branch);
				//				if (HtmlReporter.tc_result)ROATFunctions.refesh(true, "Teller");
				if (HtmlReporter.tc_result)
					ROATModules.Teller_navigateCashDeposit();
				if (HtmlReporter.tc_result)
					ROATModules.Teller_AuthorizeCashDeposit();
				if (HtmlReporter.tc_result)ROATModules.Teller_AuthorizeCashDeposit_Edit();


				ROATFunctions.refesh(true,Launcher.UserRole);

			}} catch (Exception e) {
				Reporter.ReportEvent("Test Case execution is failed", "Test Case execution is failed",
						"Test Case execution is failed", false);
			}

	}

	public static void VaultLCYReceiptCDC_Sanity()  {

		try {
			ROATFunctions.refesh(false, Launcher.UserRole);
			//			ROATFunctions.refesh(true,"Vault Custodian");
			if (HtmlReporter.tc_result)
				ROATModules.Navigate_VaultLCYReceiptCDC();
			if (HtmlReporter.tc_result)ROATModules.ViewLCYReceiptCDC();
			if (HtmlReporter.tc_result)ROATModules.Vault_LCYReceiptFromCDCReject();
			if (HtmlReporter.tc_result)ROATModules.Vault_LCYReceiptFromCDCCancel();
			if (HtmlReporter.tc_result)ROATModules.Vault_LCYReceiptFromCDCAuthorize();
		} catch (Exception e) {
			Reporter.ReportEvent("Test Case execution is failed", "Test Case execution is failed",
					"Test Case execution is failed", false);
		}
	}

	public static void VaultFCYPaymentCDC_Sanity()  {

		try {
			ROATFunctions.refesh(false, Launcher.UserRole);
			//			ROATFunctions.refesh(true,"Vault Custodian");
			if (HtmlReporter.tc_result)
				ROATModules.Navigate_VaultFCYPaymentCDC();
			if (HtmlReporter.tc_result)
				ROATModules.ViewFCYPaymentCDC();
			if (HtmlReporter.tc_result)
				ROATModules.Vault_FCYPaymentToCDCReject();
			if (HtmlReporter.tc_result)
				ROATModules.Vault_FCYPaymentToCDCAccept();
			if (HtmlReporter.tc_result)
				ROATModules.Vault_FCYPaymentToCDCCancel();		

		} catch (Exception e) {
			Reporter.ReportEvent("Test Case execution is failed", "Test Case execution is failed",
					"Test Case execution is failed", false);
		}
	}

	public static void Send_Health_Status_Mail() throws SQLException
	{
		String country_status_U_STA = "" ;
		String country_status_U_OSF = "" ;
		String eboday = "";
		String mbody = "";
		String Env_flag = "STA,OSF";


		String arrEnv_flag [] = Env_flag.split(",");
		ArrayList<String> arryeboday =new ArrayList<>();
		for(int i = 0;i<arrEnv_flag.length;i++)
		{

			ArrayList<String> list_U_STA =new ArrayList<>();
			//STA
			String getval_ZMB_U_STA  = DatabaseFactory.retrive_CYCLE_ID_table_val_as_env("ZMB","UAT",arrEnv_flag[i]);
			String getval_GHA_U_STA  = DatabaseFactory.retrive_CYCLE_ID_table_val_as_env("GHA","UAT",arrEnv_flag[i]);
			String getval_TZA_U_STA  = DatabaseFactory.retrive_CYCLE_ID_table_val_as_env("TZA","UAT",arrEnv_flag[i]);		
			String getval_BWA_U_STA  = DatabaseFactory.retrive_CYCLE_ID_table_val_as_env("BWA","UAT",arrEnv_flag[i]);
			String getval_MUS_U_STA  = DatabaseFactory.retrive_CYCLE_ID_table_val_as_env("MUS","UAT",arrEnv_flag[i]);
			String getval_MUO_U_STA  = DatabaseFactory.retrive_CYCLE_ID_table_val_as_env("MUO","UAT",arrEnv_flag[i]);		
			String getval_SYC_U_STA  = DatabaseFactory.retrive_CYCLE_ID_table_val_as_env("SYC","UAT",arrEnv_flag[i]);




			list_U_STA.add(getval_ZMB_U_STA);
			list_U_STA.add(getval_GHA_U_STA);
			list_U_STA.add(getval_TZA_U_STA);
			list_U_STA.add(getval_BWA_U_STA);
			list_U_STA.add(getval_MUS_U_STA);
			list_U_STA.add(getval_MUO_U_STA);
			list_U_STA.add(getval_SYC_U_STA);

			//		 eboday = HtmlReporter.email_Boday(list_U_STA,arrEnv_flag[i]);
			if (i<1)
			{
				mbody =mbody+eboday;
			}
			else
			{
				mbody =mbody+"%"+eboday;
			}


		}
		//			HtmlReporter.sending_mail(mbody);
		//End STA

		//OSF

		/*String getval_ZMB_U_OSF  = DatabaseFactory.retrive_CYCLE_ID_table_val_as_env("ZMB","UAT","OSF");
		String getval_GHA_U_OSF  = DatabaseFactory.retrive_CYCLE_ID_table_val_as_env("GHA","UAT","OSF");
		String getval_TZA_U_OSF  = DatabaseFactory.retrive_CYCLE_ID_table_val_as_env("TZA","UAT","OSF");		
		String getval_BWA_U_OSF  = DatabaseFactory.retrive_CYCLE_ID_table_val_as_env("BWA","UAT","OSF");
		String getval_MUS_U_OSF  = DatabaseFactory.retrive_CYCLE_ID_table_val_as_env("MUS","UAT","OSF");
		String getval_MUO_U_OSF  = DatabaseFactory.retrive_CYCLE_ID_table_val_as_env("MUO","UAT","OSF");		
		String getval_SYC_U_OSF  = DatabaseFactory.retrive_CYCLE_ID_table_val_as_env("SYC","UAT","OSF");



		ArrayList<String> list_STA_U_OSF =new ArrayList<>();
		list_STA_U_OSF.add(getval_ZMB_U_OSF);
		list_STA_U_OSF.add(getval_GHA_U_OSF);
		list_STA_U_OSF.add(getval_TZA_U_OSF);
		list_STA_U_OSF.add(getval_BWA_U_OSF);
		list_STA_U_OSF.add(getval_MUS_U_OSF);
		list_STA_U_OSF.add(getval_MUO_U_OSF);
		list_STA_U_OSF.add(getval_SYC_U_OSF);*/


		//End OSF
	}








	//******************************************************User Entitlement***************************************************************************	

	public static void UserEntitlement() 
	{
		try{
			if (HtmlReporter.tc_result)ROATModules.UserEntilement_Teller();
			if (HtmlReporter.tc_result)ROATModules.UserEntilement_Supervisor();/////////////////////
			if (HtmlReporter.tc_result)ROATModules.UserEntilement_Superuser();
			if (HtmlReporter.tc_result)ROATModules.UserEntilement_HeadTeller();
			if (HtmlReporter.tc_result)ROATModules.UserEntilement_UserAdmin();
			if (HtmlReporter.tc_result)ROATModules.UserEntilement_BackOfficeClear();//////////
			if (HtmlReporter.tc_result)ROATModules.UserEntilement_Authorizer();////////////////
			if (HtmlReporter.tc_result)ROATModules.UserEntilement_VaultCustodian();//////
			if (HtmlReporter.tc_result)ROATModules.UserEntilement_SignatureAuthorizer();
			if (HtmlReporter.tc_result)ROATModules.UserEntilement_GlobalAdmin();
			if (HtmlReporter.tc_result)ROATModules.UserEntilement_EnquiryClerk();
		}
		catch(Exception e)
		{
			e.printStackTrace();
			Reporter.ReportEvent("Comma Seperator Test Case execution is failed", "Comma Seperator Test Case should be executed",
					"Comma Seperator Test Case is not executed", false);

		}
	}
	public static void CommaSep() 
	{try{
		ROATFunctions.refesh(true, Launcher.UserRole);
		if (HtmlReporter.tc_result)ROATModules.CommaSep_AccountSearch();
		HtmlReporter.tc_result=true;
		if (HtmlReporter.tc_result)ROATModules.CommaSep_CustomerInquiry();
		HtmlReporter.tc_result=true;
		if (HtmlReporter.tc_result)ROATModules.CommaSep_Account_His_Inquiry();

		HtmlReporter.tc_result=true;
		if (HtmlReporter.tc_result)ROATModules.CommaSep_cdFCYPayment();
		HtmlReporter.tc_result=true;
		if (HtmlReporter.tc_result)ROATModules.CommaSep_cdLCYPayment();
		HtmlReporter.tc_result=true;
		if (HtmlReporter.tc_result)ROATModules.CommaSep_cdLCYReciept();
		HtmlReporter.tc_result=true;
		if (HtmlReporter.tc_result)ROATModules.CommaSep_cdFCYReciept();
		HtmlReporter.tc_result=true;
		if (HtmlReporter.tc_result)ROATModules.CommaSep_cdFCYtransfout();
		HtmlReporter.tc_result=true;
		if (HtmlReporter.tc_result)ROATModules.CommaSep_cdLCYtransfout();

		ROATFunctions.refesh(false, "Super User");
		HtmlReporter.tc_result=true;
		if (HtmlReporter.tc_result)ROATModules.CommaSep_fees();
		HtmlReporter.tc_result=true;
		ROATFunctions.refesh(false, "Vault Custodian");
		if (HtmlReporter.tc_result)ROATModules.CommaSep_vaultFCYrep();
		HtmlReporter.tc_result=true;
		if (HtmlReporter.tc_result)ROATModules.CommaSep_vaultLCYrep();
		HtmlReporter.tc_result=true;
		if (HtmlReporter.tc_result)ROATModules.CommaSep_vaultLCYpay();
		HtmlReporter.tc_result=true;
		if (HtmlReporter.tc_result)ROATModules.CommaSep_vaultFCYpay();
		HtmlReporter.tc_result=true;
		if (HtmlReporter.tc_result)ROATModules.CommaSep_vaultLcyatm();
		//				HtmlReporter.tc_result=true;
		//				if (HtmlReporter.tc_result)ROATModules.CommaSep_cdBranch();
	}
	catch(Exception e)
	{
		e.printStackTrace();
		Reporter.ReportEvent("Comma Seperator Test Case execution is failed", "Comma Seperator Test Case should be executed",
				"Comma Seperator Test Case is not executed", false);

	}
	}

	public static void correspondentBankMaint() {
		try {
			ROATFunctions.refesh(true, Launcher.UserRole);
			if (HtmlReporter.tc_result)ROATModules.navigateCorrBankMaint();
			if (HtmlReporter.tc_result)ROATModules.verifyCorrBankMaint();
			if (HtmlReporter.tc_result)ROATModules.addCorrBankMaint();
			if (HtmlReporter.tc_result)ROATModules.viewCorrBankMaint();
			if (HtmlReporter.tc_result)ROATModules.updateCorrBankMaint();
			if (HtmlReporter.tc_result)ROATModules.deleteCorrBankMaint();

		} catch (Exception e) {
			e.printStackTrace();
		}

	}
	public static void inventoryDraftBill() 
	{
		try
		{
			ROATFunctions.refesh(false, Launcher.UserRole);
			if (HtmlReporter.tc_result)ROATModules.navigateInventoryDraftBill();
			if (HtmlReporter.tc_result)ROATModules.verifyInventoryDraftBill();
			if (HtmlReporter.tc_result)ROATModules.updateInventoryDraftBill();
		}
		catch (Exception e) 
		{
			e.printStackTrace();
			Reporter.ReportEvent("Test Case execution is failed", "Test Case execution is failed",
					"Test Case execution is failed", false);
		}
	}
	public static void Sanity_Suite_FCYPaymentFrmCDC()
	{
		try
		{

			boolean Role = false;
			boolean accStatus = false, sodStatus = false, ocpStatus = false, aml = false, status = false;

			//			if (HtmlReporter.tc_result)ROATFunctions.refesh(false, Launcher.UserRole);
			//			if (HtmlReporter.tc_result)ROATModules.AI_navigateAccountInquiry();
			//			if (HtmlReporter.tc_result)ROATModules.AI_searchAccountNumber();
			//			if (HtmlReporter.tc_result){
			//				Role = ROATModules.verifyUserRole(WebDr.getValue("Role"));
			//			}
			//			if (HtmlReporter.tc_result)ROATFunctions.refesh(false, "Supervisor");
			//			if (HtmlReporter.tc_result)ROATModules.navigateSOD();
			//			if (HtmlReporter.tc_result) { sodStatus = ROATModules.verifySOD();
			//			System.out.println("SOD Status : " +sodStatus);
			//			} 
			if (HtmlReporter.tc_result)ROATFunctions.refesh(false, "Teller");
			if (HtmlReporter.tc_result)ROATModules.navigateOCP();
			if (HtmlReporter.tc_result) { ocpStatus = ROATModules.verifyOCP();
			System.out.println("OCP Status : " +ocpStatus);
			}

			if (HtmlReporter.tc_result){
				//if(Role && sodStatus && ocpStatus){
				if(ocpStatus){
					status = true;
					try {
						System.out.println("done");
						if (HtmlReporter.tc_result)ROATFunctions.refesh(false, "Vault Custodian");
						if (HtmlReporter.tc_result)
							ROATModules.Navigate_VaultFCYPaymentCDC();
						if (HtmlReporter.tc_result)
							ROATModules.ViewFCYPaymentCDC();
						if (HtmlReporter.tc_result)
							ROATModules.Vault_FCYPaymentToCDCReject();
						if (HtmlReporter.tc_result)
							ROATModules.Vault_FCYPaymentToCDCCancel();	
						if (HtmlReporter.tc_result)
							ROATModules.Vault_FCYPaymentToCDCAccept();
					} catch (Exception e) {
						Reporter.ReportEvent("Test Case execution is failed", "Test Case execution is failed",
								"Test Case execution is failed", false);
					}
				}
			}

		}catch(Exception e)
		{
			Reporter.ReportEvent("Vault LCY Receipt from CDC Sanity", "Vault LCY Receipt from CDC Sanity failed", "Vault LCY Receipt from CDC Sanity failed", false);
		}
	}
	public static void Sanity_Suite_LCYPaymentFrmCDC()
	{
		try
		{

			boolean Role = false;
			boolean accStatus = false, sodStatus = false, ocpStatus = false, aml = false, status = false;
			if (HtmlReporter.tc_result)ROATFunctions.refesh(false, Launcher.UserRole);
			if (HtmlReporter.tc_result){
				Role = ROATModules.verifyUserRole(WebDr.getValue("Role"));
			}
			if (HtmlReporter.tc_result)ROATFunctions.refesh(false, "Supervisor");
			if (HtmlReporter.tc_result)ROATModules.navigateSOD();

			if (HtmlReporter.tc_result) { sodStatus = ROATModules.verifySOD();
			System.out.println("SOD Status : " +sodStatus);
			}
			if (HtmlReporter.tc_result)ROATFunctions.refesh(false, "Teller");
			if (HtmlReporter.tc_result)ROATModules.navigateOCP();
			if (HtmlReporter.tc_result) { ocpStatus = ROATModules.verifyOCP();
			System.out.println("OCP Status : " +ocpStatus);
			}

			if (HtmlReporter.tc_result){
				if(Role && sodStatus && ocpStatus){
					status = true;
					try {
						System.out.println("done");

						if (HtmlReporter.tc_result)ROATFunctions.refesh(false, "Vault Custodian");
						if (HtmlReporter.tc_result)
							ROATModules.Navigate_VaultLCYPaymentCDC();
						if (HtmlReporter.tc_result)
							ROATModules.ViewLCYPaymentCDC();
						if (HtmlReporter.tc_result)
							ROATModules.Vault_LCYPaymentToCDCReject();
						if (HtmlReporter.tc_result)
							ROATModules.Vault_LCYPaymentToCDCCancel();
						//								if (HtmlReporter.tc_result)
						//									ROATValidations.Verify_FieldValidation(3);
						if (HtmlReporter.tc_result)
							ROATModules.Vault_LCYPaymentToCDCAuthorize();

					} catch (Exception e) {
						Reporter.ReportEvent("Test Case execution is failed", "Test Case execution is failed",
								"Test Case execution is failed", false);
					}
				}
			}

		}catch(Exception e){
			Reporter.ReportEvent("Vault LCY Receipt from CDC Sanity", "Vault LCY Receipt from CDC Sanity failed", "Vault LCY Receipt from CDC Sanity failed", false);
		}
	}
	//screentimeout
	public static void Screentimeout() {
		try {
			Random random = new Random();
			int screen_time_out=random.nextInt(300-181)+181;
			System.out.println(screen_time_out);

			ROATFunctions.refesh(false, Launcher.UserRole);

			if (HtmlReporter.tc_result)ROATModules.Setup_navigateUser_Maintenance();
			if (HtmlReporter.tc_result)ROATModules.AddUser_UserMaintenance_screentimeout(screen_time_out);

			if (HtmlReporter.tc_result)ROATFunctions.refesh(true, "Teller",WebDr.getValue("UserID1"),WebDr.getValue("Password"));
			//if (HtmlReporter.tc_result)ROATModules.Implicitwait(screen_time_out);
			//System.out.println("done");
			//if (HtmlReporter.tc_result)WebDr.waitTillElementIsDisplayed("");

		} catch (Exception e)
		{
			Reporter.ReportEvent("Test Case execution is failed", "Test Case execution is failed",
					"Test Case execution is failed", false);
		}
	}

	public static void forexPurchase() {

		try {

			try
			{
				ROATFunctions.refesh(false, Launcher.UserRole);
				if (HtmlReporter.tc_result)
				{
					String user_branch = ROATModules.fetch_user_branch();
					if (HtmlReporter.tc_result)
						ROATFunctions.refesh(true, "Super User");
					//							if (HtmlReporter.tc_result)
					//								ROATModules.AUTHRem_navigateRemAuthorization();
					//							if (HtmlReporter.tc_result)
					//								ROATModules.updateRemoteAuthorization(user_branch);
					if (HtmlReporter.tc_result)
						ROATModules.navigate_ForexFees();
					if (HtmlReporter.tc_result)
					{
						Map<String, Integer> mapForexPurchaseValues = ROATModules.checkRecordorAddRecordForexPurchase(user_branch);
					}
				}
			}
			catch (Exception e)
			{
				Reporter.ReportEvent("Test Case execution is failed", "Test Case execution is failed",
						"Test Case execution is failed", false);
			}
			ROATFunctions.refesh(true, "Teller");
			for (int i=1; i<=2; i++) {
				if (HtmlReporter.tc_result)ROATModules.menu_navigation("MasterPage", "tab_Forex", "tab_Forex_closed");
				if (HtmlReporter.tc_result)ROATModules.menu_navigation1("MasterPage", "tab_Exchange", "tab_Exchange_closed");
				if (HtmlReporter.tc_result)ROATModules.menu_navigation1("MasterPage", "tab_Purchase", "null");
				if (HtmlReporter.tc_result)ROATModules.forexExchangePurchase(i);
				Thread.sleep(2000);
				if (HtmlReporter.tc_result)ROATModules.postForexPurchase(i);
			}

		} catch (Exception e) {
			e.printStackTrace();
		}

	}

	public static void chargeApplications() {
		String balance, user_branch, balance2 =null;
		try {
			ROATFunctions.refesh(false, Launcher.UserRole);
			//					if (HtmlReporter.tc_result)ROATFunctions.refesh(true,"Head Teller");
			//					if (HtmlReporter.tc_result)ROATModules.CD_navigateTeller_Inquiry();
			//					if (HtmlReporter.tc_result) {
			//						balance = ROATModules.verifyTellerBalance();
			//						System.out.println("balance"+balance);
			//						if (HtmlReporter.tc_result)ROATFunctions.refesh(true,"Super User");
			//						if (HtmlReporter.tc_result) {
			//							user_branch = ROATModules.fetch_user_branch();
			//							if (HtmlReporter.tc_result)ROATModules.AUTHRem_navigateRemAuthorization();
			//							if (HtmlReporter.tc_result)ROATModules.updateRemoteAuthorization(user_branch);
			if (HtmlReporter.tc_result)ROATModules.Setup_navigateTariffMaintenence();
			if (HtmlReporter.tc_result)ROATModules.setupTariffMaintenance();

			//							if (HtmlReporter.tc_result)ROATModules.CD_navigateTeller_Inquiry();
			//							if (HtmlReporter.tc_result) {
			//								balance2 = ROATModules.verifyTellerBalance();
			//								if(balance != balance2) {
			//									Reporter.ReportEvent("Balance verification", "balance should be updated", "balance is updated", true);
			//								} else {
			//									Reporter.ReportEvent("Balance verification", "balance should be updated", "balance is not updated", false);
			//								}
			//							}
			//						}
			//}
		} catch (Exception e) {
			Reporter.ReportEvent("Test Case execution is failed", "Test Case execution is failed",
					"Test Case execution is failed", false);
		}
	}

	//vault cash adjustment
	public static void vaultadjustment() {
		try {
			int flag=0;

			ROATFunctions.refesh(false, Launcher.UserRole);
			ROATModules.navigate_VaultAdjustment();
			WebDr.SetPageObjects("vaultadjustment");
			//check vault not open message 
			String error = WebDr.getElement("labelErrMsg").getText();
			System.out.println(error);

			if(WebDr.isExists_Display("labelErrMsg") == 0)
			{

				//cashin/cashout cancel functions-YES
				ROATModules.Enterdata_VaultAdjustment("cashin",flag);
				ROATModules.Enterdata_VaultAdjustment("cashout",flag);

				//cashin/cashout accept functions
				flag=1;
				ROATModules.Enterdata_VaultAdjustment("cashin",flag);
				ROATModules.Enterdata_VaultAdjustment("cashout",flag);


				//cashin/cashout cancel functions-No
				flag =2;
				ROATModules.Enterdata_VaultAdjustment("cashin",flag);
				ROATModules.Enterdata_VaultAdjustment("cashout",flag);

				//cashin/cashout reject
				flag =3;
				ROATModules.Enterdata_VaultAdjustment("cashin",flag);
				ROATModules.Enterdata_VaultAdjustment("cashout",flag);

				//35 bytes error not coded as application not working 
				ROATModules.Enterdata_VaultAdjustment("cashin",flag);

			}
			else
			{
				Reporter.ReportEvent(error, error,error, false);

			}


		} catch (Exception e) {
			e.printStackTrace();
		}
	}



	//************************************menu access******************
	public static void before_SOD() {
		try{
			try {
				DatabaseFactory.update_User_Session("SYC","0",Driver.Env_IP);
			} catch (Exception e1) {
				e1.printStackTrace();
			}
			ROATFunctions.refesh(false, Launcher.UserRole);
			String branch_no = ROATModules.fetch_user_branch();
			DatabaseFactory.update_CCP_OCP_Whole_Branch("N","Y",branch_no, "SYC",Driver.Env_IP);
			if (HtmlReporter.tc_result)ROATModules.nav_periodprocessing();
			if (HtmlReporter.tc_result)ROATModules.preriodprocces_verification();
			if (HtmlReporter.tc_result)
			{
				boolean check_sod=ROATModules.Sod_check();
				if(!check_sod)
				{
					//						if (HtmlReporter.tc_result)
					//							ROATModules.setup_StartOfTheDay();	
					if (HtmlReporter.tc_result)
						ROATModules.do_eod();
					if (HtmlReporter.tc_result)
						check_sod=ROATModules.Sod_check();

				}
				//sod check for teller
				if (HtmlReporter.tc_result)ROATFunctions.refesh(true,"Teller");
				if (HtmlReporter.tc_result)ROATModules.access_check_sod("Teller");
				//					//sod check for HeadTeller
				if (HtmlReporter.tc_result)ROATFunctions.refesh(true,"Head Teller");
				if (HtmlReporter.tc_result)ROATModules.access_check_sod("Head Teller");
				//sod check for Supervisor
				if (HtmlReporter.tc_result)ROATFunctions.refesh(true,"Supervisor");
				if (HtmlReporter.tc_result)ROATModules.access_check_sod("Supervisor");
				//sod check for SuperUser
				if (HtmlReporter.tc_result)ROATFunctions.refesh(true,"Super User");
				if (HtmlReporter.tc_result)ROATModules.access_check_sod("Super User");
				//sod check for vault custodian
				if (HtmlReporter.tc_result)ROATFunctions.refesh(true,"Vault Custodian");
				if (HtmlReporter.tc_result)ROATModules.access_check_sod("Vault Custodian");
				//sod check for Enquiry Clerk
				if (HtmlReporter.tc_result)ROATFunctions.refesh(true,"Enquiry Clerk");
				if (HtmlReporter.tc_result)ROATModules.access_check_sod("Enquiry Clerk");
				//					//sod check for Central Supervisor
				if (HtmlReporter.tc_result)ROATFunctions.refesh(true,"Central Supervisor");
				if (HtmlReporter.tc_result)ROATModules.access_check_sod("Central Supervisor");
				//sod check for User Admin 
				if (HtmlReporter.tc_result)ROATFunctions.refesh(true,"User Admin");
				if (HtmlReporter.tc_result)ROATModules.access_check_sod("User Admin");
				//sod check for Signature Authorizer
				if (HtmlReporter.tc_result)ROATFunctions.refesh(true,"Signature Authorizer");
				if (HtmlReporter.tc_result)ROATModules.access_check_sod("Signature Authorizer");
				//sod check for Global Admin
				if (HtmlReporter.tc_result)ROATFunctions.refesh(true,"Global Admin");
				if (HtmlReporter.tc_result)ROATModules.access_check_sod("Global Admin");
				//sod check for Back office clerk
				if (HtmlReporter.tc_result)ROATFunctions.refesh(true,"Back office clerk");
				if (HtmlReporter.tc_result)ROATModules.access_check_sod("Back office clerk");
				//sod check for Authorizer
				if (HtmlReporter.tc_result)ROATFunctions.refesh(true,"Authorizer");
				if (HtmlReporter.tc_result)ROATModules.access_check_sod("Authorizer");


			}
		}
		catch (Exception e) {
			Reporter.ReportEvent("Testcase execution failed", "End to end functionality failed signature add", "End to end functionality failed signature add", false);
			e.printStackTrace();

		}
	}

	public static void after_sod_before_OCP() {
		try{
			try {
				//DatabaseFactory.update_User_Session(Driver.Country_code,"0",Driver.Env_IP);//rhea added comment
				DatabaseFactory.update_User_Session(CommonUtils.readConfig("Country_code"),"0",Driver.Env_IP);
			} catch (Exception e1) {
				e1.printStackTrace();
			}
			ROATFunctions.refesh(false, Launcher.UserRole);
			String branch_no = ROATModules.fetch_user_branch();
			DatabaseFactory.update_CCP_OCP_Whole_Branch("N","Y",branch_no, CommonUtils.readConfig("Country_code"),Driver.Env_IP);

			if (HtmlReporter.tc_result)ROATModules.nav_periodprocessing();
			if (HtmlReporter.tc_result)ROATModules.preriodprocces_verification();
			if (HtmlReporter.tc_result)
			{
				boolean check_sod=ROATModules.Sod_check_bf_opc();
				if(!check_sod)
				{
					//						if (HtmlReporter.tc_result)
					//							ROATModules.setup_StartOfTheDay();	
					if (HtmlReporter.tc_result)
						ROATModules.do_sod();

				}
				else
				{
					if (HtmlReporter.tc_result)
						ROATModules.do_eod();
					if (HtmlReporter.tc_result)
						ROATModules.do_sod();
				}

				//sod check for teller
				if (HtmlReporter.tc_result)ROATFunctions.refesh(true,"Teller");
				if (HtmlReporter.tc_result)ROATModules.access_check_cp("Teller");
				//sod check for HeadTeller
				if (HtmlReporter.tc_result)ROATFunctions.refesh(true,"Head Teller");
				if (HtmlReporter.tc_result)ROATModules.access_check_cp("Head Teller");
				//sod check for Supervisor
				if (HtmlReporter.tc_result)ROATFunctions.refesh(true,"Supervisor");
				if (HtmlReporter.tc_result)ROATModules.access_check_cp("Supervisor");
				//sod check for SuperUser
				if (HtmlReporter.tc_result)ROATFunctions.refesh(true,"Super User");
				if (HtmlReporter.tc_result)ROATModules.access_check_cp("Super User");
				//sod check for vault custodian
				if (HtmlReporter.tc_result)ROATFunctions.refesh(true,"Vault Custodian");
				if (HtmlReporter.tc_result)ROATModules.access_check_cp("Vault Custodian");
				//sod check for Enquiry Clerk
				if (HtmlReporter.tc_result)ROATFunctions.refesh(true,"Enquiry Clerk");
				if (HtmlReporter.tc_result)ROATModules.access_check_cp("Enquiry Clerk"); ///////////////////////
				//sod check for Central Supervisor
				if (HtmlReporter.tc_result)ROATFunctions.refesh(true,"Central Supervisor");
				if (HtmlReporter.tc_result)ROATModules.access_check_cp("Central Supervisor");////////////////////////
				//sod check for User Admin
				if (HtmlReporter.tc_result)ROATFunctions.refesh(true,"User Admin");
				if (HtmlReporter.tc_result)ROATModules.access_check_cp("User Admin");
				//sod check for Signature Authorizer
				if (HtmlReporter.tc_result)ROATFunctions.refesh(true,"Signature Authorizer");
				if (HtmlReporter.tc_result)ROATModules.access_check_cp("Signature Authorizer");
				//sod check for Global Admin
				if (HtmlReporter.tc_result)ROATFunctions.refesh(true,"Global Admin");
				if (HtmlReporter.tc_result)ROATModules.access_check_cp("Global Admin");
				//sod check for Back office clerk
				if (HtmlReporter.tc_result)ROATFunctions.refesh(true,"Back office clerk");
				if (HtmlReporter.tc_result)ROATModules.access_check_cp("Back office clerk");
				//sod check for Authorizer
				if (HtmlReporter.tc_result)ROATFunctions.refesh(true,"Authorizer");
				if (HtmlReporter.tc_result)ROATModules.access_check_cp("Authorizer");


			}
		}
		catch (Exception e) {
			Reporter.ReportEvent("Testcase execution failed", "End to end functionality failed signature add", "End to end functionality failed signature add", false);
			e.printStackTrace();

		}
	}
	public static void after_CCP() {
		try{
			try {
				//DatabaseFactory.update_User_Session(Driver.Country_code,"0",Driver.Env_IP);//rhea added comment
				DatabaseFactory.update_User_Session(CommonUtils.readConfig("Country_code"),"0",Driver.Env_IP);
			} catch (Exception e1) {
				e1.printStackTrace();
			}
			ROATFunctions.refesh(false, Launcher.UserRole);
			String branch_no = ROATModules.fetch_user_branch();
			//DatabaseFactory.update_CCP_OCP_Whole_Branch("N","Y",branch_no, Driver.Country_code,Driver.Env_IP);
			DatabaseFactory.update_CCP_OCP_Whole_Branch("N","Y",branch_no, CommonUtils.readConfig("Country_code"),Driver.Env_IP);
			if (HtmlReporter.tc_result)ROATModules.nav_periodprocessing();
			if (HtmlReporter.tc_result)ROATModules.preriodprocces_verification();
			if (HtmlReporter.tc_result)
			{
				boolean check_sod=ROATModules.Sod_check_bf_opc();
				if(!check_sod)
				{
					//					if (HtmlReporter.tc_result)
					//						ROATModules.setup_StartOfTheDay();	
					if (HtmlReporter.tc_result)
						ROATModules.do_sod();

				}
				else
				{
					if (HtmlReporter.tc_result)
						ROATModules.do_eod();
					if (HtmlReporter.tc_result)
						ROATModules.do_sod();
				}
				if (HtmlReporter.tc_result)ROATFunctions.refesh(true,"Teller");

				if (HtmlReporter.tc_result)ROATModules.do_ocp();
				if (HtmlReporter.tc_result)ROATModules.do_ccp();

				//sod check for teller
				if (HtmlReporter.tc_result)ROATFunctions.refesh(true,"Teller");
				if (HtmlReporter.tc_result)ROATModules.access_check_cp("Teller");
				//sod check for HeadTeller
				//					if (HtmlReporter.tc_result)ROATFunctions.refesh(true,"Head Teller");
				//					if (HtmlReporter.tc_result)ROATModules.access_check_cp("Head Teller");
				//sod check for Supervisor
				//					if (HtmlReporter.tc_result)ROATFunctions.refesh(true,"Supervisor");
				//					if (HtmlReporter.tc_result)ROATModules.access_check_cp("Supervisor");
				//					//sod check for SuperUser
				//					if (HtmlReporter.tc_result)ROATFunctions.refesh(true,"Super User");
				//					if (HtmlReporter.tc_result)ROATModules.access_check_cp("Super User");
				//sod check for vault custodian
				//					if (HtmlReporter.tc_result)ROATFunctions.refesh(true,"Vault Custodian");
				//					if (HtmlReporter.tc_result)ROATModules.access_check_vp("Vault Custodian");
				//sod check for Enquiry Clerk
				//					if (HtmlReporter.tc_result)ROATFunctions.refesh(true,"Enquiry Clerk");
				//					//sod check for Central Supervisor
				//					if (HtmlReporter.tc_result)ROATFunctions.refesh(true,"Central Supervisor");
				//					if (HtmlReporter.tc_result)ROATModules.access_check_cp("Central Supervisor");
				//					//sod check for User Admin
				//					if (HtmlReporter.tc_result)ROATFunctions.refesh(true,"User Admin");
				//					if (HtmlReporter.tc_result)ROATModules.access_check_cp("User Admin");
				//					//sod check for Signature Authorizer
				//					if (HtmlReporter.tc_result)ROATFunctions.refesh(true,"Signature Authorizer");
				//					if (HtmlReporter.tc_result)ROATModules.access_check_cp("Signature Authorizer");
				//					//sod check for Global Admin
				//					if (HtmlReporter.tc_result)ROATFunctions.refesh(true,"Global Admin");
				//					if (HtmlReporter.tc_result)ROATModules.access_check_cp("Global Admin");
				//					//sod check for Back office clerk
				//					if (HtmlReporter.tc_result)ROATFunctions.refesh(true,"Back office clerk");
				//					if (HtmlReporter.tc_result)ROATModules.access_check_cp("Back office clerk");
				//					//sod check for Authorizer
				//					if (HtmlReporter.tc_result)ROATFunctions.refesh(true,"Authorizer");
				//					if (HtmlReporter.tc_result)ROATModules.access_check_cp("Authorizer");
				//

			}
		}
		catch (Exception e) {
			Reporter.ReportEvent("Testcase execution failed", "End to end functionality failed signature add", "End to end functionality failed signature add", false);
			e.printStackTrace();

		}
	}
	public static void tellerOCP() {
		try {
			try {
				//DatabaseFactory.update_User_Session(Driver.Country_code,"0",Driver.Env_IP);//rhea added comment
				DatabaseFactory.update_User_Session(CommonUtils.readConfig("Country_code"),"0",Driver.Env_IP);
			} catch (Exception e1)
			{
				e1.printStackTrace();
			}
			ROATFunctions.refesh(false, Launcher.UserRole);
			String branch_no = ROATModules.fetch_user_branch();
			DatabaseFactory.update_CCP_OCP_Whole_Branch("N","Y",branch_no, CommonUtils.readConfig("Country_code"),Driver.Env_IP);
			if (HtmlReporter.tc_result)ROATModules.menu_navigation("MasterPage", "tab_CashDrawer", "tab_CashDrawer_closed");
			if (HtmlReporter.tc_result)ROATModules.menu_navigation1("MasterPage", "CCP", "null");
			if (HtmlReporter.tc_result)ROATModules.performTellerOCP();
			if (HtmlReporter.tc_result)ROATModules.performTellerCCP();
		} 
		catch (Exception e)
		{
			e.printStackTrace();
		}
	}
	public static void sequenceNoMaint() {
		try {
			ROATFunctions.refesh(false, Launcher.UserRole);
			if (HtmlReporter.tc_result)ROATModules.menu_navigation("MasterPage", "tab_setup", "tab_setup_closed");
			if (HtmlReporter.tc_result)ROATModules.menu_navigation1("MasterPage", "tab_seqno", "null");
			if (HtmlReporter.tc_result)ROATModules.verifySequenceNo();
		} catch (Exception e) {
			e.printStackTrace();
		}
	}
	public static void performStatementRequest() {
		String seq_no = "";
		try {
			ROATFunctions.refesh(false, Launcher.UserRole);
			if (HtmlReporter.tc_result)ROATModules.menu_navigation("MasterPage", "tab_acc_info", "tab_accInfo_closed");
			if (HtmlReporter.tc_result)ROATModules.menu_navigation1("MasterPage", "tab_statement", "null");
			if (HtmlReporter.tc_result) {
				seq_no = ROATModules.statementRequest();
			}
			if (HtmlReporter.tc_result)ROATModules.reverseStatementRequest(seq_no);
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	public static void Decoy()
	{				
		try
		{
			ROATFunctions.refesh(false, Launcher.UserRole);
			String a_str ="";
			double a=0;
			//check for account amount error 
			if (HtmlReporter.tc_result) a_str =ROATModules.setup_navigateCurrency_decoy();
			if (HtmlReporter.tc_result) a= Double.parseDouble(a_str);
			if (HtmlReporter.tc_result)a = a + 1;
			if (HtmlReporter.tc_result) ROATModules.setup_LcyChangeinAmountNavigation();

			if (HtmlReporter.tc_result) ROATModules.checkerror(a);
			//navigate enter data and check for disabled
			if (HtmlReporter.tc_result) ROATModules.setup_LcyChangeinAmountNavigation();
			if (HtmlReporter.tc_result)WebDr.SetPageObjects("decoy");
			if (HtmlReporter.tc_result) WebDr.waitTillElementIsDisplayed("txtAmount");
			if (HtmlReporter.tc_result) WebDr.setText("X", "txtAmount",WebDr.getValue("amount"), "set amount greater should give errror ", true);
			if (HtmlReporter.tc_result)ROATFunctions.clickTab();
			if (HtmlReporter.tc_result) ROATModules.disablecheck();
		}
		catch (Exception e)
		{
			e.printStackTrace();
			Reporter.ReportEvent("Decoy test case", "Decoy test case", "Decoy test case failed", false);
		}
	}

	public static void MICRTypes() {

		try{

			ROATFunctions.refesh(false, Launcher.UserRole);
			if (HtmlReporter.tc_result)WebDr.SetPageObjects("MasterPage");
			if (HtmlReporter.tc_result)ROATModules.navigate_MICRTypes();
			if (HtmlReporter.tc_result)ROATModules.addnew_MICRTypes();
		}
		catch(Exception e){
			e.printStackTrace();
		}
	}
	public static void MICRParameters() {

		try{
			ROATFunctions.refesh(false, Launcher.UserRole);
			if (HtmlReporter.tc_result)WebDr.SetPageObjects("MasterPage");
			if (HtmlReporter.tc_result)ROATModules.navigate_MICRParameters();
			if (HtmlReporter.tc_result)ROATModules.addnew_MICRParameters();
		}
		catch(Exception e){
			e.printStackTrace();
			Reporter.ReportEvent("Test case execution failed", "Test case execution failed","Test case execution failed", false);
		}

	}

	//*******************vault change amount**************************************************************************************
	public static void vlt_lcy_change()
	{
		try {
			ROATFunctions.refesh(false, Launcher.UserRole);
			if (HtmlReporter.tc_result)ROATModules.vlt_lcy_change_navigation();
			if (HtmlReporter.tc_result)ROATModules.vlt_lcy_change_cms();
			if (HtmlReporter.tc_result)ROATModules.vlt_nav_change_page(false,"100");
			if (HtmlReporter.tc_result)ROATModules.vlt_change_cancel(false);
			if (HtmlReporter.tc_result)ROATModules.vlt_nav_change_page(false,"100");
			if (HtmlReporter.tc_result)ROATModules.vlt_change_soil_decoy();
			if (HtmlReporter.tc_result)
			{
				if (HtmlReporter.tc_result)ROATModules.vlt_nav_change_page(false,WebDr.getValue("Amount_reject"));
				ArrayList<Integer> pos_bfr_reject=ROATModules.vlt_change_positon_check("Vault_Lcy_Change");
				if (HtmlReporter.tc_result)ROATModules.vlt_change_reject(false);
				if (HtmlReporter.tc_result)ROATModules.vlt_nav_change_page(false,WebDr.getValue("Amount_reject"));
				List<Integer> pos_aft_reject=ROATModules.vlt_change_positon_check("Vault_Lcy_Change");
				if (HtmlReporter.tc_result)
				{
					pos_bfr_reject=ROATModules.perform_update("Vault_Lcy_Change", pos_bfr_reject, "0",  "0", "0", "0", "0", "0");
					if(pos_bfr_reject.stream().equals(pos_aft_reject.stream()))
					{
						Reporter.ReportEvent("Checking vault position update ", "Vault postion must be updated", "Vault postion is updated", true);
					}
					else
					{
						Reporter.ReportEvent("Checking vault position update ", "Vault postion must be updated", "Vault postion is not  updated", true);
					}
				}
				if (HtmlReporter.tc_result)ROATModules.vlt_lcy_change_navigation();
				if (HtmlReporter.tc_result)ROATModules.vlt_nav_change_page(false,WebDr.getValue("Amount_reject"));
				if (HtmlReporter.tc_result)ROATModules.vlt_change_authorize(false);
				if (HtmlReporter.tc_result)ROATModules.vlt_nav_change_page(false,WebDr.getValue("Amount_reject"));
				List<Integer> pos_aft_auth=ROATModules.vlt_change_positon_check("Vault_Lcy_Change");
				if (HtmlReporter.tc_result)
				{
					pos_aft_reject=ROATModules.perform_update("Vault_Lcy_Change", pos_bfr_reject, WebDr.getValue("note1_in"),  WebDr.getValue("note2_in"), WebDr.getValue("note3_in"), WebDr.getValue("note1_out"), WebDr.getValue("note2_out"), WebDr.getValue("note3_out"));
					if(pos_aft_reject.equals(pos_aft_auth))
					{
						Reporter.ReportEvent("Checking vault position update ", "Vault postion must be updated", "Vault postion is updated", true);
					}
					else
					{
						Reporter.ReportEvent("Checking vault position update ", "Vault postion must be updated", "Vault postion is not updated", true);
					}
				}

			} 
			if (HtmlReporter.tc_result)ROATModules.vlt_lcy_error_msg_check();
		}
		catch (Exception e)
		{
			e.printStackTrace();
			Reporter.ReportEvent("LCY vault change in", "LCY vault change in must be successfull", "LCY vault change in failed", true);
		}
	}

	public static void vlt_fcy_change()
	{
		try {
			ROATFunctions.refesh(false, Launcher.UserRole);
			if (HtmlReporter.tc_result)ROATModules.vlt_fcy_change_navigation();
			if (HtmlReporter.tc_result)ROATModules.vlt_fcy_change_cms();
			if (HtmlReporter.tc_result)ROATModules.vlt_nav_change_page(true,"100");
			if (HtmlReporter.tc_result)ROATModules.vlt_change_cancel(true);
			//					if (HtmlReporter.tc_result)ROATModules.vlt_nav_change_page(true,"100");
			//					if (HtmlReporter.tc_result)ROATModules.vlt_change_soil_decoy();
			if (HtmlReporter.tc_result)
			{
				if (HtmlReporter.tc_result)ROATModules.vlt_nav_change_page(true,WebDr.getValue("Amount_reject"));
				ArrayList<Integer> pos_bfr_reject=ROATModules.vlt_change_positon_check("Vault_Fcy_Change");
				if (HtmlReporter.tc_result)ROATModules.vlt_change_reject(true);
				if (HtmlReporter.tc_result)ROATModules.vlt_nav_change_page(true,WebDr.getValue("Amount_reject"));
				ArrayList<Integer> pos_aft_reject=ROATModules.vlt_change_positon_check("Vault_Fcy_Change");
				if (HtmlReporter.tc_result)
				{
					pos_bfr_reject=ROATModules.perform_update("Vault_Fcy_Change", pos_bfr_reject, "0",  "0", "0", "0", "0", "0");
					if(pos_bfr_reject.equals(pos_aft_reject))
					{
						Reporter.ReportEvent("Checking vault position update ", "Vault postion must be updated", "Vault postion is updated", true);
					}
					else
					{
						Reporter.ReportEvent("Checking vault position update ", "Vault postion must be updated", "Vault postion is not  updated", false);
					}
				}
				//					if (HtmlReporter.tc_result)ROATModules.vlt_nav_change_page(true,WebDr.getValue("Amount_reject"));
				if (HtmlReporter.tc_result)ROATModules.vlt_change_authorize(true);
				if (HtmlReporter.tc_result)ROATModules.vlt_nav_change_page(true,WebDr.getValue("Amount_reject"));
				ArrayList<Integer> pos_aft_auth=ROATModules.vlt_change_positon_check("Vault_Fcy_Change");
				if (HtmlReporter.tc_result)
				{
					pos_aft_reject=ROATModules.perform_update("Vault_Fcy_Change", pos_bfr_reject, WebDr.getValue("note1_in"),  WebDr.getValue("note2_in"), WebDr.getValue("note3_in"), WebDr.getValue("note1_out"), WebDr.getValue("note2_out"), WebDr.getValue("note3_out"));
					if(pos_aft_reject.equals(pos_aft_auth))
					{
						Reporter.ReportEvent("Checking vault position update ", "Vault postion must be updated", "Vault postion is updated", true);
					}
					else
					{
						Reporter.ReportEvent("Checking vault position update ", "Vault postion must be updated", "Vault postion is not updated", true);
					}
				}

			}
			if (HtmlReporter.tc_result)ROATModules.vlt_fcy_error_msg_check();
		}
		catch (Exception e)
		{
			e.printStackTrace();
			Reporter.ReportEvent("FCY vault change in", "FCY vault change in must be successfull", "FCY vault change in failed", true);
		}
	}


	public static void MM_check() 
	{
		HashMap<String,HashMap<String, BigDecimal>> cd_inquiry_fcy = new HashMap<String,HashMap<String, BigDecimal>>();
		HashMap<String, BigDecimal> cd_inquiry_lcy = new HashMap<String, BigDecimal>();
		HashMap<String, BigDecimal> vlt_inquiry_lcy = new HashMap<String, BigDecimal>();
		try 
		{	
			//					ROATModules.CD_teller_inquiry("USD-U.S.DOLLARS");
			//					vlt_inquiry_lcy=ROATModules.Vault_LCY_Inquiry();

			cd_inquiry_lcy=ROATModules.LCY_inquiry();
			cd_inquiry_fcy=ROATModules.FCY_Inquiry();
			System.out.println(cd_inquiry_fcy);
			//					ROATModules.Vault_FCY_Inquiry();
			//					ROATModules.perform_tellerCCP();
			//					ROATFunctions.refesh(false, "Supervisor");
			//					ROATModules.perform_eod_sod();
			//					ROATModules.perform_tellerOCP();
			//					ROATModules.CD_teller_inquiry();
			//					ROATModules.Vault_LCY_Inquiry();
			//					ROATModules.LCY_inquiry();
			//					ROATModules.FCY_Inquiry();
			//					ROATModules.Vault_FCY_Inquiry();
		} 
		catch (Exception e) 
		{
			e.printStackTrace();
		}


	}

	public static void forexDrafts() {
		try {
			//			
			//			if (HtmlReporter.tc_result)
			//				ROATFunctions.refesh(true, "Super User");
			//			String user_branch = ROATModules.fetch_user_branch();
			//			if (HtmlReporter.tc_result)
			//				ROATModules.navigate_ForexFees();
			//			if (HtmlReporter.tc_result)
			//			{
			//				Map<String, Integer> mapForexPurchaseValues = ROATModules.checkRecordorAddRecordForexPurchase(user_branch);
			//			}
			ROATFunctions.refesh(true, "Teller");
			if (HtmlReporter.tc_result)ROATModules.menu_navigation("MasterPage", "tab_Forex", "tab_Forex_closed");
			if (HtmlReporter.tc_result)ROATModules.menu_navigation("MasterPage", "tab_drafts", "tab_drafts_closed");
			if (HtmlReporter.tc_result)ROATModules.menu_navigation1("MasterPage", "tab_draft_issue", "null");
			if (HtmlReporter.tc_result)ROATModules.forexDraftsIssue("Yes");
			if (HtmlReporter.tc_result) ROATModules.menu_navigation("MasterPage", "tab_Forex", "tab_Forex_closed");
			if (HtmlReporter.tc_result) ROATModules.menu_navigation("MasterPage", "tab_drafts", "tab_drafts_closed");
			if (HtmlReporter.tc_result) ROATModules.menu_navigation1("MasterPage", "tab_draft_view", "null");
			if (HtmlReporter.tc_result) ROATModules.ForexDraft_View();
			if (HtmlReporter.tc_result)ROATModules.menu_navigation("MasterPage", "tab_Forex", "tab_Forex_closed");
			if (HtmlReporter.tc_result)ROATModules.menu_navigation("MasterPage", "tab_drafts", "tab_drafts_closed");
			if (HtmlReporter.tc_result)ROATModules.menu_navigation1("MasterPage", "tab_draft_issue", "null");
			if (HtmlReporter.tc_result)ROATModules.forexDraftsIssue("No");
			if (HtmlReporter.tc_result) ROATModules.menu_navigation("MasterPage", "tab_Forex", "tab_Forex_closed");
			if (HtmlReporter.tc_result) ROATModules.menu_navigation("MasterPage", "tab_drafts", "tab_drafts_closed");
			if (HtmlReporter.tc_result) ROATModules.menu_navigation1("MasterPage", "tab_draft_pending", "null");
			if (HtmlReporter.tc_result) ROATModules.ForexDraft_Filter();
			if (HtmlReporter.tc_result) ROATModules.ForexDraft_Sorting();
			//if (HtmlReporter.tc_result)	ROATModules.ForexDraft_CountTotalRecords();
			if (HtmlReporter.tc_result) ROATModules.ForexDraft_Actions();
			if (HtmlReporter.tc_result)ROATModules.menu_navigation("MasterPage", "tab_Forex", "tab_Forex_closed");
			if (HtmlReporter.tc_result)ROATModules.menu_navigation("MasterPage", "tab_drafts", "tab_drafts_closed");
			if (HtmlReporter.tc_result)ROATModules.menu_navigation1("MasterPage", "tab_draft_issue", "null");
			if (HtmlReporter.tc_result)ROATModules.forexDraftsIssue("No");
			if (HtmlReporter.tc_result)ROATModules.menu_navigation("MasterPage", "tab_Forex", "tab_Forex_closed");
			if (HtmlReporter.tc_result)ROATModules.menu_navigation("MasterPage", "tab_drafts", "tab_drafts_closed");
			if (HtmlReporter.tc_result)ROATModules.menu_navigation1("MasterPage", "tab_draft_cancel", "null");
			if (HtmlReporter.tc_result)ROATModules.forexDraftsCancel();
		} catch (Exception e) {
			e.printStackTrace();
		}

	}

	public static void ActualUnClearedEffects()
	{
		try
		{
			ROATFunctions.refesh(false, Launcher.UserRole);

			if (HtmlReporter.tc_result) ROATModules.menu_navigation("MasterPage", "tab_Value_Adjustment", "tab_Value_Adjustment_closed");
			if (HtmlReporter.tc_result) ROATModules.menu_navigation("MasterPage", "tab_Actual_Uncleared", "null");

			if (HtmlReporter.tc_result) ROATModules.ActualUnclearedEffectsCancel();
			if (HtmlReporter.tc_result) ROATModules.ActualUnclearedEffects();
			if (HtmlReporter.tc_result) ROATModules.ActualUnclearedEffectsValidation();
		}
		catch(Exception ex)
		{
			ex.printStackTrace();
			Reporter.ReportEvent("testcase failed", "testcase failed", "testcase failed", false);
		}
	}

	public static void InterestUnClearedEffects()
	{
		try
		{
			ROATFunctions.refesh(false, Launcher.UserRole);

			if (HtmlReporter.tc_result) ROATModules.menu_navigation("MasterPage", "tab_Value_Adjustment", "tab_Value_Adjustment_closed");
			if (HtmlReporter.tc_result) ROATModules.menu_navigation("MasterPage", "tab_Interest_Uncleared", "null");
			if (HtmlReporter.tc_result) ROATModules.InterestUnclearedEffectsCancel();
			if (HtmlReporter.tc_result) ROATModules.InterestUnclearedEffects();
			if (HtmlReporter.tc_result) ROATModules.InterestUnclearedEffectsValidation();
		}
		catch(Exception ex)
		{
			ex.printStackTrace();
			Reporter.ReportEvent("testcase failed", "testcase failed", "testcase failed", false);
		}
	}



	public static void Teller_Posting() {
		try {	ROATFunctions.refesh(true, Launcher.UserRole);
		if (HtmlReporter.tc_result)ROATModules.menu_navigation("MasterPage", "tab_Teller", "tab_Teller_closed");
		if (HtmlReporter.tc_result)ROATModules.menu_navigation1("MasterPage", "tab_Posting", "null");
		if (HtmlReporter.tc_result)ROATModules.postingCms();
		if (HtmlReporter.tc_result)ROATModules.postingUpdateDelete();
		if (HtmlReporter.tc_result)ROATModules.postingAdd();

		} catch (Exception e) {
			e.printStackTrace();
			Reporter.ReportEvent("Test Case execution is Failed", "excution successfull",
					"excution Failed", false);
		}

	}
	public static void batchPosting() 
	{
		try{
			ROATFunctions.refesh(false, Launcher.UserRole);
			if (HtmlReporter.tc_result)ROATModules.menu_navigation("MasterPage", "tab_Teller", "null");
			if (HtmlReporter.tc_result)ROATModules.menu_navigation1("MasterPage", "tab_Batch", "null");
			if (HtmlReporter.tc_result)ROATModules.Batch_Posting_ContinueDelete_Batch("Continue");
			if (HtmlReporter.tc_result) ROATModules.reNavigateToPosting();
			if (HtmlReporter.tc_result)ROATModules.Batch_Posting_ContinueDelete_Batch("Delete");
			if (HtmlReporter.tc_result)ROATModules.Batch_Posting_Cancel();
			if (HtmlReporter.tc_result)ROATModules.Batch_Posting_Add();
			if (HtmlReporter.tc_result)ROATModules.Batch_Posting_Cancel();
			if (HtmlReporter.tc_result)ROATModules.Batch_Posting_Add();
			if (HtmlReporter.tc_result)ROATModules.Batch_Posting_Saveforlater();
			if (HtmlReporter.tc_result) ROATModules.reNavigateToPosting();
			if (HtmlReporter.tc_result)ROATModules.Batch_Posting_Add_Next();
		} catch (Exception e) {
			e.printStackTrace();
			Reporter.ReportEvent("Test Case execution is Failed", "excution successfull",
					"excution Failed", false);
		}
	} 



	public static void Vaultopencashposition() {
		try {

			WebDr.SetPageObjects("MasterPage");
			ROATFunctions.refesh(false, Launcher.UserRole);

			if (HtmlReporter.tc_result) ROATModules.navigate_Vaultopencashpositionnavigate("open");

			if (HtmlReporter.tc_result) WebDr.SetPageObjects("Vaultopencashclosecash");
			//check vault open message 
			if (HtmlReporter.tc_result)
			{
				//				String value = WebDr.getElement("labelErrMsg").getText();
				//				if(value.equalsIgnoreCase(WebDr.getValue("check2")))
				if((WebDr.isExists_Display("labelErrMsg")>0))
				{
					if (HtmlReporter.tc_result) ROATModules.opentoclose();
					if (HtmlReporter.tc_result) ROATModules.closetoopen();
				}
				else 
				{
					if (HtmlReporter.tc_result) ROATModules.navigate_Vaultopencashpositionnavigate("close");	
					if (HtmlReporter.tc_result) WebDr.SetPageObjects("Vaultopencashclosecash");
					WebDr.waitTillElementIsDisplayed("labelErrMsg");
					String value1 = WebDr.getElement("labelErrMsg").getText();
					if (value1.toLowerCase().contains(WebDr.getValue("check1").toLowerCase()))
					{
						if (HtmlReporter.tc_result) ROATModules.closetoopen();
						if (HtmlReporter.tc_result) ROATModules.opentoclose();
					}

					else
					{
						String error = WebDr.getElement("labelErrMsg").getText();
						if (HtmlReporter.tc_result) Reporter.ReportEvent(error, error,error, false);
					}
				}
			}

		}
		catch (Exception e) 
		{
			Reporter.ReportEvent("Test Case execution is failed", "Test Case execution is failed","Test Case execution is failed", false);
			e.printStackTrace();
		}
	}

	public static void FrontArena()
	{
		try
		{
			ROATFunctions.refesh(false, Launcher.UserRole);
			WebDr.SetPageObjects("MasterPage");
			if (HtmlReporter.tc_result) ROATModules.menu_navigation("MasterPage", "tab_setup", "tab_setup_closed");
			if (HtmlReporter.tc_result) ROATModules.menu_navigation1("MasterPage", "tab_front_arena", "null");

			WebDr.SetPageObjects("FrontArena");
			if (HtmlReporter.tc_result) WebDr.waitTillElementIsDisplayed("btn_AddNew");
			if (HtmlReporter.tc_result)WebDr.clickX("X", "btn_AddNew", "Clicking on btn_AddNew ", true);
			if (HtmlReporter.tc_result) ROATModules.FADetails_submit(false);
			if (HtmlReporter.tc_result) WebDr.waitTillElementIsDisplayed("btn_AddNew");
			if (HtmlReporter.tc_result)WebDr.clickX("X", "btn_AddNew", "Clicking on btn_AddNew ", true);
			if (HtmlReporter.tc_result) ROATModules.FADetails_submit(true);
			//			if (HtmlReporter.tc_result) ROATModules.FADetails_view();
			//			if (HtmlReporter.tc_result) ROATModules.FADetails_Update(true);
			//			if (HtmlReporter.tc_result) ROATModules.FADetails_Update(false);

			if (HtmlReporter.tc_result) ROATModules.FADetails_Delete(false);
			if (HtmlReporter.tc_result) ROATModules.FADetails_Delete(true);

			//sarang
			//			if (HtmlReporter.tc_result) ROATFunctions.CDA_SysDateValidation("FrontArena","Front_Arena_Date");
			//			if (HtmlReporter.tc_result) ROATModules.FrontArena_Sorting();
			//			if (HtmlReporter.tc_result)	ROATModules.FrontArena_CountTotalRecords();
			//			if (HtmlReporter.tc_result) ROATModules.FrontArena_Filter();

		}
		catch(Exception ex)
		{
			ex.printStackTrace();
		}
	}

	public static void Accgroups() {
		try{

			ROATFunctions.refesh(false, Launcher.UserRole);

			WebDr.SetPageObjects("MasterPage");
			//navigate 
			if(HtmlReporter.tc_result)ROATModules.navigateAccGroups();

			if(HtmlReporter.tc_result)WebDr.SetPageObjects("Accountgroups");
			//cancel
			if(HtmlReporter.tc_result)ROATModules.cancelAccGroups(true);
			if(HtmlReporter.tc_result)ROATModules.cancelAccGroups(false);
			//add
			String seqno= ROATModules.AddAccGroups("1");
			if(HtmlReporter.tc_result)
			{
				String revseqno=(WebDr.getValue("terminal_no").concat(seqno));
				System.out.println(revseqno);
				if(HtmlReporter.tc_result)ROATModules.AddAccGroups("2");
				if(HtmlReporter.tc_result)ROATModules.AddAccGroups("3");
				if(HtmlReporter.tc_result)ROATModules.AddAccGroups("4");

				//reverse
				//			if(HtmlReporter.tc_result)ROATModules.reverseAccGroups(revseqno);

				//delete
				if(HtmlReporter.tc_result)ROATModules.deleteAccGroups(false,true);
				if(HtmlReporter.tc_result)ROATModules.deleteAccGroups(false,false);
				if(HtmlReporter.tc_result)ROATModules.deleteAccGroups(true,false);
			}}
		catch(Exception e){
			e.printStackTrace();
			Reporter.ReportEvent("Test Case execution is Failed", "excution successfull",
					"excution Failed", false);
		}

	}
	public static void AccountMaintaince() {
		try {
			if(HtmlReporter.tc_result)WebDr.SetPageObjects("MasterPage");
			if(HtmlReporter.tc_result)ROATFunctions.refesh(false, Launcher.UserRole);
			//navigate 
			if(HtmlReporter.tc_result)ROATModules.NavigateAccountMaintaince();
			if(HtmlReporter.tc_result)ROATModules.ClearSearchAccountMaintaince(true);
			if(HtmlReporter.tc_result)ROATModules.ClearSearchAccountMaintaince(false);
			if(HtmlReporter.tc_result)ROATModules.UpdateAccountMaintaince(true);

		} catch (Exception e) {

			e.printStackTrace();
			Reporter.ReportEvent("Test Case execution is Failed", "excution successfull",
					"excution Failed", false);
		}

	}

	public static void ForexInquiryBills() {
		try {
			WebDr.SetPageObjects("MasterPage");
			ROATFunctions.refesh(false, Launcher.UserRole);
			//navigate 
			ROATModules.NavigateForexInquiryBills();
			//inquire
			ROATModules.InquireForexInquiryBills();

		} catch (Exception e) {

			e.printStackTrace();
			Reporter.ReportEvent("Test Case execution is Failed", "excution successfull",
					"excution Failed", false);
		}
	}


	public static void AutoTransfers() 
	{
		try
		{
			WebDr.SetPageObjects("MasterPage");
			ROATFunctions.refesh(false, Launcher.UserRole);
			//navigate 
			if(HtmlReporter.tc_result)ROATModules.NavigateAutoTransfers();
			//Add Cancel
			if(HtmlReporter.tc_result)ROATModules.AutoTransferCancel();
			//Add 
			if(HtmlReporter.tc_result)
			{
				String seqNo=ROATModules.AutoTransfersubmit();

				if(HtmlReporter.tc_result)
				{
					String seqNo1=WebDr.getValue("terminalno")+seqNo;
					//reverse
					if(HtmlReporter.tc_result)ROATModules.AutoTransferReverse(seqNo1);
					//delete
					if(HtmlReporter.tc_result)ROATModules.AutoTransferDelete();
					//change
					if(HtmlReporter.tc_result)ROATModules.AutoTransferChange();
				}
			}

		} catch (Exception e)
		{

			e.printStackTrace();
			Reporter.ReportEvent("Test Case execution is Failed", "Auto transfer excution successfull","Auto transfer excution Failed", false);
		}
	}
	public static void MailingAddress() {
		try {
			WebDr.SetPageObjects("MasterPage");
			ROATFunctions.refesh(false, Launcher.UserRole);
			//navigate 
			if(HtmlReporter.tc_result)ROATModules.NavigateMailingAdress();
			//Add Cancel
			if(HtmlReporter.tc_result)ROATModules.MailingAdressCancel();
			//Add 
			if(HtmlReporter.tc_result){
				String seqNo=ROATModules.MailingAddresssubmit();
				if(HtmlReporter.tc_result){
					String seqNo1=WebDr.getValue("terminalno")+seqNo;
					//reverse
					if(HtmlReporter.tc_result)ROATModules.MailingAddressReverse(seqNo1);
					//delete
					if(HtmlReporter.tc_result)ROATModules.MailingAddressDelete();
					//change
					if(HtmlReporter.tc_result)ROATModules.MailingAddressChange();
				}
			}
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
	public static void UT_AccInfoMaint() {
		String sequence="";
		try {
			ROATFunctions.refesh(false, Launcher.UserRole);
			if (HtmlReporter.tc_result)ROATModules.menu_navigation("MasterPage", "tab_acc_info", "tab_accInfo_closed");
			if (HtmlReporter.tc_result)ROATModules.menu_navigation1("MasterPage", "acc_details", "null");
			if (HtmlReporter.tc_result)ROATModules.addAccInfoMaint();
			if (HtmlReporter.tc_result) {
				sequence = ROATModules.changeAccInfoMaint();
			}
			if (HtmlReporter.tc_result)ROATModules.reverseAccInfoMaint(sequence);

		} catch (Exception e) {
			e.printStackTrace();
		}
	}


	public static void Internal_transfer_Receipt() 
	{

		try{
			if (HtmlReporter.tc_result)ROATFunctions.refesh(false,"Teller");
			if (HtmlReporter.tc_result) {
				int trxnamt = ROATModules.generateBreachAmount("Tranfer_amt_no_breach", 1, "low");
				if (HtmlReporter.tc_result)
					ROATModules.navigateInternalTransfer();
				if (HtmlReporter.tc_result)
					ROATModules.transferPageVerification();
				if (HtmlReporter.tc_result) {
					Double fromnewbal = ROATModules.ent_detail_lcy(Integer.toString(trxnamt));
					if (HtmlReporter.tc_result) {
						if (HtmlReporter.tc_result)
							ROATModules.button("nextbt");
						Thread.sleep(4000);
						if (HtmlReporter.tc_result)
							ROATModules.button("accept");
						if (HtmlReporter.tc_result)ROATModules.breachauthorize();
						Thread.sleep(1000);
						String reference_no=WebDr.getElement("serial_num").getText();
						if (HtmlReporter.tc_result)
							ROATModules.button("post");
						if (HtmlReporter.tc_result)ROATModules.verify_messg("succ_mgs", "msg_success");
						if (HtmlReporter.tc_result)WebDr.Wait_Time(5000);
						try{
							if(WebDr.isExists("Reciept")>0)
							{
								Reporter.ReportEvent("Checking reciept generation", "Internal transfer reciept generated",
										"Internal transfer reciept generated", true);
							}
							else
							{
								Reporter.ReportEvent("Checking reciept generation", "Internal transfer reciept generated",
										"Internal transfer reciept not generated", false);
							}
						}
						catch(Exception e)
						{
							Reporter.ReportEvent("Checking reciept generation", "Internal transfer reciept generated",
									"Internal transfer reciept not generated", false);	
						}
						//					if (HtmlReporter.tc_result)
						//						ROATModules.verifyReceipt("Internal_Transfer","Receipt_fv","Rep_detl");
						//					if (HtmlReporter.tc_result)
						//						ROATModules.verifyReceiptD("Internal_Transfer","Receipt_fv","Rep_detl",reference_no);

						if (HtmlReporter.tc_result)
							ROATModules.button("done");

						if (HtmlReporter.tc_result)
							ROATModules.transferPageVerification();
						if (HtmlReporter.tc_result) {
							fromnewbal = ROATModules.ent_detail_fcy(Integer.toString(trxnamt));
							if (HtmlReporter.tc_result) {
								if (HtmlReporter.tc_result)
									ROATModules.button("nextbt");
								Thread.sleep(4000);
								if (HtmlReporter.tc_result)
									ROATModules.button("accept");
								if (HtmlReporter.tc_result)ROATModules.breachauthorize();
								Thread.sleep(1000);
								reference_no=WebDr.getElement("serial_num").getText();
								if (HtmlReporter.tc_result)
									ROATModules.button("post");
								if (HtmlReporter.tc_result)
									ROATModules.verify_messg("succ_mgs", "msg_success");
								if (HtmlReporter.tc_result)WebDr.Wait_Time(5000);
								//							if (HtmlReporter.tc_result)
								//								ROATModules.verifyReceipt("Internal_Transfer","Receipt_fv","Rep_detf");
								//							if (HtmlReporter.tc_result)
								//								ROATModules.verifyReceiptD("Internal_Transfer","Receipt_fv","Rep_detl",reference_no);
								//							if (HtmlReporter.tc_result)
								try{
									if(WebDr.isExists("Reciept")>0)
									{
										Reporter.ReportEvent("Checking reciept generation", "Internal transfer reciept generated",
												"Internal transfer reciept generated", true);
									}
									else
									{
										Reporter.ReportEvent("Checking reciept generation", "Internal transfer reciept generated",
												"Internal transfer reciept not generated", false);
									}
								}
								catch(Exception e)
								{
									Reporter.ReportEvent("Checking reciept generation", "Internal transfer reciept generated",
											"Internal transfer reciept not generated", false);	
								}
								ROATModules.button("done");
							}
						}
					}
					if (HtmlReporter.tc_result == false) {
						ROATFunctions.refesh(true,Launcher.UserRole);
					}
				}
			}
		} catch (Exception e) {

			e.printStackTrace();
			Reporter.ReportEvent("Test Case execution is Failed", "excution successfull",
					"excution Failed", false);
		}
	}

	public static void Setup_currency_acc_maintainance() {
		try
		{

			ROATFunctions.refesh(false,Launcher.UserRole);
			if (HtmlReporter.tc_result){String branch_name=ROATModules.fetch_user_branch();
			if (HtmlReporter.tc_result)	WebDr.SetPageObjects("Setup_curr_maint");

			if (HtmlReporter.tc_result)ROATModules.Setup_navigation_currency_maint();

			local_gl__number=WebDr.getValue("Local_cur_GL_Branch")+" "+WebDr.getValue("Local_cur_GL_no");
			forex_gl__number=WebDr.getValue("Local_cur_GL_Branch")+" "+WebDr.getValue("Local_cur_GL_no");

			WebDr.SetPageObjects("Setup_curr_maint");
			if (HtmlReporter.tc_result) WebDr.setText("X", "enter_branch_name", branch_name, "Entering branch Name", true);
			if (HtmlReporter.tc_result) WebDr.setText("X", "enter_curency_name", WebDr.getValue("Local_currency"), "Entering currency Name", true);
			if (HtmlReporter.tc_result) WebDr.Wait_Time(1000);

			if (WebDr.isExists("update_icon")==1)
			{
				if (HtmlReporter.tc_result)ROATModules.setup_currency_update_reject(branch_name);
				if (HtmlReporter.tc_result) WebDr.setText("X", "enter_branch_name", branch_name, "Entering branch Name", true);
				if (HtmlReporter.tc_result) WebDr.setText("X", "enter_curency_name", WebDr.getValue("Local_currency"), "Entering currency Name", true);
				if (HtmlReporter.tc_result) WebDr.Wait_Time(1000);
				if (HtmlReporter.tc_result)ROATModules.setup_currency_update_accept(branch_name,"Local_cur_GL_Branch","Local_cur_GL_no");
			}

			else if (WebDr.isExists("empty_table")==1)
			{
				if (HtmlReporter.tc_result)ROATModules.setup_currency_add_new_reject(branch_name);
				if (HtmlReporter.tc_result) WebDr.setText("X", "enter_branch_name", branch_name, "Entering branch Name", true);
				if (HtmlReporter.tc_result) WebDr.setText("X", "enter_curency_name", WebDr.getValue("Local_currency"), "Entering currency Name", true);
				if (HtmlReporter.tc_result) WebDr.Wait_Time(1000);
				if (HtmlReporter.tc_result)ROATModules.setup_currency_add_new_accept(branch_name,true);
			}

			WebDr.SetPageObjects("Setup_curr_maint");
			if (HtmlReporter.tc_result) WebDr.setText("X", "enter_branch_name", branch_name, "Entering branch Name", true);
			if (HtmlReporter.tc_result) WebDr.setText("X", "enter_curency_name", WebDr.getValue("Forex_currency"), "Entering currency Name", true);
			if (HtmlReporter.tc_result) WebDr.Wait_Time(1000);

			if (WebDr.isExists("update_icon")==1)
			{
				if (HtmlReporter.tc_result)ROATModules.duplicate_check(branch_name);
				if (HtmlReporter.tc_result) WebDr.setText("X", "enter_branch_name", branch_name, "Entering branch Name", true);
				if (HtmlReporter.tc_result) WebDr.setText("X", "enter_curency_name", WebDr.getValue("Forex_currency"), "Entering currency Name", true);
				if (HtmlReporter.tc_result) WebDr.Wait_Time(1000);
				if (HtmlReporter.tc_result)ROATModules.setup_currency_update_accept(branch_name,"Forex_cur_GL_Branch","Forex_cur_GL_no");
			}

			if (WebDr.isExists("empty_table")==1)
			{
				if (HtmlReporter.tc_result)ROATModules.setup_currency_add_new_accept(branch_name,false);
			}

			}
		}

		catch(Exception ex)
		{
			ex.printStackTrace();
			Reporter.ReportEvent("Test Case execution is Failed", "Currency account maintainence must be successfully executed",
					"Currency account maintainence executition failed", false);
		}

	}

	//***********************************setup branch**********************************
	public static void Setup_branch() {
		try
		{
			String updated_brch_name="";
			ROATFunctions.refesh(false,"Super User");
			WebDr.SetPageObjects("Setup_branch");

			if (HtmlReporter.tc_result)ROATModules.Setup_navigation_branch();

			//cancel
			if (HtmlReporter.tc_result)ROATModules.Setup_branch_add_cancel();
			//add new
			if (HtmlReporter.tc_result)ROATModules.Setup_branch_add_new();


			//update
			if (HtmlReporter.tc_result) WebDr.setText("X", "enter_branch_number", WebDr.getValue("Branch_number"), "Entering branch No", true);
			if (HtmlReporter.tc_result) WebDr.setText("X", "enter_branch_name", WebDr.getValue("Branch_name"), "Entering branch Name", true);
			if (WebDr.isExists("update_icon")==1)
			{
				if (HtmlReporter.tc_result){
					updated_brch_name=ROATModules.Setup_branch_update();
				}
			}


			//delete
			if (HtmlReporter.tc_result) WebDr.setText("X", "enter_branch_name", updated_brch_name, "Entering branch No", true);
			WebDr.waitTillElementIsDisplayed("delete_icon");
			if (WebDr.isExists("delete_icon")==1)
			{
				if (HtmlReporter.tc_result)ROATModules.Setup_branch_delete();
			}

		}
		catch(Exception ex)
		{
			ex.printStackTrace();
			Reporter.ReportEvent("Checking branch add,delete and update functionality ", "Branch must be added,updated and deleted", "Branch is not added,updated and deleted", true);
		}
	}

	///*******************************cash drawer adjustment********************************

	public static void draweradjustment() {
		try {
			int flag=0;

			ROATFunctions.refesh(false, Launcher.UserRole);
			String branch_no = ROATModules.fetch_user_branch();
			try{
				DatabaseFactory.update_CCP_OCP_Whole_Branch("Y","N",branch_no, CommonUtils.readConfig("Country_code"),Driver.Env_IP);
			}
			catch(Exception e)
			{
				e.printStackTrace();	
			}
			if (HtmlReporter.tc_result)ROATModules.menu_navigation("MasterPage", "tab_CashDrawer", "tab_CashDrawer_closed");
			if (HtmlReporter.tc_result)ROATModules.menu_navigation1("MasterPage", "tab_CashDrawerAdjustment", "null");
			if (HtmlReporter.tc_result)WebDr.SetPageObjects("draweradjustment");
			if (HtmlReporter.tc_result)ROATModules.CashDrawerAdjustment();
			/*if (HtmlReporter.tc_result)ROATModules.Enterdata_CashDrawerAdjustment("cashin",flag);
			if (HtmlReporter.tc_result)ROATModules.Enterdata_CashDrawerAdjustment("cashout",flag);*/

			//cashin/cashout accept functions
			String error = WebDr.getElement("labelErrMsg").getText();
			System.out.println(error);

			if(WebDr.isExists_Display("labelErrMsg") == 0)
			{
				//				//cashin/cashout cancel functions-No
				flag =2;
				if (HtmlReporter.tc_result)	ROATModules.Enterdata_CashDrawerAdjustment("cashin",flag);
				if (HtmlReporter.tc_result)ROATModules.Enterdata_CashDrawerAdjustment("cashout",flag);


				//cashin/cashout cancel functions-YES
				if (HtmlReporter.tc_result) ROATModules.Enterdata_CashDrawerAdjustment("cashin",flag);
				if (HtmlReporter.tc_result)ROATModules.Enterdata_CashDrawerAdjustment("cashout",flag);


				//cashin/cashout reject
				flag =3;
				if (HtmlReporter.tc_result)ROATModules.Enterdata_CashDrawerAdjustment("cashin",flag);
				if (HtmlReporter.tc_result)ROATModules.Enterdata_CashDrawerAdjustment("cashout",flag);

				//cashin/cashout accept functions
				flag=1;
				if (HtmlReporter.tc_result)ROATModules.Enterdata_CashDrawerAdjustment("cashin",flag);
				if (HtmlReporter.tc_result)ROATModules.Enterdata_CashDrawerAdjustment("cashout",flag);

				//35 bytes error not coded as application not working 
				if (HtmlReporter.tc_result)ROATModules.Enterdata_CashDrawerAdjustment("cashin",flag);

			}
			else
			{
				Reporter.ReportEvent(error, error,error, false);

			}

		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	public static void cancellationInterest()
	{
		String seqNo1="", seqNo2="";
		try {
			ROATFunctions.refesh(true, Launcher.UserRole);
			if (HtmlReporter.tc_result)ROATModules.menu_navigation("MasterPage", "cancellations", "cancellations_closed");
			if (HtmlReporter.tc_result)ROATModules.menu_navigation1("MasterPage", "interest", "null");
			if (HtmlReporter.tc_result)
			{
				seqNo1= ROATModules.addCancellationInterest(1);
			}
			if (HtmlReporter.tc_result)ROATModules.reverseCancellationInterest(seqNo1,1);
			if (HtmlReporter.tc_result)
			{
				seqNo2=ROATModules.addCancellationInterest(2);
			}
			if (HtmlReporter.tc_result)ROATModules.reverseCancellationInterest(seqNo2,2);
			if (HtmlReporter.tc_result)ROATModules.reverseCancellationInterest(seqNo2,3);
		}
		catch (Exception e)
		{
			e.printStackTrace();
			Reporter.ReportEvent("Test Case execution is failed", "Test Case execution is failed",
					"Test Case execution is failed", false);
		}
	}

	public static void Reporting() {
		try {
			WebDr.SetPageObjects("MasterPage");
			ROATFunctions.refesh(false, Launcher.UserRole);
			if (HtmlReporter.tc_result)
				ROATModules.navigateReporting();
			if (HtmlReporter.tc_result)
				ROATModules.EarmarkFundsReporting();
			if (HtmlReporter.tc_result)
				ROATModules.BillsNegotiable();
			if (HtmlReporter.tc_result)
				ROATModules.FETReporting();
			if (HtmlReporter.tc_result)
				ROATModules.EarmarksFundMaturity();
			if (HtmlReporter.tc_result)
				ROATModules.IssueandCancelledDrafts();



		}
		catch (Exception e) {
			e.printStackTrace();
			Reporter.ReportEvent("Test Case execution is Failed", "test case excecution Failed","test case excecution Failed", false);
		}

	}

	// cancel_autotransfers
	public static void AutoTransfers_cancel() {
		try {
			if (HtmlReporter.tc_result)ROATFunctions.refesh(true, Launcher.UserRole);
			//navigate 
			if(HtmlReporter.tc_result)ROATModules.NavigateCancellationsAutoTransfers();
			//accept cancellation
			if(HtmlReporter.tc_result)ROATModules.AcceptCancellationsAutoTransfers();
			//reverse
			if(HtmlReporter.tc_result)ROATModules.CancellationReversal();
			//cancel cancellation
			if(HtmlReporter.tc_result)ROATModules.AutoTransferCancelCancellation();
			//cancel auto transfer reversal
			if(HtmlReporter.tc_result)ROATModules.AutoTransferReversalCancelCancellation();

		} catch (Exception e) {
			e.printStackTrace();
			Reporter.ReportEvent("Test Case execution is Failed", "test case excecution Failed","test case excecution Failed", false);
		}

	}

	//cancellation fees
	public static void UTCancellationFees() {
		try 
		{	
			String[] opt= {"Commitment","Ledger","Penalty"};
			ROATFunctions.refesh(false, Launcher.UserRole);
			if (HtmlReporter.tc_result)ROATModules.CancellationFeesNavigation();
			for(int i=0;i<3;i++)
			{
				if (HtmlReporter.tc_result)ROATModules.call_func(opt[i]);

			}
		}
		catch (Exception e) 
		{
			Reporter.ReportEvent("testcase failed ", "testcase failed", "testcase failed", true);
			e.printStackTrace();
		}
	}

	public static void ReportingParameters() {
		try {

			ROATFunctions.refesh(false, Launcher.UserRole);

			WebDr.SetPageObjects("MasterPage");
			//global
			if (HtmlReporter.tc_result)ROATModules.navigateReportingParameters();
			if (HtmlReporter.tc_result)ROATModules.ReportingParametersValidations();
			WebDr.SetPageObjects("MasterPage");
			//branch
			if (HtmlReporter.tc_result)WebDr.click("X", "Branch", "Clicking on  Branch", true);
			if (HtmlReporter.tc_result)ROATModules.ReportingParametersValidations1();

		}
		catch (Exception e) {
			e.printStackTrace();
			Reporter.ReportEvent("Test Case execution is Failed", "test case excecution Failed","test case excecution Failed", false);
		}


	}

	public static void Internal_Clearing_Reports() {
		try {
			// String updated_brch_name="";
			ROATFunctions.refesh(true, Launcher.UserRole);

			if (HtmlReporter.tc_result)ROATModules.Setup_nav_internal_clearing_reports();	
			if (HtmlReporter.tc_result)WebDr.SetPageObjects("Internal_Clearing_Reports");
			if (HtmlReporter.tc_result)ROATModules.Setup_add_info();

		}

		catch(Exception ex)
		{
			ex.printStackTrace();
			Reporter.ReportEvent("Internal Clearing Reports functionality ", "Internal Clearing Reports present", "Internal Clearing Reports not present", true);
		}
	}
	public static void External_Clearing_Reports() {
		try {
			// String updated_brch_name="";
			ROATFunctions.refesh(false, Launcher.UserRole);

			if (HtmlReporter.tc_result)ROATModules.Setup_nav_external_clearing_reports();
			if (HtmlReporter.tc_result)WebDr.SetPageObjects("External_Clearing_Reports");
			if (HtmlReporter.tc_result)ROATModules.Setup_add_info_ext_clearing(true);
			if (HtmlReporter.tc_result)ROATModules.Setup_add_info_ext_clearing(false);

		}

		catch(Exception ex)
		{
			ex.printStackTrace();
			Reporter.ReportEvent("External Clearing Reports functionality ", "Reports present", "Reports not present", true);
		}	

	}

	public static void UT_FixedDeposit_Add_Change()
	{
		try 
		{
			ROATFunctions.refesh(true, Launcher.UserRole);
			WebDr.SetPageObjects("MasterPage");

			//navigate 
			if(HtmlReporter.tc_result)ROATModules.Deposit_Placing_Navigate();
			//terminal number validation 
			if(HtmlReporter.tc_result)ROATModules.terminal_number_validation();
			//cancel flowee
			if(HtmlReporter.tc_result)ROATModules.Deposit_Cancel();
			//accept flow
			if(HtmlReporter.tc_result)ROATModules.Deposit_Accept();
			if(HtmlReporter.tc_result)ROATModules.Deposit_Placing_Navigate();

			if (HtmlReporter.tc_result) ROATModules.change_terminal_number_validation();
			//change cancel
			if(HtmlReporter.tc_result)ROATModules.Change_Deposit_Cancel();

			if(HtmlReporter.tc_result)ROATModules.Deposit_Placing_Navigate();
			//terminal number validation 
			//			if(HtmlReporter.tc_result)ROATModules.change_terminal_number_validation();
			//change accept
			if(HtmlReporter.tc_result)ROATModules.Change_Deposit_Accept();

			if(HtmlReporter.tc_result)ROATModules.Deposit_Placing_Navigate();
			//terminal number validation 
			if(HtmlReporter.tc_result)ROATModules.rollover_terminal_number_validation();
			//rollover cancel
			if(HtmlReporter.tc_result)ROATModules.Rollover_Deposit_Cancel();

			if(HtmlReporter.tc_result)ROATModules.Deposit_Placing_Navigate();
			//terminal number validation 
			if(HtmlReporter.tc_result)ROATModules.rollover_terminal_number_validation();
			//rollover accept
			if(HtmlReporter.tc_result)ROATModules.Rollover_Deposit_Accept();

			if(Driver.Country_code.equalsIgnoreCase("BWA"))
			{
				if(HtmlReporter.tc_result)ROATModules.Deposit_Placing_Navigate();
				//placing validation
				if(HtmlReporter.tc_result)ROATModules.placing_terminal_number_valiation();
				//placing cancel
				if(HtmlReporter.tc_result)ROATModules.Add_Placing_Cancel();
				//placing accept
				if(HtmlReporter.tc_result)ROATModules.Add_Placing_Accept();
			}

		}
		catch (Exception e)
		{
			e.printStackTrace();
			Reporter.ReportEvent("Test Case execution is Failed", "Auto transfer excution successfull",
					"Auto transfer excution Failed", false);
		}

	}

	public static void ForexExchangeTransfer() {

		try {

			ROATFunctions.refesh(false, Launcher.UserRole);
			WebDr.SetPageObjects("MasterPage");

			if (HtmlReporter.tc_result)ROATModules.navigateExchangeTransfer();
			if (HtmlReporter.tc_result)ROATModules.ExchangeTransfer();
			//cancel
			if (HtmlReporter.tc_result)WebDr.clickX("X", "btnCancel", "Clicking on  btnCancel", true);

			ROATFunctions.refesh(true, Launcher.UserRole);
			WebDr.SetPageObjects("MasterPage");
			if (HtmlReporter.tc_result)ROATModules.navigateExchangeTransfer();
			if (HtmlReporter.tc_result)ROATModules.ExchangeTransfer();
			//submit
			if (HtmlReporter.tc_result)WebDr.clickX("X", "btnSubmit", "Clicking on  btnSubmit", true);

			WebDr.Wait_Time(5020);
			if(WebDr.isExists_Display("insufficient_fund_popup")>0)
			{
				if (HtmlReporter.tc_result)WebDr.click("X", "insufficient_fund_popup", "Click on insufficient fund pop up", true);
			}
			//authorisation
			String auth[] = Launcher.Auth_User_Login("Super User");
			String Login[] = Launcher.User_Login("Super User");
			ROATModules.authorize_Credential_Vault("ForexExchangeTransfer", "username", "password", "username2", "password2", auth[0],  auth[1], Login[0], Login[1],true);

			//cancel
			if (HtmlReporter.tc_result)WebDr.clickX("X", "btnCancel", "Clicking on  btnCancel", true);


			ROATFunctions.refesh(true, Launcher.UserRole);
			WebDr.SetPageObjects("MasterPage");
			if (HtmlReporter.tc_result)ROATModules.navigateExchangeTransfer();
			if (HtmlReporter.tc_result)ROATModules.ExchangeTransfer();
			//submit
			if (HtmlReporter.tc_result)WebDr.clickX("X", "btnSubmit", "Clicking on  btnSubmit", true);
			WebDr.Wait_Time(5020);
			if(WebDr.isExists_Display("insufficient_fund_popup")>0)
			{
				if (HtmlReporter.tc_result)WebDr.click("X", "insufficient_fund_popup", "Click on insufficient fund pop up", true);
			}
			String auth1[] = Launcher.Auth_User_Login("Super User");
			String Login1[] = Launcher.User_Login("Super User");
			ROATModules.authorize_Credential_Vault("ForexExchangeTransfer", "username", "password", "username2", "password2", auth1[0],  auth1[1], Login1[0], Login1[1],true);
			if (HtmlReporter.tc_result)WebDr.waitTillElementIsDisplayed("Accept");
			//Accept
			if (HtmlReporter.tc_result)WebDr.clickX("X", "Accept", "Clicking on  Accept", true);
			//post
			if (HtmlReporter.tc_result)WebDr.waitTillElementIsDisplayed("post");
			if (HtmlReporter.tc_result)WebDr.clickX("X", "post", "Clicking on  post", true);
			if (HtmlReporter.tc_result)WebDr.waitTillElementIsDisplayed("successmsg");
			if (HtmlReporter.tc_result)WebDr.verifyText("X", "successmsg", true, WebDr.getValue("successmsg"), "reciept generation ", true);

			//			ROATModules.ExchangeTransfer_success();

		}
		catch (Exception e) 
		{
			e.printStackTrace();
			Reporter.ReportEvent("Test Case execution is Failed", "test case excecution Failed","test case excecution Failed", false);
		}

	}


	public static void ForexBillsNegotiableAdd() 
	{
		try 
		{

			ROATFunctions.refesh(false, Launcher.UserRole);

			WebDr.SetPageObjects("MasterPage");
			if (HtmlReporter.tc_result)
				ROATModules.navigateBillsNegotiableAdd();
			if (HtmlReporter.tc_result)
				ROATModules.BillsNegotiableAdd();
			//cancel
			if (HtmlReporter.tc_result)WebDr.clickX("X", "cancel", "Clicking on cancel", true);
			if (HtmlReporter.tc_result)WebDr.click("X", "button_yes", "Clicking on cancel yes", true);
			//success
			if (HtmlReporter.tc_result)
				ROATModules.BillsNegotiableAdd();
			if (HtmlReporter.tc_result)WebDr.clickX("X", "Next", "Clicking on Next", true);
			String auth1[] = Launcher.Auth_User_Login("Super User");
			String Login1[] = Launcher.User_Login("Super User");
			ROATModules.authorize_Credential_Vault("forexbillsnegotiableadd", "username", "password", "username2", "password2", auth1[0],  auth1[1], Login1[0], Login1[1],true);
			if (HtmlReporter.tc_result)WebDr.clickX("X", "btnAccept", "Clicking on btnAccept", true);
			if (HtmlReporter.tc_result)WebDr.clickX("X", "post", "Clicking on post", true);
			WebDr.waitTillElementIsDisplayed("sucess_msg");
			if (HtmlReporter.tc_result)WebDr.verifyText("X", "sucess_msg", true, WebDr.getValue("success_messg_auth"), "verifying recipt genration and sucss message", true);
			if (HtmlReporter.tc_result)WebDr.clickX("X", "done", "Clicking on done", true);
			//reject
			if (HtmlReporter.tc_result)ROATModules.BillsNegotiableAdd();
			if (HtmlReporter.tc_result)WebDr.clickX("X", "Next", "Clicking on Next", true);
			String auth[] = Launcher.Auth_User_Login("Super User");
			String Login[] = Launcher.User_Login("Super User");
			ROATModules.authorize_Credential_Vault("forexbillsnegotiableadd", "username", "password", "username2", "password2", auth[0],  auth[1], Login[0], Login[1],false);
			WebDr.waitTillElementIsDisplayed("rej_msg");
			if (HtmlReporter.tc_result)WebDr.verifyText("X", "rej_msg", true, WebDr.getValue("reject_msg_auth"), "verifying reject  message", true);




		}
		catch (Exception e) {
			e.printStackTrace();
			Reporter.ReportEvent("Test Case execution is Failed", "test case excecution Failed","test case excecution Failed", false);
		}

	}
	public static void TellerChargesreceipt() {
		try {
			ROATFunctions.refesh(true, Launcher.UserRole);
			if (HtmlReporter.tc_result)ROATModules.navigate_TellerCharge();
			if (HtmlReporter.tc_result)ROATModules.EnterInfo_TellerCharge();
			if (HtmlReporter.tc_result)ROATModules.CheckReceipt_TellerCharge();

		} catch (Exception e) {
			Reporter.ReportEvent("Test Case execution is failed", "Test Case execution is failed",
					"Test Case execution is failed", false);
		}

	}

	public static void Teller_Cash_DepositEdit_Receipt()  {
		try {
			ROATFunctions.refesh(false, Launcher.UserRole);
			if (HtmlReporter.tc_result) {
				String user_branch = ROATModules.fetch_user_branch();

				ROATFunctions.refesh(false, "Super User");
				if (HtmlReporter.tc_result)
					ROATModules.AUTHRem_navigateRemAuthorization();
				if (HtmlReporter.tc_result)
					ROATModules.updateRemoteAuthorization(user_branch);
				ROATFunctions.refesh(false,"Teller");
				if (HtmlReporter.tc_result)
					ROATModules.Teller_navigateCashDeposit();
				if (HtmlReporter.tc_result)
					ROATModules.Teller_AuthorizeCashDeposit();
				if (HtmlReporter.tc_result)
					ROATModules.verifyReceipt("CashDepositPage","Receipt_fv","Rep_detl");
				//								if (HtmlReporter.tc_result)
				//									ROATModules.verifyReceiptD("Internal_Transfer","Receipt_fv","Rep_detl",reference_no);

			}
		} catch (Exception e) {
			Reporter.ReportEvent("Test Case execution is failed", "Test Case execution is failed",
					"Test Case execution is failed", false);
		}

	}

	public static void Forex_Exchange_ChequeDeposit() {
		try {
			ROATFunctions.refesh(false, Launcher.UserRole);
			//WebDr.SetPageObjects("MasterPage");

			//navigate 
			if(HtmlReporter.tc_result)ROATModules.Forex_Navigation();
			//continue
			if(HtmlReporter.tc_result)ROATModules.Forex_Cheque_deposit_continue();
			//cancel
			if(HtmlReporter.tc_result)ROATModules.Forex_Cheque_deposit_cancel();
			//accept
			if(HtmlReporter.tc_result)ROATModules.Forex_Cheque_deposit_accept();
		} catch (Exception e) {

			e.printStackTrace();
			Reporter.ReportEvent("Test Case execution is Failed", "Auto transfer excution successfull",
					"Auto transfer excution Failed", false);
		}

	}

	public static void UT_Audit_Trails() {
		try {
			ROATFunctions.refesh(false, Launcher.UserRole);
			if(HtmlReporter.tc_result)ROATModules.NavigateAuditTrailReports();
			if(HtmlReporter.tc_result)ROATModules.AuditTrailsMessage();
		}
		catch (Exception e) {
			e.printStackTrace();
			Reporter.ReportEvent("Test Case execution is Failed", "test case excecution Failed","test case excecution Failed", false);
		}
	}

	public static void SupportFunctions()
	{
		try
		{
			if (HtmlReporter.tc_result) ROATFunctions.refesh(false, Launcher.UserRole);
			//Navigate 
			if (HtmlReporter.tc_result) ROATModules.NavigateSupportFunctions();
			//Cancel
			if (HtmlReporter.tc_result) ROATModules.CancelSupportFunctions();
			//Min Max Transaction No
			//if (HtmlReporter.tc_result) ROATModules.MinMaxTransactionNoCheck();
			//Performing transaction - deposit
			if (HtmlReporter.tc_result) ROATModules.Teller_navigateCashDeposit();
			if (HtmlReporter.tc_result) ROATModules.Teller_AuthorizeCashDeposit();
			//Navigate 
			if (HtmlReporter.tc_result) ROATModules.NavigateSupportFunctions();
			//Delete  - 110-000004
			if (HtmlReporter.tc_result) ROATModules.DeleteTransactionNoCheck();
			//Verify
			if (HtmlReporter.tc_result) ROATModules.VerifyTransactionNoCheck();
		}
		catch(Exception ex)
		{
			ex.printStackTrace();
		}
	}
	////*************************charge package search ******************************************************************
	public static void Charge_Package_Search() 
	{
		try
		{
			//String updated_brch_name="";
			ROATFunctions.refesh(true,"Teller");
			WebDr.SetPageObjects("Charge_Package_Search");
			if (HtmlReporter.tc_result)ROATModules.Charge_Package_Search_Navigation();
			WebDr.SetPageObjects("Charge_Package_Search");
			if (HtmlReporter.tc_result)ROATModules.Charge_package();

		}
		catch(Exception ex)
		{
			ex.printStackTrace();
			Reporter.ReportEvent("Checking branch add,delete and update functionality ", "Branch must be added,updated and deleted", "Branch is not added,updated and deleted", true);
		}

	}
	//******************************Journal printing*******************************************************

	public static void Journal_Printing() {
		try
		{
			//String updated_brch_name="";
			ROATFunctions.refesh(false,Launcher.UserRole);

			if (HtmlReporter.tc_result)ROATModules.Journal_Printing_Navigation();
			WebDr.SetPageObjects("Journal_Printing");
			if (HtmlReporter.tc_result)ROATModules.Journal_Printing_Download(true);
			if (HtmlReporter.tc_result)ROATModules.Journal_Printing_Download(false);


		}
		catch(Exception ex)
		{
			ex.printStackTrace();
			Reporter.ReportEvent("Checking Journal Printing functionality ", "Journal Printing Successfull", "Journal Printing Unsuccessful", true);
		}
	}
	//**************** charge packages reports******************************************
	public static void Charge_Package_Reports() {

		try
		{
			//String updated_brch_name="";
			ROATFunctions.refesh(false,Launcher.UserRole);
			if (HtmlReporter.tc_result)ROATModules.Charge_Package_Reports_Navigation();
			WebDr.SetPageObjects("Charge_Package_Reports");
			if (HtmlReporter.tc_result)ROATModules.Charge_Package_Reports();

		}
		catch(Exception ex)
		{
			ex.printStackTrace();
			Reporter.ReportEvent("Checking charge package reports functionality ", "charge package reports functionality successful", "charge package reports functionality unsuccessful", true);
		}

	}
	//****************User List ******************************************
	public static void User_List() 
	{

		try
		{
			//String updated_brch_name="";
			ROATFunctions.refesh(false,Launcher.UserRole);

			if (HtmlReporter.tc_result)ROATModules.User_List_Navigation();
			WebDr.SetPageObjects("User_List");
			if (HtmlReporter.tc_result)ROATModules.User_List_Branch();

			ROATFunctions.refesh(false,"Supervisor");
			if (HtmlReporter.tc_result)ROATModules.User_List_Navigation();
			WebDr.SetPageObjects("User_List");
			if (HtmlReporter.tc_result)ROATModules.User_List_Branchcheck();

		}
		catch(Exception ex)
		{
			ex.printStackTrace();
			Reporter.ReportEvent("Checking User List functionality ", "User List Successful", "User List Unsuccessful", true);
		}
	}
	// *******************************transaction list**********************************************
	public static void Transaction_List_Reports() {
		try{
			ROATFunctions.refesh(true,Launcher.UserRole);

			if (HtmlReporter.tc_result)ROATModules.Transaction_List_Navigation();
			WebDr.SetPageObjects("Transaction_List");
			if (HtmlReporter.tc_result)ROATModules.Transaction_List_Branch();
		}
		catch(Exception ex)
		{
			ex.printStackTrace();
			Reporter.ReportEvent("Checking user transaction list navigation ", "Should navigate to transaction list", "Navigation to transaction list unsuccesful", true);
		}
	}

	//*******************************Reporting parameters**********************************************
	public static void Reporting_Parameters() {
		try{
			ROATFunctions.refesh(false,Launcher.UserRole);

			if (HtmlReporter.tc_result)ROATModules.Reporting_Parameters_Navigation();
			WebDr.SetPageObjects("Reporting_Parameters");
			if (HtmlReporter.tc_result)ROATModules.Reporting_Parameters_Branch(true);
			if (HtmlReporter.tc_result)ROATModules.Reporting_Parameters_Branch(false);

		}
		catch(Exception ex)
		{
			ex.printStackTrace();
			Reporter.ReportEvent("Checking Reporting Parameters navigation ", "Should navigate to Reporting Parameters", "Navigation to Reporting Parameters unsuccesful", true);
		}
	}
	//************************************force log off***************************************
	public static void Force_Logoff() {
		try
		{
			//String updated_brch_name="";
			ROATFunctions.refesh(false,Launcher.UserRole);

			if (HtmlReporter.tc_result)ROATModules.Force_Logoff_Navigation();
			WebDr.SetPageObjects("Force_Logoff");
			if (HtmlReporter.tc_result)ROATModules.Force_Logoff_Branch();

			ROATFunctions.refesh(false,"Supervisor");
			if (HtmlReporter.tc_result)ROATModules.Force_Logoff_Navigation();
			WebDr.SetPageObjects("Force_Logoff");
			if (HtmlReporter.tc_result)ROATModules.Force_Logoff_Branchcheck();

		}
		catch(Exception ex)
		{
			ex.printStackTrace();
			Reporter.ReportEvent("Checking User List functionality ", "User List Successful", "User List Unsuccessful", true);
		}

	}
	//************************************cheque withdrawal******************************
	public static void CashWithdrawal_Encashment() {
		try{
			ROATFunctions.refesh(false,Launcher.UserRole);

			if (HtmlReporter.tc_result)ROATModules.CashWithdrawal_Encashment_Navigation();
			WebDr.SetPageObjects("ChequeWithdrawal_Encashment");

			if(!(Driver.Country_code.equals("SYC") ||Driver.Country_code.equals( "MUO" )||Driver.Country_code.equals("MUS")))
			{
				if (HtmlReporter.tc_result)ROATModules.CashWithdrawal_Encashment_Branch();
			}
			else
			{
				if (HtmlReporter.tc_result)ROATModules.CashWithdrawal_Encashment_Noscan();
			}

		}
		catch(Exception ex)
		{
			ex.printStackTrace();
			Reporter.ReportEvent("Checking Cash Withdrawal/Encashment navigation ", "Should navigate to Cash Withdrawal/Encashment", "Navigation to Cash Withdrawal/Encashment unsuccesful", true);
		}

	}

	//************************************forex sell**********************************
	public static void Forex_Exchange_Sell() 
	{

		try
		{
			if (HtmlReporter.tc_result)ROATFunctions.refesh(true,"Teller");
			//cancel function
			if (HtmlReporter.tc_result)ROATModules.Navigate_Exchange_Sell();
			ROATFunctions.clickTab();
			if(WebDr.isExists_Display("err_FieldError")>0)
			{
				WebDr.setText("X", "txtAccNum", WebDr.getValue("txtAccNum"), "amount", true);
				ROATFunctions.clickTab();
			}
			if (HtmlReporter.tc_result) 
			{
				if(WebDr.isExists_Display("IDV") > 0)
				{
					if (HtmlReporter.tc_result)WebDr.click("X", "IDV", "Click idv radio btn", true);
					if (HtmlReporter.tc_result)WebDr.click("X", "Okclick", "Click ok btn", true);
				}
			}
			WebDr.setText("X", "txtAccNum", WebDr.getValue("txtAccNum"), "amount", true);
			ROATFunctions.clickTab();
			if (HtmlReporter.tc_result==true) WebDr.clickX("X", "cancel", "Clicking on cancel  field", true);
			if (HtmlReporter.tc_result==true)ROATFunctions.scrolltotop();





			//reject
			if (HtmlReporter.tc_result)ROATModules.Navigate_Exchange_Sell();
			//			if (HtmlReporter.tc_result)WebDr.setText("X", "txtAccNum", WebDr.getValue("txtAccNum"), "amount", true);
			ROATFunctions.clickTab();
			if(WebDr.isExists_Display("err_FieldError")>0)
			{
				WebDr.setText("X", "txtAccNum", WebDr.getValue("txtAccNum"), "amount", true);
				ROATFunctions.clickTab();
			}
			if (HtmlReporter.tc_result) 
			{
				if(WebDr.isExists_Display("IDV") > 0)
				{
					if (HtmlReporter.tc_result)WebDr.click("X", "IDV", "Click idv radio btn", true);
					if (HtmlReporter.tc_result)WebDr.click("X", "Okclick", "Click ok btn", true);
				}
			}
			WebDr.setText("X", "txtAccNum", WebDr.getValue("txtAccNum"), "amount", true);
			ROATFunctions.clickTab();
			if (HtmlReporter.tc_result==true) WebDr.clickX("X", "accept", "Clicking on accept  field", true);
			if (HtmlReporter.tc_result)ROATModules.authorisation();
			if (HtmlReporter.tc_result==true) WebDr.clickX("X", "btnReject", "Clicking on btnReject  field", true);
			if (HtmlReporter.tc_result)WebDr.setText("X", "reason", WebDr.getValue("reason"), "Enteringreject reason", true);
			if (HtmlReporter.tc_result==true) WebDr.clickX("X", "btnSubmit2", "Clicking on btnSubmit2  field", true);




			//btnClear
			if (HtmlReporter.tc_result==true)ROATFunctions.scrolltotop();
			if (HtmlReporter.tc_result)ROATModules.Navigate_Exchange_Sell();
			//			if (HtmlReporter.tc_result)	WebDr.setText("X", "txtAccNum", WebDr.getValue("txtAccNum"), "amount", true);
			if (HtmlReporter.tc_result)ROATFunctions.clickTab();
			if(WebDr.isExists_Display("err_FieldError")>0)
			{
				WebDr.setText("X", "txtAccNum", WebDr.getValue("txtAccNum"), "amount", true);
				ROATFunctions.clickTab();
			}
			if (HtmlReporter.tc_result) 
			{
				if(WebDr.isExists_Display("IDV") > 0)
				{
					if (HtmlReporter.tc_result)WebDr.click("X", "IDV", "Click idv radio btn", true);

					if (HtmlReporter.tc_result)WebDr.click("X", "Okclick", "Click ok btn", true);
				}
			}
			WebDr.setText("X", "txtAccNum", WebDr.getValue("txtAccNum"), "amount", true);
			ROATFunctions.clickTab();
			if (HtmlReporter.tc_result==true) WebDr.clickX("X", "accept", "Clicking on accept  field", true);
			if (HtmlReporter.tc_result)ROATModules.authorisation();
			if (HtmlReporter.tc_result==true)WebDr.clickX("X", "btnAuthorize", "Clicking on Autorize button", true);
			if (HtmlReporter.tc_result==true)WebDr.clickX("X", "btnClear", "Clicking on btnClear  field", true);
			if (HtmlReporter.tc_result==true)ROATFunctions.scrolltotop();

			if (HtmlReporter.tc_result)ROATFunctions.refesh(true,"Teller");
			//btnCancel
			if (HtmlReporter.tc_result)ROATModules.Navigate_Exchange_Sell();
			//			if (HtmlReporter.tc_result)WebDr.setText("X", "txtAccNum", WebDr.getValue("txtAccNum"), "amount", true);
			if (HtmlReporter.tc_result)ROATFunctions.clickTab();
			if(WebDr.isExists_Display("err_FieldError")>0)
			{
				WebDr.setText("X", "txtAccNum", WebDr.getValue("txtAccNum"), "amount", true);
				ROATFunctions.clickTab();
			}
			if (HtmlReporter.tc_result) 
			{
				if(WebDr.isExists_Display("IDV") > 0)
				{
					if (HtmlReporter.tc_result)WebDr.click("X", "IDV", "Click idv radio btn", true);
					if (HtmlReporter.tc_result)WebDr.click("X", "Okclick", "Click ok btn", true);
				}
			}
			WebDr.setText("X", "txtAccNum", WebDr.getValue("txtAccNum"), "amount", true);
			ROATFunctions.clickTab();
			if (HtmlReporter.tc_result==true) WebDr.clickX("X", "accept", "Clicking on accept  field", true);
			if (HtmlReporter.tc_result)ROATModules.authorisation();
			if (HtmlReporter.tc_result==true)WebDr.clickX("X", "btnAuthorize", "Clicking on Autorize button", true);
			if (HtmlReporter.tc_result==true) WebDr.clickX("X", "btnCancel", "Clicking on btnClear  field", true);
			if (HtmlReporter.tc_result==true)ROATFunctions.scrolltotop();



			//btncancel2
			if (HtmlReporter.tc_result)ROATModules.Navigate_Exchange_Sell();
			//			WebDr.setText("X", "txtAccNum", WebDr.getValue("txtAccNum"), "amount", true);
			if (HtmlReporter.tc_result)ROATFunctions.clickTab();
			if(WebDr.isExists_Display("err_FieldError")>0)
			{
				WebDr.setText("X", "txtAccNum", WebDr.getValue("txtAccNum"), "amount", true);
				ROATFunctions.clickTab();
			}
			if (HtmlReporter.tc_result) 
			{
				if(WebDr.isExists_Display("IDV") > 0)
				{
					if (HtmlReporter.tc_result)WebDr.click("X", "IDV", "Click idv radio btn", true);
					if (HtmlReporter.tc_result)WebDr.click("X", "Okclick", "Click ok btn", true);
				}
			}
			WebDr.setText("X", "txtAccNum", WebDr.getValue("txtAccNum"), "amount", true);
			ROATFunctions.clickTab();
			if (HtmlReporter.tc_result==true) WebDr.clickX("X", "accept", "Clicking on accept  field", true);
			if (HtmlReporter.tc_result)ROATModules.authorisation();
			if (HtmlReporter.tc_result==true)WebDr.clickX("X", "btnAuthorize", "Clicking on Autorize button", true);
			if (HtmlReporter.tc_result==true) WebDr.clickX("X", "btnAccept", "Clicking on btnAccept  field", true);
			if (HtmlReporter.tc_result)ROATModules.duefromcustomer("cashFCYtoLCY");
			if (HtmlReporter.tc_result==true) WebDr.clickX("X", "btnCancel", "Clicking on btnCancel  field", true);
			if (HtmlReporter.tc_result==true)ROATFunctions.scrolltotop();




			//cash to cash function( FCY -> LCY)
			if (HtmlReporter.tc_result)ROATModules.Navigate_Exchange_Sell();
			if (HtmlReporter.tc_result)ROATFunctions.clickTab();
			WebDr.Wait_Time(1000);
			if(WebDr.isExists_Display("err_FieldError")>0)
			{
				WebDr.setText("X", "txtAccNum", WebDr.getValue("txtAccNum"), "amount", true);
				ROATFunctions.clickTab();
			}
			if (HtmlReporter.tc_result) 
			{
				if(WebDr.isExists_Display("IDV") > 0)
				{
					if (HtmlReporter.tc_result)WebDr.click("X", "IDV", "Click idv radio btn", true);
					if (HtmlReporter.tc_result)WebDr.click("X", "Okclick", "Click ok btn", true);
				}
			}
			WebDr.setText("X", "txtAccNum", WebDr.getValue("txtAccNum"), "amount", true);
			ROATFunctions.clickTab();
			if (HtmlReporter.tc_result==true) WebDr.clickX("X", "accept", "Clicking on accept  field", true);
			if (HtmlReporter.tc_result)ROATModules.authorisation();
			if (HtmlReporter.tc_result==true)WebDr.clickX("X", "btnAuthorize", "Clicking on Autorize button", true);
			if (HtmlReporter.tc_result==true) WebDr.clickX("X", "btnAccept", "Clicking on btnAccept  field", true);
			if (HtmlReporter.tc_result)ROATModules.duefromcustomer("cashFCYtoLCY");
			if (HtmlReporter.tc_result==true) WebDr.clickX("X", "btnNext", "Clicking on btnNext  field", true);
			if (HtmlReporter.tc_result==true) WebDr.clickX("X", "btnCancel", "Clicking on btnCancel  field", true);

			if (HtmlReporter.tc_result==true)ROATFunctions.scrolltotop();






			//cash to account function(FCY ->LCY)
			if (HtmlReporter.tc_result)ROATFunctions.refesh(true,"Teller");
			if (HtmlReporter.tc_result)ROATModules.Navigate_Exchange_Sell();
			//			if (HtmlReporter.tc_result)WebDr.setText("X", "txtAccNum", WebDr.getValue("txtAccNum"), "amount", true);
			if (HtmlReporter.tc_result)ROATFunctions.clickTab();
			if(WebDr.isExists_Display("err_FieldError")>0)
			{
				WebDr.setText("X", "txtAccNum", WebDr.getValue("txtAccNum"), "amount", true);
				ROATFunctions.clickTab();
			}
			if (HtmlReporter.tc_result) 
			{
				if(WebDr.isExists_Display("IDV") > 0)
				{
					if (HtmlReporter.tc_result)WebDr.click("X", "IDV", "Click idv radio btn", true);
					if (HtmlReporter.tc_result)WebDr.click("X", "Okclick", "Click ok btn", true);
				}
			}
			WebDr.setText("X", "txtAccNum", WebDr.getValue("txtAccNum"), "amount", true);
			ROATFunctions.clickTab();
			if (HtmlReporter.tc_result)ROATModules.clickaccountmethod("Pay");
			if (HtmlReporter.tc_result==true) WebDr.clickX("X", "accept", "Clicking on accept  field", true);
			for(int k=0;k<2;k++)
			{
				if(WebDr.isExists_Display("insufficient_fund_popup")>0)
				{
					if (HtmlReporter.tc_result)WebDr.click("X", "insufficient_fund_popup", "Click on insufficient_fund_popup", true);
					break;
				}
			}
			if (HtmlReporter.tc_result)ROATModules.authorisation();
			if (HtmlReporter.tc_result==true)WebDr.clickX("X", "btnAuthorize", "Clicking on Autorize button", true);
			if (HtmlReporter.tc_result==true) WebDr.clickX("X", "btnAccept", "Clicking on btnAccept  field", true);
			//			if (HtmlReporter.tc_result)ROATModules.duefromcustomer("cashFCYtoLCY");
			if (HtmlReporter.tc_result==true) WebDr.clickX("X", "btnNext", "Clicking on btnNext  field", true);
			if (HtmlReporter.tc_result==true) WebDr.clickX("X", "btnCancel", "Clicking on accept  field", true);
			if (HtmlReporter.tc_result==true)ROATFunctions.scrolltotop();








			//account to cash function (FCY ->LCY)
			if (HtmlReporter.tc_result)ROATModules.Navigate_Exchange_Sell();
			if (HtmlReporter.tc_result)ROATFunctions.cash_drawer_DD1("BranchDD", "BranchIN", "BranchOP", WebDr.getValue("Branch_fcy"));
			if (HtmlReporter.tc_result)WebDr.clickX("X", "BranchOP", "Click on branch", true);
			if (HtmlReporter.tc_result)ROATFunctions.clickTab();

			if(WebDr.isExists_Display("err_FieldError")>0)
			{
				WebDr.setText("X", "acc_no", WebDr.getValue("Account_fcy"), "amount", true);
				ROATFunctions.clickTab();
			}
			if (HtmlReporter.tc_result) 
			{
				if(WebDr.isExists_Display("IDV") > 0)
				{
					if (HtmlReporter.tc_result)WebDr.click("X", "IDV", "Click idv radio btn", true);
					if (HtmlReporter.tc_result)WebDr.click("X", "Okclick", "Click ok btn", true);
				}
			}
			WebDr.setText("X", "acc_no", WebDr.getValue("Account_fcy"), "amount", true);
			ROATFunctions.clickTab();
			if (HtmlReporter.tc_result)ROATModules.clickaccountmethod("Sell");

			if (HtmlReporter.tc_result==true) WebDr.clickX("X", "accept", "Clicking on accept  field", true);
			for(int k=0;k<2;k++)
			{
				if(WebDr.isExists_Display("insufficient_fund_popup")>0)
				{
					if (HtmlReporter.tc_result)WebDr.click("X", "insufficient_fund_popup", "Click on insufficient_fund_popup", true);
					break;
				}
			}
			if (HtmlReporter.tc_result)ROATModules.authorisation();
			if (HtmlReporter.tc_result==true)WebDr.clickX("X", "btnAuthorize", "Clicking on Autorize button", true);
			if (HtmlReporter.tc_result==true) WebDr.clickX("X", "btnAccept", "Clicking on btnAccept  field", true);
			if (HtmlReporter.tc_result)ROATModules.duefromcustomer("cashFCYtoLCY");
			if (HtmlReporter.tc_result==true) WebDr.clickX("X", "btnNext", "Clicking on btnNext  field", true);
			if (HtmlReporter.tc_result==true) WebDr.clickX("X", "btnCancel", "Clicking on btnCancel  field", true);
			if (HtmlReporter.tc_result==true)ROATFunctions.scrolltotop();







			System.out.println("DOne UI");

		}
		catch(Exception e)
		{
			Reporter.ReportEvent("Test Case execution is failed", "Test Case execution is failed","Test Case execution is failed", false);
		}


	}

	public static void Sanity_SupportFunctions() {
		try
		{
			if (HtmlReporter.tc_result) ROATFunctions.refesh(false, Launcher.UserRole);
			//Navigate 
			if (HtmlReporter.tc_result) ROATModules.NavigateSupportFunctions();
			//Cancel
			if (HtmlReporter.tc_result) ROATModules.CancelSupportFunctions();
			//Min Max Transaction No
			//if (HtmlReporter.tc_result) ROATModules.MinMaxTransactionNoCheck();
			//Performing transaction - deposit
			if (HtmlReporter.tc_result) ROATModules.Teller_navigateCashDeposit();
			if (HtmlReporter.tc_result) ROATModules.Teller_AuthorizeCashDeposit();
			//Navigate 
			if (HtmlReporter.tc_result) ROATModules.NavigateSupportFunctions();
			//Delete  - 110-000004
			if (HtmlReporter.tc_result) ROATModules.DeleteTransactionNoCheck();
			//Verify
			if (HtmlReporter.tc_result) ROATModules.VerifyTransactionNoCheck();
		}
		catch(Exception ex)
		{
			ex.printStackTrace();
		}
	}

	public static void Sanity_ForexBillsNegotiableAdd()
	{
		try 
		{
			ROATFunctions.refesh(false, Launcher.UserRole);

			WebDr.SetPageObjects("MasterPage");
			if (HtmlReporter.tc_result)ROATModules.navigateBillsNegotiableAdd();
			if (HtmlReporter.tc_result)ROATModules.BillsNegotiableAdd();
			//cancel
			if (HtmlReporter.tc_result)WebDr.clickX("X", "cancel", "Clicking on cancel", true);
			if (HtmlReporter.tc_result)WebDr.clickX("X", "button_yes", "Clicking on yes cancel", true);
			//success
			if (HtmlReporter.tc_result)ROATModules.navigateBillsNegotiableAdd();
			if (HtmlReporter.tc_result)ROATModules.BillsNegotiableAdd();
			if (HtmlReporter.tc_result)WebDr.clickX("X", "Next", "Clicking on Next", true);
			String auth1[] = Launcher.Auth_User_Login("Super User");
			String Login1[] = Launcher.User_Login("Super User");
			ROATModules.authorize_Credential_Vault("forexbillsnegotiableadd", "username", "password", "username2", "password2", auth1[0],  auth1[1], Login1[0], Login1[1],true);
			if (HtmlReporter.tc_result)WebDr.waitTillElementIsDisplayed("btnAccept");
			if (HtmlReporter.tc_result)WebDr.clickX("X", "btnAccept", "Clicking on btnAccept", true);
			if (HtmlReporter.tc_result)WebDr.waitTillElementIsDisplayed("post");
			if (HtmlReporter.tc_result)WebDr.clickX("X", "post", "Clicking on post", true);
			if (HtmlReporter.tc_result)WebDr.waitTillElementIsDisplayed("done");
			if (HtmlReporter.tc_result)WebDr.waitTillElementIsDisplayed("sucess_msg");
			if (HtmlReporter.tc_result)WebDr.verifyText("X", "sucess_msg", true, WebDr.getValue("success_messg_auth"), "verifying recipt genration and sucss message", true);
			if (HtmlReporter.tc_result)WebDr.waitTillElementIsDisplayed("done");
			if (HtmlReporter.tc_result)WebDr.clickX("X", "done", "Clicking on done", true);
			Thread.sleep(5000);
			//reject
			if (HtmlReporter.tc_result)ROATModules.BillsNegotiableAdd();
			WebDr.Wait_Time(2000);
			if (HtmlReporter.tc_result)WebDr.clickX("X", "Next", "Clicking on Next", true);
			String auth[] = Launcher.Auth_User_Login("Super User");
			String Login[] = Launcher.User_Login("Super User");
			ROATModules.authorize_Credential_Vault("forexbillsnegotiableadd", "username", "password", "username2", "password2", auth[0],  auth[1], Login[0], Login[1],false);
			if (HtmlReporter.tc_result)WebDr.waitTillElementIsDisplayed("rej_msg");
			if (HtmlReporter.tc_result)WebDr.verifyText("X", "rej_msg", true, WebDr.getValue("reject_msg_auth"), "verifying reject  message", true);

		}
		catch (Exception e)
		{
			e.printStackTrace();
			Reporter.ReportEvent("Test Case execution is Failed", "test case excecution Failed","test case excecution Failed", false);
		}
	}

	public static void Sanity_sequenceNoMaint()
	{
		try {
			ROATFunctions.refesh(false, Launcher.UserRole);
			if (HtmlReporter.tc_result)ROATModules.menu_navigation("MasterPage", "tab_setup", "tab_setup_closed");
			if (HtmlReporter.tc_result)ROATModules.menu_navigation1("MasterPage", "tab_seqno", "null");
			if (HtmlReporter.tc_result)ROATModules.verifySequenceNo();
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	public static void Sanity_currency_acc_maintainance() {
		try
		{

			ROATFunctions.refesh(false,Launcher.UserRole);
			if (HtmlReporter.tc_result){String branch_name=ROATModules.fetch_user_branch();
			if (HtmlReporter.tc_result)	WebDr.SetPageObjects("Setup_curr_maint");

			if (HtmlReporter.tc_result)ROATModules.Setup_navigation_currency_maint();

			local_gl__number=WebDr.getValue("Local_cur_GL_Branch")+" "+WebDr.getValue("Local_cur_GL_no");
			forex_gl__number=WebDr.getValue("Local_cur_GL_Branch")+" "+WebDr.getValue("Local_cur_GL_no");

			WebDr.SetPageObjects("Setup_curr_maint");
			if (HtmlReporter.tc_result) WebDr.setText("X", "enter_branch_name", branch_name, "Entering branch Name", true);
			if (HtmlReporter.tc_result) WebDr.setText("X", "enter_curency_name", WebDr.getValue("Local_currency"), "Entering currency Name", true);
			if (HtmlReporter.tc_result) WebDr.Wait_Time(1000);

			if (WebDr.isExists("update_icon")==1)
			{
				if (HtmlReporter.tc_result)ROATModules.setup_currency_update_reject(branch_name);
				if (HtmlReporter.tc_result) WebDr.setText("X", "enter_branch_name", branch_name, "Entering branch Name", true);
				if (HtmlReporter.tc_result) WebDr.setText("X", "enter_curency_name", WebDr.getValue("Local_currency"), "Entering currency Name", true);
				if (HtmlReporter.tc_result) WebDr.Wait_Time(1000);
				if (HtmlReporter.tc_result)ROATModules.setup_currency_update_accept(branch_name,"Local_cur_GL_Branch","Local_cur_GL_no");
			}

			else if (WebDr.isExists("empty_table")==1)
			{
				if (HtmlReporter.tc_result)ROATModules.setup_currency_add_new_reject(branch_name);
				if (HtmlReporter.tc_result) WebDr.setText("X", "enter_branch_name", branch_name, "Entering branch Name", true);
				if (HtmlReporter.tc_result) WebDr.setText("X", "enter_curency_name", WebDr.getValue("Local_currency"), "Entering currency Name", true);
				if (HtmlReporter.tc_result) WebDr.Wait_Time(1000);
				if (HtmlReporter.tc_result)ROATModules.setup_currency_add_new_accept(branch_name,true);
			}

			WebDr.SetPageObjects("Setup_curr_maint");
			if (HtmlReporter.tc_result) WebDr.setText("X", "enter_branch_name", branch_name, "Entering branch Name", true);
			if (HtmlReporter.tc_result) WebDr.setText("X", "enter_curency_name", WebDr.getValue("Forex_currency"), "Entering currency Name", true);
			if (HtmlReporter.tc_result) WebDr.Wait_Time(1000);

			if (WebDr.isExists("update_icon")==1)
			{
				if (HtmlReporter.tc_result)ROATModules.duplicate_check(branch_name);
				if (HtmlReporter.tc_result) WebDr.setText("X", "enter_branch_name", branch_name, "Entering branch Name", true);
				if (HtmlReporter.tc_result) WebDr.setText("X", "enter_curency_name", WebDr.getValue("Forex_currency"), "Entering currency Name", true);
				if (HtmlReporter.tc_result) WebDr.Wait_Time(1000);
				if (HtmlReporter.tc_result)ROATModules.setup_currency_update_accept(branch_name,"Forex_cur_GL_Branch","Forex_cur_GL_no");
			}

			if (WebDr.isExists("empty_table")==1)
			{
				if (HtmlReporter.tc_result)ROATModules.setup_currency_add_new_accept(branch_name,false);
			}

			}
		}

		catch(Exception ex)
		{
			ex.printStackTrace();
			Reporter.ReportEvent("Test Case execution is Failed", "Currency account maintainence must be successfully executed",
					"Currency account maintainence executition failed", false);
		}
	}

	public static void Sanity_batchPosting()
	{
		try
		{
			ROATFunctions.refesh(false, Launcher.UserRole);
			if (HtmlReporter.tc_result)ROATModules.menu_navigation("MasterPage", "tab_Teller", "null");
			if (HtmlReporter.tc_result)ROATModules.menu_navigation1("MasterPage", "tab_Batch", "null");
			if (HtmlReporter.tc_result)ROATModules.Batch_Posting_ContinueDelete_Batch("Continue");
			if (HtmlReporter.tc_result) ROATModules.reNavigateToPosting();
			if (HtmlReporter.tc_result)ROATModules.Batch_Posting_ContinueDelete_Batch("Delete");
			if (HtmlReporter.tc_result)ROATModules.Batch_Posting_Cancel();
			if (HtmlReporter.tc_result)ROATModules.Batch_Posting_Add();
			if (HtmlReporter.tc_result)ROATModules.Batch_Posting_Cancel();
			if (HtmlReporter.tc_result)ROATModules.Batch_Posting_Add();
			if (HtmlReporter.tc_result)ROATModules.Batch_Posting_Saveforlater();
			if (HtmlReporter.tc_result) ROATModules.reNavigateToPosting();
			if (HtmlReporter.tc_result)ROATModules.Batch_Posting_Add_Next();
		}
		catch (Exception e)
		{
			e.printStackTrace();
		}
	}

	public static void Sanity_chargePackage() 
	{
		try {
			ROATFunctions.refesh(false, Launcher.UserRole);
			String ChargePack = null;
			if (HtmlReporter.tc_result)ROATModules.Packages_navigateChargePackages();
			ChargePack = ROATModules.Packages_AddChargePackage();
			if (HtmlReporter.tc_result)ROATModules.Packages_CancelAddChargePackage();
			/*if (HtmlReporter.tc_result)ROATModules.Packages_ViewChargePackages();*/
			if (HtmlReporter.tc_result)ROATModules.Packages_UpdateChargePackage(ChargePack);
			if (HtmlReporter.tc_result)ROATModules.Packages_DeleteChargePackage(ChargePack);
			//			 if (HtmlReporter.tc_result)ROATModules.Packages_ActivityChargePackage(ChargePack);
			if (HtmlReporter.tc_result == false)
			{
				ROATFunctions.refesh(true,Launcher.UserRole);
			} 
		}
		catch (Exception e) 
		{
			Reporter.ReportEvent("Test Case execution is failed", "Test Case execution is failed",
					"Test Case execution is failed", false);
		}
	}




	public static void Sanity_AccountMaintaince() {
		try {
			if(HtmlReporter.tc_result)WebDr.SetPageObjects("MasterPage");
			if(HtmlReporter.tc_result)ROATFunctions.refesh(false, Launcher.UserRole);
			//navigate 
			if(HtmlReporter.tc_result)ROATModules.NavigateAccountMaintaince();
			if(HtmlReporter.tc_result)ROATModules.ClearSearchAccountMaintaince(true);
			if(HtmlReporter.tc_result)ROATModules.ClearSearchAccountMaintaince(false);
			if(HtmlReporter.tc_result)ROATModules.UpdateAccountMaintaince(true);

		} catch (Exception e) {

			e.printStackTrace();
			Reporter.ReportEvent("Test Case execution is Failed", "excution successfull",
					"excution Failed", false);
		}
	}

	public static void Sanity_Internal_transfer_Receipt()
	{

		try{
			if (HtmlReporter.tc_result)ROATFunctions.refesh(false,"Teller");
			if (HtmlReporter.tc_result)
			{
				int trxnamt = ROATModules.generateBreachAmount("Tranfer_amt_no_breach", 1, "low");
				if (HtmlReporter.tc_result)ROATModules.navigateInternalTransfer();
				if (HtmlReporter.tc_result)ROATModules.transferPageVerification();
				if (HtmlReporter.tc_result) 
				{
					Double fromnewbal = ROATModules.ent_detail_lcy(Integer.toString(trxnamt));

					if (HtmlReporter.tc_result) 
					{
						if (HtmlReporter.tc_result)ROATModules.button("nextbt");
						Thread.sleep(4000);
						if (HtmlReporter.tc_result)ROATModules.button("accept");
						if (HtmlReporter.tc_result)ROATModules.breachauthorize();
						Thread.sleep(1000);
						String reference_no=WebDr.getElement("serial_num").getText();
						if (HtmlReporter.tc_result)ROATModules.button("post");
						if (HtmlReporter.tc_result)ROATModules.verify_messg("succ_mgs", "msg_success");
						if (HtmlReporter.tc_result)WebDr.Wait_Time(5000);
						try
						{
							if(WebDr.isExists("Reciept")>0)
							{
								Reporter.ReportEvent("Checking reciept generation", "Internal transfer reciept generated",
										"Internal transfer reciept generated", true);
							}
							else
							{
								Reporter.ReportEvent("Checking reciept generation", "Internal transfer reciept generated",
										"Internal transfer reciept not generated", false);
							}
						}
						catch(Exception e)
						{
							Reporter.ReportEvent("Checking reciept generation", "Internal transfer reciept generated",
									"Internal transfer reciept not generated", false);	
						}
						//					if (HtmlReporter.tc_result)
						//						ROATModules.verifyReceipt("Internal_Transfer","Receipt_fv","Rep_detl");
						//					if (HtmlReporter.tc_result)
						//						ROATModules.verifyReceiptD("Internal_Transfer","Receipt_fv","Rep_detl",reference_no);

						if (HtmlReporter.tc_result)ROATModules.button("done");

						if (HtmlReporter.tc_result)ROATModules.transferPageVerification();
						if (HtmlReporter.tc_result) 
						{
							fromnewbal = ROATModules.ent_detail_fcy(Integer.toString(trxnamt));
							if (HtmlReporter.tc_result) 
							{
								if (HtmlReporter.tc_result)ROATModules.button("nextbt");
								Thread.sleep(4000);
								if (HtmlReporter.tc_result)ROATModules.button("accept");
								if (HtmlReporter.tc_result)ROATModules.breachauthorize();
								Thread.sleep(1000);
								reference_no=WebDr.getElement("serial_num").getText();
								if (HtmlReporter.tc_result)ROATModules.button("post");
								if (HtmlReporter.tc_result)ROATModules.verify_messg("succ_mgs", "msg_success");
								if (HtmlReporter.tc_result)WebDr.Wait_Time(5000);
								//							if (HtmlReporter.tc_result)
								//								ROATModules.verifyReceipt("Internal_Transfer","Receipt_fv","Rep_detf");
								//							if (HtmlReporter.tc_result)
								//								ROATModules.verifyReceiptD("Internal_Transfer","Receipt_fv","Rep_detl",reference_no);
								//							if (HtmlReporter.tc_result)
								try
								{
									if(WebDr.isExists("Reciept")>0)
									{
										Reporter.ReportEvent("Checking reciept generation", "Internal transfer reciept generated",
												"Internal transfer reciept generated", true);
									}
									else
									{
										Reporter.ReportEvent("Checking reciept generation", "Internal transfer reciept generated",
												"Internal transfer reciept not generated", false);
									}
								}
								catch(Exception e)
								{
									Reporter.ReportEvent("Checking reciept generation", "Internal transfer reciept generated",
											"Internal transfer reciept not generated", false);	
								}
								ROATModules.button("done");
							}
						}
					}
					if (HtmlReporter.tc_result == false)
					{
						ROATFunctions.refesh(true,Launcher.UserRole);
					}
				}
			}
		} 
		catch (Exception e)
		{

			e.printStackTrace();
			Reporter.ReportEvent("Test Case execution is Failed", "excution successfull",
					"excution Failed", false);
		}
	}

	public static void Sanity_MICRParameters() {
		try{
			ROATFunctions.refesh(false, Launcher.UserRole);
			if (HtmlReporter.tc_result)WebDr.SetPageObjects("MasterPage");
			if (HtmlReporter.tc_result)ROATModules.navigate_MICRParameters();
			if (HtmlReporter.tc_result)ROATModules.addnew_MICRParameters();
		}
		catch(Exception e){
			e.printStackTrace();
			Reporter.ReportEvent("Test case execution failed", "Test case execution failed","Test case execution failed", false);
		}
	}

	public static void Sanity_chargeApplication() {
		try {
			ROATFunctions.refesh(false, Launcher.UserRole);
			if (HtmlReporter.tc_result)ROATModules.Setup_navigateTariffMaintenence();
			if (HtmlReporter.tc_result)ROATModules.setupTariffMaintenance();

		} catch (Exception e) {
			Reporter.ReportEvent("Test Case execution is failed", "Test Case execution is failed",
					"Test Case execution is failed", false);
		}
	}

	public static void Sanity_forexPurchase() 
	{
		try {
			try
			{/*
				ROATFunctions.refesh(false, Launcher.UserRole);
				if (HtmlReporter.tc_result)
				{
					String user_branch = ROATModules.fetch_user_branch();
					if (HtmlReporter.tc_result)ROATModules.navigate_ForexFees();
					if (HtmlReporter.tc_result)
					{
						Map<String, Integer> mapForexPurchaseValues = ROATModules.checkRecordorAddRecordForexPurchase(user_branch);
					}
				}
			 */}
			catch (Exception e)
			{
				Reporter.ReportEvent("Test Case execution is failed", "Test Case execution is failed","Test Case execution is failed", false);
			}
			ROATFunctions.refesh(true, "Teller");
			for (int i=1; i<=2; i++) 
			{
				if (HtmlReporter.tc_result)ROATModules.menu_navigation("MasterPage", "tab_Forex", "tab_Forex_closed");
				if (HtmlReporter.tc_result)ROATModules.menu_navigation1("MasterPage", "tab_Exchange", "tab_Exchange_closed");
				if (HtmlReporter.tc_result)ROATModules.menu_navigation1("MasterPage", "tab_Purchase", "null");
				if (HtmlReporter.tc_result)ROATModules.forexExchangePurchase(i);
				Thread.sleep(2000);
				if (HtmlReporter.tc_result)ROATModules.postForexPurchase(i);
			}

		}
		catch (Exception e)
		{
			e.printStackTrace();
		}	
	}

	public static void Sanity_TariffMaintenence() 
	{
		try {
			ROATFunctions.refesh(false, Launcher.UserRole);
			if (HtmlReporter.tc_result)ROATModules.Setup_navigateTariffMaintenence();
			if (HtmlReporter.tc_result)ROATModules.Setup_ViewTariffMaintenence();
			if (HtmlReporter.tc_result)ROATModules.Setup_UpdateTariffMaintenece();
			if (HtmlReporter.tc_result)ROATModules.Setup_DeleteTariffMaintenece();
		} 
		catch (Exception e)
		{
			Reporter.ReportEvent("Test Case execution is failed", "Test Case execution is failed","Test Case execution is failed", false);
		}
	}




	@SuppressWarnings({ "unchecked", "unused" })
	public static void sanity_lcy_transferout () throws Exception
	{
		int lcy_range=Integer.parseInt(WebDr.getValue("lcy_range"));
		String local_currency=WebDr.getValue("lcy_currency");
		String branch_name="";
		String den_value="1";
		int number_rows=99;
		List<String> conditions=new ArrayList<String>();

		HashMap<String, BigDecimal> tt_inquiry_lcy = new HashMap<String, BigDecimal>();
		HashMap<String, BigDecimal> tt_inquiry_lcy_after = new HashMap<String, BigDecimal>();
		HashMap<String, HashMap<String,Object>> auth_dual = new HashMap<String, HashMap<String,Object>>();
		HashMap<String, BigDecimal> cd_teller_inquiry = new HashMap<String, BigDecimal>();
		HashMap<String, BigDecimal> cd_teller_inquiry_after = new HashMap<String, BigDecimal>();

		ROATFunctions.refesh(false,"Teller");
		branch_name=ROATModules.fetch_user_branch();

		auth_dual=ROATModules.authoriation_dual_control_inquiry(branch_name);

		//*************************************************************Teller---LCY Transferout**********************************************
		Reporter.ReportEvent("End to end ", "Teller LCY Tranferout","Teller LCY Tranferout", true);
		HtmlReporter.tc_result=true;
		try
		{

			//Teller inquiry before transaction
			tt_inquiry_lcy=ROATModules.LCY_inquiry();

			if (HtmlReporter.tc_result)
			{
				System.out.println("tt_inquiry_lcy:"+tt_inquiry_lcy);

				//payee inquiry--before
				if (HtmlReporter.tc_result)
				{
					cd_teller_inquiry=ROATModules.CD_teller_inquiry(local_currency,WebDr.getValue("tranferout_id"));	



					//Teller transferout 
					if (HtmlReporter.tc_result)ROATModules.tt_tranfer_out_lcy(WebDr.getValue("tranferout_id"),lcy_range,"1",99,auth_dual);



					if (HtmlReporter.tc_result)
					{
						WebDr.Wait_Time(10000);//  waiting for balance to get updated
						//Teller inquiry after transaction
						tt_inquiry_lcy_after=ROATModules.LCY_inquiry();
						System.out.println("tt_inquiry_lcy_after:"+tt_inquiry_lcy_after);
						conditions.add("Payment");
						ROATModules.teller_inquiry_check(tt_inquiry_lcy,tt_inquiry_lcy_after,conditions, lcy_range,"Addition");
						conditions.clear();
						conditions.add("Balance");
						conditions.add("Denomination_balance");
						ROATModules.teller_inquiry_check(tt_inquiry_lcy,tt_inquiry_lcy_after,conditions, lcy_range,"Subtraction");
						conditions.clear();

						if (HtmlReporter.tc_result)
						{
							System.out.println("tt_inquiry_lcy:"+tt_inquiry_lcy);

							//payee inquiry---after
							if (HtmlReporter.tc_result)
							{

								cd_teller_inquiry_after=ROATModules.CD_teller_inquiry(local_currency,WebDr.getValue("tranferout_id"));	
								WebDr.Wait_Time(5000);//  waiting for balance to get updated
								conditions.add("Receipts");
								conditions.add("Balance");
								conditions.add("Denomination_balance");
								ROATModules.teller_inquiry_check(cd_teller_inquiry,cd_teller_inquiry_after,conditions, lcy_range,"Addition");
								conditions.clear();

							}
						}
					}	
				}
			}
		}
		catch(Exception ex)
		{
			ex.printStackTrace();
			Reporter.ReportEvent("Test Case execution is Failed", "end to end Transfer out","end to end Deposit executition failed", false);
		}



	}
	@SuppressWarnings({ "unused", "unchecked" })
	public static void sanity_lcy_changein() throws Exception
	{
		int lcy_range=Integer.parseInt(WebDr.getValue("lcy_range"));
		String local_currency=WebDr.getValue("lcy_currency");
		String branch_name="";
		String den_value="1";
		int number_rows=99;

		List<String> conditions=new ArrayList<String>();
		HashMap<String, HashMap<String,Object>> auth_dual = new HashMap<String, HashMap<String,Object>>();
		HashMap<String, HashMap<String, String>> transaction_codes = new HashMap<String, HashMap<String, String>>();

		HashMap<String, BigDecimal> tt_inquiry_lcy = new HashMap<String, BigDecimal>();
		HashMap<String, BigDecimal> tt_inquiry_lcy_after = new HashMap<String, BigDecimal>();
		HashMap<String, BigDecimal> vault_inquiry_lcy = new HashMap<String, BigDecimal>();
		HashMap<String, BigDecimal> vault_inquiry_lcy_after = new HashMap<String, BigDecimal>();


		ROATFunctions.refesh(false,"Teller");
		branch_name=ROATModules.fetch_user_branch();

		auth_dual=ROATModules.authoriation_dual_control_inquiry(branch_name);


		//*************************************************************Vault---Vault LCY--change**********************************************
		Reporter.ReportEvent("End to end ", "Vault LCY--change","Vault LCY--change", true);
		HtmlReporter.tc_result=true;
		try
		{
			//vault inquiry before transaction
			if(HtmlReporter.tc_result)
			{
				vault_inquiry_lcy=ROATModules.Vault_LCY_Inquiry();

				if(HtmlReporter.tc_result)
				{
					System.out.println("vault_inquiry_lcy:"+vault_inquiry_lcy);

					ROATModules.vault_cdc_change_lcy(WebDr.getValue("Change_in_amt"));


					//vault Inquiry verification
					if (HtmlReporter.tc_result)
					{
						vault_inquiry_lcy_after=ROATModules.Vault_LCY_Inquiry();
						{
							System.out.println("vault_inquiry_lcy_after:"+vault_inquiry_lcy_after);
							conditions.add("CDC_Payments");
							conditions.clear();
							conditions.add("Denomination_balance");
							conditions.add("balance");
							ROATModules.teller_inquiry_check(vault_inquiry_lcy,vault_inquiry_lcy_after,conditions, 0,"Subtraction");
							conditions.clear();


						}
					}
				}
			}
		}
		catch(Exception ex)
		{
			ex.printStackTrace();
			Reporter.ReportEvent("End to end ", "Vault LCY--change","Vault LCY--change FAILED", false);
		}


	}
	@SuppressWarnings({ "unused", "unchecked" })
	public static void sanity_forex_fees()
	{
		String branch_name="";
		String den_value="1";
		int number_rows=99;
		String currency="USD";
		String user_branch ="";
		String user_id="";
		String user_name = "";
		String Branch="";
		int fcy_range=Integer.parseInt(WebDr.getValue("fcy_range"));
		List<String> conditions=new ArrayList<String>();
		HashMap<String, HashMap<String,Object>> auth_dual = new HashMap<String, HashMap<String,Object>>();
		HashMap<String, HashMap<String, String>> transaction_codes = new HashMap<String, HashMap<String, String>>();

		HashMap<String, BigDecimal> tt_inquiry_lcy = new HashMap<String, BigDecimal>();
		HashMap<String, BigDecimal> tt_inquiry_lcy_after = new HashMap<String, BigDecimal>();
		HashMap<String, BigDecimal> vault_inquiry_lcy = new HashMap<String, BigDecimal>();
		HashMap<String, BigDecimal> vault_inquiry_lcy_after = new HashMap<String, BigDecimal>();
		HashMap<String, Double> forex_fees=new HashMap<String, Double>();
		HashMap<String, Map<String,String>> store_forex_rate = new HashMap<String, Map<String,String>>();


		try
		{
			ROATFunctions.refesh(false,"Teller");
			if (HtmlReporter.tc_result)
			{ 
				user_branch = ROATModules.fetch_user_branch();
				user_id=ROATFunctions.fetch_user_Id();
				user_name = ROATModules.fetchuser();
				Branch=ROATFunctions.fetch_user_branch_name();
				branch_name=ROATModules.fetch_user_branch();
				// Fetch forex rate 
				ROATFunctions.refesh(false, "Central Supervisor");
				WebDr.SetPageObjects("end_end");
				store_forex_rate = ROATModules.set_forexrate(currency);

				if (HtmlReporter.tc_result)
				{
					forex_fees = ROATModules.forex_purchase_fees_setup(user_branch,fcy_range,store_forex_rate,true);
					System.out.println(forex_fees);
				}

				if (HtmlReporter.tc_result)
				{
					forex_fees = ROATModules.forex_purchase_fees_setup(user_branch,fcy_range,store_forex_rate,false);
					System.out.println(forex_fees);
				}

			}
		}
		catch(Exception ex)
		{
			ex.printStackTrace();
			Reporter.ReportEvent("End to end ", "Vault LCY--change","Vault LCY--change FAILED", false);
		}


	}
	public static void sanity_vlt_fcy_change() {
		try {
			ROATFunctions.refesh(false, Launcher.UserRole);
			if (HtmlReporter.tc_result)ROATModules.vlt_fcy_change_navigation();
			if (HtmlReporter.tc_result)ROATModules.vlt_fcy_change_cms();
			if (HtmlReporter.tc_result)
			{
				if (HtmlReporter.tc_result)ROATModules.vlt_nav_change_page(true,WebDr.getValue("Amount_reject"));
				ArrayList<Integer> pos_bfr_auth=ROATModules.vlt_change_positon_check("Vault_Fcy_Change");
				if (HtmlReporter.tc_result)ROATModules.vlt_change_authorize(true);
				if (HtmlReporter.tc_result)ROATModules.vlt_nav_change_page(true,WebDr.getValue("Amount_reject"));
				ArrayList<Integer> pos_aft_auth=ROATModules.vlt_change_positon_check("Vault_Fcy_Change");
				if (HtmlReporter.tc_result)
				{
					pos_bfr_auth=ROATModules.perform_update("Vault_Fcy_Change", pos_bfr_auth, WebDr.getValue("note1_in"),  WebDr.getValue("note2_in"), WebDr.getValue("note3_in"), WebDr.getValue("note1_out"), WebDr.getValue("note2_out"), WebDr.getValue("note3_out"));
					if(pos_bfr_auth.equals(pos_aft_auth))
					{
						Reporter.ReportEvent("Checking vault position update ", "Vault postion must be updated", "Vault postion is updated", true);
					}
					else
					{
						Reporter.ReportEvent("Checking vault position update ", "Vault postion must be updated", "Vault postion is not updated", true);
					}
				}

			} }
		catch (Exception e)
		{
			e.printStackTrace();
		}

	}

	public static void sanity_forexDrafts() {
		try {
			//			
			//			if (HtmlReporter.tc_result)
			//				ROATFunctions.refesh(true, "Super User");
			//			String user_branch = ROATModules.fetch_user_branch();
			//			if (HtmlReporter.tc_result)
			//				ROATModules.navigate_ForexFees();
			//			if (HtmlReporter.tc_result)
			//			{
			//				Map<String, Integer> mapForexPurchaseValues = ROATModules.checkRecordorAddRecordForexPurchase(user_branch);
			//			}
			ROATFunctions.refesh(true, "Teller");
			if (HtmlReporter.tc_result)ROATModules.menu_navigation("MasterPage", "tab_Forex", "tab_Forex_closed");
			if (HtmlReporter.tc_result)ROATModules.menu_navigation("MasterPage", "tab_drafts", "tab_drafts_closed");
			if (HtmlReporter.tc_result)ROATModules.menu_navigation1("MasterPage", "tab_draft_issue", "null");
			if (HtmlReporter.tc_result)ROATFunctions.scrollTillEnd_element(WebDr.getElement("tab_draft_issue"));
			if (HtmlReporter.tc_result)ROATModules.forexDraftsIssue("Yes");
			if (HtmlReporter.tc_result) ROATModules.menu_navigation("MasterPage", "tab_Forex", "tab_Forex_closed");
			if (HtmlReporter.tc_result) ROATModules.menu_navigation("MasterPage", "tab_drafts", "tab_drafts_closed");
			if (HtmlReporter.tc_result)ROATFunctions.scrollTillEnd_element(WebDr.getElement("tab_draft_view"));
			if (HtmlReporter.tc_result) ROATModules.menu_navigation1("MasterPage", "tab_draft_view", "null");
			if (HtmlReporter.tc_result)ROATFunctions.scrollTillEnd();
			if (HtmlReporter.tc_result) ROATModules.ForexDraft_View();
			if (HtmlReporter.tc_result)ROATModules.menu_navigation("MasterPage", "tab_Forex", "tab_Forex_closed");
			if (HtmlReporter.tc_result)ROATModules.menu_navigation("MasterPage", "tab_drafts", "tab_drafts_closed");
			if (HtmlReporter.tc_result)ROATFunctions.scrollTillEnd_element(WebDr.getElement("tab_draft_issue"));
			if (HtmlReporter.tc_result)ROATModules.menu_navigation1("MasterPage", "tab_draft_issue", "null");
			if (HtmlReporter.tc_result)ROATModules.forexDraftsIssue("No");
			if (HtmlReporter.tc_result) ROATModules.menu_navigation("MasterPage", "tab_Forex", "tab_Forex_closed");
			if (HtmlReporter.tc_result) ROATModules.menu_navigation("MasterPage", "tab_drafts", "tab_drafts_closed");
			if (HtmlReporter.tc_result)ROATFunctions.scrollTillEnd_element(WebDr.getElement("tab_draft_pending"));
			if (HtmlReporter.tc_result) ROATModules.menu_navigation1("MasterPage", "tab_draft_pending", "null");
			if (HtmlReporter.tc_result) ROATModules.ForexDraft_Filter();
			if (HtmlReporter.tc_result) ROATModules.ForexDraft_Sorting();
			//if (HtmlReporter.tc_result)	ROATModules.ForexDraft_CountTotalRecords();
			if (HtmlReporter.tc_result) ROATModules.ForexDraft_Actions();
			if (HtmlReporter.tc_result)ROATModules.menu_navigation("MasterPage", "tab_Forex", "tab_Forex_closed");
			if (HtmlReporter.tc_result)ROATModules.menu_navigation("MasterPage", "tab_drafts", "tab_drafts_closed");
			if (HtmlReporter.tc_result)ROATFunctions.scrollTillEnd_element(WebDr.getElement("tab_draft_issue"));
			if (HtmlReporter.tc_result)ROATModules.menu_navigation1("MasterPage", "tab_draft_issue", "null");
			if (HtmlReporter.tc_result)ROATModules.forexDraftsIssue("No");
			if (HtmlReporter.tc_result)ROATModules.menu_navigation("MasterPage", "tab_Forex", "tab_Forex_closed");
			if (HtmlReporter.tc_result)ROATModules.menu_navigation("MasterPage", "tab_drafts", "tab_drafts_closed");
			if (HtmlReporter.tc_result)ROATFunctions.scrollTillEnd_element(WebDr.getElement("tab_draft_cancel"));
			if (HtmlReporter.tc_result)ROATModules.menu_navigation1("MasterPage", "tab_draft_cancel", "null");
			if (HtmlReporter.tc_result)ROATModules.forexDraftsCancel();
		} catch (Exception e) {
			e.printStackTrace();
		}

	}



	public static void Sanity_Forex_Exchange_Sell() 
	{

		try
		{
			if (HtmlReporter.tc_result)ROATFunctions.refesh(true,"Teller");
			//cancel function
			if (HtmlReporter.tc_result)ROATModules.Navigate_Exchange_Sell();
			ROATFunctions.clickTab();
			if(WebDr.isExists_Display("err_FieldError")>0)
			{
				WebDr.setText("X", "txtAccNum", WebDr.getValue("txtAccNum"), "amount", true);
				ROATFunctions.clickTab();
			}
			if (HtmlReporter.tc_result) 
			{
				if(WebDr.isExists_Display("IDV") > 0)
				{
					if (HtmlReporter.tc_result)WebDr.click("X", "IDV", "Click idv radio btn", true);
					if (HtmlReporter.tc_result)WebDr.click("X", "Okclick", "Click ok btn", true);
				}
			}
			WebDr.setText("X", "txtAccNum", WebDr.getValue("txtAccNum"), "amount", true);
			ROATFunctions.clickTab();
			if (HtmlReporter.tc_result==true) WebDr.clickX("X", "cancel", "Clicking on cancel  field", true);
			if (HtmlReporter.tc_result==true)ROATFunctions.scrolltotop();





			//reject
			if (HtmlReporter.tc_result)ROATModules.Navigate_Exchange_Sell();
			//			if (HtmlReporter.tc_result)WebDr.setText("X", "txtAccNum", WebDr.getValue("txtAccNum"), "amount", true);
			ROATFunctions.clickTab();
			if(WebDr.isExists_Display("err_FieldError")>0)
			{
				WebDr.setText("X", "txtAccNum", WebDr.getValue("txtAccNum"), "amount", true);
				ROATFunctions.clickTab();
			}
			if (HtmlReporter.tc_result) 
			{
				if(WebDr.isExists_Display("IDV") > 0)
				{
					if (HtmlReporter.tc_result)WebDr.click("X", "IDV", "Click idv radio btn", true);
					if (HtmlReporter.tc_result)WebDr.click("X", "Okclick", "Click ok btn", true);
				}
			}
			WebDr.setText("X", "txtAccNum", WebDr.getValue("txtAccNum"), "amount", true);
			ROATFunctions.clickTab();
			if (HtmlReporter.tc_result==true) WebDr.clickX("X", "accept", "Clicking on accept  field", true);
			if (HtmlReporter.tc_result)ROATModules.authorisation();
			if (HtmlReporter.tc_result==true) WebDr.clickX("X", "btnReject", "Clicking on btnReject  field", true);
			if (HtmlReporter.tc_result)WebDr.setText("X", "reason", WebDr.getValue("reason"), "Enteringreject reason", true);
			if (HtmlReporter.tc_result==true) WebDr.clickX("X", "btnSubmit2", "Clicking on btnSubmit2  field", true);




			//btnClear
			if (HtmlReporter.tc_result==true)ROATFunctions.scrolltotop();
			if (HtmlReporter.tc_result)ROATModules.Navigate_Exchange_Sell();
			//			if (HtmlReporter.tc_result)	WebDr.setText("X", "txtAccNum", WebDr.getValue("txtAccNum"), "amount", true);
			if (HtmlReporter.tc_result)ROATFunctions.clickTab();
			if(WebDr.isExists_Display("err_FieldError")>0)
			{
				WebDr.setText("X", "txtAccNum", WebDr.getValue("txtAccNum"), "amount", true);
				ROATFunctions.clickTab();
			}
			if (HtmlReporter.tc_result) 
			{
				if(WebDr.isExists_Display("IDV") > 0)
				{
					if (HtmlReporter.tc_result)WebDr.click("X", "IDV", "Click idv radio btn", true);

					if (HtmlReporter.tc_result)WebDr.click("X", "Okclick", "Click ok btn", true);
				}
			}
			WebDr.setText("X", "txtAccNum", WebDr.getValue("txtAccNum"), "amount", true);
			ROATFunctions.clickTab();
			if (HtmlReporter.tc_result==true) WebDr.clickX("X", "accept", "Clicking on accept  field", true);
			if (HtmlReporter.tc_result)ROATModules.authorisation();
			if (HtmlReporter.tc_result==true)WebDr.clickX("X", "btnAuthorize", "Clicking on Autorize button", true);
			if (HtmlReporter.tc_result==true)WebDr.clickX("X", "btnClear", "Clicking on btnClear  field", true);
			if (HtmlReporter.tc_result==true)ROATFunctions.scrolltotop();

			if (HtmlReporter.tc_result)ROATFunctions.refesh(true,"Teller");
			//btnCancel
			if (HtmlReporter.tc_result)ROATModules.Navigate_Exchange_Sell();
			//			if (HtmlReporter.tc_result)WebDr.setText("X", "txtAccNum", WebDr.getValue("txtAccNum"), "amount", true);
			if (HtmlReporter.tc_result)ROATFunctions.clickTab();
			if(WebDr.isExists_Display("err_FieldError")>0)
			{
				WebDr.setText("X", "txtAccNum", WebDr.getValue("txtAccNum"), "amount", true);
				ROATFunctions.clickTab();
			}
			if (HtmlReporter.tc_result) 
			{
				if(WebDr.isExists_Display("IDV") > 0)
				{
					if (HtmlReporter.tc_result)WebDr.click("X", "IDV", "Click idv radio btn", true);
					if (HtmlReporter.tc_result)WebDr.click("X", "Okclick", "Click ok btn", true);
				}
			}
			WebDr.setText("X", "txtAccNum", WebDr.getValue("txtAccNum"), "amount", true);
			ROATFunctions.clickTab();
			if (HtmlReporter.tc_result==true) WebDr.clickX("X", "accept", "Clicking on accept  field", true);
			if (HtmlReporter.tc_result)ROATModules.authorisation();
			if (HtmlReporter.tc_result==true)WebDr.clickX("X", "btnAuthorize", "Clicking on Autorize button", true);
			if (HtmlReporter.tc_result==true) WebDr.clickX("X", "btnCancel", "Clicking on btnClear  field", true);
			if (HtmlReporter.tc_result==true)ROATFunctions.scrolltotop();



			//btncancel2
			if (HtmlReporter.tc_result)ROATModules.Navigate_Exchange_Sell();
			//			WebDr.setText("X", "txtAccNum", WebDr.getValue("txtAccNum"), "amount", true);
			if (HtmlReporter.tc_result)ROATFunctions.clickTab();
			if(WebDr.isExists_Display("err_FieldError")>0)
			{
				WebDr.setText("X", "txtAccNum", WebDr.getValue("txtAccNum"), "amount", true);
				ROATFunctions.clickTab();
			}
			if (HtmlReporter.tc_result) 
			{
				if(WebDr.isExists_Display("IDV") > 0)
				{
					if (HtmlReporter.tc_result)WebDr.click("X", "IDV", "Click idv radio btn", true);
					if (HtmlReporter.tc_result)WebDr.click("X", "Okclick", "Click ok btn", true);
				}
			}
			WebDr.setText("X", "txtAccNum", WebDr.getValue("txtAccNum"), "amount", true);
			ROATFunctions.clickTab();
			if (HtmlReporter.tc_result==true) WebDr.clickX("X", "accept", "Clicking on accept  field", true);
			if (HtmlReporter.tc_result)ROATModules.authorisation();
			if (HtmlReporter.tc_result==true)WebDr.clickX("X", "btnAuthorize", "Clicking on Autorize button", true);
			if (HtmlReporter.tc_result==true) WebDr.clickX("X", "btnAccept", "Clicking on btnAccept  field", true);
			if (HtmlReporter.tc_result)ROATModules.duefromcustomer("cashFCYtoLCY");
			if (HtmlReporter.tc_result==true) WebDr.clickX("X", "btnCancel", "Clicking on btnCancel  field", true);
			if (HtmlReporter.tc_result==true)ROATFunctions.scrolltotop();




			//cash to cash function( FCY -> LCY)
			if (HtmlReporter.tc_result)ROATModules.Navigate_Exchange_Sell();
			if (HtmlReporter.tc_result)ROATFunctions.clickTab();
			WebDr.Wait_Time(1000);
			if(WebDr.isExists_Display("err_FieldError")>0)
			{
				WebDr.setText("X", "txtAccNum", WebDr.getValue("txtAccNum"), "amount", true);
				ROATFunctions.clickTab();
			}
			if (HtmlReporter.tc_result) 
			{
				if(WebDr.isExists_Display("IDV") > 0)
				{
					if (HtmlReporter.tc_result)WebDr.click("X", "IDV", "Click idv radio btn", true);
					if (HtmlReporter.tc_result)WebDr.click("X", "Okclick", "Click ok btn", true);
				}
			}
			WebDr.setText("X", "txtAccNum", WebDr.getValue("txtAccNum"), "amount", true);
			ROATFunctions.clickTab();
			if (HtmlReporter.tc_result==true) WebDr.clickX("X", "accept", "Clicking on accept  field", true);
			if (HtmlReporter.tc_result)ROATModules.authorisation();
			if (HtmlReporter.tc_result==true)WebDr.clickX("X", "btnAuthorize", "Clicking on Autorize button", true);
			if (HtmlReporter.tc_result==true) WebDr.clickX("X", "btnAccept", "Clicking on btnAccept  field", true);
			if (HtmlReporter.tc_result)ROATModules.duefromcustomer("cashFCYtoLCY");
			if (HtmlReporter.tc_result==true) WebDr.clickX("X", "btnNext", "Clicking on btnNext  field", true);
			if (HtmlReporter.tc_result==true) WebDr.clickX("X", "btnCancel", "Clicking on btnCancel  field", true);

			if (HtmlReporter.tc_result==true)ROATFunctions.scrolltotop();






			//cash to account function(FCY ->LCY)
			if (HtmlReporter.tc_result)ROATFunctions.refesh(true,"Teller");
			if (HtmlReporter.tc_result)ROATModules.Navigate_Exchange_Sell();
			//			if (HtmlReporter.tc_result)WebDr.setText("X", "txtAccNum", WebDr.getValue("txtAccNum"), "amount", true);
			if (HtmlReporter.tc_result)ROATFunctions.clickTab();
			if(WebDr.isExists_Display("err_FieldError")>0)
			{
				WebDr.setText("X", "txtAccNum", WebDr.getValue("txtAccNum"), "amount", true);
				ROATFunctions.clickTab();
			}
			if (HtmlReporter.tc_result) 
			{
				if(WebDr.isExists_Display("IDV") > 0)
				{
					if (HtmlReporter.tc_result)WebDr.click("X", "IDV", "Click idv radio btn", true);
					if (HtmlReporter.tc_result)WebDr.click("X", "Okclick", "Click ok btn", true);
				}
			}
			WebDr.setText("X", "txtAccNum", WebDr.getValue("txtAccNum"), "amount", true);
			ROATFunctions.clickTab();
			if (HtmlReporter.tc_result)ROATModules.clickaccountmethod("Pay");
			if (HtmlReporter.tc_result==true) WebDr.clickX("X", "accept", "Clicking on accept  field", true);
			for(int k=0;k<2;k++)
			{
				if(WebDr.isExists_Display("insufficient_fund_popup")>0)
				{
					if (HtmlReporter.tc_result)WebDr.click("X", "insufficient_fund_popup", "Click on insufficient_fund_popup", true);
					break;
				}
			}
			if (HtmlReporter.tc_result)ROATModules.authorisation();
			if (HtmlReporter.tc_result==true)WebDr.clickX("X", "btnAuthorize", "Clicking on Autorize button", true);
			if (HtmlReporter.tc_result==true) WebDr.clickX("X", "btnAccept", "Clicking on btnAccept  field", true);
			//			if (HtmlReporter.tc_result)ROATModules.duefromcustomer("cashFCYtoLCY");
			if (HtmlReporter.tc_result==true) WebDr.clickX("X", "btnNext", "Clicking on btnNext  field", true);
			if (HtmlReporter.tc_result==true) WebDr.clickX("X", "btnCancel", "Clicking on accept  field", true);
			if (HtmlReporter.tc_result==true)ROATFunctions.scrolltotop();



			/*




			//account to cash function (FCY ->LCY)
			if (HtmlReporter.tc_result)ROATModules.Navigate_Exchange_Sell();
			if (HtmlReporter.tc_result)ROATFunctions.cash_drawer_DD1("BranchDD", "BranchIN", "BranchOP", WebDr.getValue("Branch_fcy"));
			if (HtmlReporter.tc_result)WebDr.clickX("X", "BranchOP", "Click on branch", true);
			if (HtmlReporter.tc_result)ROATFunctions.clickTab();

			if(WebDr.isExists_Display("err_FieldError")>0)
			{
				WebDr.setText("X", "acc_no", WebDr.getValue("Account_fcy"), "amount", true);
				ROATFunctions.clickTab();
			}
			if (HtmlReporter.tc_result) 
			{
				if(WebDr.isExists_Display("IDV") > 0)
				{
					if (HtmlReporter.tc_result)WebDr.click("X", "IDV", "Click idv radio btn", true);
					if (HtmlReporter.tc_result)WebDr.click("X", "Okclick", "Click ok btn", true);
				}
			}
			WebDr.setText("X", "acc_no", WebDr.getValue("Account_fcy"), "amount", true);
			ROATFunctions.clickTab();
			if (HtmlReporter.tc_result)ROATModules.clickaccountmethod("Sell");

			if (HtmlReporter.tc_result==true) WebDr.clickX("X", "accept", "Clicking on accept  field", true);
			for(int k=0;k<2;k++)
			{
			if(WebDr.isExists_Display("insufficient_fund_popup")>0)
			{
				if (HtmlReporter.tc_result)WebDr.click("X", "insufficient_fund_popup", "Click on insufficient_fund_popup", true);
				break;
			}
			}
			if (HtmlReporter.tc_result)ROATModules.authorisation();
			if (HtmlReporter.tc_result==true)WebDr.clickX("X", "btnAuthorize", "Clicking on Autorize button", true);
			if (HtmlReporter.tc_result==true) WebDr.clickX("X", "btnAccept", "Clicking on btnAccept  field", true);
			if (HtmlReporter.tc_result)ROATModules.duefromcustomer("cashFCYtoLCY");
			if (HtmlReporter.tc_result==true) WebDr.clickX("X", "btnNext", "Clicking on btnNext  field", true);
			if (HtmlReporter.tc_result==true) WebDr.clickX("X", "btnCancel", "Clicking on btnCancel  field", true);
			if (HtmlReporter.tc_result==true)ROATFunctions.scrolltotop();






			 */
			System.out.println("DOne UI");

		}
		catch(Exception e)
		{
			Reporter.ReportEvent("Test Case execution is failed", "Test Case execution is failed","Test Case execution is failed", false);
		}


	}


	public static void sanity_cash_DrawerTeller_Inquiry()  {
		try {
			ROATFunctions.refesh(false, Launcher.UserRole);
			if (HtmlReporter.tc_result)ROATModules.CD_navigateTeller_Inquiry();
			if (HtmlReporter.tc_result)ROATModules.CD_Teller_InquiryView();
			if (HtmlReporter.tc_result)ROATModules.CD_Teller_InquiryBalanceCheck();
			if (HtmlReporter.tc_result)ROATModules.CD_Teller_InquiryDataSort();
		} catch (Exception e) {
			Reporter.ReportEvent("Test Case execution is failed", "Test Case execution is failed",
					"Test Case execution is failed", false);
		}
	}

	public static void Sanity_user_maintenance()
	{
		try
		{
			ROATFunctions.refesh(false, Launcher.UserRole);

			if (HtmlReporter.tc_result)ROATModules.Setup_navigateUser_Maintenance();

			if (HtmlReporter.tc_result)ROATModules.Setup_UserMaintenance_View();			

			//if (HtmlReporter.tc_result)ROATModules.Sanity_AddUser_UserMaintenance();	

		}
		catch (Exception e)
		{
			Reporter.ReportEvent("Test Case execution is failed", "Test Case execution is failed",
					"Test Case execution is failed", false);
		}

	}
	public static void Sanity_Packages_RatePackages() {
		try {

			ROATFunctions.refesh(false, Launcher.UserRole);
			if (HtmlReporter.tc_result)ROATModules.navigate_RatePakages();
			if (HtmlReporter.tc_result)ROATModules.AddNew_MarketSegment_CancelFunc();
			if (HtmlReporter.tc_result)ROATModules.PaginationValidation("Packages_RatePackages", "PaginationTop");
			if (HtmlReporter.tc_result)ROATModules.PaginationValidation("Packages_RatePackages", "PaginationBottom");

			if (HtmlReporter.tc_result)ROATModules.AddNew_MarketSegment_AuthFunc();
			/*if (HtmlReporter.tc_result)
						ROATModules.AddNew_MarkSeg_DupMsgFunc();*/
			if (HtmlReporter.tc_result)ROATModules.RatePackage_ViewAddNewValFunc();
			if (HtmlReporter.tc_result)ROATModules.AddNew_AcountType_AuthorizationFun();
			if (HtmlReporter.tc_result)ROATModules.RatePackage_ViewFunc();
			if (HtmlReporter.tc_result)ROATModules.RatePackage_Update_CancelFunc();
			if (HtmlReporter.tc_result)ROATModules.RatePackage_UpdateRejectFunc();
			if (HtmlReporter.tc_result)ROATModules.RatePackage_UpdateFunc();
			if (HtmlReporter.tc_result)ROATModules.RatePackage_ViewValidationFunc();

			if (HtmlReporter.tc_result)ROATModules.RatePackage_MarkUp_CancelFunc();
			if (HtmlReporter.tc_result)ROATModules.RatePackage_MarkUp_RejectionFunc();
			if (HtmlReporter.tc_result)ROATModules.RatePackage_MarkUpFunc();
			if (HtmlReporter.tc_result)ROATModules.RatePackage_ViewMarkUpFunc();
			if (HtmlReporter.tc_result)ROATModules.RatePackage_UpdateMarkUpFunc();
			if (HtmlReporter.tc_result)ROATModules.RatePackage_ViewMarkUpFunc();
			if (HtmlReporter.tc_result)ROATModules.RatePackage_DeleteMarkUpFunc();

			if (HtmlReporter.tc_result)ROATModules.RatePackage_DeleteFunc_No();
			if (HtmlReporter.tc_result)ROATModules.RatePackage_Delete_RejectionFunc();
			if (HtmlReporter.tc_result)ROATModules.RatePackage_DeleteFunc_Yes();
			if (HtmlReporter.tc_result == false) 
			{
				ROATFunctions.refesh(true,Launcher.UserRole);
			}
		} 
		catch (Exception e)
		{
			Reporter.ReportEvent("Test Case execution is failed", "Test Case execution is failed","Test Case execution is failed", false);
		}


	}

	public static void sod_eod_check() 
	{
		try
		{
			ROATFunctions.refesh(false,"Supervisor");
			String branch_no = ROATModules.fetch_user_branch();
			DatabaseFactory.update_CCP_OCP_Whole_Branch("N","Y",branch_no, CommonUtils.readConfig("Country_code"),Driver.Env_IP);
			if(HtmlReporter.tc_result==true)
			{
				WebDr.SetPageObjects("PeriodProcessing");
				for(int i=0;i<5;i++)
				{
					WebDr.Wait_Time(200);
					if(WebDr.isExists_Display("warning")>0)
					{
						if(HtmlReporter.tc_result==true)WebDr.click("X", "warning", "Clicking on OK button on warning pop-up", true);
						break;
					}
				}
			}
			if (HtmlReporter.tc_result)ROATModules.nav_periodprocessing();
			//			if (HtmlReporter.tc_result)ROATModules.preriodprocces_verification();
			if (HtmlReporter.tc_result)
			{
				boolean check_sod=ROATModules.Sod_check();
				if(check_sod)
				{
					if (HtmlReporter.tc_result)ROATModules.do_eod();
				}

				if (HtmlReporter.tc_result)
				{
					ROATModules.do_sod();
				}
				/*if (!HtmlReporter.tc_result)
				{
					ROATModules.do_sod_fail_safe();
				}*/

				DatabaseFactory.update_CCP_OCP_Whole_Branch("Y","N",branch_no, CommonUtils.readConfig("Country_code"),Driver.Env_IP);
			}
		}
		catch (Exception e)
		{
			Reporter.ReportEvent("Testcase execution failed", "End to end functionality failed signature add", "End to end functionality failed signature add", false);
			e.printStackTrace();

		}
	}
	public static void force_sod_eod() 
	{
		try
		{
			
			String branch_no = ROATModules.fetch_user_branch();
			DatabaseFactory.update_CCP_OCP_Whole_Branch("N","Y",branch_no, CommonUtils.readConfig("Country_code"),Driver.Env_IP);
			if(HtmlReporter.tc_result==true)
			{
				WebDr.SetPageObjects("PeriodProcessing");
				for(int i=0;i<5;i++)
				{
					WebDr.Wait_Time(200);
					if(WebDr.isExists_Display("warning")>0)
					{
						if(HtmlReporter.tc_result==true)WebDr.click("X", "warning", "Clicking on OK button on warning pop-up", true);
						break;
					}
				}
			}
			if (HtmlReporter.tc_result)ROATModules.nav_periodprocessing();
			//			if (HtmlReporter.tc_result)ROATModules.preriodprocces_verification();
			if (HtmlReporter.tc_result)
			{
				boolean check_sod=ROATModules.Sod_check();
				if(check_sod)
				{
					if (HtmlReporter.tc_result)ROATModules.do_eod();
				}

				if (HtmlReporter.tc_result)
				{
					ROATModules.do_sod();
				}
				/*if (!HtmlReporter.tc_result)
				{
					ROATModules.do_sod_fail_safe();
				}*/

				DatabaseFactory.update_CCP_OCP_Whole_Branch("Y","N",branch_no, CommonUtils.readConfig("Country_code"),Driver.Env_IP);
			}
		}
		catch (Exception e)
		{
			Reporter.ReportEvent("Testcase execution failed", "End to end functionality failed signature add", "End to end functionality failed signature add", false);
			e.printStackTrace();

		}
	}

	///**************************************************end to end******************************************************
	///**************************************************end to end******************************************************
	@SuppressWarnings({ "unchecked" })
	public static void E_E_Teller_transactions()
	{
		
		int lcy_range=88;
		int fcy_range=186;
		String foreign_currency="USD";
		String local_currency="GHS";
		String local_gl_num=WebDr.getValue("Local_cur_GL_Branch").split("-")[0]+"-"+WebDr.getValue("Local_cur_GL_no");
		String foreign_gl_num=WebDr.getValue("Forex_cur_GL_Branch").split("-")[0]+"-"+WebDr.getValue("Forex_cur_GL_no");
		String branch_name="";
		String den_value="1";
		int number_rows=99;
		List<String> conditions=new ArrayList<String>();
		HashMap<String, HashMap<String,Object>> auth_dual = new HashMap<String, HashMap<String,Object>>();
		HashMap<String, HashMap<String, String>> transaction_codes = new HashMap<String, HashMap<String, String>>();

		HashMap<String, BigDecimal> tt_inquiry_lcy = new HashMap<String, BigDecimal>();
		HashMap<String, BigDecimal> tt_inquiry_lcy_after = new HashMap<String, BigDecimal>();
		HashMap<String, BigDecimal> vault_inquiry_lcy = new HashMap<String, BigDecimal>();
		HashMap<String, BigDecimal> vault_inquiry_lcy_after = new HashMap<String, BigDecimal>();
		HashMap<String, BigDecimal> vault_inquiry_lcy_after_delete = new HashMap<String, BigDecimal>();
		HashMap<String, BigDecimal> vault_inquiry_lcy_after_delete_receipt = new HashMap<String, BigDecimal>();
		HashMap<String, BigDecimal> vault_inquiry_lcy_after_ATM = new HashMap<String, BigDecimal>();

		HashMap<String, BigDecimal> tt_inquiry_fcy = new HashMap<String, BigDecimal>();
		HashMap<String, BigDecimal> tt_inquiry_fcy_after = new HashMap<String, BigDecimal>();
		HashMap<String, BigDecimal> vault_inquiry_fcy = new HashMap<String, BigDecimal>();
		HashMap<String, BigDecimal> vault_inquiry_fcy_after = new HashMap<String, BigDecimal>();
		HashMap<String, BigDecimal> vault_inquiry_fcy_after_delete = new HashMap<String, BigDecimal>();

		HashMap<String, BigDecimal> cd_teller_inquiry = new HashMap<String, BigDecimal>();
		HashMap<String, BigDecimal> cd_teller_inquiry_after = new HashMap<String, BigDecimal>();
		try 
		{
			ROATFunctions.refesh(false,"Teller");
			branch_name=ROATModules.fetch_user_branch();
			lcy_range=ROATModules.LCY_Payment_amount("1",99);
			fcy_range=ROATModules.FCY_Payment_amount(foreign_currency,"1",99);
			auth_dual=ROATModules.authoriation_dual_control_inquiry(branch_name);
			transaction_codes=ROATModules.Transaction_Codes();
			System.out.println(transaction_codes);

		}
		catch (Exception e) { Reporter.ReportEvent("End to end ", "prerequistes inquiry fetching must be successfull","prerequistes inquiry fetching failed", true);
		e.printStackTrace();
		}
		//*************************************************************Vault---LCY---FCY Setups**********************************************
		Reporter.ReportEvent("End to end ", "Currency account Maintainence","Currency account Maintainence", true);
		try
		{


			ROATFunctions.refesh(false,"Super User");

			if (HtmlReporter.tc_result == true)	WebDr.SetPageObjects("Setup_curr_maint");

			if (HtmlReporter.tc_result == true)ROATModules.Setup_navigation_currency_maint();

			WebDr.SetPageObjects("Setup_curr_maint");
			if (HtmlReporter.tc_result) WebDr.setText("X", "enter_branch_name", branch_name, "Entering branch Name", true);
			if (HtmlReporter.tc_result) WebDr.setText("X", "enter_curency_name", WebDr.getValue("Local_currency"), "Entering currency Name", true);
			if (HtmlReporter.tc_result) WebDr.Wait_Time(1000);

			if (WebDr.isExists("update_icon")==1)
			{
				if (HtmlReporter.tc_result == true)ROATModules.setup_currency_update_accept(branch_name,"Local_cur_GL_Branch","Local_cur_GL_no");
			}

			else if (WebDr.isExists("empty_table")==1)
			{
				if (HtmlReporter.tc_result == true)ROATModules.setup_currency_add_new_accept(branch_name,true);
			}

			WebDr.SetPageObjects("Setup_curr_maint");
			if (HtmlReporter.tc_result) WebDr.setText("X", "enter_branch_name", branch_name, "Entering branch Name", true);
			if (HtmlReporter.tc_result) WebDr.setText("X", "enter_curency_name", WebDr.getValue("Forex_currency"), "Entering currency Name", true);
			if (HtmlReporter.tc_result) WebDr.Wait_Time(1000);

			if (WebDr.isExists("update_icon")==1)
			{
				if (HtmlReporter.tc_result == true)ROATModules.setup_currency_update_accept(branch_name,"Forex_cur_GL_Branch","Forex_cur_GL_no");
			}

			if (WebDr.isExists("empty_table")==1)
			{
				if (HtmlReporter.tc_result == true)ROATModules.setup_currency_add_new_accept(branch_name,false);
			}


		}
		catch(Exception ex)
		{
			ex.printStackTrace();
			Reporter.ReportEvent("End to end ", "Currency account Maintainence FAILED","Currency account Maintainence FAILED", false);
		}



		//*************************************************************Teller---LCY Receipt**********************************************
		Reporter.ReportEvent("End to end ", "Teller LCY receipt","Teller LCY receipt", true);
		HtmlReporter.tc_result=true;
		try
		{
			//Teller inquiry before transaction
			tt_inquiry_lcy=ROATModules.LCY_inquiry();
			if (HtmlReporter.tc_result)
			{
				System.out.println("tt_inquiry_lcy:"+tt_inquiry_lcy);

				//vault inquiry before transaction
				vault_inquiry_lcy=ROATModules.Vault_LCY_Inquiry();
				if (HtmlReporter.tc_result)
				{
					System.out.println("vault_inquiry_lcy:"+vault_inquiry_lcy);

					//Teller receipt 
					ROATModules.tt_receipt_lcy(lcy_range,"1",99,auth_dual);

					//teller cash drawer verification
					if (HtmlReporter.tc_result)
					{
						tt_inquiry_lcy_after=ROATModules.LCY_inquiry();
						System.out.println("tt_inquiry_lcy_after:"+tt_inquiry_lcy_after);
						conditions.add("Receipts");
						conditions.add("Balance");
						conditions.add("Denomination_balance");
						if (HtmlReporter.tc_result)
						{
							Reporter.ReportEvent("Teller LCY receipt", "Teller LCY receipt","Teller LCY receipt", true);
							ROATModules.teller_inquiry_check(tt_inquiry_lcy,tt_inquiry_lcy_after,conditions, lcy_range,"Addition");
							conditions.clear();

							//vault Inquiry verification
							vault_inquiry_lcy_after=ROATModules.Vault_LCY_Inquiry();
							System.out.println("vault_inquiry_lcy_after:"+vault_inquiry_lcy_after);
							conditions.add("Teller_Receipts");
							Reporter.ReportEvent("Teller LCY receipt", "Teller LCY receipt","Teller LCY receipt", true);
							ROATModules.teller_inquiry_check(vault_inquiry_lcy,vault_inquiry_lcy_after,conditions, lcy_range,"Addition");
							conditions.clear();
							conditions.add("Denomination_balance");
							conditions.add("balance");
							ROATModules.teller_inquiry_check(vault_inquiry_lcy,vault_inquiry_lcy_after,conditions, lcy_range,"Subtraction");
							conditions.clear();

						}
					}
				}
			}
		}
		catch(Exception e)
		{
			Reporter.ReportEvent("End to end ", "Teller LCY receipt Failed","Teller LCY receipt Failed", false);
			e.printStackTrace();
		}


		//*************************************************************Teller---FCY Receipt**********************************************
		Reporter.ReportEvent("End to end ", "Teller FCY receipt","Teller FCY receipt", true);
		HtmlReporter.tc_result=true;
		try{	
			HashMap<String,HashMap<String, BigDecimal>> inquiry_fcy = new HashMap<String,HashMap<String, BigDecimal>>();

			inquiry_fcy= ROATModules.FCY_Inquiry();
			Set<String> curreny_list=inquiry_fcy.keySet();
			String temp="";
			for(String set:curreny_list)
			{
				if(set.contains(foreign_currency))
					temp=set;
			}


			//Teller inquiry before transaction
			if (HtmlReporter.tc_result)
			{
				tt_inquiry_fcy=inquiry_fcy.get(temp);
				System.out.println("tt_inquiry_fcy:"+tt_inquiry_fcy);

				//vault inquiry before transaction
				inquiry_fcy=ROATModules.Vault_FCY_Inquiry();
				if (HtmlReporter.tc_result)
				{
					vault_inquiry_fcy=inquiry_fcy.get(temp);
					System.out.println("vault_inquiry_fcy:"+vault_inquiry_fcy);

					//Teller receipt 
					ROATModules.tt_receipt_fcy(foreign_currency,fcy_range,"1",99,auth_dual);

					//teller cash drawer verifications
					if (HtmlReporter.tc_result)
					{

						inquiry_fcy= ROATModules.FCY_Inquiry();
						tt_inquiry_fcy_after=inquiry_fcy.get(temp);
						System.out.println("tt_inquiry_fcy_after:"+tt_inquiry_fcy_after);
						conditions.add("Receipts");
						conditions.add("Balance");
						conditions.add("Denomination_balance");
						Reporter.ReportEvent("End to end ", "Teller FCY receipt","Teller FCY receipt", true);
						ROATModules.teller_inquiry_check(tt_inquiry_fcy,tt_inquiry_fcy_after,conditions, fcy_range,"Addition");
						conditions.clear();

						//vault Inquiry verification
						if (HtmlReporter.tc_result)
						{
							inquiry_fcy=ROATModules.Vault_FCY_Inquiry();
							if (HtmlReporter.tc_result)
							{
								vault_inquiry_fcy_after=inquiry_fcy.get(temp);
								System.out.println("vault_inquiry_fcy_after:"+vault_inquiry_fcy_after);
								conditions.add("Teller_Receipts");
								Reporter.ReportEvent("End to end ", "Teller FCY receipt","Teller FCY receipt", true);
								ROATModules.teller_inquiry_check(vault_inquiry_fcy,vault_inquiry_fcy_after,conditions, fcy_range,"Addition");
								conditions.clear();
								conditions.add("Denomination_balance");
								conditions.add("balance");
								ROATModules.teller_inquiry_check(vault_inquiry_fcy,vault_inquiry_fcy_after,conditions, fcy_range,"Subtraction");
								conditions.clear();
							}
						}
					}
				}
			}
		}
		catch(Exception ex)
		{
			ex.printStackTrace();
			Reporter.ReportEvent("Test Case execution is Failed", "Teller FCY receipt","Teller FCY receipt ffailed", false);
		}

		//*************************************************************Teller---LCY Payment**********************************************
		Reporter.ReportEvent("End to end ", "Teller LCY payment","Teller LCY payment", true);
		HtmlReporter.tc_result=true;
		try
		{
			//Teller inquiry before transaction
			tt_inquiry_lcy=ROATModules.LCY_inquiry();

			if (HtmlReporter.tc_result)
			{
				System.out.println("tt_inquiry_lcy:"+tt_inquiry_lcy);

				//vault inquiry before transaction
				if(HtmlReporter.tc_result)
				{
					vault_inquiry_lcy=ROATModules.Vault_LCY_Inquiry();
					System.out.println("vault_inquiry_lcy:"+vault_inquiry_lcy);

					//Teller receipt 
					if (HtmlReporter.tc_result)ROATModules.tt_payment_lcy(lcy_range,"1",99,auth_dual);

					//teller cash drawer verification
					tt_inquiry_lcy_after=ROATModules.LCY_inquiry();
					if (HtmlReporter.tc_result)
					{
						System.out.println("tt_inquiry_lcy_after:"+tt_inquiry_lcy_after);
						conditions.add("Payment");
						ROATModules.teller_inquiry_check(tt_inquiry_lcy,tt_inquiry_lcy_after,conditions, lcy_range,"Addition");
						conditions.clear();
						conditions.add("Balance");
						conditions.add("Denomination_balance");
						ROATModules.teller_inquiry_check(tt_inquiry_lcy,tt_inquiry_lcy_after,conditions, lcy_range,"Subtraction");
						conditions.clear();
					}

					//vault Inquiry verification
					if (HtmlReporter.tc_result)
					{
						vault_inquiry_lcy_after=ROATModules.Vault_LCY_Inquiry();
						{
							System.out.println("vault_inquiry_lcy_after:"+vault_inquiry_lcy_after);
							conditions.add("Teller_Payment");
							conditions.add("Denomination_balance");
							conditions.add("balance");
							ROATModules.teller_inquiry_check(vault_inquiry_lcy,vault_inquiry_lcy_after,conditions, lcy_range,"Addition");
							conditions.clear();
						}
					}
				}
			}
		}
		catch(Exception e)
		{
			e.printStackTrace();
			Reporter.ReportEvent("Test Case execution is Failed", "LCY payment transaction","LCY payment transaction failed", false);
		}

		//*************************************************************Teller---FCY payment**********************************************
		Reporter.ReportEvent("End to end ", "Teller FCY payment","Teller FCY payment", true);
		HtmlReporter.tc_result=true;
		try{	
			HashMap<String,HashMap<String, BigDecimal>> inquiry_fcy = new HashMap<String,HashMap<String, BigDecimal>>();

			inquiry_fcy= ROATModules.FCY_Inquiry();
			Set<String> curreny_list=inquiry_fcy.keySet();
			String temp="";
			for(String set:curreny_list)
			{
				if(set.contains(foreign_currency))
					temp=set;
			}


			//Teller inquiry before transaction
			if (HtmlReporter.tc_result)
			{
				tt_inquiry_fcy=inquiry_fcy.get(temp);
				System.out.println("tt_inquiry_fcy:"+tt_inquiry_fcy);

				//vault inquiry before transaction
				if (HtmlReporter.tc_result)
				{
					inquiry_fcy=ROATModules.Vault_FCY_Inquiry();
					if (HtmlReporter.tc_result)
					{
						vault_inquiry_fcy=inquiry_fcy.get(temp);
						System.out.println("vault_inquiry_fcy:"+vault_inquiry_fcy);

						//Teller receipt 
						if (HtmlReporter.tc_result)ROATModules.tt_payment_fcy(foreign_currency,fcy_range,"1",99,auth_dual);

						//teller cash drawer verifications
						if (HtmlReporter.tc_result){
							inquiry_fcy= ROATModules.FCY_Inquiry();
							if (HtmlReporter.tc_result)
							{
								tt_inquiry_fcy_after=inquiry_fcy.get(temp);
								if (HtmlReporter.tc_result)
								{
									System.out.println("tt_inquiry_fcy_after:"+tt_inquiry_fcy_after);
									conditions.add("Payment");
									ROATModules.teller_inquiry_check(tt_inquiry_fcy,tt_inquiry_fcy_after,conditions, fcy_range,"Addition");
									conditions.clear();
									conditions.add("Balance");
									conditions.add("Denomination_balance");
									if (HtmlReporter.tc_result)ROATModules.teller_inquiry_check(tt_inquiry_fcy,tt_inquiry_fcy_after,conditions, fcy_range,"Subtraction");
									conditions.clear();


									//vault Inquiry verification
									if (HtmlReporter.tc_result)
									{
										inquiry_fcy=ROATModules.Vault_FCY_Inquiry();
										if (HtmlReporter.tc_result)
										{
											vault_inquiry_fcy_after=inquiry_fcy.get(temp);
											if (HtmlReporter.tc_result)
											{
												System.out.println("vault_inquiry_fcy_after:"+vault_inquiry_fcy_after);
												conditions.add("Teller_Payment");
												conditions.add("Denomination_balance");
												conditions.add("balance");
												if (HtmlReporter.tc_result)ROATModules.teller_inquiry_check(vault_inquiry_fcy,vault_inquiry_fcy_after,conditions, fcy_range,"Addition");
												conditions.clear();
											}
										}
									}
								}
							}
						}
					}
				}
			}
		}
		catch(Exception ex)
		{
			ex.printStackTrace();
			Reporter.ReportEvent("Test Case execution is Failed", "Teller FCY payment","Teller FCY payment failed", false);
		}

		//*************************************************************Teller---LCY Transferout**********************************************
		Reporter.ReportEvent("End to end ", "Teller LCY Tranferout","Teller LCY Tranferout", true);
		HtmlReporter.tc_result=true;
		try
		{

			//Teller inquiry before transaction
			tt_inquiry_lcy=ROATModules.LCY_inquiry();

			if (HtmlReporter.tc_result)
			{
				System.out.println("tt_inquiry_lcy:"+tt_inquiry_lcy);

				//payee inquiry--before
				if (HtmlReporter.tc_result)
				{
					cd_teller_inquiry=ROATModules.CD_teller_inquiry(local_currency,WebDr.getValue("tranferout_id"));	



					//Teller transferout 
					if (HtmlReporter.tc_result)ROATModules.tt_tranfer_out_lcy(WebDr.getValue("tranferout_id"),lcy_range,"1",99,auth_dual);



					if (HtmlReporter.tc_result)
					{
						//Teller inquiry after transaction
						tt_inquiry_lcy_after=ROATModules.LCY_inquiry();
						System.out.println("tt_inquiry_lcy_after:"+tt_inquiry_lcy_after);
						conditions.add("Payment");
						ROATModules.teller_inquiry_check(tt_inquiry_lcy,tt_inquiry_lcy_after,conditions, lcy_range,"Addition");
						conditions.clear();
						conditions.add("Balance");
						conditions.add("Denomination_balance");
						ROATModules.teller_inquiry_check(tt_inquiry_lcy,tt_inquiry_lcy_after,conditions, lcy_range,"Subtraction");
						conditions.clear();

						if (HtmlReporter.tc_result)
						{
							System.out.println("tt_inquiry_lcy:"+tt_inquiry_lcy);

							//payee inquiry---after
							if (HtmlReporter.tc_result)
							{

								cd_teller_inquiry_after=ROATModules.CD_teller_inquiry(local_currency,WebDr.getValue("tranferout_id"));	

								conditions.add("Receipts");
								conditions.add("Balance");
								conditions.add("Denomination_balance");
								ROATModules.teller_inquiry_check(cd_teller_inquiry,cd_teller_inquiry_after,conditions, lcy_range,"Addition");
								conditions.clear();

							}
						}
					}	
				}
			}
		}
		catch(Exception ex)
		{
			ex.printStackTrace();
			Reporter.ReportEvent("Test Case execution is Failed", "end to end Transfer out","end to end Deposit executition failed", false);
		}


		//*************************************************************Teller---FCY Transferout**********************************************
		Reporter.ReportEvent("End to end ", "Teller FCY Tranferout","Teller FCY Tranferout", true);
		HtmlReporter.tc_result=true;
		try
		{
			HashMap<String,HashMap<String, BigDecimal>> inquiry_fcy = new HashMap<String,HashMap<String, BigDecimal>>();

			inquiry_fcy= ROATModules.FCY_Inquiry();
			Set<String> curreny_list=inquiry_fcy.keySet();
			String temp="";
			for(String set:curreny_list)
			{
				if(set.contains(foreign_currency))
					temp=set;
			}
			//Teller inquiry before transaction---FCY
			if (HtmlReporter.tc_result)
			{
				tt_inquiry_fcy=inquiry_fcy.get(temp);

				if (HtmlReporter.tc_result)
				{
					System.out.println("tt_inquiry_fcy:"+tt_inquiry_fcy);

					//payee inquiry--before
					if (HtmlReporter.tc_result)
					{
						cd_teller_inquiry=ROATModules.CD_teller_inquiry(foreign_currency,WebDr.getValue("tranferout_id"));	



						//Teller transferout 
						if (HtmlReporter.tc_result)ROATModules.tt_tranfer_out_fcy(WebDr.getValue("tranferout_id"),foreign_currency,fcy_range,"1",99,auth_dual);



						if (HtmlReporter.tc_result)
						{
							//Teller inquiry after transaction
							inquiry_fcy= ROATModules.FCY_Inquiry();
							tt_inquiry_fcy_after=inquiry_fcy.get(temp);
							if (HtmlReporter.tc_result)
							{
								System.out.println("tt_inquiry_fcy:"+tt_inquiry_fcy_after);
								conditions.add("Payment");
								ROATModules.teller_inquiry_check(tt_inquiry_fcy,tt_inquiry_fcy_after,conditions, fcy_range,"Addition");
								conditions.clear();
								conditions.add("Balance");
								conditions.add("Denomination_balance");
								ROATModules.teller_inquiry_check(tt_inquiry_fcy,tt_inquiry_fcy_after,conditions, fcy_range,"Subtraction");
								conditions.clear();


								//payee inquiry---after
								if (HtmlReporter.tc_result)
								{

									cd_teller_inquiry_after=ROATModules.CD_teller_inquiry(foreign_currency,WebDr.getValue("tranferout_id"));	

									conditions.add("Receipts");
									conditions.add("Balance");
									conditions.add("Denomination_balance");
									ROATModules.teller_inquiry_check(cd_teller_inquiry,cd_teller_inquiry_after,conditions, fcy_range,"Addition");
									conditions.clear();

								}
							}
						}	
					}
				}
			}
		}
		catch(Exception ex)
		{
			ex.printStackTrace();
			Reporter.ReportEvent("Test Case execution is Failed", "end to end Transfer out","end to end Transfer out executition failed", false);
		}




		//*************************************************************Vault---LCY--payment to cdc**********************************************
		Reporter.ReportEvent("End to end ", "vault lcy payment","vault lcy payment", true);
		HtmlReporter.tc_result=true;
		try
		{
			ROATModules.vault_cdc_payment_lcy(local_gl_num.split("-")[0],local_gl_num.split("-")[1],lcy_range,den_value,number_rows,auth_dual);
			//vault inquiry before transaction
			if(HtmlReporter.tc_result)
			{
				vault_inquiry_lcy=ROATModules.Vault_LCY_Inquiry();

				if(HtmlReporter.tc_result)
				{
					System.out.println("vault_inquiry_lcy:"+vault_inquiry_lcy);

					int pre_trans_number =ROATModules.support_function( true, "lcy", 0,"Payment To CDC",local_gl_num,local_gl_num,String.valueOf(lcy_range), true);
					System.out.println(pre_trans_number);

					ROATModules.vault_cdc_payment_lcy(local_gl_num.split("-")[0],local_gl_num.split("-")[1],lcy_range,den_value,number_rows,auth_dual);


					//vault Inquiry verification
					if (HtmlReporter.tc_result)
					{
						vault_inquiry_lcy_after=ROATModules.Vault_LCY_Inquiry();
						{
							System.out.println("vault_inquiry_lcy_after:"+vault_inquiry_lcy_after);
							conditions.add("CDC_Payments");
							ROATModules.teller_inquiry_check(vault_inquiry_lcy,vault_inquiry_lcy_after,conditions, lcy_range,"Addition");
							conditions.clear();
							conditions.add("Denomination_balance");
							conditions.add("balance");
							ROATModules.teller_inquiry_check(vault_inquiry_lcy,vault_inquiry_lcy_after,conditions, lcy_range,"Subtraction");
							conditions.clear();

						}

					}
					pre_trans_number =ROATModules.support_function( false, "lcy", pre_trans_number,"Payment To CDC",local_gl_num,local_gl_num,String.valueOf(lcy_range), true);
					System.out.println(pre_trans_number);

					if (HtmlReporter.tc_result)
					{
						vault_inquiry_lcy_after_delete=ROATModules.Vault_LCY_Inquiry();
						{
							System.out.println("vault_inquiry_lcy_after:"+vault_inquiry_lcy_after_delete);
							conditions.add("CDC_Payments");
							ROATModules.teller_inquiry_check(vault_inquiry_lcy_after,vault_inquiry_lcy_after_delete,conditions, lcy_range,"Subtraction");
							conditions.clear();
							conditions.add("Denomination_balance");
							conditions.add("balance");
							ROATModules.teller_inquiry_check(vault_inquiry_lcy_after,vault_inquiry_lcy_after_delete,conditions, lcy_range,"Addition");
							conditions.clear();

						}
					}
				}
			}
		}
		catch(Exception ex)
		{
			ex.printStackTrace();
			Reporter.ReportEvent("End to end ", "vault lcy payment FAILED","vault lcy payment FAILED", false);
		}

		//*************************************************************Vault---FCY--payment to cdc**********************************************
		Reporter.ReportEvent("End to end ", "vault lcy payment","vault lcy payment", true);
		HtmlReporter.tc_result=true;
		try
		{

//			ROATModules.vault_cdc_payment_fcy(foreign_currency,foreign_gl_num.split("-")[0],foreign_gl_num.split("-")[1],fcy_range,den_value,number_rows,auth_dual);
			HashMap<String,HashMap<String, BigDecimal>> inquiry_fcy = new HashMap<String,HashMap<String, BigDecimal>>();

			inquiry_fcy= ROATModules.Vault_FCY_Inquiry();
			Set<String> curreny_list=inquiry_fcy.keySet();
			String temp="";
			for(String set:curreny_list)
			{
				if(set.contains(foreign_currency))
					temp=set;
			}

			//vault inquiry before transaction
			if (HtmlReporter.tc_result)
			{
				vault_inquiry_fcy=inquiry_fcy.get(temp);
				System.out.println("vault_inquiry_fcy:"+vault_inquiry_fcy);

				int pre_trans_number =ROATModules.support_function( true, "fcy", 0,"Payment To CDC",foreign_gl_num,foreign_gl_num,String.valueOf(fcy_range), true);
				System.out.println(pre_trans_number);

				ROATModules.vault_cdc_payment_fcy(foreign_currency,foreign_gl_num.split("-")[0],foreign_gl_num.split("-")[1],fcy_range,den_value,number_rows,auth_dual);


				//vault Inquiry verification
				if (HtmlReporter.tc_result)
				{
					inquiry_fcy=ROATModules.Vault_FCY_Inquiry();
					if (HtmlReporter.tc_result)
					{
						vault_inquiry_fcy_after=inquiry_fcy.get(temp);
						System.out.println("vault_inquiry_fcy_after:"+vault_inquiry_fcy_after);

						conditions.add("CDC_Payments");
						ROATModules.teller_inquiry_check(vault_inquiry_fcy,vault_inquiry_fcy_after,conditions, fcy_range,"Addition");
						conditions.clear();
						conditions.add("Denomination_balance");
						conditions.add("balance");
						ROATModules.teller_inquiry_check(vault_inquiry_fcy,vault_inquiry_fcy_after,conditions, fcy_range,"Subtraction");
						conditions.clear();

					}
				}

				pre_trans_number =ROATModules.support_function( false, "fcy", pre_trans_number,"Payment To CDC",foreign_gl_num,foreign_gl_num,String.valueOf(fcy_range), true);
				System.out.println(pre_trans_number);


				if (HtmlReporter.tc_result)
				{
					inquiry_fcy=ROATModules.Vault_FCY_Inquiry();
					if (HtmlReporter.tc_result)
					{
						vault_inquiry_fcy_after_delete=inquiry_fcy.get(temp);
						System.out.println("vault_inquiry_fcy_after:"+vault_inquiry_fcy_after_delete);

						conditions.add("CDC_Payments");
						ROATModules.teller_inquiry_check(vault_inquiry_fcy_after,vault_inquiry_fcy_after_delete,conditions, fcy_range,"Subtraction");
						conditions.clear();
						conditions.add("Denomination_balance");
						conditions.add("balance");
						ROATModules.teller_inquiry_check(vault_inquiry_fcy_after,vault_inquiry_fcy_after_delete,conditions, fcy_range,"Addition");
						conditions.clear();

					}
				}

			}
		}
		catch(Exception ex)
		{
			ex.printStackTrace();
			Reporter.ReportEvent("End to end ", "vault lcy payment FAILED","vault lcy payment FAILED", false);
		}


		//*************************************************************Vault---LCY--Receipt from cdc**********************************************
		Reporter.ReportEvent("End to end ", "vault lcy Receipt from cdc","vault lcy Receipt from cdc", true);
		HtmlReporter.tc_result=true;
		try
		{
//			ROATModules.vault_cdc_receipt_lcy(local_gl_num.split("-")[0],local_gl_num.split("-")[1],lcy_range,den_value,number_rows,auth_dual);
			//vault inquiry before transaction
			if(HtmlReporter.tc_result)
			{
				vault_inquiry_lcy=ROATModules.Vault_LCY_Inquiry();

				if(HtmlReporter.tc_result)
				{
					System.out.println("vault_inquiry_lcy:"+vault_inquiry_lcy);

					int pre_trans_number =ROATModules.support_function( true, "lcy", 0,"Receipt From CDC",local_gl_num,local_gl_num,String.valueOf(lcy_range), true);
					System.out.println(pre_trans_number);

					ROATModules.vault_cdc_receipt_lcy(local_gl_num.split("-")[0],local_gl_num.split("-")[1],lcy_range,den_value,number_rows,auth_dual);


					//vault Inquiry verification
					if (HtmlReporter.tc_result)
					{
						vault_inquiry_lcy_after=ROATModules.Vault_LCY_Inquiry();
						{
							System.out.println("vault_inquiry_lcy_after:"+vault_inquiry_lcy_after);
							conditions.add("CDC_Receipts");
							conditions.add("Denomination_balance");
							conditions.add("balance");
							ROATModules.teller_inquiry_check(vault_inquiry_lcy,vault_inquiry_lcy_after,conditions, lcy_range,"Addition");
							conditions.clear();

						}
					}

					pre_trans_number =ROATModules.support_function( false, "lcy", pre_trans_number,"Receipt From CDC",local_gl_num,local_gl_num,String.valueOf(lcy_range), true);
					System.out.println(pre_trans_number);

					if (HtmlReporter.tc_result)
					{
						vault_inquiry_lcy_after_delete_receipt=ROATModules.Vault_LCY_Inquiry();
						{
							System.out.println("vault_inquiry_lcy_after:"+vault_inquiry_lcy_after_delete_receipt);
							conditions.add("CDC_Receipts");
							conditions.add("Denomination_balance");
							conditions.add("balance");
							ROATModules.teller_inquiry_check(vault_inquiry_lcy_after,vault_inquiry_lcy_after_delete_receipt,conditions, lcy_range,"Subtraction");
							conditions.clear();

						}
					}
				}
			}
		}
		catch(Exception ex)
		{
			ex.printStackTrace();
			Reporter.ReportEvent("End to end ", "vault lcy Receipt from cdc FAILED","vault lcy Receipt from cdc FAILED", false);
		}

		//*************************************************************Vault---FCY--Receipt from cdc**********************************************
		Reporter.ReportEvent("End to end ", "vault fcy Receipt from cdc","vault lcy Receipt from cdc ", true);
		HtmlReporter.tc_result=true;
		try
		{

//			ROATModules.vault_cdc_receipt_fcy(foreign_currency,foreign_gl_num.split("-")[0],foreign_gl_num.split("-")[1],fcy_range,den_value,number_rows,auth_dual);
			HashMap<String,HashMap<String, BigDecimal>> inquiry_fcy = new HashMap<String,HashMap<String, BigDecimal>>();

			inquiry_fcy= ROATModules.Vault_FCY_Inquiry();
			Set<String> curreny_list=inquiry_fcy.keySet();
			String temp="";
			for(String set:curreny_list)
			{
				if(set.contains(foreign_currency))
					temp=set;
			}

			//vault inquiry before transaction
			if (HtmlReporter.tc_result)
			{
				vault_inquiry_fcy=inquiry_fcy.get(temp);
				System.out.println("vault_inquiry_fcy:"+vault_inquiry_fcy);


				int pre_trans_number =ROATModules.support_function( true, "fcy", 0,"Receipt From CDC",foreign_gl_num,foreign_gl_num,String.valueOf(fcy_range), true);
				System.out.println(pre_trans_number);

				ROATModules.vault_cdc_receipt_fcy(foreign_currency,foreign_gl_num.split("-")[0],foreign_gl_num.split("-")[1],fcy_range,den_value,number_rows,auth_dual);


				//vault Inquiry verification
				if (HtmlReporter.tc_result)
				{
					inquiry_fcy=ROATModules.Vault_FCY_Inquiry();
					if (HtmlReporter.tc_result)
					{
						vault_inquiry_fcy_after=inquiry_fcy.get(temp);
						System.out.println("vault_inquiry_fcy_after:"+vault_inquiry_fcy_after);

						conditions.add("CDC_Receipts");
						conditions.add("Denomination_balance");
						conditions.add("balance");
						ROATModules.teller_inquiry_check(vault_inquiry_fcy,vault_inquiry_fcy_after,conditions, fcy_range,"Addition");
						conditions.clear();


					}
				}
				pre_trans_number =ROATModules.support_function( false, "fcy", pre_trans_number,"Receipt From CDC",foreign_gl_num,foreign_gl_num,String.valueOf(fcy_range), true);
				System.out.println(pre_trans_number);
				if (HtmlReporter.tc_result)
				{
					inquiry_fcy=ROATModules.Vault_FCY_Inquiry();
					if (HtmlReporter.tc_result)
					{
						vault_inquiry_fcy_after=inquiry_fcy.get(temp);
						System.out.println("vault_inquiry_fcy_after:"+vault_inquiry_fcy_after);

						conditions.add("CDC_Receipts");
						conditions.add("Denomination_balance");
						conditions.add("balance");
						ROATModules.teller_inquiry_check(vault_inquiry_fcy,vault_inquiry_fcy_after,conditions, fcy_range,"Subtraction");
						conditions.clear();


					}
				}
			}
		}
		catch(Exception ex)
		{
			ex.printStackTrace();
			Reporter.ReportEvent("End to end ", "vault fcy Receipt from cdc","vault lcy Receipt from cdc ", false);
		}

		//*************************************************************Vault---LCY--payment to ATM**********************************************
		Reporter.ReportEvent("End to end ", "LCY--payment to ATM","LCY--payment to ATM", true);
		HtmlReporter.tc_result=true;
		try
		{   
			String atm_gl_num=WebDr.getValue("Payment_ATM_GL_No");
//			ROATModules.atm_cdc_payment_lcy(atm_gl_num.split("-")[0],atm_gl_num.split("-")[1],lcy_range,den_value,number_rows,auth_dual,transaction_codes);
			//vault inquiry before transaction
			if(HtmlReporter.tc_result)
			{
				vault_inquiry_lcy=ROATModules.Vault_LCY_Inquiry();

				if(HtmlReporter.tc_result)
				{
					System.out.println("vault_inquiry_lcy:"+vault_inquiry_lcy);
					//String atm_gl_num=WebDr.getValue("Payment_ATM_GL_No");

					int pre_trans_number =ROATModules.support_function( true, "lcy", 0,"Payment To ATM",atm_gl_num,atm_gl_num,String.valueOf(lcy_range), true);
					System.out.println(pre_trans_number);

					ROATModules.atm_cdc_payment_lcy(atm_gl_num.split("-")[0],atm_gl_num.split("-")[1],lcy_range,den_value,number_rows,auth_dual,transaction_codes);


					//vault Inquiry verification
					if (HtmlReporter.tc_result)
					{
						vault_inquiry_lcy_after=ROATModules.Vault_LCY_Inquiry();
						{
							System.out.println("vault_inquiry_lcy_after:"+vault_inquiry_lcy_after);
							conditions.add("CDC_Payments");
							ROATModules.teller_inquiry_check(vault_inquiry_lcy,vault_inquiry_lcy_after,conditions, lcy_range,"Addition");
							conditions.clear();
							conditions.add("Denomination_balance");
							conditions.add("balance");
							ROATModules.teller_inquiry_check(vault_inquiry_lcy,vault_inquiry_lcy_after,conditions, lcy_range,"Subtraction");
							conditions.clear();


						}
					}
					pre_trans_number =ROATModules.support_function( false, "lcy", pre_trans_number,"Payment To ATM",atm_gl_num,atm_gl_num,String.valueOf(lcy_range), true);
					System.out.println(pre_trans_number);

					if (HtmlReporter.tc_result)
					{
						vault_inquiry_lcy_after_ATM=ROATModules.Vault_LCY_Inquiry();
						{
							System.out.println("vault_inquiry_lcy_after:"+vault_inquiry_lcy_after_ATM);
							conditions.add("CDC_Payments");
							ROATModules.teller_inquiry_check(vault_inquiry_lcy_after,vault_inquiry_lcy_after_ATM,conditions, lcy_range,"Addition");
							conditions.clear();
							conditions.add("Denomination_balance");
							conditions.add("balance");
							ROATModules.teller_inquiry_check(vault_inquiry_lcy_after,vault_inquiry_lcy_after_ATM,conditions, lcy_range,"Subtraction");
							conditions.clear();


						}
					}
				}
			}
		}
		catch(Exception ex)
		{
			ex.printStackTrace();
			Reporter.ReportEvent("End to end ", "LCY--payment to ATM","LCY--payment to ATM FAILED", false);
		}
		//*************************************************************Vault---Vault LCY--change**********************************************
		Reporter.ReportEvent("End to end ", "Vault LCY--change","Vault LCY--change", true);
		HtmlReporter.tc_result=true;
		try
		{
			//vault inquiry before transaction
			if(HtmlReporter.tc_result)
			{
				vault_inquiry_lcy=ROATModules.Vault_LCY_Inquiry();

				if(HtmlReporter.tc_result)
				{
					System.out.println("vault_inquiry_lcy:"+vault_inquiry_lcy);

					ROATModules.vault_cdc_change_lcy(WebDr.getValue("Change_in_amt_lcy"));


					//vault Inquiry verification
					if (HtmlReporter.tc_result)
					{
						vault_inquiry_lcy_after=ROATModules.Vault_LCY_Inquiry();
						{
							System.out.println("vault_inquiry_lcy_after:"+vault_inquiry_lcy_after);
							conditions.add("CDC_Payments");
							conditions.clear();
							conditions.add("Denomination_balance");
							conditions.add("balance");
							ROATModules.teller_inquiry_check(vault_inquiry_lcy,vault_inquiry_lcy_after,conditions, 0,"Subtraction");
							conditions.clear();


						}
					}
				}
			}
		}
		catch(Exception ex)
		{
			ex.printStackTrace();
			Reporter.ReportEvent("End to end ", "Vault LCY--change","Vault LCY--change FAILED", false);
		}

		//*************************************************************Vault---Vault fCY--change**********************************************
		Reporter.ReportEvent("End to end ", "Vault LCY--change","Vault LCY--change", true);
		HtmlReporter.tc_result=true;
		try
		{
			HashMap<String,HashMap<String, BigDecimal>> inquiry_fcy = new HashMap<String,HashMap<String, BigDecimal>>();

			inquiry_fcy= ROATModules.Vault_FCY_Inquiry();
			Set<String> curreny_list=inquiry_fcy.keySet();
			String temp="";
			for(String set:curreny_list)
			{
				if(set.contains(foreign_currency))
					temp=set;
			}

			//vault inquiry before transaction
			if (HtmlReporter.tc_result)
			{
				vault_inquiry_fcy=inquiry_fcy.get(temp);
				System.out.println("vault_inquiry_fcy:"+vault_inquiry_fcy);


				ROATModules.vault_cdc_change_fcy(WebDr.getValue("Change_in_amt_fcy"));

				//vault Inquiry verification
				if (HtmlReporter.tc_result)
				{
					inquiry_fcy=ROATModules.Vault_FCY_Inquiry();
					if (HtmlReporter.tc_result)
					{
						vault_inquiry_fcy_after=inquiry_fcy.get(temp);
						System.out.println("vault_inquiry_fcy_after:"+vault_inquiry_fcy_after);
						conditions.add("CDC_Payments");
						conditions.clear();
						conditions.add("Denomination_balance");
						conditions.add("balance");
						ROATModules.teller_inquiry_check(vault_inquiry_fcy,vault_inquiry_fcy_after,conditions, 0,"Subtraction");
						conditions.clear();



					}
				}
			}
		}

		catch(Exception ex)
		{
			ex.printStackTrace();
			Reporter.ReportEvent("End to end ", "Vault FCY--change","Vault FCY--change FAILED", false);
		}

		try
		{
			ROATModules.sod_eod_end_to_end();	
		}
		catch(Exception e)
		{
			e.printStackTrace();
			Reporter.ReportEvent("performing sod eod ", "sod eod must be succesful","sod eod failed for deposits", false);
		}
	}
	
	//*****************************************end to end deposits******************************************************
	//Author :Akshay Gopu
	@SuppressWarnings("unchecked")
	public static void end_to_end_deposit()
	{
		if(HtmlReporter.tc_result)
		{
			try
			{
				int lcy_range=50;
				int fcy_range=100;
				int midrate=1;
				String currency=WebDr.getValue("ETE_FCY_Account_no").split("\\|")[0];
				String user_branch ="";
				String user_id="";
				String user_name = "";
				String Branch="";
				String local_gl__number=WebDr.getValue("LCY_GL_no");
				String forex_gl__number=WebDr.getValue("FCY_GL_No");
				HashMap<String, ArrayList<String>> limits_map = new HashMap<String, ArrayList<String>>();
				HashMap<String, String> CreditRisk = new HashMap<String, String>();
				HashMap<String, Map<String,String>> store_matrix = new HashMap<String, Map<String,String>>();
				HashMap<String, Map<String,String>> store_forex = new HashMap<String, Map<String,String>>();

				ROATFunctions.refesh(false,"Teller");
				if (HtmlReporter.tc_result)
				{ 
					user_branch = ROATModules.fetch_user_branch();
					user_id=ROATFunctions.fetch_user_Id();
					user_name = ROATModules.fetchuser();
					Branch=ROATFunctions.fetch_user_branch_name();
					lcy_range=ROATModules.LCY_Payment_amount("1",1);
					fcy_range=ROATModules.FCY_Payment_amount(currency,"1",1);


					// Fetching forex rate 
					ROATFunctions.refesh(false, "Central Supervisor");
					WebDr.SetPageObjects("end_end");
					store_forex = ROATModules.set_forexrate(currency);

					// Fetching teller matrix 
					ROATFunctions.refesh(false, "Super User");
					if (HtmlReporter.tc_result)ROATModules.setup_TellerMatrixNavigation();
					store_matrix=ROATModules.fetch_teller_matrix(Branch,WebDr.getValue("default_branch"));

					//Fetching Authorization remote
					if (HtmlReporter.tc_result)
					{
						//CreditRisk=ROATModules.auto_remo_update(Branch);	
						if (HtmlReporter.tc_result)ROATModules.setup_navigateLTLimits();


						//******************************Breaches verification************************************


						// LCY Transaction with Limits set as 'Send for authorization' 
						if (HtmlReporter.tc_result)
						{ 
							//LCY Limit breach------mid rate=1
							midrate=1;
							String action="Send for Authorization";
							if (HtmlReporter.tc_result)
							{
								limits_map=ROATModules.fetch_limits_ALL(user_branch, user_id+"-"+user_name,lcy_range,action);
							}


							ROATFunctions.refesh(false, "Teller");
							if (HtmlReporter.tc_result)ROATModules.Teller_navigateCashDeposit();
							if (HtmlReporter.tc_result)ROATModules.Teller_cash_deposit_ete(limits_map,CreditRisk,store_matrix,lcy_range,action,midrate);

						}




						// test cases flag true
						HtmlReporter.tc_result=true; 

						// LCY Transaction with Limits set as 'Decline'
						if (HtmlReporter.tc_result)
						{
							//LCY Limit breach------mid rate=1
							midrate=1;
							ROATFunctions.refesh(false, "Super User");
							String action="Decline";
							if (HtmlReporter.tc_result)ROATModules.setup_navigateLTLimits();
							if (HtmlReporter.tc_result == true)
							{
								limits_map=ROATModules.fetch_limits_ALL(user_branch, user_id+"-"+user_name,lcy_range,action);

							}


							ROATFunctions.refesh(false, "Teller");
							if (HtmlReporter.tc_result == true)ROATModules.Teller_navigateCashDeposit();
							if (HtmlReporter.tc_result == true)ROATModules.Teller_cash_deposit_ete(limits_map,CreditRisk,store_matrix,lcy_range,action,midrate);

						}




						// test cases flag true
						HtmlReporter.tc_result=true; 

						// FCY Transaction with Limits set as Send for authorization
						if (HtmlReporter.tc_result)
						{
							midrate=Integer.parseInt(store_forex.get("Mid Rate").get("Mid Rate"));
							ROATFunctions.refesh(false, "Super User");
							String action="Send for Authorization";
							if (HtmlReporter.tc_result)ROATModules.setup_navigateLTLimits();
							if (HtmlReporter.tc_result == true)
							{
								limits_map=ROATModules.fetch_limits_ALL(user_branch, user_id+"-"+user_name,fcy_range*midrate,action);
							}

							//FCY Limit breach------mid rate

							ROATFunctions.refesh(false, "Teller");
							if (HtmlReporter.tc_result == true)ROATModules.Teller_navigateCashDeposit();
							if (HtmlReporter.tc_result == true)ROATModules.Teller_cash_deposit_ete(limits_map,CreditRisk,store_matrix,fcy_range,action,midrate);

						}

					}
					System.out.println("store_matrix");
					System.out.println(store_matrix);
					System.out.println();
					System.out.println("credit Risk");
					System.out.println(CreditRisk);
				}




				// test cases flag true
				HtmlReporter.tc_result=true; 




				//	***********************************************end to end******************************
				//end to end---LCY---Transaction--without Breach 
				HtmlReporter.tc_result=true;
				ROATFunctions.refesh(false,"Super User");
				String	action="Send for Authorization";
				midrate=Integer.parseInt(store_forex.get("Mid Rate").get("Mid Rate"));
				if (HtmlReporter.tc_result)ROATModules.setup_navigateLTLimits();
				limits_map=ROATModules.fetch_limits_ALL(user_branch, user_id+"-"+user_name,fcy_range*midrate*2,action);
				
				
				
				HtmlReporter.tc_result=true;
				midrate=1;
				ROATModules.end_to_end_deposit(lcy_range,midrate,local_gl__number);


				//************************************support functions****************************


				//delete transaction from 'support function'--- local currency
				HtmlReporter.tc_result=true;
				midrate=1;
				ROATModules.end_to_end_deposit_support_fuction(lcy_range,midrate,local_gl__number);

				//delete transaction from 'support function'--- foreign currency 
				HtmlReporter.tc_result=true;
				midrate=Integer.parseInt(store_forex.get("Mid Rate").get("Mid Rate"));
				ROATModules.end_to_end_deposit_support_fuction(fcy_range,midrate,forex_gl__number);


				//************************************Deposit with cashback****************************
				HtmlReporter.tc_result=true;
				ROATModules.end_end_deposit_cashback();


				HtmlReporter.tc_result=true;
				//sod eod vault close,teller close
				try
				{
					ROATModules.sod_eod_end_to_end();	
				}
				catch(Exception e)
				{
					e.printStackTrace();
					Reporter.ReportEvent("performing sod eod ", "sod eod must be succesful","sod eod failed for deposits", false);
				}
			}


			catch(Exception ex)
			{
				ex.printStackTrace();
				Reporter.ReportEvent("Test Case execution is Failed", "end to end Deposit","end to end Deposit executition failed", false);
			}


		}
		else
		{
			Reporter.ReportEvent("performing sod", "sod must be successfull","sod failed", false);
		}
	}
	
	@SuppressWarnings("unchecked")
	public static void end_to_end_deposit_breaches_lcy()
	{
		if(HtmlReporter.tc_result)
		{
			try
			{
				int lcy_range=50;
				int fcy_range=100;
				int midrate=1;
				String currency=WebDr.getValue("ETE_FCY_Account_no").split("\\|")[0];
				String user_branch ="";
				String user_id="";
				String user_name = "";
				String Branch="";
				String local_gl__number=WebDr.getValue("LCY_GL_no");
				String forex_gl__number=WebDr.getValue("FCY_GL_No");
				HashMap<String, ArrayList<String>> limits_map = new HashMap<String, ArrayList<String>>();
				HashMap<String, String> CreditRisk = new HashMap<String, String>();
				HashMap<String, Map<String,String>> store_matrix = new HashMap<String, Map<String,String>>();
				HashMap<String, Map<String,String>> store_forex = new HashMap<String, Map<String,String>>();

				ROATFunctions.refesh(false,"Teller");
				if (HtmlReporter.tc_result)
				{ 
					user_branch = ROATModules.fetch_user_branch();
					user_id=ROATFunctions.fetch_user_Id();
					user_name = ROATModules.fetchuser();
					Branch=ROATFunctions.fetch_user_branch_name();
					lcy_range=ROATModules.LCY_Payment_amount("1",1);
					fcy_range=ROATModules.FCY_Payment_amount(currency,"1",1);


					// Fetching forex rate 
					ROATFunctions.refesh(false, "Central Supervisor");
					WebDr.SetPageObjects("end_end");
					store_forex = ROATModules.set_forexrate(currency);

					// Fetching teller matrix 
					ROATFunctions.refesh(false, "Super User");
					if (HtmlReporter.tc_result)ROATModules.setup_TellerMatrixNavigation();
					store_matrix=ROATModules.fetch_teller_matrix(Branch,WebDr.getValue("default_branch"));

					//Fetching Authorization remote
					if (HtmlReporter.tc_result)
					{
						//CreditRisk=ROATModules.auto_remo_update(Branch);	


						//******************************Breaches verification************************************


						// LCY Transaction with Limits set as 'Send for authorization' 
						if (HtmlReporter.tc_result)
						{ 
							//LCY Limit breach------mid rate=1
							midrate=1;
							String action="Send for Authorization";
							if (HtmlReporter.tc_result)
							{
								if (HtmlReporter.tc_result)ROATModules.setup_navigateLTLimits();
								limits_map=ROATModules.fetch_limits_ALL(user_branch, user_id+"-"+user_name,lcy_range,action);
							}


							ROATFunctions.refesh(false, "Teller");
							if (HtmlReporter.tc_result)ROATModules.Teller_navigateCashDeposit();
							if (HtmlReporter.tc_result)ROATModules.Teller_cash_deposit_ete(limits_map,CreditRisk,store_matrix,lcy_range,action,midrate);

						}

					}

				}
			}
			catch(Exception e)
			{
				Reporter.ReportEvent("Test Case execution is Failed", "end to end Deposit","end to end Deposit executition failed", false);
			}
		}
	}
	@SuppressWarnings("unchecked")
	public static void end_to_end_deposit_breaches_fcy()
	{
		if(HtmlReporter.tc_result)
		{
			try
			{
				int lcy_range=50;
				int fcy_range=100;
				int midrate=1;
				String currency=WebDr.getValue("ETE_FCY_Account_no").split("\\|")[0];
				String user_branch ="";
				String user_id="";
				String user_name = "";
				String Branch="";
				String local_gl__number=WebDr.getValue("LCY_GL_no");
				String forex_gl__number=WebDr.getValue("FCY_GL_No");
				HashMap<String, ArrayList<String>> limits_map = new HashMap<String, ArrayList<String>>();
				HashMap<String, String> CreditRisk = new HashMap<String, String>();
				HashMap<String, Map<String,String>> store_matrix = new HashMap<String, Map<String,String>>();
				HashMap<String, Map<String,String>> store_forex = new HashMap<String, Map<String,String>>();

				ROATFunctions.refesh(false,"Teller");
				if (HtmlReporter.tc_result)
				{ 
					user_branch = ROATModules.fetch_user_branch();
					user_id=ROATFunctions.fetch_user_Id();
					user_name = ROATModules.fetchuser();
					Branch=ROATFunctions.fetch_user_branch_name();
					lcy_range=ROATModules.LCY_Payment_amount("1",1);
					fcy_range=ROATModules.FCY_Payment_amount(currency,"1",1);


					// Fetching forex rate 
					ROATFunctions.refesh(false, "Central Supervisor");
					WebDr.SetPageObjects("end_end");
					store_forex = ROATModules.set_forexrate(currency);

					// Fetching teller matrix 
					ROATFunctions.refesh(false, "Super User");
					if (HtmlReporter.tc_result)ROATModules.setup_TellerMatrixNavigation();
					store_matrix=ROATModules.fetch_teller_matrix(Branch,WebDr.getValue("default_branch"));

					//Fetching Authorization remote
					if (HtmlReporter.tc_result)
					{
						//CreditRisk=ROATModules.auto_remo_update(Branch);	


						//******************************Breaches verification************************************



						// test cases flag true
						HtmlReporter.tc_result=true; 

						// FCY Transaction with Limits set as Send for authorization
						if (HtmlReporter.tc_result)
						{
							midrate=Integer.parseInt(store_forex.get("Mid Rate").get("Mid Rate"));
							ROATFunctions.refesh(false, "Super User");
							String action="Send for Authorization";
							if (HtmlReporter.tc_result)ROATModules.setup_navigateLTLimits();
							if (HtmlReporter.tc_result == true)
							{
								limits_map=ROATModules.fetch_limits_ALL(user_branch, user_id+"-"+user_name,fcy_range*midrate,action);
							}

							//FCY Limit breach------mid rate

							ROATFunctions.refesh(false, "Teller");
							if (HtmlReporter.tc_result == true)ROATModules.Teller_navigateCashDeposit();
							if (HtmlReporter.tc_result == true)ROATModules.Teller_cash_deposit_ete(limits_map,CreditRisk,store_matrix,fcy_range,action,midrate);

						}

					}

				}
			}
			catch(Exception e)
			{
				Reporter.ReportEvent("Test Case execution is Failed", "end to end Deposit","end to end Deposit executition failed", false);
			}
		}
	}
	@SuppressWarnings("unchecked")
	public static void end_to_end_deposit_support_functions_lcy()
	{
		if(HtmlReporter.tc_result)
		{
			try
			{
				int lcy_range=50;
				int fcy_range=100;
				int midrate=1;
				String currency=WebDr.getValue("ETE_FCY_Account_no").split("\\|")[0];
				String user_branch ="";
				String user_id="";
				String user_name = "";
				String Branch="";
				String local_gl__number=WebDr.getValue("LCY_GL_no");
				String forex_gl__number=WebDr.getValue("FCY_GL_No");
				HashMap<String, ArrayList<String>> limits_map = new HashMap<String, ArrayList<String>>();
				HashMap<String, String> CreditRisk = new HashMap<String, String>();
				HashMap<String, Map<String,String>> store_matrix = new HashMap<String, Map<String,String>>();
				HashMap<String, Map<String,String>> store_forex = new HashMap<String, Map<String,String>>();

				ROATFunctions.refesh(false,"Teller");
				if (HtmlReporter.tc_result)
				{ 
					user_branch = ROATModules.fetch_user_branch();
					//user_id=ROATFunctions.fetch_user_Id();
					user_name = ROATModules.fetchuser();
					Branch=ROATFunctions.fetch_user_branch_name();
					lcy_range=ROATModules.LCY_Payment_amount("1",1);
					fcy_range=ROATModules.FCY_Payment_amount(currency,"1",1);


					// Fetching forex rate 
					ROATFunctions.refesh(false, "Central Supervisor");
					WebDr.SetPageObjects("end_end");
					store_forex = ROATModules.set_forexrate(currency);

					// Fetching teller matrix 
					ROATFunctions.refesh(false, "Super User");
					if (HtmlReporter.tc_result)ROATModules.setup_TellerMatrixNavigation();
					store_matrix=ROATModules.fetch_teller_matrix(Branch,WebDr.getValue("default_branch"));

					//Fetching Authorization remote
					if (HtmlReporter.tc_result)
					{
						//CreditRisk=ROATModules.auto_remo_update(Branch);	


						//	***********************************************end to end******************************
						//end to end---LCY---Transaction--without Breach 
						HtmlReporter.tc_result=true;
						ROATFunctions.refesh(false,"Super User");
						String	action="Send for Authorization";
						midrate=Integer.parseInt(store_forex.get("Mid Rate").get("Mid Rate"));
						if (HtmlReporter.tc_result)ROATModules.setup_navigateLTLimits();
						limits_map=ROATModules.fetch_limits_ALL(user_branch, user_id+"-"+user_name,lcy_range*midrate*2,action);
						
						
						
						HtmlReporter.tc_result=true;
						midrate=1;
						ROATModules.end_to_end_deposit(lcy_range,midrate,local_gl__number);




						//************************************support functions****************************


						//delete transaction from 'support function'--- local currency
						HtmlReporter.tc_result=true;
						midrate=1;
						ROATModules.end_to_end_deposit_support_fuction(lcy_range,midrate,local_gl__number);

					}

				}
			}
			catch(Exception e)
			{
				Reporter.ReportEvent("Test Case execution is Failed", "end to end Deposit","end to end Deposit executition failed", false);
			}
		}
	}
	@SuppressWarnings("unchecked")
	public static void end_to_end_deposit_support_functions_Cashback_fcy()
	{
		if(HtmlReporter.tc_result)
		{
			try
			{
				int lcy_range=50;
				int fcy_range=100;
				int midrate=1;
				String currency=WebDr.getValue("ETE_FCY_Account_no").split("\\|")[0];
				String user_branch ="";
				String user_id="";
				String user_name = "";
				String Branch="";
				String local_gl__number=WebDr.getValue("LCY_GL_no");
				String forex_gl__number=WebDr.getValue("FCY_GL_No");
				HashMap<String, ArrayList<String>> limits_map = new HashMap<String, ArrayList<String>>();
				HashMap<String, String> CreditRisk = new HashMap<String, String>();
				HashMap<String, Map<String,String>> store_matrix = new HashMap<String, Map<String,String>>();
				HashMap<String, Map<String,String>> store_forex = new HashMap<String, Map<String,String>>();

				ROATFunctions.refesh(false,"Teller");
				if (HtmlReporter.tc_result)
				{ 
					user_branch = ROATModules.fetch_user_branch();
					user_id=ROATFunctions.fetch_user_Id();
					user_name = ROATModules.fetchuser();
					Branch=ROATFunctions.fetch_user_branch_name();
					lcy_range=ROATModules.LCY_Payment_amount("1",1);
					fcy_range=ROATModules.FCY_Payment_amount(currency,"1",1);


					// Fetching forex rate 
					ROATFunctions.refesh(false, "Central Supervisor");
					WebDr.SetPageObjects("end_end");
					store_forex = ROATModules.set_forexrate(currency);

					// Fetching teller matrix 
					ROATFunctions.refesh(false, "Super User");
					if (HtmlReporter.tc_result)ROATModules.setup_TellerMatrixNavigation();
					store_matrix=ROATModules.fetch_teller_matrix(Branch,WebDr.getValue("default_branch"));

					//Fetching Authorization remote
					if (HtmlReporter.tc_result)
					{
						//CreditRisk=ROATModules.auto_remo_update(Branch);	

						//************************************support functions****************************


						//end to end---LCY---Transaction--without Breach 
						HtmlReporter.tc_result=true;
						ROATFunctions.refesh(false,"Super User");
						String	action="Send for Authorization";
						midrate=Integer.parseInt(store_forex.get("Mid Rate").get("Mid Rate"));
						if (HtmlReporter.tc_result)ROATModules.setup_navigateLTLimits();
						limits_map=ROATModules.fetch_limits_ALL(user_branch, user_id+"-"+user_name,fcy_range*midrate*2,action);
						

						//delete transaction from 'support function'--- foreign currency 
						HtmlReporter.tc_result=true;
						midrate=Integer.parseInt(store_forex.get("Mid Rate").get("Mid Rate"));
						ROATModules.end_to_end_deposit_support_fuction(fcy_range,midrate,forex_gl__number);


						//************************************Deposit with cashback****************************
						HtmlReporter.tc_result=true;
						ROATFunctions.refesh(false,"Super User");
						midrate=Integer.parseInt(store_forex.get("Mid Rate").get("Mid Rate"));
						if (HtmlReporter.tc_result)ROATModules.setup_navigateLTLimits();
						limits_map=ROATModules.fetch_limits_ALL(user_branch, user_id+"-"+user_name,lcy_range*2,action);
						
						HtmlReporter.tc_result=true;
						ROATModules.end_end_deposit_cashback();


						HtmlReporter.tc_result=true;
						//sod eod vault close,teller close
						/*try
						{
							ROATModules.sod_eod_end_to_end();	
						}
						catch(Exception e)
						{
							e.printStackTrace();
							Reporter.ReportEvent("performing sod eod ", "sod eod must be succesful","sod eod failed for deposits", false);
						}*/

					}

				}
			}
			catch(Exception e)
			{
				Reporter.ReportEvent("Test Case execution is Failed", "end to end Deposit","end to end Deposit executition failed", false);
			}
		}
	}
	@SuppressWarnings("unchecked")
	public static void end_to_end_deposit_breaches_lcy_decline()
	{
		if(HtmlReporter.tc_result)
		{
			try
			{
				int lcy_range=50;
				int fcy_range=100;
				int midrate=1;
				String currency=WebDr.getValue("ETE_FCY_Account_no").split("\\|")[0];
				String user_branch ="";
				String user_id="";
				String user_name = "";
				String Branch="";
				String local_gl__number=WebDr.getValue("LCY_GL_no");
				String forex_gl__number=WebDr.getValue("FCY_GL_No");
				HashMap<String, ArrayList<String>> limits_map = new HashMap<String, ArrayList<String>>();
				HashMap<String, String> CreditRisk = new HashMap<String, String>();
				HashMap<String, Map<String,String>> store_matrix = new HashMap<String, Map<String,String>>();
				HashMap<String, Map<String,String>> store_forex = new HashMap<String, Map<String,String>>();

				ROATFunctions.refesh(true,"Teller");
				if (HtmlReporter.tc_result)
				{ 
					user_branch = ROATModules.fetch_user_branch();
					user_id=ROATFunctions.fetch_user_Id();
					user_name = ROATModules.fetchuser();
					Branch=ROATFunctions.fetch_user_branch_name();
					lcy_range=ROATModules.LCY_Payment_amount("1",1);
					fcy_range=ROATModules.FCY_Payment_amount(currency,"1",1);


					// Fetching forex rate 
					ROATFunctions.refesh(false, "Central Supervisor");
					WebDr.SetPageObjects("end_end");
					store_forex = ROATModules.set_forexrate(currency);

					// Fetching teller matrix 
					ROATFunctions.refesh(false, "Super User");
					if (HtmlReporter.tc_result)ROATModules.setup_TellerMatrixNavigation();
					store_matrix=ROATModules.fetch_teller_matrix(Branch,WebDr.getValue("default_branch"));

					//Fetching Authorization remote
					if (HtmlReporter.tc_result)
					{
						//CreditRisk=ROATModules.auto_remo_update(Branch);	

						//******************************Breaches verification************************************


						// test cases flag true
						HtmlReporter.tc_result=true; 

						// LCY Transaction with Limits set as 'Decline'
						if (HtmlReporter.tc_result)
						{
							//LCY Limit breach------mid rate=1
							midrate=1;
							ROATFunctions.refesh(false, "Super User");
							String action="Decline";
							if (HtmlReporter.tc_result)ROATModules.setup_navigateLTLimits();
							if (HtmlReporter.tc_result == true)
							{
								limits_map=ROATModules.fetch_limits_ALL(user_branch, user_id+"-"+user_name,lcy_range,action);

							}


							ROATFunctions.refesh(false, "Teller");
							if (HtmlReporter.tc_result == true)ROATModules.Teller_navigateCashDeposit();
							if (HtmlReporter.tc_result == true)ROATModules.Teller_cash_deposit_ete(limits_map,CreditRisk,store_matrix,lcy_range,action,midrate);

						}

					}

				}
			}
			catch(Exception e)
			{
				Reporter.ReportEvent("Test Case execution is Failed", "end to end Deposit","end to end Deposit executition failed", false);
			}
		}
	}
	
	//***************************************************************************************************************
	public static void LogInLogOut() {
		try 
		{
			String a[]= {"User Admin","Teller","Back office clerk","Authorizer","Head Teller","Vault Custodian","Super User","Signature Authorizer","Supervisor","Global Admin","Enquiry Clerk","Central Supervisor","Back Office Supervisor","Back Office Operator","Back Office Executive"};
			for(int i=0;i<=a.length;i++){
				ROATFunctions.refesh(false,a[i]);
				WebDr.waitTillElementIsDisplayed("logout_button");
				WebDr.clickX("X", "logout_button", "click on logout button", true);
				JavascriptExecutor js  = (JavascriptExecutor) Driver.driver;	
				WebElement koken = WebDr.getElementObject("XPATH|//*[@id='logoutDialog']/div");

				js.executeScript("arguments[0].style.opacity ='1'", koken);
				WebDr.clickX("X", "logout_confirm", "click on logout confirm button", true);
				//			Driver.driver.quit();
			}

		}
		catch(Exception ex)
		{
			ex.printStackTrace();
			Reporter.ReportEvent("Test Case execution is Failed", "end to end Deposit","end to end Deposit executition failed", false);
		}
	}
	public static void sod_eod_lcy_fcy_inquiry_check() throws Exception {
		ROATFunctions.refesh(false,"Teller");
		String branch_no_teller = ROATModules.fetch_user_branch();
		try {
			DatabaseFactory.update_CCP_OCP_Whole_Branch("Y","N",branch_no_teller, CommonUtils.readConfig("Country_code"),Driver.Env_IP);
		} catch (Exception e1) {

			e1.printStackTrace();
		}
		HashMap<String, BigDecimal> tt_inquiry_lcy_before = new HashMap<String, BigDecimal>();
		HashMap<String, BigDecimal> vault_inquiry_lcy_before = new HashMap<String, BigDecimal>();
		HashMap<String,HashMap<String, BigDecimal>> inquiry_fcy_before= new HashMap<String,HashMap<String, BigDecimal>>();
		HashMap<String,HashMap<String, BigDecimal>> vault_inquiry_fcy_before= new HashMap<String,HashMap<String, BigDecimal>>();
		try
		{  
			tt_inquiry_lcy_before=ROATModules.LCY_inquiry();

			inquiry_fcy_before=ROATModules.FCY_Inquiry();

			vault_inquiry_lcy_before=ROATModules.Vault_LCY_Inquiry();

			vault_inquiry_fcy_before=ROATModules.Vault_FCY_Inquiry();

			ROATFunctions.refesh(false,"Supervisor");
			String branch_no = ROATModules.fetch_user_branch();
			DatabaseFactory.update_CCP_OCP_Whole_Branch("N","Y",branch_no, Driver.Country_code,Driver.Env_IP);
			if(HtmlReporter.tc_result==true)
			{
				WebDr.SetPageObjects("PeriodProcessing");
				for(int i=0;i<5;i++)
				{
					WebDr.Wait_Time(200);
					if(WebDr.isExists_Display("warning")>0)
					{
						if(HtmlReporter.tc_result==true)WebDr.click("X", "warning", "Clicking on OK button on warning pop-up", true);
						break;
					}
				}
			}
			if (HtmlReporter.tc_result)ROATModules.nav_periodprocessing();
			//			if (HtmlReporter.tc_result)ROATModules.preriodprocces_verification();
			if (HtmlReporter.tc_result)
			{
				boolean check_sod=ROATModules.Sod_check();
				if(check_sod)
				{
					if (HtmlReporter.tc_result)ROATModules.do_eod();
				}

				if (HtmlReporter.tc_result)
				{
					ROATModules.do_sod();
				}
				if (!HtmlReporter.tc_result)
				{
					ROATModules.do_sod_fail_safe();
				}

				DatabaseFactory.update_CCP_OCP_Whole_Branch("Y","N",branch_no, Driver.Country_code,Driver.Env_IP);
			}

			if (HtmlReporter.tc_result)ROATModules.LCYInquiryCheck(tt_inquiry_lcy_before);

			if (HtmlReporter.tc_result)ROATModules.FCYInquiryCheck(inquiry_fcy_before);

			if (HtmlReporter.tc_result)ROATModules.Vault_LCYInquiry_Check(vault_inquiry_lcy_before);

			if (HtmlReporter.tc_result)ROATModules.VaultFCYInquiryCheck(vault_inquiry_fcy_before);


		}
		catch (Exception e)
		{
			Reporter.ReportEvent("Testcase execution failed", "End to end functionality failed signature add", "End to end functionality failed signature add", false);
			e.printStackTrace();

		}




	}
	public static void receipt_reprint() {
		try
		{
			if (HtmlReporter.tc_result) ROATFunctions.refesh(false, Launcher.UserRole);
			//Navigate 
			if (HtmlReporter.tc_result) ROATModules.NavigateReprint();
			//cms check
			WebDr.SetPageObjects("receipt_reprint");
			if (HtmlReporter.tc_result)ROATFunctions.cmscheck();

			if (HtmlReporter.tc_result) ROATModules.Receipt_Search(true);
			if (HtmlReporter.tc_result) ROATModules.Receipt_Search(false);

			//			if (HtmlReporter.tc_result) ROATModules.Receipt_Condition_check();

		}

		catch (Exception e)
		{
			Reporter.ReportEvent("Testcase execution failed", "End to end functionality failed signature add", "End to end functionality failed signature add", false);
			e.printStackTrace();

		}


	}

	public static void Setup_UserMaintenanceAdd_duty_matrix() 
	{
		try
		{


			ROATFunctions.refesh(true, Launcher.UserRole);

			if (HtmlReporter.tc_result)ROATModules.Setup_navigateUser_Maintenance();
			if (HtmlReporter.tc_result)ROATModules.AddUser_UserMaintenance_duty_matrix();
			if (HtmlReporter.tc_result)ROATModules.Setup_navigateUser_Maintenance();
			if (HtmlReporter.tc_result)ROATModules.AddUser_UserMaintenance_duty_matrix_vault();
		}
		catch (Exception e)
		{
			e.printStackTrace();
			Reporter.ReportEvent("Test Case execution is failed", "Test Case execution is failed","Test Case execution is failed", false);
		}
	}



	//*****************************end to end Withdrawals**********************************************************************
	//Author :Akshay Gopu
	@SuppressWarnings("unchecked")
	public static void end_to_end_withdrawal()
	{
		
		if(HtmlReporter.tc_result)
		{
			try
			{
				int lcy_range=50;
				int fcy_range=100;
				int midrate=1;
				String currency=WebDr.getValue("ETE_FCY_Account_no").split("\\|")[0];
				String user_branch ="";
				String user_id="";
				String user_name = "";
				String Branch="";
				String local_gl__number=WebDr.getValue("LCY_GL_no");
				String forex_gl__number=WebDr.getValue("FCY_GL_No");
				HashMap<String, ArrayList<String>> limits_map = new HashMap<String, ArrayList<String>>();
				HashMap<String, String> CreditRisk = new HashMap<String, String>();
				HashMap<String, Map<String,String>> store_matrix = new HashMap<String, Map<String,String>>();
				HashMap<String, Map<String,String>> store_forex = new HashMap<String, Map<String,String>>();

				ROATFunctions.refesh(false,"Teller");
				if (HtmlReporter.tc_result)
				{ 
					user_branch = ROATModules.fetch_user_branch();
					user_id=ROATFunctions.fetch_user_Id();
					user_name = ROATModules.fetchuser();
					Branch=ROATFunctions.fetch_user_branch_name();
					lcy_range=ROATModules.LCY_Payment_amount("1",1);
					fcy_range=ROATModules.FCY_Payment_amount(currency,"1",1);


					// Fetching forex rate 
					ROATFunctions.refesh(false, "Central Supervisor");
					WebDr.SetPageObjects("end_end");
					store_forex = ROATModules.set_forexrate(currency);

					// Fetching teller matrix 
					ROATFunctions.refesh(false, "Super User");
					if (HtmlReporter.tc_result)ROATModules.setup_TellerMatrixNavigation();
					store_matrix=ROATModules.fetch_teller_matrix(Branch,WebDr.getValue("default_branch"));

					//Fetching Authorization remote
					if (HtmlReporter.tc_result)
					{
						//						CreditRisk=ROATModules.auto_remo_update(Branch);	
						if (HtmlReporter.tc_result)ROATModules.setup_navigateLTLimits();


						//******************************Breaches verification************************************


						// LCY Transaction with Limits set as 'Send for authorization' 
						if (HtmlReporter.tc_result)
						{ 
							//LCY Limit breach------mid rate=1
							midrate=1;
							String action="Send for Authorization";
							if (HtmlReporter.tc_result)
							{
								limits_map=ROATModules.fetch_limits_ALL(user_branch, user_id+"-"+user_name,lcy_range,action);
							}


							ROATFunctions.refesh(false, "Teller");
							if (HtmlReporter.tc_result)ROATModules.navigate_TellerWithdrawal();
							if (HtmlReporter.tc_result)ROATModules.Teller_cash_withdrawal_ete(limits_map,CreditRisk,store_matrix,lcy_range,action,midrate);


						}


						// test cases flag true
						HtmlReporter.tc_result=true; 

						// LCY Transaction with Limits set as 'Decline'
						if (HtmlReporter.tc_result)
						{
							//LCY Limit breach------mid rate=1
							midrate=1;
							ROATFunctions.refesh(false, "Super User");
							String action="Decline";
							if (HtmlReporter.tc_result)ROATModules.setup_navigateLTLimits();
							if (HtmlReporter.tc_result == true)
							{
								limits_map=ROATModules.fetch_limits_ALL(user_branch, user_id+"-"+user_name,lcy_range,action);

							}


							ROATFunctions.refesh(false, "Teller");
							if (HtmlReporter.tc_result == true)ROATModules.navigate_TellerWithdrawal();
							if (HtmlReporter.tc_result == true)ROATModules.Teller_cash_withdrawal_ete(limits_map,CreditRisk,store_matrix,lcy_range,action,midrate);

						}


						// test cases flag true
						HtmlReporter.tc_result=true; 

						// FCY Transaction with Limits set as Send for authorization
						if (HtmlReporter.tc_result)
						{
							midrate=Integer.parseInt(store_forex.get("Mid Rate").get("Mid Rate"));
							ROATFunctions.refesh(false, "Super User");
							String action="Send for Authorization";
							if (HtmlReporter.tc_result)ROATModules.setup_navigateLTLimits();
							if (HtmlReporter.tc_result == true)
							{
								limits_map=ROATModules.fetch_limits_ALL(user_branch, user_id+"-"+user_name,fcy_range*midrate,action);
							}

							//FCY Limit breach------mid rate

							ROATFunctions.refesh(false, "Teller");
							if (HtmlReporter.tc_result == true)ROATModules.navigate_TellerWithdrawal();
							if (HtmlReporter.tc_result == true)ROATModules.Teller_cash_withdrawal_ete(limits_map,CreditRisk,store_matrix,fcy_range,action,midrate);

						}

					}
					System.out.println("store_matrix");
					System.out.println(store_matrix);
					System.out.println();
					System.out.println("credit Risk");
					System.out.println(CreditRisk);
				}



				// test cases flag true
				HtmlReporter.tc_result=true; 


				//	***********************************************end to end******************************
				//end to end---LCY---Transaction--without Breach 
				ROATFunctions.refesh(false,"Super User");
				String action="Send for Authorization";
				midrate=Integer.parseInt(store_forex.get("Mid Rate").get("Mid Rate"));
				if (HtmlReporter.tc_result)ROATModules.setup_navigateLTLimits();
				limits_map=ROATModules.fetch_limits_ALL(user_branch, user_id+"-"+user_name,lcy_range*midrate*2,action);

				if(HtmlReporter.tc_result)
				{
					midrate=1;
					ROATModules.end_to_end_withdrawal(lcy_range,midrate,local_gl__number);



					//************************************support functions****************************


					//delete transaction from 'support function'--- local currency

					HtmlReporter.tc_result=true; 
					midrate=1;
					ROATModules.end_to_end_withdrawal_support_functions(lcy_range,midrate,local_gl__number);



					//delete transaction from 'support function'--- foreign currency 
					HtmlReporter.tc_result=true;
					midrate=Integer.parseInt(store_forex.get("Mid Rate").get("Mid Rate"));
					ROATModules.end_to_end_withdrawal_support_functions(fcy_range,midrate,forex_gl__number);

				}

				//sod eod vault close,teller close
				try
				{
					ROATModules.sod_eod_end_to_end();	
				}
				catch(Exception e)
				{
					e.printStackTrace();
					Reporter.ReportEvent("performing sod eod ", "sod eod must be succesful","sod eod failed for deposits", false);
				}


			}

			catch(Exception ex)
			{
				ex.printStackTrace();
				Reporter.ReportEvent("Test Case execution is Failed", "end to end Deposit","end to end Deposit executition failed", false);
			}
		}
		else
		{
			Reporter.ReportEvent("performing sod", "sod must be successfull","sod failed", false);
		}

	}
	@SuppressWarnings("unchecked")
	public static void end_to_end_withdrawal_breaches_lcy()
	{

		try
		{
			int lcy_range=50;
			int fcy_range=100;
			int midrate=1;
			String currency=WebDr.getValue("ETE_FCY_Account_no").split("\\|")[0];
			String user_branch ="";
			String user_id="";
			String user_name = "";
			String Branch="";
			String local_gl__number=WebDr.getValue("LCY_GL_no");
			String forex_gl__number=WebDr.getValue("FCY_GL_No");
			HashMap<String, ArrayList<String>> limits_map = new HashMap<String, ArrayList<String>>();
			HashMap<String, String> CreditRisk = new HashMap<String, String>();
			HashMap<String, Map<String,String>> store_matrix = new HashMap<String, Map<String,String>>();
			HashMap<String, Map<String,String>> store_forex = new HashMap<String, Map<String,String>>();

			ROATFunctions.refesh(false,"Teller");
			if (HtmlReporter.tc_result)
			{ 
				user_branch = ROATModules.fetch_user_branch();
				user_id=ROATFunctions.fetch_user_Id();
				user_name = ROATModules.fetchuser();
				Branch=ROATFunctions.fetch_user_branch_name();
				lcy_range=ROATModules.LCY_Payment_amount("1",1);
				fcy_range=ROATModules.FCY_Payment_amount(currency,"1",1);


				// Fetching forex rate 
				ROATFunctions.refesh(false, "Central Supervisor");
				WebDr.SetPageObjects("end_end");
				store_forex = ROATModules.set_forexrate(currency);

				// Fetching teller matrix 
				ROATFunctions.refesh(false, "Super User");
				if (HtmlReporter.tc_result)ROATModules.setup_TellerMatrixNavigation();
				store_matrix=ROATModules.fetch_teller_matrix(Branch,WebDr.getValue("default_branch"));

				//Fetching Authorization remote
				if (HtmlReporter.tc_result)
				{
					//						CreditRisk=ROATModules.auto_remo_update(Branch);	
					if (HtmlReporter.tc_result)ROATModules.setup_navigateLTLimits();


					//******************************Breaches verification************************************

					// LCY Transaction with Limits set as 'Send for authorization' 
					if (HtmlReporter.tc_result)
					{ 
						//LCY Limit breach------mid rate=1
						midrate=1;
						String action="Send for Authorization";
						if (HtmlReporter.tc_result)
						{
							limits_map=ROATModules.fetch_limits_ALL(user_branch, user_id+"-"+user_name,lcy_range,action);
						}


						ROATFunctions.refesh(false, "Teller");
						if (HtmlReporter.tc_result)ROATModules.navigate_TellerWithdrawal();
						if (HtmlReporter.tc_result)ROATModules.Teller_cash_withdrawal_ete(limits_map,CreditRisk,store_matrix,lcy_range,action,midrate);

					}
				}
			}
		}

		catch(Exception ex)
		{
			ex.printStackTrace();
			Reporter.ReportEvent("Test Case execution is Failed", "end to end Deposit","end to end Deposit executition failed", false);
		}

	}
	@SuppressWarnings("unchecked")
	public static void end_to_end_withdrawal_breaches_lcy_decline()
	{

		try
		{
			int lcy_range=50;
			int fcy_range=100;
			int midrate=1;
			String currency=WebDr.getValue("ETE_FCY_Account_no").split("\\|")[0];
			String user_branch ="";
			String user_id="";
			String user_name = "";
			String Branch="";
			String local_gl__number=WebDr.getValue("LCY_GL_no");
			String forex_gl__number=WebDr.getValue("FCY_GL_No");
			HashMap<String, ArrayList<String>> limits_map = new HashMap<String, ArrayList<String>>();
			HashMap<String, String> CreditRisk = new HashMap<String, String>();
			HashMap<String, Map<String,String>> store_matrix = new HashMap<String, Map<String,String>>();
			HashMap<String, Map<String,String>> store_forex = new HashMap<String, Map<String,String>>();

			ROATFunctions.refesh(false,"Teller");
			if (HtmlReporter.tc_result)
			{ 
				user_branch = ROATModules.fetch_user_branch();
				user_id=ROATFunctions.fetch_user_Id();
				user_name = ROATModules.fetchuser();
				Branch=ROATFunctions.fetch_user_branch_name();
				lcy_range=ROATModules.LCY_Payment_amount("1",1);
				fcy_range=ROATModules.FCY_Payment_amount(currency,"1",1);


				// Fetching forex rate 
				ROATFunctions.refesh(false, "Central Supervisor");
				WebDr.SetPageObjects("end_end");
				store_forex = ROATModules.set_forexrate(currency);

				// Fetching teller matrix 
				ROATFunctions.refesh(false, "Super User");
				if (HtmlReporter.tc_result)ROATModules.setup_TellerMatrixNavigation();
				store_matrix=ROATModules.fetch_teller_matrix(Branch,WebDr.getValue("default_branch"));

				//Fetching Authorization remote
				if (HtmlReporter.tc_result)
				{
					//						CreditRisk=ROATModules.auto_remo_update(Branch);	
					if (HtmlReporter.tc_result)ROATModules.setup_navigateLTLimits();


					//******************************Breaches verification************************************
					// test cases flag true
					HtmlReporter.tc_result=true; 

					// LCY Transaction with Limits set as 'Decline'
					if (HtmlReporter.tc_result)
					{
						//LCY Limit breach------mid rate=1
						midrate=1;
						ROATFunctions.refesh(false, "Super User");
						String action="Decline";
						if (HtmlReporter.tc_result)ROATModules.setup_navigateLTLimits();
						if (HtmlReporter.tc_result == true)
						{
							limits_map=ROATModules.fetch_limits_ALL(user_branch, user_id+"-"+user_name,lcy_range,action);

						}

						ROATFunctions.refesh(false, "Teller");
						if (HtmlReporter.tc_result == true)ROATModules.navigate_TellerWithdrawal();
						if (HtmlReporter.tc_result == true)ROATModules.Teller_cash_withdrawal_ete(limits_map,CreditRisk,store_matrix,lcy_range,action,midrate);

					}
				}
			}
		}

		catch(Exception ex)
		{
			ex.printStackTrace();
			Reporter.ReportEvent("Test Case execution is Failed", "end to end Deposit","end to end Deposit executition failed", false);
		}

	}

	@SuppressWarnings("unchecked")
	public static void end_to_end_withdrawal_breaches_fcy()
	{

		try
		{
			int lcy_range=50;
			int fcy_range=100;
			int midrate=1;
			String currency=WebDr.getValue("ETE_FCY_Account_no").split("\\|")[0];
			String user_branch ="";
			String user_id="";
			String user_name = "";
			String Branch="";
			String local_gl__number=WebDr.getValue("LCY_GL_no");
			String forex_gl__number=WebDr.getValue("FCY_GL_No");
			HashMap<String, ArrayList<String>> limits_map = new HashMap<String, ArrayList<String>>();
			HashMap<String, String> CreditRisk = new HashMap<String, String>();
			HashMap<String, Map<String,String>> store_matrix = new HashMap<String, Map<String,String>>();
			HashMap<String, Map<String,String>> store_forex = new HashMap<String, Map<String,String>>();

			ROATFunctions.refesh(false,"Teller");
			if (HtmlReporter.tc_result)
			{ 
				user_branch = ROATModules.fetch_user_branch();
				user_id=ROATFunctions.fetch_user_Id();
				user_name = ROATModules.fetchuser();
				Branch=ROATFunctions.fetch_user_branch_name();
				lcy_range=ROATModules.LCY_Payment_amount("1",1);
				fcy_range=ROATModules.FCY_Payment_amount(currency,"1",1);


				// Fetching forex rate 
				ROATFunctions.refesh(false, "Central Supervisor");
				WebDr.SetPageObjects("end_end");
				store_forex = ROATModules.set_forexrate(currency);

				// Fetching teller matrix 
				ROATFunctions.refesh(false, "Super User");
				if (HtmlReporter.tc_result)ROATModules.setup_TellerMatrixNavigation();
				store_matrix=ROATModules.fetch_teller_matrix(Branch,WebDr.getValue("default_branch"));

				//Fetching Authorization remote
				if (HtmlReporter.tc_result)
				{
					//						CreditRisk=ROATModules.auto_remo_update(Branch);	
					// test cases flag true
					HtmlReporter.tc_result=true; 

					// FCY Transaction with Limits set as Send for authorization
					if (HtmlReporter.tc_result)
					{
						midrate=Integer.parseInt(store_forex.get("Mid Rate").get("Mid Rate"));
						ROATFunctions.refesh(false, "Super User");
						String action="Send for Authorization";
						if (HtmlReporter.tc_result)ROATModules.setup_navigateLTLimits();
						if (HtmlReporter.tc_result == true)
						{
							limits_map=ROATModules.fetch_limits_ALL(user_branch, user_id+"-"+user_name,fcy_range*midrate,action);
						}

						//FCY Limit breach------mid rate

						ROATFunctions.refesh(false, "Teller");
						if (HtmlReporter.tc_result == true)ROATModules.navigate_TellerWithdrawal();
						if (HtmlReporter.tc_result == true)ROATModules.Teller_cash_withdrawal_ete(limits_map,CreditRisk,store_matrix,fcy_range,action,midrate);

					}

				}

			}
		}

		catch(Exception ex)
		{
			ex.printStackTrace();
			Reporter.ReportEvent("Test Case execution is Failed", "end to end Deposit","end to end Deposit executition failed", false);
		}

	}
	@SuppressWarnings("unchecked")
	public static void end_to_end_withdrawal_support_functions_lcy()
	{

		try
		{
			int lcy_range=50;
			int fcy_range=100;
			int midrate=1;
			String currency=WebDr.getValue("ETE_FCY_Account_no").split("\\|")[0];
			String user_branch ="";
			String user_id="";
			String user_name = "";
			String Branch="";
			String local_gl__number=WebDr.getValue("LCY_GL_no");
			String forex_gl__number=WebDr.getValue("FCY_GL_No");
			HashMap<String, ArrayList<String>> limits_map = new HashMap<String, ArrayList<String>>();
			HashMap<String, String> CreditRisk = new HashMap<String, String>();
			HashMap<String, Map<String,String>> store_matrix = new HashMap<String, Map<String,String>>();
			HashMap<String, Map<String,String>> store_forex = new HashMap<String, Map<String,String>>();

			ROATFunctions.refesh(false,"Teller");
			if (HtmlReporter.tc_result)
			{ 
				user_branch = ROATModules.fetch_user_branch();
				user_id=ROATFunctions.fetch_user_Id();
				user_name = ROATModules.fetchuser();
				Branch=ROATFunctions.fetch_user_branch_name();
				lcy_range=ROATModules.LCY_Payment_amount("1",1);
				fcy_range=ROATModules.FCY_Payment_amount(currency,"1",1);


				// Fetching forex rate 
				ROATFunctions.refesh(false, "Central Supervisor");
				WebDr.SetPageObjects("end_end");
				store_forex = ROATModules.set_forexrate(currency);

				// Fetching teller matrix 
				ROATFunctions.refesh(false, "Super User");
				if (HtmlReporter.tc_result)ROATModules.setup_TellerMatrixNavigation();
				store_matrix=ROATModules.fetch_teller_matrix(Branch,WebDr.getValue("default_branch"));

				//Fetching Authorization remote
				if (HtmlReporter.tc_result)
				{
					//						CreditRisk=ROATModules.auto_remo_update(Branch);	


					//	***********************************************end to end******************************
					//end to end---LCY---Transaction--without Breach 
					ROATFunctions.refesh(false,"Super User");
					String action="Send for Authorization";
					midrate=Integer.parseInt(store_forex.get("Mid Rate").get("Mid Rate"));
					if (HtmlReporter.tc_result)ROATModules.setup_navigateLTLimits();
					limits_map=ROATModules.fetch_limits_ALL(user_branch, user_id+"-"+user_name,lcy_range*midrate*2,action);

					if(HtmlReporter.tc_result)
					{
						midrate=1;
						ROATModules.end_to_end_withdrawal(lcy_range,midrate,local_gl__number);



						//************************************support functions****************************


						//delete transaction from 'support function'--- local currency

						HtmlReporter.tc_result=true; 
						midrate=1;
						ROATModules.end_to_end_withdrawal_support_functions(lcy_range,midrate,local_gl__number);

					}

				}

			}
		}

		catch(Exception ex)
		{
			ex.printStackTrace();
			Reporter.ReportEvent("Test Case execution is Failed", "end to end Deposit","end to end Deposit executition failed", false);
		}

	}


	@SuppressWarnings("unchecked")
	public static void end_to_end_withdrawal_support_functions_fcy()
	{

		try
		{
			int lcy_range=50;
			int fcy_range=100;
			int midrate=1;
			String currency=WebDr.getValue("ETE_FCY_Account_no").split("\\|")[0];
			String user_branch ="";
			String user_id="";
			String user_name = "";
			String Branch="";
			String local_gl__number=WebDr.getValue("LCY_GL_no");
			String forex_gl__number=WebDr.getValue("FCY_GL_No");
			HashMap<String, ArrayList<String>> limits_map = new HashMap<String, ArrayList<String>>();
			HashMap<String, String> CreditRisk = new HashMap<String, String>();
			HashMap<String, Map<String,String>> store_matrix = new HashMap<String, Map<String,String>>();
			HashMap<String, Map<String,String>> store_forex = new HashMap<String, Map<String,String>>();

			ROATFunctions.refesh(false,"Teller");
			if (HtmlReporter.tc_result)
			{ 
				user_branch = ROATModules.fetch_user_branch();
				user_id=ROATFunctions.fetch_user_Id();
				user_name = ROATModules.fetchuser();
				Branch=ROATFunctions.fetch_user_branch_name();
				lcy_range=ROATModules.LCY_Payment_amount("1",1);
				fcy_range=ROATModules.FCY_Payment_amount(currency,"1",1);


				// Fetching forex rate 
				ROATFunctions.refesh(false, "Central Supervisor");
				WebDr.SetPageObjects("end_end");
				store_forex = ROATModules.set_forexrate(currency);

				// Fetching teller matrix 
				ROATFunctions.refesh(false, "Super User");
				if (HtmlReporter.tc_result)ROATModules.setup_TellerMatrixNavigation();
				store_matrix=ROATModules.fetch_teller_matrix(Branch,WebDr.getValue("default_branch"));

				//Fetching Authorization remote
				if (HtmlReporter.tc_result)
				{
					//						CreditRisk=ROATModules.auto_remo_update(Branch);	


					//	***********************************************end to end******************************
					//end to end---LCY---Transaction--without Breach 
					ROATFunctions.refesh(false,"Super User");
					String action="Send for Authorization";
					midrate=Integer.parseInt(store_forex.get("Mid Rate").get("Mid Rate"));
					if (HtmlReporter.tc_result)ROATModules.setup_navigateLTLimits();
					limits_map=ROATModules.fetch_limits_ALL(user_branch, user_id+"-"+user_name,fcy_range*midrate*2,action);

					if(HtmlReporter.tc_result)
					{
						//delete transaction from 'support function'--- foreign currency 
						HtmlReporter.tc_result=true;
						midrate=Integer.parseInt(store_forex.get("Mid Rate").get("Mid Rate"));
						ROATModules.end_to_end_withdrawal_support_functions(fcy_range,midrate,forex_gl__number);

					}

				}

			}
		}

		catch(Exception ex)
		{
			ex.printStackTrace();
			Reporter.ReportEvent("Test Case execution is Failed", "end to end Deposit","end to end Deposit executition failed", false);
		}

	}



	//******************************************************Ear mark End to end***********************************
	//Author: vishwajit Gupta Date:25/09/2019

	public static void E_E_Earmarks() 
	{
		HashMap<String, String> LCYaccount_details = new HashMap<String, String>();
		HashMap<String, String> FCYaccount_details = new HashMap<String, String>();
		BigDecimal Amount_Val=BigDecimal.ZERO;
		
			try 
			{

				if (HtmlReporter.tc_result)ROATModules.earmark_delete_all( WebDr.getValue("LCY_Account"),"0","",true);
				if (HtmlReporter.tc_result)ROATModules.earmark_inquiry( WebDr.getValue("LCY_Account"), Amount_Val,"","delete");
				if (HtmlReporter.tc_result)ROATFunctions.refesh(false,"Teller");
				WebDr.Wait_Time(500);
				LCYaccount_details=ROATModules.account_inquiry(WebDr.getValue("LCY_Account").split("\\-")[1],WebDr.getValue("LCY_Account").split("\\-")[0]);
				String before_balance =(LCYaccount_details.get("current_Earmarked Balance")).replace(":","");
				String before_amount = before_balance.trim();
				BigDecimal before_add_balance=new BigDecimal(before_amount);

				// LCY Account and CR
				if (HtmlReporter.tc_result) Amount_Val= new BigDecimal(ROATModules.earmark_add( WebDr.getValue("LCY_Account"),"0","CR",true));
				LCYaccount_details=ROATModules.account_inquiry(WebDr.getValue("LCY_Account").split("\\-")[1],WebDr.getValue("LCY_Account").split("\\-")[0]);
				String LCRafter_balance =(LCYaccount_details.get("current_Earmarked Balance")).replace(":","");
				String LCR_Amount = (LCRafter_balance).replace(",","");
				String LCRafter_amount = LCR_Amount.trim();
				BigDecimal after_LCRadd_balance=new BigDecimal(LCRafter_amount);
				if(after_LCRadd_balance.equals(before_add_balance.add(Amount_Val)))
				{
					Reporter.ReportEvent("Earmark add", "Earmark should be added in Inquiry", "Earmark added", true);
				}
				else
				{
					Reporter.ReportEvent("Earmark add", "Earmark should be added in Inquiry", "Earmark not added", false);
				}
				if (HtmlReporter.tc_result)ROATModules.earmark_inquiry( WebDr.getValue("LCY_Account"),Amount_Val,"CR","add");
				if (HtmlReporter.tc_result)ROATModules.earmark_update( WebDr.getValue("LCY_Account"),Amount_Val,"CR","true");
				if (HtmlReporter.tc_result)ROATModules.earmark_inquiry( WebDr.getValue("LCY_Account"),Amount_Val,"CR","update");
				if (HtmlReporter.tc_result)ROATModules.earmark_delete_all( WebDr.getValue("LCY_Account"),"0","",true);

				// LCY Account and DR
				WebDr.Wait_Time(500);
				if (HtmlReporter.tc_result)Amount_Val=new BigDecimal(ROATModules.earmark_add( WebDr.getValue("LCY_Account"),"0","DR",true));
				LCYaccount_details=ROATModules.account_inquiry(WebDr.getValue("LCY_Account").split("\\-")[1],WebDr.getValue("LCY_Account").split("\\-")[0]);
				String LDRafter_balance =(LCYaccount_details.get("current_Earmarked Balance")).replace(":","");
				String LDR_Amount = (LDRafter_balance).replace(",","");
				String LDRafter_amount = LDR_Amount.trim();
				BigDecimal after_LDRadd_balance=new BigDecimal(LDRafter_amount);
				if(after_LDRadd_balance.equals(before_add_balance.subtract(Amount_Val)))
				{
					Reporter.ReportEvent("Earmark add", "Earmark should be added in Inquiry", "Earmark added", true);
				}
				else
				{
					Reporter.ReportEvent("Earmark add", "Earmark should be added in Inquiry", "Earmark not added", false);
				}
				if (HtmlReporter.tc_result)ROATModules.earmark_inquiry( WebDr.getValue("LCY_Account"),Amount_Val,"DR","add");
				if (HtmlReporter.tc_result)ROATModules.earmark_update( WebDr.getValue("LCY_Account"),Amount_Val,"DR","true");
				if (HtmlReporter.tc_result)ROATModules.earmark_inquiry( WebDr.getValue("LCY_Account"),Amount_Val,"DR","update");
				if (HtmlReporter.tc_result)ROATModules.earmark_delete_all( WebDr.getValue("LCY_Account"),"0","",true);

				// test cases flag true
				HtmlReporter.tc_result=true;
				
				//Earmark for FCY Account
				if (HtmlReporter.tc_result)ROATModules.earmark_delete_all( WebDr.getValue("FCY_Account"),"0","",true);
				if (HtmlReporter.tc_result)ROATModules.earmark_inquiry( WebDr.getValue("FCY_Account"), Amount_Val,"","delete");

				if (HtmlReporter.tc_result)ROATFunctions.refesh(false,"Teller");
				WebDr.Wait_Time(500);
				FCYaccount_details=ROATModules.account_inquiry(WebDr.getValue("FCY_Account").split("\\-")[1],WebDr.getValue("FCY_Account").split("\\-")[0]);
				String FCYbefore_balance =(FCYaccount_details.get("current_Earmarked Balance")).replace(":","");
				String FCY_Amount = (FCYbefore_balance).replace(",","");
				String FCYbefore_amount = FCY_Amount.trim();
				//			System.out.println(FCYbefore_amount);
				BigDecimal FCYbefore_add_balance=new BigDecimal(FCYbefore_amount);

				//FCY Account and CR 
				WebDr.Wait_Time(500);
				if (HtmlReporter.tc_result)Amount_Val=new BigDecimal(ROATModules.earmark_add( WebDr.getValue("FCY_Account"),"0","CR",true));
				FCYaccount_details=ROATModules.account_inquiry(WebDr.getValue("FCY_Account").split("\\-")[1],WebDr.getValue("FCY_Account").split("\\-")[0]);
				String FCRafter_balance =(FCYaccount_details.get("current_Earmarked Balance")).replace(":","");
				String FCR_Amount = (FCRafter_balance).replace(",","");
				String FCRafter_amount = FCR_Amount.trim();
				System.out.println(FCRafter_amount);
				BigDecimal after_FCRadd_balance=new BigDecimal(FCRafter_amount);

				if(after_FCRadd_balance.equals(FCYbefore_add_balance.add(Amount_Val)))
				{
					Reporter.ReportEvent("Earmark add", "Earmark should be added in Inquiry", "Earmark added", true);
				}
				else
				{
					Reporter.ReportEvent("Earmark add", "Earmark should be added in Inquiry", "Earmark not added", false);
				}
				if (HtmlReporter.tc_result)ROATModules.earmark_inquiry( WebDr.getValue("FCY_Account"),Amount_Val,"CR","add");
				if (HtmlReporter.tc_result)ROATModules.earmark_update( WebDr.getValue("FCY_Account"),Amount_Val,"CR","true");
				if (HtmlReporter.tc_result)ROATModules.earmark_inquiry( WebDr.getValue("FCY_Account"),Amount_Val,"CR","update");
				if (HtmlReporter.tc_result)ROATModules.earmark_delete_all( WebDr.getValue("FCY_Account"),"0","",true);

				//FCY Account and DR
				WebDr.Wait_Time(500);
				if (HtmlReporter.tc_result)Amount_Val=new BigDecimal(ROATModules.earmark_add( WebDr.getValue("FCY_Account"),"0","DR",true));
				FCYaccount_details=ROATModules.account_inquiry(WebDr.getValue("FCY_Account").split("\\-")[1],WebDr.getValue("FCY_Account").split("\\-")[0]);
				String FDRafter_balance =(FCYaccount_details.get("current_Earmarked Balance")).replace(":","");
				String FDR_Amount = (FDRafter_balance).replace(",","");
				String FDRafter_amount = FDR_Amount.trim();
				BigDecimal after_FDRadd_balance=new BigDecimal(FDRafter_amount);
				System.out.println(after_FDRadd_balance);
				System.out.println(FCYbefore_add_balance);
				System.out.println(Amount_Val); 

				if(after_FDRadd_balance.equals(FCYbefore_add_balance.subtract(Amount_Val)))
				{
					Reporter.ReportEvent("Earmark add", "Earmark should be added in Inquiry", "Earmark added", true);
				}
				else
				{
					Reporter.ReportEvent("Earmark add", "Earmark should be added in Inquiry", "Earmark not added", false);
				}
				if (HtmlReporter.tc_result)ROATModules.earmark_inquiry( WebDr.getValue("FCY_Account"),Amount_Val,"DR","add");
				if (HtmlReporter.tc_result)ROATModules.earmark_update( WebDr.getValue("FCY_Account"),Amount_Val,"DR","true");
				if (HtmlReporter.tc_result)ROATModules.earmark_inquiry( WebDr.getValue("FCY_Account"),Amount_Val,"DR","update");
				if (HtmlReporter.tc_result)ROATModules.earmark_delete_all( WebDr.getValue("FCY_Account"),"0","",true);
				
		// Third party Earmark
				Amount_Val=BigDecimal.ZERO;
				//To Inquire the business earmark
				if (HtmlReporter.tc_result)ROATModules.earmark_inquiry( WebDr.getValue("TParty_Account"),Amount_Val,"DR","Third Party Inquire");
				
				//To update the Business earmark
				if (HtmlReporter.tc_result)ROATModules.earmark_update( WebDr.getValue("TParty_Account"),Amount_Val,"DR","Third Party");
				
				// To Inquire the updated Earmark
				if (HtmlReporter.tc_result)ROATModules.earmark_inquiry( WebDr.getValue("TParty_Account"),Amount_Val,"DR","Third Party Update");
				
				//To add the Earmark
				if (HtmlReporter.tc_result)Amount_Val=new BigDecimal(ROATModules.earmark_add( WebDr.getValue("TParty_Account"),"0","DR",true));
				
				//To Inquire new earmark added
				if (HtmlReporter.tc_result)ROATModules.earmark_inquiry( WebDr.getValue("TParty_Account"),Amount_Val,"DR","add");
				String Amount = String.valueOf(Amount_Val);
				// to delete the added earmark
				if (HtmlReporter.tc_result)ROATModules.earmark_delete_all( WebDr.getValue("TParty_Account"),Amount,"DR",false);
				
				// to verify the Business earmark not deleted
				if (HtmlReporter.tc_result)ROATModules.earmark_inquiry( WebDr.getValue("TParty_Account"),Amount_Val,"DR","Third Party Update");
				
				if (HtmlReporter.tc_result)
				{
					Reporter.ReportEvent("Earmark testcase", "Earmark Testcase execution should be passed","Earmark Testcase execution passed", true);	
				}
				
			}
			catch (Exception e) 
			{
				Reporter.ReportEvent("Test Case execution is failed", "Test Case execution is failed","Test Case execution is failed", false);
			}
		
	}	

			// Endmark Breaches***************************************************
		public static void E_E_Earmarks_Breaches() 
		{
		HashMap<String, String> LCYaccount_details = new HashMap<String, String>();
		HashMap<String, String> FCYaccount_details = new HashMap<String, String>();
		BigDecimal Amount_Val=BigDecimal.ZERO;
		try
			{
				int lcy_range=50;
				int fcy_range=100;
				int midrate=1;
				String currency=WebDr.getValue("ETE_FCY_Account_no").split("\\|")[0];
				String user_branch ="";
				String user_id="";
				String user_name = "";
				String Branch="";
				String local_gl__number=WebDr.getValue("LCY_GL_no");
				String forex_gl__number=WebDr.getValue("FCY_GL_No");
				HashMap<String, ArrayList<String>> limits_map = new HashMap<String, ArrayList<String>>();
				HashMap<String, String> CreditRisk = new HashMap<String, String>();
				HashMap<String, Map<String,String>> store_matrix = new HashMap<String, Map<String,String>>();
				HashMap<String, Map<String,String>> store_forex = new HashMap<String, Map<String,String>>();

				ROATFunctions.refesh(false,"Teller");
				if (HtmlReporter.tc_result)
				{ 
					user_branch = ROATModules.fetch_user_branch();
					user_id=ROATFunctions.fetch_user_Id();
					user_name = ROATModules.fetchuser();
					Branch=ROATFunctions.fetch_user_branch_name();
					lcy_range=ROATModules.LCY_Payment_amount("1",1);
					fcy_range=ROATModules.FCY_Payment_amount(currency,"1",1);


					// Fetching forex rate 
					ROATFunctions.refesh(false, "Central Supervisor");
					WebDr.SetPageObjects("end_end");
					store_forex = ROATModules.set_forexrate(currency);

					// Fetching teller matrix 
					ROATFunctions.refesh(false, "Super User");
					if (HtmlReporter.tc_result)ROATModules.setup_TellerMatrixNavigation();
					store_matrix=ROATModules.fetch_teller_matrix(Branch,WebDr.getValue("default_branch"));

					//Fetching Authorization remote
					if (HtmlReporter.tc_result)
					{
						//CreditRisk=ROATModules.auto_remo_update(Branch);	
						if (HtmlReporter.tc_result)ROATModules.setup_navigateLTLimits();


						//******************************Breaches verification************************************


						// LCY Transaction with Limits set as 'Send for authorization' 
						if (HtmlReporter.tc_result)
						{ 
							//LCY Limit breach------mid rate=1
							midrate=1;
							String action="Send for Authorization";
							if (HtmlReporter.tc_result)
							{
								limits_map=ROATModules.fetch_limits_ALL(user_branch, user_id+"-"+user_name,lcy_range,action);
							}

							ROATFunctions.refesh(false, "Teller");
							if (HtmlReporter.tc_result)ROATModules.AI_navigateearmark();
							if (HtmlReporter.tc_result)ROATModules.Earmark_ete(limits_map,CreditRisk,store_matrix,lcy_range,action,midrate);
						}

						// test cases flag true
						HtmlReporter.tc_result=true; 

						// LCY Transaction with Limits set as 'Decline'
						if (HtmlReporter.tc_result)
						{
							//LCY Limit breach------mid rate=1
							midrate=1;
							ROATFunctions.refesh(false, "Super User");
							String action="Decline";
							if (HtmlReporter.tc_result)ROATModules.setup_navigateLTLimits();
							if (HtmlReporter.tc_result == true)
							{
								limits_map=ROATModules.fetch_limits_ALL(user_branch, user_id+"-"+user_name,lcy_range,action);
							}
							ROATFunctions.refesh(false, "Teller");
							if (HtmlReporter.tc_result == true)ROATModules.AI_navigateearmark();
							if (HtmlReporter.tc_result == true)ROATModules.Earmark_ete(limits_map,CreditRisk,store_matrix,lcy_range,action,midrate);
						}
						
						if (HtmlReporter.tc_result)
						{
							Reporter.ReportEvent("Earmark Breach Testcase ", "Earmark Breach Testcase should be passed","Earmark Breach Testcase Passed", true);
						}
						
					}
				}
			}	

			catch(Exception e)
			{
				e.getStackTrace();
				Reporter.ReportEvent("Earmark Breach Testcase ", "Earmark Breach Testcase should be passed","Earmark Breach Testcase Failed", false);
			}
			
		}
		







	

		//*************** Forex Sell End to End*************************************
		
				@SuppressWarnings("unused")
				public static void E_E_forex_sell()
				{
					/*try
					{
						ROATModules.sod_eod_end_to_end();	
					}
					catch(Exception e)
					{
						e.printStackTrace();
						Reporter.ReportEvent("performing sod eod ", "sod eod must be succesful","sod eod failed for deposits", false);
					}
					*/
					String user_branch="",user_id="",user_name="",Branch="",sellAccountNumber="",payAccountNumber="",sellBranch="",payBranch="";
					
					String sell_Currency=WebDr.getValue("Sell Currency").split("-")[0],pay_Currency = WebDr.getValue("Pay Currency").split("-")[0]; 
					int lcy_range=0,fcy_range=0,fcy_range2=0,pre_trans_number=0;
					System.out.println(WebDr.getValue("Local Currency Flag"));
					String lcyFlag = WebDr.getValue("Local Currency Flag");
					
					HashMap<String, Map<String,String>> store_forex_rate = new HashMap<String, Map<String,String>>();
					HashMap<String, Double> forex_fees=new HashMap<String, Double>();
					HashMap<String, String> storeLedgerAccountNumber = new HashMap<String, String>();
					HashMap<String, String> storeAccountInquiryDetails = new HashMap<String, String>();
					HashMap<String, String> storeNewAccountInquiryDetails = new HashMap<String, String>();
					//HashMap<String, String> storeCashDrawerDetails = new HashMap<String, String>();
					//HashMap<String, String> storeNewCashDrawerDetails = new HashMap<String, String>();
					HashMap<String, String> exchangeAmounts =new HashMap<String, String>();
					Set<String> curreny_list=new HashSet<>();
					HashMap<String, BigDecimal> cd_inquiry = new HashMap<String, BigDecimal>();
					HashMap<String, BigDecimal> cd_inquiry_after = new HashMap<String, BigDecimal>();
					HashMap<String,HashMap<String, BigDecimal>> storeCashDrawerDetails = new HashMap<String,HashMap<String, BigDecimal>>();
					HashMap<String,HashMap<String, BigDecimal>> storeNewCashDrawerDetails = new HashMap<String,HashMap<String, BigDecimal>>();
					
					String exchange_account = WebDr.getValue("Exchange Account"),selling_PayMode = WebDr.getValue("Selling Payment Mode"),paying_PayMode = WebDr.getValue("Paying Payment Mode");
					
					try{
						
					//Fetch teller details
						ROATFunctions.refesh(false,"Teller");
						if (HtmlReporter.tc_result)
						{ 
							user_branch = ROATModules.fetch_user_branch();
							user_id=ROATFunctions.fetch_user_Id();
							user_name = ROATModules.fetchuser();
							Branch=ROATFunctions.fetch_user_branch_name();
							System.out.println(WebDr.getValue("Local Currency Flag"));
							if(Boolean.valueOf(lcyFlag)){
								lcy_range=ROATModules.LCY_Payment_amount("1",1);
							}else{
								lcy_range=ROATModules.FCY_Payment_amount(pay_Currency,"1",1);
							}
							
							
							//lcy_range=ROATModules.LCY_Payment_amount("1",1);
							fcy_range=ROATModules.FCY_Payment_amount(sell_Currency,"1",1);
							
							
						}
						
						
						//verify cash position
						if(HtmlReporter.tc_result){
							ROATModules.navigateOCP();
							boolean ocp = ROATModules.verifyOCP();
						}
						
						
						ROATFunctions.refesh(false, "Super User");
						
						//Ledger Account details for Sell and Pay currency
						if(HtmlReporter.tc_result){
							storeLedgerAccountNumber = ROATModules.getLegderAccountDetails(Branch,WebDr.getValue("Sell Currency"));
							storeLedgerAccountNumber.putAll(ROATModules.getLegderAccountDetails(Branch,WebDr.getValue("Pay Currency")));
						}
						
						ROATFunctions.refesh(false, "Teller");
						if (HtmlReporter.tc_result)WebDr.Wait_Time(1500);
						
						//get ledger and transaction accounts
						if(selling_PayMode.toUpperCase().equals("ACCOUNT")){
							
							if(HtmlReporter.tc_result){
								sellBranch = exchange_account.split("-")[0];
								payBranch = storeLedgerAccountNumber.get(pay_Currency).split("-")[0].replace(":","").trim();
								sellAccountNumber = exchange_account.split("-")[1];
								payAccountNumber = storeLedgerAccountNumber.get(pay_Currency).split("-")[1].trim();
							}
							
						}else if(paying_PayMode.toUpperCase().equals("ACCOUNT")){
							
							if(HtmlReporter.tc_result){
								
								sellBranch = storeLedgerAccountNumber.get(sell_Currency).split("-")[0].replace(":","").trim();
								payBranch = exchange_account.split("-")[0];
								sellAccountNumber = storeLedgerAccountNumber.get(sell_Currency).split("-")[1].trim();
								payAccountNumber = exchange_account.split("-")[1];
							}
						}else{
							
							if(HtmlReporter.tc_result){
								
								sellBranch = storeLedgerAccountNumber.get(sell_Currency).split("-")[0].replace(":","").trim();
								payBranch = storeLedgerAccountNumber.get(pay_Currency).split("-")[0].replace(":","").trim();
								sellAccountNumber = storeLedgerAccountNumber.get(sell_Currency).split("-")[1].trim();
								payAccountNumber = storeLedgerAccountNumber.get(pay_Currency).split("-")[1].trim();
							}
						}
						
						
						//get original balance for ledger and transaction accounts
						
						storeAccountInquiryDetails = ROATModules.account_inquiryExSell(sellAccountNumber,sellBranch);
						storeAccountInquiryDetails.putAll(ROATModules.account_inquiryExSell(payAccountNumber,payBranch));
						
						
						
					
						Thread.sleep(5000);
						//cash drawer inquiry before performing
						if(HtmlReporter.tc_result)
							storeCashDrawerDetails= ROATModules.FCY_Inquiry();
							curreny_list=storeCashDrawerDetails.keySet();
							String temp="";
							for(String set:curreny_list)
							{
								if(set.contains(sell_Currency))
									temp=set;
							}
							cd_inquiry=storeCashDrawerDetails.get(temp);
							System.out.println(cd_inquiry);
							BigDecimal Den_before_bal = cd_inquiry.get("Denomination_balance");
							BigDecimal Tran_before_bal = cd_inquiry.get("Transaction Payment");
							BigDecimal Balance_before_bal = cd_inquiry.get("Balance");
							System.out.println(Den_before_bal);
							System.out.println(Tran_before_bal);
							System.out.println(Balance_before_bal);
						
						// Fetch forex rates 
						ROATFunctions.refesh(false, "Central Supervisor");
						WebDr.SetPageObjects("end_end");
						store_forex_rate = ROATModules.set_forexrateSellPurchase(sell_Currency);
						
						if (HtmlReporter.tc_result)WebDr.Wait_Time(1500);
						
						//if fcy to fcy only then update rate for pay currency
						if(!Boolean.valueOf(lcyFlag)){
							store_forex_rate.putAll(ROATModules.set_forexrateSellPurchase(pay_Currency));
						}else{
							store_forex_rate.putAll(ROATModules.readandStoreForexRate(pay_Currency));
						}
						
						
						//fetch forex fees
						if (HtmlReporter.tc_result)
						{
							forex_fees = ROATModules.forex_sell_fees_setup(sell_Currency,user_branch,fcy_range,store_forex_rate,false);
							System.out.println(forex_fees);
						}
						
						Thread.sleep(6000);
						//get old transaction no from support function
						if(Boolean.valueOf(lcyFlag))
						{
							 pre_trans_number =ROATModules.support_function( true, "lcy", 0,"Forex Sell",payBranch+"-"+payAccountNumber,sellBranch+"-"+sellAccountNumber,String.valueOf(lcy_range), true);
						}
						else
						{
							 pre_trans_number =ROATModules.support_function( true, "fcy", 0,"Forex Sell",payBranch+"-"+payAccountNumber,sellBranch+"-"+sellAccountNumber,String.valueOf(lcy_range), true);
						}

						Thread.sleep(5000);
						ROATFunctions.refesh(false,"Teller");
						//xchange sell flow
						if (HtmlReporter.tc_result)
							{
							exchangeAmounts = ROATModules.e_e_forex_sell(sell_Currency,pay_Currency,selling_PayMode,paying_PayMode,exchange_account,store_forex_rate, forex_fees);
							//ROATModules.e_e_forex_sell("USD","BWP","Cash","Cash",exchange_account,store_forex_rate, forex_fees);
							}
						
						
						if (HtmlReporter.tc_result)WebDr.Wait_Time(15000);
						
						//get updated balance for ledger and transaction accounts
						storeNewAccountInquiryDetails = ROATModules.account_inquiryExSell(sellAccountNumber,sellBranch);
						storeNewAccountInquiryDetails.putAll(ROATModules.account_inquiryExSell(payAccountNumber,payBranch));
						
						
						//get balance figures and compare them
						double oldValue = Double.parseDouble(storeAccountInquiryDetails.get("current_Actual Balance"+payAccountNumber).split(":")[1].trim().replace(",", ""));
						double newValue = Double.parseDouble(storeNewAccountInquiryDetails.get("current_Actual Balance"+payAccountNumber).split(":")[1].trim().replace(",", ""));
						if (HtmlReporter.tc_result)WebDr.Wait_Time(1000);
						
						if(oldValue>newValue && oldValue-newValue==Double.parseDouble(exchangeAmounts.get("Paying Amount").replace(",", "").trim())){
							System.out.println("pay currency has been debited");
							Reporter.ReportEvent("pay currency debit", "pay currency has been debited","pay currency has been debited", true);
							
						}else{
							Reporter.ReportEvent("pay currency debit", "pay currency has been debited","pay currency debit has been failed ", false);
						}
						
						oldValue = Double.parseDouble(storeAccountInquiryDetails.get("current_Actual Balance"+sellAccountNumber).split(":")[1].trim().replace(",", ""));
						newValue = Double.parseDouble(storeNewAccountInquiryDetails.get("current_Actual Balance"+sellAccountNumber).split(":")[1].trim().replace(",", ""));
						
						if(oldValue<newValue){
							System.out.println("sell currency has been credited");
							Reporter.ReportEvent("sell currency credit", "sell currency has been credited","sell currency credit has been passed", true);
						}else{
							Reporter.ReportEvent("sell currency credit", "sell currency has been credited","sell currency credit has been failed ", false);
						}
						
						
						//cash drawer inquiry after performing
						
						if(HtmlReporter.tc_result)
							storeNewCashDrawerDetails= ROATModules.FCY_Inquiry();
							curreny_list=storeNewCashDrawerDetails.keySet();
							//String temp="";
							for(String set:curreny_list)
							{
								if(set.contains(sell_Currency))
									temp=set;
							}
							cd_inquiry_after=storeNewCashDrawerDetails.get(temp);
							System.out.println(cd_inquiry_after);
							
							BigDecimal Den_after_bal = cd_inquiry_after.get("Denomination_balance");
							BigDecimal Tran_after_bal = cd_inquiry_after.get("Transaction Payment");
							BigDecimal Balance_after_bal = cd_inquiry_after.get("Balance");
							System.out.println(Den_after_bal);
							System.out.println(Tran_after_bal);
							System.out.println(Balance_after_bal);
						
						if(!selling_PayMode.equalsIgnoreCase("Account")){
							if(Den_before_bal.compareTo(Den_after_bal)==1 && Tran_after_bal.compareTo(Tran_before_bal)==1 && Balance_before_bal.compareTo(Balance_after_bal)==1)
							{
							Reporter.ReportEvent("Cash Drawer", "Cash Drawer verified after Exchange cell", "Cash Drawer Exchange Successfully", true);
							}
							else
							{
								Reporter.ReportEvent("Cash Drawer", "Cash Drawer verified after Exchange cell", "Cash Drawer Exchange Failed", false);
							}
						}
						
						//delete transaction from support function
						if(Boolean.valueOf(lcyFlag))
						{
							ROATModules.support_function_forex( false, "lcy", pre_trans_number,"Forex Sell",payBranch+"-"+payAccountNumber,sellBranch+"-"+sellAccountNumber,exchangeAmounts.get("Paying Amount").replace(",", "").trim(),exchangeAmounts.get("Selling Amount").replace(",", "").trim(),paying_PayMode, true);
						}
						else
						{
							ROATModules.support_function_forex( false, "fcy", pre_trans_number,"Forex Sell",payBranch+"-"+payAccountNumber,sellBranch+"-"+sellAccountNumber,exchangeAmounts.get("Paying Amount").replace(",", "").trim(),exchangeAmounts.get("Selling Amount").replace(",", "").trim(),paying_PayMode, true);
						}
						
						//to verify rate and charge override
						ROATFunctions.refesh(false,"Teller");
						ROATModules.forex_exchange_sell_override(sell_Currency,pay_Currency,selling_PayMode,paying_PayMode,exchange_account);
						
										
					}catch(Exception e){
						e.printStackTrace();
						Reporter.ReportEvent("performing forex sell end to end", "End to End Forex Sell must be succesful","End to End Forex Sell failed", false);
						
					}		
			
			
			
		}
				
				public static void end_to_end_Forex_Sell_Breaches(){

					//***************************************************Forex Exchange Sell with Breaches*****************************************
					
					String user_branch="",user_id="",user_name="",Branch="",sellAccountNumber="",payAccountNumber="",sellBranch="",payBranch="";
					
					String sell_Currency=WebDr.getValue("Sell Currency").split("-")[0],pay_Currency = WebDr.getValue("Pay Currency").split("-")[0]; 
					int lcy_range=0,fcy_range=0,fcy_range2=0,pre_trans_number=0;
					System.out.println(WebDr.getValue("Local Currency Flag"));
					String lcyFlag = WebDr.getValue("Local Currency Flag");
					
					HashMap<String, Map<String,String>> store_forex_rate = new HashMap<String, Map<String,String>>();
					HashMap<String, Double> forex_fees=new HashMap<String, Double>();
					HashMap<String, String> storeLedgerAccountNumber = new HashMap<String, String>();
					HashMap<String, String> storeAccountInquiryDetails = new HashMap<String, String>();
					HashMap<String, String> storeNewAccountInquiryDetails = new HashMap<String, String>();
					//HashMap<String, String> storeCashDrawerDetails = new HashMap<String, String>();
					//HashMap<String, String> storeNewCashDrawerDetails = new HashMap<String, String>();
					HashMap<String, String> exchangeAmounts =new HashMap<String, String>();
					Set<String> curreny_list=new HashSet<>();
					HashMap<String, BigDecimal> cd_inquiry = new HashMap<String, BigDecimal>();
					HashMap<String, BigDecimal> cd_inquiry_after = new HashMap<String, BigDecimal>();
					HashMap<String,HashMap<String, BigDecimal>> storeCashDrawerDetails = new HashMap<String,HashMap<String, BigDecimal>>();
					HashMap<String,HashMap<String, BigDecimal>> storeNewCashDrawerDetails = new HashMap<String,HashMap<String, BigDecimal>>();
					
					String exchange_account = WebDr.getValue("Exchange Account"),selling_PayMode = WebDr.getValue("Selling Payment Mode"),paying_PayMode = WebDr.getValue("Paying Payment Mode");
					
					try
					{
						
						int midrate=1;
						String currency=WebDr.getValue("ETE_FCY_Account_no").split("\\|")[0];
						String local_gl__number=WebDr.getValue("LCY_GL_no");
						String forex_gl__number=WebDr.getValue("FCY_GL_No");
						HashMap<String, ArrayList<String>> limits_map = new HashMap<String, ArrayList<String>>();
						HashMap<String, String> CreditRisk = new HashMap<String, String>();
						HashMap<String, Map<String,String>> store_matrix = new HashMap<String, Map<String,String>>();
						HashMap<String, Map<String,String>> store_forex = new HashMap<String, Map<String,String>>();

						ROATFunctions.refesh(false,"Teller");
						if (HtmlReporter.tc_result)
						{ 
							user_branch = ROATModules.fetch_user_branch();
							user_id=ROATFunctions.fetch_user_Id();
							user_name = ROATModules.fetchuser();
							Branch=ROATFunctions.fetch_user_branch_name();
							lcy_range=ROATModules.LCY_Payment_amount("1",1);
							fcy_range=ROATModules.FCY_Payment_amount(currency,"1",1);


							// Fetching forex rate 
							ROATFunctions.refesh(false, "Central Supervisor");
							WebDr.SetPageObjects("end_end");
							store_forex_rate = ROATModules.set_forexrateSellPurchase(currency);
							
							//Fetching 

							// Fetching forex matrix 
							ROATFunctions.refesh(false, "Super User");
							if (HtmlReporter.tc_result)ROATModules.Forex_MatrixNavigation();
							store_matrix=ROATModules.fetch_Forex_teller_matrix(Branch,WebDr.getValue("default_branch"));

							//Fetching Authorization remote
							if (HtmlReporter.tc_result)
							{
								//CreditRisk=ROATModules.auto_remo_update(Branch);
								WebDr.Wait_Time(5000);
								if (HtmlReporter.tc_result)ROATModules.setup_navigateLTLimits();
								WebDr.Wait_Time(10000);


								//******************************Breaches verification************************************


								//  Limits set as 'Send for authorization' 
								if (HtmlReporter.tc_result)
								{ 
									//Limit breach------
									//System.out.println(store_forex);
									midrate=Integer.parseInt(store_forex_rate.get("Cash Currency"+currency).get("Sell"));
									String action="Send for Authorization";
									if (HtmlReporter.tc_result)
									{
										WebDr.Wait_Time(10000);
										limits_map=ROATModules.fetch_limits_ALL(user_branch, user_id+"-"+user_name,fcy_range,action);
									}

									ROATFunctions.refesh(false, "Teller");
									if (HtmlReporter.tc_result)ROATModules.forex_sell_navigate();
									if (HtmlReporter.tc_result)ROATModules.Forex_Exchange_Sell_ete(limits_map,CreditRisk,store_matrix,fcy_range,action,midrate,"Cash","Account","USD",WebDr.getValue("Payment Currency"));
								//	if (HtmlReporter.tc_result)ROATModules.Forex_Exchange_Sell_ete(limits_map,CreditRisk,store_matrix,fcy_range,action,midrate,"Account","Cash","USD",WebDr.getValue("Payment Currency"));
								}

								// test cases flag true
								HtmlReporter.tc_result=true; 

								// LCY Transaction with Limits set as 'Decline'
								if (HtmlReporter.tc_result)
								{
									// Limit breach------
									
									midrate=Integer.parseInt(store_forex_rate.get("Cash Currency"+currency).get("Sell"));
									ROATFunctions.refesh(false, "Super User");
									String action="Decline";
									if (HtmlReporter.tc_result)ROATModules.setup_navigateLTLimits();
									WebDr.Wait_Time(10000);
									if (HtmlReporter.tc_result == true)
									{
										WebDr.Wait_Time(10000);
										limits_map=ROATModules.fetch_limits_ALL(user_branch, user_id+"-"+user_name,fcy_range,action);
									}
									ROATFunctions.refesh(false, "Teller");
									if (HtmlReporter.tc_result)ROATModules.forex_sell_navigate();
									if (HtmlReporter.tc_result)ROATModules.Forex_Exchange_Sell_ete(limits_map,CreditRisk,store_matrix,fcy_range,action,midrate,"Cash","Account","USD",WebDr.getValue("Payment Currency"));
								
									}
							}
						}
					}

					catch(Exception e)
					{
						e.getStackTrace();
						Reporter.ReportEvent("forex Exchange Sell Breaches Testcase ", " Testcase should be passed","forex Exchange Sell Breaches Testcase Failed", false);
					}
					
							

				}




	
	public static void E_E_Internal_Transfer() 
	{

		
		try
		{
			int lcy_range=50;
			int fcy_range=100;
			int midrate=1;
			String currency=WebDr.getValue("ETE_FCY_Account_no").split("\\|")[0];
			String user_branch ="";
			String user_id="";
			String user_name = "";
			String Branch="";
			String local_gl__number=WebDr.getValue("LCY_GL_no");
			String forex_gl__number=WebDr.getValue("FCY_GL_No");
			HashMap<String, ArrayList<String>> limits_map = new HashMap<String, ArrayList<String>>();
			HashMap<String, String> CreditRisk = new HashMap<String, String>();
			HashMap<String, Map<String,String>> store_matrix = new HashMap<String, Map<String,String>>();
			HashMap<String, Map<String,String>> store_forex = new HashMap<String, Map<String,String>>();

			ROATFunctions.refesh(false,"Teller");
			if (HtmlReporter.tc_result)
			{ 
				user_branch = ROATModules.fetch_user_branch();
				user_id=ROATFunctions.fetch_user_Id();
				user_name = ROATModules.fetchuser();
				Branch=ROATFunctions.fetch_user_branch_name();
				lcy_range=ROATModules.LCY_Payment_amount("1",1);
				fcy_range=ROATModules.FCY_Payment_amount(currency,"1",1);


				// Fetching forex rate 
				ROATFunctions.refesh(false, "Central Supervisor");
				WebDr.SetPageObjects("end_end");
				store_forex = ROATModules.set_forexrate(currency);

				// Fetching teller matrix 
				ROATFunctions.refesh(false, "Super User");
				if (HtmlReporter.tc_result)ROATModules.setup_TellerMatrixNavigation();
				store_matrix=ROATModules.fetch_teller_matrix(Branch,WebDr.getValue("default_branch"));

				//Fetching Authorization remote
				if (HtmlReporter.tc_result)
				{
					//CreditRisk=ROATModules.auto_remo_update(Branch);	
					if (HtmlReporter.tc_result)ROATModules.setup_navigateLTLimits();


					//******************************Breaches verification************************************


					// LCY Transaction with Limits set as 'Send for authorization' 
					if (HtmlReporter.tc_result)
					{ 
						//LCY Limit breach------mid rate=1
						midrate=1;
						String action="Send for Authorization";
						if (HtmlReporter.tc_result)
						{
							limits_map=ROATModules.fetch_limits_ALL(user_branch, user_id+"-"+user_name,lcy_range,action);
						}


						ROATFunctions.refesh(false, "Teller");
						if (HtmlReporter.tc_result)ROATModules.navigateInternalTransfer();
						if (HtmlReporter.tc_result)ROATModules.Teller_internal_transfer_ete(limits_map,CreditRisk,store_matrix,lcy_range,action,midrate);

					}


					// test cases flag true
					HtmlReporter.tc_result=true; 

					// LCY Transaction with Limits set as 'Decline'
					if (HtmlReporter.tc_result)
					{
						//LCY Limit breach------mid rate=1
						midrate=1;
						ROATFunctions.refesh(false, "Super User");
						String action="Decline";
						if (HtmlReporter.tc_result)ROATModules.setup_navigateLTLimits();
						if (HtmlReporter.tc_result == true)
						{
							limits_map=ROATModules.fetch_limits_ALL(user_branch, user_id+"-"+user_name,lcy_range,action);

						}


						ROATFunctions.refesh(false, "Teller");
						if (HtmlReporter.tc_result)ROATModules.navigateInternalTransfer();
						if (HtmlReporter.tc_result)ROATModules.Teller_internal_transfer_ete(limits_map,CreditRisk,store_matrix,lcy_range,action,midrate);

					}

				    // test cases flag true
					HtmlReporter.tc_result=true; 

					// FCY Transaction with Limits set as Send for authorization
					if (HtmlReporter.tc_result)
					{
						midrate=Integer.parseInt(store_forex.get("Mid Rate").get("Mid Rate"));
						ROATFunctions.refesh(false, "Super User");
						String action="Send for Authorization";
						if (HtmlReporter.tc_result)ROATModules.setup_navigateLTLimits();
						if (HtmlReporter.tc_result == true)
						{
							limits_map=ROATModules.fetch_limits_ALL(user_branch, user_id+"-"+user_name,fcy_range*midrate,action);
						}

						//FCY Limit breach------mid rate

						ROATFunctions.refesh(false, "Teller");
						if (HtmlReporter.tc_result)ROATModules.navigateInternalTransfer();
						if (HtmlReporter.tc_result)ROATModules.Teller_internal_transfer_ete(limits_map,CreditRisk,store_matrix,lcy_range,action,midrate);

					}

				}
				
			}

			/*//sod eod vault close,teller close
			try
				{
				ROATModules.sod_eod_end_to_end();	
				}
				catch(Exception e)
				{
					e.printStackTrace();
					Reporter.ReportEvent("performing sod eod ", "sod eod must be succesful","sod eod failed for deposits", false);
				}*/
		}

		catch(Exception ex)
		{
			ex.printStackTrace();
			Reporter.ReportEvent("Test Case execution is Failed", "end to end Deposit","end to end Deposit executition failed", false);
		}

	}

	//*****************************end to end Forex transfers**********************************************************************
	//Author :Akshay Gopu
			@SuppressWarnings("unchecked")
			public static void end_to_end_forex_transfer()
			{
				
				if(HtmlReporter.tc_result)
				{
					try
					{
						int lcy_range=100;
						int fcy_range=100;
						int midrate=1;
						String currency=WebDr.getValue("ETE_FCY_Account_no").split("\\|")[0];
						String user_branch ="";
						String user_id="";
						String user_name = "";
						String Branch="";
						String local_gl__number=WebDr.getValue("LCY_GL_no");
						String forex_gl__number=WebDr.getValue("FCY_GL_No");
						HashMap<String, ArrayList<String>> limits_map = new HashMap<String, ArrayList<String>>();
						HashMap<String, String> CreditRisk = new HashMap<String, String>();
						HashMap<String, Map<String,String>> store_matrix = new HashMap<String, Map<String,String>>();
						HashMap<String, Map<String,String>> store_forex = new HashMap<String, Map<String,String>>();
						HashMap<String, BigDecimal> forex_fees=new HashMap<String, BigDecimal>();
						ROATFunctions.refesh(false,"Teller");
						if (HtmlReporter.tc_result)
						{ 
							user_branch = ROATModules.fetch_user_branch();
							user_id=ROATFunctions.fetch_user_Id();
							user_name = ROATModules.fetchuser();
							Branch=ROATFunctions.fetch_user_branch_name();
							lcy_range=ROATModules.LCY_Payment_amount("1",1);
							fcy_range=ROATModules.FCY_Payment_amount(currency,"1",1);


							// Fetching forex rate 
							ROATFunctions.refesh(false, "Central Supervisor");
							WebDr.SetPageObjects("end_end");
							store_forex = ROATModules.set_forexrate(currency);

							// Fetching teller matrix 
							ROATFunctions.refesh(false, "Super User");
							if (HtmlReporter.tc_result)ROATModules.Forex_MatrixNavigation();
							store_matrix=ROATModules.fetch_Forex_teller_matrix(Branch,WebDr.getValue("default_branch"));

							//Fetching Authorization remote
							if (HtmlReporter.tc_result)
							{
								//								CreditRisk=ROATModules.auto_remo_update(Branch);store_forex.get("Mid Rate").get("Mid Rate")
								forex_fees = ROATModules.forex_tranfer_fees_setup("",user_branch,fcy_range,Integer.parseInt(store_forex.get("Mid Rate").get("Mid Rate")),false);


								//******************************Breaches verification************************************


								// LCY Transaction with Limits set as 'Send for authorization' 
								if (HtmlReporter.tc_result)
								{ 
									//LCY Limit breach------mid rate=1
									midrate=1;
									String action="Send for Authorization";
									if (HtmlReporter.tc_result)
									{
										if (HtmlReporter.tc_result)ROATModules.setup_navigateLTLimits();
										limits_map=ROATModules.fetch_limits_ALL(user_branch, user_id+"-"+user_name,lcy_range,action);
									}


									ROATFunctions.refesh(false, "Teller");
									if (HtmlReporter.tc_result)ROATModules.navigateExchangeTransfer();
									if (HtmlReporter.tc_result)ROATModules.Exchange_forex_transfer_ete(forex_fees,limits_map,CreditRisk,store_matrix,lcy_range,action,midrate,WebDr.getValue("Credit_Account_FCY"));

								}


								// test cases flag true
								HtmlReporter.tc_result=true; 

								// LCY Transaction with Limits set as 'Decline'
								if (HtmlReporter.tc_result)
								{
									//FCY Limit breach------mid rate=1
									midrate=1;
									ROATFunctions.refesh(false, "Super User");
									String action="Decline";
									if (HtmlReporter.tc_result)ROATModules.setup_navigateLTLimits();
									if (HtmlReporter.tc_result == true)
									{
										limits_map=ROATModules.fetch_limits_ALL(user_branch, user_id+"-"+user_name,lcy_range,action);

									}


									ROATFunctions.refesh(false, "Teller");
									if (HtmlReporter.tc_result == true)ROATModules.navigateExchangeTransfer();
									if (HtmlReporter.tc_result == true)ROATModules.Exchange_forex_transfer_ete(forex_fees,limits_map,CreditRisk,store_matrix,lcy_range,action,midrate,WebDr.getValue("Credit_Account_FCY"));

								}




								// test cases flag true
								HtmlReporter.tc_result=true; 

								// FCY Transaction with Limits set as Send for authorization
								if (HtmlReporter.tc_result)
								{
									midrate=Integer.parseInt(store_forex.get("Cash Currency").get("Buy"));
									ROATFunctions.refesh(false, "Super User");
									String action="Send for Authorization";
									if (HtmlReporter.tc_result)ROATModules.setup_navigateLTLimits();
									if (HtmlReporter.tc_result == true)
									{
										limits_map=ROATModules.fetch_limits_ALL(user_branch, user_id+"-"+user_name,fcy_range*midrate,action);
									}

									//FCY Limit breach------mid rate

									ROATFunctions.refesh(false, "Teller");
									if (HtmlReporter.tc_result == true)ROATModules.navigateExchangeTransfer();
									if (HtmlReporter.tc_result == true)ROATModules.Exchange_forex_transfer_ete(forex_fees,limits_map,CreditRisk,store_matrix,lcy_range,action,midrate,WebDr.getValue("Credit_Account_LCY"));

								}

							}
							System.out.println("store_matrix");
							System.out.println(store_matrix);
							System.out.println();
							System.out.println("credit Risk");
							System.out.println(CreditRisk);
						}



						// test cases flag true
						HtmlReporter.tc_result=true; 






						//	***********************************************end to end******************************
						//end to end---LCY---Transaction--without Breach 
						ROATFunctions.refesh(false,"Super User");
						String action="Send for Authorization";
						midrate=Integer.parseInt(store_forex.get("Cash Currency").get("Buy"));
						if (HtmlReporter.tc_result)ROATModules.setup_navigateLTLimits();
						limits_map=ROATModules.fetch_limits_ALL(user_branch, user_id+"-"+user_name,lcy_range*midrate*2,action);
						
						
						
						midrate=1;
						ROATModules.end_to_end_forextrans(forex_fees,lcy_range,midrate,WebDr.getValue("Credit_Account_FCY"));






						//************************************support functions****************************
						//delete transaction from 'support function'--- local currency 
						HtmlReporter.tc_result=true;
						midrate=1;
						ROATModules.end_to_end_forextrans_supportFunction(store_forex,forex_fees,lcy_range,midrate,WebDr.getValue("Credit_Account_FCY"));

						//delete transaction from 'support function'--- foreign currency 
						HtmlReporter.tc_result=true;
						midrate=Integer.parseInt(store_forex.get("Cash Currency").get("Buy"));
						ROATModules.end_to_end_forextrans_supportFunction(store_forex,forex_fees,fcy_range,midrate,WebDr.getValue("Credit_Account_LCY"));


						HtmlReporter.tc_result=true; 

						//sod eod vault close,teller close
						try
						{
							ROATModules.sod_eod_end_to_end();	
						}
						catch(Exception e)
						{
							e.printStackTrace();
							Reporter.ReportEvent("performing sod eod ", "sod eod must be succesful","sod eod failed for deposits", false);
						}


					}

					catch(Exception ex)
					{
						ex.printStackTrace();
						Reporter.ReportEvent("Test Case execution is Failed", "end to end Deposit","end to end Deposit executition failed", false);
					}
				}
				else
				{
					Reporter.ReportEvent("performing sod", "sod must be successfull","sod failed", false);
				}

			}
			@SuppressWarnings("unchecked")
			public static void e_to_e_frx_trns_breaches_lcy()
			{

				try
				{
					int lcy_range=100;
					int fcy_range=100;
					int midrate=1;
					String currency=WebDr.getValue("ETE_FCY_Account_no").split("\\|")[0];
					String user_branch ="";
					String user_id="";
					String user_name = "";
					String Branch="";
					String local_gl__number=WebDr.getValue("LCY_GL_no");
					String forex_gl__number=WebDr.getValue("FCY_GL_No");
					HashMap<String, ArrayList<String>> limits_map = new HashMap<String, ArrayList<String>>();
					HashMap<String, String> CreditRisk = new HashMap<String, String>();
					HashMap<String, Map<String,String>> store_matrix = new HashMap<String, Map<String,String>>();
					HashMap<String, Map<String,String>> store_forex = new HashMap<String, Map<String,String>>();
					HashMap<String, BigDecimal> forex_fees=new HashMap<String, BigDecimal>();
					ROATFunctions.refesh(false,"Teller");
					if (HtmlReporter.tc_result)
					{ 
						user_branch = ROATModules.fetch_user_branch();
						user_id=ROATFunctions.fetch_user_Id();
						user_name = ROATModules.fetchuser();
						Branch=ROATFunctions.fetch_user_branch_name();
						lcy_range=ROATModules.LCY_Payment_amount("1",1);
						fcy_range=ROATModules.FCY_Payment_amount(currency,"1",1);


						// Fetching forex rate 
						ROATFunctions.refesh(false, "Central Supervisor");
						WebDr.SetPageObjects("end_end");
						store_forex = ROATModules.set_forexrate(currency);

						// Fetching teller matrix 
						ROATFunctions.refesh(false, "Super User");
						if (HtmlReporter.tc_result)ROATModules.Forex_MatrixNavigation();
						store_matrix=ROATModules.fetch_Forex_teller_matrix(Branch,WebDr.getValue("default_branch"));

						//Fetching Authorization remote
						if (HtmlReporter.tc_result)
						{
							//								CreditRisk=ROATModules.auto_remo_update(Branch);store_forex.get("Mid Rate").get("Mid Rate")
							forex_fees = ROATModules.forex_tranfer_fees_setup("",user_branch,fcy_range,Integer.parseInt(store_forex.get("Mid Rate").get("Mid Rate")),false);


							//******************************Breaches verification************************************


							// LCY Transaction with Limits set as 'Send for authorization' 
							if (HtmlReporter.tc_result)
							{ 
								//LCY Limit breach------mid rate=1
								midrate=1;
								String action="Send for Authorization";
								if (HtmlReporter.tc_result)
								{
									if (HtmlReporter.tc_result)ROATModules.setup_navigateLTLimits();
									limits_map=ROATModules.fetch_limits_ALL(user_branch, user_id+"-"+user_name,lcy_range,action);
								}

								ROATFunctions.refesh(false, "Teller");
								if (HtmlReporter.tc_result)ROATModules.navigateExchangeTransfer();
								if (HtmlReporter.tc_result)ROATModules.Exchange_forex_transfer_ete(forex_fees,limits_map,CreditRisk,store_matrix,lcy_range,action,midrate,WebDr.getValue("Credit_Account_FCY"));

							}
						}
					}
				}

				catch(Exception ex)
				{
					ex.printStackTrace();
					Reporter.ReportEvent("Test Case execution is Failed", "end to end Deposit","end to end Deposit executition failed", false);
				}

			}
			@SuppressWarnings("unchecked")
			public static void e_to_e_frx_trns_breaches_lcy_decline()
			{

				try
				{
					int lcy_range=100;
					int fcy_range=100;
					int midrate=1;
					String currency=WebDr.getValue("ETE_FCY_Account_no").split("\\|")[0];
					String user_branch ="";
					String user_id="";
					String user_name = "";
					String Branch="";
					String local_gl__number=WebDr.getValue("LCY_GL_no");
					String forex_gl__number=WebDr.getValue("FCY_GL_No");
					HashMap<String, ArrayList<String>> limits_map = new HashMap<String, ArrayList<String>>();
					HashMap<String, String> CreditRisk = new HashMap<String, String>();
					HashMap<String, Map<String,String>> store_matrix = new HashMap<String, Map<String,String>>();
					HashMap<String, Map<String,String>> store_forex = new HashMap<String, Map<String,String>>();
					HashMap<String, BigDecimal> forex_fees=new HashMap<String, BigDecimal>();
					ROATFunctions.refesh(false,"Teller");
					if (HtmlReporter.tc_result)
					{ 
						user_branch = ROATModules.fetch_user_branch();
						user_id=ROATFunctions.fetch_user_Id();
						user_name = ROATModules.fetchuser();
						Branch=ROATFunctions.fetch_user_branch_name();
						lcy_range=ROATModules.LCY_Payment_amount("1",1);
						fcy_range=ROATModules.FCY_Payment_amount(currency,"1",1);


						// Fetching forex rate 
						ROATFunctions.refesh(false, "Central Supervisor");
						WebDr.SetPageObjects("end_end");
						store_forex = ROATModules.set_forexrate(currency);

						// Fetching teller matrix 
						ROATFunctions.refesh(false, "Super User");
						if (HtmlReporter.tc_result)ROATModules.Forex_MatrixNavigation();
						store_matrix=ROATModules.fetch_Forex_teller_matrix(Branch,WebDr.getValue("default_branch"));

						//Fetching Authorization remote
						if (HtmlReporter.tc_result)
						{
							//								CreditRisk=ROATModules.auto_remo_update(Branch);store_forex.get("Mid Rate").get("Mid Rate")
							forex_fees = ROATModules.forex_tranfer_fees_setup("",user_branch,fcy_range,Integer.parseInt(store_forex.get("Mid Rate").get("Mid Rate")),false);


							//******************************Breaches verification************************************



							// LCY Transaction with Limits set as 'Decline'
							if (HtmlReporter.tc_result)
							{
								//FCY Limit breach------mid rate=1
								midrate=1;
								ROATFunctions.refesh(false, "Super User");
								String action="Decline";
								if (HtmlReporter.tc_result)ROATModules.setup_navigateLTLimits();
								if (HtmlReporter.tc_result == true)
								{
									limits_map=ROATModules.fetch_limits_ALL(user_branch, user_id+"-"+user_name,lcy_range,action);

								}


								ROATFunctions.refesh(false, "Teller");
								if (HtmlReporter.tc_result == true)ROATModules.navigateExchangeTransfer();
								if (HtmlReporter.tc_result == true)ROATModules.Exchange_forex_transfer_ete(forex_fees,limits_map,CreditRisk,store_matrix,lcy_range,action,midrate,WebDr.getValue("Credit_Account_FCY"));
							}
						}
					}
				}

				catch(Exception ex)
				{
					ex.printStackTrace();
					Reporter.ReportEvent("Test Case execution is Failed", "end to end Deposit","end to end Deposit executition failed", false);
				}

			}
			@SuppressWarnings("unchecked")
			public static void e_to_e_frx_trns_breaches_fcy()
			{

				try
				{
					int lcy_range=100;
					int fcy_range=100;
					int midrate=1;
					String currency=WebDr.getValue("ETE_FCY_Account_no").split("\\|")[0];
					String user_branch ="";
					String user_id="";
					String user_name = "";
					String Branch="";
					String local_gl__number=WebDr.getValue("LCY_GL_no");
					String forex_gl__number=WebDr.getValue("FCY_GL_No");
					HashMap<String, ArrayList<String>> limits_map = new HashMap<String, ArrayList<String>>();
					HashMap<String, String> CreditRisk = new HashMap<String, String>();
					HashMap<String, Map<String,String>> store_matrix = new HashMap<String, Map<String,String>>();
					HashMap<String, Map<String,String>> store_forex = new HashMap<String, Map<String,String>>();
					HashMap<String, BigDecimal> forex_fees=new HashMap<String, BigDecimal>();
					ROATFunctions.refesh(false,"Teller");
					if (HtmlReporter.tc_result)
					{ 
						user_branch = ROATModules.fetch_user_branch();
						user_id=ROATFunctions.fetch_user_Id();
						user_name = ROATModules.fetchuser();
						Branch=ROATFunctions.fetch_user_branch_name();
						lcy_range=ROATModules.LCY_Payment_amount("1",1);
						fcy_range=ROATModules.FCY_Payment_amount(currency,"1",1);


						// Fetching forex rate 
						ROATFunctions.refesh(false, "Central Supervisor");
						WebDr.SetPageObjects("end_end");
						store_forex = ROATModules.set_forexrate(currency);

						// Fetching teller matrix 
						ROATFunctions.refesh(false, "Super User");
						if (HtmlReporter.tc_result)ROATModules.Forex_MatrixNavigation();
						store_matrix=ROATModules.fetch_Forex_teller_matrix(Branch,WebDr.getValue("default_branch"));

						//Fetching Authorization remote
						if (HtmlReporter.tc_result)
						{
							//								CreditRisk=ROATModules.auto_remo_update(Branch);store_forex.get("Mid Rate").get("Mid Rate")
							forex_fees = ROATModules.forex_tranfer_fees_setup("",user_branch,fcy_range,Integer.parseInt(store_forex.get("Mid Rate").get("Mid Rate")),false);


							//******************************Breaches verification************************************


							// FCY Transaction with Limits set as Send for authorization
							if (HtmlReporter.tc_result)
							{
								midrate=Integer.parseInt(store_forex.get("Cash Currency").get("Buy"));
								ROATFunctions.refesh(false, "Super User");
								String action="Send for Authorization";
								if (HtmlReporter.tc_result)ROATModules.setup_navigateLTLimits();
								if (HtmlReporter.tc_result == true)
								{
									limits_map=ROATModules.fetch_limits_ALL(user_branch, user_id+"-"+user_name,fcy_range*midrate,action);
								}

								//FCY Limit breach------mid rate

								ROATFunctions.refesh(false, "Teller");
								if (HtmlReporter.tc_result == true)ROATModules.navigateExchangeTransfer();
								if (HtmlReporter.tc_result == true)ROATModules.Exchange_forex_transfer_ete(forex_fees,limits_map,CreditRisk,store_matrix,lcy_range,action,midrate,WebDr.getValue("Credit_Account_LCY"));

							}
						}
					}
				}

				catch(Exception ex)
				{
					ex.printStackTrace();
					Reporter.ReportEvent("Test Case execution is Failed", "end to end Deposit","end to end Deposit executition failed", false);
				}
			}
			
			@SuppressWarnings("unchecked")
			public static void e_to_e_frx_trns_support_functions_lcy()
			{

				try
				{
					int lcy_range=100;
					int fcy_range=100;
					int midrate=1;
					String currency=WebDr.getValue("ETE_FCY_Account_no").split("\\|")[0];
					String user_branch ="";
					String user_id="";
					String user_name = "";
					String Branch="";
					String local_gl__number=WebDr.getValue("LCY_GL_no");
					String forex_gl__number=WebDr.getValue("FCY_GL_No");
					HashMap<String, ArrayList<String>> limits_map = new HashMap<String, ArrayList<String>>();
					HashMap<String, String> CreditRisk = new HashMap<String, String>();
					HashMap<String, Map<String,String>> store_matrix = new HashMap<String, Map<String,String>>();
					HashMap<String, Map<String,String>> store_forex = new HashMap<String, Map<String,String>>();
					HashMap<String, BigDecimal> forex_fees=new HashMap<String, BigDecimal>();
					ROATFunctions.refesh(false,"Teller");
					if (HtmlReporter.tc_result)
					{ 
						user_branch = ROATModules.fetch_user_branch();
						user_id=ROATFunctions.fetch_user_Id();
						user_name = ROATModules.fetchuser();
						Branch=ROATFunctions.fetch_user_branch_name();
						lcy_range=ROATModules.LCY_Payment_amount("1",1);
						fcy_range=ROATModules.FCY_Payment_amount(currency,"1",1);


						// Fetching forex rate 
						ROATFunctions.refesh(false, "Central Supervisor");
						WebDr.SetPageObjects("end_end");
						store_forex = ROATModules.set_forexrate(currency);

						// Fetching teller matrix 
						ROATFunctions.refesh(false, "Super User");
						if (HtmlReporter.tc_result)ROATModules.Forex_MatrixNavigation();
						store_matrix=ROATModules.fetch_Forex_teller_matrix(Branch,WebDr.getValue("default_branch"));

						//Fetching Authorization remote
						if (HtmlReporter.tc_result)
						{
							//								CreditRisk=ROATModules.auto_remo_update(Branch);store_forex.get("Mid Rate").get("Mid Rate")
							forex_fees = ROATModules.forex_tranfer_fees_setup("",user_branch,fcy_range,Integer.parseInt(store_forex.get("Mid Rate").get("Mid Rate")),false);


							//	***********************************************end to end******************************
							//end to end---LCY---Transaction--without Breach 
							ROATFunctions.refesh(false,"Super User");
							String action="Send for Authorization";
							midrate=Integer.parseInt(store_forex.get("Cash Currency").get("Buy"));
							if (HtmlReporter.tc_result)ROATModules.setup_navigateLTLimits();
							limits_map=ROATModules.fetch_limits_ALL(user_branch, user_id+"-"+user_name,lcy_range*midrate*2,action);
							
							
							
							midrate=1;
							ROATModules.end_to_end_forextrans(forex_fees,lcy_range,midrate,WebDr.getValue("Credit_Account_FCY"));






							//************************************support functions****************************
							//delete transaction from 'support function'--- local currency 
							HtmlReporter.tc_result=true;
							midrate=1;
							ROATModules.end_to_end_forextrans_supportFunction(store_forex,forex_fees,lcy_range,midrate,WebDr.getValue("Credit_Account_FCY"));
						}
					}
				}

				catch(Exception ex)
				{
					ex.printStackTrace();
					Reporter.ReportEvent("Test Case execution is Failed", "end to end Deposit","end to end Deposit executition failed", false);
				}

			}
			@SuppressWarnings("unchecked")
			public static void e_to_e_frx_trns_support_functions_fcy()
			{

				try
				{
					int lcy_range=100;
					int fcy_range=100;
					int midrate=1;
					String currency=WebDr.getValue("ETE_FCY_Account_no").split("\\|")[0];
					String user_branch ="";
					String user_id="";
					String user_name = "";
					String Branch="";
					String local_gl__number=WebDr.getValue("LCY_GL_no");
					String forex_gl__number=WebDr.getValue("FCY_GL_No");
					HashMap<String, ArrayList<String>> limits_map = new HashMap<String, ArrayList<String>>();
					HashMap<String, String> CreditRisk = new HashMap<String, String>();
					HashMap<String, Map<String,String>> store_matrix = new HashMap<String, Map<String,String>>();
					HashMap<String, Map<String,String>> store_forex = new HashMap<String, Map<String,String>>();
					HashMap<String, BigDecimal> forex_fees=new HashMap<String, BigDecimal>();
					ROATFunctions.refesh(false,"Teller");
					if (HtmlReporter.tc_result)
					{ 
						user_branch = ROATModules.fetch_user_branch();
						user_id=ROATFunctions.fetch_user_Id();
						user_name = ROATModules.fetchuser();
						Branch=ROATFunctions.fetch_user_branch_name();
						lcy_range=ROATModules.LCY_Payment_amount("1",1);
						fcy_range=ROATModules.FCY_Payment_amount(currency,"1",1);


						// Fetching forex rate 
						ROATFunctions.refesh(false, "Central Supervisor");
						WebDr.SetPageObjects("end_end");
						store_forex = ROATModules.set_forexrate(currency);

						// Fetching teller matrix 
						ROATFunctions.refesh(false, "Super User");
						if (HtmlReporter.tc_result)ROATModules.Forex_MatrixNavigation();
						store_matrix=ROATModules.fetch_Forex_teller_matrix(Branch,WebDr.getValue("default_branch"));

						//Fetching Authorization remote
						if (HtmlReporter.tc_result)
						{
							//								CreditRisk=ROATModules.auto_remo_update(Branch);store_forex.get("Mid Rate").get("Mid Rate")
							forex_fees = ROATModules.forex_tranfer_fees_setup("",user_branch,fcy_range,Integer.parseInt(store_forex.get("Mid Rate").get("Mid Rate")),false);




							//	***********************************************end to end******************************
							//end to end---LCY---Transaction--without Breach 
							ROATFunctions.refesh(false,"Super User");
							String action="Send for Authorization";
							midrate=Integer.parseInt(store_forex.get("Cash Currency").get("Buy"));
							if (HtmlReporter.tc_result)ROATModules.setup_navigateLTLimits();
							limits_map=ROATModules.fetch_limits_ALL(user_branch, user_id+"-"+user_name,fcy_range*midrate*2,action);
							

							//delete transaction from 'support function'--- foreign currency 
							HtmlReporter.tc_result=true;
							midrate=Integer.parseInt(store_forex.get("Cash Currency").get("Buy"));
							ROATModules.end_to_end_forextrans_supportFunction(store_forex,forex_fees,fcy_range,midrate,WebDr.getValue("Credit_Account_LCY"));


							HtmlReporter.tc_result=true; 

							//sod eod vault close,teller close
							try
							{
								ROATModules.sod_eod_end_to_end();	
							}
							catch(Exception e)
							{
								e.printStackTrace();
								Reporter.ReportEvent("performing sod eod ", "sod eod must be succesful","sod eod failed for deposits", false);
							}
						}
					}
				}

				catch(Exception ex)
				{
					ex.printStackTrace();
					Reporter.ReportEvent("Test Case execution is Failed", "end to end Deposit","end to end Deposit executition failed", false);
				}

			}
//*****************************end to end Mixed deposits**********************************************************************
//Author :Chelsea D	
			
			public static void end_to_end_mixed_deposit()
			{
				if(HtmlReporter.tc_result)
			{
				try
				{
					int lcy_range=50;
					int fcy_range=100;
					int midrate=1;
					String currency=WebDr.getValue("ETE_FCY_Account_no").split("\\|")[0];
					String user_branch ="";
					String user_id="";
					String user_name = "";
					String Branch="";
					String local_gl__number=WebDr.getValue("LCY_GL_no");
					String forex_gl__number=WebDr.getValue("FCY_GL_No");
					HashMap<String, ArrayList<String>> limits_map = new HashMap<String, ArrayList<String>>();
					HashMap<String, String> CreditRisk = new HashMap<String, String>();
					HashMap<String, Map<String,String>> store_matrix = new HashMap<String, Map<String,String>>();
					HashMap<String, Map<String,String>> store_forex = new HashMap<String, Map<String,String>>();

					ROATFunctions.refesh(false,"Teller");
					if (HtmlReporter.tc_result)
					{ 
						user_branch = ROATModules.fetch_user_branch();
						user_id=ROATFunctions.fetch_user_Id();
						user_name = ROATModules.fetchuser();
						Branch=ROATFunctions.fetch_user_branch_name();
						lcy_range=ROATModules.LCY_Payment_amount("1",1);
						fcy_range=ROATModules.FCY_Payment_amount(currency,"1",1);


						// Fetching forex rate 
						ROATFunctions.refesh(false, "Central Supervisor");
						WebDr.SetPageObjects("end_end");
						store_forex = ROATModules.set_forexrate(currency);

						// Fetching teller matrix 
						ROATFunctions.refesh(false, "Super User");
						if (HtmlReporter.tc_result)ROATModules.setup_TellerMatrixNavigation();
						store_matrix=ROATModules.fetch_teller_matrix(Branch,WebDr.getValue("default_branch"));

						//Fetching Authorization remote
						if (HtmlReporter.tc_result)
						{
							//CreditRisk=ROATModules.auto_remo_update(Branch);	
							if (HtmlReporter.tc_result)ROATModules.setup_navigateLTLimits();


							//******************************Breaches verification************************************


							// LCY Transaction with Limits set as 'Send for authorization' 
							if (HtmlReporter.tc_result)
							{
								//LCY Limit breach------mid rate=1
								midrate=1;
								String action="Send for Authorization";
								if (HtmlReporter.tc_result)
								{
									limits_map=ROATModules.fetch_limits_ALL(user_branch, user_id+"-"+user_name,lcy_range,action);
								}


								ROATFunctions.refesh(false, "Teller");
								if (HtmlReporter.tc_result)ROATModules.Teller_navigateMixedDeposit();
								if (HtmlReporter.tc_result)ROATModules.Teller_mixed_deposit_ete(limits_map,CreditRisk,store_matrix,lcy_range,action,midrate);

							}
						// test cases flag true
							HtmlReporter.tc_result=true; 

							// LCY Transaction with Limits set as 'Decline'
							if (HtmlReporter.tc_result)
							{
								//LCY Limit breach------mid rate=1
								midrate=1;
								ROATFunctions.refesh(false, "Super User");
								String action="Decline";
								if (HtmlReporter.tc_result)ROATModules.setup_navigateLTLimits();
								if (HtmlReporter.tc_result == true)
								{
								  limits_map=ROATModules.fetch_limits_ALL(user_branch, user_id+"-"+user_name,lcy_range,action);

								}

								WebDr.Wait_Time(100);
								ROATFunctions.refesh(false, "Teller");
								if (HtmlReporter.tc_result)ROATModules.Teller_navigateMixedDeposit();
								if (HtmlReporter.tc_result)ROATModules.Teller_mixed_deposit_ete(limits_map,CreditRisk,store_matrix,lcy_range,action,midrate);

							}




							// test cases flag true
							HtmlReporter.tc_result=true; 

							// FCY Transaction with Limits set as Send for authorization
							if (HtmlReporter.tc_result)
							{
								midrate=Integer.parseInt(store_forex.get("Mid Rate").get("Mid Rate"));
								ROATFunctions.refesh(false, "Super User");
								String action="Send for Authorization";
								if (HtmlReporter.tc_result)ROATModules.setup_navigateLTLimits();
								if (HtmlReporter.tc_result == true)
								{
									limits_map=ROATModules.fetch_limits_ALL(user_branch, user_id+"-"+user_name,fcy_range*midrate,action);
								}

								//FCY Limit breach------mid rate
								WebDr.Wait_Time(100);
								ROATFunctions.refesh(false, "Teller");
								if (HtmlReporter.tc_result)ROATModules.Teller_navigateMixedDeposit();
								if (HtmlReporter.tc_result)ROATModules.Teller_mixed_deposit_ete(limits_map,CreditRisk,store_matrix,fcy_range,action,midrate);

							}

						}
						System.out.println("store_matrix");
						System.out.println(store_matrix);
						System.out.println();
						System.out.println("credit Risk");
						System.out.println(CreditRisk);
					}




					// test cases flag true
					HtmlReporter.tc_result=true; 





					//	***********************************************end to end******************************
					//end to end---LCY---Transaction--without Breach 
					ROATFunctions.refesh(false,"Super User");
					String action="Send for Authorization";
					midrate=Integer.parseInt(store_forex.get("Mid Rate").get("Mid Rate"));
					if (HtmlReporter.tc_result)ROATModules.setup_navigateLTLimits();
					limits_map=ROATModules.fetch_limits_ALL(user_branch, user_id+"-"+user_name,lcy_range*midrate*2,action);
					
					
					midrate=1;
					ROATModules.end_to_end_mixed_deposit(lcy_range,midrate,local_gl__number);



					//************************************support functions****************************

					HtmlReporter.tc_result=true;
					//delete transaction from 'support function'--- local currency
					midrate=1;
					ROATModules.end_to_end_mixed_deposit_support_function(lcy_range,midrate,local_gl__number);

					ROATFunctions.refesh(false,"Super User");
					//String action="Send for Authorization";
					midrate=Integer.parseInt(store_forex.get("Mid Rate").get("Mid Rate"));
					if (HtmlReporter.tc_result)ROATModules.setup_navigateLTLimits();
					limits_map=ROATModules.fetch_limits_ALL(user_branch, user_id+"-"+user_name,fcy_range*midrate*2,action);
					
					
					HtmlReporter.tc_result=true;
					//delete transaction from 'support function'--- foreign currency 
					midrate=Integer.parseInt(store_forex.get("Mid Rate").get("Mid Rate"));
					ROATModules.end_to_end_mixed_deposit_support_function(fcy_range,midrate,forex_gl__number);


					//************************************Deposit with cashback****************************
					HtmlReporter.tc_result=true;
					ROATModules.end_end_mixed_deposit_cashback();

					//sod eod vault close,teller close
					/*try
					{
						ROATModules.sod_eod_end_to_end();	
					}
					catch(Exception e)
					{
						e.printStackTrace();
						Reporter.ReportEvent("performing sod eod ", "sod eod must be succesful","sod eod failed for deposits", false);
					}*/
				}


				catch(Exception ex)
				{
					ex.printStackTrace();
					Reporter.ReportEvent("Test Case execution is Failed", "end to end Deposit","end to end Deposit executition failed", false);
				}


			}
			else
			{
				Reporter.ReportEvent("performing sod", "sod must be successfull","sod failed", false);
			}
		
			
			
		}
			
			


			//////////////////////////////////////////////////////End to End Draft Issue ////////////////////////////////////////////////////////////////////////////////////////////


			public static void E_E_ForexDraftIssue() 
			{
				HashMap<String, String> LCYaccount_details = new HashMap<String, String>();
				HashMap<String, String> FCYaccount_details = new HashMap<String, String>();
				HashMap <String, String> invertoryDraftBillMap = new HashMap <String, String>();
				HashMap <String, String> corrBankMaintenanceMap = new HashMap <String, String>();
				HashMap<String, String> storeLedgerAccountNumber = new HashMap<String, String>();
				HashMap<String, Double> forex_fees=new HashMap<String, Double>();
				BigDecimal DraftAmount=BigDecimal.ZERO;
				String DraftData="";
				String user_branch="",user_id="",user_name="",Branch="",payAccountNumber="";
				int lcy_range=0,fcy_range=0, midrate=1;
				String currency=WebDr.getValue("ETE_FCY_Account_no").split("\\|")[0];
				String Mbranch=WebDr.getValue("Account Currency").split("\\|")[0];
				String Mcurrency=WebDr.getValue("Account Currency").split("\\|")[1];
				String CurrAccountNumber="",CurrAccBranch="";
				//String DraftType=WebDr.getValue("DraftType");
				String Voucher="";
				int pre_trans_number=0;
				String DraftNumber="";
				String local_gl__number=WebDr.getValue("LCY_GL_no");
				String forex_gl__number=WebDr.getValue("FCY_GL_No");
				String lcyFlag = WebDr.getValue("Local Currency Flag");
				HashMap<String, ArrayList<String>> limits_map = new HashMap<String, ArrayList<String>>();
				HashMap<String, String> CreditRisk = new HashMap<String, String>();
				HashMap<String, Map<String,String>> store_matrix = new HashMap<String, Map<String,String>>();
				HashMap<String, Map<String,String>> store_forex = new HashMap<String, Map<String,String>>();

				try
				{
					ROATFunctions.refesh(false, "Teller");
					user_branch = ROATModules.fetch_user_branch();
					user_id=ROATFunctions.fetch_user_Id();
					user_name = ROATModules.fetchuser();
					Branch=ROATFunctions.fetch_user_branch_name();
					lcy_range=ROATModules.LCY_Payment_amount("1",1);
					fcy_range=ROATModules.FCY_Payment_amount(currency,"1",1);

					ROATFunctions.refesh(false, "Supervisor");
					if (HtmlReporter.tc_result){
						ROATModules.navigateInventoryDraftBill();
						invertoryDraftBillMap = ROATModules.updateStoreInventoryDraftBill();
						System.out.println(invertoryDraftBillMap);

					}

					if (HtmlReporter.tc_result){
						ROATModules.navigateCorrBankMaint();
						corrBankMaintenanceMap = ROATModules.getCorrBankMainDetails();
						System.out.println(corrBankMaintenanceMap);
					}

					// Fetching forex rate 
					if (HtmlReporter.tc_result){
						ROATFunctions.refesh(false, "Central Supervisor");
						WebDr.SetPageObjects("end_end");
						store_forex=ROATModules.set_forexrateSellPurchase(currency);
					}
					ROATFunctions.refesh(false, "Super User");

					if (HtmlReporter.tc_result){
						ROATModules.navigate_MICRTypes();
						Voucher = ROATModules.getMICRTypesVoucher();
						System.out.println(Voucher);
					}

					//forex fees
					if (HtmlReporter.tc_result)
					{
						forex_fees = ROATModules.forex_sell_fees_setup(currency,user_branch,fcy_range,store_forex,false);
						System.out.println(forex_fees);
					}

					if (HtmlReporter.tc_result)
					{
						if (HtmlReporter.tc_result)ROATModules.setup_navigateLTLimits();
						ROATModules.fetch_limits_ALL(user_branch, user_id+"-"+user_name,fcy_range,"Send for Authorization");
					}

					//fetching maintenance account
					if (HtmlReporter.tc_result)
						storeLedgerAccountNumber=ROATModules.getLegderAccMaintenaceDetails(Mbranch, Mcurrency);
					System.out.println(storeLedgerAccountNumber);

					if(HtmlReporter.tc_result){

						CurrAccBranch = storeLedgerAccountNumber.get(Mcurrency).split("-")[0].replace(":","").trim();

						CurrAccountNumber = storeLedgerAccountNumber.get(Mcurrency).split("-")[1].trim();

					}
					// to get the transaction number before performing draft issue
					if(Boolean.valueOf(lcyFlag))
					{
						pre_trans_number =ROATModules.support_function_forexDraft( false, "lcy", pre_trans_number,"Draft Issue",WebDr.getValue("LCY_Account"),DraftAmount,true);
					}
					else
					{
						pre_trans_number =ROATModules.support_function_forexDraft( true, "lcy", pre_trans_number,"Draft Issue",WebDr.getValue("LCY_Account"),DraftAmount,true);
					}	


					// to inquire maintenance account
					if (HtmlReporter.tc_result)ROATFunctions.refesh(false,"Teller");
					LCYaccount_details=ROATModules.account_inquiry(CurrAccountNumber,CurrAccBranch);
					String before_balance_Macc =(LCYaccount_details.get("current_Available Balance")).replace(":","").replace(",", "");
					String before_amount_Macc = before_balance_Macc.trim();
					System.out.println(before_amount_Macc);
					BigDecimal before_Macc_balance=new BigDecimal(before_amount_Macc);		

		/// LCY to FCY End to End
					// User Account Information before adding draft
					if (HtmlReporter.tc_result)ROATFunctions.refesh(false,"Teller");
					LCYaccount_details=ROATModules.account_inquiry(WebDr.getValue("LCY_Account").split("\\-")[1],WebDr.getValue("LCY_Account").split("\\-")[0]);
					String before_balance =(LCYaccount_details.get("current_Available Balance")).replace(":","").replace(",", "");
					String before_amount = before_balance.trim();
					System.out.println(before_amount);
					BigDecimal before_Draft_balance=new BigDecimal(before_amount);

					// Draft Issue
					if (HtmlReporter.tc_result)ROATModules.DraftIssue_Navigation();
					if (HtmlReporter.tc_result)DraftData =ROATModules.forexDraftsIssue("6 Sundry Draft",WebDr.getValue("LCY_Account"),"No",Voucher);
					String Amount=DraftData.split("-")[0].trim();
					DraftNumber=DraftData.split("-")[1].trim();
					DraftAmount = new BigDecimal(Amount);
					WebDr.Wait_Time(1000);

					// Account Information after adding draft
					if (HtmlReporter.tc_result)ROATFunctions.refesh(false,"Teller");
					LCYaccount_details=ROATModules.account_inquiry(WebDr.getValue("LCY_Account").split("\\-")[1],WebDr.getValue("LCY_Account").split("\\-")[0]);
					String after_balance =(LCYaccount_details.get("current_Available Balance")).replace(":","").replace(",", "");
					String after_amount = after_balance.trim();
					BigDecimal after_Draft_balance=new BigDecimal(after_amount);
					//System.out.println(before_Draft_balance);
					System.out.println(DraftAmount);
					System.out.println(after_Draft_balance);

					//Verifying the draft added and printed or not
					if(after_Draft_balance.equals(before_Draft_balance.subtract(DraftAmount)))
					{
						Reporter.ReportEvent("Draft Issue", "Draft issue addded Successfully", "Draft Issue added", true);
					}
					else
					{
						Reporter.ReportEvent("Draft Issue", "Draft issue addded Successfully", "Draft Issue Failed", false);
					}

					// to inquire maintenance account after draft issue
					if (HtmlReporter.tc_result)ROATFunctions.refesh(false,"Teller");
					LCYaccount_details=ROATModules.account_inquiry(CurrAccountNumber,CurrAccBranch);
					String after_balance_Macc =(LCYaccount_details.get("current_Available Balance")).replace(":","").replace(",", "");
					String after_amount_Macc = after_balance_Macc.trim();
					System.out.println(after_amount_Macc);
					BigDecimal after_Macc_balance=new BigDecimal(after_amount_Macc);

					//to verify currency maintenance account
					if(after_Macc_balance.compareTo(before_Macc_balance)==1)
					{
						Reporter.ReportEvent("Draft Issue", "Account maintenance verification", "Passed", true);
					}
					else
					{
						Reporter.ReportEvent("Draft Issue", "Account maintenance verification", "Failed", false);
					}

					// Draft View
					if (HtmlReporter.tc_result)ROATModules.DraftView_Navigation();
					if (HtmlReporter.tc_result)ROATModules.forexDraftsView("6 Sundry Draft",DraftNumber);

					// Draft Pending and printing the satus
					if (HtmlReporter.tc_result)ROATModules.DraftPending_Navigation();
					if (HtmlReporter.tc_result)ROATModules.forexDraftsPending("Yes",DraftNumber);	

					//delete transaction from support function

						if(Boolean.valueOf(lcyFlag))
						{
						ROATModules.support_function_forexDraft( false, "lcy", pre_trans_number,"Draft Issue",WebDr.getValue("LCY_Account"),DraftAmount,true);
						}
						else
						{
							ROATModules.support_function_forexDraft( false, "lcy", pre_trans_number,"Draft Issue",WebDr.getValue("LCY_Account"),DraftAmount, true);
						}
				
						if (HtmlReporter.tc_result)
						{
							Reporter.ReportEvent("Draft Issue LCY to FCY ", "Draft Issue LCY to FCY should be Passed","Draft Issue LCY to FCY Passed", true);
						}
						else
						{
							Reporter.ReportEvent("Draft Issue LCY to FCY ", "Draft Issue LCY to FCY should be Passed","Draft Issue LCY to FCY Failed", false);
						}
		/// LCY to LCY End to End 
						
						// User Account Information before adding draft
						if (HtmlReporter.tc_result)ROATFunctions.refesh(false,"Teller");
						LCYaccount_details=ROATModules.account_inquiry(WebDr.getValue("LCY_Account").split("\\-")[1],WebDr.getValue("LCY_Account").split("\\-")[0]);
						String LCYbefore_balance =(LCYaccount_details.get("current_Available Balance")).replace(":","").replace(",", "");
						String LCYbefore_amount = LCYbefore_balance.trim();
						System.out.println(LCYbefore_amount);
						BigDecimal LCYbefore_Draft_balance=new BigDecimal(LCYbefore_amount);
						
						// Draft Issue
						if (HtmlReporter.tc_result)ROATModules.DraftIssue_Navigation();
						if (HtmlReporter.tc_result)DraftData =ROATModules.forexDraftsIssue("1 Managers Cheque",WebDr.getValue("LCY_Account"),"Yes",Voucher);
						String LCYAmount=DraftData.split("-")[0].trim();
						DraftNumber=DraftData.split("-")[1].trim();
						DraftAmount = new BigDecimal(LCYAmount);
						WebDr.Wait_Time(1000);

						// Account Information after adding draft
						if (HtmlReporter.tc_result)ROATFunctions.refesh(false,"Teller");
						LCYaccount_details=ROATModules.account_inquiry(WebDr.getValue("LCY_Account").split("\\-")[1],WebDr.getValue("LCY_Account").split("\\-")[0]);
						String LCYafter_balance =(LCYaccount_details.get("current_Available Balance")).replace(":","").replace(",", "");
						String LCYafter_amount = LCYafter_balance.trim();
						BigDecimal LCYafter_Draft_balance=new BigDecimal(LCYafter_amount);
						
						//Verifying the draft added and printed or not
						if(LCYafter_Draft_balance.equals(LCYbefore_Draft_balance.subtract(DraftAmount)))
						{
							Reporter.ReportEvent("Draft Issue", "Draft issue addded Successfully", "Draft Issue added", true);
						}
						else
						{
							Reporter.ReportEvent("Draft Issue", "Draft issue addded Successfully", "Draft Issue Failed", false);
						}
						// Draft View
						if (HtmlReporter.tc_result)ROATModules.DraftView_Navigation();
						if (HtmlReporter.tc_result)ROATModules.forexDraftsView("1 Managers Cheque",DraftNumber);

					if (HtmlReporter.tc_result)
					{
						Reporter.ReportEvent("Draft Issue LCY to LCY ", "Draft Issue LCY to LCY should be Passed","Draft Issue LCY to LCY Passed", true);
					}
					else
					{
						Reporter.ReportEvent("Draft Issue LCY to LCY ", "Draft Issue LCY to LCY should be Passed","Draft Issue LCY to LCY Failed", false);
					}
						
			/// FCY to FCY End to End 
						// User Account Information before adding draft
						if (HtmlReporter.tc_result)ROATFunctions.refesh(false,"Teller");
						FCYaccount_details=ROATModules.account_inquiry(WebDr.getValue("FCY_Account").split("\\-")[1],WebDr.getValue("FCY_Account").split("\\-")[0]);
						String FCYbefore_balance =(FCYaccount_details.get("current_Available Balance")).replace(":","").replace(",", "");
						String FCYbefore_amount = FCYbefore_balance.trim();
						System.out.println(FCYbefore_amount);
						BigDecimal FCYbefore_Draft_balance=new BigDecimal(FCYbefore_amount);
						
						// Draft Issue
						if (HtmlReporter.tc_result)ROATModules.DraftIssue_Navigation();
						if (HtmlReporter.tc_result)DraftData =ROATModules.forexDraftsIssue("4 USD Draft",WebDr.getValue("FCY_Account"),"No",Voucher);
						String FCYAmount=DraftData.split("-")[0].trim();
						DraftNumber=DraftData.split("-")[1].trim();
						DraftAmount = new BigDecimal(FCYAmount);
						WebDr.Wait_Time(1000);

						// Account Information after adding draft
						if (HtmlReporter.tc_result)ROATFunctions.refesh(false,"Teller");
						FCYaccount_details=ROATModules.account_inquiry(WebDr.getValue("FCY_Account").split("\\-")[1],WebDr.getValue("FCY_Account").split("\\-")[0]);
						String FCYafter_balance =(FCYaccount_details.get("current_Available Balance")).replace(":","").replace(",", "");
						String FCYafter_amount = FCYafter_balance.trim();
						BigDecimal FCYafter_Draft_balance=new BigDecimal(FCYafter_amount);
						
						//Verifying the draft added and printed or not
						if(FCYafter_Draft_balance.equals(FCYbefore_Draft_balance.subtract(DraftAmount)))
						{
							Reporter.ReportEvent("Draft Issue", "Draft issue addded Successfully", "Draft Issue added", true);
						}
						else
						{
							Reporter.ReportEvent("Draft Issue", "Draft issue addded Successfully", "Draft Issue Failed", false);
						}
						// Draft View
						if (HtmlReporter.tc_result)ROATModules.DraftView_Navigation();
						if (HtmlReporter.tc_result)ROATModules.forexDraftsView("4 USD Draft",DraftNumber);

						// Draft Pending and printing the satus
						if (HtmlReporter.tc_result)ROATModules.DraftPending_Navigation();
						if (HtmlReporter.tc_result)ROATModules.forexDraftsPending("Yes",DraftNumber);
				if (HtmlReporter.tc_result)
					{
						Reporter.ReportEvent("Draft Issue FCY to FCY ", "Draft Issue FCY to FCY should be Passed","Draft Issue FCY to FCY Passed", true);
					}
					else
					{
						Reporter.ReportEvent("Draft Issue FCY to FCY ", "Draft Issue FCY to FCY should be Passed","Draft Issue FCY to FCY Failed", false);
					}	
						
			if (HtmlReporter.tc_result)
					Reporter.ReportEvent("Draft Issue Testcases ", "Draft Issue Testcases should be Passed","Draft Issue Testcases Passed", true);
			
				}
				catch(Exception e)
				{
					e.printStackTrace();
					Reporter.ReportEvent("Draft Issue Testcases ", "Draft Issue Testcases should be Passed","Draft Issue Testcases Failed", false);
				}


			}

				//************************Forex Draft Issue Breaches****************************//
				public static void E_E_ForexDraftIssue_Breaches() 
				{
					HashMap<String, String> LCYaccount_details = new HashMap<String, String>();
					HashMap<String, String> FCYaccount_details = new HashMap<String, String>();
					HashMap <String, String> invertoryDraftBillMap = new HashMap <String, String>();
					HashMap <String, String> corrBankMaintenanceMap = new HashMap <String, String>();
					HashMap<String, String> storeLedgerAccountNumber = new HashMap<String, String>();
					HashMap<String, Double> forex_fees=new HashMap<String, Double>();
					BigDecimal DraftAmount=BigDecimal.ZERO;
					String DraftData="";
					String user_branch="",user_id="",user_name="",Branch="",payAccountNumber="";
					int lcy_range=0,fcy_range=0, midrate=1;
					String currency=WebDr.getValue("ETE_FCY_Account_no").split("\\|")[0];
					String Mbranch=WebDr.getValue("Account Currency").split("\\|")[0];
					String Mcurrency=WebDr.getValue("Account Currency").split("\\|")[1];
					String CurrAccountNumber="",CurrAccBranch="";
					String DraftType=WebDr.getValue("DraftType");
					String Voucher="";
					int pre_trans_number=0;
					String local_gl__number=WebDr.getValue("LCY_GL_no");
					String forex_gl__number=WebDr.getValue("FCY_GL_No");
					String lcyFlag = WebDr.getValue("Local Currency Flag");
					HashMap<String, ArrayList<String>> limits_map = new HashMap<String, ArrayList<String>>();
					HashMap<String, String> CreditRisk = new HashMap<String, String>();
					HashMap<String, Map<String,String>> store_matrix = new HashMap<String, Map<String,String>>();
					HashMap<String, Map<String,String>> store_forex = new HashMap<String, Map<String,String>>();
				try
				{
					ROATFunctions.refesh(false,"Teller");
					if (HtmlReporter.tc_result)
					{ 
						user_branch = ROATModules.fetch_user_branch();
						user_id=ROATFunctions.fetch_user_Id();
						user_name = ROATModules.fetchuser();
						Branch=ROATFunctions.fetch_user_branch_name();
						lcy_range=ROATModules.LCY_Payment_amount("1",1);
						fcy_range=ROATModules.FCY_Payment_amount(currency,"1",1);
						
						// Fetching forex rate 
						ROATFunctions.refesh(false, "Central Supervisor");
						WebDr.SetPageObjects("end_end");
						store_forex = ROATModules.set_forexrate(currency);

						// Fetching forex matrix 
						ROATFunctions.refesh(false, "Super User");
						if (HtmlReporter.tc_result)ROATModules.Forex_MatrixNavigation();
						store_matrix=ROATModules.fetch_Forex_teller_matrix(Branch,WebDr.getValue("default_branch"));

						//Fetching Authorization remote
						if (HtmlReporter.tc_result)
						{
							//CreditRisk=ROATModules.auto_remo_update(Branch);	
							if (HtmlReporter.tc_result)ROATModules.setup_navigateLTLimits();


							//********************************************Breaches verification************************************

							//  Limits set as 'Send for authorization' 
							if (HtmlReporter.tc_result)
							{ 
								midrate=Integer.parseInt(store_forex.get("Demand Drafts").get("Sell"));
								String action="Send for Authorization";
								if (HtmlReporter.tc_result)
								{
									limits_map=ROATModules.fetch_limits_ALL(user_branch, user_id+"-"+user_name,fcy_range,action);
								}

								//ROATFunctions.refesh(false, "Teller");
								//if (HtmlReporter.tc_result)ROATModules.DraftIssue_Navigation();
								if (HtmlReporter.tc_result)ROATModules.Forex_Draft_Issue_ete(limits_map,CreditRisk,store_matrix,fcy_range,action,midrate,DraftType,"No",Voucher);
							}

							// test cases flag true
							HtmlReporter.tc_result=true; 

							// Transaction with Limits set as 'Decline'
							if (HtmlReporter.tc_result)
							{
								midrate=Integer.parseInt(store_forex.get("Demand Drafts").get("Sell"));
								ROATFunctions.refesh(false, "Super User");
								String action="Decline";
								if (HtmlReporter.tc_result)ROATModules.setup_navigateLTLimits();
								if (HtmlReporter.tc_result == true)
								{
									limits_map=ROATModules.fetch_limits_ALL(user_branch, user_id+"-"+user_name,fcy_range,action);
								}
								//ROATFunctions.refesh(false, "Teller");
								//if (HtmlReporter.tc_result)ROATModules.DraftIssue_Navigation();
								if (HtmlReporter.tc_result)ROATModules.Forex_Draft_Issue_ete(limits_map,CreditRisk,store_matrix,fcy_range,action,midrate,DraftType,"No",Voucher);
							}
						}
					}
				}

				catch(Exception e)
				{
					e.getStackTrace();
					Reporter.ReportEvent("forex Draft Issue Breaches Testcase ", " Testcase should be passed","forex Draft Issue Breaches Testcase Failed", false);
				}		

			}



			//************************************* End to End Forex Draft Cancel*************************************************************//
			public static void E_E_ForexDraftCancel() 
			{
				HashMap<String, String> LCYaccount_details = new HashMap<String, String>();
				HashMap<String, String> FCYaccount_details = new HashMap<String, String>();
				HashMap <String, String> invertoryDraftBillMap = new HashMap <String, String>();
				HashMap <String, String> corrBankMaintenanceMap = new HashMap <String, String>();
				HashMap<String, String> storeLedgerAccountNumber = new HashMap<String, String>();
				String DraftNumber="";
				String DraftData="";
				BigDecimal DraftAmount=BigDecimal.ZERO;
				String user_branch="",user_id="",user_name="",Branch="",payAccountNumber="";
				String Voucher="";	
				int pre_trans_number=0;
				int lcy_range=0,fcy_range=0, midrate=1;
				String DraftType=WebDr.getValue("DraftType");
				String currency=WebDr.getValue("ETE_FCY_Account_no").split("\\|")[0];
				String Mbranch=WebDr.getValue("Account Currency").split("\\|")[0];
				String Mcurrency=WebDr.getValue("Account Currency").split("\\|")[1];
				String CurrAccountNumber="",CurrAccBranch="";
				String local_gl__number=WebDr.getValue("LCY_GL_no");
				String forex_gl__number=WebDr.getValue("FCY_GL_No");
				String lcyFlag = WebDr.getValue("Local Currency Flag");
				HashMap<String, ArrayList<String>> limits_map = new HashMap<String, ArrayList<String>>();
				HashMap<String, String> CreditRisk = new HashMap<String, String>();
				HashMap<String, Map<String,String>> store_matrix = new HashMap<String, Map<String,String>>();
				HashMap<String, Map<String,String>> store_forex = new HashMap<String, Map<String,String>>();
				HashMap<String, String> getDraftNumber = new HashMap<String, String>();
				try
				{

					ROATFunctions.refesh(false, "Teller");
					user_branch = ROATModules.fetch_user_branch();
					user_id=ROATFunctions.fetch_user_Id();
					user_name = ROATModules.fetchuser();
					Branch=ROATFunctions.fetch_user_branch_name();
					lcy_range=ROATModules.LCY_Payment_amount("1",1);
					fcy_range=ROATModules.FCY_Payment_amount(currency,"1",1);

					ROATFunctions.refesh(false, "Supervisor");
					if (HtmlReporter.tc_result){
						ROATModules.navigateInventoryDraftBill();
						invertoryDraftBillMap = ROATModules.updateStoreInventoryDraftBill();
						System.out.println(invertoryDraftBillMap);

					}

					if (HtmlReporter.tc_result){
						ROATModules.navigateCorrBankMaint();
						corrBankMaintenanceMap = ROATModules.getCorrBankMainDetails();
						System.out.println(corrBankMaintenanceMap);
					}

					// Fetching forex rate 
					if (HtmlReporter.tc_result){
						ROATFunctions.refesh(false, "Central Supervisor");
						WebDr.SetPageObjects("end_end");
						store_forex=ROATModules.set_forexrateSellPurchase(currency);
					}

					ROATFunctions.refesh(false, "Super User");

					if (HtmlReporter.tc_result){
						ROATModules.navigate_MICRTypes();
						Voucher = ROATModules.getMICRTypesVoucher();
						System.out.println(Voucher);
					}

					System.out.println(store_forex);
					//forex fees
					if (HtmlReporter.tc_result)
					{
						ROATModules.forexDraft_fees_setup(currency,user_branch,fcy_range,false);
					}
					if (HtmlReporter.tc_result)
					{
						if (HtmlReporter.tc_result)ROATModules.setup_navigateLTLimits();
						ROATModules.fetch_limits_ALL(user_branch, user_id+"-"+user_name,fcy_range,"Send for Authorization");
					}

					//fetching maintenance account
					if (HtmlReporter.tc_result)
						storeLedgerAccountNumber=ROATModules.getLegderAccMaintenaceDetails(Mbranch, Mcurrency);
					System.out.println(storeLedgerAccountNumber);

					if(HtmlReporter.tc_result){

						CurrAccBranch = storeLedgerAccountNumber.get(Mcurrency).split("-")[0].replace(":","").trim();

						CurrAccountNumber = storeLedgerAccountNumber.get(Mcurrency).split("-")[1].trim();
					}
					
					// to get the transaction number before performing draft issue
					if(Boolean.valueOf(lcyFlag))
					{
						pre_trans_number =ROATModules.support_function_forexDraft( false, "lcy", pre_trans_number,"Forex Draft-Cancel",WebDr.getValue("LCY_Account"),DraftAmount,true);
					}
					else
					{
						pre_trans_number =ROATModules.support_function_forexDraft( true, "lcy", pre_trans_number,"Forex Draft-Cancel",WebDr.getValue("LCY_Account"),DraftAmount,true);
					}	
					WebDr.Wait_Time(2000);
					// Draft Issue
					if (HtmlReporter.tc_result)ROATModules.DraftIssue_Navigation();
					if (HtmlReporter.tc_result)DraftData =ROATModules.forexDraftsIssue(DraftType,WebDr.getValue("LCY_Account"),"No",Voucher);
					String Amount=DraftData.split("-")[0].trim();
					DraftNumber=DraftData.split("-")[1].trim();
					DraftAmount = new BigDecimal(Amount);

					// Draft View
					if (HtmlReporter.tc_result)ROATModules.DraftView_Navigation();
					if (HtmlReporter.tc_result)ROATModules.forexDraftsView(DraftType,DraftNumber);

					//Draft Pending
					if (HtmlReporter.tc_result)ROATModules.DraftPending_Navigation();
					if (HtmlReporter.tc_result)ROATModules.forexDraftsPending("No",DraftNumber);
					WebDr.Wait_Time(2000);
					// to inquire maintenance account before Cancel					
					if (HtmlReporter.tc_result)ROATFunctions.refesh(false,"Teller");
					LCYaccount_details=ROATModules.account_inquiry(CurrAccountNumber,CurrAccBranch);
					String before_balance_Macc =(LCYaccount_details.get("current_Available Balance")).replace(":","").replace(",", "");
					String before_amount_Macc = before_balance_Macc.trim();
					System.out.println(before_amount_Macc);
					BigDecimal before_Macc_balance=new BigDecimal(before_amount_Macc);		
					WebDr.Wait_Time(2000);
					// User Account Information before draft Cancel
					if (HtmlReporter.tc_result)ROATFunctions.refesh(false,"Teller");
					LCYaccount_details=ROATModules.account_inquiry(WebDr.getValue("LCY_Account").split("\\-")[1],WebDr.getValue("LCY_Account").split("\\-")[0]);
					String before_balance =(LCYaccount_details.get("current_Available Balance")).replace(":","").replace(",", "");
					String before_amount = before_balance.trim();
					System.out.println(before_amount);
					BigDecimal before_Draft_balance=new BigDecimal(before_amount);

					//Draft Cancel
					if (HtmlReporter.tc_result)ROATModules.DraftCancel_Navigation();
					if (HtmlReporter.tc_result)ROATModules.forexDraftsCancel(DraftType,DraftNumber);
					

					//Account Inquiry after draft cancel
					if (HtmlReporter.tc_result)ROATFunctions.refesh(false,"Teller");
					LCYaccount_details=ROATModules.account_inquiry(WebDr.getValue("LCY_Account").split("\\-")[1],WebDr.getValue("LCY_Account").split("\\-")[0]);
					String afterCancel_balance =(LCYaccount_details.get("current_Available Balance")).replace(":","").replace(",", "");
					String afterCancel_amount = afterCancel_balance.trim();
					BigDecimal after_Cancel_balance=new BigDecimal(afterCancel_amount);

					//Verifying the draft is cancel
					if(after_Cancel_balance.compareTo(before_Draft_balance)==1)
					{
						Reporter.ReportEvent("Draft Cancel", "Draft Cancel done Successfully", "Draft Cancel Successfully", true);
					}
					else
					{
						Reporter.ReportEvent("Draft Cancel", "Draft Cancel done Successfully", "Draft Cancel Failed", false);
					}

					// to inquire maintenance account after draft cancel
					if (HtmlReporter.tc_result)ROATFunctions.refesh(false,"Teller");
					LCYaccount_details=ROATModules.account_inquiry(CurrAccountNumber,CurrAccBranch);
					String after_balance_Macc =(LCYaccount_details.get("current_Available Balance")).replace(":","").replace(",", "");
					String after_amount_Macc = after_balance_Macc.trim();
					System.out.println(after_amount_Macc);
					BigDecimal after_Macc_balance=new BigDecimal(after_amount_Macc);

					//to verify currency maintenance account
					if(after_Macc_balance.compareTo(before_Macc_balance)==-1)
					{
						Reporter.ReportEvent("Draft Cancel", "Account maintenance verification", "Passed", true);
					}
					else
					{
						Reporter.ReportEvent("Draft Cancel", "Account maintenance verification", "Failed", false);
					} 
					if (HtmlReporter.tc_result)
						Reporter.ReportEvent("Draft Cancel Testcases ", "Draft Cancel Testcases should be Passed","Draft Cancel Testcases Passed", true);
					
					// delete the transaction
					if(Boolean.valueOf(lcyFlag))
					{
					ROATModules.support_function_forexDraft( false, "lcy", pre_trans_number,"Forex Draft-Cancel",WebDr.getValue("LCY_Account"),DraftAmount,true);
					}
					else
					{
						ROATModules.support_function_forexDraft( false, "lcy", pre_trans_number,"Forex Draft-Cancel",WebDr.getValue("LCY_Account"),DraftAmount, true);
					}

			}
				catch(Exception e)
				{
					e.printStackTrace();
					Reporter.ReportEvent("Draft Cancel Testcases ", "Draft Cancel Testcases should be Passed","Draft Cancel Testcases Failed", false);
				}

			}
			
			//************************Forex Draft Cancel Breaches****************************//
			public static void E_E_ForexDraftCancel_Breaches() 
			{
				HashMap<String, String> LCYaccount_details = new HashMap<String, String>();
				HashMap<String, String> FCYaccount_details = new HashMap<String, String>();
				HashMap <String, String> invertoryDraftBillMap = new HashMap <String, String>();
				HashMap <String, String> corrBankMaintenanceMap = new HashMap <String, String>();
				HashMap<String, String> storeLedgerAccountNumber = new HashMap<String, String>();
				String DraftNumber="";
				String DraftData="";
				BigDecimal DraftAmount=BigDecimal.ZERO;
				String user_branch="",user_id="",user_name="",Branch="",payAccountNumber="";
				String Voucher="";				
				int lcy_range=0,fcy_range=0, midrate=1;
				String DraftType=WebDr.getValue("DraftType");
				String currency=WebDr.getValue("ETE_FCY_Account_no").split("\\|")[0];
				String Mbranch=WebDr.getValue("Account Currency").split("\\|")[0];
				String Mcurrency=WebDr.getValue("Account Currency").split("\\|")[1];
				String CurrAccountNumber="",CurrAccBranch="";
				String local_gl__number=WebDr.getValue("LCY_GL_no");
				String forex_gl__number=WebDr.getValue("FCY_GL_No");
				String lcyFlag = WebDr.getValue("Local Currency Flag");
				HashMap<String, ArrayList<String>> limits_map = new HashMap<String, ArrayList<String>>();
				HashMap<String, String> CreditRisk = new HashMap<String, String>();
				HashMap<String, Map<String,String>> store_matrix = new HashMap<String, Map<String,String>>();
				HashMap<String, Map<String,String>> store_forex = new HashMap<String, Map<String,String>>();
				HashMap<String, String> getDraftNumber = new HashMap<String, String>();
			try
			{
				ROATFunctions.refesh(false,"Teller");
				if (HtmlReporter.tc_result)
				{ 
					user_branch = ROATModules.fetch_user_branch();
					user_id=ROATFunctions.fetch_user_Id();
					user_name = ROATModules.fetchuser();
					Branch=ROATFunctions.fetch_user_branch_name();
					lcy_range=ROATModules.LCY_Payment_amount("1",1);
					fcy_range=ROATModules.FCY_Payment_amount(currency,"1",1);
					
					// Fetching forex rate 
					ROATFunctions.refesh(false, "Central Supervisor");
					WebDr.SetPageObjects("end_end");
					store_forex = ROATModules.set_forexrate(currency);

					// Fetching forex matrix 
					ROATFunctions.refesh(false, "Super User");
					if (HtmlReporter.tc_result)ROATModules.Forex_MatrixNavigation();
					store_matrix=ROATModules.fetch_Forex_teller_matrix(Branch,WebDr.getValue("default_branch"));

					//Fetching Authorization remote
					if (HtmlReporter.tc_result)
					{
						//CreditRisk=ROATModules.auto_remo_update(Branch);	
						if (HtmlReporter.tc_result)ROATModules.setup_navigateLTLimits();


						//********************************************Breaches verification************************************

						//  Limits set as 'Send for authorization' 
						if (HtmlReporter.tc_result)
						{ 
							midrate=Integer.parseInt(store_forex.get("Demand Drafts").get("Sell"));
							String action="Send for Authorization";
							if (HtmlReporter.tc_result)
							{
								limits_map=ROATModules.fetch_limits_ALL(user_branch, user_id+"-"+user_name,fcy_range,action);
							}
							if (HtmlReporter.tc_result)getDraftNumber=ROATModules.Forex_Draft_Cancel_ete(limits_map,CreditRisk,store_matrix,fcy_range,action,midrate,DraftType,"No",Voucher,getDraftNumber);
						
							System.out.println(getDraftNumber);
						}

						// test cases flag true
						HtmlReporter.tc_result=true; 

						// Transaction with Limits set as 'Decline'
						if (HtmlReporter.tc_result)
						{
							midrate=Integer.parseInt(store_forex.get("Demand Drafts").get("Sell"));
							ROATFunctions.refesh(false, "Super User");
							String action="Decline";
							if (HtmlReporter.tc_result)ROATModules.setup_navigateLTLimits();
							if (HtmlReporter.tc_result == true)
							{
								limits_map=ROATModules.fetch_limits_ALL(user_branch, user_id+"-"+user_name,fcy_range,action);
							}
							
							if (HtmlReporter.tc_result)ROATModules.Forex_Draft_Cancel_ete(limits_map,CreditRisk,store_matrix,fcy_range,action,midrate,DraftType,"No",Voucher,getDraftNumber);
						}
					}
				}
			}

			catch(Exception e)
			{
				e.getStackTrace();
				Reporter.ReportEvent("forex Draft Cancel Breaches Testcase ", " Testcase should be passed","forex Draft Cancel Breaches Testcase Failed", false);
			}		

		}





			
//*************** Forex Purchase End to End*************************************
			
			@SuppressWarnings("unused")
			public static void E_E_forex_purchase()
			{
				
				String user_branch="",user_id="",user_name="",Branch="",purchaseAccountNumber="",payAccountNumber="",purchaseBranch="",payBranch="";
				
				String purchase_Currency=WebDr.getValue("Purchase Currency").split("-")[0],pay_Currency = WebDr.getValue("Pay Currency").split("-")[0]; 
				int lcy_range=0,fcy_range=0,fcy_range2=0,pre_trans_number=0;
				System.out.println(WebDr.getValue("Local Currency Flag"));
				//String lcyFlag = "true";
				String lcyFlag = WebDr.getValue("Local Currency Flag");
				HashMap<String, Map<String,String>> store_forex_rate = new HashMap<String, Map<String,String>>();
				HashMap<String, Double> forex_fees=new HashMap<String, Double>();
				HashMap<String, String> storeLedgerAccountNumber = new HashMap<String, String>();
				HashMap<String, String> storeAccountInquiryDetails = new HashMap<String, String>();
				HashMap<String, String> storeNewAccountInquiryDetails = new HashMap<String, String>();
				//HashMap<String, String> storeCashDrawerDetails = new HashMap<String, String>();
				//HashMap<String, String> storeNewCashDrawerDetails = new HashMap<String, String>();
				HashMap<String, String> exchangeAmounts =new HashMap<String, String>();
				Set<String> curreny_list=new HashSet<>();
				HashMap<String, BigDecimal> cd_inquiry = new HashMap<String, BigDecimal>();
				HashMap<String, BigDecimal> cd_inquiry_after = new HashMap<String, BigDecimal>();
				HashMap<String,HashMap<String, BigDecimal>> storeCashDrawerDetails = new HashMap<String,HashMap<String, BigDecimal>>();
				HashMap<String,HashMap<String, BigDecimal>> storeNewCashDrawerDetails = new HashMap<String,HashMap<String, BigDecimal>>();
				
				String exchange_account = WebDr.getValue("Exchange Account"),purchase_PayMode = WebDr.getValue("Purchase Payment Mode"),paying_PayMode = WebDr.getValue("Paying Payment Mode");
				
				try{
					
				//Fetch teller details
					ROATFunctions.refesh(false,"Teller");
					if (HtmlReporter.tc_result)
					{ 
						user_branch = ROATModules.fetch_user_branch();
						user_id=ROATFunctions.fetch_user_Id();
						user_name = ROATModules.fetchuser();
						Branch=ROATFunctions.fetch_user_branch_name();
						System.out.println(WebDr.getValue("Local Currency Flag"));
						if(Boolean.valueOf(lcyFlag)){
							lcy_range=ROATModules.LCY_Payment_amount("1",1);
						}else{
							lcy_range=ROATModules.FCY_Payment_amount(pay_Currency,"1",1);
						}
						
						
						//lcy_range=ROATModules.LCY_Payment_amount("1",1);
						fcy_range=ROATModules.FCY_Payment_amount(purchase_Currency,"1",1);
						
						
					}
					
					
					//verify cash position
					if(HtmlReporter.tc_result){
						ROATModules.navigateOCP();
						boolean ocp = ROATModules.verifyOCP();
					}
					
					
					ROATFunctions.refesh(false, "Super User");
					
					//Ledger Account details for Sell and Pay currency
					if(HtmlReporter.tc_result){
						storeLedgerAccountNumber = ROATModules.getLegderAccountDetails(Branch,WebDr.getValue("Purchase Currency"));
						storeLedgerAccountNumber.putAll(ROATModules.getLegderAccountDetails(Branch,WebDr.getValue("Pay Currency")));
					}
					
					ROATFunctions.refesh(false, "Teller");
					if (HtmlReporter.tc_result)WebDr.Wait_Time(1500);
					
					//get ledger and transaction accounts
			if(purchase_PayMode.toUpperCase().equals("ACCOUNT")){
				
				if(HtmlReporter.tc_result){
					purchaseBranch = exchange_account.split("-")[0];
					payBranch = storeLedgerAccountNumber.get(pay_Currency).split("-")[0].replace(":","").trim();
					purchaseAccountNumber = exchange_account.split("-")[1];
					payAccountNumber = storeLedgerAccountNumber.get(pay_Currency).split("-")[1].trim();
				}
				
			}else if(paying_PayMode.toUpperCase().equals("ACCOUNT")){
				
				if(HtmlReporter.tc_result){
					
					purchaseBranch = storeLedgerAccountNumber.get(purchase_Currency).split("-")[0].replace(":","").trim();
					payBranch = exchange_account.split("-")[0];
					purchaseAccountNumber = storeLedgerAccountNumber.get(purchase_Currency).split("-")[1].trim();
					payAccountNumber = exchange_account.split("-")[1];
				}
			}else{
				
				if(HtmlReporter.tc_result){
					
					purchaseBranch = storeLedgerAccountNumber.get(purchase_Currency).split("-")[0].replace(":","").trim();
					payBranch = storeLedgerAccountNumber.get(pay_Currency).split("-")[0].replace(":","").trim();
					purchaseAccountNumber = storeLedgerAccountNumber.get(purchase_Currency).split("-")[1].trim();
					payAccountNumber = storeLedgerAccountNumber.get(pay_Currency).split("-")[1].trim();
				}
			}
					
					//get original balance for ledger and transaction accounts
					
					storeAccountInquiryDetails = ROATModules.account_inquiryExSell(purchaseAccountNumber,purchaseBranch);
					storeAccountInquiryDetails.putAll(ROATModules.account_inquiryExSell(payAccountNumber,payBranch));
					
					
					WebDr.Wait_Time(10000);
				
					//cash drawer inquiry before performing
					if(HtmlReporter.tc_result)
						storeCashDrawerDetails= ROATModules.FCY_Inquiry();
						curreny_list=storeCashDrawerDetails.keySet();
						String temp="";
						for(String set:curreny_list)
						{
							if(set.contains(purchase_Currency))
								temp=set;
						}
						cd_inquiry=storeCashDrawerDetails.get(temp);
						System.out.println(cd_inquiry);
						BigDecimal Den_before_bal = cd_inquiry.get("Denomination_balance");
						BigDecimal Tran_before_bal = cd_inquiry.get("Transaction Payment");
						BigDecimal Balance_before_bal = cd_inquiry.get("Balance");
						System.out.println(Den_before_bal);
						System.out.println(Tran_before_bal);
						System.out.println(Balance_before_bal);
					
					// Fetch forex rates 
					ROATFunctions.refesh(false, "Central Supervisor");
					WebDr.SetPageObjects("end_end");
					store_forex_rate = ROATModules.set_forexrateSellPurchase(purchase_Currency);
					
					if (HtmlReporter.tc_result)WebDr.Wait_Time(1500);
					
					//if fcy to fcy only then update rate for pay currency
					if(!Boolean.valueOf(lcyFlag)){
						store_forex_rate.putAll(ROATModules.set_forexrateSellPurchase(pay_Currency));
					}else{
						store_forex_rate.putAll(ROATModules.readandStoreForexRate(pay_Currency));
					}
					
					WebDr.Wait_Time(10000);
					//fetch forex fees
					if (HtmlReporter.tc_result)
					{
						forex_fees = ROATModules.forex_sell_fees_setup(purchase_Currency,user_branch,fcy_range,store_forex_rate,false);
						System.out.println(forex_fees);
					}
					
					Thread.sleep(6000);
					//get old transaction no from support function
					if(Boolean.valueOf(lcyFlag))
					{
						 pre_trans_number =ROATModules.support_function( true, "lcy", 0,"Forex Purchase",payBranch+"-"+payAccountNumber,purchaseBranch+"-"+purchaseAccountNumber,String.valueOf(lcy_range), true);
					}
					else
					{
						 pre_trans_number =ROATModules.support_function( true, "fcy", 0,"Forex Purchase",payBranch+"-"+payAccountNumber,purchaseBranch+"-"+purchaseAccountNumber,String.valueOf(lcy_range), true);
					}

					Thread.sleep(5000);
					ROATFunctions.refesh(false,"Teller");
					//xchange sell flow
					if (HtmlReporter.tc_result)
						{
						exchangeAmounts = ROATModules.e_e_forex_purchase(purchase_Currency,pay_Currency,purchase_PayMode,paying_PayMode,exchange_account,store_forex_rate, forex_fees);
						//ROATModules.e_e_forex_sell("USD","BWP","Cash","Cash",exchange_account,store_forex_rate, forex_fees);
						}
					
					
					if (HtmlReporter.tc_result)WebDr.Wait_Time(10000);
					
					//get updated balance for ledger and transaction accounts
					storeNewAccountInquiryDetails = ROATModules.account_inquiryExSell(purchaseAccountNumber,purchaseBranch);
					storeNewAccountInquiryDetails.putAll(ROATModules.account_inquiryExSell(payAccountNumber,payBranch));
					
					//get balance figures and compare them
   				double oldValue = Double.parseDouble(storeAccountInquiryDetails.get("current_Actual Balance"+payAccountNumber).split(":")[1].trim().replace(",", ""));
  				double newValue = Double.parseDouble(storeNewAccountInquiryDetails.get("current_Actual Balance"+payAccountNumber).split(":")[1].trim().replace(",", ""));
  				if (HtmlReporter.tc_result)WebDr.Wait_Time(1000);
  				double PAmount = Double.parseDouble(exchangeAmounts.get("Paying Amount").replace(",", "").trim());
  				System.out.println(PAmount);
  				if(oldValue<newValue)
  				{
   					System.out.println("pay currency has been debited");
   					Reporter.ReportEvent("pay currency credit", "pay currency has been credited","pay currency has been credited", true);
  				}
  				else
  				{
  	  				Reporter.ReportEvent("pay currency credit", "pay currency has been credited","pay currency credit has been failed ", false);
     			}
  			oldValue = Double.parseDouble(storeAccountInquiryDetails.get("current_Actual Balance"+purchaseAccountNumber).split(":")[1].trim().replace(",", ""));
  			newValue = Double.parseDouble(storeNewAccountInquiryDetails.get("current_Actual Balance"+purchaseAccountNumber).split(":")[1].trim().replace(",", ""));
  			if(oldValue>newValue)
  			{
   				System.out.println("purchase currency has been debited");
   				Reporter.ReportEvent("purchase currency debit", "purchase currency has been debited","purchase currency debit has been passed", true);
  			}
  			else
  			{
   				Reporter.ReportEvent("purchase currency debit", "purchase currency has been debited","purchase currency debit has been failed ", false);
  			}
					
					
					//cash drawer inquiry after performing
					
					if(HtmlReporter.tc_result)
						storeNewCashDrawerDetails= ROATModules.FCY_Inquiry();
						curreny_list=storeNewCashDrawerDetails.keySet();
						//String temp="";
						for(String set:curreny_list)
						{
							if(set.contains(purchase_Currency))
								temp=set;
						}
						cd_inquiry_after=storeNewCashDrawerDetails.get(temp);
						System.out.println(cd_inquiry_after);
						
						BigDecimal Den_after_bal = cd_inquiry_after.get("Denomination_balance");
						BigDecimal Tran_after_bal = cd_inquiry_after.get("Transaction Receipt");
						BigDecimal Balance_after_bal = cd_inquiry_after.get("Balance");
						System.out.println(Den_after_bal);
						System.out.println(Tran_after_bal);
						System.out.println(Balance_after_bal);
						//Denomination_balance increases, Transaction receipt Increases, Balance increases
					if(!purchase_PayMode.equalsIgnoreCase("Account")){
						if(Den_before_bal.compareTo(Den_after_bal)==-1 && Tran_before_bal.compareTo(Tran_after_bal)==-1 && Balance_before_bal.compareTo(Balance_after_bal)==-1)
						{
						Reporter.ReportEvent("Cash Drawer", "Cash Drawer verified after Exchange cell", "Cash Drawer Exchange Successfully", true);
						}
						else
						{
							Reporter.ReportEvent("Cash Drawer", "Cash Drawer verified after Exchange cell", "Cash Drawer Exchange Failed", false);
						}
					}
					
					//delete transaction from support function
					//delete transaction from support function
					if(Boolean.valueOf(lcyFlag))
					{
						ROATModules.support_function_forex( false, "lcy", pre_trans_number,"Forex Purchase",payBranch+"-"+payAccountNumber,purchaseBranch+"-"+purchaseAccountNumber,exchangeAmounts.get("Paying Amount").replace(",", "").trim(),exchangeAmounts.get("Purchase Amount").replace(",", "").trim(),paying_PayMode, true);
					}
					else
					{
						ROATModules.support_function_forex( false, "fcy", pre_trans_number,"Forex Purchase",payBranch+"-"+payAccountNumber,purchaseBranch+"-"+purchaseAccountNumber,exchangeAmounts.get("Paying Amount").replace(",", "").trim(),exchangeAmounts.get("Purchase Amount").replace(",", "").trim(),paying_PayMode, true);
					}
					
					//to verify rate and charge override
					ROATFunctions.refesh(false,"Teller");
					ROATModules.forex_exchange_purchase_override(purchase_Currency,pay_Currency,purchase_PayMode,paying_PayMode,exchange_account);	
  			
									
				}catch(Exception e){
					e.printStackTrace();
					Reporter.ReportEvent("performing forex purchase end to end", "End to End Forex purchase must be succesful","End to End Forex purchase failed", false);
					
				}
			
			
		}
			
			public static void E_E_forex_purchase_Breaches(){

				String user_branch="",user_id="",user_name="",Branch="",purchaseAccountNumber="",payAccountNumber="",purchaseBranch="",payBranch="";
				
				String purchase_Currency=WebDr.getValue("Purchase Currency").split("-")[0],pay_Currency = WebDr.getValue("Pay Currency").split("-")[0]; 
				int lcy_range=0,fcy_range=0,fcy_range2=0,pre_trans_number=0;
				System.out.println(WebDr.getValue("Local Currency Flag"));
				//String lcyFlag = "true";
				String lcyFlag = WebDr.getValue("Local Currency Flag");
				HashMap<String, Map<String,String>> store_forex_rate = new HashMap<String, Map<String,String>>();
				HashMap<String, Double> forex_fees=new HashMap<String, Double>();
				HashMap<String, String> storeLedgerAccountNumber = new HashMap<String, String>();
				HashMap<String, String> storeAccountInquiryDetails = new HashMap<String, String>();
				HashMap<String, String> storeNewAccountInquiryDetails = new HashMap<String, String>();
				//HashMap<String, String> storeCashDrawerDetails = new HashMap<String, String>();
				//HashMap<String, String> storeNewCashDrawerDetails = new HashMap<String, String>();
				HashMap<String, String> exchangeAmounts =new HashMap<String, String>();
				Set<String> curreny_list=new HashSet<>();
				HashMap<String, BigDecimal> cd_inquiry = new HashMap<String, BigDecimal>();
				HashMap<String, BigDecimal> cd_inquiry_after = new HashMap<String, BigDecimal>();
				HashMap<String,HashMap<String, BigDecimal>> storeCashDrawerDetails = new HashMap<String,HashMap<String, BigDecimal>>();
				HashMap<String,HashMap<String, BigDecimal>> storeNewCashDrawerDetails = new HashMap<String,HashMap<String, BigDecimal>>();
				
				String exchange_account = WebDr.getValue("Exchange Account"),purchase_PayMode = WebDr.getValue("Purchase Payment Mode"),paying_PayMode = WebDr.getValue("Paying Payment Mode");
				
				//***************************************************Forex Exchange Purchase with Breaches*****************************************
				
				try
				{
					
					int midrate=1;
					String currency=WebDr.getValue("ETE_FCY_Account_no").split("\\|")[0];
					String local_gl__number=WebDr.getValue("LCY_GL_no");
					String forex_gl__number=WebDr.getValue("FCY_GL_No");
					HashMap<String, ArrayList<String>> limits_map = new HashMap<String, ArrayList<String>>();
					HashMap<String, String> CreditRisk = new HashMap<String, String>();
					HashMap<String, Map<String,String>> store_matrix = new HashMap<String, Map<String,String>>();
					HashMap<String, Map<String,String>> store_forex = new HashMap<String, Map<String,String>>();

					ROATFunctions.refesh(false,"Teller");
					if (HtmlReporter.tc_result)
					{ 
						user_branch = ROATModules.fetch_user_branch();
						user_id=ROATFunctions.fetch_user_Id();
						user_name = ROATModules.fetchuser();
						Branch=ROATFunctions.fetch_user_branch_name();
						lcy_range=ROATModules.LCY_Payment_amount("1",1);
						fcy_range=ROATModules.FCY_Payment_amount(currency,"1",1);


						// Fetching forex rate 
						ROATFunctions.refesh(false, "Central Supervisor");
						WebDr.SetPageObjects("end_end");
						store_forex = ROATModules.set_forexrateSellPurchase(currency);
						
						//Fetching 

						// Fetching forex matrix 
						ROATFunctions.refesh(false, "Super User");
						if (HtmlReporter.tc_result)ROATModules.Forex_MatrixNavigation();
						store_matrix=ROATModules.fetch_Forex_teller_matrix(Branch,WebDr.getValue("default_branch"));

						//Fetching Authorization remote
						if (HtmlReporter.tc_result)
						{
							//CreditRisk=ROATModules.auto_remo_update(Branch);	
							if (HtmlReporter.tc_result)ROATModules.setup_navigateLTLimits();


							//******************************Breaches verification************************************


							//  Limits set as 'Send for authorization' 
							if (HtmlReporter.tc_result)
							{ 
								//Limit breach------
								//System.out.println(store_forex);
								midrate=Integer.parseInt(store_forex.get("Cash Currency"+currency).get("Buy"));
								String action="Send for Authorization";
								if (HtmlReporter.tc_result)
								{
									limits_map=ROATModules.fetch_limits_ALL(user_branch, user_id+"-"+user_name,fcy_range,action);
								}

								ROATFunctions.refesh(false, "Teller");
								if (HtmlReporter.tc_result)ROATModules.forex_purchase_navigate();
								if (HtmlReporter.tc_result)ROATModules.Forex_Exchange_Purchase_ete(limits_map,CreditRisk,store_matrix,fcy_range,action,midrate,"Cash","Account","USD",WebDr.getValue("Payment Currency"));
							}

							// test cases flag true
							HtmlReporter.tc_result=true; 

							// Transaction with Limits set as 'Decline'
							if (HtmlReporter.tc_result)
							{
								// Limit breach------
								
								midrate=Integer.parseInt(store_forex.get("Cash Currency"+currency).get("Buy"));
								ROATFunctions.refesh(false, "Super User");
								String action="Decline";
								if (HtmlReporter.tc_result)ROATModules.setup_navigateLTLimits();
								if (HtmlReporter.tc_result == true)
								{
									limits_map=ROATModules.fetch_limits_ALL(user_branch, user_id+"-"+user_name,fcy_range,action);
								}
								ROATFunctions.refesh(false, "Teller");
								if (HtmlReporter.tc_result)ROATModules.forex_purchase_navigate();
								if (HtmlReporter.tc_result)ROATModules.Forex_Exchange_Purchase_ete(limits_map,CreditRisk,store_matrix,fcy_range,action,midrate,"Cash","Account","USD",WebDr.getValue("Payment Currency"));
								}
						}
					}
				}

				catch(Exception e)
				{
					e.getStackTrace();
					Reporter.ReportEvent("forex Exchange Purchase Breaches Testcase ", " Testcase should be passed","forex Exchange Purchase Breaches Testcase Failed", false);
				}
			}

			public static void end_to_end_mixed_breaches_lcy() {
				

				if(HtmlReporter.tc_result)
				{
					try
					{
						int lcy_range=50;
						int fcy_range=100;
						int midrate=1;
						String currency=WebDr.getValue("ETE_FCY_Account_no").split("\\|")[0];
						String user_branch ="";
						String user_id="";
						String user_name = "";
						String Branch="";
						String local_gl__number=WebDr.getValue("LCY_GL_no");
						String forex_gl__number=WebDr.getValue("FCY_GL_No");
						HashMap<String, ArrayList<String>> limits_map = new HashMap<String, ArrayList<String>>();
						HashMap<String, String> CreditRisk = new HashMap<String, String>();
						HashMap<String, Map<String,String>> store_matrix = new HashMap<String, Map<String,String>>();
						HashMap<String, Map<String,String>> store_forex = new HashMap<String, Map<String,String>>();

						ROATFunctions.refesh(false,"Teller");
						if (HtmlReporter.tc_result)
						{ 
							user_branch = ROATModules.fetch_user_branch();
							user_id=ROATFunctions.fetch_user_Id();
							user_name = ROATModules.fetchuser();
							Branch=ROATFunctions.fetch_user_branch_name();
							lcy_range=ROATModules.LCY_Payment_amount("1",1);
							fcy_range=ROATModules.FCY_Payment_amount(currency,"1",1);


							// Fetching forex rate 
							ROATFunctions.refesh(false, "Central Supervisor");
							WebDr.SetPageObjects("end_end");
							store_forex = ROATModules.set_forexrate(currency);

							// Fetching teller matrix 
							ROATFunctions.refesh(false, "Super User");
							if (HtmlReporter.tc_result)ROATModules.setup_TellerMatrixNavigation();
							store_matrix=ROATModules.fetch_teller_matrix(Branch,WebDr.getValue("default_branch"));

							//Fetching Authorization remote
							if (HtmlReporter.tc_result)
							{
								//CreditRisk=ROATModules.auto_remo_update(Branch);	


								//******************************Breaches verification************************************


								// LCY Transaction with Limits set as 'Send for authorization' 
								if (HtmlReporter.tc_result)
								{
									//LCY Limit breach------mid rate=1
									midrate=1;
									String action="Send for Authorization";
									if (HtmlReporter.tc_result)
									{
										if (HtmlReporter.tc_result)ROATModules.setup_navigateLTLimits();
										limits_map=ROATModules.fetch_limits_ALL(user_branch, user_id+"-"+user_name,lcy_range,action);
									}


									ROATFunctions.refesh(false, "Teller");
									if (HtmlReporter.tc_result)ROATModules.Teller_navigateMixedDeposit();
									if (HtmlReporter.tc_result)ROATModules.Teller_mixed_deposit_ete(limits_map,CreditRisk,store_matrix,lcy_range,action,midrate);

								}

							}

						}
					}
					catch(Exception e)
					{
						Reporter.ReportEvent("Test Case execution is Failed", "end to end Deposit","end to end Deposit executition failed", false);
					}
				}
			
				
			}
			public static void end_to_end_mixed_breaches_lcy_decline() 
			{

				if(HtmlReporter.tc_result)
				{
					try
					{
						int lcy_range=50;
						int fcy_range=100;
						int midrate=1;
						String currency=WebDr.getValue("ETE_FCY_Account_no").split("\\|")[0];
						String user_branch ="";
						String user_id="";
						String user_name = "";
						String Branch="";
						String local_gl__number=WebDr.getValue("LCY_GL_no");
						String forex_gl__number=WebDr.getValue("FCY_GL_No");
						HashMap<String, ArrayList<String>> limits_map = new HashMap<String, ArrayList<String>>();
						HashMap<String, String> CreditRisk = new HashMap<String, String>();
						HashMap<String, Map<String,String>> store_matrix = new HashMap<String, Map<String,String>>();
						HashMap<String, Map<String,String>> store_forex = new HashMap<String, Map<String,String>>();

						ROATFunctions.refesh(false,"Teller");
						if (HtmlReporter.tc_result)
						{ 
							user_branch = ROATModules.fetch_user_branch();
							user_id=ROATFunctions.fetch_user_Id();
							user_name = ROATModules.fetchuser();
							Branch=ROATFunctions.fetch_user_branch_name();
							lcy_range=ROATModules.LCY_Payment_amount("1",1);
							fcy_range=ROATModules.FCY_Payment_amount(currency,"1",1);


							// Fetching forex rate 
							ROATFunctions.refesh(false, "Central Supervisor");
							WebDr.SetPageObjects("end_end");
							store_forex = ROATModules.set_forexrate(currency);

							// Fetching teller matrix 
							ROATFunctions.refesh(false, "Super User");
							if (HtmlReporter.tc_result)ROATModules.setup_TellerMatrixNavigation();
							store_matrix=ROATModules.fetch_teller_matrix(Branch,WebDr.getValue("default_branch"));

							//Fetching Authorization remote
							if (HtmlReporter.tc_result)
							{
								//CreditRisk=ROATModules.auto_remo_update(Branch);	

								//******************************Breaches verification************************************


								// test cases flag true
								HtmlReporter.tc_result=true; 

								// LCY Transaction with Limits set as 'Decline'
								if (HtmlReporter.tc_result)
								{
									//LCY Limit breach------mid rate=1
									midrate=1;
									ROATFunctions.refesh(false, "Super User");
									String action="Decline";
									if (HtmlReporter.tc_result)ROATModules.setup_navigateLTLimits();
									if (HtmlReporter.tc_result == true)
									{
									  limits_map=ROATModules.fetch_limits_ALL(user_branch, user_id+"-"+user_name,lcy_range,action);

									}

									WebDr.Wait_Time(100);
									ROATFunctions.refesh(false, "Teller");
									if (HtmlReporter.tc_result)ROATModules.Teller_navigateMixedDeposit();
									if (HtmlReporter.tc_result)ROATModules.Teller_mixed_deposit_ete(limits_map,CreditRisk,store_matrix,lcy_range,action,midrate);

								}

							}

						}
					}
					catch(Exception e)
					{
						Reporter.ReportEvent("Test Case execution is Failed", "end to end Deposit","end to end Deposit executition failed", false);
					}
				}
			
			}
			public static void end_to_end_mixed_breaches_fcy() 
			{

				if(HtmlReporter.tc_result)
				{
					try
					{
						int lcy_range=50;
						int fcy_range=100;
						int midrate=1;
						String currency=WebDr.getValue("ETE_FCY_Account_no").split("\\|")[0];
						String user_branch ="";
						String user_id="";
						String user_name = "";
						String Branch="";
						String local_gl__number=WebDr.getValue("LCY_GL_no");
						String forex_gl__number=WebDr.getValue("FCY_GL_No");
						HashMap<String, ArrayList<String>> limits_map = new HashMap<String, ArrayList<String>>();
						HashMap<String, String> CreditRisk = new HashMap<String, String>();
						HashMap<String, Map<String,String>> store_matrix = new HashMap<String, Map<String,String>>();
						HashMap<String, Map<String,String>> store_forex = new HashMap<String, Map<String,String>>();

						ROATFunctions.refesh(false,"Teller");
						if (HtmlReporter.tc_result)
						{ 
							user_branch = ROATModules.fetch_user_branch();
							user_id=ROATFunctions.fetch_user_Id();
							user_name = ROATModules.fetchuser();
							Branch=ROATFunctions.fetch_user_branch_name();
							lcy_range=ROATModules.LCY_Payment_amount("1",1);
							fcy_range=ROATModules.FCY_Payment_amount(currency,"1",1);


							// Fetching forex rate 
							ROATFunctions.refesh(false, "Central Supervisor");
							WebDr.SetPageObjects("end_end");
							store_forex = ROATModules.set_forexrate(currency);

							// Fetching teller matrix 
							ROATFunctions.refesh(false, "Super User");
							if (HtmlReporter.tc_result)ROATModules.setup_TellerMatrixNavigation();
							store_matrix=ROATModules.fetch_teller_matrix(Branch,WebDr.getValue("default_branch"));

							//Fetching Authorization remote
							if (HtmlReporter.tc_result)
							{
								//CreditRisk=ROATModules.auto_remo_update(Branch);	


								//******************************Breaches verification************************************



								// test cases flag true
								HtmlReporter.tc_result=true; 

								// FCY Transaction with Limits set as Send for authorization
								if (HtmlReporter.tc_result)
								{
									midrate=Integer.parseInt(store_forex.get("Mid Rate").get("Mid Rate"));
									ROATFunctions.refesh(false, "Super User");
									String action="Send for Authorization";
									if (HtmlReporter.tc_result)ROATModules.setup_navigateLTLimits();
									if (HtmlReporter.tc_result == true)
									{
										limits_map=ROATModules.fetch_limits_ALL(user_branch, user_id+"-"+user_name,fcy_range*midrate,action);
									}

									//FCY Limit breach------mid rate
									WebDr.Wait_Time(100);
									ROATFunctions.refesh(false, "Teller");
									if (HtmlReporter.tc_result)ROATModules.Teller_navigateMixedDeposit();
									if (HtmlReporter.tc_result)ROATModules.Teller_mixed_deposit_ete(limits_map,CreditRisk,store_matrix,fcy_range,action,midrate);

								}

							}

						}
					}
					catch(Exception e)
					{
						Reporter.ReportEvent("Test Case execution is Failed", "end to end Deposit","end to end Deposit executition failed", false);
					}
				}
			
				
				
			}
			public static void end_to_end_mixed_support_functions_lcy() 
			{

				if(HtmlReporter.tc_result)
				{
					try
					{
						int lcy_range=50;
						int fcy_range=100;
						int midrate=1;
						String currency=WebDr.getValue("ETE_FCY_Account_no").split("\\|")[0];
						String user_branch ="";
						String user_id="";
						String user_name = "";
						String Branch="";
						String local_gl__number=WebDr.getValue("LCY_GL_no");
						String forex_gl__number=WebDr.getValue("FCY_GL_No");
						HashMap<String, ArrayList<String>> limits_map = new HashMap<String, ArrayList<String>>();
						HashMap<String, String> CreditRisk = new HashMap<String, String>();
						HashMap<String, Map<String,String>> store_matrix = new HashMap<String, Map<String,String>>();
						HashMap<String, Map<String,String>> store_forex = new HashMap<String, Map<String,String>>();

						ROATFunctions.refesh(false,"Teller");
						if (HtmlReporter.tc_result)
						{ 
							user_branch = ROATModules.fetch_user_branch();
							user_id=ROATFunctions.fetch_user_Id();
							user_name = ROATModules.fetchuser();
							Branch=ROATFunctions.fetch_user_branch_name();
							lcy_range=ROATModules.LCY_Payment_amount("1",1);
							fcy_range=ROATModules.FCY_Payment_amount(currency,"1",1);


							// Fetching forex rate 
							ROATFunctions.refesh(false, "Central Supervisor");
							WebDr.SetPageObjects("end_end");
							store_forex = ROATModules.set_forexrate(currency);

							// Fetching teller matrix 
							ROATFunctions.refesh(false, "Super User");
							if (HtmlReporter.tc_result)ROATModules.setup_TellerMatrixNavigation();
							store_matrix=ROATModules.fetch_teller_matrix(Branch,WebDr.getValue("default_branch"));

							//Fetching Authorization remote
							if (HtmlReporter.tc_result)
							{
								//CreditRisk=ROATModules.auto_remo_update(Branch);	


								//	***********************************************end to end******************************
								//end to end---LCY---Transaction--without Breach 
								HtmlReporter.tc_result=true;
								ROATFunctions.refesh(false,"Super User");
								String	action="Send for Authorization";
								midrate=Integer.parseInt(store_forex.get("Mid Rate").get("Mid Rate"));
								if (HtmlReporter.tc_result)ROATModules.setup_navigateLTLimits();
								limits_map=ROATModules.fetch_limits_ALL(user_branch, user_id+"-"+user_name,lcy_range*midrate*2,action);
								
								
								
								HtmlReporter.tc_result=true;
								midrate=1;
								ROATModules.end_to_end_mixed_deposit(lcy_range,midrate,local_gl__number);





								//************************************support functions****************************


								//delete transaction from 'support function'--- local currency
								HtmlReporter.tc_result=true;
								midrate=1;
								ROATModules.end_to_end_mixed_deposit_support_function(lcy_range,midrate,local_gl__number);

							}

						}
					}
					catch(Exception e)
					{
						Reporter.ReportEvent("Test Case execution is Failed", "end to end Deposit","end to end Deposit executition failed", false);
					}
				}
			
			}
			public static void end_to_end_mixed_support_functions_Cashback_fcy()
{

				if(HtmlReporter.tc_result)
				{
					try
					{
						int lcy_range=50;
						int fcy_range=100;
						int midrate=1;
						String currency=WebDr.getValue("ETE_FCY_Account_no").split("\\|")[0];
						String user_branch ="";
						String user_id="";
						String user_name = "";
						String Branch="";
						String local_gl__number=WebDr.getValue("LCY_GL_no");
						String forex_gl__number=WebDr.getValue("FCY_GL_No");
						HashMap<String, ArrayList<String>> limits_map = new HashMap<String, ArrayList<String>>();
						HashMap<String, String> CreditRisk = new HashMap<String, String>();
						HashMap<String, Map<String,String>> store_matrix = new HashMap<String, Map<String,String>>();
						HashMap<String, Map<String,String>> store_forex = new HashMap<String, Map<String,String>>();

						ROATFunctions.refesh(false,"Teller");
						if (HtmlReporter.tc_result)
						{ 
							user_branch = ROATModules.fetch_user_branch();
							user_id=ROATFunctions.fetch_user_Id();
							user_name = ROATModules.fetchuser();
							Branch=ROATFunctions.fetch_user_branch_name();
							lcy_range=ROATModules.LCY_Payment_amount("1",1);
							fcy_range=ROATModules.FCY_Payment_amount(currency,"1",1);


							// Fetching forex rate 
							ROATFunctions.refesh(false, "Central Supervisor");
							WebDr.SetPageObjects("end_end");
							store_forex = ROATModules.set_forexrate(currency);

							// Fetching teller matrix 
							ROATFunctions.refesh(false, "Super User");
							if (HtmlReporter.tc_result)ROATModules.setup_TellerMatrixNavigation();
							store_matrix=ROATModules.fetch_teller_matrix(Branch,WebDr.getValue("default_branch"));

							//Fetching Authorization remote
							if (HtmlReporter.tc_result)
							{
								//CreditRisk=ROATModules.auto_remo_update(Branch);	

								//************************************support functions****************************


								//end to end---LCY---Transaction--without Breach 
								HtmlReporter.tc_result=true;
								ROATFunctions.refesh(false,"Super User");
								String	action="Send for Authorization";
								midrate=Integer.parseInt(store_forex.get("Mid Rate").get("Mid Rate"));
								if (HtmlReporter.tc_result)ROATModules.setup_navigateLTLimits();
								limits_map=ROATModules.fetch_limits_ALL(user_branch, user_id+"-"+user_name,fcy_range*midrate*2,action);
								

								//delete transaction from 'support function'--- foreign currency 
								HtmlReporter.tc_result=true;
								midrate=Integer.parseInt(store_forex.get("Mid Rate").get("Mid Rate"));
								ROATModules.end_to_end_mixed_deposit_support_function(fcy_range,midrate,forex_gl__number);



								//************************************Deposit with cashback****************************
								/*HtmlReporter.tc_result=true;
								ROATModules.end_end_deposit_cashback();


								HtmlReporter.tc_result=true;
								//sod eod vault close,teller close
								try
								{
									ROATModules.sod_eod_end_to_end();	
								}
								catch(Exception e)
								{
									e.printStackTrace();
									Reporter.ReportEvent("performing sod eod ", "sod eod must be succesful","sod eod failed for deposits", false);
								}
*/
							}

						}
					}
					catch(Exception e)
					{
						Reporter.ReportEvent("Test Case execution is Failed", "end to end Deposit","end to end Deposit executition failed", false);
					}
				}
			
				
			}
			public static void E_E_Vault_transactions() 
			{
				int lcy_range=88;
				int fcy_range=186;
				String foreign_currency="USD";
				//String local_currency="GHS";
				String local_currency=WebDr.getValue("lcy_currency").split("\\|")[0];
				String local_gl_num=WebDr.getValue("Local_cur_GL_Branch").split("-")[0]+"-"+WebDr.getValue("Local_cur_GL_no");
				String foreign_gl_num=WebDr.getValue("Forex_cur_GL_Branch").split("-")[0]+"-"+WebDr.getValue("Forex_cur_GL_no");
				String branch_name="";
				String den_value="1";
				int number_rows=99;
				List<String> conditions=new ArrayList<String>();
				HashMap<String, HashMap<String,Object>> auth_dual = new HashMap<String, HashMap<String,Object>>();
				HashMap<String, HashMap<String, String>> transaction_codes = new HashMap<String, HashMap<String, String>>();

				HashMap<String, BigDecimal> tt_inquiry_lcy = new HashMap<String, BigDecimal>();
				HashMap<String, BigDecimal> tt_inquiry_lcy_after = new HashMap<String, BigDecimal>();
				HashMap<String, BigDecimal> vault_inquiry_lcy = new HashMap<String, BigDecimal>();
				HashMap<String, BigDecimal> vault_inquiry_lcy_after = new HashMap<String, BigDecimal>();
				HashMap<String, BigDecimal> vault_inquiry_lcy_after_delete = new HashMap<String, BigDecimal>();
				HashMap<String, BigDecimal> vault_inquiry_lcy_after_delete_receipt = new HashMap<String, BigDecimal>();
				HashMap<String, BigDecimal> vault_inquiry_lcy_after_ATM = new HashMap<String, BigDecimal>();

				HashMap<String, BigDecimal> tt_inquiry_fcy = new HashMap<String, BigDecimal>();
				HashMap<String, BigDecimal> tt_inquiry_fcy_after = new HashMap<String, BigDecimal>();
				HashMap<String, BigDecimal> vault_inquiry_fcy = new HashMap<String, BigDecimal>();
				HashMap<String, BigDecimal> vault_inquiry_fcy_after = new HashMap<String, BigDecimal>();
				HashMap<String, BigDecimal> vault_inquiry_fcy_after_delete = new HashMap<String, BigDecimal>();

				HashMap<String, BigDecimal> cd_teller_inquiry = new HashMap<String, BigDecimal>();
				HashMap<String, BigDecimal> cd_teller_inquiry_after = new HashMap<String, BigDecimal>();
				try 
				{
					ROATFunctions.refesh(false,"Teller");
					branch_name=ROATModules.fetch_user_branch();
					lcy_range=ROATModules.LCY_Payment_amount("1",99);
					fcy_range=ROATModules.FCY_Payment_amount(foreign_currency,"1",99);
					auth_dual=ROATModules.authoriation_dual_control_inquiry(branch_name);
					transaction_codes=ROATModules.Transaction_Codes();
					System.out.println(transaction_codes);

				}
				catch (Exception e) { Reporter.ReportEvent("End to end ", "prerequistes inquiry fetching must be successfull","prerequistes inquiry fetching failed", true);
				e.printStackTrace();
				}
				//*************************************************************Vault---LCY---FCY Setups**********************************************
				Reporter.ReportEvent("End to end ", "Currency account Maintainence","Currency account Maintainence", true);
				try
				{


					ROATFunctions.refesh(false,"Super User");

					if (HtmlReporter.tc_result == true)	WebDr.SetPageObjects("Setup_curr_maint");

					if (HtmlReporter.tc_result == true)ROATModules.Setup_navigation_currency_maint();

					WebDr.SetPageObjects("Setup_curr_maint");
					if (HtmlReporter.tc_result) WebDr.setText("X", "enter_branch_name", branch_name, "Entering branch Name", true);
					if (HtmlReporter.tc_result) WebDr.setText("X", "enter_curency_name", WebDr.getValue("Local_currency"), "Entering currency Name", true);
					if (HtmlReporter.tc_result) WebDr.Wait_Time(1000);

					if (WebDr.isExists("update_icon")==1)
					{
						if (HtmlReporter.tc_result == true)ROATModules.setup_currency_update_accept(branch_name,"Local_cur_GL_Branch","Local_cur_GL_no");
					}

					else if (WebDr.isExists("empty_table")==1)
					{
						if (HtmlReporter.tc_result == true)ROATModules.setup_currency_add_new_accept(branch_name,true);
					}

					WebDr.SetPageObjects("Setup_curr_maint");
					if (HtmlReporter.tc_result) WebDr.setText("X", "enter_branch_name", branch_name, "Entering branch Name", true);
					if (HtmlReporter.tc_result) WebDr.setText("X", "enter_curency_name", WebDr.getValue("Forex_currency"), "Entering currency Name", true);
					if (HtmlReporter.tc_result) WebDr.Wait_Time(1000);

					if (WebDr.isExists("update_icon")==1)
					{
						if (HtmlReporter.tc_result == true)ROATModules.setup_currency_update_accept(branch_name,"Forex_cur_GL_Branch","Forex_cur_GL_no");
					}

					if (WebDr.isExists("empty_table")==1)
					{
						if (HtmlReporter.tc_result == true)ROATModules.setup_currency_add_new_accept(branch_name,false);
					}


				}
				catch(Exception ex)
				{
					ex.printStackTrace();
					Reporter.ReportEvent("End to end ", "Currency account Maintainence FAILED","Currency account Maintainence FAILED", false);
				}
				
				//*************************************************************Vault---LCY--payment to cdc**********************************************
				Reporter.ReportEvent("End to end ", "vault lcy payment","vault lcy payment", true);
				HtmlReporter.tc_result=true;
				try
				{
					ROATModules.vault_cdc_payment_lcy(local_gl_num.split("-")[0],local_gl_num.split("-")[1],lcy_range,den_value,number_rows,auth_dual);
					//vault inquiry before transaction
					if(HtmlReporter.tc_result)
					{
						vault_inquiry_lcy=ROATModules.Vault_LCY_Inquiry();

						if(HtmlReporter.tc_result)
						{
							System.out.println("vault_inquiry_lcy:"+vault_inquiry_lcy);

							int pre_trans_number =ROATModules.support_function( true, "lcy", 0,"Payment To CDC",local_gl_num,local_gl_num,String.valueOf(lcy_range), true);
							System.out.println(pre_trans_number);

							ROATModules.vault_cdc_payment_lcy(local_gl_num.split("-")[0],local_gl_num.split("-")[1],lcy_range,den_value,number_rows,auth_dual);


							//vault Inquiry verification
							if (HtmlReporter.tc_result)
							{
								vault_inquiry_lcy_after=ROATModules.Vault_LCY_Inquiry();
								{
									System.out.println("vault_inquiry_lcy_after:"+vault_inquiry_lcy_after);
									conditions.add("CDC_Payments");
									ROATModules.teller_inquiry_check(vault_inquiry_lcy,vault_inquiry_lcy_after,conditions, lcy_range,"Addition");
									conditions.clear();
									conditions.add("Denomination_balance");
									conditions.add("balance");
									ROATModules.teller_inquiry_check(vault_inquiry_lcy,vault_inquiry_lcy_after,conditions, lcy_range,"Subtraction");
									conditions.clear();

								}

							}
							pre_trans_number =ROATModules.support_function( false, "lcy", pre_trans_number,"Payment To CDC",local_gl_num,local_gl_num,String.valueOf(lcy_range), true);
							System.out.println(pre_trans_number);

							if (HtmlReporter.tc_result)
							{
								vault_inquiry_lcy_after_delete=ROATModules.Vault_LCY_Inquiry();
								{
									System.out.println("vault_inquiry_lcy_after:"+vault_inquiry_lcy_after_delete);
									conditions.add("CDC_Payments");
									ROATModules.teller_inquiry_check(vault_inquiry_lcy_after,vault_inquiry_lcy_after_delete,conditions, lcy_range,"Subtraction");
									conditions.clear();
									conditions.add("Denomination_balance");
									conditions.add("balance");
									ROATModules.teller_inquiry_check(vault_inquiry_lcy_after,vault_inquiry_lcy_after_delete,conditions, lcy_range,"Addition");
									conditions.clear();

								}
							}
						}
					}
				}
				catch(Exception ex)
				{
					ex.printStackTrace();
					Reporter.ReportEvent("End to end ", "vault lcy payment FAILED","vault lcy payment FAILED", false);
				}

				//*************************************************************Vault---FCY--payment to cdc**********************************************
				Reporter.ReportEvent("End to end ", "vault lcy payment","vault lcy payment", true);
				HtmlReporter.tc_result=true;
				try
				{

//					ROATModules.vault_cdc_payment_fcy(foreign_currency,foreign_gl_num.split("-")[0],foreign_gl_num.split("-")[1],fcy_range,den_value,number_rows,auth_dual);
					HashMap<String,HashMap<String, BigDecimal>> inquiry_fcy = new HashMap<String,HashMap<String, BigDecimal>>();

					inquiry_fcy= ROATModules.Vault_FCY_Inquiry();
					Set<String> curreny_list=inquiry_fcy.keySet();
					String temp="";
					for(String set:curreny_list)
					{
						if(set.contains(foreign_currency))
							temp=set;
					}

					//vault inquiry before transaction
					if (HtmlReporter.tc_result)
					{
						vault_inquiry_fcy=inquiry_fcy.get(temp);
						System.out.println("vault_inquiry_fcy:"+vault_inquiry_fcy);

						int pre_trans_number =ROATModules.support_function( true, "fcy", 0,"Payment To CDC",foreign_gl_num,foreign_gl_num,String.valueOf(fcy_range), true);
						System.out.println(pre_trans_number);

						ROATModules.vault_cdc_payment_fcy(foreign_currency,foreign_gl_num.split("-")[0],foreign_gl_num.split("-")[1],fcy_range,den_value,number_rows,auth_dual);


						//vault Inquiry verification
						if (HtmlReporter.tc_result)
						{
							inquiry_fcy=ROATModules.Vault_FCY_Inquiry();
							if (HtmlReporter.tc_result)
							{
								vault_inquiry_fcy_after=inquiry_fcy.get(temp);
								System.out.println("vault_inquiry_fcy_after:"+vault_inquiry_fcy_after);

								conditions.add("CDC_Payments");
								ROATModules.teller_inquiry_check(vault_inquiry_fcy,vault_inquiry_fcy_after,conditions, fcy_range,"Addition");
								conditions.clear();
								conditions.add("Denomination_balance");
								conditions.add("balance");
								ROATModules.teller_inquiry_check(vault_inquiry_fcy,vault_inquiry_fcy_after,conditions, fcy_range,"Subtraction");
								conditions.clear();

							}
						}

						pre_trans_number =ROATModules.support_function( false, "fcy", pre_trans_number,"Payment To CDC",foreign_gl_num,foreign_gl_num,String.valueOf(fcy_range), true);
						System.out.println(pre_trans_number);


						if (HtmlReporter.tc_result)
						{
							inquiry_fcy=ROATModules.Vault_FCY_Inquiry();
							if (HtmlReporter.tc_result)
							{
								vault_inquiry_fcy_after_delete=inquiry_fcy.get(temp);
								System.out.println("vault_inquiry_fcy_after:"+vault_inquiry_fcy_after_delete);

								conditions.add("CDC_Payments");
								ROATModules.teller_inquiry_check(vault_inquiry_fcy_after,vault_inquiry_fcy_after_delete,conditions, fcy_range,"Subtraction");
								conditions.clear();
								conditions.add("Denomination_balance");
								conditions.add("balance");
								ROATModules.teller_inquiry_check(vault_inquiry_fcy_after,vault_inquiry_fcy_after_delete,conditions, fcy_range,"Addition");
								conditions.clear();

							}
						}

					}
				}
				catch(Exception ex)
				{
					ex.printStackTrace();
					Reporter.ReportEvent("End to end ", "vault lcy payment FAILED","vault lcy payment FAILED", false);
				}


				//*************************************************************Vault---LCY--Receipt from cdc**********************************************
				Reporter.ReportEvent("End to end ", "vault lcy Receipt from cdc","vault lcy Receipt from cdc", true);
				HtmlReporter.tc_result=true;
				try
				{
//					ROATModules.vault_cdc_receipt_lcy(local_gl_num.split("-")[0],local_gl_num.split("-")[1],lcy_range,den_value,number_rows,auth_dual);
					//vault inquiry before transaction
					if(HtmlReporter.tc_result)
					{
						vault_inquiry_lcy=ROATModules.Vault_LCY_Inquiry();

						if(HtmlReporter.tc_result)
						{
							System.out.println("vault_inquiry_lcy:"+vault_inquiry_lcy);

							int pre_trans_number =ROATModules.support_function( true, "lcy", 0,"Receipt From CDC",local_gl_num,local_gl_num,String.valueOf(lcy_range), true);
							System.out.println(pre_trans_number);

							ROATModules.vault_cdc_receipt_lcy(local_gl_num.split("-")[0],local_gl_num.split("-")[1],lcy_range,den_value,number_rows,auth_dual);


							//vault Inquiry verification
							if (HtmlReporter.tc_result)
							{
								vault_inquiry_lcy_after=ROATModules.Vault_LCY_Inquiry();
								{
									System.out.println("vault_inquiry_lcy_after:"+vault_inquiry_lcy_after);
									conditions.add("CDC_Receipts");
									conditions.add("Denomination_balance");
									conditions.add("balance");
									ROATModules.teller_inquiry_check(vault_inquiry_lcy,vault_inquiry_lcy_after,conditions, lcy_range,"Addition");
									conditions.clear();

								}
							}

							pre_trans_number =ROATModules.support_function( false, "lcy", pre_trans_number,"Receipt From CDC",local_gl_num,local_gl_num,String.valueOf(lcy_range), true);
							System.out.println(pre_trans_number);

							if (HtmlReporter.tc_result)
							{
								vault_inquiry_lcy_after_delete_receipt=ROATModules.Vault_LCY_Inquiry();
								{
									System.out.println("vault_inquiry_lcy_after:"+vault_inquiry_lcy_after_delete_receipt);
									conditions.add("CDC_Receipts");
									conditions.add("Denomination_balance");
									conditions.add("balance");
									ROATModules.teller_inquiry_check(vault_inquiry_lcy_after,vault_inquiry_lcy_after_delete_receipt,conditions, lcy_range,"Subtraction");
									conditions.clear();

								}
							}
						}
					}
				}
				catch(Exception ex)
				{
					ex.printStackTrace();
					Reporter.ReportEvent("End to end ", "vault lcy Receipt from cdc FAILED","vault lcy Receipt from cdc FAILED", false);
				}

				//*************************************************************Vault---FCY--Receipt from cdc**********************************************
				Reporter.ReportEvent("End to end ", "vault fcy Receipt from cdc","vault lcy Receipt from cdc ", true);
				HtmlReporter.tc_result=true;
				try
				{

//					ROATModules.vault_cdc_receipt_fcy(foreign_currency,foreign_gl_num.split("-")[0],foreign_gl_num.split("-")[1],fcy_range,den_value,number_rows,auth_dual);
					HashMap<String,HashMap<String, BigDecimal>> inquiry_fcy = new HashMap<String,HashMap<String, BigDecimal>>();

					inquiry_fcy= ROATModules.Vault_FCY_Inquiry();
					Set<String> curreny_list=inquiry_fcy.keySet();
					String temp="";
					for(String set:curreny_list)
					{
						if(set.contains(foreign_currency))
							temp=set;
					}

					//vault inquiry before transaction
					if (HtmlReporter.tc_result)
					{
						vault_inquiry_fcy=inquiry_fcy.get(temp);
						System.out.println("vault_inquiry_fcy:"+vault_inquiry_fcy);


						int pre_trans_number =ROATModules.support_function( true, "fcy", 0,"Receipt From CDC",foreign_gl_num,foreign_gl_num,String.valueOf(fcy_range), true);
						System.out.println(pre_trans_number);

						ROATModules.vault_cdc_receipt_fcy(foreign_currency,foreign_gl_num.split("-")[0],foreign_gl_num.split("-")[1],fcy_range,den_value,number_rows,auth_dual);


						//vault Inquiry verification
						if (HtmlReporter.tc_result)
						{
							inquiry_fcy=ROATModules.Vault_FCY_Inquiry();
							if (HtmlReporter.tc_result)
							{
								vault_inquiry_fcy_after=inquiry_fcy.get(temp);
								System.out.println("vault_inquiry_fcy_after:"+vault_inquiry_fcy_after);

								conditions.add("CDC_Receipts");
								conditions.add("Denomination_balance");
								conditions.add("balance");
								ROATModules.teller_inquiry_check(vault_inquiry_fcy,vault_inquiry_fcy_after,conditions, fcy_range,"Addition");
								conditions.clear();


							}
						}
						pre_trans_number =ROATModules.support_function( false, "fcy", pre_trans_number,"Receipt From CDC",foreign_gl_num,foreign_gl_num,String.valueOf(fcy_range), true);
						System.out.println(pre_trans_number);
						if (HtmlReporter.tc_result)
						{
							inquiry_fcy=ROATModules.Vault_FCY_Inquiry();
							if (HtmlReporter.tc_result)
							{
								vault_inquiry_fcy_after=inquiry_fcy.get(temp);
								System.out.println("vault_inquiry_fcy_after:"+vault_inquiry_fcy_after);

								conditions.add("CDC_Receipts");
								conditions.add("Denomination_balance");
								conditions.add("balance");
								ROATModules.teller_inquiry_check(vault_inquiry_fcy,vault_inquiry_fcy_after,conditions, fcy_range,"Subtraction");
								conditions.clear();


							}
						}
					}
				}
				catch(Exception ex)
				{
					ex.printStackTrace();
					Reporter.ReportEvent("End to end ", "vault fcy Receipt from cdc","vault lcy Receipt from cdc ", false);
				}

				//*************************************************************Vault---LCY--payment to ATM**********************************************
				Reporter.ReportEvent("End to end ", "LCY--payment to ATM","LCY--payment to ATM", true);
				HtmlReporter.tc_result=true;
				try
				{   
					String atm_gl_num=WebDr.getValue("Payment_ATM_GL_No");
//					ROATModules.atm_cdc_payment_lcy(atm_gl_num.split("-")[0],atm_gl_num.split("-")[1],lcy_range,den_value,number_rows,auth_dual,transaction_codes);
					//vault inquiry before transaction
					if(HtmlReporter.tc_result)
					{
						vault_inquiry_lcy=ROATModules.Vault_LCY_Inquiry();

						if(HtmlReporter.tc_result)
						{
							System.out.println("vault_inquiry_lcy:"+vault_inquiry_lcy);
							//String atm_gl_num=WebDr.getValue("Payment_ATM_GL_No");

							int pre_trans_number =ROATModules.support_function( true, "lcy", 0,"Payment To ATM",atm_gl_num,atm_gl_num,String.valueOf(lcy_range), true);
							System.out.println(pre_trans_number);

							ROATModules.atm_cdc_payment_lcy(atm_gl_num.split("-")[0],atm_gl_num.split("-")[1],lcy_range,den_value,number_rows,auth_dual,transaction_codes);


							//vault Inquiry verification
							if (HtmlReporter.tc_result)
							{
								vault_inquiry_lcy_after=ROATModules.Vault_LCY_Inquiry();
								{
									System.out.println("vault_inquiry_lcy_after:"+vault_inquiry_lcy_after);
									conditions.add("CDC_Payments");
									ROATModules.teller_inquiry_check(vault_inquiry_lcy,vault_inquiry_lcy_after,conditions, lcy_range,"Addition");
									conditions.clear();
									conditions.add("Denomination_balance");
									conditions.add("balance");
									ROATModules.teller_inquiry_check(vault_inquiry_lcy,vault_inquiry_lcy_after,conditions, lcy_range,"Subtraction");
									conditions.clear();


								}
							}
							pre_trans_number =ROATModules.support_function( false, "lcy", pre_trans_number,"Payment To ATM",atm_gl_num,atm_gl_num,String.valueOf(lcy_range), true);
							System.out.println(pre_trans_number);

							if (HtmlReporter.tc_result)
							{
								vault_inquiry_lcy_after_ATM=ROATModules.Vault_LCY_Inquiry();
								{
									System.out.println("vault_inquiry_lcy_after:"+vault_inquiry_lcy_after_ATM);
									conditions.add("CDC_Payments");
									ROATModules.teller_inquiry_check(vault_inquiry_lcy_after,vault_inquiry_lcy_after_ATM,conditions, lcy_range,"Addition");
									conditions.clear();
									conditions.add("Denomination_balance");
									conditions.add("balance");
									ROATModules.teller_inquiry_check(vault_inquiry_lcy_after,vault_inquiry_lcy_after_ATM,conditions, lcy_range,"Subtraction");
									conditions.clear();


								}
							}
						}
					}
				}
				catch(Exception ex)
				{
					ex.printStackTrace();
					Reporter.ReportEvent("End to end ", "LCY--payment to ATM","LCY--payment to ATM FAILED", false);
				}
				//*************************************************************Vault---Vault LCY--change**********************************************
				Reporter.ReportEvent("End to end ", "Vault LCY--change","Vault LCY--change", true);
				HtmlReporter.tc_result=true;
				try
				{
					//vault inquiry before transaction
					if(HtmlReporter.tc_result)
					{
						vault_inquiry_lcy=ROATModules.Vault_LCY_Inquiry();

						if(HtmlReporter.tc_result)
						{
							System.out.println("vault_inquiry_lcy:"+vault_inquiry_lcy);

							ROATModules.vault_cdc_change_lcy(WebDr.getValue("Change_in_amt_lcy"));


							//vault Inquiry verification
							if (HtmlReporter.tc_result)
							{
								vault_inquiry_lcy_after=ROATModules.Vault_LCY_Inquiry();
								{
									System.out.println("vault_inquiry_lcy_after:"+vault_inquiry_lcy_after);
									conditions.add("CDC_Payments");
									conditions.clear();
									conditions.add("Denomination_balance");
									conditions.add("balance");
									ROATModules.teller_inquiry_check(vault_inquiry_lcy,vault_inquiry_lcy_after,conditions, 0,"Subtraction");
									conditions.clear();


								}
							}
						}
					}
				}
				catch(Exception ex)
				{
					ex.printStackTrace();
					Reporter.ReportEvent("End to end ", "Vault LCY--change","Vault LCY--change FAILED", false);
				}

				//*************************************************************Vault---Vault fCY--change**********************************************
				Reporter.ReportEvent("End to end ", "Vault LCY--change","Vault LCY--change", true);
				HtmlReporter.tc_result=true;
				try
				{
					HashMap<String,HashMap<String, BigDecimal>> inquiry_fcy = new HashMap<String,HashMap<String, BigDecimal>>();

					inquiry_fcy= ROATModules.Vault_FCY_Inquiry();
					Set<String> curreny_list=inquiry_fcy.keySet();
					String temp="";
					for(String set:curreny_list)
					{
						if(set.contains(foreign_currency))
							temp=set;
					}

					//vault inquiry before transaction
					if (HtmlReporter.tc_result)
					{
						vault_inquiry_fcy=inquiry_fcy.get(temp);
						System.out.println("vault_inquiry_fcy:"+vault_inquiry_fcy);


						ROATModules.vault_cdc_change_fcy(WebDr.getValue("Change_in_amt_fcy"));

						//vault Inquiry verification
						if (HtmlReporter.tc_result)
						{
							inquiry_fcy=ROATModules.Vault_FCY_Inquiry();
							if (HtmlReporter.tc_result)
							{
								vault_inquiry_fcy_after=inquiry_fcy.get(temp);
								System.out.println("vault_inquiry_fcy_after:"+vault_inquiry_fcy_after);
								conditions.add("CDC_Payments");
								conditions.clear();
								conditions.add("Denomination_balance");
								conditions.add("balance");
								ROATModules.teller_inquiry_check(vault_inquiry_fcy,vault_inquiry_fcy_after,conditions, 0,"Subtraction");
								conditions.clear();



							}
						}
					}
				}

				catch(Exception ex)
				{
					ex.printStackTrace();
					Reporter.ReportEvent("End to end ", "Vault FCY--change","Vault FCY--change FAILED", false);
				}




				
				
			}
			public static void E_E_Transactions()
			{
				int lcy_range=88;
				int fcy_range=186;
				String foreign_currency="USD";
				//String local_currency="GHS";
				String local_currency=WebDr.getValue("lcy_currency").split("\\|")[0];
				String local_gl_num=WebDr.getValue("Local_cur_GL_Branch").split("-")[0]+"-"+WebDr.getValue("Local_cur_GL_no");
				String foreign_gl_num=WebDr.getValue("Forex_cur_GL_Branch").split("-")[0]+"-"+WebDr.getValue("Forex_cur_GL_no");
				String branch_name="";
				String den_value="1";
				int number_rows=99;
				List<String> conditions=new ArrayList<String>();
				HashMap<String, HashMap<String,Object>> auth_dual = new HashMap<String, HashMap<String,Object>>();
				HashMap<String, HashMap<String, String>> transaction_codes = new HashMap<String, HashMap<String, String>>();

				HashMap<String, BigDecimal> tt_inquiry_lcy = new HashMap<String, BigDecimal>();
				HashMap<String, BigDecimal> tt_inquiry_lcy_after = new HashMap<String, BigDecimal>();
				HashMap<String, BigDecimal> vault_inquiry_lcy = new HashMap<String, BigDecimal>();
				HashMap<String, BigDecimal> vault_inquiry_lcy_after = new HashMap<String, BigDecimal>();
				HashMap<String, BigDecimal> vault_inquiry_lcy_after_delete = new HashMap<String, BigDecimal>();
				HashMap<String, BigDecimal> vault_inquiry_lcy_after_delete_receipt = new HashMap<String, BigDecimal>();
				HashMap<String, BigDecimal> vault_inquiry_lcy_after_ATM = new HashMap<String, BigDecimal>();

				HashMap<String, BigDecimal> tt_inquiry_fcy = new HashMap<String, BigDecimal>();
				HashMap<String, BigDecimal> tt_inquiry_fcy_after = new HashMap<String, BigDecimal>();
				HashMap<String, BigDecimal> vault_inquiry_fcy = new HashMap<String, BigDecimal>();
				HashMap<String, BigDecimal> vault_inquiry_fcy_after = new HashMap<String, BigDecimal>();
				HashMap<String, BigDecimal> vault_inquiry_fcy_after_delete = new HashMap<String, BigDecimal>();

				HashMap<String, BigDecimal> cd_teller_inquiry = new HashMap<String, BigDecimal>();
				HashMap<String, BigDecimal> cd_teller_inquiry_after = new HashMap<String, BigDecimal>();
				try 
				{
					ROATFunctions.refesh(false,"Teller");
					branch_name=ROATModules.fetch_user_branch();
					lcy_range=ROATModules.LCY_Payment_amount("1",99);
					fcy_range=ROATModules.FCY_Payment_amount(foreign_currency,"1",99);
					auth_dual=ROATModules.authoriation_dual_control_inquiry(branch_name);
					transaction_codes=ROATModules.Transaction_Codes();
					System.out.println(transaction_codes);

				}
				catch (Exception e) { Reporter.ReportEvent("End to end ", "prerequistes inquiry fetching must be successfull","prerequistes inquiry fetching failed", true);
				e.printStackTrace();
				}
				//*************************************************************Vault---LCY---FCY Setups**********************************************
				Reporter.ReportEvent("End to end ", "Currency account Maintainence","Currency account Maintainence", true);
				try
				{


					ROATFunctions.refesh(false,"Super User");

					if (HtmlReporter.tc_result == true)	WebDr.SetPageObjects("Setup_curr_maint");

					if (HtmlReporter.tc_result == true)ROATModules.Setup_navigation_currency_maint();

					WebDr.SetPageObjects("Setup_curr_maint");
					if (HtmlReporter.tc_result) WebDr.setText("X", "enter_branch_name", branch_name, "Entering branch Name", true);
					if (HtmlReporter.tc_result) WebDr.setText("X", "enter_curency_name", WebDr.getValue("Local_currency"), "Entering currency Name", true);
					if (HtmlReporter.tc_result) WebDr.Wait_Time(1000);

					if (WebDr.isExists("update_icon")==1)
					{
						if (HtmlReporter.tc_result == true)ROATModules.setup_currency_update_accept(branch_name,"Local_cur_GL_Branch","Local_cur_GL_no");
					}

					else if (WebDr.isExists("empty_table")==1)
					{
						if (HtmlReporter.tc_result == true)ROATModules.setup_currency_add_new_accept(branch_name,true);
					}

					WebDr.SetPageObjects("Setup_curr_maint");
					if (HtmlReporter.tc_result) WebDr.setText("X", "enter_branch_name", branch_name, "Entering branch Name", true);
					if (HtmlReporter.tc_result) WebDr.setText("X", "enter_curency_name", WebDr.getValue("Forex_currency"), "Entering currency Name", true);
					if (HtmlReporter.tc_result) WebDr.Wait_Time(1000);

					if (WebDr.isExists("update_icon")==1)
					{
						if (HtmlReporter.tc_result == true)ROATModules.setup_currency_update_accept(branch_name,"Forex_cur_GL_Branch","Forex_cur_GL_no");
					}

					if (WebDr.isExists("empty_table")==1)
					{
						if (HtmlReporter.tc_result == true)ROATModules.setup_currency_add_new_accept(branch_name,false);
					}


				}
				catch(Exception ex)
				{
					ex.printStackTrace();
					Reporter.ReportEvent("End to end ", "Currency account Maintainence FAILED","Currency account Maintainence FAILED", false);
				}



				//*************************************************************Teller---LCY Receipt**********************************************
				Reporter.ReportEvent("End to end ", "Teller LCY receipt","Teller LCY receipt", true);
				HtmlReporter.tc_result=true;
				try
				{
					//Teller inquiry before transaction
					tt_inquiry_lcy=ROATModules.LCY_inquiry();
					if (HtmlReporter.tc_result)
					{
						System.out.println("tt_inquiry_lcy:"+tt_inquiry_lcy);

						//vault inquiry before transaction
						vault_inquiry_lcy=ROATModules.Vault_LCY_Inquiry();
						if (HtmlReporter.tc_result)
						{
							System.out.println("vault_inquiry_lcy:"+vault_inquiry_lcy);

							//Teller receipt 
							ROATModules.tt_receipt_lcy(lcy_range,"1",99,auth_dual);

							//teller cash drawer verification
							if (HtmlReporter.tc_result)
							{
								tt_inquiry_lcy_after=ROATModules.LCY_inquiry();
								System.out.println("tt_inquiry_lcy_after:"+tt_inquiry_lcy_after);
								conditions.add("Receipts");
								conditions.add("Balance");
								conditions.add("Denomination_balance");
								if (HtmlReporter.tc_result)
								{
									Reporter.ReportEvent("Teller LCY receipt", "Teller LCY receipt","Teller LCY receipt", true);
									ROATModules.teller_inquiry_check(tt_inquiry_lcy,tt_inquiry_lcy_after,conditions, lcy_range,"Addition");
									conditions.clear();

									//vault Inquiry verification
									vault_inquiry_lcy_after=ROATModules.Vault_LCY_Inquiry();
									System.out.println("vault_inquiry_lcy_after:"+vault_inquiry_lcy_after);
									conditions.add("Teller_Receipts");
									Reporter.ReportEvent("Teller LCY receipt", "Teller LCY receipt","Teller LCY receipt", true);
									ROATModules.teller_inquiry_check(vault_inquiry_lcy,vault_inquiry_lcy_after,conditions, lcy_range,"Addition");
									conditions.clear();
									conditions.add("Denomination_balance");
									conditions.add("balance");
									ROATModules.teller_inquiry_check(vault_inquiry_lcy,vault_inquiry_lcy_after,conditions, lcy_range,"Subtraction");
									conditions.clear();

								}
							}
						}
					}
				}
				catch(Exception e)
				{
					Reporter.ReportEvent("End to end ", "Teller LCY receipt Failed","Teller LCY receipt Failed", false);
					e.printStackTrace();
				}


				//*************************************************************Teller---FCY Receipt**********************************************
				Reporter.ReportEvent("End to end ", "Teller FCY receipt","Teller FCY receipt", true);
				HtmlReporter.tc_result=true;
				try{	
					HashMap<String,HashMap<String, BigDecimal>> inquiry_fcy = new HashMap<String,HashMap<String, BigDecimal>>();

					inquiry_fcy= ROATModules.FCY_Inquiry();
					Set<String> curreny_list=inquiry_fcy.keySet();
					String temp="";
					for(String set:curreny_list)
					{
						if(set.contains(foreign_currency))
							temp=set;
					}


					//Teller inquiry before transaction
					if (HtmlReporter.tc_result)
					{
						tt_inquiry_fcy=inquiry_fcy.get(temp);
						System.out.println("tt_inquiry_fcy:"+tt_inquiry_fcy);

						//vault inquiry before transaction
						inquiry_fcy=ROATModules.Vault_FCY_Inquiry();
						if (HtmlReporter.tc_result)
						{
							vault_inquiry_fcy=inquiry_fcy.get(temp);
							System.out.println("vault_inquiry_fcy:"+vault_inquiry_fcy);

							//Teller receipt 
							ROATModules.tt_receipt_fcy(foreign_currency,fcy_range,"1",99,auth_dual);

							//teller cash drawer verifications
							if (HtmlReporter.tc_result)
							{

								inquiry_fcy= ROATModules.FCY_Inquiry();
								tt_inquiry_fcy_after=inquiry_fcy.get(temp);
								System.out.println("tt_inquiry_fcy_after:"+tt_inquiry_fcy_after);
								conditions.add("Receipts");
								conditions.add("Balance");
								conditions.add("Denomination_balance");
								Reporter.ReportEvent("End to end ", "Teller FCY receipt","Teller FCY receipt", true);
								ROATModules.teller_inquiry_check(tt_inquiry_fcy,tt_inquiry_fcy_after,conditions, fcy_range,"Addition");
								conditions.clear();

								//vault Inquiry verification
								if (HtmlReporter.tc_result)
								{
									inquiry_fcy=ROATModules.Vault_FCY_Inquiry();
									if (HtmlReporter.tc_result)
									{
										vault_inquiry_fcy_after=inquiry_fcy.get(temp);
										System.out.println("vault_inquiry_fcy_after:"+vault_inquiry_fcy_after);
										conditions.add("Teller_Receipts");
										Reporter.ReportEvent("End to end ", "Teller FCY receipt","Teller FCY receipt", true);
										ROATModules.teller_inquiry_check(vault_inquiry_fcy,vault_inquiry_fcy_after,conditions, fcy_range,"Addition");
										conditions.clear();
										conditions.add("Denomination_balance");
										conditions.add("balance");
										ROATModules.teller_inquiry_check(vault_inquiry_fcy,vault_inquiry_fcy_after,conditions, fcy_range,"Subtraction");
										conditions.clear();
									}
								}
							}
						}
					}
				}
				catch(Exception ex)
				{
					ex.printStackTrace();
					Reporter.ReportEvent("Test Case execution is Failed", "Teller FCY receipt","Teller FCY receipt ffailed", false);
				}

				//*************************************************************Teller---LCY Payment**********************************************
				Reporter.ReportEvent("End to end ", "Teller LCY payment","Teller LCY payment", true);
				HtmlReporter.tc_result=true;
				try
				{
					//Teller inquiry before transaction
					tt_inquiry_lcy=ROATModules.LCY_inquiry();

					if (HtmlReporter.tc_result)
					{
						System.out.println("tt_inquiry_lcy:"+tt_inquiry_lcy);

						//vault inquiry before transaction
						if(HtmlReporter.tc_result)
						{
							vault_inquiry_lcy=ROATModules.Vault_LCY_Inquiry();
							System.out.println("vault_inquiry_lcy:"+vault_inquiry_lcy);

							//Teller receipt 
							if (HtmlReporter.tc_result)ROATModules.tt_payment_lcy(lcy_range,"1",99,auth_dual);

							//teller cash drawer verification
							tt_inquiry_lcy_after=ROATModules.LCY_inquiry();
							if (HtmlReporter.tc_result)
							{
								System.out.println("tt_inquiry_lcy_after:"+tt_inquiry_lcy_after);
								conditions.add("Payment");
								ROATModules.teller_inquiry_check(tt_inquiry_lcy,tt_inquiry_lcy_after,conditions, lcy_range,"Addition");
								conditions.clear();
								conditions.add("Balance");
								conditions.add("Denomination_balance");
								ROATModules.teller_inquiry_check(tt_inquiry_lcy,tt_inquiry_lcy_after,conditions, lcy_range,"Subtraction");
								conditions.clear();
							}

							//vault Inquiry verification
							if (HtmlReporter.tc_result)
							{
								vault_inquiry_lcy_after=ROATModules.Vault_LCY_Inquiry();
								{
									System.out.println("vault_inquiry_lcy_after:"+vault_inquiry_lcy_after);
									conditions.add("Teller_Payment");
									conditions.add("Denomination_balance");
									conditions.add("balance");
									ROATModules.teller_inquiry_check(vault_inquiry_lcy,vault_inquiry_lcy_after,conditions, lcy_range,"Addition");
									conditions.clear();
								}
							}
						}
					}
				}
				catch(Exception e)
				{
					e.printStackTrace();
					Reporter.ReportEvent("Test Case execution is Failed", "LCY payment transaction","LCY payment transaction failed", false);
				}

				//*************************************************************Teller---FCY payment**********************************************
				Reporter.ReportEvent("End to end ", "Teller FCY payment","Teller FCY payment", true);
				HtmlReporter.tc_result=true;
				try{	
					HashMap<String,HashMap<String, BigDecimal>> inquiry_fcy = new HashMap<String,HashMap<String, BigDecimal>>();

					inquiry_fcy= ROATModules.FCY_Inquiry();
					Set<String> curreny_list=inquiry_fcy.keySet();
					String temp="";
					for(String set:curreny_list)
					{
						if(set.contains(foreign_currency))
							temp=set;
					}


					//Teller inquiry before transaction
					if (HtmlReporter.tc_result)
					{
						tt_inquiry_fcy=inquiry_fcy.get(temp);
						System.out.println("tt_inquiry_fcy:"+tt_inquiry_fcy);

						//vault inquiry before transaction
						if (HtmlReporter.tc_result)
						{
							inquiry_fcy=ROATModules.Vault_FCY_Inquiry();
							if (HtmlReporter.tc_result)
							{
								vault_inquiry_fcy=inquiry_fcy.get(temp);
								System.out.println("vault_inquiry_fcy:"+vault_inquiry_fcy);

								//Teller receipt 
								if (HtmlReporter.tc_result)ROATModules.tt_payment_fcy(foreign_currency,fcy_range,"1",99,auth_dual);

								//teller cash drawer verifications
								if (HtmlReporter.tc_result){
									inquiry_fcy= ROATModules.FCY_Inquiry();
									if (HtmlReporter.tc_result)
									{
										tt_inquiry_fcy_after=inquiry_fcy.get(temp);
										if (HtmlReporter.tc_result)
										{
											System.out.println("tt_inquiry_fcy_after:"+tt_inquiry_fcy_after);
											conditions.add("Payment");
											ROATModules.teller_inquiry_check(tt_inquiry_fcy,tt_inquiry_fcy_after,conditions, fcy_range,"Addition");
											conditions.clear();
											conditions.add("Balance");
											conditions.add("Denomination_balance");
											if (HtmlReporter.tc_result)ROATModules.teller_inquiry_check(tt_inquiry_fcy,tt_inquiry_fcy_after,conditions, fcy_range,"Subtraction");
											conditions.clear();


											//vault Inquiry verification
											if (HtmlReporter.tc_result)
											{
												inquiry_fcy=ROATModules.Vault_FCY_Inquiry();
												if (HtmlReporter.tc_result)
												{
													vault_inquiry_fcy_after=inquiry_fcy.get(temp);
													if (HtmlReporter.tc_result)
													{
														System.out.println("vault_inquiry_fcy_after:"+vault_inquiry_fcy_after);
														conditions.add("Teller_Payment");
														conditions.add("Denomination_balance");
														conditions.add("balance");
														if (HtmlReporter.tc_result)ROATModules.teller_inquiry_check(vault_inquiry_fcy,vault_inquiry_fcy_after,conditions, fcy_range,"Addition");
														conditions.clear();
													}
												}
											}
										}
									}
								}
							}
						}
					}
				}
				catch(Exception ex)
				{
					ex.printStackTrace();
					Reporter.ReportEvent("Test Case execution is Failed", "Teller FCY payment","Teller FCY payment failed", false);
				}

				//*************************************************************Teller---LCY Transferout**********************************************
				Reporter.ReportEvent("End to end ", "Teller LCY Tranferout","Teller LCY Tranferout", true);
				HtmlReporter.tc_result=true;
				try
				{

					//Teller inquiry before transaction
					tt_inquiry_lcy=ROATModules.LCY_inquiry();

					if (HtmlReporter.tc_result)
					{
						System.out.println("tt_inquiry_lcy:"+tt_inquiry_lcy);

						//payee inquiry--before
						if (HtmlReporter.tc_result)
						{
							cd_teller_inquiry=ROATModules.CD_teller_inquiry(local_currency,WebDr.getValue("tranferout_id"));	



							//Teller transferout 
							if (HtmlReporter.tc_result)ROATModules.tt_tranfer_out_lcy(WebDr.getValue("tranferout_id"),lcy_range,"1",99,auth_dual);



							if (HtmlReporter.tc_result)
							{
								//Teller inquiry after transaction
								tt_inquiry_lcy_after=ROATModules.LCY_inquiry();
								System.out.println("tt_inquiry_lcy_after:"+tt_inquiry_lcy_after);
								conditions.add("Payment");
								ROATModules.teller_inquiry_check(tt_inquiry_lcy,tt_inquiry_lcy_after,conditions, lcy_range,"Addition");
								conditions.clear();
								conditions.add("Balance");
								conditions.add("Denomination_balance");
								ROATModules.teller_inquiry_check(tt_inquiry_lcy,tt_inquiry_lcy_after,conditions, lcy_range,"Subtraction");
								conditions.clear();

								if (HtmlReporter.tc_result)
								{
									System.out.println("tt_inquiry_lcy:"+tt_inquiry_lcy);

									//payee inquiry---after
									if (HtmlReporter.tc_result)
									{

										cd_teller_inquiry_after=ROATModules.CD_teller_inquiry(local_currency,WebDr.getValue("tranferout_id"));	

										conditions.add("Receipts");
										conditions.add("Balance");
										conditions.add("Denomination_balance");
										ROATModules.teller_inquiry_check(cd_teller_inquiry,cd_teller_inquiry_after,conditions, lcy_range,"Addition");
										conditions.clear();

									}
								}
							}	
						}
					}
				}
				catch(Exception ex)
				{
					ex.printStackTrace();
					Reporter.ReportEvent("Test Case execution is Failed", "end to end Transfer out","end to end Deposit executition failed", false);
				}


				//*************************************************************Teller---FCY Transferout**********************************************
				Reporter.ReportEvent("End to end ", "Teller FCY Tranferout","Teller FCY Tranferout", true);
				HtmlReporter.tc_result=true;
				try
				{
					HashMap<String,HashMap<String, BigDecimal>> inquiry_fcy = new HashMap<String,HashMap<String, BigDecimal>>();

					inquiry_fcy= ROATModules.FCY_Inquiry();
					Set<String> curreny_list=inquiry_fcy.keySet();
					String temp="";
					for(String set:curreny_list)
					{
						if(set.contains(foreign_currency))
							temp=set;
					}
					//Teller inquiry before transaction---FCY
					if (HtmlReporter.tc_result)
					{
						tt_inquiry_fcy=inquiry_fcy.get(temp);

						if (HtmlReporter.tc_result)
						{
							System.out.println("tt_inquiry_fcy:"+tt_inquiry_fcy);

							//payee inquiry--before
							if (HtmlReporter.tc_result)
							{
								cd_teller_inquiry=ROATModules.CD_teller_inquiry(foreign_currency,WebDr.getValue("tranferout_id"));	



								//Teller transferout 
								if (HtmlReporter.tc_result)ROATModules.tt_tranfer_out_fcy(WebDr.getValue("tranferout_id"),foreign_currency,fcy_range,"1",99,auth_dual);



								if (HtmlReporter.tc_result)
								{
									//Teller inquiry after transaction
									inquiry_fcy= ROATModules.FCY_Inquiry();
									tt_inquiry_fcy_after=inquiry_fcy.get(temp);
									if (HtmlReporter.tc_result)
									{
										System.out.println("tt_inquiry_fcy:"+tt_inquiry_fcy_after);
										conditions.add("Payment");
										ROATModules.teller_inquiry_check(tt_inquiry_fcy,tt_inquiry_fcy_after,conditions, fcy_range,"Addition");
										conditions.clear();
										conditions.add("Balance");
										conditions.add("Denomination_balance");
										ROATModules.teller_inquiry_check(tt_inquiry_fcy,tt_inquiry_fcy_after,conditions, fcy_range,"Subtraction");
										conditions.clear();


										//payee inquiry---after
										if (HtmlReporter.tc_result)
										{

											cd_teller_inquiry_after=ROATModules.CD_teller_inquiry(foreign_currency,WebDr.getValue("tranferout_id"));	

											conditions.add("Receipts");
											conditions.add("Balance");
											conditions.add("Denomination_balance");
											ROATModules.teller_inquiry_check(cd_teller_inquiry,cd_teller_inquiry_after,conditions, fcy_range,"Addition");
											conditions.clear();

										}
									}
								}	
							}
						}
					}
				}
				catch(Exception ex)
				{
					ex.printStackTrace();
					Reporter.ReportEvent("Test Case execution is Failed", "end to end Transfer out","end to end Transfer out executition failed", false);
				}




				
			}
//*********************** Forex Cheque Deposit ***********************************************************************
			
			public static void end_to_end_ForexChequeDeposit(){
				
				String user_branch="",user_id="",user_name="",Branch="",voucher="";
				int pre_trans_number=0,lcy_range,fcy_range;
				
				HashMap<String, Map<String,String>> store_forex_rate = new HashMap<String, Map<String,String>>();
				HashMap<String, String> storeLedgerAccountNumber = new HashMap<String, String>();
				HashMap<String, String> storeAccountInquiryDetails = new HashMap<String, String>();
				HashMap<String, String> storeNewAccountInquiryDetails = new HashMap<String, String>();
				HashMap<String,String> amounts = new HashMap<String,String>();
				
				String cheque_account = WebDr.getValue("Cheque Account");
				String deposit_account = WebDr.getValue("Deposit Account");
				String bankID = WebDr.getValue("Bank ID");
				String chequeCurrency = WebDr.getValue("Cheque Currency");
				String chequeCurrencyLimit = WebDr.getValue("Cheque Currency fcy");
				String depositCurrency = WebDr.getValue("Deposit Currency");
				String depositLocalFlag = WebDr.getValue("Deposit Local Flag");
				
				try{
					
					ROATFunctions.refesh(false, "Super User");
					
					//get voucher Type
					if (HtmlReporter.tc_result){
						ROATModules.navigate_MICRTypes();
						if(depositLocalFlag.toUpperCase().equals("TRUE")){
						voucher = ROATModules.getMICRTypesVoucherDeposit("Yes");
						}else{
							voucher = ROATModules.getMICRTypesVoucherDeposit("No");	
						}
						System.out.println(voucher);
					}
					
					
					//store forex rates
					ROATFunctions.refesh(false, "Central Supervisor");
					WebDr.Wait_Time(2000);
					if(Boolean.valueOf(depositLocalFlag)){
						store_forex_rate.putAll(ROATModules.readandStoreForexRate(chequeCurrency));
					}else{
						store_forex_rate.putAll(ROATModules.readandStoreForexRate(depositCurrency));
					}
					
					System.out.println(store_forex_rate);
					
					
					//get old transaction no from support function
					if(Boolean.valueOf(depositLocalFlag))
					{
						 pre_trans_number =ROATModules.support_function( true, "lcy", 0,"Forex Cheque Deposit",deposit_account,cheque_account,null, true);
					}
					else
					{
						 pre_trans_number =ROATModules.support_function( true, "fcy", 0,"Forex Cheque Deposit",deposit_account,cheque_account,null, true);
					}
					
					
					ROATFunctions.refesh(false,"Teller");
					if (HtmlReporter.tc_result)
					{ 
						user_branch = ROATModules.fetch_user_branch();
						user_id=ROATFunctions.fetch_user_Id();
						user_name = ROATModules.fetchuser();
						Branch=ROATFunctions.fetch_user_branch_name();
						
					}
					
					//verify cash position
					if(HtmlReporter.tc_result){
						ROATModules.navigateOCP();
						boolean ocp = ROATModules.verifyOCP();
					}
					
					//get original balance for transaction accounts
					storeAccountInquiryDetails = ROATModules.account_inquiryExSell(deposit_account.split("-")[1],deposit_account.split("-")[0]);
					storeAccountInquiryDetails.putAll(ROATModules.account_inquiryExSell(cheque_account.split("-")[1],cheque_account.split("-")[0]));
					
				
					
					
					//cheque deposit
					if(HtmlReporter.tc_result){
						ROATModules.Forex_Navigation();
						amounts = ROATModules.end_to_end_Forex_Cheque_deposit(depositLocalFlag,cheque_account,deposit_account,chequeCurrency,depositCurrency,store_forex_rate,bankID,voucher);
					}
					
					WebDr.Wait_Time(15000);
					//ROATFunctions.refesh(false, "Teller");
					//get new balance for transaction accounts
					storeNewAccountInquiryDetails = ROATModules.account_inquiryExSell(deposit_account.split("-")[1],deposit_account.split("-")[0]);
					storeNewAccountInquiryDetails.putAll(ROATModules.account_inquiryExSell(cheque_account.split("-")[1],cheque_account.split("-")[0]));
					

					//get balance figures and compare them
	   				double oldValue = Double.parseDouble(storeAccountInquiryDetails.get("current_Actual Balance"+cheque_account.split("-")[1]).split(":")[1].trim().replace(",", ""));
	  				double newValue = Double.parseDouble(storeNewAccountInquiryDetails.get("current_Actual Balance"+cheque_account.split("-")[1]).split(":")[1].trim().replace(",", ""));
	  				if (HtmlReporter.tc_result)WebDr.Wait_Time(3000);
	  				if(oldValue>newValue && oldValue-newValue==Double.parseDouble(amounts.get("Cheque_Amount").replace(",", "").trim()))
	  				{
	   					System.out.println("cheque currency has been debited");
	   					Reporter.ReportEvent("cheque currency debited", "cheque currency has been debited","cheque currency has been debited", true);
	  				}
	  				else
	  				{
	  	  				Reporter.ReportEvent("cheque currency debited", "cheque currency has been debited","cheque currency debit has been failed ", false);
	     			}
	  				
		  			oldValue = Double.parseDouble(storeAccountInquiryDetails.get("current_Actual Balance"+deposit_account.split("-")[1]).split(":")[1].trim().replace(",", ""));
		  			newValue = Double.parseDouble(storeNewAccountInquiryDetails.get("current_Actual Balance"+deposit_account.split("-")[1]).split(":")[1].trim().replace(",", ""));
		  			if(oldValue<newValue)
		  			{
		   				System.out.println("deposit currency has been credited");
		   				Reporter.ReportEvent("deposit currency credited", "deposit currency has been credited","deposit currency credit has been passed", true);
		  			}
		  			else
		  			{
		   				Reporter.ReportEvent("deposit currency credited", "deposit currency has been credited","deposit currency credit has been failed ", false);
		  			}
					
					
					//delete transaction from support function

	  				if(Boolean.valueOf(depositLocalFlag))
	  				{
	  					ROATModules.support_function_forex_deposit( false, "lcy", pre_trans_number,"Forex Cheque Deposit",deposit_account,cheque_account,amounts.get("Cheque_Amount").replace(",", "").trim(),amounts.get("Credit_Amount").replace(",", "").trim(), true);
					}
					else
					{
						ROATModules.support_function_forex_deposit( false, "lcy", pre_trans_number,"Forex Cheque Deposit",deposit_account,cheque_account,amounts.get("Cheque_Amount").replace(",", "").trim(),amounts.get("Credit_Amount").replace(",", "").trim(), true);
					}
	  				
	  			
					
				}catch(Exception e){
					e.printStackTrace();
					Reporter.ReportEvent("Forex Cheque Deposit End to End", "Forex Cheque Deposit End to End performed successfully", "Forex Cheque Deposit End to End failed", false);
				}
				
			
				
			}
			
			
			public static void end_to_end_ForexChequeDeposit_breaches(){
				
				
				//***************************************************Forex Exchange Cheque Deposit with Breaches*****************************************
				String user_branch="",user_id="",user_name="",Branch="",voucher="";
				int pre_trans_number=0,lcy_range,fcy_range;
				
				HashMap<String, Map<String,String>> store_forex_rate = new HashMap<String, Map<String,String>>();
				HashMap<String, String> storeLedgerAccountNumber = new HashMap<String, String>();
				HashMap<String, String> storeAccountInquiryDetails = new HashMap<String, String>();
				HashMap<String, String> storeNewAccountInquiryDetails = new HashMap<String, String>();
				HashMap<String,String> amounts = new HashMap<String,String>();
				
				String cheque_account = WebDr.getValue("Cheque Account");
				String deposit_account = WebDr.getValue("Deposit Account");
				String bankID = WebDr.getValue("Bank ID");
				String chequeCurrency = WebDr.getValue("Cheque Currency");
				String chequeCurrencyLimit = WebDr.getValue("Cheque Currency fcy");
				String depositCurrency = WebDr.getValue("Deposit Currency");
				String depositLocalFlag = WebDr.getValue("Deposit Local Flag");

				// test cases flag true
				HtmlReporter.tc_result=true; 
				try
				{
					
					int midrate=1;
					String currency=WebDr.getValue("ETE_FCY_Account_no").split("\\|")[0];
					String local_gl__number=WebDr.getValue("LCY_GL_no");
					String forex_gl__number=WebDr.getValue("FCY_GL_No");
					HashMap<String, ArrayList<String>> limits_map = new HashMap<String, ArrayList<String>>();
					HashMap<String, String> CreditRisk = new HashMap<String, String>();
					HashMap<String, Map<String,String>> store_matrix = new HashMap<String, Map<String,String>>();
					HashMap<String, Map<String,String>> store_forex = new HashMap<String, Map<String,String>>();

					ROATFunctions.refesh(false,"Teller");
					if (HtmlReporter.tc_result)
					{ 
						user_branch = ROATModules.fetch_user_branch();
						user_id=ROATFunctions.fetch_user_Id();
						user_name = ROATModules.fetchuser();
						Branch=ROATFunctions.fetch_user_branch_name();
						lcy_range=ROATModules.LCY_Payment_amount("1",1);
						fcy_range=ROATModules.FCY_Payment_amount(currency,"1",1);


						// Fetching forex rate 
						ROATFunctions.refesh(false, "Central Supervisor");
						WebDr.SetPageObjects("end_end");
						store_forex = ROATModules.set_forexrateSellPurchase(currency);
						
						//Fetching 

						// Fetching forex matrix 
						ROATFunctions.refesh(false, "Super User");
						if (HtmlReporter.tc_result)ROATModules.Forex_MatrixNavigation();
						store_matrix=ROATModules.fetch_Forex_teller_matrix(Branch,WebDr.getValue("default_branch"));

						
						//Fetching Authorization remote
						if (HtmlReporter.tc_result)
						{
							//CreditRisk=ROATModules.auto_remo_update(Branch);	
							if (HtmlReporter.tc_result)ROATModules.setup_navigateLTLimits();


							//******************************Breaches verification************************************
							Thread.sleep(5000);

							//  Limits set as 'Send for authorization' 
							if (HtmlReporter.tc_result)
							{ 
								//Limit breach------
								//System.out.println(store_forex);
								midrate=Integer.parseInt(store_forex.get("Cash Currency"+currency).get("Buy"));
								String action="Send for Authorization";
								if (HtmlReporter.tc_result)
								{
									limits_map=ROATModules.fetch_limits_ALL(user_branch, user_id+"-"+user_name,fcy_range,action);
								}

								ROATFunctions.refesh(false, "Teller");
								if (HtmlReporter.tc_result)ROATModules.Forex_Navigation();
								if (HtmlReporter.tc_result)ROATModules.Forex_Exchange_Cheque_Deposit_ete(limits_map,CreditRisk,store_matrix,fcy_range,action,midrate,depositCurrency,chequeCurrencyLimit,deposit_account,bankID,voucher,depositLocalFlag);
							}

							// test cases flag true
							HtmlReporter.tc_result=true; 

							// Transaction with Limits set as 'Decline'
							if (HtmlReporter.tc_result)
							{
								// Limit breach------
								
								midrate=Integer.parseInt(store_forex.get("Cash Currency"+currency).get("Buy"));
								ROATFunctions.refesh(false, "Super User");
								String action="Decline";
								if (HtmlReporter.tc_result)ROATModules.setup_navigateLTLimits();
								if (HtmlReporter.tc_result == true)
								{
									limits_map=ROATModules.fetch_limits_ALL(user_branch, user_id+"-"+user_name,fcy_range,action);
								}
								ROATFunctions.refesh(false, "Teller");
								if (HtmlReporter.tc_result)ROATModules.Forex_Navigation();
								if (HtmlReporter.tc_result)ROATModules.Forex_Exchange_Cheque_Deposit_ete(limits_map,CreditRisk,store_matrix,fcy_range,action,midrate,depositCurrency,chequeCurrencyLimit,deposit_account,bankID,voucher,depositLocalFlag);
								}
						}
					}
				}

				catch(Exception e)
				{
					e.getStackTrace();
					Reporter.ReportEvent("forex Exchange cheque Deposit Breaches Testcase ", " Testcase should be passed","forex Exchange cheque Deposit Breaches Testcase Failed", false);
				}
				
			}
			
			
			
			// Bills end to end lcy to lcy
			public static void end_to_end_bill_negotiable_lcy_lcy_ete() {
				
				if(HtmlReporter.tc_result)
				{
					try
					{
						int lcy_range=50;
						int fcy_range=100;
						int midrate=1;
						String currency=WebDr.getValue("ETE_FCY_Account_no").split("\\|")[0];
						String lcyTofcyflag  = WebDr.getValue("LCY TO FCY");
						String user_branch ="";
						String user_id="";
						String user_name = "";
						String Branch="";
						String local_gl__number=WebDr.getValue("LCY_GL_no");
						String forex_gl__number=WebDr.getValue("FCY_GL_No");
						HashMap<String, ArrayList<String>> limits_map = new HashMap<String, ArrayList<String>>();
						HashMap<String, String> exchangeAmounts = new HashMap<String, String>();
						HashMap<String, String> CreditRisk = new HashMap<String, String>();
						HashMap<String, Double> forex_fees=new HashMap<String, Double>();
						HashMap<String, Map<String,String>> store_matrix = new HashMap<String, Map<String,String>>();
						HashMap<String, Map<String,String>> store_forex = new HashMap<String, Map<String,String>>();
						int pre_trans_number=0;
						String lcyFlag = WebDr.getValue("Local Currency Flag");

						ROATFunctions.refesh(false,"Teller");
						if (HtmlReporter.tc_result)
						{ 
							user_branch = ROATModules.fetch_user_branch();
							user_id=ROATFunctions.fetch_user_Id();
							user_name = ROATModules.fetchuser();
							Branch=ROATFunctions.fetch_user_branch_name();
							lcy_range=ROATModules.LCY_Payment_amount("1",1);
							
							if(lcyTofcyflag.toUpperCase().endsWith("TRUE")){
								fcy_range=ROATModules.FCY_Payment_amount(currency,"1",1);
								// Fetching forex rate 
								ROATFunctions.refesh(false, "Central Supervisor");
								WebDr.SetPageObjects("end_end");
								store_forex = ROATModules.set_forexrateSellPurchase(currency);
							}



							// to get the transaction number before performing draft issue
							if(Boolean.valueOf(lcyFlag))
							{
								pre_trans_number =ROATModules.support_function_forexBills( false, "lcy", pre_trans_number,"Draft Issue",WebDr.getValue("LCY_Account"),true);
							}
							else
							{
								pre_trans_number =ROATModules.support_function_forexBills( true, "lcy", pre_trans_number,"Draft Issue",WebDr.getValue("LCY_Account"),true);
							}
							
							
							if (HtmlReporter.tc_result)
							{
								if (HtmlReporter.tc_result)
								{ 
									ROATFunctions.refesh(false, "Teller");
									HashMap<String, String> credit_account_details_col = new HashMap<String, String>();
									String credit_account = WebDr.getValue("FCY_GL_No").split("-")[1];
									String credit_account_brn = WebDr.getValue("FCY_GL_No").split("-")[0];
									credit_account_details_col = ROATModules.account_inquiry(credit_account, credit_account_brn);
									
									
									String currency_credit_account = credit_account_details_col.get("Currency").split(":")[1].trim().replace(",", "");
									
									if (HtmlReporter.tc_result)ROATModules.navigateBillsNegotiableAdd();
									if (HtmlReporter.tc_result){
										exchangeAmounts = ROATModules.bill_negotiable_lcy_fcy_end_to_end(store_forex,lcy_range,currency_credit_account);
									}
									String Auth_user_AL[] =Launcher.Auth_User_Login("Super User");
									ROATModules.Enter_Authorize_Credential("Internal_Transfer", "user", "pass",Auth_user_AL[0], Auth_user_AL[1],"","","","", true);
									WebDr.SetPageObjects("forexbillsnegotiableadd");
									if (HtmlReporter.tc_result)WebDr.waitTillElementIsDisplayed("btnAccept");
									if (HtmlReporter.tc_result)WebDr.click("X", "btnAccept", "Click on Accept button", true); 
									if (HtmlReporter.tc_result)WebDr.waitTillElementIsDisplayed("post");
									if (HtmlReporter.tc_result)WebDr.click("X", "post", "Click on Post button", true);
									if (HtmlReporter.tc_result)WebDr.waitTillElementIsDisplayed("done");
									if (HtmlReporter.tc_result)WebDr.clickX("X", "done", "Click on Done button", true);
									
									WebDr.Wait_Time(8000);
									
									HashMap<String, String> new_credit_account_details_col = new HashMap<String, String>();
									String new_credit_account = WebDr.getValue("FCY_GL_No").split("-")[1];
									String new_credit_account_brn = WebDr.getValue("FCY_GL_No").split("-")[0];
									new_credit_account_details_col = ROATModules.account_inquiry(new_credit_account, new_credit_account_brn);
									
									
									
									
									double oldValue = Double.parseDouble(credit_account_details_col.get("current_Actual Balance").split(":")[1].trim().replace(",", ""));
									double newValue = Double.parseDouble(new_credit_account_details_col.get("current_Actual Balance").split(":")[1].trim().replace(",", ""));
									if (HtmlReporter.tc_result)WebDr.Wait_Time(1000);

									if(oldValue<newValue){
										System.out.println("Bills Amount has been credited");
										Reporter.ReportEvent("Bills Amount credit", "Bills Amount has been credited","Bills Amount credit passed", true);

									}else{
										Reporter.ReportEvent("Bills Amount credit", "Bills Amount has been credited","Bills Amount credit failed", false);
									}

									// to verify the refernce number in Inquiry Bills tab
									String ReferenceNumber= exchangeAmounts.get("Reference");
									if (HtmlReporter.tc_result)ROATModules.InquiryRef(ReferenceNumber);

									// to get the transaction number before performing draft issue
									if(Boolean.valueOf(lcyFlag))
									{
										pre_trans_number =ROATModules.support_function_forexBills( false, "lcy", pre_trans_number,"Draft Issue",WebDr.getValue("LCY_Account"),true);
									}
									else
									{
										pre_trans_number =ROATModules.support_function_forexBills( true, "lcy", pre_trans_number,"Draft Issue",WebDr.getValue("LCY_Account"),true);
									}
									
								}

							}

						}
					}
					catch(Exception e)
					{
						Reporter.ReportEvent("Test Case execution is Failed", "end to end Deposit","end to end Deposit executition failed", false);
					}
				}
			
			}
			
			// bills end to end fcy to lcy
	public static void end_to_end_bill_negotiable_fcy_lcy_ete() {
				
				if(HtmlReporter.tc_result)
				{
					try
					{
						int lcy_range=50;
						int fcy_range=100;
						int midrate=1;
						
						String currency;
						String fcyTolcyflag  = WebDr.getValue("FCY TO LCY");						
						String user_branch ="";
						String user_id="";
						String user_name = "";
						String Branch="";
						String local_gl__number=WebDr.getValue("LCY_GL_no");
						String forex_gl__number=WebDr.getValue("FCY_GL_No");
						HashMap<String, ArrayList<String>> limits_map = new HashMap<String, ArrayList<String>>();
						HashMap<String, String> exchangeAmounts = new HashMap<String, String>();
						HashMap<String, String> CreditRisk = new HashMap<String, String>();
						HashMap<String, Double> forex_fees=new HashMap<String, Double>();
						HashMap<String, Map<String,String>> store_matrix = new HashMap<String, Map<String,String>>();
						HashMap<String, Map<String,String>> store_forex = new HashMap<String, Map<String,String>>();
						int pre_trans_number=0;
						String lcyFlag = WebDr.getValue("Local Currency Flag");
						if(fcyTolcyflag.toUpperCase().equals("TRUE")){
							currency=WebDr.getValue("ETE_LCY_Account_no").split("\\|")[0];
						}else{
							currency=WebDr.getValue("ETE_FCY_Account_no").split("\\|")[0];
						}

						ROATFunctions.refesh(false,"Teller");
						if (HtmlReporter.tc_result)
						{ 
							user_branch = ROATModules.fetch_user_branch();
							user_id=ROATFunctions.fetch_user_Id();
							user_name = ROATModules.fetchuser();
							Branch=ROATFunctions.fetch_user_branch_name();
							lcy_range=ROATModules.LCY_Payment_amount("1",1);
							fcy_range=ROATModules.FCY_Payment_amount(currency,"1",1);
							
							// To get the current login currency
							String PrimaryCurr=ROATFunctions.fetch_user_primay_currency();

							// Fetching forex rate 
							ROATFunctions.refesh(false, "Central Supervisor");
							WebDr.SetPageObjects("end_end");
							if(!PrimaryCurr.equalsIgnoreCase(currency))
							{
							store_forex = ROATModules.set_forexrateSellPurchase(currency);
							}
							// to get the transaction number before performing draft issue
							if(Boolean.valueOf(lcyFlag))
							{
								pre_trans_number =ROATModules.support_function_forexBills( false, "lcy", pre_trans_number,"Draft Issue",WebDr.getValue("LCY_Account"),true);
							}
							else
							{
								pre_trans_number =ROATModules.support_function_forexBills( true, "lcy", pre_trans_number,"Draft Issue",WebDr.getValue("LCY_Account"),true);
							}
							
							if (HtmlReporter.tc_result)
							{
								if (HtmlReporter.tc_result)
								{ 
									ROATFunctions.refesh(false, "Teller");
									HashMap<String, String> credit_account_details_col = new HashMap<String, String>();
									String credit_account = WebDr.getValue("FCY_GL_No").split("-")[1];
									String credit_account_brn = WebDr.getValue("FCY_GL_No").split("-")[0];
									credit_account_details_col = ROATModules.account_inquiry(credit_account, credit_account_brn);
									
									
									String currency_credit_account = credit_account_details_col.get("Currency").split(":")[1].trim().replace(",", "");
									
									if (HtmlReporter.tc_result)ROATModules.navigateBillsNegotiableAdd();
									if (HtmlReporter.tc_result){
										exchangeAmounts = ROATModules.bill_negotiable_fcy_lcy_end_to_end(store_forex,lcy_range,currency_credit_account);
									}
									String Auth_user_AL[] =Launcher.Auth_User_Login("Super User");
									ROATModules.Enter_Authorize_Credential("Internal_Transfer", "user", "pass",Auth_user_AL[0], Auth_user_AL[1],"","","","", true);
									WebDr.SetPageObjects("forexbillsnegotiableadd");
									if (HtmlReporter.tc_result)WebDr.waitTillElementIsDisplayed("btnAccept");
									if (HtmlReporter.tc_result)WebDr.click("X", "btnAccept", "Click on Accept button", true); 
									if (HtmlReporter.tc_result)WebDr.waitTillElementIsDisplayed("post");
									if (HtmlReporter.tc_result)WebDr.click("X", "post", "Click on Post button", true);
									if (HtmlReporter.tc_result)WebDr.waitTillElementIsDisplayed("done");
									if (HtmlReporter.tc_result)WebDr.clickX("X", "done", "Click on Done button", true);
									
									WebDr.Wait_Time(8000);
									
									HashMap<String, String> new_credit_account_details_col = new HashMap<String, String>();
									String new_credit_account = WebDr.getValue("FCY_GL_No").split("-")[1];
									String new_credit_account_brn = WebDr.getValue("FCY_GL_No").split("-")[0];
									new_credit_account_details_col = ROATModules.account_inquiry(new_credit_account, new_credit_account_brn);
									
									
									
									
									double oldValue = Double.parseDouble(credit_account_details_col.get("current_Actual Balance").split(":")[1].trim().replace(",", ""));
									double newValue = Double.parseDouble(new_credit_account_details_col.get("current_Actual Balance").split(":")[1].trim().replace(",", ""));
									if (HtmlReporter.tc_result)WebDr.Wait_Time(1000);

									if(oldValue<newValue){
										System.out.println("Bills Amount has been credited");
										Reporter.ReportEvent("Bills Amount credit", "Bills Amount has been credited","Bills Amount credit passed", true);

									}else{
										Reporter.ReportEvent("Bills Amount credit", "Bills Amount has been credited","Bills Amount credit failed", false);
									}
									
									
									// to verify the refernce number in Inquiry Bills tab
									String ReferenceNumber= exchangeAmounts.get("Reference");
									if (HtmlReporter.tc_result)ROATModules.InquiryRef(ReferenceNumber);
									

									// to get the transaction number after performing Bills
									if(Boolean.valueOf(lcyFlag))
									{
										pre_trans_number =ROATModules.support_function_forexBills( false, "lcy", pre_trans_number,"Draft Issue",WebDr.getValue("LCY_Account"),true);
									}
									else
									{
										pre_trans_number =ROATModules.support_function_forexBills( true, "lcy", pre_trans_number,"Draft Issue",WebDr.getValue("LCY_Account"),true);
									}
									
								}

							}

						}
					}
					catch(Exception e)
					{
						Reporter.ReportEvent("Test Case execution is Failed", "end to end Deposit","end to end Deposit executition failed", false);
					}
				}
			
			}

			
		//////////////////////////////End to End charge module////////////////////////////////////////////
	
				@SuppressWarnings("unchecked")
				public static void E2EChargesModule() throws Exception
				{				
					ROATModules.performChargeTariffSetUp();
					ROATProcesses.chargeModule("LOCAL", "STANDARD", "CW");
					ROATProcesses.chargeModule("FOREIGN", "STANDARD", "CW");
					
					ROATProcesses.chargeModule("LOCAL", "STANDARD", "IT");
					ROATProcesses.chargeModule("FOREIGN", "STANDARD", "IT");
					
					ROATProcesses.chargeModule("LOCAL", "MASSMARKET", "CW");
					ROATProcesses.chargeModule("FOREIGN", "MASSMARKET", "CW");
					
					ROATProcesses.chargeModule("LOCAL", "MASSMARKET", "IT");
					ROATProcesses.chargeModule("FOREIGN", "MASSMARKET", "IT");
					
					ROATProcesses.chargeModule("LOCAL", "ACCTTYPE", "CW");
					ROATProcesses.chargeModule("FOREIGN", "ACCTTYPE", "CW");
					
					ROATProcesses.chargeModule("LOCAL", "ACCTTYPE", "IT");
					ROATProcesses.chargeModule("FOREIGN", "ACCTTYPE", "IT");
				
				}

	
				public static void CAL() {
					// TODO Auto-generated method stub
					
					if(HtmlReporter.tc_result)
					{
						try
						{
							int lcy_range=50;
							int fcy_range=100;
							int midrate=1;
							String currency=WebDr.getValue("ETE_FCY_Account_no").split("\\|")[0];
							String user_branch ="";
							String user_id="";
							String user_name = "";
							String Branch="";
							String local_gl__number=WebDr.getValue("LCY_GL_no");
							String forex_gl__number=WebDr.getValue("FCY_GL_No");
							HashMap<String, ArrayList<String>> limits_map = new HashMap<String, ArrayList<String>>();
							HashMap<String, String> CreditRisk = new HashMap<String, String>();
							HashMap<String, Map<String,String>> store_matrix = new HashMap<String, Map<String,String>>();
							HashMap<String, Map<String,String>> store_forex = new HashMap<String, Map<String,String>>();
			
							ROATFunctions.refesh(false,"Teller");
							if (HtmlReporter.tc_result)
							{ 
								user_branch = ROATModules.fetch_user_branch();
								user_id=ROATFunctions.fetch_user_Id();
								user_name = ROATModules.fetchuser();
								Branch=ROATFunctions.fetch_user_branch_name();
								lcy_range=ROATModules.LCY_Payment_amount("1",1);
								fcy_range=ROATModules.FCY_Payment_amount(currency,"1",1);
			
			
								// Fetching forex rate 
								ROATFunctions.refesh(false, "Central Supervisor");
								WebDr.SetPageObjects("end_end");
								store_forex = ROATModules.set_forexrate(currency);
			
								// Fetching teller matrix 
								ROATFunctions.refesh(false, "Super User");
								if (HtmlReporter.tc_result)ROATModules.setup_TellerMatrixNavigation();
								store_matrix=ROATModules.fetch_teller_matrix(Branch,WebDr.getValue("default_branch"));
			
								//Fetching Authorization remote
								if (HtmlReporter.tc_result)
								{
									//CreditRisk=ROATModules.auto_remo_update(Branch);	
			
			
									//******************************Breaches verification************************************
			
			
									// LCY Transaction with Limits set as 'Send for authorization' 
									if (HtmlReporter.tc_result)
									{ 
										//LCY Limit breach------mid rate=1
										midrate=1;
										String action="Send for Authorization";
										if (HtmlReporter.tc_result)
										{
											if (HtmlReporter.tc_result)ROATModules.setup_navigateLTLimits();
											limits_map=ROATModules.fetch_limits_ALL(user_branch, user_id+"-"+user_name,lcy_range,action);
										}
			
			
										ROATFunctions.refesh(false, "Teller");
										if (HtmlReporter.tc_result)ROATProcesses.navigateChargeApplication();
										if (HtmlReporter.tc_result)ROATProcesses.tellerCA(limits_map,CreditRisk,store_matrix,lcy_range,action,midrate);
			
									}
			
								}
			
							}
						}
						catch(Exception e)
						{
							Reporter.ReportEvent("Test Case execution is Failed", "end to end Deposit","end to end Deposit executition failed", false);
						}
					}
				}
			

/// *******************************************Bills Breaches*************************************************///////////////////
			
			
				public static void end_to_end_bill_negotiable_breaches_lcy() {
					if(HtmlReporter.tc_result)
					{
						try
						{
							int lcy_range=50;
							int fcy_range=100;
							int midrate=1;
							String currency=WebDr.getValue("ETE_FCY_Account_no").split("\\|")[0];
							String user_branch ="";
							String user_id="";
							String user_name = "";
							String Branch="";
							String local_gl__number=WebDr.getValue("LCY_GL_no");
							String forex_gl__number=WebDr.getValue("FCY_GL_No");
							HashMap<String, ArrayList<String>> limits_map = new HashMap<String, ArrayList<String>>();
							HashMap<String, String> CreditRisk = new HashMap<String, String>();
							HashMap<String, Map<String,String>> store_matrix = new HashMap<String, Map<String,String>>();
							HashMap<String, Map<String,String>> store_forex = new HashMap<String, Map<String,String>>();

							ROATFunctions.refesh(false,"Teller");
							if (HtmlReporter.tc_result)
							{ 
								user_branch = ROATModules.fetch_user_branch();
								user_id=ROATFunctions.fetch_user_Id();
								user_name = ROATModules.fetchuser();
								Branch=ROATFunctions.fetch_user_branch_name();
								lcy_range=ROATModules.LCY_Payment_amount("1",1);
								fcy_range=ROATModules.FCY_Payment_amount(currency,"1",1);


								// Fetching forex rate 
								ROATFunctions.refesh(false, "Central Supervisor");
								WebDr.SetPageObjects("end_end");
								store_forex = ROATModules.set_forexrate(currency);

								// Fetching teller matrix 
								ROATFunctions.refesh(false, "Super User");
								if (HtmlReporter.tc_result)ROATModules.Forex_MatrixNavigation();
								store_matrix=ROATModules.fetch_Forex_teller_matrix(Branch,WebDr.getValue("default_branch"));
								if (HtmlReporter.tc_result)
								{
									//******************************Breaches verification************************************

									// LCY Transaction with Limits set as 'Send for authorization' 
									if (HtmlReporter.tc_result)
									{ 
										//LCY Limit breach------mid rate=1
										midrate=1;
										String action="Send for Authorization";
										if (HtmlReporter.tc_result)
										{
											if (HtmlReporter.tc_result)ROATModules.setup_navigateLTLimits();
											limits_map=ROATModules.fetch_limits_ALL(user_branch, user_id+"-"+user_name,lcy_range,action);
										}

										ROATFunctions.refesh(false, "Teller");
										if (HtmlReporter.tc_result)ROATModules.navigateBillsNegotiableAdd();;
										if (HtmlReporter.tc_result)ROATModules.bill_negotiable_ete(limits_map,CreditRisk,store_matrix,lcy_range,action,midrate);

									}

								}

							}
						}
						catch(Exception e)
						{
							Reporter.ReportEvent("Test Case execution is Failed", "end to end Deposit","end to end Deposit executition failed", false);
						}
					}
				}
				public static void end_to_end_bill_negotiable_breaches_fcy() {
					if(HtmlReporter.tc_result)
					{
						try
						{
							int lcy_range=50;
							int fcy_range=100;
							int midrate=1;
							String currency=WebDr.getValue("ETE_FCY_Account_no").split("\\|")[0];
							String user_branch ="";
							String user_id="";
							String user_name = "";
							String Branch="";
							String local_gl__number=WebDr.getValue("LCY_GL_no");
							String forex_gl__number=WebDr.getValue("FCY_GL_No");
							HashMap<String, ArrayList<String>> limits_map = new HashMap<String, ArrayList<String>>();
							HashMap<String, String> CreditRisk = new HashMap<String, String>();
							HashMap<String, Map<String,String>> store_matrix = new HashMap<String, Map<String,String>>();
							HashMap<String, Map<String,String>> store_forex = new HashMap<String, Map<String,String>>();

							ROATFunctions.refesh(false,"Teller");
							if (HtmlReporter.tc_result)
							{ 
								user_branch = ROATModules.fetch_user_branch();
								user_id=ROATFunctions.fetch_user_Id();
								user_name = ROATModules.fetchuser();
								Branch=ROATFunctions.fetch_user_branch_name();
								lcy_range=ROATModules.LCY_Payment_amount("1",1);
								fcy_range=ROATModules.FCY_Payment_amount(currency,"1",1);


								// Fetching forex rate 
								ROATFunctions.refesh(false, "Central Supervisor");
								WebDr.SetPageObjects("end_end");
								store_forex = ROATModules.set_forexrate(currency);

								// Fetching teller matrix 
								ROATFunctions.refesh(false, "Super User");
								if (HtmlReporter.tc_result)ROATModules.Forex_MatrixNavigation();
								store_matrix=ROATModules.fetch_Forex_teller_matrix(Branch,WebDr.getValue("default_branch"));
								if (HtmlReporter.tc_result)
								{
									//******************************Breaches verification************************************

									// LCY Transaction with Limits set as 'Send for authorization' 
									if (HtmlReporter.tc_result)
									{ 
										//LCY Limit breach------mid rate=1
										midrate=1;
										String action="Send for Authorization";
										if (HtmlReporter.tc_result)
										{
											if (HtmlReporter.tc_result)ROATModules.setup_navigateLTLimits();
											limits_map=ROATModules.fetch_limits_ALL(user_branch, user_id+"-"+user_name,lcy_range,action);
										}

										ROATFunctions.refesh(false, "Teller");
										if (HtmlReporter.tc_result)ROATModules.navigateBillsNegotiableAdd();;
										if (HtmlReporter.tc_result)ROATModules.bill_negotiable_ete(limits_map,CreditRisk,store_matrix,lcy_range,action,midrate);
									}

								}

							}
						}
						catch(Exception e)
						{
							Reporter.ReportEvent("Test Case execution is Failed", "end to end Deposit","end to end Deposit executition failed", false);
						}
					}
				}
				public static void end_to_end_bill_negotiable_lcy_ete() {
					
					if(HtmlReporter.tc_result)
					{
						try
						{
							int lcy_range=50;
							int fcy_range=100;
							int midrate=1;
							String currency=WebDr.getValue("ETE_FCY_Account_no").split("\\|")[0];
							String user_branch ="";
							String user_id="";
							String user_name = "";
							String Branch="";
							String local_gl__number=WebDr.getValue("LCY_GL_no");
							String forex_gl__number=WebDr.getValue("FCY_GL_No");
							HashMap<String, ArrayList<String>> limits_map = new HashMap<String, ArrayList<String>>();
							HashMap<String, String> CreditRisk = new HashMap<String, String>();
							HashMap<String, Map<String,String>> store_matrix = new HashMap<String, Map<String,String>>();
							HashMap<String, Map<String,String>> store_forex = new HashMap<String, Map<String,String>>();

							ROATFunctions.refesh(false,"Teller");
							if (HtmlReporter.tc_result)
							{ 
								user_branch = ROATModules.fetch_user_branch();
								user_id=ROATFunctions.fetch_user_Id();
								user_name = ROATModules.fetchuser();
								Branch=ROATFunctions.fetch_user_branch_name();
								lcy_range=ROATModules.LCY_Payment_amount("1",1);
								fcy_range=ROATModules.FCY_Payment_amount(currency,"1",1);


								// Fetching forex rate 
								ROATFunctions.refesh(false, "Central Supervisor");
								WebDr.SetPageObjects("end_end");
								store_forex = ROATModules.set_forexrate(currency);

								// Fetching teller matrix 
								ROATFunctions.refesh(false, "Super User");
								if (HtmlReporter.tc_result)ROATModules.Forex_MatrixNavigation();
								store_matrix=ROATModules.fetch_Forex_teller_matrix(Branch,WebDr.getValue("default_branch"));
								if (HtmlReporter.tc_result)
								{
									//******************************Breaches verification************************************

									// LCY Transaction with Limits set as 'Send for authorization' 
									if (HtmlReporter.tc_result)
									{ 
										//LCY Limit breach------mid rate=1
										midrate=1;
										String action="Send for Authorization";
										if (HtmlReporter.tc_result)
										{
											if (HtmlReporter.tc_result)ROATModules.setup_navigateLTLimits();
											limits_map=ROATModules.fetch_limits_ALL(user_branch, user_id+"-"+user_name,lcy_range,action);
										}
										ROATFunctions.refesh(false, "Teller");
										HashMap<String, String> credit_account_details_col = new HashMap<String, String>();
										String credit_account = WebDr.getValue("AccountNumber1").split("\\|")[1].split("-")[1];
										String credit_account_brn = WebDr.getValue("AccountNumber1").split("\\|")[1].split("-")[0];
										credit_account_details_col = ROATModules.account_inquiry(credit_account, credit_account_brn);
										
										HashMap<String, String> debit_account_details_col = new HashMap<String, String>();
										String debit_account = WebDr.getValue("LCY_GL_no").split("\\|")[1].split("-")[1];
										String debit_account_brn = WebDr.getValue("LCY_GL_no").split("\\|")[1].split("-")[0];
										debit_account_details_col = ROATModules.account_inquiry(debit_account, debit_account_brn);
										
										HashMap<String, String> debit_gl1_account_details_col = new HashMap<String, String>();
										String debit_gl1_account = WebDr.getValue("Commission_GL1_No").split("\\|")[1].split("-")[1];
										String debit_gl1_account_brn = WebDr.getValue("Commission_GL1_No").split("\\|")[1].split("-")[0];
										debit_gl1_account_details_col = ROATModules.account_inquiry(debit_gl1_account, debit_gl1_account_brn);
										
										HashMap<String, String> debit_gl2_account_details_col = new HashMap<String, String>();
										String debit_gl2_account = WebDr.getValue("Commission_GL2_No").split("\\|")[1].split("-")[1];
										String debit_gl2_account_brn = WebDr.getValue("Commission_GL2_No").split("\\|")[1].split("-")[0];
										debit_gl2_account_details_col = ROATModules.account_inquiry(debit_gl2_account, debit_gl2_account_brn);
										
										
										String currency_credit_account = credit_account_details_col.get("Currency").split(":")[1].trim().replace(",", "");
										
										if (HtmlReporter.tc_result)ROATModules.navigateBillsNegotiableAdd();;
										if (HtmlReporter.tc_result)ROATModules.bill_negotiable_lcy_fcy_ete(limits_map,CreditRisk,store_matrix,lcy_range,action,midrate, currency_credit_account);
										String Auth_user_AL[] =Launcher.Auth_User_Login("Super User");
										ROATModules.Enter_Authorize_Credential("Internal_Transfer", "user", "pass",Auth_user_AL[0], Auth_user_AL[1],"","","","", true);
										if (HtmlReporter.tc_result)WebDr.waitTillElementIsDisplayed("Accept");
										if (HtmlReporter.tc_result)WebDr.click("X", "Accept", "Click on Accept button", true); 
										if (HtmlReporter.tc_result)WebDr.waitTillElementIsDisplayed("post");
										if (HtmlReporter.tc_result)WebDr.click("X", "post", "Click on Post button", true);
										if (HtmlReporter.tc_result)WebDr.waitTillElementIsDisplayed("done");
										if (HtmlReporter.tc_result)WebDr.clickX("X", "done", "Click on Done button", true);
										
										
										
										HashMap<String, String> new_credit_account_details_col = new HashMap<String, String>();
										String new_credit_account = WebDr.getValue("AccountNumber1").split("\\|")[1].split("-")[1];
										String new_credit_account_brn = WebDr.getValue("AccountNumber1").split("\\|")[1].split("-")[0];
										new_credit_account_details_col = ROATModules.account_inquiry(new_credit_account, new_credit_account_brn);
										
										HashMap<String, String> new_debit_account_details_col = new HashMap<String, String>();
										String new_debit_account = WebDr.getValue("LCY_GL_no").split("\\|")[1].split("-")[1];
										String new_debit_account_brn = WebDr.getValue("LCY_GL_no").split("\\|")[1].split("-")[0];
										new_debit_account_details_col = ROATModules.account_inquiry(new_debit_account, new_debit_account_brn);
										
										HashMap<String, String> new_debit_gl1_account_details_col = new HashMap<String, String>();
										String new_debit_gl1_account = WebDr.getValue("Commission_GL1_No").split("\\|")[1].split("-")[1];
										String new_debit_gl1_account_brn = WebDr.getValue("Commission_GL1_No").split("\\|")[1].split("-")[0];
										new_debit_gl1_account_details_col = ROATModules.account_inquiry(new_debit_gl1_account, new_debit_gl1_account_brn);
										
										HashMap<String, String> new_debit_gl2_account_details_col = new HashMap<String, String>();
										String new_debit_gl2_account = WebDr.getValue("Commission_GL2_No").split("\\|")[1].split("-")[1];
										String new_debit_gl2_account_brn = WebDr.getValue("Commission_GL2_No").split("\\|")[1].split("-")[0];
										new_debit_gl2_account_details_col = ROATModules.account_inquiry(new_debit_gl2_account, new_debit_gl2_account_brn);
										
										
										
										double oldValue = Double.parseDouble(credit_account_details_col.get("current_Actual Balance"+new_credit_account).split(":")[1].trim().replace(",", ""));
										double newValue = Double.parseDouble(new_credit_account_details_col.get("current_Actual Balance"+new_credit_account).split(":")[1].trim().replace(",", ""));
										if (HtmlReporter.tc_result)WebDr.Wait_Time(1000);

										if(oldValue>newValue && oldValue-newValue==Double.parseDouble(credit_account_details_col.get("Paying Amount").replace(",", "").trim())){
											System.out.println("pay currency has been debited");
											Reporter.ReportEvent("pay currency debit", "pay currency has been debited","pay currency has been debited", true);

										}else{
											Reporter.ReportEvent("pay currency debit", "pay currency has been debited","pay currency debit has been failed ", false);
										}

										
										
										
										
										//************************************support functions****************************
										//delete transaction from 'support function'--- local currency 
										//HtmlReporter.tc_result=true;
										//midrate=1;
										//ROATModules.end_to_end_bills_negotiable_supportFunction(store_forex,forex_fees,lcy_range,midrate,WebDr.getValue("Credit_Account_FCY"));

										
										
									}

								}

							}
						}
						catch(Exception e)
						{
							Reporter.ReportEvent("Test Case execution is Failed", "end to end Deposit","end to end Deposit executition failed", false);
						}
					}
				
				}
				
				
				public static void end_to_end_bill_negotiable_fcy_ete() {

					if(HtmlReporter.tc_result)
					{
						try
						{
							int lcy_range=50;
							int fcy_range=100;
							int midrate=1;
							String currency=WebDr.getValue("ETE_FCY_Account_no").split("\\|")[0];
							String user_branch ="";
							String user_id="";
							String user_name = "";
							String Branch="";
							String local_gl__number=WebDr.getValue("LCY_GL_no");
							String forex_gl__number=WebDr.getValue("FCY_GL_No");
							HashMap<String, ArrayList<String>> limits_map = new HashMap<String, ArrayList<String>>();
							HashMap<String, String> CreditRisk = new HashMap<String, String>();
							HashMap<String, Map<String,String>> store_matrix = new HashMap<String, Map<String,String>>();
							HashMap<String, Map<String,String>> store_forex = new HashMap<String, Map<String,String>>();

							ROATFunctions.refesh(false,"Teller");
							if (HtmlReporter.tc_result)
							{ 
								user_branch = ROATModules.fetch_user_branch();
								user_id=ROATFunctions.fetch_user_Id();
								user_name = ROATModules.fetchuser();
								Branch=ROATFunctions.fetch_user_branch_name();
								lcy_range=ROATModules.LCY_Payment_amount("1",1);
								fcy_range=ROATModules.FCY_Payment_amount(currency,"1",1);


								// Fetching forex rate 
								ROATFunctions.refesh(false, "Central Supervisor");
								WebDr.SetPageObjects("end_end");
								store_forex = ROATModules.set_forexrate(currency);
								
								ROATFunctions.refesh(false, "Teller");
								HashMap<String, String> credit_account_details_col = new HashMap<String, String>();
								String credit_account = WebDr.getValue("AccountNumber1").split("\\|")[1].split("-")[1];
								String credit_account_brn = WebDr.getValue("AccountNumber1").split("\\|")[1].split("-")[0];
								credit_account_details_col = ROATModules.account_inquiry(credit_account, credit_account_brn);
								
								String currency_credit_account = credit_account_details_col.get("Currency").split(":")[1].trim().replace(",", "");

								// Fetching teller matrix 
								ROATFunctions.refesh(false, "Super User");
								if (HtmlReporter.tc_result)ROATModules.Forex_MatrixNavigation();
								store_matrix=ROATModules.fetch_Forex_teller_matrix(Branch,WebDr.getValue("default_branch"));
								if (HtmlReporter.tc_result)
								{
									//******************************Breaches verification************************************

									// LCY Transaction with Limits set as 'Send for authorization' 
									if (HtmlReporter.tc_result)
									{ 
										//LCY Limit breach------mid rate=1
										midrate=1;
										String action="Send for Authorization";
										if (HtmlReporter.tc_result)
										{
											if (HtmlReporter.tc_result)ROATModules.setup_navigateLTLimits();
											limits_map=ROATModules.fetch_limits_ALL(user_branch, user_id+"-"+user_name,lcy_range,action);
										}

										ROATFunctions.refesh(false, "Teller");
										if (HtmlReporter.tc_result)ROATModules.navigateBillsNegotiableAdd();;
										if (HtmlReporter.tc_result)ROATModules.bill_negotiable_lcy_fcy_ete(limits_map,CreditRisk,store_matrix,lcy_range,action,midrate,currency_credit_account);

										
										
										//************************************support functions****************************
										
										//delete transaction from 'support function'--- foreign currency 
										HtmlReporter.tc_result=true;
										midrate=Integer.parseInt(store_forex.get("Cash Currency").get("Buy"));
										//ROATModules.end_to_end_forextrans_supportFunction(store_forex,forex_fees,fcy_range,midrate,WebDr.getValue("Credit_Account_LCY"));

									}

								}

							}
						}
						catch(Exception e)
						{
							Reporter.ReportEvent("Test Case execution is Failed", "end to end Deposit","end to end Deposit executition failed", false);
						}
					}
				
				}
				public static void end_to_end_teller_posting_breaches_lcy() 
				{


					try
					{
						int lcy_range=50;
						int fcy_range=100;
						int midrate=1;
						String currency=WebDr.getValue("ETE_FCY_Account_no").split("\\|")[0];
						String user_branch ="";
						String user_id="";
						String user_name = "";
						String Branch="";
						String local_gl__number=WebDr.getValue("LCY_GL_no");
						String forex_gl__number=WebDr.getValue("FCY_GL_No");
						HashMap<String, ArrayList<String>> limits_map = new HashMap<String, ArrayList<String>>();
						HashMap<String, String> CreditRisk = new HashMap<String, String>();
						HashMap<String, Map<String,String>> store_matrix = new HashMap<String, Map<String,String>>();
						HashMap<String, Map<String,String>> store_forex = new HashMap<String, Map<String,String>>();

						ROATFunctions.refesh(false,"Teller");
						if (HtmlReporter.tc_result)
						{ 
							user_branch = ROATModules.fetch_user_branch();
							//user_id=ROATFunctions.fetch_user_Id();
							user_name = ROATModules.fetchuser();
							Branch=ROATFunctions.fetch_user_branch_name();
							lcy_range=ROATModules.LCY_Payment_amount("1",1);
							fcy_range=ROATModules.FCY_Payment_amount(currency,"1",1);


							// Fetching forex rate 
							ROATFunctions.refesh(false, "Central Supervisor");
							WebDr.SetPageObjects("end_end");
							store_forex = ROATModules.set_forexrate(currency);

							// Fetching teller matrix 
							ROATFunctions.refesh(false, "Super User");
							if (HtmlReporter.tc_result)ROATModules.setup_TellerMatrixNavigation();
							store_matrix=ROATModules.fetch_teller_matrix(Branch,WebDr.getValue("default_branch"));

							//Fetching Authorization remote
							if (HtmlReporter.tc_result)
							{
								//						CreditRisk=ROATModules.auto_remo_update(Branch);	
								if (HtmlReporter.tc_result)ROATModules.setup_navigateLTLimits();


								//******************************Breaches verification************************************

								// LCY Transaction with Limits set as 'Send for authorization' 
								if (HtmlReporter.tc_result)
								{ 
									//LCY Limit breach------mid rate=1
									midrate=1;
									String action="Send for Authorization";
									if (HtmlReporter.tc_result)
									{
										limits_map=ROATModules.fetch_limits_ALL(user_branch, user_id+"-"+user_name,lcy_range,action);
									}


									ROATFunctions.refesh(false, "Teller");
									if(HtmlReporter.tc_result)ROATModules.navigate_teller_posting();
									WebDr.Wait_Time(2000);
									if (HtmlReporter.tc_result)ROATModules.Teller_Posting_ete(limits_map,CreditRisk,store_matrix,lcy_range,action,midrate);
								}
							}
						}
					}

					catch(Exception ex)
					{
						ex.printStackTrace();
						Reporter.ReportEvent("Test Case execution is Failed", "end to end Deposit","end to end Deposit executition failed", false);
					}

				
					
					
				}
				public static void end_to_end_teller_posting_breaches_lcy_decline()
				{



					try
					{
						int lcy_range=50;
						int fcy_range=100;
						int midrate=1;
						String currency=WebDr.getValue("ETE_FCY_Account_no").split("\\|")[0];
						String user_branch ="";
						String user_id="";
						String user_name = "";
						String Branch="";
						String local_gl__number=WebDr.getValue("LCY_GL_no");
						String forex_gl__number=WebDr.getValue("FCY_GL_No");
						HashMap<String, ArrayList<String>> limits_map = new HashMap<String, ArrayList<String>>();
						HashMap<String, String> CreditRisk = new HashMap<String, String>();
						HashMap<String, Map<String,String>> store_matrix = new HashMap<String, Map<String,String>>();
						HashMap<String, Map<String,String>> store_forex = new HashMap<String, Map<String,String>>();

						ROATFunctions.refesh(false,"Teller");
						if (HtmlReporter.tc_result)
						{ 
							user_branch = ROATModules.fetch_user_branch();
							user_id=ROATFunctions.fetch_user_Id();
							user_name = ROATModules.fetchuser();
							Branch=ROATFunctions.fetch_user_branch_name();
							lcy_range=ROATModules.LCY_Payment_amount("1",1);
							fcy_range=ROATModules.FCY_Payment_amount(currency,"1",1);


							// Fetching forex rate 
							ROATFunctions.refesh(false, "Central Supervisor");
							WebDr.SetPageObjects("end_end");
							store_forex = ROATModules.set_forexrate(currency);

							// Fetching teller matrix 
							ROATFunctions.refesh(false, "Super User");
							if (HtmlReporter.tc_result)ROATModules.setup_TellerMatrixNavigation();
							store_matrix=ROATModules.fetch_teller_matrix(Branch,WebDr.getValue("default_branch"));

							//Fetching Authorization remote
							if (HtmlReporter.tc_result)
							{
								//						CreditRisk=ROATModules.auto_remo_update(Branch);	
								if (HtmlReporter.tc_result)ROATModules.setup_navigateLTLimits();


								//******************************Breaches verification************************************

								// LCY Transaction with Limits set as 'Decline' 
								if (HtmlReporter.tc_result)
								{ 
									//LCY Limit breach------mid rate=1
									midrate=1;
									String action="Decline";
									if (HtmlReporter.tc_result)
									{
										limits_map=ROATModules.fetch_limits_ALL(user_branch, user_id+"-"+user_name,lcy_range,action);
									}


									ROATFunctions.refesh(false, "Teller");
									if(HtmlReporter.tc_result)ROATModules.navigate_teller_posting();
									WebDr.Wait_Time(2000);
									if (HtmlReporter.tc_result)ROATModules.Teller_Posting_ete(limits_map,CreditRisk,store_matrix,lcy_range,action,midrate);
								}
							}
						}
					}

					catch(Exception ex)
					{
						ex.printStackTrace();
						Reporter.ReportEvent("Test Case execution is Failed", "end to end Deposit","end to end Deposit executition failed", false);
					}

				
					
					
					
					
				}
				
					public static void end_to_end_teller_posting_breaches_fcy()
					{

						if(HtmlReporter.tc_result)
						{
							try
							{
								int lcy_range=50;
								int fcy_range=100;
								int midrate=1;
								String currency=WebDr.getValue("ETE_FCY_Account_no").split("\\|")[0];
								String user_branch ="";
								String user_id="";
								String user_name = "";
								String Branch="";
								String local_gl__number=WebDr.getValue("LCY_GL_no");
								String forex_gl__number=WebDr.getValue("FCY_GL_No");
								HashMap<String, ArrayList<String>> limits_map = new HashMap<String, ArrayList<String>>();
								HashMap<String, String> CreditRisk = new HashMap<String, String>();
								HashMap<String, Map<String,String>> store_matrix = new HashMap<String, Map<String,String>>();
								HashMap<String, Map<String,String>> store_forex = new HashMap<String, Map<String,String>>();

								ROATFunctions.refesh(false,"Teller");
								if (HtmlReporter.tc_result)
								{ 
									user_branch = ROATModules.fetch_user_branch();
									user_id=ROATFunctions.fetch_user_Id();
									user_name = ROATModules.fetchuser();
									Branch=ROATFunctions.fetch_user_branch_name();
									lcy_range=ROATModules.LCY_Payment_amount("1",1);
									fcy_range=ROATModules.FCY_Payment_amount(currency,"1",1);


									// Fetching forex rate 
									ROATFunctions.refesh(false, "Central Supervisor");
									WebDr.SetPageObjects("end_end");
									store_forex = ROATModules.set_forexrate(currency);

									// Fetching teller matrix 
									ROATFunctions.refesh(false, "Super User");
									if (HtmlReporter.tc_result)ROATModules.setup_TellerMatrixNavigation();
									store_matrix=ROATModules.fetch_teller_matrix(Branch,WebDr.getValue("default_branch"));

									//Fetching Authorization remote
									if (HtmlReporter.tc_result)
									{
										//CreditRisk=ROATModules.auto_remo_update(Branch);	


										//******************************Breaches verification************************************



										// test cases flag true
										HtmlReporter.tc_result=true; 

										// FCY Transaction with Limits set as Send for authorization
										if (HtmlReporter.tc_result)
										{
											midrate=Integer.parseInt(store_forex.get("Mid Rate").get("Mid Rate"));
											ROATFunctions.refesh(false, "Super User");
											String action="Send for Authorization";
											if (HtmlReporter.tc_result)ROATModules.setup_navigateLTLimits();
											if (HtmlReporter.tc_result == true)
											{
												limits_map=ROATModules.fetch_limits_ALL(user_branch, user_id+"-"+user_name,fcy_range*midrate,action);
											}

											//FCY Limit breach------mid rate

											ROATFunctions.refesh(false, "Teller");
											if(HtmlReporter.tc_result)ROATModules.navigate_teller_posting();							
											if (HtmlReporter.tc_result)ROATModules.Teller_Posting_ete(limits_map,CreditRisk,store_matrix,lcy_range,action,midrate);
										}

									}

								}
							}
							catch(Exception e)
							{
								Reporter.ReportEvent("Test Case execution is Failed", "end to end Deposit","end to end Deposit executition failed", false);
							}
						}
					
						
					}
					
				
				public static void end_to_end_teller_posting_support_functions_lcy()
				{

					if(HtmlReporter.tc_result)
					{
						try
						{
							int lcy_range=50;
							int fcy_range=100;
							int midrate=1;
							String currency=WebDr.getValue("ETE_FCY_Account_no").split("\\|")[0];
							String user_branch ="";
							String user_id="";
							String user_name = "";
							String Branch="";
							String local_gl__number=WebDr.getValue("LCY_GL_no");
							String forex_gl__number=WebDr.getValue("FCY_GL_No");
							HashMap<String, ArrayList<String>> limits_map = new HashMap<String, ArrayList<String>>();
							HashMap<String, String> CreditRisk = new HashMap<String, String>();
							HashMap<String, Map<String,String>> store_matrix = new HashMap<String, Map<String,String>>();
							HashMap<String, Map<String,String>> store_forex = new HashMap<String, Map<String,String>>();

							ROATFunctions.refesh(false,"Teller");
							if (HtmlReporter.tc_result)
							{ 
								user_branch = ROATModules.fetch_user_branch();
								user_id=ROATFunctions.fetch_user_Id();
								user_name = ROATModules.fetchuser();
								Branch=ROATFunctions.fetch_user_branch_name();
								lcy_range=ROATModules.LCY_Payment_amount("1",1);
								fcy_range=ROATModules.FCY_Payment_amount(currency,"1",1);


								// Fetching forex rate 
								ROATFunctions.refesh(false, "Central Supervisor");
								WebDr.SetPageObjects("end_end");
								store_forex = ROATModules.set_forexrate(currency);

								// Fetching teller matrix 
								ROATFunctions.refesh(false, "Super User");
								if (HtmlReporter.tc_result)ROATModules.setup_TellerMatrixNavigation();
								store_matrix=ROATModules.fetch_teller_matrix(Branch,WebDr.getValue("default_branch"));

								//Fetching Authorization remote
								if (HtmlReporter.tc_result)
								{
									//CreditRisk=ROATModules.auto_remo_update(Branch);	


									//	***********************************************end to end******************************
									//end to end---LCY---Transaction--without Breach 
									HtmlReporter.tc_result=true;
									ROATFunctions.refesh(false,"Super User");
									String	action="Send for Authorization";
									midrate=Integer.parseInt(store_forex.get("Mid Rate").get("Mid Rate"));
									if (HtmlReporter.tc_result)ROATModules.setup_navigateLTLimits();
									limits_map=ROATModules.fetch_limits_ALL(user_branch, user_id+"-"+user_name,lcy_range*midrate*2,action);
									
									
	                               	//************************************support functions****************************


									//delete transaction from 'support function'--- local currency
									HtmlReporter.tc_result=true;
									midrate=1;
									ROATModules.end_to_end_teller_posting_support_fuction(lcy_range,midrate,local_gl__number);

								}

							}
						}
						catch(Exception e)
						{
							Reporter.ReportEvent("Test Case execution is Failed", "end to end Deposit","end to end Deposit executition failed", false);
						}
					}
				
					
					
				}
				
					public static void end_to_end_teller_posting_support_functions_fcy()
					{

						if(HtmlReporter.tc_result)
						{
							try
							{
								int lcy_range=50;
								int fcy_range=100;
								int midrate=1;
								String currency=WebDr.getValue("ETE_FCY_Account_no").split("\\|")[0];
								String user_branch ="";
								String user_id="";
								String user_name = "";
								String Branch="";
								String local_gl__number=WebDr.getValue("LCY_GL_no");
								String forex_gl__number=WebDr.getValue("FCY_GL_No");
								HashMap<String, ArrayList<String>> limits_map = new HashMap<String, ArrayList<String>>();
								HashMap<String, String> CreditRisk = new HashMap<String, String>();
								HashMap<String, Map<String,String>> store_matrix = new HashMap<String, Map<String,String>>();
								HashMap<String, Map<String,String>> store_forex = new HashMap<String, Map<String,String>>();

								ROATFunctions.refesh(false,"Teller");
								if (HtmlReporter.tc_result)
								{ 
									user_branch = ROATModules.fetch_user_branch();
									user_id=ROATFunctions.fetch_user_Id();
									user_name = ROATModules.fetchuser();
									Branch=ROATFunctions.fetch_user_branch_name();
									lcy_range=ROATModules.LCY_Payment_amount("1",1);
									fcy_range=ROATModules.FCY_Payment_amount(currency,"1",1);


									// Fetching forex rate 
									ROATFunctions.refesh(false, "Central Supervisor");
									WebDr.SetPageObjects("end_end");
									store_forex = ROATModules.set_forexrate(currency);

									// Fetching teller matrix 
									ROATFunctions.refesh(false, "Super User");
									if (HtmlReporter.tc_result)ROATModules.setup_TellerMatrixNavigation();
									store_matrix=ROATModules.fetch_teller_matrix(Branch,WebDr.getValue("default_branch"));

									//Fetching Authorization remote
									if (HtmlReporter.tc_result)
									{
										//CreditRisk=ROATModules.auto_remo_update(Branch);	


										//	***********************************************end to end******************************
										//end to end---LCY---Transaction--without Breach 
										HtmlReporter.tc_result=true;
										ROATFunctions.refesh(false,"Super User");
										String	action="Send for Authorization";
										midrate=Integer.parseInt(store_forex.get("Mid Rate").get("Mid Rate"));
										if (HtmlReporter.tc_result)ROATModules.setup_navigateLTLimits();
										limits_map=ROATModules.fetch_limits_ALL(user_branch, user_id+"-"+user_name,fcy_range*midrate*2,action);
										
										
		                               	//************************************support functions****************************


										//delete transaction from 'support function'--- local currency
										HtmlReporter.tc_result=true;
										midrate=1;
										ROATModules.end_to_end_teller_posting_support_fuction(fcy_range,midrate,forex_gl__number);

									}

								}
							}
							catch(Exception e)
							{
								Reporter.ReportEvent("Test Case execution is Failed", "end to end Deposit","end to end Deposit executition failed", false);
							}
						}
					}
						
						
					
						
						
				
					public static void E_E_Internal_Transfer_end_to_end()
					{


					if(HtmlReporter.tc_result)
					{
						try
						{
							int lcy_range=50;
							int fcy_range=100;
							int midrate=1;
							String currency=WebDr.getValue("ETE_FCY_Account_no").split("\\|")[0];
							String user_branch ="";
							String user_id="";
							String user_name = "";
							String Branch="";
							String local_gl__number=WebDr.getValue("LCY_GL_no");
							String forex_gl__number=WebDr.getValue("FCY_GL_No");
							HashMap<String, ArrayList<String>> limits_map = new HashMap<String, ArrayList<String>>();
							HashMap<String, String> CreditRisk = new HashMap<String, String>();
							HashMap<String, Map<String,String>> store_matrix = new HashMap<String, Map<String,String>>();
							HashMap<String, Map<String,String>> store_forex = new HashMap<String, Map<String,String>>();

							ROATFunctions.refesh(false,"Teller");
							if (HtmlReporter.tc_result)
							{ 
								user_branch = ROATModules.fetch_user_branch();
								user_id=ROATFunctions.fetch_user_Id();
								user_name = ROATModules.fetchuser();
								Branch=ROATFunctions.fetch_user_branch_name();
								lcy_range=ROATModules.LCY_Payment_amount("1",1);
								fcy_range=ROATModules.FCY_Payment_amount(currency,"1",1);


								// Fetching forex rate 
								ROATFunctions.refesh(false, "Central Supervisor");
								WebDr.SetPageObjects("end_end");
								store_forex = ROATModules.set_forexrate(currency);

								// Fetching teller matrix 
								ROATFunctions.refesh(false, "Super User");
								if (HtmlReporter.tc_result)ROATModules.setup_TellerMatrixNavigation();
								store_matrix=ROATModules.fetch_teller_matrix(Branch,WebDr.getValue("default_branch"));

								//Fetching Authorization remote
								if (HtmlReporter.tc_result)
								{
									//CreditRisk=ROATModules.auto_remo_update(Branch);	


									//	***********************************************end to end******************************
									ROATFunctions.refesh(false,"Super User");
									String action="Send for Authorization";
									midrate=Integer.parseInt(store_forex.get("Mid Rate").get("Mid Rate"))+1;
									if (HtmlReporter.tc_result)ROATModules.setup_navigateLTLimits();
									limits_map=ROATModules.fetch_limits_ALL(user_branch, user_id+"-"+user_name,fcy_range*midrate*2,action);
									
									
									//end to end---LCY---Transaction--without Breach 
									midrate=1;
									if (HtmlReporter.tc_result)ROATModules.setup_navigateLTLimits();
									ROATModules.end_to_end_internal_trans(lcy_range,midrate,WebDr.getValue("Account2"));



									//************************************support functions****************************

									HtmlReporter.tc_result=true;
									//delete transaction from 'support function'--- local currency
									midrate=1;
									ROATModules.end_to_end_internal_transfer_support_fuction(lcy_range,midrate,WebDr.getValue("Account2"));

									
									//delete transaction from 'support function'--- foreign currency 
									HtmlReporter.tc_result=true;
									midrate=Integer.parseInt(store_forex.get("Mid Rate").get("Mid Rate"));
									ROATModules.end_to_end_internal_transfer_support_fuction(fcy_range,midrate,WebDr.getValue("FCYAccount2"));



								}

							}
						}
						catch(Exception e)
						{
							Reporter.ReportEvent("Test Case execution is Failed", "end to end Deposit","end to end Deposit executition failed", false);
						}
					}
					}
					public static void end_to_end_batch_posting_breaches_lcy()
					{



						try
						{
							int lcy_range=50;
							int fcy_range=100;
							int midrate=1;
							String currency=WebDr.getValue("ETE_FCY_Account_no").split("\\|")[0];
							String user_branch ="";
							String user_id="";
							String user_name = "";
							String Branch="";
							String local_gl__number=WebDr.getValue("LCY_GL_no");
							String forex_gl__number=WebDr.getValue("FCY_GL_No");
							HashMap<String, ArrayList<String>> limits_map = new HashMap<String, ArrayList<String>>();
							HashMap<String, String> CreditRisk = new HashMap<String, String>();
							HashMap<String, Map<String,String>> store_matrix = new HashMap<String, Map<String,String>>();
							HashMap<String, Map<String,String>> store_forex = new HashMap<String, Map<String,String>>();

							ROATFunctions.refesh(false,"Teller");
							if (HtmlReporter.tc_result)
							{ 
								user_branch = ROATModules.fetch_user_branch();
								user_id=ROATFunctions.fetch_user_Id();
								user_name = ROATModules.fetchuser();
								Branch=ROATFunctions.fetch_user_branch_name();
								lcy_range=ROATModules.LCY_Payment_amount("1",1);
								fcy_range=ROATModules.FCY_Payment_amount(currency,"1",1);


								// Fetching forex rate 
								ROATFunctions.refesh(false, "Central Supervisor");
								WebDr.SetPageObjects("end_end");
								store_forex = ROATModules.set_forexrate(currency);

								// Fetching teller matrix 
								ROATFunctions.refesh(false, "Super User");
								if (HtmlReporter.tc_result)ROATModules.setup_TellerMatrixNavigation();
								store_matrix=ROATModules.fetch_teller_matrix(Branch,WebDr.getValue("default_branch"));

								//Fetching Authorization remote
								if (HtmlReporter.tc_result)
								{
									//						CreditRisk=ROATModules.auto_remo_update(Branch);	
									if (HtmlReporter.tc_result)ROATModules.setup_navigateLTLimits();


									//******************************Breaches verification************************************

									// LCY Transaction with Limits set as 'Send for authorization' 
									if (HtmlReporter.tc_result)
									{ 
										//LCY Limit breach------mid rate=1
										midrate=1;
										String action="Send for Authorization";
										if (HtmlReporter.tc_result)
										{
											limits_map=ROATModules.fetch_limits_ALL(user_branch, user_id+"-"+user_name,lcy_range,action);
										}


										ROATFunctions.refesh(false, "Back office clerk");
										if(HtmlReporter.tc_result)ROATModules.navigate_batch_posting();							
										if (HtmlReporter.tc_result)ROATModules.Batch_Posting_ete(limits_map,CreditRisk,store_matrix,lcy_range,action,midrate);
									}
								}
							}
						}

						catch(Exception ex)
						{
							ex.printStackTrace();
							Reporter.ReportEvent("Test Case execution is Failed", "end to end Deposit","end to end Deposit executition failed", false);
						}

					
						
						
					
						
					}
					public static void end_to_end_batch_posting_support_functions_lcy()
					{
						if(HtmlReporter.tc_result)
						{
							try
							{
								int lcy_range=50;
								int fcy_range=100;
								int midrate=1;
								String currency=WebDr.getValue("ETE_FCY_Account_no").split("\\|")[0];
								String user_branch ="";
								String user_id="";
								String user_name = "";
								String Branch="";
								String local_gl__number=WebDr.getValue("LCY_GL_no");
								String forex_gl__number=WebDr.getValue("FCY_GL_No");
								HashMap<String, ArrayList<String>> limits_map = new HashMap<String, ArrayList<String>>();
								HashMap<String, String> CreditRisk = new HashMap<String, String>();
								HashMap<String, Map<String,String>> store_matrix = new HashMap<String, Map<String,String>>();
								HashMap<String, Map<String,String>> store_forex = new HashMap<String, Map<String,String>>();

								ROATFunctions.refesh(false,"Teller");
								if (HtmlReporter.tc_result)
								{ 
									user_branch = ROATModules.fetch_user_branch();
									user_id=ROATFunctions.fetch_user_Id();
									user_name = ROATModules.fetchuser();
									Branch=ROATFunctions.fetch_user_branch_name();
									lcy_range=ROATModules.LCY_Payment_amount("1",1);
									fcy_range=ROATModules.FCY_Payment_amount(currency,"1",1);


									// Fetching forex rate 
									ROATFunctions.refesh(false, "Central Supervisor");
									WebDr.SetPageObjects("end_end");
									store_forex = ROATModules.set_forexrate(currency);

									// Fetching teller matrix 
									ROATFunctions.refesh(false, "Super User");
									if (HtmlReporter.tc_result)ROATModules.setup_TellerMatrixNavigation();
									store_matrix=ROATModules.fetch_teller_matrix(Branch,WebDr.getValue("default_branch"));

									//Fetching Authorization remote
									if (HtmlReporter.tc_result)
									{
										//CreditRisk=ROATModules.auto_remo_update(Branch);	


										//	***********************************************end to end******************************
										//end to end---LCY---Transaction--without Breach 
										HtmlReporter.tc_result=true;
										ROATFunctions.refesh(false,"Super User");
										String	action="Send for Authorization";
										midrate=Integer.parseInt(store_forex.get("Mid Rate").get("Mid Rate"));
										if (HtmlReporter.tc_result)ROATModules.setup_navigateLTLimits();
										limits_map=ROATModules.fetch_limits_ALL(user_branch, user_id+"-"+user_name,lcy_range*midrate*2,action);
										
										
		                               	//************************************support functions****************************


										//delete transaction from 'support function'--- local currency
										HtmlReporter.tc_result=true;
										midrate=1;
										ROATModules.end_to_end_batch_posting_support_fuction(lcy_range,midrate,local_gl__number);

									}

								}
							}
							catch(Exception e)
							{
								Reporter.ReportEvent("Test Case execution is Failed", "end to end Deposit","end to end Deposit executition failed", false);
							}
						}
					}
					public static void end_to_end_batch_posting_breaches_fcy()
					{




						try
						{
							int lcy_range=50;
							int fcy_range=100;
							int midrate=1;
							String currency=WebDr.getValue("ETE_FCY_Account_no").split("\\|")[0];
							String user_branch ="";
							String user_id="";
							String user_name = "";
							String Branch="";
							String local_gl__number=WebDr.getValue("LCY_GL_no");
							String forex_gl__number=WebDr.getValue("FCY_GL_No");
							HashMap<String, ArrayList<String>> limits_map = new HashMap<String, ArrayList<String>>();
							HashMap<String, String> CreditRisk = new HashMap<String, String>();
							HashMap<String, Map<String,String>> store_matrix = new HashMap<String, Map<String,String>>();
							HashMap<String, Map<String,String>> store_forex = new HashMap<String, Map<String,String>>();

							ROATFunctions.refesh(false,"Teller");
							if (HtmlReporter.tc_result)
							{ 
								user_branch = ROATModules.fetch_user_branch();
								user_id=ROATFunctions.fetch_user_Id();
								user_name = ROATModules.fetchuser();
								Branch=ROATFunctions.fetch_user_branch_name();
								lcy_range=ROATModules.LCY_Payment_amount("1",1);
								fcy_range=ROATModules.FCY_Payment_amount(currency,"1",1);


								// Fetching forex rate 
								ROATFunctions.refesh(false, "Central Supervisor");
								WebDr.SetPageObjects("end_end");
								store_forex = ROATModules.set_forexrate(currency);

								// Fetching teller matrix 
								ROATFunctions.refesh(false, "Super User");
								if (HtmlReporter.tc_result)ROATModules.setup_TellerMatrixNavigation();
								store_matrix=ROATModules.fetch_teller_matrix(Branch,WebDr.getValue("default_branch"));

								//Fetching Authorization remote
								if (HtmlReporter.tc_result)
								{
									//						CreditRisk=ROATModules.auto_remo_update(Branch);	
									if (HtmlReporter.tc_result)ROATModules.setup_navigateLTLimits();


									//******************************Breaches verification************************************

									// LCY Transaction with Limits set as 'Send for authorization' 
									if (HtmlReporter.tc_result)
									{ 
										//LCY Limit breach------mid rate=1
										midrate=1;
										String action="Send for Authorization";
										if (HtmlReporter.tc_result)
										{
											limits_map=ROATModules.fetch_limits_ALL(user_branch, user_id+"-"+user_name,fcy_range,action);
										}


										ROATFunctions.refesh(false, "Back office clerk");
										if(HtmlReporter.tc_result)ROATModules.navigate_batch_posting();							
										if (HtmlReporter.tc_result)ROATModules.Batch_Posting_ete(limits_map,CreditRisk,store_matrix,fcy_range,action,midrate);
									}
								}
							}
						}

						catch(Exception ex)
						{
							ex.printStackTrace();
							Reporter.ReportEvent("Test Case execution is Failed", "end to end Deposit","end to end Deposit executition failed", false);
						}

					
						
						
					
						
					
						
						
					}
					public static void end_to_end_batch_posting_support_functions_fcy()
					{

						if(HtmlReporter.tc_result)
						{
							try
							{
								int lcy_range=50;
								int fcy_range=100;
								int midrate=1;
								String currency=WebDr.getValue("ETE_FCY_Account_no").split("\\|")[0];
								String user_branch ="";
								String user_id="";
								String user_name = "";
								String Branch="";
								String local_gl__number=WebDr.getValue("LCY_GL_no");
								String forex_gl__number=WebDr.getValue("FCY_GL_No");
								HashMap<String, ArrayList<String>> limits_map = new HashMap<String, ArrayList<String>>();
								HashMap<String, String> CreditRisk = new HashMap<String, String>();
								HashMap<String, Map<String,String>> store_matrix = new HashMap<String, Map<String,String>>();
								HashMap<String, Map<String,String>> store_forex = new HashMap<String, Map<String,String>>();

								ROATFunctions.refesh(false,"Teller");
								if (HtmlReporter.tc_result)
								{ 
									user_branch = ROATModules.fetch_user_branch();
									user_id=ROATFunctions.fetch_user_Id();
									user_name = ROATModules.fetchuser();
									Branch=ROATFunctions.fetch_user_branch_name();
									lcy_range=ROATModules.LCY_Payment_amount("1",1);
									fcy_range=ROATModules.FCY_Payment_amount(currency,"1",1);


									// Fetching forex rate 
									ROATFunctions.refesh(false, "Central Supervisor");
									WebDr.SetPageObjects("end_end");
									store_forex = ROATModules.set_forexrate(currency);

									// Fetching teller matrix 
									ROATFunctions.refesh(false, "Super User");
									if (HtmlReporter.tc_result)ROATModules.setup_TellerMatrixNavigation();
									store_matrix=ROATModules.fetch_teller_matrix(Branch,WebDr.getValue("default_branch"));

									//Fetching Authorization remote
									if (HtmlReporter.tc_result)
									{
										//CreditRisk=ROATModules.auto_remo_update(Branch);	


										//	***********************************************end to end******************************
										//end to end---LCY---Transaction--without Breach 
										HtmlReporter.tc_result=true;
										ROATFunctions.refesh(false,"Super User");
										String	action="Send for Authorization";
										midrate=Integer.parseInt(store_forex.get("Mid Rate").get("Mid Rate"));
										if (HtmlReporter.tc_result)ROATModules.setup_navigateLTLimits();
										limits_map=ROATModules.fetch_limits_ALL(user_branch, user_id+"-"+user_name,fcy_range*midrate*2,action);
										
										
		                               	//************************************support functions****************************


										//delete transaction from 'support function'--- local currency
										HtmlReporter.tc_result=true;
										midrate=1;
										ROATModules.end_to_end_batch_posting_support_fuction(fcy_range,midrate,forex_gl__number);

									}

								}
							}
							catch(Exception e)
							{
								Reporter.ReportEvent("Test Case execution is Failed", "end to end Deposit","end to end Deposit executition failed", false);
							}
						}
					
						
					}
					
				
					/**
					 *  Charges with Remote Auth 
					 */
					public static void end_to_end_Deposit_Remote()
					{

						try
						{
							int lcy_range=50;
							int fcy_range=100;
							int midrate=1;
							String currency=WebDr.getValue("ETE_FCY_Account_no").split("\\|")[0];
							String user_branch ="";
							String user_id="";
							String user_name = "";
							String Branch="";
							String local_gl__number=WebDr.getValue("LCY_GL_no");
							String forex_gl__number=WebDr.getValue("FCY_GL_No");
							HashMap<String, ArrayList<String>> limits_map = new HashMap<String, ArrayList<String>>();
							HashMap<String, String> CreditRisk = new HashMap<String, String>();
							HashMap<String, Map<String,String>> store_matrix = new HashMap<String, Map<String,String>>();
							HashMap<String, Map<String,String>> store_forex = new HashMap<String, Map<String,String>>();

							ROATFunctions.refesh(false,"Teller");
							if (HtmlReporter.tc_result)
							{ 
								user_branch = ROATModules.fetch_user_branch();
								user_id=ROATFunctions.fetch_user_Id();
								user_name = ROATModules.fetchuser();
								Branch=ROATFunctions.fetch_user_branch_name();
								lcy_range=ROATModules.LCY_Payment_amount("1",1);
								fcy_range=ROATModules.FCY_Payment_amount(currency,"1",1);


								// Fetching forex rate 
								ROATFunctions.refesh(false, "Central Supervisor");
								WebDr.SetPageObjects("end_end");
								store_forex = ROATModules.set_forexrate(currency);

								// Fetching teller matrix 
								ROATFunctions.refesh(false, "Super User");
								if (HtmlReporter.tc_result)ROATModules.setup_TellerMatrixNavigation();
								store_matrix=ROATModules.fetch_teller_matrix(Branch,WebDr.getValue("default_branch"));

								//Fetching Authorization remote
								if (HtmlReporter.tc_result)
								{
									//CreditRisk=ROATModules.auto_remo_update(Branch);	


									//******************************Breaches verification************************************


									// LCY Transaction with Limits set as 'Send for authorization' 
									if (HtmlReporter.tc_result)
									{ 
										//LCY Limit breach------mid rate=1
										midrate=1;
										String action="Send for Authorization";
										if (HtmlReporter.tc_result)
										{
											if (HtmlReporter.tc_result)ROATModules.setup_navigateLTLimits();
											limits_map=ROATModules.fetch_limits_ALL(user_branch, user_id+"-"+user_name,lcy_range,action);
										}

										ROATFunctions.refesh(false, "Teller");
										if (HtmlReporter.tc_result)ROATModules.authFlagWrite("True");
										if (HtmlReporter.tc_result)ROATModules.Teller_navigateCashDeposit();
										if (HtmlReporter.tc_result)ROATModules.Teller_cash_deposit_ete_remote(limits_map,CreditRisk,store_matrix,lcy_range,action,midrate);
										

									}

								}

							}
						}
						catch(Exception e)
						{
							Reporter.ReportEvent("Test Case execution is Failed", "end to end Deposit","end to end Deposit executition failed", false);
						}				
					} 
				public static void end_to_end_withdrawal_chq_scan()
				{


					try
					{
						int lcy_range=50;
						int fcy_range=100;
						int midrate=1;
						String currency=WebDr.getValue("ETE_FCY_Account_no").split("\\|")[0];
						String user_branch ="";
						String user_id="";
						String user_name = "";
						String Branch="";
						String local_gl__number=WebDr.getValue("LCY_GL_no");
						String forex_gl__number=WebDr.getValue("FCY_GL_No");
						HashMap<String, ArrayList<String>> limits_map = new HashMap<String, ArrayList<String>>();
						HashMap<String, String> CreditRisk = new HashMap<String, String>();
						HashMap<String, Map<String,String>> store_matrix = new HashMap<String, Map<String,String>>();
						HashMap<String, Map<String,String>> store_forex = new HashMap<String, Map<String,String>>();

						ROATFunctions.refesh(false,"Teller");
						if (HtmlReporter.tc_result)
						{ 
							user_branch = ROATModules.fetch_user_branch();
							user_id=ROATFunctions.fetch_user_Id();
							user_name = ROATModules.fetchuser();
							Branch=ROATFunctions.fetch_user_branch_name();
							lcy_range=ROATModules.LCY_Payment_amount("1",1);
							fcy_range=ROATModules.FCY_Payment_amount(currency,"1",1);


							// Fetching forex rate 
//							ROATFunctions.refesh(false, "Central Supervisor");
//							WebDr.SetPageObjects("end_end");
//							store_forex = ROATModules.set_forexrate(currency);
//
//							// Fetching teller matrix 
//							ROATFunctions.refesh(false, "Super User");
//							if (HtmlReporter.tc_result)ROATModules.setup_TellerMatrixNavigation();
//							store_matrix=ROATModules.fetch_teller_matrix(Branch,WebDr.getValue("default_branch"));

							//Fetching Authorization remote
							if (HtmlReporter.tc_result)
							{
								//						CreditRisk=ROATModules.auto_remo_update(Branch);	


								//	***********************************************end to end******************************
								//end to end---LCY---Transaction--without Breach 
//								ROATFunctions.refesh(false,"Super User");
//								String action="Send for Authorization";
//								midrate=Integer.parseInt(store_forex.get("Mid Rate").get("Mid Rate"));
//								if (HtmlReporter.tc_result)ROATModules.setup_navigateLTLimits();
//								limits_map=ROATModules.fetch_limits_ALL(user_branch, user_id+"-"+user_name,lcy_range*midrate*2,action);

								if(HtmlReporter.tc_result)
								{
									midrate=1;
									ROATModules.end_to_end_withdrawal_chq(lcy_range,midrate,local_gl__number);



									//************************************support functions****************************


									//delete transaction from 'support function'--- local currency

									/*HtmlReporter.tc_result=true; 
									midrate=1;
									ROATModules.end_to_end_withdrawal_support_functions(lcy_range,midrate,local_gl__number);*/

								}

							}

						}
					}

					catch(Exception ex)
					{
						ex.printStackTrace();
						Reporter.ReportEvent("Test Case execution is Failed", "end to end Deposit","end to end Deposit executition failed", false);
					}
				} 

				public static void end_to_end_Remote_Auth(){
					try {
						WebDr.SetPageObjects("Remote");						
						HtmlReporter.tc_result=true;
						if(ROATModules.authFlagRead().equalsIgnoreCase("True")){							
							System.out.println("Auth Flag Status: Auth Flag is True");
							//SIT LOGIN Function
							
							if((WebDr.isExists_Display("UserIDLabel"))>0){			//username, Login, UserIDLabel
								ROATFunctions.refesh_authorizer(true, "Authorizer");
							}
							ROATModules.authApproval();
							Thread.sleep(5000);				
						}
						else{
							System.out.println("Auth Flag Status: Auth Flag is False");
							Thread.sleep(4000);
							if((WebDr.isExists("logout"))>0){
								ROATModules.logout();	
							}
							Reporter.ReportEvent("Remote Authorization: ", "Flag status should be True", "Flag Status is False", true);
						}
						if(ROATModules.authFlagRead().equalsIgnoreCase("Quit")){
							Driver.driver.quit();
						}
						else{						
							end_to_end_Remote_Auth();
						}
//						ROATModules.authApprovalFlagCheck();
					} catch (Exception e) {
						// TODO Auto-generated catch block
						e.printStackTrace();
					}
				}
}//body


