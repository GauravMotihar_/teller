package roatResuableModule;

import java.util.List;
import java.util.Map;

import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;

import testCases.Driver;
import utility.HtmlReporter;
import utility.WebDr;

//import utility.JIRAFactory;
//import utility.Launcher;

public class SmokeROATNTBFLows 
{	
	public static boolean flag;
	public static void AI_AccountInquiry() throws Exception
	{
		
		if (HtmlReporter.tc_result==true)SmokeROATModules.AI_navigateAccountInquiry();
		if (HtmlReporter.tc_result==true)SmokeROATModules.AI_searchAccountNumber();
		if (HtmlReporter.tc_result==true)SmokeROATModules.AI_validations();
		}
	//*********************************CUSTOMER SEARCH*******************************
	public static void CS_CustomerSearch() throws Exception {
		
		if (HtmlReporter.tc_result==true)SmokeROATModules.CS_navigateCustomerSearch();
		if (HtmlReporter.tc_result==true)SmokeROATModules.CS_customerDeatilSearch();
		
		if (HtmlReporter.tc_result==true)SmokeROATModules.CS_clearSearch();
	
	}
	//*****************************Customer Inquiry*************************************
		public static void CI_CustomerInquiry() throws Exception
		{
			
			if (HtmlReporter.tc_result==true)SmokeROATModules.CI_navigateCustomerInquiry();
			if (HtmlReporter.tc_result==true)SmokeROATModules.CI_customerSearch();
			if (HtmlReporter.tc_result==true)SmokeROATModules.CI_customerExpand();
			//if (HtmlReporter.tc_result==true)SmokeROATModules.CI_CustomerCollapse();
			//if (HtmlReporter.tc_result==true)SmokeROATModules.CI_accountAccordianVerification();
			}
		
	//******************************Account Search********************************
		public static void AS_AccountSearch() throws Exception
		{
		
			if (HtmlReporter.tc_result==true)SmokeROATModules.AS_NavigateAccountSearch();
			if (HtmlReporter.tc_result==true)SmokeROATModules.AS_PerformAccountSearch();
		}	
		
		
		//****************************************Currency**********************************************************

				public static void LT_Currency() throws Exception{
					
					if (HtmlReporter.tc_result==true)SmokeROATModules.setup_navigateCurrency();
					if (HtmlReporter.tc_result==true)SmokeROATModules.setup_currencyVerification();
					if (HtmlReporter.tc_result==true)SmokeROATModules.setup_currencyView();
					
				}
				
//******************************Cash Drawer Cash Summary*****************************************
				
				public static void cash_DrawerCash_Summary() throws Exception
				{
					
					if (HtmlReporter.tc_result==true)SmokeROATModules.CD_navigateCash_Summary();				
					if (HtmlReporter.tc_result==true)SmokeROATModules.CD_Cash_SummaryView();
					
				}


				public static void LT_Fees_AddNewFunctionality() throws Exception{
					
					
					if (HtmlReporter.tc_result==true) SmokeROATModules.setup_navigateFeesExchange();
					if (HtmlReporter.tc_result==true) SmokeROATModules.totalRecords();
					
					
				}
				
				//*****************************LCY Payment Transfer*************************************



				public static void LCY_Payment() throws Exception

				{
				WebDr.SetPageObjects("LCYPayment");
				
				if (HtmlReporter.tc_result==true)SmokeROATModules.menu_navigation("MasterPage","tab_CashDrawer");
				WebDr.SetPageObjects("MasterPage");
				if (HtmlReporter.tc_result==true)WebDr.waitTillElementIsDisplayed("tab_LCY");
				if (HtmlReporter.tc_result==true)SmokeROATModules.menu_navigation1("MasterPage","tab_LCY");
				WebDr.SetPageObjects("MasterPage");
				if (HtmlReporter.tc_result==true)WebDr.waitTillElementIsDisplayed("tab_LCY_payment");
				if (HtmlReporter.tc_result==true)SmokeROATModules.menu_navigation1("MasterPage","tab_LCY_payment");
				if (HtmlReporter.tc_result==true)SmokeROATModules.menu_navigation1("MasterPage","tab_LCY");
				if (HtmlReporter.tc_result==true)SmokeROATModules.menu_navigation("MasterPage","tab_CashDrawer");
				WebDr.SetPageObjects("LCYPayment");

				if (HtmlReporter.tc_result==true)SmokeROATModules.Enter_payment_Amount("LCYPayment","txtAmounts");
				if (HtmlReporter.tc_result==true)SmokeROATModules.error_Message_verification("Mismatch",WebDr.getValue("ErrorMsg_Mismatch"));
				if (HtmlReporter.tc_result==true)SmokeROATModules.enter_note_coin_Count("LCYPayment","tablenotes","txtnots");
				if (HtmlReporter.tc_result==true)SmokeROATModules.enter_note_coin_Count("LCYPayment","tableCoins","txtcoins");

				//if (HtmlReporter.tc_result==true)SmokeROATModules.enter_Soiled_decoy_value();

				if (HtmlReporter.tc_result==true)SmokeROATModules.fn_verify_total_cash_payment_amount("LCYPayment",WebDr.getValue("Enter_Amount"));
				if (HtmlReporter.tc_result==true)SmokeROATModules.Enter_Authorize_Credential("LCYPayment","txt_AuthorizerUser","txt_AuthorizerPass",WebDr.getValue("Username"),WebDr.getValue("Password"),true);
				if (HtmlReporter.tc_result==true)SmokeROATModules.error_Message_verification("Authorize",WebDr.getValue("VerifyPostMessage"));

				

				}

				public static void TM_TellerMatrix() throws Exception{
					
					if (HtmlReporter.tc_result==true)SmokeROATModules.setup_TellerMatrixNavigation();
					if (HtmlReporter.tc_result==true)SmokeROATModules.setup_TellerMatrixVerification();
					}
				
			
				
				public static void TM_TellerMatrixUpdate() throws Exception{
					if (HtmlReporter.tc_result==true)SmokeROATModules.setup_TellerMatrixNavigation();
					if (HtmlReporter.tc_result==true)SmokeROATModules.setup_TellerMatrixUpdate();
				}

				public static void Teller_Withdrawal() throws Exception
				{

					
					if (HtmlReporter.tc_result==true) SmokeROATModules.setup_TellerWithdrawaltNavigation();
					if (HtmlReporter.tc_result==true) SmokeROATModules.enterAccountNo("TellerWithdrawal","Enter_AccountNo");
					if (HtmlReporter.tc_result==true) SmokeROATModules.balance("TellerWithdrawal","Enter_Amount","Narration","WithdrawalName");
					if (HtmlReporter.tc_result==true) SmokeROATModules.enter_note_coin_Count("TellerWithdrawal","tablenotes","txtnots");
					if (HtmlReporter.tc_result==true) SmokeROATModules.enter_note_coin_Count("TellerWithdrawal","tableCoins","txtcoins");
					if (HtmlReporter.tc_result==true) SmokeROATModules.agentDetails("TellerWithdrawal", "id", "name", "phone");
					if (HtmlReporter.tc_result==true) SmokeROATModules.clickNextOrPost();
					if (HtmlReporter.tc_result==true) SmokeROATModules.clickAccept();
					if (HtmlReporter.tc_result==true) SmokeROATModules.clickNextOrPost();
					if (HtmlReporter.tc_result==true) SmokeROATModules.successReceipt_Message_verification();
				}
				


				public static void LT_LimitsVerificationViewUpdate() throws Exception{
					
					if (HtmlReporter.tc_result==true) SmokeROATModules.setup_navigateLTLimits();
					if (HtmlReporter.tc_result==true) SmokeROATModules.totalRecords1();
					if (HtmlReporter.tc_result==true) SmokeROATModules.setup_limitsVerification();
				
				}
				

				


}//class body close
				

