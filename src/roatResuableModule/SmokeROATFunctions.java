
package roatResuableModule;



import java.text.NumberFormat;
import java.util.HashMap;
import java.util.Map;
import java.util.List;
import java.util.Locale;

import org.apache.poi.util.SystemOutLogger;
import org.openqa.selenium.By;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.Keys;
import org.openqa.selenium.WebElement;



import testCases.Driver;
import utility.HtmlReporter;
import utility.Reporter;
import utility.WebDr;


 
public class SmokeROATFunctions 
{        
	//*************************************************************************************
	//*************************************************************************************
	public static Map<String, String> roatEnv = new HashMap<String, String>();
	
	
	public static boolean searchAccountInList(String accountNumber) throws Exception
	{
		
		WebElement elmNav=WebDr.getElement("nav_NextPage");
		int found=0, i=0;
		while(i==0)
		{	
			try
			{
				if(Driver.driver.findElement(By.xpath("//md-cell[text()=' "+ accountNumber +" ']/../md-cell[6]/md-icon/*/img")).isDisplayed())
				{
					HtmlReporter.WriteStep("Selecting Account Number", "Account No : " + accountNumber, "Selected Account No. "+ accountNumber +" in list", true);
					i=1;
					found=1;					
				}
				
			}
			catch(Exception e)
			{
				if(elmNav.getAttribute("ng-reflect-disabled").equals("true"))
				{
					i=1;
				}
				else
				{
					elmNav.click();
					JavascriptExecutor executor= (JavascriptExecutor)Driver.driver;
					executor.executeScript("arguments[0].click();", elmNav);
					
				}
				continue;
			}
			
		}
		if(found==1)return true;
		else HtmlReporter.WriteStep("X Selecting Account Number", "Account No : " + accountNumber, "Account No. " + accountNumber +" Not found in list", false);return false; 
		
	}
	//      login//
	public static void logintoROATeller() throws InterruptedException
	{
		
		try

	    {

	           WebDr.SetPageObjects("MasterPage");
	           if (HtmlReporter.tc_result==true) WebDr.click("X", "username", "Clicking on username text", true);
	           if (HtmlReporter.tc_result==true)WebDr.setText("X", "username", "G01204834", "Entering data in Username field", true);
	           if (HtmlReporter.tc_result==true)WebDr.setText("X", "Password", "123", "Entering data in password field", true);
	           if (HtmlReporter.tc_result==true) WebDr.click("X", "Login", "Clicking on login button", true);
	           if (HtmlReporter.tc_result==true) WebDr.waitTillElementIsDisplayed("dropdown_role");
	           if (HtmlReporter.tc_result==true) WebDr.select("X", "dropdown_role", "VisibleText","SuperAdmin", "Select role", true);
	           if (HtmlReporter.tc_result==true) WebDr.click("X", "Login", "Clicking on login button", true);
	           if (HtmlReporter.tc_result==true) WebDr.waitTillElementIsDisplayed("tab_Search");

	    }catch(Exception e)

	    {

	           Reporter.ReportEvent("login error", "login button should click", "login button object not found", false);
	           
	           
	    }
		
		
	}
	//*************************************************************************************************************
	//*************Converting Runtime String obtained using getText() to Integer***********************************	
public static int Convert(String elementName)
{
	try
	{
		String text=null;
		String desc=utility.WebDr.page_Objects.get(elementName);
		String [] a= desc.split("\\|");
		switch(a[0])
		{
			case "ID":  
				text = Driver.driver.findElement(By.id(a[1])).getText();
				return Integer.parseInt(text);
			case "CLASSNAME":  
				text =  Driver.driver.findElement(By.className(a[1])).getText();
				return Integer.parseInt(text);
			case "LINKTEXT": 
				text =  Driver.driver.findElement(By.linkText(a[1])).getText();
				return Integer.parseInt(text);
			case "NAME": 
				text =  Driver.driver.findElement(By.name(a[1])).getText();
				return Integer.parseInt(text);
			case "XPATH": 
				text =  Driver.driver.findElement(By.xpath(a[1])).getText();
				return Integer.parseInt(text);
			default: System.out.println("Function getElement cannot return object for " + elementName);break;
		}
	}
	catch(Exception e){System.out.println("Exeption in WebDr.getElement - "+e);return 0;}
	

	return 0;	
}


	//*************************************************************************************************************
	//********************Authorizing the Action*******************************************************************

public static int Authorization(String Auth_Username,String Password, boolean isMandatory)
{
	
	WebDr.isDefect=false;
	try
	{
		if(WebDr.getValue(Auth_Username)!=null)
		{
			WebElement elmn;
			
				elmn=WebDr.getElementObject("ID|inputauthousername");
			
			if(elmn.isDisplayed())
			{
				elmn.clear();
				Thread.sleep(500);
				elmn.sendKeys(Auth_Username);
				WebDr.isDefect=false;
				Reporter.ReportEvent("User Authorization", "Enter Text", "Entered: Username", true );
			}
			else
			{
				WebDr.isDefect=true;
				Reporter.ReportEvent("Object not visible - " + "User Authorization", "Enter Text", "Not Entered - Username" , false );
			}
		}
		
		if(WebDr.getValue(Password)!=null)
		{
			WebElement elmn;
			
				elmn=WebDr.getElementObject("ID|inputauthopassword");
			
			if(elmn.isDisplayed())
			{
				elmn.clear();
				Thread.sleep(500);
				elmn.sendKeys(Password);
				WebDr.isDefect=false;
				Reporter.ReportEvent("User Authorization", "Enter Text", "Entered: Password" , true );
			}
			else
			{
				WebDr.isDefect=true;
				Reporter.ReportEvent("Object not visible - " + "User Authorization", "Enter Text", "Not Entered - Password" , false );
			}
		}
		
		Driver.driver.findElement(By.id("inputauthopassword")).sendKeys(Keys.TAB);
		Thread.sleep(5000);
		Driver.driver.findElement(By.id("buttonAuthorize")).sendKeys(Keys.ENTER);
	}
	catch(Exception e){System.out.println("Exeption in WebDr.setText - "+e);}
	finally
	{
		if((isMandatory) &&(WebDr.isDefect))
		{
			System.out.println("WebDr.Call for a Bug Creation - Create Bug");
		}
	}
	
	return 0;
		
}

	//***************************************************************************************************************
	//***************************************************************Rejecting the Action****************************
	
	
	public static int Rejection(String Auth_Username,String Auth_Password, boolean isMandatory)
	{
		
		WebDr.isDefect=false;
		try
		{
			if(WebDr.getValue(Auth_Username)!=null)
			{
				WebElement elmn;
				
					elmn=WebDr.getElementObject("ID|inputauthousername");
				
				if(elmn.isDisplayed())
				{
					elmn.clear();
					Thread.sleep(500);
					elmn.sendKeys(Auth_Username);
					WebDr.isDefect=false;
					Reporter.ReportEvent("User Rejection", "Enter Text", "Entered: Username", true );
				}
				else
				{
					WebDr.isDefect=true;
					Reporter.ReportEvent("Object not visible - " + "User Rejection", "Enter Text", "Not Entered - Username" , false );
				}
			}
			
			if(WebDr.getValue(Auth_Password)!=null)
			{
				WebElement elmn;
				
					elmn=WebDr.getElementObject("ID|inputauthopassword");
				
				if(elmn.isDisplayed())
				{
					elmn.clear();
					Thread.sleep(500);
					elmn.sendKeys(Auth_Password);
					WebDr.isDefect=false;
					Reporter.ReportEvent("User Rejection", "Enter Text", "Entered: Password", true );
				}
				else
				{
					WebDr.isDefect=true;
					Reporter.ReportEvent("Object not visible - " + "User Rejection", "Enter Text", "Not Entered - Password", false );
				}
			}
			
			Driver.driver.findElement(By.id("inputauthopassword")).sendKeys(Keys.TAB);
			Driver.driver.findElement(By.id("buttonAuthorize")).sendKeys(Keys.TAB);
			Thread.sleep(5000);
			Driver.driver.findElement(By.id("buttonReject")).sendKeys(Keys.ENTER);
			WebDr.setText("X", "txt_rejectreason", WebDr.getValue("Reject_Reason"), "Entering reason for deletion", true);
			Driver.driver.findElement(By.id("inputautho")).sendKeys(Keys.TAB);
			Driver.driver.findElement(By.id("authosubmit")).sendKeys(Keys.ENTER);
		}
		catch(Exception e){System.out.println("Exeption in WebDr.setText - "+e);}
		finally
		{
			if((isMandatory) &&(WebDr.isDefect))
			{
				System.out.println("WebDr.Call for a Bug Creation - Create Bug");
			}
		}
		
		return 0;
			
	}


	
	public static void validateReferStream(String value) 
	{
		try{
			String fieldName,  expectedValue;
		String [] a= value.split("\\|");
		fieldName=a[0];
		expectedValue=a[1];
		WebElement oElm=WebDr.getElementObject("XPATH|//div[@class='ui-grid-row']//div[contains(text(),'" + fieldName + "')]/following-sibling::div[1]");
		String actual=oElm.getText();                 
		if(actual.equals(": " + expectedValue))
			Reporter.ReportEvent("Verifying Refer Stream Code", fieldName + "  Expected:" + expectedValue, "Actual:" + actual, true);
		else
			Reporter.ReportEvent("Verifying Refer Stream Code", fieldName + "  Expected:" + expectedValue, "Actual:" + actual, false);
		
	
	}catch(Exception e)
	{
		Reporter.ReportEvent("View page is not visible hence cannot be validated", "View page should be visible to validate", "View page is not visible ", false);
	}
	}
		
	
	//*************************************************************************************
	//*************************************************************************************	
	public static boolean selectAccountFromList(String accountNumber) throws Exception
	{
		boolean flag=searchAccountInList(accountNumber);
		if(flag)
		{
			WebDr.clickX("CUSTOM", "XPATH|//md-cell[text()=' "+ accountNumber +" ']/../md-cell[6]/md-icon/*/img", "Clicking Withdraw icon on Withdrawal page", true);
			return true;
		}
		else 
			HtmlReporter.WriteStep("Y Selecting Account Number", "Account No : " + accountNumber, "Account No. " + accountNumber +" Not found in list", false);return false; 
		
	}
	//*************************************************************************************
	//*************************************************************************************	
	public static void EnterDenominationZAR() throws Exception
	{
		String dnomData=WebDr.getValue("Denominations");
		String[] eachDenom=dnomData.split(",");
		
		for(int i=0;i<eachDenom.length;i++)
		{
			String dnv[]=eachDenom[i].split(":");			
			WebDr.setText("CUSTOM", "XPATH|//input[@ng-reflect-model='" + dnv[0] +"']/../../td[4]/*/*/*/*/input", dnv[1], "Enering Denomination " + dnv[0] + " value: " + dnv[1], false);
			
			
		}				
	}
	//*************************************************************************************
	//*************************************************************************************	
	public static void clearROATEnvVaribale() throws Exception
	{
		roatEnv.clear();
	}
	//*************************************************************************************
	//*************************************************************************************	
	public static void storeAccountBalanceCheck() throws Exception
	{
		roatEnv.put("LocalBalance", "1");
	}
	//*************************************************************************************
	//*************************************************************************************	
	public static void storeTellerBalanceCheck() throws Exception
	{
		WebDr.SetPageObjects("AccountInfoPage");
		String denomTR=WebDr.page_Objects.get("DenominationRows");
		List<WebElement> elms= Driver.driver.findElements(By.xpath(denomTR));
		
		for (int i=2; i<=elms.size(); i++) 
		{		
			List<WebElement> elms1= Driver.driver.findElements(By.xpath(denomTR + "[" + i + "]/td"));
			if(elms1.size() < 3)
			{
				i++;
				continue;
			}
			
			String key=Driver.driver.findElement(By.xpath(denomTR + "[" + i + "]/td[2]")).getText();
			String value1=(Driver.driver.findElement(By.xpath(denomTR + "[" + i + "]/td[1]")).getText()).trim();
			String value2=(Driver.driver.findElement(By.xpath(denomTR + "[" + i + "]/td[4]//input")).getAttribute("value")).trim();
 			String value=(Integer.parseInt(value1)- (value2.equals("") ? 0 :	Integer.parseInt(value2))) +"";			
			roatEnv.put(key, value);
		}		
	}
	//*************************************************************************************
	//*************************************************************************************	
	public static void verifyTellerBalance() throws Exception
	{
		WebDr.SetPageObjects("AccountInfoPage");
		String denomTR=WebDr.page_Objects.get("DenominationRows");
		List<WebElement> elms= Driver.driver.findElements(By.xpath(denomTR));
		
		for (int i=2; i<=elms.size(); i++)
		{		
			List<WebElement> elms1= Driver.driver.findElements(By.xpath(denomTR + "[" + i + "]/td"));
			if(elms1.size() < 3)
			{
				i++;
				continue;
			}
			
			String key=Driver.driver.findElement(By.xpath(denomTR + "[" + i + "]/td[2]")).getText();
			
			String expected=roatEnv.get(key);
			String actual=Driver.driver.findElement(By.xpath(denomTR + "[" + i + "]/td[1]")).getText();
			
			if(expected.equals(actual))
				HtmlReporter.WriteStep("Verifying Teller Balance", "Denomination : " + key + "  Expected:" + expected, "Actual:" + actual, true);
			else
				HtmlReporter.WriteStep("Verifying Teller Balance", "Denomination : " + key + "  Expected:" + expected, "Actual:" + actual, false);		
			
		}		
	}
	//*************************************************************************************
	//*************************************************************************************	
	public static void storeIBTAccountBalance() throws Exception
	{
		List<WebElement> elms=Driver.driver.findElements(By.xpath("//span[@class='amountspace']/span"));		
		
		String a=elms.get(0).getText();
		a=a.substring(4).replaceAll(",", "");
		NumberFormat format = NumberFormat.getCurrencyInstance(Locale.US);
		String FromAccountBal = "ZAR " + ("" +format.format((new Double(a).doubleValue()-Double.parseDouble(WebDr.getValue("TransferAmount"))))).substring(1);	
		
		a=elms.get(2).getText();
		a=a.substring(4).replaceAll(",", "");
		format = NumberFormat.getCurrencyInstance(Locale.US);
		String ToAccountBal = "ZAR " + ("" +format.format((new Double(a).doubleValue()+Double.parseDouble(WebDr.getValue("TransferAmount"))))).substring(1);	
		
		
		roatEnv.put("FromAccountBal", FromAccountBal);
		roatEnv.put("ToAccountBal", ToAccountBal);
		
		System.out.println(roatEnv.get("FromAccountBal"));
		System.out.println(roatEnv.get("ToAccountBal"));
		
	}	
	//*************************************************************************************
	//*************************************************************************************	
	public static void verifyIBTAccountBalance() throws Exception
	{
		Thread.sleep(2000);
		WebDr.SetPageObjects("Transfer_AccountInfoPage");
		WebDr.setText("X", "txt_TransferAmt", WebDr.getValue("TransferAmount"), "Entering Transfer", true);
		WebDr.setText("X","txt_FromNarrative", WebDr.getValue("FromNarrative"), "Entering Narrative under From Account", true);
		WebDr.setText("X","txt_ToAccountNo", WebDr.getValue("ToAccountNumber"), "Entering To Account Number for Transfer", true);
		WebDr.setText("X","txt_ToNarrative", WebDr.getValue("ToNarrative"), "Entering Narrative under To Account", true);
		WebDr.clickX("X", "btn_OK", "Clicking OK buttun after entering To Account Number", true);
		Thread.sleep(2000);
		
		List<WebElement> elms=Driver.driver.findElements(By.xpath("//span[@class='amountspace']/span"));		
		
		
		System.out.println(elms.get(0).getText());
		System.out.println(roatEnv.get("FromAccountBal"));
		System.out.println(elms.get(2).getText());		
		System.out.println(roatEnv.get("ToAccountBal"));
		
		if(elms.get(0).getText().equals(roatEnv.get("FromAccountBal")))
			HtmlReporter.WriteStep("Verifying From Account Balance", "Expected -" + roatEnv.get("FromAccountBal") , " Matched Successfully", true);
		else
			HtmlReporter.WriteStep("Verifying From Account Balance", "Expected -" + roatEnv.get("FromAccountBal") , "Actual - "+ elms.get(0).getText(), false);
				
		if(elms.get(2).getText().equals(roatEnv.get("ToAccountBal")))
			HtmlReporter.WriteStep("Verifying From Account Balance", "Expected -" + roatEnv.get("ToAccountBal") , " Matched Successfully", true);
		else
			HtmlReporter.WriteStep("Verifying From Account Balance", "Expected -" + roatEnv.get("ToAccountBal") , "Actual - "+ elms.get(2).getText(), false);
		
		
	}
	
	
	public static void validateAccountInquiry(String value) throws Exception
	{
		try{
		String fieldName,  expectedValue;
		String [] a= value.split("\\|");
		fieldName=a[0];
		expectedValue=a[1];
		WebElement oElm=WebDr.getElementObject("XPATH|//p-accordiontab//div[text()='" + fieldName + "']/following-sibling::div[1]");
		String actual=oElm.getText();
		if(actual.equals(": " + expectedValue))
			HtmlReporter.WriteStep("Verifying Account Inquiry", fieldName + "  Expected:" + expectedValue, "Actual:" + actual, true);
		else
			HtmlReporter.WriteStep("Verifying Account Inquiry", fieldName + "  Expected:" + expectedValue, "Actual:" + actual, false);
		
	}catch(Exception e)
		{e.printStackTrace();
		Reporter.ReportEvent("Test Case execution is failed", "Account Details should be present", "Account Details are not present",false);
		
		}
		}
	
	public static void validateCustomerInquiry(String value)
	{
		try
		{
		String fieldName,  expectedValue;
		String [] a= value.split("\\|");
		fieldName=a[0];
		expectedValue=a[1];
		WebElement oElm=WebDr.getElementObject("XPATH|//p-accordiontab//div[text()='" + fieldName + "']/following-sibling::div[1]");
		String actual=oElm.getText();
		if(actual.equals(": " + expectedValue))
			HtmlReporter.WriteStep("Verifying Customer Inquiry", fieldName + "  Expected:" + expectedValue, "Actual:" + actual, true);
		else
			HtmlReporter.WriteStep("Verifying Customer Inquiry", fieldName + "  Expected:" + expectedValue, "Actual:" + actual, false);
		
	
	}catch(Exception e)

	{
		e.printStackTrace();
		Reporter.ReportEvent("Test Case execution is failed", "Customer Inquiry data should be correct", "Customer Inquiry data is not correct",false);
		
	}
	}
	
	public static void validate_CashDrawer_user_page(String Htmlid,String value, int index)
	{
		try{
		String fieldName,  expectedValue;
		//int index = 1; 
		String [] a= value.split("\\|");
		fieldName=a[0];
		expectedValue=a[1];
		
		
		WebElement oElm=WebDr.getElementObject("XPATH|//div[@id='"+Htmlid + index+ "'][text()='" + fieldName + "']/following-sibling::div[1]");
		
		String actual=oElm.getText();
		
		if(actual.equals(expectedValue))
			HtmlReporter.WriteStep("Verifying User_view_Page", fieldName + "  Expected:" + expectedValue, "Actual:" + actual, true);
			
		else
			HtmlReporter.WriteStep("Verifying User_view_Page", fieldName + "  Expected:" + expectedValue, "Actual:" + actual, false);
		}
		catch(Exception e )
		{
		e.printStackTrace();
		Reporter.ReportEvent("Test Case execution is failed", "View Text is displayed", "View Text is not displayed",false);
		
		}
		
	}
	
	public static void cash_drawer_DD(String Dropdown, String Searchbox, String DD_values, String Inputvalue) throws Exception{
		
		WebDr.click("X",Dropdown, "Click on dropdown", true);
		WebDr.setText("X", Searchbox,Inputvalue, "select the teller", true);
		WebDr.click("CUSTOM", WebDr.page_Objects.get(DD_values)+"//following::div[text()='"+Inputvalue+"']", "click", true);
		
	
	}
	
	
	
}