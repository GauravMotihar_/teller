package roatResuableModule;

import java.awt.Robot;
import java.awt.event.KeyEvent;
import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import org.apache.commons.lang3.StringEscapeUtils;
import org.openqa.selenium.WebElement;
import utility.Constant;
import utility.HtmlReporter;
import utility.JIRAFactory;
import utility.Reporter;
import utility.WebDr;

public class ROATValidations
{
	public static String alpha="X";
	public static String fieldName, objectName, postButtonName,Element_Type;
	public static String numeric="123456789012345678901234567890";
	public static String alphaNumeric="A1B1C1D1E2F2G2H2I3J3K3L3M4N4O4P4Q5R5S5T5U6V6W6X6Y7Z7";
	public static String specialChar="\"!#$%&'()*+-/:;<=>?@[]^_`{\\|}~.,";
	public static boolean pressFormButton=false;
	public static int count; 
	

	public static void Verify_FieldValidation(int loop) throws Exception
	{
		objectName="";
		postButtonName="";	
		Element_Type = "X";
		Robot robot = new Robot();
		 count = 0;
		int i = 1;
		for( i= 1;i<=loop;i++)
		{
			
			switch (WebDr.getValue("fieldName_"+i))
			{	
			case"sig_Account_Number":
			{
				
				pressFormButton=false;
				fieldName = WebDr.getValue("fieldName_"+i);
				WebDr.SetPageObjects("signature");
				objectName="account_no";
				break;
			}
				case"AI_Account_Number":
				{
					
					pressFormButton=false;
					fieldName = WebDr.getValue("fieldName_"+i);
					objectName="txt_AccountNumber";
					postButtonName="btn_Search";	
					WebDr.SetPageObjects("AccountInquiryPage");
					break;
				}
				case "Search_Bar":
				{
					pressFormButton=false;
					fieldName = WebDr.getValue("fieldName_"+i);
					objectName="txt_Search";
				    postButtonName="btn_Search";				    
				    WebDr.SetPageObjects("SearchBar");
				    break;
				}
				case "Quick_Bar":
				{
					pressFormButton=false;
					fieldName = WebDr.getValue("fieldName_"+i);
					objectName="txt_QuickLink";
				    postButtonName="btn_Search";					    
				    WebDr.SetPageObjects("QuickLink");
				    break;
				}
				
				case"CI_Customer_Number":
					fieldName = WebDr.getValue("fieldName_"+i);
				pressFormButton=true;
				objectName="txt_CustomerNumber";
				postButtonName="btn_Search";	
				WebDr.SetPageObjects("CustomerInquiryPage");	
				break;
			
			
			  case "AHI_Account_Number":
				  fieldName = WebDr.getValue("fieldName_"+i);
				  pressFormButton=true;
				  objectName="txt_AccountNumber";
				  postButtonName="btn_Search";	
				  WebDr.SetPageObjects("AccountHistoryInquiry");
			  break;
			  
			  case "CS_FamilyName":
				  fieldName = WebDr.getValue("fieldName_"+i); 
			    pressFormButton=false;
				objectName="tab_FamilyName";
				postButtonName="btn_Search";
				//ROATModules.CS_navigateCustomerSearch();
				
				WebDr.SetPageObjects("CustomerSearchPage"); 
				break;
			  case"CS_CompanyName":
				  fieldName = WebDr.getValue("fieldName_"+i);
				pressFormButton=false;
				objectName="tab_CompanyName";
				postButtonName="btn_Search";
				
			//	ROATModules.CS_navigateCustomerSearch();
				
				WebDr.SetPageObjects("CustomerSearchPage");
				WebDr.click("X", "btn_Clear", "Clicking on Clear Button", true);
				break;
				
			  case"AS_Account_Number":
				    fieldName = WebDr.getValue("fieldName_"+i);
				    pressFormButton=false;
					objectName="txt_AccountNumber";
				    postButtonName="btn_Search";	
				  //  ROATModules.AS_NavigateAccountSearch();
				    WebDr.SetPageObjects("AccountSearch");	
				    break;
				    
			  case "AS_Account_Name":
				  
				    fieldName = WebDr.getValue("fieldName_"+i);
				    pressFormButton=false;
					objectName="txt_AccountName";	
				    postButtonName="btn_Search";	
				  //  ROATModules.AS_NavigateAccountSearch();
				    WebDr.SetPageObjects("AccountSearch");
				    WebDr.click("X", "btn_Clear", "Clicking on Clear Button", true);
				    break;
				    
			  case "Currency_Denomination":
				  
				    fieldName = WebDr.getValue("fieldName_"+i);
				    pressFormButton=false;
					objectName="txt_Denomination";	
				    postButtonName="buttonAccept";	
				
				    
				
				    
				  //  ROATModules.AS_NavigateAccountSearch();
				    WebDr.SetPageObjects("CurrencyPage");
				    WebDr.click("X", "icon_currencyupdate", "Clicking on update icon", true);
				    WebDr.click("X", "Update_AddButton", "Clicking on Add Button in Currency", true);
					WebDr.click("X", "Update_Type_DropDown", "Click on Dropdown", true);
					robot.keyPress(KeyEvent.VK_DOWN);
					robot.keyPress(KeyEvent.VK_TAB);
				    break;
				    
			  case "Currency_Multiplier":
				  
				    fieldName = WebDr.getValue("fieldName_"+i);
				    pressFormButton=false;
					objectName="txt_multipler";	
				    postButtonName="buttonAccept";	
				    
				  //  ROATModules.AS_NavigateAccountSearch();
				    
				    break;
				    
			  case "4.2 Uncleared Effects":
				  
				    fieldName = WebDr.getValue("fieldName_"+i);
				    pressFormButton=false;
				    Element_Type = "CUSTOM";
				    WebDr.SetPageObjects("Auto_RulesPage");
					objectName=WebDr.page_Objects.get("Selectedlabel")+ fieldName+"']/../.."+WebDr.page_Objects.get("AmountText");	
				    postButtonName="buttonAccept";	
				    
				    if (HtmlReporter.tc_result==true)WebDr.waitTillElementIsDisplayed("buttonUpdate");
				    if (HtmlReporter.tc_result==true) WebDr.click("X", "buttonUpdate", "Clicking on update icon", true);
				    
				    break;
				    
				    
			  case "3.4 Low value":
							  
				    fieldName = WebDr.getValue("fieldName_"+i);
				    pressFormButton=false;
				    Element_Type = "CUSTOM";
				    WebDr.SetPageObjects("Auto_RulesPage");
					objectName=WebDr.page_Objects.get("Selectedlabel")+ fieldName+"']/../.."+WebDr.page_Objects.get("AmountText");	
					postButtonName="buttonAccept";	
					   
					break;
					
			  case "6.2 High Excess":
				  
				    fieldName = WebDr.getValue("fieldName_"+i);
				    pressFormButton=false;
				    Element_Type = "CUSTOM";
				    WebDr.SetPageObjects("Auto_RulesPage");
					objectName=WebDr.page_Objects.get("Selectedlabel")+ fieldName+"']/../.."+WebDr.page_Objects.get("AmountText");	
					postButtonName="buttonAccept";	
					
					WebDr.SetPageObjects("Auto_RulesPage");
					break;
			  case "RC_ReferCode":
				  
				    fieldName = WebDr.getValue("fieldName_"+i);
				    pressFormButton=false;
				    Element_Type = "X";
				    WebDr.SetPageObjects("ReferCodeMaintenancePage");
					objectName="AddNewReferCode";	
					postButtonName="buttonAccept";	
					
					if (HtmlReporter.tc_result==true)WebDr.clickX("X", "btn_addnew", "Clicking on AddNew button", true);
					
					break;
					
			  case "RC_BackupReferCode":
				  
				    fieldName = WebDr.getValue("fieldName_"+i);
				    pressFormButton=false;
				    Element_Type = "X";
				    WebDr.SetPageObjects("ReferCodeMaintenancePage");
					objectName="AddNewBack";	
					postButtonName="buttonAccept";	
					
					if (HtmlReporter.tc_result==true)WebDr.waitTillElementIsDisplayed("txt_AddreferCode");
					if (HtmlReporter.tc_result==true)WebDr.setText("X", "txt_AddreferCode", "18", "Entering data in Refer", true);
					break;
					
			  case "RC_Telephone":
				  
				    fieldName = WebDr.getValue("fieldName_"+i);
				    pressFormButton=false;
				    Element_Type = "X";
				    WebDr.SetPageObjects("ReferCodeMaintenancePage");
					objectName="AddNewTelephone";	
					postButtonName="buttonAccept";	
					
					if (HtmlReporter.tc_result==true)WebDr.setText("X", "AddNewBack", "5", "Enter refer code", true);
					break;
					
			  case "RC_UserID":
				  
				    fieldName = WebDr.getValue("fieldName_"+i);
				    pressFormButton=false;
				    Element_Type = "X";
				    WebDr.SetPageObjects("ReferCodeMaintenancePage");
					objectName="AddNewUserID";	
					postButtonName="buttonAccept";	
					
					if (HtmlReporter.tc_result==true)WebDr.setText("X", "AddNewTelephone", "1234567890", "Enter refer code", true);
					break;
				
			 					
			  case "RC_Contact":
				  
				    fieldName = WebDr.getValue("fieldName_"+i);
				    pressFormButton=false;
				    Element_Type = "X";
				    WebDr.SetPageObjects("ReferCodeMaintenancePage");
					objectName="AddNewContact";	
					postButtonName="buttonAccept";	
					
					if (HtmlReporter.tc_result==true)WebDr.setText("X", "AddNewUserID", "ABC3456788", "Enter refer code", true);
					break;
					
			  case "RC_MgReferCode":
				  
				    fieldName = WebDr.getValue("fieldName_"+i);
				    pressFormButton=false;
				    Element_Type = "X";
				    WebDr.SetPageObjects("ReferCodeMaintenancePage");
					objectName="AddNewMgReferCode";	
					postButtonName="buttonAccept";	
					 
					if (HtmlReporter.tc_result==true)WebDr.setText("X", "AddNewContact", "ABCDF355", "Enter refer code", true);
					break;
					
				    //transaction code    
			  case "TC_DrTranCode":
				  	
				  	
				  	fieldName = WebDr.getValue("fieldName_"+i);
				    pressFormButton=false;
				    Element_Type = "X";
				    WebDr.SetPageObjects("TransactionCodeNew");
				    if (HtmlReporter.tc_result==true)ROATModules.setup_TransactionCodeNavigation();
					  if (HtmlReporter.tc_result==true)ROATModules.setup_TransactionCodeNavigationUpdate();
					objectName="txt_Local_DR_Tran_Code";
					
					if (HtmlReporter.tc_result==true)WebDr.clickX("X", "accept", "Clicking on AddNew button", true);
					postButtonName="accept";	
					
					break;
				   
					
					
			  case "CD_Limit1":
				  
				    fieldName = WebDr.getValue("fieldName_"+i);
				    pressFormButton=false;
				    Element_Type = "X";
				    WebDr.SetPageObjects("LimitsPage");
					objectName="BL_1";	
					postButtonName="buttonAccept";	
					 
					if (HtmlReporter.tc_result==true)WebDr.click("X", "btn_AddNew", "Clicking on add new button", true);
					break;
					
			  case "CD_Days":
				  
				    fieldName = WebDr.getValue("fieldName_"+i);
				    pressFormButton=false;
				    Element_Type = "X";
				    WebDr.SetPageObjects("LimitsPage");
					objectName="txt_add_CRs_for_Retail_A/cs_Open_days1";	
					postButtonName="buttonAccept";	
					 
					if (HtmlReporter.tc_result==true)WebDr.setText("X", "BL_1", "22", "enter teller transaction limits", true);
					break;
					
			  case "CD_Limit2":
				  
				    fieldName = WebDr.getValue("fieldName_"+i);
				    pressFormButton=false;
				    Element_Type = "X";
				    WebDr.SetPageObjects("LimitsPage");
					objectName="txt_add_limit_CRs_for_Retail_A/cs_Open1";	
					postButtonName="buttonAccept";	
					 
					if (HtmlReporter.tc_result==true)WebDr.setText("X", "txt_add_limit_CRs_for_Retail_A/cs_Open1", "22", "enter no of days for retails account open", true);
					break;
			  case"LCY_change_in":
				    fieldName = WebDr.getValue("fieldName_"+i);
				    pressFormButton=false;
					objectName="txtnote1_In";
				    postButtonName="btn_Search";	
				  ROATModules.setup_LcyChangeinAmountNavigation();
				  if (HtmlReporter.tc_result==true)ROATModules.enterChangeinAmount("LCYChangeinAmount","Enter_Amount");
				  if (HtmlReporter.tc_result==true)ROATModules.Click_EnterKey();
				    WebDr.SetPageObjects("LCYChangeinAmount");	
				    break;
			  case"CC_Buy":
				{
					fieldName = WebDr.getValue("fieldName_"+i);
					pressFormButton=false;
					objectName="CC_Buy";
					postButtonName="btn_Search";	
					WebDr.SetPageObjects("ForexRateUpdate");
					break;
				}
				case"CC_Sell":
				{
					fieldName = WebDr.getValue("fieldName_"+i);
					pressFormButton=false;
					objectName="CC_Sell";
					postButtonName="btn_Search";	
					WebDr.SetPageObjects("ForexRateUpdate");
					break;
				}
				case"DD_Buy":
				{
					fieldName = WebDr.getValue("fieldName_"+i);
					pressFormButton=false;
					objectName="DD_Buy";
					postButtonName="btn_Search";	
					WebDr.SetPageObjects("ForexRateUpdate");
					break;
				}
				case"DD_Sell":
				{
					fieldName = WebDr.getValue("fieldName_"+i);
					pressFormButton=false;
					objectName="DD_Sell";
					postButtonName="btn_Search";	
					WebDr.SetPageObjects("ForexRateUpdate");
					break;
				}
				case"BD_Buy":
				{
					fieldName = WebDr.getValue("fieldName_"+i);
					pressFormButton=false;
					objectName="BD_Buy";
					postButtonName="btn_Search";	
					WebDr.SetPageObjects("ForexRateUpdate");
					break;
				}
				case"BD_Sell":
				{
					fieldName = WebDr.getValue("fieldName_"+i);
					pressFormButton=false;
					objectName="BD_Sell";
					postButtonName="btn_Search";	
					WebDr.SetPageObjects("ForexRateUpdate");
					break;
				}
				case"MR_Buy":
				{
					fieldName = WebDr.getValue("fieldName_"+i);
					pressFormButton=false;
					objectName="MR_Buy";
					postButtonName="btn_Search";	
					WebDr.SetPageObjects("ForexRateUpdate");
					break;
				}
				case"STP_limit":
				{
					fieldName = WebDr.getValue("fieldName_"+i);
					pressFormButton=false;
					objectName="STP_limit";
					postButtonName="btn_Search";	
					WebDr.SetPageObjects("ForexRateUpdate");
					break;
				}
				case"settlement_date":
				{
					fieldName = WebDr.getValue("fieldName_"+i);
					pressFormButton=false;
					objectName="settlement_date";
					postButtonName="btn_Search";	
					WebDr.SetPageObjects("ForexRateUpdate");
					break;
				}
				case"Account_no":
				{
					
					pressFormButton=false;
					fieldName = WebDr.getValue("fieldName_"+i);
					objectName="account_no";
					postButtonName="btn_Search";	
					WebDr.SetPageObjects("AddStoppedCheque");
					
					break;
				}
				case"Range":
				{
					
					pressFormButton=false;
					fieldName = WebDr.getValue("fieldName_"+i);
					objectName="range";
					postButtonName="btn_Search";	
					WebDr.SetPageObjects("AddStoppedCheque");
					if (HtmlReporter.tc_result==true)WebDr.setText("X", "cheque_no", "10000000", "Entering Cheque Number", true);
					break;
				}
				case"Cheque_no":
				{
					
					pressFormButton=false;
					fieldName = WebDr.getValue("fieldName_"+i);
					objectName="cheque_no";
					postButtonName="btn_Search";	
					WebDr.SetPageObjects("AddStoppedCheque");
					if (HtmlReporter.tc_result==true)WebDr.setText("X", "account_no", WebDr.getValue("Account_no"), "Entering Account Nummber", true);
					
					break;
				}
				case"Update_Account_no":
				{
					
					pressFormButton=false;
					fieldName = WebDr.getValue("fieldName_"+i);
					objectName="account_no";
					postButtonName="btn_Search";	
					WebDr.SetPageObjects("StopChequeInquiryUpdate");
					
					break;
				}
				case"Update_Range":
				{
					
					pressFormButton=false;
					fieldName = WebDr.getValue("fieldName_"+i);
					objectName="range";
					postButtonName="btn_Search";	
					WebDr.SetPageObjects("StopChequeInquiryUpdate");
					if (HtmlReporter.tc_result==true)WebDr.setText("X", "cheque_no", "1000000", "Entering Cheque Number", true);
					break;
				}
				case"Update_Cheque_no":
				{
					
					pressFormButton=false;
					fieldName = WebDr.getValue("fieldName_"+i);
					objectName="cheque_no";
					postButtonName="btn_Search";	
					WebDr.SetPageObjects("StopChequeInquiryUpdate");
					if (HtmlReporter.tc_result==true)WebDr.setText("X", "account_no", WebDr.getValue("Account_no"), "Entering Account Nummber", true);
					break;
				}
				
				 case "GL_Number":
					  
					    fieldName = WebDr.getValue("fieldName_"+i);
					    pressFormButton=false;
					    Element_Type = "X";
					    WebDr.SetPageObjects("VaultLCYPaymentCDC");
						objectName="txtGLNo";	
						postButtonName="buttonAccept";	
						
						break;
							
				  case "Amount":
					  
					    fieldName = WebDr.getValue("fieldName_"+i);
					    pressFormButton=false;
					    Element_Type = "X";
					    WebDr.SetPageObjects("VaultLCYPaymentCDC");
						objectName="txtAmount";	
						postButtonName="buttonAccept";	
						
						if (HtmlReporter.tc_result==true)WebDr.setText("X", "txtGLNo", WebDr.getValue("GLNumber1"), "enter no of days for retails account open", true);
						break;
						
				  case "Count":
					  
					    fieldName = WebDr.getValue("fieldName_"+i);
					    pressFormButton=false;
					    Element_Type = "X";
					    WebDr.SetPageObjects("VaultLCYPaymentCDC");
						objectName="txtCount";	
						postButtonName="buttonAccept";	
						
						if (HtmlReporter.tc_result==true)WebDr.setText("X", "txtAmount", "91.86", "enter no of days for retails account open", true);
						break;
					
				  case "CDAccount_Number":
					  
					    fieldName = WebDr.getValue("fieldName_"+i);
					    pressFormButton=false;
					    WebDr.SetPageObjects("CashDepositPage");
						objectName="txt_AccountNumber";	
						postButtonName="buttonAccept";	
						
						if (HtmlReporter.tc_result==true)WebDr.click("X", "CashDepositRadiobtn", "Click on Cash deposit radio button", true);
						if (HtmlReporter.tc_result==true)WebDr.click("X", "CDManualRadiobtn", "Click on Manual option radio button", true);
						if (HtmlReporter.tc_result==true)WebDr.waitTillElementIsDisplayed("txt_AccountNumber");
						//ROATFunctions.cash_drawer_DD("BranchDD", "BranchIN", "BranchOP", WebDr.getValue("BranchNumber1"));
						break;
						
						
				  case "CDDeposit_Amount":
					  
					    fieldName = WebDr.getValue("fieldName_"+i);
					    pressFormButton=false;
					    WebDr.SetPageObjects("CashDepositPage");
						objectName="DepositAmount";	
						postButtonName="buttonAccept";	
						ROATFunctions.cash_drawer_DDX("BranchDD", "BranchIN", "BranchOP", WebDr.getValue("BranchNumber1"));
						if (HtmlReporter.tc_result==true)WebDr.setText("X", "txt_AccountNumber", WebDr.getValue("AccountNumber1"), "Enter the Account Number", true);
						break;
						
				  case "CDTotal_Cash":
					  
					    fieldName = WebDr.getValue("fieldName_"+i);
					    pressFormButton=false;
					    WebDr.SetPageObjects("CashDepositPage");
						objectName="TotalCash";	
						postButtonName="buttonAccept";	
						
						if (HtmlReporter.tc_result==true)WebDr.setText("X", "DepositAmount", WebDr.getValue("Deposit Amount"), "Enter the Deposit Amount", true);
						break;
						
				
						
					 			
					case "Fees_FlatFee":
						
						fieldName = WebDr.getValue("fieldName_" + i);
						pressFormButton = false;
						WebDr.SetPageObjects("FeesDraft");
						objectName = "Fees_SlidingScalePopUp_FlatFee";
						postButtonName = "buttonAccept";
						if (HtmlReporter.tc_result==true)WebDr.waitTillElementIsDisplayed("Fees_AddButton");
						if (HtmlReporter.tc_result==true)WebDr.click("X", "Fees_AddButton", "Clicked on Add New Button in Fees", true);
						ROATFunctions.cash_drawer_DD("BranchDD", "BranchIN", "BranchOP", WebDr.getValue("Fees_SellExchangeSellDropdown1"));
						ROATFunctions.cash_drawer_DD("SetupDD", "SetupIN", "SetupOP", WebDr.getValue("Fees_SellExchangeSellDropdown8"));
						if (HtmlReporter.tc_result==true)WebDr.click("X", "Fees_SlideScaleDD", "click on sliding scale", true);
						if (HtmlReporter.tc_result==true)WebDr.click("CUSTOM", WebDr.page_Objects.get("Fees_SlideScaleOP")+"Yes']", "click on sliding scale", true);
						break;
						
					case "PerOfTotalSec":
						
						fieldName = WebDr.getValue("fieldName_" + i);
						pressFormButton = false;
						WebDr.SetPageObjects("FeesDraft");
						objectName = "Fees_PerOfTotalSecRow";
						postButtonName = "buttonAccept";
						if (HtmlReporter.tc_result==true)WebDr.setText("X", "Fees_SlidingScalePopUp_FlatFee", WebDr.getValue("FlatFee"),
								"Successfully entered value for Flat fee", true);

						break;
						
					case "RangeFromSec":
						
						fieldName = WebDr.getValue("fieldName_" + i);
						pressFormButton = false;
						WebDr.SetPageObjects("FeesDraft");
						objectName = "Fees_RangeFromSecRow";
						postButtonName = "buttonAccept";
						if (HtmlReporter.tc_result==true)WebDr.setText("X", "Fees_PerOfTotalSecRow", WebDr.getValue("FlatFee"),
								"Successfully entered value for Flat fee", true);
						break;
			
					case "RangeToSec":
						
						fieldName = WebDr.getValue("fieldName_" + i);
						pressFormButton = false;
						WebDr.SetPageObjects("FeesDraft");
						objectName = "Fees_RangeToSecRow";
						postButtonName = "buttonAccept";
						if (HtmlReporter.tc_result==true)WebDr.setText("X", "Fees_RangeFromSecRow", WebDr.getValue("FlatFee"),
								"Successfully entered value for Flat fee", true);
						break;
			
					case "MinCharge":
						
						fieldName = WebDr.getValue("fieldName_" + i);
						pressFormButton = false;
						WebDr.SetPageObjects("FeesDraft");
						objectName = "Fees_MinChargeSecRow";
						postButtonName = "buttonAccept";
						if (HtmlReporter.tc_result==true)WebDr.setText("X", "Fees_RangeToSecRow", WebDr.getValue("FlatFee"),
								"Successfully entered value for Flat fee", true);
						break;
						
					case "MaxCharge":
						
						fieldName = WebDr.getValue("fieldName_" + i);
						pressFormButton = false;
						WebDr.SetPageObjects("FeesDraft");
						objectName = "Fees_MaxChargeSecRow";
						postButtonName = "buttonAccept";
						if (HtmlReporter.tc_result==true)WebDr.setText("X", "Fees_MinChargeSecRow", WebDr.getValue("FlatFee"),
								"Successfully entered value for Flat fee", true);
						break;
						
					case "CRTransCode":
						
						fieldName = WebDr.getValue("fieldName_" + i);
						pressFormButton = false;
						WebDr.SetPageObjects("FeesDraft");
						objectName = "Fees_CRTransCode";
						postButtonName = "buttonAccept";
						if (HtmlReporter.tc_result==true)WebDr.setText("X", "Fees_AddNew_Account", WebDr.getValue("Fess_AddNewAccount"),
								"Successfully entered value for Account No", true);

						break;
						
					case "FeesAccountNo":
						fieldName = WebDr.getValue("fieldName_" + i);
						pressFormButton = false;
						WebDr.SetPageObjects("FeesDraft");
						objectName = "Fees_AddNew_Account";
						postButtonName = "buttonAccept";
						if (HtmlReporter.tc_result==true)WebDr.setText("X", "Fees_MaxChargeSecRow", WebDr.getValue("FlatFee_Max"),
								"Successfully entered value for Flat fee", true);
						if (HtmlReporter.tc_result==true)WebDr.waitTillElementIsDisplayed("Fees_Accept");
						if (HtmlReporter.tc_result==true)WebDr.click("X", "Fees_Accept", "Clicking on Accept Button on sliding scale page", true);
						ROATFunctions.cash_drawer_DD("BranchDD", "BranchIN", "BranchOP", WebDr.getValue("Fees_SellExchangeSellDropdown1"));
						
						
						ROATFunctions.clickEnter();
						break;
	case "Charge_Package":
						  
					    fieldName = WebDr.getValue("fieldName_"+i);
					    pressFormButton=false;
					    WebDr.SetPageObjects("ChargePackagesPage");
						objectName="txtChargePackage";	
						postButtonName="buttonAccept";	
						if (HtmlReporter.tc_result == true)WebDr.waitTillElementIsDisplayed("ButtonAddNew");
						if (HtmlReporter.tc_result == true)WebDr.click("X", "ButtonAddNew", "Click on Add New", true);
						
						break;
					case "Group_Name":
						  
					    fieldName = WebDr.getValue("fieldName_"+i);
					    pressFormButton=false;
					    WebDr.SetPageObjects("TariffMaintenancePage");
						objectName="txtGroupName";	
						postButtonName="buttonAccept";	
						if (HtmlReporter.tc_result == true)WebDr.click("X", "ButtonAddNew", "Click on Add New Button", true);
						
						break;
						
				  case "Group_Description":
					  
					    fieldName = WebDr.getValue("fieldName_"+i);
					    pressFormButton=false;
					    WebDr.SetPageObjects("TariffMaintenancePage");
						objectName="txtGroupDesc";	
						postButtonName="buttonAccept";	
						if (HtmlReporter.tc_result == true)WebDr.setText("X", "txtGroupName", "Veena", "Enter Group Name", true);
						break;
						
				  case "Flat_Fee":
					  
					    fieldName = WebDr.getValue("fieldName_"+i);
					    pressFormButton=false;
					    WebDr.SetPageObjects("TariffMaintenancePage");
						objectName="txtFlatFee";	
						postButtonName="buttonAccept";	
						if (HtmlReporter.tc_result == true)WebDr.setText("X", "txtGroupDesc", "Description for Tariff", "Enter Group Desc", true);
						break;
						
				  case "EarmarksAccNumber":
					  
					    fieldName = WebDr.getValue("fieldName_"+i);
					    pressFormButton=false;
					    Element_Type = "X";
					    WebDr.SetPageObjects("Earmarks_Update");
						objectName="EarUpd_AccNo_Textbox";	
						postButtonName="buttonAccept";	
						WebDr.Wait_Time(3000);						
						
						break;
						
				  case"Earmark_AccountNo":
						
						pressFormButton=false;
						fieldName = WebDr.getValue("fieldName_"+i);
						objectName="account_no";
						postButtonName="btn_Search";	
						WebDr.SetPageObjects("EarmarksInquiry");
						break;
					
			}			
			if(WebDr.getValue("alphaNumeric_1") !=null)
				alpha=WebDr.getValue("alphaNumeric_1");
				if (HtmlReporter.tc_result==true)
				
				{	
					alphaNumericCheck(i);
					fixedLenghtCheck(i);
					minLenghtCheck(i);
					maxLenghtCheck(i);
					spacesCheck(i);
					specialCharsCheck(i);
					amountCheck(i);
					blankCheck(i);
					negativeNumberCheck(i);
					reportBugInJIRA();
				}
				
				if(i<loop)
				{
					if (WebDr.flag_validation)
					{
						
						count=count+1;
						WebDr.flag_validation = false;
					}
				HtmlReporter.tc_result=true;
				
				}
				
		}
		
		if (count>0)
		{
			HtmlReporter.tc_result=false;
		}
		
		
	}
		
	
	
	//********************************************************************************************************
	//********************************************************************************************************
	private static void fixedLenghtCheck(int i) throws Exception
	{
		System.out.println(WebDr.getValue("fixedLength_"+i));
		
		System.out.println(WebDr.getValue("fixedLenght_"+i).indexOf('|'));
		if(WebDr.getValue("fixedLength_"+i) !="")	
		{
			String a1=WebDr.getValue("fixedLenght_"+i).substring(0, WebDr.getValue("fixedLenght_"+i).indexOf('|'));
			String a2=WebDr.getValue("fixedLenght_"+i).substring(WebDr.getValue("fixedLenght_"+i).indexOf('|')+1, WebDr.getValue("fixedLenght_"+i).length());
			
			String nt1="", nt2="", t1="";
			if(alpha.contains("true"))
			{
				Reporter.ReportEvent("**Field Validation - Fixed Lenght", "Checking Alpha Numeric Field -" + fieldName, "Validating Fixed Length -" + a1 , true );
				nt1=alphaNumeric.substring(0, Integer.parseInt(a1)-1);
				nt2=alphaNumeric.substring(0, Integer.parseInt(a1)+1);
				t1=alphaNumeric.substring(0, Integer.parseInt(a1));
			}
			else if (alpha.contains("false"))
			{
				Reporter.ReportEvent("**Field Validation - Fixed Lenght", "Checking Numeric Field -" + fieldName, "Validating Fixed Length -" + a1 , true );
				nt1=numeric.substring(0, (Integer.parseInt(a1)-1));
				nt2=numeric.substring(0, (Integer.parseInt(a1)+1));
				t1=numeric.substring(0, Integer.parseInt(a1));
			}
			
			setVerify(nt1, a2,"L");
			setVerify(nt2, a2,"L");
			setVerifyDisplayed(t1);
		}
	}
	//********************************************************************************************************
	//********************************************************************************************************
	private static void minLenghtCheck(int i) throws Exception
	{
		
		if(WebDr.getValue("minLenght_"+i) !="")	
		{
			String a1=WebDr.getValue("minLenght_"+i).substring(0, WebDr.getValue("minLenght_"+i).indexOf('|'));
			String a2=WebDr.getValue("minLenght_"+i).substring(WebDr.getValue("minLenght_"+i).indexOf('|')+1, WebDr.getValue("minLenght_"+i).length());
			
			String nt1="", nt2="", t1="";
			if(alpha.contains("true"))
			{
				Reporter.ReportEvent("**Field Validation - Minimum Lenght", "Checking Alpha Numeric Field -" + fieldName, "Validating Minumum Length -" + a1 , true );
				nt1=alphaNumeric.substring(0, 1);
				nt2=alphaNumeric.substring(0, Integer.parseInt(a1)-1);
				t1=alphaNumeric.substring(0, Integer.parseInt(a1));
			}
			else if (alpha.contains("false"))
			{
				Reporter.ReportEvent("**Field Validation - Minimum Lenght", "Checking Numeric Field -" + fieldName, "Validating Minumum Length -" + a1 , true );
				nt1=numeric.substring(0, 1);
				nt2=numeric.substring(0, Integer.parseInt(a1)-1);
				t1=numeric.substring(0, Integer.parseInt(a1));
			}
			
			setVerify(nt1, a2,"L");
			setVerify(nt2, a2,"L");
			setVerifyDisplayed(t1);
		}
	}
	//********************************************************************************************************
	//********************************************************************************************************
	private static void maxLenghtCheck(int i) throws Exception
	{
		if(WebDr.getValue("maxLenght_"+i) !="")	
		{
			String a1=WebDr.getValue("maxLenght_"+i).substring(0, WebDr.getValue("maxLenght_"+i).indexOf('|'));
			String a2=WebDr.getValue("maxLenght_"+i).substring(WebDr.getValue("maxLenght_"+i).indexOf('|')+1, WebDr.getValue("maxLenght_"+i).length());
			
			String nt1="", t2="", t1="";
			if(alpha.contains("true"))
			{
				Reporter.ReportEvent("**Field Validation - Maximum Lenght", "Checking Alpha Numeric Field -" + fieldName, "Validating Maximum Length -" + a1 , true );
				nt1=alphaNumeric.substring(0, Integer.parseInt(a1) +1 );
				t1=alphaNumeric.substring(0, Integer.parseInt(a1));
				t2=alphaNumeric.substring(0, Integer.parseInt(a1)-1);
			}
			else if (alpha.contains("false"))
			{
				Reporter.ReportEvent("**Field Validation - Maximum Lenght", "Checking Numeric Field -" + fieldName, "Validating Maximum Length -" + a1 , true );
				nt1=numeric.substring(0, Integer.parseInt(a1) +1 );
				t1=numeric.substring(0, Integer.parseInt(a1));
				t2=numeric.substring(0, Integer.parseInt(a1)-1);
			}
			//pressFormButton=true;
			setVerify(nt1, a2,"L");
			setVerifyDisplayed(t1);
			setVerifyDisplayed(t2);
			
		}
	}
	//********************************************************************************************************
	//********************************************************************************************************
	private static void spacesCheck(int i) throws Exception
	{		
		if(WebDr.getValue("space_"+i) !="")	
		{
			String a1=WebDr.getValue("space_"+i).substring(0, WebDr.getValue("space_"+i).indexOf('|'));
			String a2=WebDr.getValue("space_"+i).substring(WebDr.getValue("space_"+i).indexOf('|')+1, WebDr.getValue("space_"+i).length());
			
			String nt1="", nt2="", nt3="",nt4="";
			if (alpha.contains("true"))
			{
				Reporter.ReportEvent("**Field Validation - Spaces", "Checking Spaces -" + fieldName, "Validating Spaces -" + a1 , true );
				nt1="A "; nt2="A B "; nt3="A B C "; nt4=" A ";
			}
			else if (alpha.contains("false")) 
			{
				Reporter.ReportEvent("**Field Validation - Spaces", "Checking Spaces -" + fieldName, "Validating Spaces -" + a1 , true );
				nt1="1 "; nt2="1 2 ";	nt3="1 2 3 "; nt4=" 3 ";
			}
			//pressFormButton=false;
			setVerify(nt1, a2,"L");
			setVerify(nt2, a2,"L");
			setVerify(nt3, a2,"L");
			setVerify(nt4, a2,"L");			
		}
	}
	//********************************************************************************************************
	//********************************************************************************************************
	private static void specialCharsCheck(int i) throws Exception
	{		
		if(WebDr.getValue("specialCharacters_"+i) !="")	
		{
			String a1=WebDr.getValue("specialCharacters_"+i).substring(0, WebDr.getValue("specialCharacters_"+i).indexOf('|'));
			String a2=WebDr.getValue("specialCharacters_"+i).substring(WebDr.getValue("specialCharacters_"+i).indexOf('|')+1, WebDr.getValue("specialCharacters_"+i).length());
			
			String nt1="", nt2="", nt3="",nt4="";
			Reporter.ReportEvent("**Field Validation - Special Characters", "Checking Special Characters -" + fieldName, "Validating Spaces -" + a1 , true );
			nt1=specialChar.substring(0, 7);
			nt2=specialChar.substring(7, 15);
			nt3=specialChar.substring(15, 23);
			nt4=specialChar.substring(23, specialChar.length());	
			setVerify(nt1, a2,"Y");
			setVerify(nt2, a2,"Y");
			setVerify(nt3, a2,"Y");
			setVerify(nt4, a2,"Y");			
		}
	}
	//********************************************************************************************************
	//********************************************************************************************************
	private static void negativeNumberCheck(int i) throws Exception
	{		
		if(WebDr.getValue("negativeNumber_"+i) !="")	
		{
			String a1=WebDr.getValue("negativeNumber_"+i).substring(0, WebDr.getValue("negativeNumber_"+i).indexOf('|'));
			String a2=WebDr.getValue("negativeNumber_"+i).substring(WebDr.getValue("negativeNumber_"+i).indexOf('|')+1, WebDr.getValue("negativeNumber_"+i).length());
			
			String nt1="", nt2="";
			Reporter.ReportEvent("**Field Validation - Negative Value", "Checking Negative value -" + fieldName, "Validating Negative value -" + a1 , true );
			nt1="-1";
			nt2="-1.1";
			
			setVerify(nt1, a2,"Y");
			setVerify(nt2, a2,"Y");	
		}
	}
	//********************************************************************************************************
	//********************************************************************************************************
	private static void alphaNumericCheck(int i) throws Exception
	{		
		String a1, a2;
		if(WebDr.getValue("alphaNumeric_"+i) !="")	
		{
			 a1=WebDr.getValue("alphaNumeric_"+i).substring(0, WebDr.getValue("alphaNumeric_"+i).indexOf('|'));
			 a2=WebDr.getValue("alphaNumeric_"+i).substring(WebDr.getValue("alphaNumeric_"+i).indexOf('|')+1, WebDr.getValue("alphaNumeric_"+i).length());
			
			
			if(a1.equals("false"))
			{
				String nt1="", nt2="", nt3;
				Reporter.ReportEvent("**Field Validation - Alpha Numeric", "Checking  Alpha Numeric -" + fieldName, "Validating Spaces -" + a1 , true );
				nt1="12ABCDEF";
				//nt2="12abcdeee";
				nt3=specialChar.substring(7, 15);				
				setVerify(nt1, a2,"L");
				//setVerify(nt2, a2, false);
				setVerify(nt3, a2,"Y");			
				
			}
		}
		if(WebDr.getValue("notNumerici_"+i) !="")	
		{
			a1=WebDr.getValue("notNumerici_"+i).substring(0, WebDr.getValue("notNumerici_"+i).indexOf('|'));
			a2=WebDr.getValue("notNumerici_"+i).substring(WebDr.getValue("notNumerici_"+i).indexOf('|')+1, WebDr.getValue("notNumerici_"+i).length());
			
			
			if(a1.equals("true"))
			{
				String nt1="";
				Reporter.ReportEvent("**Field Validation - Not Numeric", "Checking  for only Numeric -" + fieldName, "Validating only Numeric -" + a1 , true );
				nt1="123456789";			
				setVerify(nt1, a2,"L");		
				
			}
			
		}
		if(WebDr.getValue("startWithSpace_"+i) !="")	
		{
			a1=WebDr.getValue("startWithSpace_"+i).substring(0, WebDr.getValue("startWithSpace_"+i).indexOf('|'));
			a2=WebDr.getValue("startWithSpace_"+i).substring(WebDr.getValue("startWithSpace_"+i).indexOf('|')+1, WebDr.getValue("startWithSpace_"+i).length());
			
			
			if(a1.equals("false"))
			{
				String nt1="";
				Reporter.ReportEvent("**Field Validation - Start with space", "Checking for Starting with Space -" + fieldName, "Validating Start with space -" + a1 , true );
				nt1=" ";			
				setVerify(nt1, a2,"L");		
			}				
		}			
	}
	//********************************************************************************************************
	//********************************************************************************************************
	private static void amountCheck(int i) throws Exception
	{		
		if(WebDr.getValue("amount_"+i)!="")	
		{
			String a1=WebDr.getValue("amount_"+i).substring(0, WebDr.getValue("amount_"+i).indexOf('|'));
			String a2=WebDr.getValue("amount_"+i).substring(WebDr.getValue("amount_"+i).indexOf('|')+1, WebDr.getValue("amount_"+i).length());
			
			String nt1, nt2, nt3,nt4, nt5, nt6,t1, t2;
			Reporter.ReportEvent("**Field Validation - Amount", "Checking Amount Field -" + fieldName, "Validating Spaces -" + a1 , true );
			nt1="00100";
			nt2="-900";
			nt3="1000.00.00";
			nt4="1000.8975";
			nt5="0";
			nt6="";
			
			t1="5000";
			t2="9000.50";
			
			setVerify(nt1, a2,"L");
			setVerify(nt2, a2,"L");
			setVerify(nt3, a2,"L");
			setVerify(nt4, a2,"L");	
			setVerify(nt5, a2,"L");	
			setVerify(nt6, a2,"L");	
			setVerifyDisplayed(t1);
			setVerifyDisplayed(t2);
			
		}
	}
	//********************************************************************************************************
	//********************************************************************************************************
	private static void blankCheck(int i) throws Exception
	{		
		if(WebDr.getValue("blank_"+i) !="")	
		{
			String a1=WebDr.getValue("blank_"+i).substring(0, WebDr.getValue("blank_"+i).indexOf('|'));
			String a2=WebDr.getValue("blank_"+i).substring(WebDr.getValue("blank_"+i).indexOf('|')+1, WebDr.getValue("blank_"+i).length());
			
			if(a1.equals("false"))
			{
				String nt1="";
				Reporter.ReportEvent("**Field Validation - Empty Field", "Checking for blank  -" + fieldName, "Validating Blank -" + a1 , true );
				nt1="";			
				setVerify(nt1, a2,"L");
			}
				
		}
	}
	//********************************************************************************************************
	//********************************************************************************************************
	public static void setVerify(String textToSet, String errToVerify,String loop) throws Exception
	{
		//----------------------------------------------------------------------------------------------------
		try
		{
			if ("L".equals(loop))
			{
				 WebDr.setText(Element_Type, objectName, textToSet, "Entering "+ textToSet + " in " + fieldName +" field", true);	
			}	
			else
			{
			 for (int i = 0; i < textToSet.length(); i++)
			    {

			        char c = textToSet.charAt(i);

			        String s = new StringBuilder().append(c).toString();

			       // element.sendKeys(s);
			        WebDr.setText_Lopp(Element_Type, objectName, s, "Entering "+ textToSet + " in " + fieldName +" field", true);	

			    } 
			 Reporter.ReportEvent("Entering "+ textToSet + " in " + fieldName +" field", "Enter Text", "Entered:" + textToSet, true );
			 //WebDr.setText_Clear(Element_Type, objectName, textToSet, "Entering "+ textToSet + " in " + fieldName +" field", true);
			 
			}
		if (pressFormButton==true)
		{
			if (!postButtonName.equals("")) WebDr.click("X", postButtonName, "Clicking Search Button", true);
			
			if (WebDr.getElement("err_FieldError") != null)
			{			
				if (!WebDr.getElement("err_FieldError").getText().equals("Account not found")) 
					WebDr.verifyText("X", "err_FieldError", true, errToVerify, "Validating " + fieldName +" Field Error", true );
				 WebDr.setText_Clear(Element_Type, objectName, textToSet, "Entering "+ textToSet + " in " + fieldName +" field", true);
			}
		}		
			else
			{
				WebDr.verifyText("X", "err_FieldError", true, errToVerify, "Validating " + fieldName +" Field Error", true );
				 WebDr.setText_Clear(Element_Type, objectName, textToSet, "Entering "+ textToSet + " in " + fieldName +" field", true);
			}
		
		}
		catch(Exception e)
		{
			System.out.println("Exeption in WebDr.verifyText - ");
			Reporter.ReportEvent("Object Not Visible", "Verify Text -" + errToVerify, "Verification failed - Object Not Visible" , false );
			
		}
		//----------------------------------------------------------------------------------------------------
	}
	//********************************************************************************************************
	//********************************************************************************************************
	public static void setVerifyDisplayed(String textToSet)throws Exception
	{	
		
		//----------------------------------------------------------------------------------------------------
		WebDr.setText("X", objectName, textToSet, "Entering "+ textToSet+ " in " + fieldName +" field", true);
		if (pressFormButton==true) 
		{
			if (!postButtonName.equals(""))WebDr.click("X", postButtonName, "Clicking Search Button", true);
			if (WebDr.getElement("err_FieldError") != null)
			{			
				if (!WebDr.getElement("err_FieldError").getText().equals("Account not found")) 
				{
					if(WebDr.displayed("X", "err_FieldError", true, "Validating field Error Should not exist", true))
						Reporter.ReportEvent("Field Error Validation", "Should not get dislpayed", "Getting displayed" , false );
					else
						Reporter.ReportEvent("Field Error Validation", "Should not get dislpayed", "Not getting displayed" , true );
				}
			}
		}
		else
		{	
			if(WebDr.displayed("X", "err_FieldError", true, "Validating field Error Should not exist", true))
				Reporter.ReportEvent("Field Error Validation", "Should not get dislpayed", "Getting displayed" , false );
			else
				Reporter.ReportEvent("Field Error Validation", "Should not get dislpayed", "Not getting displayed" , true );
		}
		Thread.sleep(1000);
		//----------------------------------------------------------------------------------------------------
	}
	//********************************************************************************************************
	//********************************************************************************************************
	public static void reportBugInJIRA() throws Exception
	{
		if(HtmlReporter.tc_result==false)
		{
			String summary="Field Validation " + fieldName + "Failed";		
			String readFileName=Constant.Path_Plugin + "TestSteps.log";
			File file = new File(readFileName);	        
			BufferedReader br = new BufferedReader(new FileReader(file));
	        String description="", st="";
	        while(st!= null)
	        {
	        	st=br.readLine();	
	        	if(st!=null)description=description + st + "\n";
	        } 
	        
	        description = StringEscapeUtils.escapeJava(description);

	        Reporter.ReportBug(summary, description);
		}	
	}

}

