package roatResuableModule;

import java.awt.Robot;
import java.awt.Toolkit;
import java.awt.datatransfer.Clipboard;
import java.awt.datatransfer.StringSelection;
import java.awt.event.KeyEvent;
import java.io.File;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Random;
import java.util.concurrent.TimeUnit;
import java.util.stream.DoubleStream;

import org.openqa.selenium.By;
import org.openqa.selenium.InvalidSelectorException;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.Keys;
import org.openqa.selenium.NoSuchElementException;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.remote.server.handler.ImplicitlyWait;
import org.openqa.selenium.support.ui.Select;
import testCases.Driver;
import utility.HtmlReporter;
import utility.JIRAFactory;
import utility.Launcher;
import utility.Reporter;
import utility.WebDr;

import org.apache.commons.io.FileUtils;
import org.apache.commons.lang.RandomStringUtils;
import org.apache.commons.lang.math.RandomUtils;
import org.apache.poi.util.SystemOutLogger;

public class SmokeROATModules 
{
	public static String CashAmount = WebDr.getValue("CashAmount");
	public static String CashInOut = WebDr.getValue("AdjustmentDrop1");
			public static Map<String, String> denomap = new HashMap<String, String>();
			//********************************************************************************************************
			//********************************************************************************************************
			public static void AI_navigateAccountInquiry() throws Exception
			{ try{
				WebDr.SetPageObjects("MasterPage");
				WebDr.waitTillElementIsDisplayed("tab_Inquiry");
				WebDr.click("X", "tab_Inquiry", "Clicking on Tab Inquiry", true);
				WebDr.waitTillElementIsDisplayed("tab_AccountInquiry");
				
				WebDr.clickX("X","tab_AccountInquiry", "Clicking on Account Inquiry", true);
				WebDr.waitTillElementIsDisplayed("tab_Inquiry");
				WebDr.click("X", "tab_Inquiry", "Clicking on Tab Inquiry", true);
			}
			catch(Exception e)
			{
				
			}
			}
			
			//********************************************************************************************************
			//********************************************************************************************************
			public static void AI_searchAccountNumber() throws Exception
			{
				try
			{System.out.println("asdfsd");
				WebDr.SetPageObjects("AccountInquiryPage");
				WebDr.waitTillElementIsDisplayed("select_BranchNumber");
				WebDr.click("X", "select_BranchNumber", "Clicking on branch Number field", true);	
				WebDr.waitTillElementIsDisplayed("txt_AccountNumber");
				WebDr.click("CUSTOM", "XPATH|//div[@class=' ui-helper-clearfix ']//div[text()='" + WebDr.getValue("Branch_Number")+ "']", "Selecting Branch Number from list", true);
				WebDr.click("X", "txt_AccountNumber", "Selecting account search text box", true);
				WebDr.setText("X", "txt_AccountNumber", WebDr.getValue("Account_Number"), "Entering Account Nummber", true);
				WebDr.click("X", "btn_Search", "Clicking on search button", true);
				
				
			}
			catch(Exception e){
				e.printStackTrace();
				Reporter.ReportEvent("Test Case execution is failed", "Account search must be performed", "Account Search not performed",false);
			}
			}
			
			
			public static void AI_validations() throws Exception
			{
				try{
				WebDr.waitTillElementIsDisplayed("labelSrchRes");
				for(int i=1; i<2; i++)
				{
					//WebDr.waitTillElementIsDisplayed("Validation"+i);
					if(WebDr.getValue("Validation" + i).equals(""))
							break;
					else
						SmokeROATFunctions.validateAccountInquiry(WebDr.getValue("Validation" + i));
				
				}
				}
				catch(Exception e){
					e.printStackTrace();
					Reporter.ReportEvent("Test Case execution is failed", "Account search validation must be performed", "Account Search validation not performed",false);
				}
				
			}
			//********************************************************************************************************
			//********************************************************************************************************
			public static void AI_clearSearch() throws Exception
			{
				WebDr.SetPageObjects("AccountInquiryPage");
				WebDr.click("X", "btn_Clear", "Clicking on ClearButton", true);
				//WebDr.Wait_Time(3000);	
			}
		
			
			
			//*********************************************************************************************************************
			//**************************************Customer Inquiry***************************************************************	
					public static void CI_navigateCustomerInquiry() throws Exception
			{
						try{
				WebDr.SetPageObjects("MasterPage");
				WebDr.waitTillElementIsDisplayed("tab_Inquiry");
				WebDr.click("X", "tab_Inquiry", "Clicking on Tab Inquiry", true);
				WebDr.waitTillElementIsDisplayed("tab_CustomerInquiry");
				WebDr.clickX("X","tab_CustomerInquiry", "Clicking on Customer Inquiry", true);	
				
				WebDr.click("X", "tab_Inquiry", "Clicking on Tab Inquiry", true);
						}
						catch(Exception e){
							e.printStackTrace();
							Reporter.ReportEvent("Test Case execution is failed", "Customer search navigation must be performed", "Customer search navigation  not performed",false);
						}
			}
			
			//Search Customer With Customer Number 
			public static void CI_customerSearch() throws Exception
			{
				try{
				WebDr.SetPageObjects("CustomerInquiryPage");
				//WebDr.select("X", "drpdwn_BranchNumber", selectType, value, description, isMandatory);
				WebDr.click("X", "txt_CustomerNumber", "Selecting account search text box", true);
				WebDr.waitTillElementIsDisplayed("txt_CustomerNumber");
				WebDr.setText("X", "txt_CustomerNumber", WebDr.getValue("Customer_Number"), "Entering Customer Nummber", true);
				WebDr.click("X", "btn_Search", "Clicking on search button", true);
				}
				catch(Exception e){
					e.printStackTrace();
					Reporter.ReportEvent("Test Case execution is failed", "Customer search  must be performed", "Customer search   not performed",false);
				}
				}
			
			//changes needs to do
			public static void CI_customerExpand() throws Exception
			{ try{
				WebDr.click("X", "link_ExpandAll", "Clicking on ExpandAll Link", true);
				//WebDr.Wait_Time(3000);
				for(int i=1; i<3; i++)
				{ WebDr.waitTillElementIsDisplayed("search_result");
					if(WebDr.getValue("Validation" + i).equals(""))
							break;
					else
						SmokeROATFunctions.validateCustomerInquiry(WebDr.getValue("Validation" + i));
				
				}
			}
			catch(Exception e){
				e.printStackTrace();
				Reporter.ReportEvent("Test Case execution is failed", "Customer search  must be performed", "Customer search   not performed",false);
			}
			}
			
		   public static void CI_CustomerCollapse() throws Exception
			{ try{
		    	WebDr.click("X", "link_CollepseAll", "Clicking on Collapse Link", true);
			}
			catch(Exception e){
				e.printStackTrace();
				Reporter.ReportEvent("Test Case execution is failed", "Collapse must be performed", "Collapse  not performed",false);
			}
			
			    //WebDr.Wait_Time(3000);
			}   

		    
		    //Clear all displayed data
		    public static void CI_CustomerClear() throws Exception
			{
		    	try{
		    	WebDr.SetPageObjects("CustomerInquiryPage");
				WebDr.click("X", "btn_Clear", "Clicking on ClearButton", true);
		    	}
		    	catch(Exception e){
					e.printStackTrace();
					Reporter.ReportEvent("Test Case execution is failed", "Clearing  must be performed", "clearing  not performed",false);
				}
			}
		    
				
				//Boolean primayValueX=WebDr.displayed("X", "verify_collapse", false, "This should not be present", true);
				//WebDr.Wait_Time(3000);
			
		    //*******************************************Account History Inquiry************************
		    //******************************************************************************************
		    //******************************************************************************************
			public static void AHI_navigateAccountHistoryInquiry() throws Exception
			{
				try{
				WebDr.SetPageObjects("MasterPage");
				//WebDr.select(elementType, elementName, selectType, value, description, isMandatory);
				WebDr.click("X", "tab_Inquiry", "Clicking on Tab Inquiry", true);
				WebDr.clickX("X","tab_AccountHistoryInquiry", "Clicking on Account History Inquiry", true);
				}
				catch(Exception e){
					e.printStackTrace();
					Reporter.ReportEvent("Test Case execution is failed", "Account history navigation  must be performed", "Account history navigation not performed",false);
				}
			}
			
			public static void AHI_PerformAccountSearch() throws Exception
			{
				try
				{
				WebDr.SetPageObjects("AccountHistoryInquiry");
				WebDr.click("X", "select_BranchNumber", "Clicking on branch Number field", true);
				WebDr.clickX("CUSTOM", "XPATH|//li/div/div[text()='" + WebDr.getValue("Branch_Number")+ "']", "Selecting Branch Number from list", true);
				WebDr.click("X", "txt_AccountNumber", "Selecting Account Name in the text box", true);
				WebDr.setText("X", "txt_AccountNumber", WebDr.getValue("Account_Number"), "Entering Account Number", true);
				WebDr.click("X", "btn_Search", "Clicking on search button", true);
			}catch(Exception e )
				{
				e.printStackTrace();
				Reporter.ReportEvent("Test Case execution is failed", "Branch Number locater should be correct", "Branch Number locater is incorrect",false);
				//Driver.driver.quit();
				}
			}
			
			
		    //*******************************************Account Search********************************
		    //******************************************************************************************
		    //******************************************************************************************
			public static void AS_NavigateAccountSearch() throws Exception
			{
				try{
				WebDr.SetPageObjects("MasterPage");
				WebDr.click("X", "tab_Search", "Clicking on Tab Search", true);
				WebDr.click("X","tab_AccountSearch", "Clicking on Account Search", true);
				WebDr.waitTillElementIsDisplayed("tab_Search");
				WebDr.click("X", "tab_Search", "Clicking on Tab Search", true);
				}
				catch(Exception e){
					e.printStackTrace();
					Reporter.ReportEvent("Test Case execution is failed", "Account Search navigation  must be performed", "Account search navigation not performed",false);
				}
				
			}
			//Search Account Accordion Verification
			public static void AS_PerformAccountSearch() throws Exception
			{
				try
				{
				WebDr.SetPageObjects("AccountSearch");
				WebDr.click("X", "btn_Search", "Clicking on search button", true);
				
		    	WebElement oElm;
		    	String Verification;
		    	WebDr.SetPageObjects("AccountSearch");
				
		    	for(int i=1; i<100; i++)
				{
					if(WebDr.getValue("SearchAccountVerification" + i).equals(""))
							break;
					else
						Verification = WebDr.getValue("SearchAccountVerification" + i);
					    oElm=WebDr.getElementObject("XPATH|//p-datatable[@id='tbAccountsData']//span[text()="+"'"+Verification+"'"+"]");
						if(Verification.equals(oElm.getText()))
						{
							Reporter.ReportEvent("Search Account Verification Link", "Verify:"+Verification+"", "Verified:"+Verification+"", true);
						}
						else
						{
							Reporter.ReportEvent("Search Account Verification Link", "Verify:"+Verification+"", "Verified:"+Verification+"", false);
						}			
				}
				
				WebDr.click("X", "btn_Clear", "Clicking on ClearButton", true);
				//WebDr.Wait_Time(3000);		
			}catch(Exception e)
				{
				e.printStackTrace();
				Reporter.ReportEvent("Test Case execution is failed", "Account search data should be correct", "Account Search data is not correct",false);
				
				}
			}
			
							

			 
			
			
			//********************************************************************************************************
			//****************************************CUSTOMER SEARCH*************************************************
			public static void CS_navigateCustomerSearch() throws Exception
			{
				try{
				WebDr.SetPageObjects("MasterPage");
				WebDr.click("X", "tab_Search", "Clicking on Search Button", true);
				WebDr.clickX("X", "tab_CustomerSearch", "Clicking on CustomerSearch", true);
				WebDr.waitTillElementIsDisplayed("tab_Search");
				WebDr.click("X", "tab_Search", "Clicking on Search Button", true);
				}
				catch(Exception e){
					e.printStackTrace();
					Reporter.ReportEvent("Test Case execution is failed", "customer Search navigation  must be performed", "customer search navigation not performed",false);
				}
				
				
			}
			
			public static void CS_customerDeatilSearch() throws Exception {
				
				try{
					WebDr.SetPageObjects("CustomerSearchPage");
				
				WebDr.click("X", "tab_ReferStreamCode", "Clicking on ReferStreamCode", true);
				WebDr.click("CUSTOM","XPATH|//div[text()='" + WebDr.getValue("ReferStreamCode")+"']", "Selecting Refer stream code from list", true);
				WebDr.waitTillElementIsDisplayed("tab_FamilyName");
		       	WebDr.setText("X", "tab_FamilyName", WebDr.getValue("FamilyName"), "Entering Family Name", true);
		        WebDr.setText("X", "tab_CompanyName", WebDr.getValue("CompanyName"), "Entering Company Name" , true);
		        WebDr.waitTillElementIsDisplayed("btn_Search");
		        WebDr.click("X", "btn_Search", "Clicking on search button", true);
		       // WebDr.Wait_Time(3000);
			    for(int i=1; i<3; i++){
			    	 WebDr.waitTillElementIsDisplayed("Search_Result");
		    		String verification;
		    		if(WebDr.getValue("CustomerValidation"+i).equals(""))
		    			{System.out.println("here");
		    			break;
		    			}
		    		else
		    		
		    			verification = WebDr.getValue("CustomerValidation" +i);
		    		    WebElement oElm = WebDr.getElementObject("XPATH|//p-datatable[@id='tbAccountData']//span[text()="+"'"+verification+"'"+"]");
		    		
		    			if(verification.equals(oElm.getText()))
		    			{
		    				Reporter.ReportEvent("Customer Verification", "Verify:"+verification+"", "Verified:"+verification+"", true);
		    			}
		    			else
		    			{
		    				Reporter.ReportEvent("Customer Search Verification", "Verify:"+verification+"", "Verified:"+verification+"", false);
		    			}			
		    		}
				}
				catch(Exception e){
					e.printStackTrace();
					Reporter.ReportEvent("Test Case execution is failed", "customer records must be presented", "Customer records not present",false);
				}
					}	
			
			//Navigation to customer			
			public static void CS_CustomerInquiryNavigation() throws Exception{
				
				try{
					WebDr.SetPageObjects("CustomerSearchPage");
				
				WebDr.click("X", "tab_CustomerLink", "clicking on customer number link", true);
				boolean fname=WebDr.displayed("X", "tab_CustomerInquiry", true, "page should be present", true);
				if(fname==true){
					Reporter.ReportEvent("Customer Inquiry Verifications", "Page should be present", "verified", true);
				}
				else{
					Reporter.ReportEvent("Customer Inquiry Verifications", "Page should be present", "failed", true);
				}
				}
				catch(Exception e){
					e.printStackTrace();
					Reporter.ReportEvent("Test Case execution is failed", "Customer Inquiry  navigation  must be performed", "Customer Inquiry navigation not performed",false);
				}
				
			}
		    
			public static void CS_clearSearch() throws Exception{
				WebDr.SetPageObjects("CustomerSearchPage");
		        WebDr.click("X", "btn_Clear", "Clicking on clear button", true);
		        //Thread.sleep(2000);
			}
			
//***********************************************Currency*******************************************************************
			
			public static void setup_navigateCurrency() throws Exception {
				try{
				WebDr.SetPageObjects("MasterPage");
				
				WebDr.waitTillElementIsDisplayed("tab_setup");
				WebDr.click("X", "tab_setup", "Clicking on Setup Tab", true);
				WebDr.clickX("X", "tab_currency", "Clicking on Currency", true);
				Thread.sleep(3000);
				WebDr.click("X", "tab_setup", "Clicking on Setup Tab", true);

			}
				catch(Exception e){
					e.printStackTrace();
					Reporter.ReportEvent("Test Case execution is failed", "Setup  navigation  must be performed", " navigation to setup page not performed",false);
				}}
   			//functionality : To validate currency text field
public static void setup_currencyVerification() throws Exception {
	WebDr.SetPageObjects("CurrencyPage");
	String verification=null;	
	WebDr.waitTillElementIsDisplayed("CurrencyText");			
	if (HtmlReporter.tc_result==true)WebDr.displayed("X", "CurrencyText", true, "Able to see currency text", true);				
	if (HtmlReporter.tc_result==true)WebDr.displayed("X", "table_currency", true, "Currency Table is displayed", true);
	try {
		if(HtmlReporter.tc_result==true){
		for (int i = 1; i <=2; i++) {
			verification = WebDr.getValue("CurrencyVerification" + i);
				String Verificationtable = WebDr.page_Objects.get("VerificationCode");							
				String oElm = WebDr.getElementObject(
						Verificationtable + "'" + verification + "'" + "]").getAttribute("innerText");
				if (verification.equals(oElm))
				{
					Reporter.ReportEvent("Currency Verification", "Verify:" + verification + "",
							"Verified:" + verification + "", true);
				}

				else {
					Reporter.ReportEvent("Currency Verification", "Verify:" + verification + "",
							"Verified:" + verification + "", false);
				}

			}
		}
	} catch (Exception e) {
		Reporter.ReportEvent("Currency Verification", "Verify:" + verification + "",
				"Verified:" + e + "", false);
	}

	// verifying table header
	try {
		if(HtmlReporter.tc_result==true){
		for (int i = 2; i < 10; i++) {
			if (WebDr.getValue("CurrencyActionVerification" + i).equals("")) {
				System.out.println("here");
				break;
			} else {
				
			verification = WebDr.getValue("CurrencyActionVerification" + i);
				String Verificationtable2 = WebDr.page_Objects.get("VerificationCode2");							
				String oElm2 = WebDr.getElementObject(
						Verificationtable2 + "'" + verification + "'" + "])[1]").getAttribute("innerText");
				
				if (verification.equals(oElm2))
				{
					Reporter.ReportEvent("Currency Actions Verification", "Verify:" + verification + "",
							"Verified:" + verification + "", true);
				}

				else {
					Reporter.ReportEvent("Currency Actions Verification", "Verify:" + verification + "",
							"Verified:" + verification + "", false);
				}
			}}
		}
	} catch (Exception e) {
		Reporter.ReportEvent("Currency Actions Verification", "Verify:" + verification + "",
				"Verified:" + e + "", false);
	}

}

       //Author: Nilima and Veena
       			//functionality : To perform  currency view functionality
public static void setup_currencyView() throws Exception {
	String verification=null;			
	try {				
	
	WebDr.SetPageObjects("CurrencyPage");
	if (HtmlReporter.tc_result==true)WebDr.click("X", "icon_currencyview", "Clicking on view icon", true);

	// Verifying the navigated page labels
	if (HtmlReporter.tc_result==true)WebDr.displayed("X", "ViewText", true, "Able to see currency text", true);
	// verifying table header
	if(HtmlReporter.tc_result==true){
	for (int i = 1; i < 10; i++) {
		
		if (WebDr.getValue("CurrencyVerification" + i).equals("")) {
			System.out.println("here");
			break;
		} else{

			verification = WebDr.getValue("CurrencyVerification" + i);
			String CurrencyViewpath = WebDr.page_Objects.get("CurrencyView");							
			String CurrencyView = WebDr.getElementObject(
					CurrencyViewpath + "'" + verification + "'" + "])[1]").getAttribute("innerText");
			
		if (verification.equals(CurrencyView))
		{
			Reporter.ReportEvent("Currency Verification", "Verify:" + verification + "",
					"Verified:" + verification + "", true);
			
		} else {
			Reporter.ReportEvent("Currency Verification", "Verify:" + verification + "",
					"Verified:" + verification + "", false);
		}
		}
		}
	}
	}
	 catch (Exception e) {
		Reporter.ReportEvent("Currency Verification", "Verify:" + verification + "",
				"Verified:" + e + "", false);
	}
	
	WebDr.click("X", "backButton_currencyview", "Clicking on view icon", true);
	
}

						//**********************************Cash Summary*************************************************************************
						//***********************************************************************************************************************
						
						public static void CD_navigateCash_Summary() throws Exception{
							try
							  {			
								WebDr.SetPageObjects("MasterPage");				
								WebDr.click("X", "tab_CashDrawer", "Clicking on Cash Drawer", true);
								WebDr.waitTillElementIsDisplayed("tab_CashSummary");
								WebDr.clickX("X","tab_CashSummary", "Clicking on Cash Drawer sub menu", true);	
								WebDr.click("X", "tab_CashDrawer", "Clicking on Cash Drawer", true);
								
							}
							catch(Exception e )
							{
								e.printStackTrace();
								Reporter.ReportEvent("Test Case execution is failed", "Cash Drawer Cash Summary page is displayed", "Cash Drawer Cash Summary page is not displayed",false);
								Reporter.ReportBug("Navigation to Cash Drawer Cash Summary Page", "Navigation to Cash drawer Cash Summary page failed");
							}
						}
						
						
						public static void CD_Cash_SummaryView() throws Exception
						{
							try
							  {	
							
								WebDr.SetPageObjects("CashSummaryPage");	
								
								//Breadcrumb verification
								WebDr.displayed("X", "BC_CashSummary", true, "Successfully navigated to Cash Summary Page", true);
								
								
								//verifying the teller name
								/*if(WebDr.getElement("TellerLogin").getText().equalsIgnoreCase(WebDr.getElement("TellerName").getText().split(" ")[2])){
									Reporter.ReportEvent("User viewing Cash Summary Page is Teller", "Teller should be able to view own cash summary", "Teller is able to view own Cash Summary", true);
								}
								else{
									Reporter.ReportEvent("User viewing Cash Summary Page is Teller", "Teller should be able to view own cash summary", "Teller is not able to view own Cash Summary", false);
								}*/
								
								
								// verifying table header
								for (int i = 1; i < 100; i++) {
									String verification;
									if (WebDr.getValue("CashSummaryVerification" + i).equals("")) {
										System.out.println("here");
										break;
									} else
				
										verification = WebDr.getValue("CashSummaryVerification" + i);
										WebElement oElm = WebDr.getElementObject(
											"XPATH|(//p-datatable//span[text()=" + "'" + verification.trim() + "'" + "])");// ----
				
									if (verification.equals(oElm.getText()))// --
									{
										Reporter.ReportEvent("Cash Summary Verification", "Verify:" + verification + "",
												"Verified:" + verification + "", true);
									} else {
										Reporter.ReportEvent("Cash Summary Verification", "Verify:" + verification + "",
												"Verified:" + verification + "", false);
									}
								}
								
								//matching the count of total records to the no of records
								String Total_Records = WebDr.getElement("Total_Records").getText();
								int total_records=Integer.parseInt(Total_Records);
								if(total_records>0){
									
									Reporter.ReportEvent("Comparing No of Total Records with the table data count", "No of Total Records matches with the table data", "No of Total Records matches with the table data", true);
								}
								else
									Reporter.ReportEvent("Comparing No of Total Records with the table data count", "No of Total Records matches with the table data", "No of Total Records does not match with the table data", false);
								
								Thread.sleep(1000);
						}
							catch(Exception e )
							{
								e.printStackTrace();
								Reporter.ReportEvent("Test Case execution is failed", "Verification of Cash Summary Page view", "Verification of Cash Summary Page view",false);
								
								
							}
					}
						//to check the sorting of the values in the table


						// ***********************************************Fees_Exchange*******************************************************************
						//Created by: Nilima Vaidya
						//functionality: To navigate to fees page from setup tab
						public static void setup_navigateFeesExchange() throws Exception {
							try{
							WebDr.SetPageObjects("MasterPage");
							WebDr.waitTillElementIsDisplayed("tab_setup");
							String value = "Fees  ";
							WebDr.click("X", "tab_setup", "Clicking on Setup Tab", true);

							WebDr.clickX("X", "tab_Fees", "Clicking on Fees", true);
							Thread.sleep(2000);
							WebDr.click("X", "tab_setup", "Clicking on Setup Tab", true);
							}
							catch(Exception e )
							{
								e.printStackTrace();
								Reporter.ReportEvent("Test Case execution is failed", "Navigation to fees page must be performed", "Navigation to fees page not performed",false);
								
								
							}

						}

						public static void  totalRecords() throws Exception {
							try{
							
							WebDr.SetPageObjects("FeesDraft");
							String Count=WebDr.page_Objects.get("TotalRecords");
							WebElement oElm;
							
							oElm = WebDr.getElementObject(Count);
								
							int data=Integer.parseInt(oElm.getText());
							if(data>0)
							{
								System.out.println("data:"+data);
								Reporter.ReportEvent("fees records", "Records should be present", "Records are present", true);
							}
							
							else
							{
								Reporter.ReportEvent("fees records", "Records should be present", "Records are not present", false);
							}
							}
							catch(Exception e )
							{
								e.printStackTrace();
								Reporter.ReportEvent("Test Case execution is failed", "Records should be present in fees module", "Records are not present",false);
								
								
							}
							
						}
						
		public static void  totalRecords1() throws Exception {
			try{
							
							WebDr.SetPageObjects("LimitsPage");
							String Count=WebDr.page_Objects.get("TotalRecords");
							WebElement oElm;
							WebDr.waitTillElementIsDisplayed("TotalRecords");
							oElm = WebDr.getElementObject(Count);
								
							int data=Integer.parseInt(oElm.getText());
							if(data>0)
							{
								System.out.println("data:"+data);
								Reporter.ReportEvent("fees records", "Records should be present", "Records are present", true);
							}
							
							else
							{
								Reporter.ReportEvent("fees records", "Records should be present", "Records are not present", false);
							}
			}
			catch(Exception e )
			{
				e.printStackTrace();
				Reporter.ReportEvent("Test Case execution is failed", "Records should be present in fees module", "Records are not present",false);
				
				
			}
			
			
						}

	
	
	// Enter payment amount in LCY Payment
				public static void Enter_payment_Amount(String object_property, String Textbox)throws Exception
				{
					try
					{
						WebDr.SetPageObjects(object_property);
						WebDr.setText("X", "txtAmounts", WebDr.getValue("Enter_Amount"), "Enter payment amount value", true);
						
					}
					catch (Exception e)
					{
						e.printStackTrace();
						Reporter.ReportEvent("Object is not Found", "Object should be display on screen", "Object not present",false);
					}
				}
				
				//Verify error Messages
				public static void error_Message_verification(String Authorize,String ExpectedMessage) throws Exception
				{
					try
					{
						WebElement oElm;
						switch (Authorize)
						{
						case "Authorize":
							
							oElm = WebDr.getElementObject("XPATH|//span[text()='Transaction Posted']");
							if(oElm.getText().equals(ExpectedMessage))
							{
								Reporter.ReportEvent("Transaction Posted Message", "Expected message"+ExpectedMessage, "Actaul message"+oElm.getText(),true);
							}
							else
							{
								Reporter.ReportEvent("Transaction Posted Message", "Expected message"+ExpectedMessage, "Actaul message"+oElm.getText(),false);	
							}
							
							
						 break;
						case "Reject":
							oElm = WebDr.getElementObject("XPATH|//span[text()='Transaction Rejected']");
							if(oElm.getText().equals(ExpectedMessage))
							{
								Reporter.ReportEvent("Transaction Posted Message", "Expected message"+ExpectedMessage, "Actaul message"+oElm.getText(),true);
							}
							else
							{
								Reporter.ReportEvent("Transaction Posted Message", "Expected message"+ExpectedMessage, "Actaul message"+oElm.getText(),false);	
							}
							
							
							 break;
						case "Mismatch":
							
							oElm = WebDr.getElementObject("XPATH|//bs-tooltip-container//span[text()='Payment Amount and Total Cash does not match']");
							if(oElm.getText().equals(ExpectedMessage))
							{
								Reporter.ReportEvent("Transaction Posted Message", "Expected message"+ExpectedMessage, "Actaul message"+oElm.getText(),true);
							}
							else
							{
								Reporter.ReportEvent("Transaction Posted Message", "Expected message"+ExpectedMessage, "Actaul message"+oElm.getText(),false);	
							}
							
							 break;
							 
						 default:
							 
							 
						}
						
					}
					catch(Exception e)
					{
						e.printStackTrace();
						Reporter.ReportEvent("Test Case execution is failed", "Authorization Present", "Authorization not present",false);
						//Driver.driver.quit();
					}
				}
					// to verify cancel functional	
				public static void Cancel_LCY_Payment_function() throws Exception
				{
					try
					{
						WebElement oElm;
						WebDr.SetPageObjects("LCYPayment");						
						
						WebDr.setText("X", "txtAmounts", WebDr.getValue("Enter_Amount"), "Enter Payment Amount Details", true);
						WebDr.clickX("X", "btncancels", "Click on cancel button", true);
						WebDr.clickX("X", "cancel_No", "Cacellation is not performed", true);
						String Value = Driver.driver.findElement(By.id("txtAmount")).getAttribute("value");
						if(Value!=null)
						{
							Reporter.ReportEvent("Cancel Operation", "Cancel No Sucessful", "Cancel No Sucessful", true);
						}
						else
						{
							Reporter.ReportEvent("Cancel Operation", "Canceled Yes Sucessful", "Canceled Yes Sucessful", false);
						}
						WebDr.clickX("X", "btncancels", "Click on cancel button", true);
						WebDr.clickX("X", "cancel_Yes", "Cacellation is performed", true);
						String Value1 = Driver.driver.findElement(By.id("txtAmount")).getAttribute("value");
						if(Value1.isEmpty())
						{
							Reporter.ReportEvent("Cancel Operation", "Cancel No Sucessful", "Cancel No Sucessful", true);
						}
						else{
							Reporter.ReportEvent("Cancel Operation", "Cancel Yes Sucessful", "Cancel Yes Sucessful", false);
						}
					}
					catch(Exception e)
					{
					e.printStackTrace();
					Reporter.ReportEvent("Test Case execution is failed", "Authorization Present", "Authorization not present",false);
					//Driver.driver.quit();
					}
				}
				
				// this Function select currency value in FCY payment Dropedown list box
				public static void FCY_payment_select_currency() throws Exception
				{
					try
					{
				
		/*				WebDr.SetPageObjects("MasterPage");
		            	WebDr.click("X", "tab_CashDrawer", "Clicking on Tab Cash Drawer", true);
		            	
		            	WebDr.click("X", "tab_FCY_payment", "Clicking on Tab Cash Drawer", true);
		            	*/
		            	
						WebDr.SetPageObjects("FCYPayment");
						
						WebDr.clickX("X", "dropcurrency", "Clicking on dropdown", true);
						WebDr.clickX("CUSTOM", "XPATH|//div[text()='" + WebDr.getValue("Currency_Name")+ "']", "Selecting currency", true);
						
						
						
					}
					catch (Exception e)
					{
						e.printStackTrace();
						Reporter.ReportEvent("Object is not Found", "Object should be display on screen", "Object not present",false);
				
					}
				}
				
				// to verify total cash any payment amount  functional	
				public static void fn_verify_total_cash_payment_amount(String object_Prpoerty,String verify_amount) throws Exception
				{
					try
					{
						WebElement oElm;
						WebDr.SetPageObjects(object_Prpoerty);						
						
						
						
						String Value = WebDr.getElementObject("XPATH|//div[contains(text"
								+ "(),'Total')]//following-sibling::strong//span").getText();
						if(Value.contains(verify_amount))
						{
							Reporter.ReportEvent("Verify_total cash and payemt amount", "amount should be match ", "amount is matching", true);
						}
						else
						{
							Reporter.ReportEvent("Verify_total cash and payemt amount", "amount should be match ", "amount is matching", false);
							Reporter.ReportBug("Expected value and actual value does not match", "Expected value"+verify_amount+" : "+"Actual Value"+Value);
						}
						
					}
					catch(Exception e)
					{
					e.printStackTrace();
					Reporter.ReportEvent("Test Case execution is failed", "Authorization Present", "Authorization not present",false);
					
					}
				}
				// This function navigate to main menu and sub menu option

				public static void menu_navigation(String objectPropertyname,String Main_Menu_ID ) throws Exception

				{

				try

				{

				WebDr.SetPageObjects(objectPropertyname); 

				WebDr.click("X", Main_Menu_ID, "Clicking on main menu object", true);

				}

				catch(Exception e )

				{

				e.printStackTrace();

				Reporter.ReportEvent("Test Case execution is failed", "Object not found ", "Navigate to correct page",false);

			

				}
				}
				public static void menu_navigation1(String objectPropertyname,String Main_Menu_ID ) throws Exception

				{

				try

				{

				WebDr.SetPageObjects(objectPropertyname); 

				WebDr.clickX("X", Main_Menu_ID, "Clicking on main menu object", true);

				}

				catch(Exception e )

				{

				e.printStackTrace();

				Reporter.ReportEvent("Test Case execution is failed", "Object not found ", "Navigate to correct page",false);

			

				}

				} 
				// Enter Note and coin count on table for LCY payment Page.
				public static void enter_note_coin_Count(String PageObjectname,String tableID,String textboxID) throws Exception{
					WebDr.SetPageObjects(PageObjectname);
					try{
					
						String table = WebDr.page_Objects.get(tableID);
						String txt = WebDr.page_Objects.get(textboxID);
						
						List<WebElement> rows = WebDr.getElements(tableID);
						
						int j = 0;
						boolean flag  = false;
						for (int i=2;i<rows.size()+1;i++)
						{
							
							WebElement row_value = WebDr.getElementObject(table +"["+i+"]//td[1]//span");
							
							flag = false;
							
							if (Integer.parseInt(row_value.getText())>0)
							{
								
								WebDr.setText("CUSTOM", txt+j, "1", "Enter value in text field", true);
								
								j = j +1;
								
								flag = true;
							}
							else
							{							
								Reporter.ReportEvent("Test Case execution is failed", "Insufficient Denomination Available", "sufficient Denomination Available",false);
								break;
							}
						}
						if (flag)
						{
							Reporter.ReportEvent("All Notes values are entered", "All Notes values should be enter", "All Notes values are entered",true);
						}
							
					}
					catch(Exception e)
					{
						e.printStackTrace();
						System.out.println("Exeption in WebDr.setText - "+e);
						Reporter.ReportEvent("Object is not Found", "Object should be display on screen", "Object not present",false);
					}
					
					}
				// Enter values in Solid and Decoy 
				public static void enter_Soiled_decoy_value()
				{
					try
					{
						WebDr.SetPageObjects("LCYPayment");
						WebDr.setText("X", "txtsolied", "1", "Enter solied value", true);
						WebDr.setText("X", "txtDecoy", "1", "Enter Decoy value", true);
					}
					catch (Exception e)
					{
						e.printStackTrace();
						Reporter.ReportEvent("Object is not Found", "Object should be display on screen", "Object not present",false);
					}
				}
				
				//Enter Authorize credential value 
				public static void Enter_Authorize_Credential(String Object_Property,String UserID,String PasswordID,String Username,String Password,boolean flag) throws Exception
				{
					try
					{
						WebDr.SetPageObjects(Object_Property);
						WebDr.clickX("X", "btn_Accept", "Clicking on Accept button", true);					
						WebDr.setText("X", UserID,Username, "Entering Authorizer username", true);
						WebDr.setText("X", PasswordID, Password, "Entering Authorizer password", true);		
						if (flag)
							{
								WebDr.clickX("X", "btn_AuthorizeButton", "Clicking on Autorize button", true);
							}
						else
							{
								WebDr.clickX("X", "btn_RejectButton", "Clicking on Autorize button", true);
							}
						
					}
					catch(Exception e)
					{
					e.printStackTrace();
					Reporter.ReportEvent("Test Case execution is failed", "Authorization Present", "Authorization not present",false);
					
					}
				}

				
				//*****************************************************************Teller Matrix*********************************************/
    			/*Module:Teller Matrix
				Author:Ajit Kesarkar*/
						public static void setup_TellerMatrixNavigation() throws Exception{
							try{
				
						WebDr.SetPageObjects("MasterPage");
						WebDr.click("X", "tab_Authorization", "Clicking on Authorization", true);
						WebDr.click("X", "tab_TellerMatrix", "Clicking on Teller Matrix", true);
						WebDr.click("X", "tab_Authorization", "Clicking on Authorization", true);
					
						}
							catch(Exception e )
							{
								e.printStackTrace();
								Reporter.ReportEvent("Test Case execution is failed", "Application must be navigated to Teller matrix page", "Application does not  be navigate to Teller matrix page",false);
								
								
							}
							
						}
						
				
				
						public static void setup_TellerMatrixVerification() throws Exception{
							try{
								
							
						WebDr.SetPageObjects("TellerMatrix");
						WebDr.click("X", "tab_drpDown", "Clicking on Branch Dropdown", true);
    					WebDr.click("X", "tab_GhanaBranch", "Clicking on ghana Branch", true);
						
						//Teller matrix Column Verification
						for (int i = 1; i < 7; i++) {
							String verification;
							if (WebDr.getValue("MatrixColumnVerification" + i).equals("")) {
								break;
							} else

								verification = WebDr.getValue("MatrixColumnVerification" + i);
							WebDr.Implicite_wait(2000);
							String tellerColumn = WebDr.page_Objects.get("TellerMatrixColumn");
							WebElement oElm = WebDr.getElementObject(tellerColumn + "'" + verification + "'" + "]");
							if (verification.equals(oElm.getText())) {
								Reporter.ReportEvent("TellerMatrix Column  Verifications", "Verify:" + verification + "",
										"Verified:" + verification + "", true);
							} else {
								Reporter.ReportEvent("TellerMatrix Column  Verifications", "Verify:" + verification + "",
										"Verified:" + verification + "", false);
							}
						
    		
						}
							}
							catch(Exception e )
							{
								e.printStackTrace();
								Reporter.ReportEvent("Test Case execution is failed", "TellerMatrix Column  must be as per Fos screens ", "TellerMatrix Column  Verifications failed",false);
								
								
							}
							
							
						}
				
				
			
				//This Function is used to check update functionality of Teller matrix
			
				
				public static void setup_TellerMatrixUpdate() throws Exception{

					
					WebDr.SetPageObjects("TellerMatrix");
					WebDr.click("X", "tab_drpDown", "Clicking on Branch Dropdown", true);
					WebDr.click("X", "tab_GhanaBranch", "Clicking on ghana Branch", true);
					/*WebDr.click("X", "tab_drpDown", "Clicking on Branch Dropdown", true);
					WebDr.click("X", "tab_GhanaBranch", "Clicking on ghana Branch", true);*/
					
					try {
					
						
						String addmsg=WebDr.page_Objects.get("label_addMsg");
						
						//This Function is used to check "add message error" size is 0 or not. 
    					/*if(Driver.driver.findElements(By.xpath(addmsg)).size() ==0)
    					{*/
						WebDr.waitTillElementIsDisplayed("btn_Update");
    					//This Function is used to click on update button. 
						WebDr.clickX("X", "btn_Update", "Clicking on Update button", true);
    					
    					String dropdownCount=WebDr.page_Objects.get("all_dropdowns");
						//int  count=Driver.driver.findElements(By.xpath(dropdownCount)).size();
						
						//integer count contains count of all dropdowns present in table.
    					//System.out.println("all counts for dropdowns:"+count);
						for(int i=3, j=1; i<5; i++, j++)
						{

							String a=WebDr.page_Objects.get("label_dropdown2");
							String b=WebDr.page_Objects.get("label_dropdown");

							Thread.sleep(1000);
							
							if(i>5 && i%6==0)
								j=1;

						WebDr.click("CUSTOM",b+i+"]", "Clicking on dropdown", true);

						// To pass excel values(0 to 6) into dropdowns .
						try {
						WebDr.clickX("CUSTOM", a+WebDr.getValue("Value"+j)+"]", "Selecting value from  dropdown", true);
						} 


						catch (Exception e) {
	
	
							Reporter.ReportEvent("Dropdown not present for this element","dropdown not present","dropdown not present", true);
	
						}


						Reporter.ReportEvent("Passed the value in dropdown","Actual:"+j,"Verified:"+j, true);
						} 

						// To click on submit button
							WebDr.click("X", "btn_submit", "Clickig on submit button", true);
							
							//To authorise the update fuctinality.
							WebDr.setText("X", "txt_USerName", "username", "Entering username details", true);
							WebDr.setText("X", "txt_Password", "password", "Entering password details", true);
							WebDr.click("X", "btn_Authorize", "Clicking on Authorize button", true);
							Thread.sleep(2000);

	
						
					/*	else
						{
							
							
							Reporter.ReportEvent("branch not updated", "branch should get update", "branch is not updated", false);
							Thread.sleep(2000);
						
						}*/
					} catch (Exception e) {
						
						e.printStackTrace();
					}
		
		
			
			
			}
				//This Function is used to check navigation and view of teller_withdrawal tab.
				
				public static void setup_TellerWithdrawaltNavigation() throws Exception
					{
				

						try
						{
							
							WebDr.SetPageObjects("MasterPage");
							WebDr.waitTillElementIsDisplayed("tab_Teller");
							WebDr.click("X", "tab_Teller", "Clicking on Tab Teller", true);

							WebDr.waitTillElementIsDisplayed("tab_Withdrawal");
							WebDr.clickX("X", "tab_Withdrawal", "Clicking on Withdrawal", true);
							WebDr.click("X", "tab_Teller", "Clicking on Tab Teller", true);
							Thread.sleep(3000);
							WebDr.SetPageObjects("TellerWithdrawal");
							//WebDr.verifyText("X", elementName, matchSubString, expectedText, description, isMandatory);
							Thread.sleep(5000);
							
//							WebDr.waitTillElementIsDisplayed("btn_CashWithdrawal");
							WebDr.click("X", "btn_CashWithdrawal", "Clicking on cashwithdrawal radio button", true);
							///WebDr.waitTillElementIsDisplayed("btn_Manual");
							Thread.sleep(3000);
							WebDr.click("X", "btn_Manual", "Clicking on manual radio button", true);
						}
						catch (Exception e)
						{
							e.printStackTrace();
							Reporter.ReportEvent("Object is not Found", "Object should be display on screen", "Object not present",false);
			
						}
						
					}
						
					public static void enterAccountNo(String PageObjectname,String amnt) throws Exception
					{
						
						WebDr.SetPageObjects("TellerWithdrawal");	
						
						try
						{
							WebDr.waitTillElementIsDisplayed("accountNo");
							
							WebDr.setText("X", "accountNo", WebDr.getValue(amnt), "Enter account no value", true);
						}
						catch (Exception e)
						{
							e.printStackTrace();
							Reporter.ReportEvent("Object is not Found", "Object should be display on screen", "Object not present",false);

						}

			
						}
					
					//This function is used to accept transaction.
					public static void clickOk() throws Exception
					{
						try{System.out.print("adsdfa");
							
							WebDr.click("CUSTOM", "XPATH|//span[text()='Account Currency']", "afdsdf", true);
							
//							WebDr.waitTillElementIsDisplayed("btn_Ok");
							WebDr.setText("X", "txtAmount", "11", "check", true);
							Thread.sleep(15000);
							
							WebDr.click("X", "btn_Ok", "Clicking on ok Button", true);	
							}
						
						catch(Exception e)
						{
							
						
							Reporter.ReportEvent("Test case failed", "Accept button should be click", "Accept button not clicked",false);
						}
						
					}
					
					public static void balance(String PageObjectname,String amnt ,String nar ,String name) throws Exception
					{
						
						WebDr.SetPageObjects("TellerWithdrawal");	
						
						try
						{
							WebDr.waitTillElementIsDisplayed("ammountNo");
							
							WebDr.setText("X", "ammountNo", WebDr.getValue(amnt), "Enter account no value", true);
							
							WebDr.setText("X", "narrative", WebDr.getValue(nar), "Enter narrative", true);
							
							WebDr.setText("X", "withdrawalName", WebDr.getValue(name), "Enter narrative", true);
							
							
							
							String bal1 = WebDr.page_Objects.get("availableBal");
							String bal2 = WebDr.page_Objects.get("newBal");
							
							
							
							
							
							
								
							
							
								WebElement a = WebDr.getElementObject(bal1);	
								WebElement b = WebDr.getElementObject(bal2);	
								
								String val=a.getText();
								String Value1=val.substring(4);
								String Value2=Value1.replaceAll(",", "");
								System.out.println("Value2:"+Value2);
								double avaBal=Double.parseDouble(Value2);
								System.out.println("avaBal:"+avaBal);
						
							
							
							//double newBal=Double.parseDouble(b.getText());
							
								String xal=a.getText();
								String Xalue1=xal.substring(4);
								String Xalue2=Xalue1.replaceAll(",", "");
								System.out.println("Value2:"+Value2);
								double newBal=Double.parseDouble(Value2);
								
							System.out.println("newBal:"+newBal);
							double ammount1=Double.parseDouble(WebDr.getValue(amnt));
							double ammount2=avaBal-newBal;
							
							System.out.println("ammount1:"+ammount1);
							
							System.out.println("ammount2:"+ammount2);
							if(ammount1==ammount2)
							{
								
								System.out.println("pass");
							}
							
							
							
							
							
							
							
						}
						catch (Exception e)
						{
							e.printStackTrace();
							Reporter.ReportEvent("Object is not Found", "Object should be display on screen", "Object not present",false);

						}

			
						}
			
					

					public static void agentDetails(String PageObjectname,String aid ,String aname ,String ano) throws Exception
					{
						
						WebDr.SetPageObjects("TellerWithdrawal");	
						
						try
						{
							WebDr.waitTillElementIsDisplayed("agentId");
							
							WebDr.setText("X", "agentId", WebDr.getValue(aid), "Enter account no value", true);
							
							WebDr.setText("X", "agentName", WebDr.getValue(aname), "Enter narrative", true);
							
							WebDr.setText("X", "phoneNumber", WebDr.getValue(ano), "Enter narrative", true);
							
							
						}
						catch (Exception e)
						{
							e.printStackTrace();
							Reporter.ReportEvent("Object is not Found", "Object should be display on screen", "Object not present",false);

						}

			
						}
					//this function used to click next & post button also
					public static void clickNextOrPost() throws Exception
					{
						try{
							
							
							WebDr.waitTillElementIsDisplayed("btn_Next");
							
							WebDr.click("X", "btn_Next", "Clicking on next Button", true);	
							}
						
						catch(Exception e)
						{
							
						
							Reporter.ReportEvent("Test case failed", "next button should be click", "next button not clicked",false);
						}
						
					}
					
					public static void clickAccept() throws Exception
					{
						try{
							
							
							WebDr.waitTillElementIsDisplayed("btn_Accept");
							
							WebDr.click("X", "btn_Accept", "Clicking on accept Button", true);	
							}
						
						catch(Exception e)
						{
							
						
							Reporter.ReportEvent("Test case failed", "Accept button should be click", "Accept button not clicked",false);
						}
						
					}
					

					public static void successReceipt_Message_verification() throws Exception
					{
						
						
						try {
							String ExpectedMessage="Transaction Posted and Receipt sent for Printing";
							
							WebElement oElm = WebDr.getElementObject("XPATH|//span[text()='Transaction Posted and Receipt sent for Printing']");
							if(oElm.getText().equals(ExpectedMessage))
							{
								Reporter.ReportEvent("Transaction Posted Message", "Expected message"+ExpectedMessage, "Actaul message"+oElm.getText(),true);
							}
							else
							{
								Reporter.ReportEvent("Transaction Posted Message", "Expected message"+ExpectedMessage, "Actaul message"+oElm.getText(),false);	
							}
						} catch (Exception e) {
							
							e.printStackTrace();
							Reporter.ReportEvent("Test Case execution is failed", "object Present", "object not present",false);
							
						}
						
						
					}
					
				public static void verifyDormantAccError() throws Exception
				{
					WebDr.SetPageObjects("TellerWithdrawal");	
					try {
						String ExpectedMessage="Account Status 3 Dormant. Can not proceed with Transaction";
						
						WebElement oElm = WebDr.getElementObject("XPATH|//span[text()='" + WebDr.getValue("DormantError")+ "']");
						if(oElm.getText().equals(ExpectedMessage))
						{
							Reporter.ReportEvent("Transaction Posted Message", "Expected message"+ExpectedMessage, "Actaul message"+oElm.getText(),true);
					
							WebDr.click("X", "btn_DormantOk", "Clicking on dormant ok Button", true);	
						}
						else
						{
							Reporter.ReportEvent("Transaction Posted Message", "Expected message"+ExpectedMessage, "Actaul message"+oElm.getText(),false);	
						}
					} catch (Exception e) {
						
						e.printStackTrace();
						Reporter.ReportEvent("Test Case execution is failed", "object Present", "object not present",false);
						
					}
				}
        			

				//***********************************************LIMITS*******************************************************************
				public static void setup_navigateLTLimits() throws Exception
				{
					try{
					WebDr.SetPageObjects("MasterPage");
					WebDr.click("X", "tab_setup", "Clicking on Setup Tab", true);
					WebDr.waitTillElementIsDisplayed("tab_limits");
					WebDr.click("X", "tab_limits", "Clicking on Limits", true);
					WebDr.click("X", "tab_setup", "Clicking on Setup Tab", true);
					}
					catch(Exception e)
					{
						e.printStackTrace();
						Reporter.ReportEvent("Test Case execution is failed", "Must be able to navigate limits page", "Un able to navigate to limits page",false);
				
					}
				}
				
				//Limits Verification 
				public static void setup_limitsVerification() throws Exception {
					
				    try{
					for(int i=1; i<100; i++){
			    		String verification;
			    		if(WebDr.getValue("LimitsVerification"+i).equals(""))
			    			{
			    			break;
			    			}
			    		else
			    		
			    			verification = WebDr.getValue("LimitsVerification" +i);
			    		    WebElement oElm = WebDr.getElementObject("XPATH|//p-datatable[@id='tbBranchData']//span[text()="+"'"+verification+"'"+"]");

			    		    if(verification.equals(oElm.getText()))
			    			{
			    				Reporter.ReportEvent("Limits Verifications", "Verify:"+verification+"", "Verified:"+verification+"", true);
			    			}
			    			else
			    			{
			    				Reporter.ReportEvent("Limits Verifications", "Verify:"+verification+"", "Verified:"+verification+"", false);
			    			}			
			    		}		
				}
				    catch(Exception e){
				    	e.printStackTrace();
		                Reporter.ReportEvent("Test Case execution is failed", "Limits page with data should be present", "Limits page with data is not present",false);
		                Reporter.ReportBug("Account Details are not present", "Account Details shoud be present");
		               // Driver.driver.quit();

				    }
				}
				
	
			


            
}//body class close







