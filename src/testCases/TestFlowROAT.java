package testCases;


import org.openqa.selenium.WebDriver;

import roatResuableModule.ROACDFlows;
import roatResuableModule.ROATNTBFLows;

public class TestFlowROAT
{	
	
	
	public static void executeTC(WebDriver driver, String str_tc) throws Exception
	{
		
		
		switch(str_tc)
		{
		//*************************** Regression Scripts ************************************************
		
			case "RT-LogInLogOut":ROATNTBFLows.LogInLogOut();break;
			case "RT-match":ROATNTBFLows.MM_check();break;
			case "RT-164":ROATNTBFLows.AI_AccountInquiry();break;
			case "RT-499":ROATNTBFLows.AHI_AccountHistoryInquiry();break;
			case "RT-502":ROATNTBFLows.CI_CustomerInquiry();break;
			case "RT-507":ROATNTBFLows.AS_AccountSearch();break;
			case "RT-504":ROATNTBFLows.CS_CustomerSearch();break;
			case "RT-769":ROATNTBFLows.LT_LimitsVerificationViewUpdate();break;
			case "RT-768":ROATNTBFLows.SetUp_Bank();break;
			case "RT-767":ROATNTBFLows.SetUp_Branch();break;			
			case "RT-772":ROATNTBFLows.LT_Currency();break;			
			case "RT-866":ROATNTBFLows.Setup_navigateBranchCurrency();break;
			case "RT-867":ROATNTBFLows.RC_ReferCodeMaintenace();break;
			case "RT-868":ROATNTBFLows.RC_ReferCodeMaintenaceAdd();break;
			case "RT-948":ROACDFlows.cash_Drawer_User();break;
			case "RT-938":ROACDFlows.cash_Drawer_Branch();break;
            case "RT-955":ROATNTBFLows.LT_Fees_AddNewFunctionality();break;
			case "RT-2257":ROATNTBFLows.LT_Fees_UpdateAndRejectFunctionality();break;
			case "RT-958":ROATNTBFLows.LT_Fees_AddNewFieldValidation();break;
			case "RT-2227":ROATNTBFLows.CD_LCYTotalVerification();break;
			case "RT-2228":ROATNTBFLows.FC_FCYinquiry();break;
			case "RT-2229":ROATNTBFLows.FC_FCYBalanceVerification();break;
			case "RT-2715":ROATNTBFLows.FCY_Payment();break;
			case "RT-2238":ROATNTBFLows.LCY_Payment();break;
			case "RT-2706":ROATNTBFLows.cash_DrawerLCYReceipt();break;
			case "RT-2707":ROATNTBFLows.cash_DrawerFCYReceipt();break;
			case "RT-2254":ROATNTBFLows.cash_DrawerTeller_Inquiry();break;
			case "RT-2256":ROATNTBFLows.cash_DrawerTeller_InquiryPrintDownload();break;
			case "RT-2251":ROATNTBFLows.cash_DrawerCash_Summary();break;
			case "RT-2252":ROATNTBFLows.cash_DrawerCash_SummaryPrintDownload();break;			
			case "RT-2244":ROATNTBFLows.cash_DrawerFCYTransferOut();break;
			case "RT-2566":ROATNTBFLows.cash_DrawerLCYTransferOut();break;	
			case "RT-2610":ROATNTBFLows.RemoteAuthorization();break;
			case "RT-2772":ROATNTBFLows.Setup_UserMaintenanceView();break;
            case "RT-3035":ROATNTBFLows.Setup_UserMaintenanceAdd();break;
            case "RT-3034":ROATNTBFLows.Setup_UserMaintenanceUpdate();break;
            case "RT-2712":ROATNTBFLows.AuthorizationAutoRules();break;
            case "RT-2716":ROATNTBFLows.AuthorizationAutoRulesAddNew();break;
            case "RT-2752":ROATNTBFLows.TM_TellerMatrix();break;
            case "RT-2755":ROATNTBFLows.TM_TellerMatrixAddNew();break;
            case "RT-2754":ROATNTBFLows.TM_TellerMatrixUpdate();break;
            case "RT-2708":ROATNTBFLows.Search();break;
			case "RT-2709":ROATNTBFLows.QuickLink();break;  
			case "RT-2710":ROATNTBFLows.Favorite();break;
			case "RT-3040":ROATNTBFLows.AccountNotesPopup_TellerandInquiry();break;
			case "RT-3045":ROATNTBFLows.Teller_Cash_Deposit();break;
			case "RT-3046":ROATNTBFLows.Teller_Cash_DepositEdit();break;
			case "RT-3047":ROATNTBFLows.AuthorizationDualControl();break;
			case "RT-2718":ROATNTBFLows.AuthorizationRouting();break;
			case "RT-3041":ROATNTBFLows.CD_FcyNotesExchange();break;
			case "RT-3675":ROATNTBFLows.CD_LcyNotesExchange();break;
			case "RT-3497":ROATNTBFLows.forexRateInquiry();break;
			case "RT-3498":ROATNTBFLows.forexRateUpdate();break;
			case "RT-3534":ROATNTBFLows.Vault_LCY_PaymentToATM();break;
			case "RT-3502":ROATNTBFLows.Teller_Withdrawal();break;
			case "RT-3501":ROATNTBFLows.Teller_Cash_DepositEdit();break;
			case "RT-3505":ROATNTBFLows.VaultLCYPaymentCDC();break;
			case "RT-3533":ROATNTBFLows.VaultLCYReceiptCDC();break;
			case "RT-3535":ROATNTBFLows.VaultFCYPaymentCDC();break;
			case "RT-3536":ROATNTBFLows.VaultFCYReceiptCDC();break;
			case "RT-3693":ROATNTBFLows.add_delete_StopCheque();break;
			case "RT-3694":ROATNTBFLows.view_update_StopCheque();break;
			case "RT-3499":ROATNTBFLows.Forex_Matrix();break;
			case "RT-3496":ROATNTBFLows.Vault_FCYBreakdown();break;
			case "RT-3678":ROATNTBFLows.Teller_Charges();break;
			case "RT-3495":ROATNTBFLows.VaultLCYInquiry();break;
			case "RT-3690":ROATNTBFLows.SI_Inquirystatements();break;
			case "RT-3687":ROATNTBFLows.Packages_RatePackages();break;
			case "RT-3682":ROATNTBFLows.PackagesChargePackages();break;
			case "RT-3679":ROATNTBFLows.SetupTariffMaintenence();break;
			case "RT-3747":ROATNTBFLows.Earmarks_Add();break;
			case "RT-3748":ROATNTBFLows.Earmarks_Update();break;
			case "RT-3746":ROATNTBFLows.earmarkInquiry();break;
			case "RT-3745":ROATNTBFLows.earmarkDelete();break;
			case "RT-3743":ROATNTBFLows.Forex_RatesDownload();break;
			case "RT-3750":ROATNTBFLows.Internal_transfer();break;
			case "RT-4059":ROATNTBFLows.Signature();break;
			case "RT-5020":ROATNTBFLows.Screentimeout();break;
			case "RT-4453":ROATNTBFLows.SetUp_TransactionCode();break;
//			case "RT-5365":ROATNTBFLows.before_SOD();break;
			case "RT-5366":ROATNTBFLows.after_sod_before_OCP();break;
			case "RT-5367":ROATNTBFLows.after_CCP();break;
			case "RT-4627":ROATNTBFLows.Vault_Summary();break;
			case "RT-5004":ROATNTBFLows.UserEntitlement();break;
			case "RT-5003":ROATNTBFLows.CommaSep();break;
			case "RT-5022":ROATNTBFLows.correspondentBankMaint();break;
			case "RT-5023":ROATNTBFLows.inventoryDraftBill();break;
			case "RT-5024":ROATNTBFLows.forexPurchase();break;
			case "RT-1222":ROATNTBFLows.vaultadjustment();break;
			case "RT-422":ROATNTBFLows.Setup_Bank();break;
			case "RT-5395":ROATNTBFLows.tellerOCP();break;
			case "RT-5396":ROATNTBFLows.sequenceNoMaint();break;
			case "RT-5397":ROATNTBFLows.performStatementRequest();break;
			case "RT-14":ROATNTBFLows.Decoy();break;
			case "RT-1224":ROATNTBFLows.MICRTypes();break;
			case "RT-1225":ROATNTBFLows.MICRParameters();break;
			case "RT-5025" :ROATNTBFLows.chargeApplications();break;
			case "RT-5385":ROATNTBFLows.vlt_lcy_change();break;
			case "RT-5386":ROATNTBFLows.vlt_fcy_change();break;
			case "RT-5601":ROATNTBFLows.forexDrafts();break;
			case "RT-5917":ROATNTBFLows.ActualUnClearedEffects();break;
			case "RT-5918":ROATNTBFLows.InterestUnClearedEffects();break;
			case "RT-1223":ROATNTBFLows.Vaultopencashposition();break;
			case "RT-5744":ROATNTBFLows.FrontArena();break;
			case "RT-5863":ROATNTBFLows.Accgroups();break;
			case "RT-5864":ROATNTBFLows.AccountMaintaince();break;
			case "RT-1228":ROATNTBFLows.ForexInquiryBills();break;
			case "RT-5866":ROATNTBFLows.AutoTransfers();break;
			case "RT-1230":ROATNTBFLows.MailingAddress();break;
			case "RT-5743":ROATNTBFLows.UT_AccInfoMaint();break;
			case "RT-924":ROATNTBFLows.Setup_currency_acc_maintainance();break;
			case "RT-5891":ROATNTBFLows.Setup_branch();break;
			case "RT-5869":ROATNTBFLows.draweradjustment();break;
			case "RT-5874":ROATNTBFLows.cancellationInterest();break;
			case "RT-5872":ROATNTBFLows.Reporting();break;
			case "RT-5870":ROATNTBFLows.Teller_Posting();break;
			case "RT-5871":ROATNTBFLows.batchPosting();break;
			case "RT-5890":ROATNTBFLows.AutoTransfers_cancel();break;
			case "RT-5982":ROATNTBFLows.UTCancellationFees();break;
			case "RT-5976":ROATNTBFLows.ReportingParameters();break;
			case "RT-5984":ROATNTBFLows.Internal_Clearing_Reports();break;
			case "RT-5985":ROATNTBFLows.External_Clearing_Reports();break;
			case "RT-5873":ROATNTBFLows.UT_FixedDeposit_Add_Change();break;
			case "RT-5990":ROATNTBFLows.ForexExchangeTransfer();break; ///////////////////
			case "RT-5947":ROATNTBFLows.ForexBillsNegotiableAdd();break;
			case "RT-6093":ROATNTBFLows.Forex_Exchange_ChequeDeposit();break;
			case "RT-5893":ROATNTBFLows.UT_Audit_Trails();break;
			case "RT-4874":ROATNTBFLows.mixedDeposit();break;
			case "RT-5983":ROATNTBFLows.SupportFunctions();break;
			case "RT-6208":ROATNTBFLows.Charge_Package_Search();break;
			case "RT-6207":ROATNTBFLows.Journal_Printing();break;
			case "RT-6209":ROATNTBFLows.Charge_Package_Reports();break;
			case "RT-6210":ROATNTBFLows.User_List();break;
			case "RT-6278":ROATNTBFLows.Transaction_List_Reports();break;
			case "RT-6280":ROATNTBFLows.Reporting_Parameters();break;
			case "RT-6283":ROATNTBFLows.Force_Logoff();break;
			case "RT-6321":ROATNTBFLows.CashWithdrawal_Encashment();break;
			case "RT-6369":ROATNTBFLows.Forex_Exchange_Sell();break;
			case "RT-5365":ROATNTBFLows.sod_eod_check();break;
			case "RT-487400":ROATNTBFLows.mixedDeposit();break;
			case "RT-536500":ROATNTBFLows.sod_eod_lcy_fcy_inquiry_check();break;
			case "RT-08":ROATNTBFLows.receipt_reprint();break;
			case "RT-303500":ROATNTBFLows.Setup_UserMaintenanceAdd_duty_matrix();break;
			
			//********************************End to end*************************************************
			case "RT-end":ROATNTBFLows.end_to_end_deposit();break;
			case "RT-end_withdrawal":ROATNTBFLows.end_to_end_withdrawal();break;
			case "RT-end_forex_transfer":ROATNTBFLows.end_to_end_forex_transfer();break;
			case "RT-end_mixed_deposit":ROATNTBFLows.end_to_end_mixed_deposit();break;
			case "RT-end_Teller_trans":ROATNTBFLows.E_E_Teller_transactions();break;
			
			//********************************Remote authorization scripts*************************************************
			case "RT-DepositRemote":ROATNTBFLows.end_to_end_Deposit_Remote();break;
			case "RT-RemoteAuth":ROATNTBFLows.end_to_end_Remote_Auth();break;
			
			//Deposits
			case "RT-end_deposit_part1":ROATNTBFLows.end_to_end_deposit_breaches_lcy();break;
			case "RT-end_deposit_part2":ROATNTBFLows.end_to_end_deposit_breaches_lcy_decline();break;
			case "RT-end_deposit_part3":ROATNTBFLows.end_to_end_deposit_breaches_fcy();break;
 			case "RT-end_deposit_part4":ROATNTBFLows.end_to_end_deposit_support_functions_lcy();break;
			case "RT-end_deposit_part5":ROATNTBFLows.end_to_end_deposit_support_functions_Cashback_fcy();break;
			
			//Withdrawals
			case "RT-end_withdrawal_part1":ROATNTBFLows.end_to_end_withdrawal_breaches_lcy();break;
			case "RT-end_withdrawal_part2":ROATNTBFLows.end_to_end_withdrawal_breaches_lcy_decline();break;
			case "RT-end_withdrawal_part3":ROATNTBFLows.end_to_end_withdrawal_breaches_fcy();break;
			case "RT-end_withdrawal_part4":ROATNTBFLows.end_to_end_withdrawal_support_functions_lcy();break;
			case "RT-end_withdrawal_part5":ROATNTBFLows.end_to_end_withdrawal_support_functions_fcy();break;
			
			//Forex Exchange Transfers
			case "RT-end_forex_transfer_part1":ROATNTBFLows.e_to_e_frx_trns_breaches_lcy();break;
			case "RT-end_forex_transfer_part2":ROATNTBFLows.e_to_e_frx_trns_breaches_lcy_decline();break;
			case "RT-end_forex_transfer_part3":ROATNTBFLows.e_to_e_frx_trns_breaches_fcy();break;
			case "RT-end_forex_transfer_part4":ROATNTBFLows.e_to_e_frx_trns_support_functions_lcy();break;
			case "RT-end_forex_transfer_part5":ROATNTBFLows.e_to_e_frx_trns_support_functions_fcy();break;
			
			
			//MixedDeposits
			case "RT-end_mixed_deposit_part1":ROATNTBFLows.end_to_end_mixed_breaches_lcy();break;
			case "RT-end_mixed_deposit_part2":ROATNTBFLows.end_to_end_mixed_breaches_lcy_decline();break;
			case "RT-end_mixed_deposit_part3":ROATNTBFLows.end_to_end_mixed_breaches_fcy();break;
			case "RT-end_mixed_deposit_part4":ROATNTBFLows.end_to_end_mixed_support_functions_lcy();break;
			case "RT-end_mixed_deposit_part5":ROATNTBFLows.end_to_end_mixed_support_functions_Cashback_fcy();break;
			
			//Vault Transactions
			case "RT-end_Teller_Transaction_1":ROATNTBFLows.E_E_Transactions();break;
			case "RT-end_Vault_trans_2":ROATNTBFLows.E_E_Vault_transactions();break;
			
			//Teller Posting
			case "RT-end_Teller_posting_part1":ROATNTBFLows.end_to_end_teller_posting_breaches_lcy();break;
			case "RT-end_Teller_posting_part2":ROATNTBFLows.end_to_end_teller_posting_breaches_lcy_decline();break;
			case "RT-end_Teller_posting_part3":ROATNTBFLows.end_to_end_teller_posting_breaches_fcy();break;
			case "RT-end_Teller_posting_part4":ROATNTBFLows.end_to_end_teller_posting_support_functions_lcy();break;
			case "RT-end_Teller_posting_part5":ROATNTBFLows.end_to_end_teller_posting_support_functions_fcy();break;
			
			//Batch Posting
			case "RT-end_Batch_posting_part1":ROATNTBFLows.end_to_end_batch_posting_breaches_lcy();break;
			case "RT-end_Batch_posting_part2":ROATNTBFLows.end_to_end_batch_posting_breaches_fcy();break;
			case "RT-end_Batch_posting_part3":ROATNTBFLows.end_to_end_batch_posting_support_functions_lcy();break;
			case "RT-end_Batch_posting_part4":ROATNTBFLows.end_to_end_batch_posting_support_functions_fcy();break;
			
			//Earmarks
			case "RT-end_Earmark":ROATNTBFLows.E_E_Earmarks();break;
			case "RT-end_Earmark_Breach":ROATNTBFLows.E_E_Earmarks_Breaches();break;
			
			//Forex Exchange-sell
			case "RT-end_forex_sell":ROATNTBFLows.E_E_forex_sell();break;
			case "RT-end_forex_sell_breaches":ROATNTBFLows.end_to_end_Forex_Sell_Breaches();break;
			
			//Forex Exchange-purchase
			case "RT-end_forex_purchase":ROATNTBFLows.E_E_forex_purchase();break;
			case "RT-end_forex_purchase_breaches":ROATNTBFLows.E_E_forex_purchase_Breaches();break;
			
			//Internal-Transfers
			case "RT-end_internal_transfer":ROATNTBFLows.E_E_Internal_Transfer();break;
			case "RT-end_internal_transfer_e2e":ROATNTBFLows.E_E_Internal_Transfer_end_to_end();break;
			
			//Draft-Issue
			case "RT-end_ForexDraftIssue":ROATNTBFLows.E_E_ForexDraftIssue();break;
			case "RT-end_ForexDraftIssue_Breach":ROATNTBFLows.E_E_ForexDraftIssue_Breaches();break;
			
			//Draft-Cancel
			case "RT-end_ForexDraftCancel":ROATNTBFLows.E_E_ForexDraftCancel();break;
			case "RT-end_ForexDraftCancel_Breach":ROATNTBFLows.E_E_ForexDraftCancel_Breaches();break;

			//Foex Cheque Deposit
			case "RT-end_ForexChequeDeposit":ROATNTBFLows.end_to_end_ForexChequeDeposit();break;
			case "RT-end_ForexChequeDeposit_breaches":ROATNTBFLows.end_to_end_ForexChequeDeposit_breaches();break;
			
			//Charges Module
			case "RT-end_ChargesModule":ROATNTBFLows.E2EChargesModule();break;
			case "RT-end_ChargeApplicationLimits":ROATNTBFLows.CAL();break;
			
			// bill negotiable
			case "RT-end_bill_negotiable_breaches_lcy":ROATNTBFLows.end_to_end_bill_negotiable_breaches_lcy();break;
			case "RT-end_bill_negotiable_breaches_fcy":ROATNTBFLows.end_to_end_bill_negotiable_breaches_fcy();break;
			case "RT-end_bill_negotiable_lcy_ete":ROATNTBFLows.end_to_end_bill_negotiable_lcy_ete();break;
			case "RT-end_bill_negotiable_fcy_ete":ROATNTBFLows.end_to_end_bill_negotiable_fcy_ete();break;
			case "RT-end_bill_negotiable_fcy_lcy_ete":ROATNTBFLows.end_to_end_bill_negotiable_fcy_lcy_ete();break;
			case "RT-end_bill_negotiable_lcy_lcy_ete":ROATNTBFLows.end_to_end_bill_negotiable_lcy_lcy_ete();break;
			
			//Withdrawal cheque scanning
			case "RT-end_withdrawal_chq_scanning":ROATNTBFLows.end_to_end_withdrawal_chq_scan();break;
			
		
			
			//********************************Receipt****************************************************
			case "RT-5855":ROATNTBFLows.Internal_transfer_Receipt();break;
			case "RT-5997":ROATNTBFLows.TellerChargesreceipt();break;
			case "RT-6094":ROATNTBFLows.Teller_Cash_DepositEdit_Receipt();
			
			//*************************** Health Scripts ************************************************
			case "RT-0710":ROATNTBFLows.Send_Health_Status_Mail();break;
			case "RT-164_H":ROATNTBFLows.Health_AccountInquiry();;break;
			case "RT-507_H":ROATNTBFLows.AS_AccountSearch();break;
			case "RT-504_H":ROATNTBFLows.Health_CustomerSearch();break;
			
			//************************** Sanity Scripts **************************************************
			case "RT-5210":ROATNTBFLows.LT_LimitsVerificationViewUpdate_Sanity();break;
			case "RT-5211":ROATNTBFLows.forexRateUpdate_Sanity();break;
			case "RT-7788":ROATNTBFLows.cash_DrawerLCYReceipt_Sanity();break;
			case "RT-5212":ROATNTBFLows.FCY_Payment_Sanity();break;
			case "RT-5213":ROATNTBFLows.AS_AccountSearch_sanity();break;
			case "RT-5214":ROATNTBFLows.CS_CustomerSearch_sanity();break;
			case "RT-5215":ROATNTBFLows.Teller_Cash_Deposit_sanity();break;
			case "RT-5216":ROATNTBFLows.Teller_Cash_DepositEdit_sanity();break;
			case "RT-5217":ROATNTBFLows.VaultLCYReceiptCDC_Sanity();break;
			case "RT-5218":ROATNTBFLows.VaultFCYPaymentCDC_Sanity();break;
			case "Sanity2": ROATNTBFLows.SanitySuite_SetupLimits();break;
			case "RT-5220": ROATNTBFLows.SanitySuite_CashDrawerUser();break;
			case "RT-5221": ROATNTBFLows.SanitySuite_VaultLCYReceiptFrmCDC();break;
			case "Sanity4": ROATNTBFLows.SanitySuite_VaultFCYReceiptFrmCDC();break;
			case "RT-5226":ROATNTBFLows.SanitySuite_LCYPaymentToATM();break;
			case "RT-san8":ROATNTBFLows.SanitySuite_LCYInquiry();break;
			case "RT-5228":ROATNTBFLows.SanitySuite_AccountInquiry();break;
			case "RT-5229":ROATNTBFLows.SanitySuite_TellerWithdral();break;
			case "RT-5230":ROATNTBFLows.CD_tellerInquiryImpact();break;	
			case "RT-5232":ROATNTBFLows.lycReceiptImpact();break;
			case "RT-5227":ROATNTBFLows.SanitySuite_FCYInquiry();break;
			case "RT-5223":ROATNTBFLows.Sanity_Suite_FCYPaymentFrmCDC();break;
			case "RT-5224":ROATNTBFLows.Sanity_Suite_LCYPaymentFrmCDC();break;
			case "RT-6213":ROATNTBFLows.Sanity_SupportFunctions();break;
			case "RT-6214":ROATNTBFLows.Sanity_ForexBillsNegotiableAdd();break;
			case "RT-6215":ROATNTBFLows.Sanity_sequenceNoMaint();break;
			case "RT-6216":ROATNTBFLows.Sanity_currency_acc_maintainance();break;
			case "RT-6217":ROATNTBFLows.Sanity_batchPosting();break;
			case "RT-6218":ROATNTBFLows.Sanity_chargePackage();break;
			case "RT-6219":ROATNTBFLows.Sanity_AccountMaintaince();break;
			case "RT-6220":ROATNTBFLows.Sanity_Internal_transfer_Receipt();break;
			case "RT-6221":ROATNTBFLows.Sanity_MICRParameters();break;
			case "RT-6222":ROATNTBFLows.Sanity_chargeApplication();break;
			case "RT-6223":ROATNTBFLows.Sanity_forexPurchase();break;
			case "RT-6224":ROATNTBFLows.Sanity_TariffMaintenence();break;
			case "RT-6326":ROATNTBFLows.sanity_lcy_transferout();break;
			case "RT-6327":ROATNTBFLows.sanity_lcy_changein();break;
			case "RT-6345":ROATNTBFLows.sanity_lcy_payments();break;
			case "RT-6328":ROATNTBFLows.sanity_forex_fees();break;
			case "RT-6354":ROATNTBFLows.sanity_vlt_fcy_change();break;
			case "RT-6366":ROATNTBFLows.sanity_forexDrafts();break;
			case "RT-6368":ROATNTBFLows.Sanity_Forex_Exchange_Sell();break;
			case "RT-6370":ROATNTBFLows.sanity_cash_DrawerTeller_Inquiry();break;
			case "RT-6371":ROATNTBFLows.Sanity_user_maintenance();break;
			case "RT-6361":ROATNTBFLows.Sanity_Packages_RatePackages();break;
			case "RT-59900":ROATNTBFLows.ForexExchangeTransfer();break;
			
		}
	}
	
}