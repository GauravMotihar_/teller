/*##############################################################################
'Class Name: Driver.java
'Description: 
'Prepared By: Minhaj Bakhsh
'Prepared On: 07/22/2015
'Updated By:
'Updated On:
'##############################################################################*/

package testCases;

import java.text.DecimalFormat;
import java.text.NumberFormat;
import java.util.HashMap;
import java.util.Map;
import org.openqa.selenium.WebDriver;

//import com.paulhammant.ngwebdriver.NgWebDriver;

import roatResuableModule.ROATFunctions;
import roatResuableModule.ROATValidations;
import utility.All_Countries_Url;
//import com.experitest.client.Client;
import utility.CommonUtils;
import utility.Constant;
import utility.ExcelUtils;
import utility.FetchTestData;
import utility.HtmlReporter;
import utility.JIRAFactory;
import utility.Launcher;
import utility.Reporter;
import utility.WebDr;
//Akshay
public class Driver 
{
	public static WebDriver driver = null;
	//public static NgWebDriver ngdriver = null;
	public static Map<String, String> dictionary = new HashMap<String, String>();
	public static Map<String, String> dataDictionary = new HashMap<String, String>();
	public static String userDir, file_TestData, appName, val_Url[],val_Test_Data[],EnvType,UrLp[], Country_code,UL,TestType,DB_Flag,Env_mode;
	public static String launchReport,runMode, runType, appURL, appEnv, userStories[], CIInvoke,epics,Country_Test_Data, JankinBuildNo,Env_IP;
	public static boolean dataFlag= false;
	public static Map<String, String> JIRAUserStoryStatus;	
	public static int Ucounter,urlcounter,TC_status_Pass,TC_status_fail,exsheetflag;

	//********************************************************************************************************
	//********************************************************************************************************
	public static void AppInovker(String appName, String fn_name) throws Exception
	{		
		
		switch (appName)
		{
			case "ROAT":TestFlowROAT.executeTC(Driver.driver, fn_name);break;
			default:System.out.println("Application name not found"+ appName);
	   		break;
		}
		
	}
	
	//********************************************************************************************************
	//********************************************************************************************************
	public void mainDriver(String ar[]) throws Exception
	{	
		
		exsheetflag =1;
		launchReport="0";
		TC_status_Pass = 0;
		TC_status_fail = 0;
		runType="";		
		dataFlag=false;
		userDir=System.getProperty("user.dir");
		appName =CommonUtils.readConfig("ApplicationName");
		file_TestData= appName + "_MasterData.xlsx";
		//rhea added
		Country_code =CommonUtils.readConfig("Country_code");
		System.out.println(Country_code);
		//
		JIRAFactory.loadConfigProperties();				
		WebDr.setUpRunEnvironment(ar);
		//Launching Test
		Reporter.setReporter();
		Launcher.InvokeLauncher();		
		
			
	}
	
	//********************************************************************************************************
	//********************************************************************************************************
	public static void main(String a[]) throws Exception
	{	
		//String UL = "http://22.241.20.108:4444/roatellerweb?~http://22.241.20.108:4444/roatellerweb?,GHA~TZA";
		//String UL = "http://22.241.20.108:4444/roatellerweb?,GHA~TZA";
		 String varcode[] = null ;
		 //a a a http://22.241.20.108:4444/roatellerweb?,GHA~TZA 1234 a CI-ALL a 22.241.21.110 STA a a
		try
		{
			JankinBuildNo = a[5];
			TestType = a[7];
			Env_IP  = a[9];
			Env_mode = a[11];
			UL = a[3] ;
			String URLType [] =UL.split(",");
			
			 UrLp = URLType[0].split("~");
			
			   varcode  = URLType[1].split("~");
			 urlcounter = 0;	
			EnvType = "ALL";
		}
		catch (Exception e)
		{
			JankinBuildNo = "";
			UL ="";
			EnvType = "";
			TestType = "Regression";
			Env_IP  = "22.241.21.110";
			Env_mode = "STA";
		}
		
			
		
		switch (EnvType)
		{
		case "ALL":
			for (int i =0; i<varcode.length;i++)
			{
				if (i>1&&UrLp.length>1)
				{
					
					System.out.println(UrLp [urlcounter]); 
					 urlcounter = urlcounter+1;
				}
				//rhea added below comment
				//Country_code = varcode [i];
				System.out.println(varcode [i]);
				System.out.println("*******************TEST EXECUTION STARTS*************************************************");
				Driver ob=new Driver();  
				ob.mainDriver(a);
				
				
			}
			break;
		default :
			{
			System.out.println("*******************TEST EXECUTION STARTS*************************************************");
			
			Driver ob=new Driver();  
			ob.mainDriver(a);
			break;
			}
		}
		
		
		
							
	}
	
	public void relaunch() throws Exception{
		boolean tc_result=HtmlReporter.tc_result;
		HtmlReporter.tc_result=true;
		ExcelUtils.setExcelFile(Constant.Path_TestData + Driver.file_TestData,"Configuration");
		WebDr.browser=ExcelUtils.getCellData(1,1);
		
		Driver.appURL=ExcelUtils.getCellData(6,1);
		Driver.appEnv=ExcelUtils.getCellData(4,1);
		Driver.Country_Test_Data=ExcelUtils.getCellData(13,1);
		WebDr.openApplication(Driver.appURL);		
		
	}
	//********************************************************************************************************
	//********************************************************************************************************		
}