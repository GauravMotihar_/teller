package testCases;


import org.openqa.selenium.WebDriver;

import roatResuableModule.ROACDFlows;
import roatResuableModule.SmokeROATModules;
import roatResuableModule.SmokeROATNTBFLows;

import utility.Reporter;

public class SmokeTestFlowROAT
{	
	
	
	public static void executeTC(WebDriver driver, String str_tc) throws Exception
	{
		
			
		

		switch(str_tc)
		{	
			case "RT-164":SmokeROATNTBFLows.AI_AccountInquiry();break;
			case "RT-502":SmokeROATNTBFLows.CI_CustomerInquiry();break;
			case "RT-507":SmokeROATNTBFLows.AS_AccountSearch();break;
			case "RT-504":SmokeROATNTBFLows.CS_CustomerSearch();break;
			case "RT-772":SmokeROATNTBFLows.LT_Currency();break;
			case "RT-2251":SmokeROATNTBFLows.cash_DrawerCash_Summary();break;
			case "RT-955":SmokeROATNTBFLows.LT_Fees_AddNewFunctionality();break;
			case "RT-2238":SmokeROATNTBFLows.LCY_Payment();break;
			case "RT-2752":SmokeROATNTBFLows.TM_TellerMatrix();break;
			case "RT-2754":SmokeROATNTBFLows.TM_TellerMatrixUpdate();break;
			case "RT-3303":SmokeROATNTBFLows.Teller_Withdrawal();break;
			case "RT-769":SmokeROATNTBFLows.LT_LimitsVerificationViewUpdate();break;
			

			


		}
		
	}
	
}